CREATE DEFINER=`root`@`localhost` PROCEDURE `insertSubUser`(
		IN sessid VARCHAR(64),
		IN email VARCHAR(100),
        IN permission char(4)
   )
BEGIN
   DECLARE rowsupdated INTEGER DEFAULT 0;
   SELECT userid INTO @userid FROM users WHERE sessionid = sessid;
   SELECT userid INTO @subuserid FROM users WHERE useremail = email;
   IF @subuserid > 0 THEN 
   SELECT DISTINCT subuser1,subuser2,subuser3 INTO @result1,@result2,@result3 FROM business WHERE userid = @userid;
   IF @result1 = 0 THEN 
   UPDATE business SET subuser1 = @subuserid, permission1 = permission WHERE userid = @userid;
   IF ROW_COUNT() > 0 THEN SELECT "Inserted Successfully"; ELSE SELECT "INVALID SESSION"; END IF;
   ELSE 
   IF @result2 = 0 THEN
   UPDATE business SET subuser2 = @subuserid, permission2 = permission WHERE userid = @userid;
   IF ROW_COUNT() > 0 THEN SELECT "Inserted Successfully"; ELSE SELECT "INVALID SESSION"; END IF;
   ELSE 
   IF @result3 = 0 THEN
   UPDATE business SET subuser3 = @subuserid, permission3 = permission WHERE userid = @userid;
   IF ROW_COUNT() > 0 THEN SELECT "Inserted Successfully"; ELSE SELECT "INVALID SESSION."; END IF;
   ELSE SELECT "Cannot insert more subusers. 3 subusers already present";
   END IF;
   END IF;
   END IF;
   ELSE 
   SELECT "Email Address does not exist";
   END IF;
   END
   
   
   
   Delimiter $$
   CREATE PROCEDURE insertbusiness (
    IN businessid VARCHAR(50),
    IN businessname VARCHAR(100),
    IN gstin VARCHAR(12),
    IN address VARCHAR(300),
    IN state VARCHAR(15),
    IN landline VARCHAR(20),
    IN mobile VARCHAR(20),
    IN email VARCHAR(50),
    IN fax VARCHAR(25),
    IN pincode int,
    IN sessionid VARCHAR(50)
    )
    BEGIN
    DECLARE result VARCHAR(20) DEFAULT "database error";
    DECLARE user VARCHAR (50) DEFAULT '';
    DECLARE done int DEFAULT FALSE;
    DECLARE cur1 CURSOR FOR SELECT userid FROM businessusers WHERE usertype = "s" AND businessid IN (SELECT businessid FROM businessusers WHERE userid IN (SELECT userid FROM tokendetail WHERE tokenid = sessionid));
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
    INSERT INTO business (businessid, businessname, gstin, address, state, landline, mobile, email,fax,pincode)   VALUES (businessid,businessname,gstin,address,state,landline,mobile,email,fax,pincode);
    IF ROW_COUNT() > 0 THEN
    INSERT INTO businessusers (businessid,userid,usertype) VALUES (businessid,(SELECT DISTINCT userid FROM tokendetail WHERE tokenid = sessionid),"p"); 
    IF ROW_COUNT() > 0 THEN
    open cur1;
    read_loop : LOOP
    FETCH cur1 INTO user;
    IF done THEN LEAVE read_loop; END IF;
    INSERT INTO businessusers (businessid,userid,usertype) VALUES (businessid,user,"s");
    IF ROW_COUNT() > 0 THEN SET result = "success"; ELSE SET result = "database error"; END IF;
    END LOOP;
    ELSE SET result = "database error"; END IF;
    ELSE SET result = "database error"; 
    END IF; 	   
    SELECT result;
    END $$
    
    
    
    Delimiter $$
    CREATE PROCEDURE insertfy (
    IN businessid VARCHAR(36),
    IN fy VARCHAR(10),
    IN sessionid VARCHAR(36),
    IN fyid VARCHAR(36)
    )
    BEGIN
    DECLARE result VARCHAR(20) DEFAULT "Invalid Entry";
    DECLARE done int DEFAULT FALSE;
    DECLARE businessidcheck VARCHAR(36) DEFAULT NULL;
    DECLARE cur1 CURSOR FOR SELECT businessid FROM businessusers WHERE userid IN (SELECT userid FROM tokendetail WHERE tokenid = sessionid);
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
    
    OPEN cur1;
    read_loop : LOOP
    	FETCH cur1 INTO businessidcheck;
    	IF done THEN LEAVE read_loop; END IF;
    	IF businessid = businessidcheck THEN 
    	INSERT INTO financialyear (fyid,businessid,financialyear) VALUES (fyid,businessid,fy);
    	IF ROW_COUNT() > 0 THEN SET result = "success"; ELSE SET result = "database error"; END IF;
    	END IF; 
    END LOOP;	
    SELECT result;	
    END $$
    
    {
	"businessname" : "Pearlkraft",
	"gstin" : "07AACCB672",
	"address" : "Fathepuri ChandniChowk",
	"state" : "Delhi",
	"mobile" : "9310529865",
	"email" : "ankit.s.bagaria@gmail.com",
	"pincode" : "201304"
}

{
	"businessid": "63abe229-58e1-4c4c-9cde-833560bbf44f",
	"financialyear" : "2018-2019" 
}


Delimiter $$
CREATE PROCEDURE insertSubUser (
IN email VARCHAR(100),
IN sessionid VARCHAR(36)
)
BEGIN
DECLARE result VARCHAR(50) DEFAULT "user doesnot exist";
DECLARE buss VARCHAR(36) DEFAULT '';
DECLARE done int DEFAULT FALSE;
DECLARE cur1 CURSOR FOR SELECT businessid from businessusers WHERE userid IN (SELECT userid FROM tokendetail WHERE tokenid = sessionid); 
DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
SELECT userid INTO @userid FROM users WHERE useremail = email;
IF @userid > 0
THEN
SET result = @userid; 
OPEN cur1;
read_loop : LOOP
FETCH cur1 INTO buss;
IF done THEN LEAVE read_loop; END IF;
INSERT INTO businessusers (businessid,userid,usertype) VALUES (buss,@userid,'s');
IF ROW_COUNT() > 0 THEN SET result = "success"; ELSE SET result = "Database error"; END IF;
END LOOP;
END IF; 
SELECT result;
END $$

















    
    
    
    
    
    
     
