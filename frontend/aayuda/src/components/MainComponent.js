import React, { Component } from 'react';
import { Switch, Route, Redirect, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { postLoginUser, postRegisterUser, getBusinessList, postAddBusiness, resetBusinessFormAndMessage, postAddSubUser, resetAddSubUserMessage, getSubUserList, deleteSubUser, addFinancialYear, updateBusiness, resetBusinessChangeMessage, addCustomer, getCustomerList, updateCustomer, resetCustomerChange, addVendor, getVendorList, updateVendor, resetVendorChange, addBankDetail, getBankDetailList, updateBankDetail, resetBankDetailChange, addItem, getItemList, updateItem, resetItemChange, raiseInvoice, getSalesData, pushNewBusiness, resetAddBusinessMessage, refreshBusinessList, refreshCustomerState, refreshVendorState, refreshItemState, refreshBankState, resetCVIB, resetaddFinancialYearMessage, resetaddFinancialYear, consumeIndividualBusinessMessage, getIndividualBusiness, refreshCustomerStateSecond, resetAddCustomerMessage, resetAddCustomerMessageAndForm, resetAddVendorMessage, resetAddVendorMessageAndForm, refreshVendorStateSecond, resetAddBankDetailMessage, resetAddBankMessageAndForm, refreshBankDetailStateSecond, resetAddItemMessageAndForm, resetAddItemMessage, refreshItemStateSecond, resetSalesDetail, resetIndividualBusinessMessage, resetaddInvoiceMessage, addPurchaseItem, raisePurchase, getPurchaseData, resetPurchaseDetailSecond, resetPurchaseDetail, resetaddPurchaseMessage, cancelInvoice, resetCancelInvoiceMessage, restoreInvoice, resetRestoreInvoiceMessage, invoiceUpdate, resetInvoiceUpdateMessage, resetSalesDetailSecond, resetDeletePurchaseMessage, raiseExpense, resetaddExpenseMessage, getExpenseDetail, resetExpenseDetail, resetExpenseDetailSecond, raisePayment, getPaymentDetail, resetPaymentDetail, resetPaymentDetailSecond, resetaddPaymentMessage, raiseBankEntry, getBankEntryDetail, resetaddBankEntryMessage, resetBankEntryDetailSecond, resetBankEntryDetail, getOpeningClosingDetail, raiseOpeningClosing, resetaddOpeningClosingMessage, resetOpeningClosingDetailSecond, resetOpeningClosingDetail, updatePartyOpeningBalance, resetOpeningBalanceSecond, resetOpeningBalance, getPartyOpeningBalance, resetLogoutMessage, logout, refreshUpdatepartyOpeningBalance, updateCashInHandDetail, getCashInHandDetail, resetCashInHandDetail, resetCashInHand, getEmployeeList, resetAddEmployeeMessage, addEmployee, refreshEmployeeState, updateAttendance, getAttendanceList, resetGetAttendance, resetUpdateAttendanceMessage, getMonthAttendanceList, resetGetMonthAttendance, updateSalary, getSalaryList, resetGetSalary, resetUpdateSalaryMessage, purchaseUpdate, resetPurchaseUpdateMessage, resetDeleteExpenseMessage, cancelExpense, getBankOpeningBalanceList, refreshUpdateBankOpeningBalance, updateBankOpeningBalance, resetBankOpeningBalance, resetGSTOpeningBalanceMessage, getGSTOpeningBalanceDetail, updateGSTOpeningBalance, resetGSTOpeningBalance, refreshDebitCredit, resetaddDebitCreditMessage, getDebitCreditList, raiseDebitCreditNote, deleteEntry, resetDeleteEntry, getExpenseHeadList, addExpenseHead, refreshExpenseHead, resetAddExpenseHeadMessageAndForm, resetAddExpenseHeadMessage, resetIncomeOSDetail, resetaddIncomeOSMessage, raiseIncomeOS, getIncomeOSDetail, reviseSalary, resetReviseSalaryMessage, terminateEmployee, resetTerminateEmployeeMessage, changePassword, resetChangePasswordMessage, raiseLoan, resetaddLoanMessage, refreshLoanState, getLoanDetail, getLoanAccountList, refreshLoanAccountState, addLoanAccount, resetAddLoanAccountMessage, resetAddLoanAccountMessageAndForm, refreshInvestmentState, getInvestmentDetail, raiseInvestment, resetaddInvestmentMessage, getIncomeHeadList, refreshIncomeHead, resetAddIncomeHeadMessageAndForm, resetAddIncomeHeadMessage, addIncomeHead, refreshAssetAccountState, getAssetAccountList, resetAddAssetAccountMessage, resetAddAssetAccountMessageAndForm, addAssetAccount, raiseAsset, resetaddAssetMessage, sellAsset, getDepreciationList, raiseDepreciation, refreshDepreciationState, getLoanOpeningBalanceList, resetLoanOpeningBalance, refreshUpdateLoanOpeningBalance, updateLoanOpeningBalance, updateAssetOpeningBalance, getAssetOpeningBalanceList, resetAssetOpeningBalance, refreshUpdateAssetOpeningBalance, resetAddDepreciationMessage, getInvestmentAccountList, refreshInvestmentAccountState, addInvestmentAccount, resetAddInvestmentAccountMessage, resetAddInvestmentAccountMessageAndForm, refreshUpdateInvestmentOpeningBalance, updateInvestmentOpeningBalance, resetInvestmentOpeningBalance, getInvestmentOpeningBalanceList, refreshUpdateTDSOpeningBalance, updateTDSOpeningBalance, resetTDSOpeningBalance, getTDSOpeningBalanceList, updateRCOpeningBalance, refreshUpdateRCOpeningBalance, getRCOpeningBalanceList, resetRCOpeningBalance, refreshUpdateStockOpeningBalance, updateStockOpeningBalance, getStockOpeningBalanceList, resetStockOpeningBalance, updateIncomeTaxOpeningBalance, getIncomeTaxOpeningBalanceList, refreshUpdateIncomeTaxOpeningBalance, resetIncomeTaxOpeningBalance, updateInvestmentAccount, resetUpdateInvestmentAccountMessage, passStockJournal, refreshStockJournalState, getStockJournalList, refreshGetStockJournal, getEmployeeOpeningBalanceList, refreshUpdateEmployeeOpeningBalance, resetEmployeeOpeningBalance, updateEmployeeOpeningBalance } from '../redux/actionCreators';
import { actions } from 'react-redux-form';
import Login from './LoginComponent';
import BusinessList from './BusinessListComponent';

const BusinessDetail = React.lazy(() => import('./BusinessDetailComponent'));
const AddBusiness = React.lazy(() => import('./AddBusinessComponent'))
const Register = React.lazy(() => import('./RegisterUserComponent'));
const AddSubUser = React.lazy(() => import('./AddSubUserComponent'));
const BusinessChange = React.lazy(() => import('./BusinessChangeComponent'));
const BusinessProcess = React.lazy(() => import('./BusinessProcessComponent'));
const Customer = React.lazy(() => import('./CustomerComponent'));
const Vendor = React.lazy(() => import('./VendorComponent'));
const BankDetail = React.lazy(() => import('./BankDetailComponent'));
const Item = React.lazy(() => import('./ItemComponent'));
const CustomerChange = React.lazy(() => import('./CustomerChangeComponent'));
const VendorChange = React.lazy(() => import('./VendorChangeComponent'));
const BankDetailChange = React.lazy(() => import('./BankDetailChangeComponent'));
const ItemChange = React.lazy(() => import('./ItemChangeComponent'));
const RaiseInvoice = React.lazy(() => import('./RaiseInvoieComponent'));
const Purchase = React.lazy(() => import('./PurchaseComponent'));
const SalesDetail = React.lazy(() => import('./SalesDetailComponent'));
const PurchaseDetail = React.lazy(() => import('./PurchaseDetailComponent'));
const InvoiceUpdate = React.lazy(() => import('./InvoiceUpdateComponent'));
const Expense = React.lazy(() => import('./ExpenseComponent'));
const Payment = React.lazy(() => import('./PaymentComponent'));
const BankEntry = React.lazy(() => import('./BankEntryComponent'));
const OpeningClosing = React.lazy(() => import('./OpeningClosingComponent'));
const ProfitLoss = React.lazy(() => import('./ProfitLossComponent'));
const OpeningBalance = React.lazy(() => import('./ConfigureOpeningBalance'));
const AccountStatement = React.lazy(() => import('./AccountStatement'));
const BusinessChoice = React.lazy(() => import('./BusinessChoiceComponent'));
const CashFlow = React.lazy(() => import('./CashFlowComponent'));
const CashInHand = React.lazy(() => import('./CashInHandComponent'));
const CashInHandStatement = React.lazy(() => import('./CashInHandStatementComponent'));
const Employee = React.lazy(() => import('./EmployeeComponent'));
const PayRollChoice = React.lazy(() => import('./PayRollChoiceComponent'));
const MarkAttendance = React.lazy(() => import('./MarkAttendance'));
const ViewAttendance = React.lazy(() => import('./ViewAttendance'));
const CalculateSalary = React.lazy(() => import('./CalculateSalaryComponent'));
const PurchaseUpdate = React.lazy(() => import('./PurchaseUpdateComponent'));
const ExpenseDetail = React.lazy(() => import('./ExpenseDetailComponent'));
const BankBookStatement = React.lazy(() => import('./BankBookComponent'));
const BankOpeningBalance = React.lazy(() => import('./BankOpeningBalanceComponent'));
const GSTOpeningBalance = React.lazy(() => import('./GSTOpeningBalanceComponent'));
const GSTStatement = React.lazy(() => import('./GstStatementComponent'));
const DebitCredit = React.lazy(() => import('./DebitCreditNoteComponent'));
const ReverseChargeDetail = React.lazy(() => import('./ReverseChargeLedger'));
const ExpenseHead = React.lazy(() => import('./ExpenseHeadComponent'));
const IncomeHead = React.lazy(() => import('./IncomeHeadComponent'));
const IncomeOS = React.lazy(() => import('./IncomeFromOtherSoucesComponent'));
const ReviseSalary = React.lazy(() => import('./ReviseSalaryComponent'));
const Terminate = React.lazy(() => import('./Terminate'));
const ChangePassword = React.lazy(() => import('./ChangePasswordComponent'));
const LoanEntry = React.lazy(() => import('./LoanComponent'));
const LoanAccount = React.lazy(() => import('./LoanAccountComponent'));
const LoanBook = React.lazy(() => import('./LoanBookComponent'));
const Investment = React.lazy(() => import('./InvestmentComponent'));
const InvestmentBook = React.lazy(() => import('./InvestmentBookComponent'));
const TDSStatement = React.lazy(() => import('./TDSBookComponent'));
const IncomeDetail = React.lazy(() => import('./IncomeDetailComponent'));
const AssetAccount = React.lazy(() => import('./AssetAccountComponent'));
const AssetDepreciation = React.lazy(() => import('./Depreciation'));
const LoanOpeningBalance = React.lazy(() => import('./LoanOpeningBalance'));
const AssetOpeningBalance = React.lazy(() => import('./AssetOpeningBalance'));
const AssetBook = React.lazy(() => import('./AssetBookComponent'));
const AccountReceivables = React.lazy(() => import('./AccountReceivablesComponent'));
const AccountPayables = React.lazy(() => import('./AccountPayableComponent'));
const InvestmentAccount = React.lazy(() => import('./InvestmentAccountComponent'));
const InvestmentOpeningBalance = React.lazy(() => import('./InvestmentOpeningBalance'));
const TDSOpeningBalance = React.lazy(() => import('./TDSOpeningBalance'));
const TrialBalance = React.lazy(() => import('./TrialBalanceComponent'));
const RCOpeningBalance = React.lazy(() => import('./ReverseChargeOpeningBalance'));
const Inventory = React.lazy(() => import('./InventoryComponent'));
const Stock = React.lazy(() => import('./StockComponent'));
const StockOpeningBalance = React.lazy(() => import('./StockOpeningBalanceComponent'));
const IncomeTaxBook = React.lazy(() => import('./IncomeTaxBookComponent'));
const IncomeTaxOpeningBalance = React.lazy(() => import('./IncomeTaxOpeningBalance'));
const InvestmentAccountChange = React.lazy(() => import('./InvestmentAccountChangeComponent'));
const StockJournal = React.lazy(() => import('./StockJournalComponent'));
const EmployeeAccountStatement = React.lazy(() => import('./EmployeeAccountStatement'));
const EmployeeOpeningBalance = React.lazy(() => import('./EmployeeOpeningBalance'))

const mapStateToProps = (state) => {
    return ({
        loginMessage: state.loginMessage,
        registerMessage: state.registerMessage,
        businessList: state.businessList,
        addBusinessMessage: state.addBusinessMessage,
        addSubUserMessage: state.addSubUserMessage,
        subUserList: state.subUserList,
        deleteSubUserState: state.deleteSubUser,
        addFinancialYearMessage: state.addFinancialYearMessage,
        updateBusinessMessage: state.updateBusinessMessage,
        addCustomerMessage: state.addCustomerMessage,
        getCustomer: state.getCustomer,
        updateCustomerMessage: state.updateCustomerMessage,
        addVendorMessage: state.addVendorMessage,
        getVendor: state.getVendor,
        updateVendorMessage: state.updateVendorMessage,
        addBankDetailMessage: state.addBankDetailMessage,
        getBankDetail: state.getBankDetail,
        updateBankDetailMessage: state.updateBankDetailMessage,
        addItemMessage: state.addItemMessage,
        getItem: state.getItem,
        updateItemMessage: state.updateItemMessage,
        addInvoiceMessage: state.addInvoiceMessage,
        salesDetail: state.salesDetail,
        individualBusinessMessage: state.individualBusinessMessage,
        addPurchaseMessage: state.addPurchaseMessage,
        purchaseDetail: state.purchaseDetail,
        cancelInvoiceMessage: state.cancelInvoiceMessage,
        restoreInvoiceMessage: state.restoreInvoiceMessage,
        invoiceUpdateMessage: state.invoiceUpdateMessage,
        deletePurchaseMessage: state.deletePurchaseMessage,
        addExpenseMessage: state.addExpenseMessage,
        getExpense: state.getExpense,
        addPaymentMessage: state.addPaymentMessage,
        getPayment: state.getPayment,
        addBankEntryMessage: state.addBankEntryMessage,
        getBankEntry: state.getBankEntry,
        addOpeningClosingMessage: state.addOpeningClosingMessage,
        getOpeningClosing: state.getOpeningClosing,
        updatePartyOpeningMessage: state.updatePartyOpeningMessage,
        getOpeningBalance: state.getOpeningBalance,
        logoutMessage: state.logoutMessage,
        getCashInHand: state.getCashInHand,
        updateCashInHandMessage: state.updateCashInHandMessage,
        getEmployee: state.getEmployee,
        addEmployeeMessage: state.addEmployeeMessage,
        getAttendance: state.getAttendance,
        updateAttendanceMessage: state.updateAttendanceMessage,
        getMonthAttendance: state.getMonthAttendance,
        getSalary: state.getSalary,
        updateSalaryMessage: state.updateSalaryMessage,
        purchaseUpdateMessage: state.purchaseUpdateMessage,
        deleteExpenseMessage: state.deleteExpenseMessage,
        getBankOpeningBalance: state.getBankOpeningBalance,
        updateBankOpeningMessage: state.updateBankOpeningMessage,
        getGSTOpeningBalance: state.getGSTOpeningBalance,
        updateGSTOpeningBalanceMessage: state.updateGSTOpeningBalanceMessage,
        addDebitCreditMessage: state.addDebitCreditMessage,
        getDebitCredit: state.getDebitCredit,
        deleteEntryMessage: state.deleteEntryMessage,
        getExpenseHead: state.getExpenseHead,
        addExpenseHeadMessage: state.addExpenseHeadMessage,
        addIncomeOSMessage: state.addIncomeOSMessage,
        getIncomeOS: state.getIncomeOS,
        reviseSalaryMessage: state.reviseSalaryMessage,
        terminateEmployeeMessage: state.terminateEmployeeMessage,
        changePasswordMessage: state.changePasswordMessage,
        addLoanMessage: state.addLoanMessage,
        getLoan: state.getLoan,
        getLoanAccount: state.getLoanAccount,
        addLoanAccountMessage: state.addLoanAccountMessage,
        getInvestment: state.getInvestment,
        addInvestmentMessage: state.addInvestmentMessage,
        addIncomeHeadMessage: state.addIncomeHeadMessage,
        getIncomeHead: state.getIncomeHead,
        getAssetAccount: state.getAssetAccount,
        addAssetAccountMessage: state.addAssetAccountMessage,
        addAssetMessage: state.addAssetMessage,
        addDepreciationMessage: state.addDepreciationMessage,
        getDepreciation: state.getDepreciation,
        getLoanOpeningBalance: state.getLoanOpeningBalance,
        updateLoanOpeningMessage: state.updateLoanOpeningMessage,
        updateAssetOpeningMessage: state.updateAssetOpeningMessage,
        getAssetOpeningBalance: state.getAssetOpeningBalance,
        getInvestmentAccount: state.getInvestmentAccount,
        addInvestmentAccountMessage: state.addInvestmentAccountMessage,
        updateInvestmentOpeningMessage: state.updateInvestmentOpeningMessage,
        getInvestmentOpeningBalance: state.getInvestmentOpeningBalance,
        updateTDSOpeningMessage: state.updateTDSOpeningMessage,
        getTDSOpeningBalance: state.getTDSOpeningBalance,
        getRCOpeningBalance: state.getRCOpeningBalance,
        updateRCOpeningMessage: state.updateRCOpeningMessage,
        updateStockOpeningMessage: state.updateStockOpeningMessage,
        getStockOpeningBalance: state.getStockOpeningBalance,
        getIncomeTaxOpeningBalance: state.getIncomeTaxOpeningBalance,
        updateIncomeTaxOpeningMessage: state.updateIncomeTaxOpeningMessage,
        updateInvestmentAccountMessage: state.updateInvestmentAccountMessage,
        passStockJournalMessage: state.passStockJournalMessage,
        getStockJournal: state.getStockJournal,
        updateEmployeeOpeningMessage: state.updateEmployeeOpeningMessage,
        getEmployeeOpeningBalance: state.getEmployeeOpeningBalance
    })
}
const mapDispatchToProps = (dispatch) => ({
    postLoginUserDispatcher: (email, password) => postLoginUser(email, password, dispatch),
    postRegisterUser: (fullname, email, password) => postRegisterUser(fullname, email, password, dispatch),
    resetLoginForm: () => dispatch(actions.reset('login')),
    resetRegisterForm: () => dispatch(actions.reset('register')),
    resetAddBusinessForm: () => dispatch(actions.reset('addBusiness')),
    resetAddSubUserForm: () => dispatch(actions.reset('addSubUser')),
    getBusinessList: () => getBusinessList(dispatch),
    refreshBusinessList: () => refreshBusinessList(dispatch),
    postAddBusiness: (businessname, alias, gstin, state, address, pincode, mobile, landline, email, stateut, pan, businesstype, noofshares, fv) => postAddBusiness(businessname, alias, gstin, state, address, pincode, mobile, landline, email, stateut, pan, businesstype, noofshares, fv, dispatch),
    resetBusinessFormAndMessage: () => resetBusinessFormAndMessage(dispatch),
    resetAddBusinessMessage: () => resetAddBusinessMessage(dispatch),
    postAddSubUser: (email, write, update, del) => postAddSubUser(email, write, update, del, dispatch),
    resetAddSubUserMessage: () => resetAddSubUserMessage(),
    getSubUserList: () => getSubUserList(dispatch),
    deleteSubUser: (subUserEmail) => deleteSubUser(subUserEmail, dispatch),
    resetAddFinancialYear: () => dispatch(actions.reset('addFinancialYear')),
    addFinancialYear: (financialyear, businessid) => addFinancialYear(financialyear, businessid, dispatch),
    resetBusinessChange: () => dispatch(actions.reset('businessChange')),
    updateBusiness: (businessid, financial, businessname, alias, gstin, state, address, pincode, mobile, landline, email, pan, businesstype, noofshares, fv) => updateBusiness(businessid, financial, businessname, alias, gstin, state, address, pincode, mobile, landline, email, pan, businesstype, noofshares, fv, dispatch),
    setDefaultBusinessChange: (values) => dispatch(actions.merge('businessChange', values)),
    resetBusinessChangeMessage: () => resetBusinessChangeMessage(dispatch),
    addCustomer: (businessid, customerDetail) => addCustomer(businessid, customerDetail, dispatch),
    getCustomerList: (businessid) => getCustomerList(businessid, dispatch),
    updateCustomer: (businessid, customerid, customerdetail) => updateCustomer(businessid, customerid, customerdetail, dispatch),
    setDefaultCustomerChange: (values) => dispatch(actions.merge('customerChange', values)),
    setDefaultInvestmentAccountChange: (values) => dispatch(actions.merge('updateInvestmentDetail', values)),
    resetCustomerChange: () => resetCustomerChange(dispatch),
    addVendor: (businessid, vendorDetail) => addVendor(businessid, vendorDetail, dispatch),
    getVendorList: (businessid) => getVendorList(businessid, dispatch),
    updateVendor: (businessid, vendorid, vendordetail) => updateVendor(businessid, vendorid, vendordetail, dispatch),
    setDefaultVendorChange: (values) => dispatch(actions.merge('vendorChange', values)),
    resetVendorChange: (msg) => resetVendorChange(msg, dispatch),
    addBankDetail: (businessid, vendorDetail) => addBankDetail(businessid, vendorDetail, dispatch),
    getBankDetailList: (businessid) => getBankDetailList(businessid, dispatch),
    updateBankDetail: (businessid, vendorid, vendordetail) => updateBankDetail(businessid, vendorid, vendordetail, dispatch),
    setDefaultBankDetailChange: (values) => dispatch(actions.merge('bankDetailChange', values)),
    resetBankDetailChange: () => resetBankDetailChange(dispatch),
    addItem: (businessid, itemDetail) => addItem(businessid, itemDetail, dispatch),
    getItemList: (businessid) => getItemList(businessid, dispatch),
    updateItem: (businessid, itemid, itemdetail) => updateItem(businessid, itemid, itemdetail, dispatch),
    setDefaultItemChange: (values) => dispatch(actions.merge('itemChange', values)),
    resetItemChange: () => resetItemChange(dispatch),
    raiseInvoice: (businessid, financialyearid, state) => raiseInvoice(businessid, financialyearid, state, dispatch),
    raisePurchase: (businessid, financialyearid, state, getVendor) => raisePurchase(businessid, financialyearid, state, dispatch, getVendor),
    raiseAsset: (businessid, fyid, state) => raiseAsset(businessid, fyid, state, dispatch),
    getSalesData: (businessid, fyid) => getSalesData(businessid, fyid, dispatch),
    pushNewBusiness: (value, id) => pushNewBusiness(value, id, dispatch),
    refreshCustomerState: () => refreshCustomerState(dispatch),
    refreshVendorState: () => refreshVendorState(dispatch),
    refreshItemState: () => refreshItemState(dispatch),
    refreshBankState: () => refreshBankState(dispatch),
    resetCVIB: () => resetCVIB(dispatch),
    resetaddFinancialYearMessage: () => resetaddFinancialYearMessage(dispatch),
    resetaddFinancialYear: () => resetaddFinancialYear(dispatch),
    consumeIndividualBusinessMessage: () => consumeIndividualBusinessMessage(dispatch),
    getIndividualBusiness: (businessid) => getIndividualBusiness(businessid, dispatch),
    refreshCustomerStateSecond: () => refreshCustomerStateSecond(dispatch),
    resetAddCustomerMessage: () => resetAddCustomerMessage(dispatch),
    resetAddCustomerMessageAndForm: () => resetAddCustomerMessageAndForm(dispatch),
    resetAddVendorMessage: () => resetAddVendorMessage(dispatch),
    resetAddVendorMessageAndForm: () => resetAddVendorMessageAndForm(dispatch),
    refreshVendorStateSecond: () => refreshVendorStateSecond(dispatch),
    refreshBankDetailStateSecond: () => refreshBankDetailStateSecond(dispatch),
    resetAddBankDetailMessage: () => resetAddBankDetailMessage(dispatch),
    resetAddBankMessageAndForm: () => resetAddBankMessageAndForm(dispatch),
    resetAddItemMessageAndForm: () => resetAddItemMessageAndForm(dispatch),
    resetAddItemMessage: () => resetAddItemMessage(dispatch),
    refreshItemStateSecond: () => refreshItemStateSecond(dispatch),
    resetSalesDetail: () => resetSalesDetail(dispatch),
    resetIndividualBusinessMessage: () => resetIndividualBusinessMessage(dispatch),
    resetaddInvoiceMessage: (message) => resetaddInvoiceMessage(message, dispatch),
    addPurchaseItem: () => addPurchaseItem(dispatch),
    getPurchaseData: (businessid, fyid) => getPurchaseData(businessid, fyid, dispatch),
    resetPurchaseDetailSecond: () => resetPurchaseDetailSecond(dispatch),
    resetPurchaseDetail: () => resetPurchaseDetail(dispatch),
    resetaddPurchaseMessage: (message) => resetaddPurchaseMessage(message, dispatch),
    cancelInvoice: (invoiceid, businessid, fyid) => cancelInvoice(invoiceid, businessid, fyid, dispatch),
    resetCancelInvoiceMessage: () => resetCancelInvoiceMessage(dispatch),
    restoreInvoice: (invoiceid, businessid, fyid) => restoreInvoice(invoiceid, businessid, fyid, dispatch),
    resetRestoreInvoiceMessage: () => resetRestoreInvoiceMessage(dispatch),
    invoiceUpdate: (businessid, fyid, state) => invoiceUpdate(businessid, fyid, state, dispatch),
    resetInvoiceUpdateMessage: () => resetInvoiceUpdateMessage(dispatch),
    resetSalesDetailSecond: () => resetSalesDetailSecond(dispatch),
    resetDeletePurchaseMessage: () => resetDeletePurchaseMessage(dispatch),
    raiseExpense: (businessid, fyid, state) => raiseExpense(businessid, fyid, state, dispatch),
    resetaddExpenseMessage: (message) => resetaddExpenseMessage(message, dispatch),
    getExpenseDetail: (businessid, fyid) => getExpenseDetail(businessid, fyid, dispatch),
    resetExpenseDetail: () => resetExpenseDetail(dispatch),
    resetExpenseDetailSecond: () => resetExpenseDetailSecond(dispatch),
    raisePayment: (businessid, fyid, state) => raisePayment(businessid, fyid, state, dispatch),
    getPaymentDetail: (businessid, fyid) => getPaymentDetail(businessid, fyid, dispatch),
    resetPaymentDetailSecond: () => resetPaymentDetailSecond(dispatch),
    resetPaymentDetail: () => resetPaymentDetail(dispatch),
    resetaddPaymentMessage: (message) => resetaddPaymentMessage(message, dispatch),
    raiseBankEntry: (businessid, fyid, state) => raiseBankEntry(businessid, fyid, state, dispatch),
    getBankEntryDetail: (businessid, fyid) => getBankEntryDetail(businessid, fyid, dispatch),
    resetaddBankEntryMessage: (message) => resetaddBankEntryMessage(message, dispatch),
    resetBankEntryDetailSecond: () => resetBankEntryDetailSecond(dispatch),
    resetBankEntryDetail: () => resetBankEntryDetail(dispatch),
    getOpeningClosingDetail: (businessid, fyid) => getOpeningClosingDetail(businessid, fyid, dispatch),
    raiseOpeningClosing: (businessid, fyid, state) => raiseOpeningClosing(businessid, fyid, state, dispatch),
    resetaddOpeningClosingMessage: (message) => resetaddOpeningClosingMessage(message, dispatch),
    resetOpeningClosingDetailSecond: () => resetOpeningClosingDetailSecond(dispatch),
    resetOpeningClosingDetail: () => resetOpeningClosingDetail(dispatch),
    updatePartyOpeningBalance: (state, businessid, fyid) => updatePartyOpeningBalance(state, businessid, fyid, dispatch),
    getPartyOpeningBalance: (businessid, fyid) => getPartyOpeningBalance(businessid, fyid, dispatch),
    resetOpeningBalanceSecond: () => resetOpeningBalanceSecond(dispatch),
    resetOpeningBalance: () => resetOpeningBalance(dispatch),
    logout: () => logout(dispatch),
    resetLogoutMessage: () => resetLogoutMessage(dispatch),
    refreshUpdatepartyOpeningBalance: () => refreshUpdatepartyOpeningBalance(dispatch),
    updateCashInHandDetail: (businessid, fyid, amount) => updateCashInHandDetail(businessid, fyid, amount, dispatch),
    getCashInHandDetail: (businessid, fyid) => getCashInHandDetail(businessid, fyid, dispatch),
    resetCashInHandDetail: () => resetCashInHandDetail(dispatch),
    resetCashInHand: () => resetCashInHand(dispatch),
    getEmployeeList: (businessid) => getEmployeeList(businessid, dispatch),
    resetAddEmployeeMessage: () => resetAddEmployeeMessage(dispatch),
    addEmployee: (state, businessid) => addEmployee(state, businessid, dispatch),
    refreshEmployeeState: () => refreshEmployeeState(dispatch),
    updateAttendance: (fyid, date, attendanceobject) => updateAttendance(fyid, date, attendanceobject, dispatch),
    getAttendanceList: (fyid, date) => getAttendanceList(fyid, date, dispatch),
    resetGetAttendance: () => resetGetAttendance(dispatch),
    resetUpdateAttendanceMessage: () => resetUpdateAttendanceMessage(dispatch),
    getMonthAttendanceList: (fyid, month, year) => getMonthAttendanceList(fyid, month, year, dispatch),
    resetGetMonthAttendance: () => resetGetMonthAttendance(dispatch),
    updateSalary: (fyid, month, state) => updateSalary(fyid, month, state, dispatch),
    getSalaryList: (fyid, month) => getSalaryList(fyid, month, dispatch),
    resetUpdateSalaryMessage: () => resetUpdateSalaryMessage(dispatch),
    resetGetSalary: () => resetGetSalary(dispatch),
    purchaseUpdate: (businessid, fyid, state) => purchaseUpdate(businessid, fyid, state, dispatch),
    resetPurchaseUpdateMessage: () => resetPurchaseUpdateMessage(dispatch),
    resetDeleteExpenseMessage: () => resetDeleteExpenseMessage(dispatch),
    cancelExpense: (expenseid, businessid, fyid) => cancelExpense(expenseid, businessid, fyid, dispatch),
    getBankOpeningBalanceList: (businessid, fyid) => getBankOpeningBalanceList(businessid, fyid, dispatch),
    refreshUpdateBankOpeningBalance: () => refreshUpdateBankOpeningBalance(dispatch),
    updateBankOpeningBalance: (businessid, fyid, state) => updateBankOpeningBalance(businessid, fyid, state, dispatch),
    resetBankOpeningBalance: () => resetBankOpeningBalance(dispatch),
    resetGSTOpeningBalanceMessage: () => resetGSTOpeningBalanceMessage(dispatch),
    getGSTOpeningBalanceDetail: (businessid, fyid) => getGSTOpeningBalanceDetail(businessid, fyid, dispatch),
    updateGSTOpeningBalance: (businessid, fyid, state) => updateGSTOpeningBalance(businessid, fyid, state, dispatch),
    resetGSTOpeningBalance: () => resetGSTOpeningBalance(dispatch),
    refreshDebitCredit: () => refreshDebitCredit(dispatch),
    resetaddDebitCreditMessage: () => resetaddDebitCreditMessage(dispatch),
    getDebitCreditList: (businessid, fyid) => getDebitCreditList(businessid, fyid, dispatch),
    raiseDebitCreditNote: (businessid, fyid, state) => raiseDebitCreditNote(businessid, fyid, state, dispatch),
    deleteEntry: (id, businessid, fyid, segment) => deleteEntry(id, businessid, fyid, segment, dispatch),
    resetDeleteEntry: () => resetDeleteEntry(dispatch),
    getExpenseHeadList: (businessid) => getExpenseHeadList(businessid, dispatch),
    addExpenseHead: (expenseheadname, businessid) => addExpenseHead(expenseheadname, businessid, dispatch),
    refreshExpenseHead: () => refreshExpenseHead(dispatch),
    resetAddExpenseHeadMessageAndForm: () => resetAddExpenseHeadMessageAndForm(dispatch),
    resetAddExpenseHeadMessage: () => resetAddExpenseHeadMessage(dispatch),
    resetIncomeOSDetail: () => resetIncomeOSDetail(dispatch),
    resetaddIncomeOSMessage: () => resetaddIncomeOSMessage(dispatch),
    raiseIncomeOS: (businessid, fyid, state) => raiseIncomeOS(businessid, fyid, state, dispatch),
    getIncomeOSDetail: (businessid, fyid) => getIncomeOSDetail(businessid, fyid, dispatch),
    reviseSalary: (state, businessid, employeeid, getEmployee) => reviseSalary(state, businessid, employeeid, getEmployee, dispatch),
    resetReviseSalaryMessage: () => resetReviseSalaryMessage(dispatch),
    terminateEmployee: (businessid, employeeid, state) => terminateEmployee(businessid, employeeid, state, dispatch),
    resetTerminateEmployeeMessage: () => resetTerminateEmployeeMessage(dispatch),
    changePassword: (state) => changePassword(state, dispatch),
    resetChangePasswordMessage: () => resetChangePasswordMessage(dispatch),
    raiseLoan: (businessid, fyid, state, fy) => raiseLoan(businessid, fyid, state, fy, dispatch),
    resetaddLoanMessage: () => resetaddLoanMessage(dispatch),
    getLoanDetail: (businessid, fyid) => getLoanDetail(businessid, fyid, dispatch),
    refreshLoanState: () => refreshLoanState(dispatch),
    addLoanAccount: (values, businessid) => addLoanAccount(values, businessid, dispatch),
    resetAddLoanAccountMessageAndForm: () => resetAddLoanAccountMessageAndForm(dispatch),
    resetAddLoanAccountMessage: () => resetAddLoanAccountMessage(dispatch),
    getLoanAccountList: (businessid) => getLoanAccountList(businessid, dispatch),
    refreshLoanAccountState: () => refreshLoanAccountState(dispatch),
    refreshInvestmentState: () => refreshInvestmentState(dispatch),
    getInvestmentDetail: (businessid, fyid) => getInvestmentDetail(businessid, fyid, dispatch),
    resetaddInvestmentMessage: () => resetaddInvestmentMessage(dispatch),
    raiseInvestment: (businessid, fyid, state, fy) => raiseInvestment(businessid, fyid, state, fy, dispatch),
    getIncomeHeadList: (businessid) => getIncomeHeadList(businessid, dispatch),
    refreshIncomeHead: () => refreshIncomeHead(dispatch),
    resetAddIncomeHeadMessageAndForm: () => resetAddIncomeHeadMessageAndForm(dispatch),
    resetAddIncomeHeadMessage: () => resetAddIncomeHeadMessage(dispatch),
    addIncomeHead: (incomeheadname, businessid) => addIncomeHead(incomeheadname, businessid, dispatch),
    refreshAssetAccountState: () => refreshAssetAccountState(dispatch),
    getAssetAccountList: (businessid) => getAssetAccountList(businessid, dispatch),
    resetAddAssetAccountMessage: () => resetAddAssetAccountMessage(dispatch),
    resetAddAssetAccountMessageAndForm: () => resetAddAssetAccountMessageAndForm(dispatch),
    addAssetAccount: (values, businessid) => addAssetAccount(values, businessid, dispatch),
    resetaddAssetMessage: () => resetaddAssetMessage(dispatch),
    sellAsset: (businessid, fyid, state) => sellAsset(businessid, fyid, state, dispatch),
    raiseDepreciation: (businessid, fyid, state) => raiseDepreciation(businessid, fyid, state, dispatch),
    resetAddDepreciationMessage: () => resetAddDepreciationMessage(dispatch),
    getDepreciationList: (businessid, fyid) => getDepreciationList(businessid, fyid, dispatch),
    refreshDepreciationState: () => refreshDepreciationState(dispatch),
    getLoanOpeningBalanceList: (businessid, fyid) => getLoanOpeningBalanceList(businessid, fyid, dispatch),
    resetLoanOpeningBalance: () => resetLoanOpeningBalance(dispatch),
    updateLoanOpeningBalance: (state, businessid, fyid) => updateLoanOpeningBalance(state, businessid, fyid, dispatch),
    refreshUpdateLoanOpeningBalance: () => refreshUpdateLoanOpeningBalance(dispatch),
    updateAssetOpeningBalance: (state, businessid, fyid) => updateAssetOpeningBalance(state, businessid, fyid, dispatch),
    getAssetOpeningBalanceList: (businessid, fyid) => getAssetOpeningBalanceList(businessid, fyid, dispatch),
    resetAssetOpeningBalance: () => resetAssetOpeningBalance(dispatch),
    refreshUpdateAssetOpeningBalance: () => refreshUpdateAssetOpeningBalance(dispatch),
    getInvestmentAccountList: (businessid) => getInvestmentAccountList(businessid, dispatch),
    refreshInvestmentAccountState: () => refreshInvestmentAccountState(dispatch),
    resetAddInvestmentAccountMessage: () => resetAddInvestmentAccountMessage(dispatch),
    resetAddInvestmentAccountMessageAndForm: () => resetAddInvestmentAccountMessageAndForm(dispatch),
    addInvestmentAccount: (values, businessid) => addInvestmentAccount(values, businessid, dispatch),
    refreshUpdateInvestmentOpeningBalance: () => refreshUpdateInvestmentOpeningBalance(dispatch),
    updateInvestmentOpeningBalance: (state, businessid, fyid) => updateInvestmentOpeningBalance(state, businessid, fyid, dispatch),
    resetInvestmentOpeningBalance: () => resetInvestmentOpeningBalance(dispatch),
    getInvestmentOpeningBalanceList: (businessid, fyid) => getInvestmentOpeningBalanceList(businessid, fyid, dispatch),
    refreshUpdateTDSOpeningBalance: () => refreshUpdateTDSOpeningBalance(dispatch),
    updateTDSOpeningBalance: (state, businessid, fyid) => updateTDSOpeningBalance(state, businessid, fyid, dispatch),
    getTDSOpeningBalanceList: (businessid, fyid) => getTDSOpeningBalanceList(businessid, fyid, dispatch),
    resetTDSOpeningBalance: () => resetTDSOpeningBalance(dispatch),
    updateRCOpeningBalance: (state, businessid, fyid) => updateRCOpeningBalance(state, businessid, fyid, dispatch),
    refreshUpdateRCOpeningBalance: () => refreshUpdateRCOpeningBalance(dispatch),
    getRCOpeningBalanceList: (businessid, fyid) => getRCOpeningBalanceList(businessid, fyid, dispatch),
    resetRCOpeningBalance: () => resetRCOpeningBalance(dispatch),
    refreshUpdateStockOpeningBalance: () => refreshUpdateStockOpeningBalance(dispatch),
    updateStockOpeningBalance: (state, businessid, fyid) => updateStockOpeningBalance(state, businessid, fyid, dispatch),
    getStockOpeningBalanceList: (businessid, fyid) => getStockOpeningBalanceList(businessid, fyid, dispatch),
    resetStockOpeningBalance: () => resetStockOpeningBalance(dispatch),
    refreshUpdateIncomeTaxOpeningBalance: () => refreshUpdateIncomeTaxOpeningBalance(dispatch),
    getIncomeTaxOpeningBalanceList: (businessid, fyid) => getIncomeTaxOpeningBalanceList(businessid, fyid, dispatch),
    updateIncomeTaxOpeningBalance: (state, businessid, fyid) => updateIncomeTaxOpeningBalance(state, businessid, fyid, dispatch),
    resetIncomeTaxOpeningBalance: () => resetIncomeTaxOpeningBalance(dispatch),
    updateInvestmentAccount: (value, businessid, id) => updateInvestmentAccount(value, businessid, id, dispatch),
    resetUpdateInvestmentAccountMessage: () => resetUpdateInvestmentAccountMessage(dispatch),
    passStockJournal: (businessid, fyid, state) => passStockJournal(businessid, fyid, state, dispatch),
    refreshStockJournalState: () => refreshStockJournalState(dispatch),
    refreshGetStockJournal: () => refreshGetStockJournal(dispatch),
    getStockJournalList: (businessid, fyid) => getStockJournalList(businessid, fyid, dispatch),
    getEmployeeOpeningBalanceList: (businessid, fyid) => getEmployeeOpeningBalanceList(businessid, fyid, dispatch),
    refreshUpdateEmployeeOpeningBalance: () => refreshUpdateEmployeeOpeningBalance(dispatch),
    resetEmployeeOpeningBalance: () => resetEmployeeOpeningBalance(dispatch),
    updateEmployeeOpeningBalance: (state, businessid, fyid) => updateEmployeeOpeningBalance(state, businessid, fyid, dispatch)
})

class Main extends Component {
    shouldComponentUpdate(nextProps) {
        if ((this.props.addFinancialYearMessage.error !== '') && (nextProps.addFinancialYearMessage.error === '')) return false;
        else if ((this.props.addCustomerMessage.error !== '') && (nextProps.addCustomerMessage.error === '')) return false;
        else if ((this.props.addBankDetailMessage.error !== '') && (nextProps.addBankDetailMessage.error === '')) return false;
        else if ((this.props.addExpenseHeadMessage.error !== '') && (nextProps.addExpenseHeadMessage.error === '')) return false;
        else if ((this.props.addItemMessage.error !== '') && (nextProps.addItemMessage.error === '')) return false;
        else if ((this.props.addInvoiceMessage.error !== '') && (nextProps.addInvoiceMessage.error === '')) return false;
        else if ((this.props.addPurchaseMessage.error !== '') && (nextProps.addPurchaseMessage.error === '')) return false;
        else if ((this.props.addExpenseMessage.error !== '') && (nextProps.addExpenseMessage.error === '')) return false;
        else if ((this.props.addPaymentMessage.error !== '') && (nextProps.addPaymentMessage.error === '')) return false;
        else if ((this.props.addBankEntryMessage.error !== '') && (nextProps.addBankEntryMessage.error === '')) return false;
        else if ((this.props.resetLogoutMessage.error !== '') && (nextProps.resetLogoutMessage.error === '')) return false;
        else if ((this.props.updatePartyOpeningMessage.error !== '') && (nextProps.updatePartyOpeningMessage.error === '')) return false;
        else if ((this.props.addOpeningClosingMessage.error !== '') && (nextProps.addOpeningClosingMessage.error === '')) return false;
        else if ((this.props.updatePartyOpeningMessage.message === 'success') && (nextProps.updatePartyOpeningMessage.message === 'initial')) return false;
        else if ((this.props.addOpeningClosingMessage.message !== 'success') && (nextProps.addOpeningClosingMessage.message === 'initial')) return false;
        else if ((this.props.addEmployeeMessage.error !== '') && (nextProps.addEmployeeMessage.error === '')) return false;
        else if ((this.props.updateAttendanceMessage.error !== '') && (nextProps.updateAttendanceMessage.error === '')) return false;
        else if ((this.props.getAttendance.data.length > 0) && (nextProps.getAttendance.data.length === 0)) return false;
        else if ((this.props.getMonthAttendance.message === 'success') && (nextProps.getMonthAttendance.message === 'initial')) return false;
        else if ((this.props.updateSalaryMessage.error !== '') && (nextProps.updateSalaryMessage.error === '')) return false;
        else if ((this.props.getSalary.data.length > 0) && (nextProps.getSalary.data.length === 0)) return false;
        else if ((this.props.updateBankOpeningMessage.error !== '') && (nextProps.updateBankOpeningMessage.error === '')) return false;
        else if ((this.props.updateBankOpeningMessage.message === 'success') && (nextProps.updateBankOpeningMessage.message === 'initial')) return false;
        else if ((this.props.addDebitCreditMessage.error !== '') && (nextProps.addDebitCreditMessage.error === '')) return false;
        else if ((this.props.addIncomeOSMessage.error !== '') && (nextProps.addIncomeOSMessage.error === '')) return false;
        else if ((this.props.reviseSalaryMessage.error !== '') && (nextProps.reviseSalaryMessage.error === '')) return false;
        else if ((this.props.terminateEmployeeMessage.error !== '') && (nextProps.terminateEmployeeMessage.error === '')) return false;
        else if ((this.props.changePasswordMessage.error !== '') && (nextProps.changePasswordMessage.error === '')) return false;
        else if ((this.props.addLoanMessage.error !== '') && (nextProps.addLoanMessage.error === '')) return false;
        else if ((this.props.addLoanAccountMessage.error !== '') && (nextProps.addLoanAccountMessage.error === '')) return false;
        else if ((this.props.addInvestmentMessage.error !== '') && (nextProps.addInvestmentMessage.error === '')) return false;
        else if ((this.props.addAssetAccountMessage.error !== '') && (nextProps.addAssetAccountMessage.error === '')) return false;
        else if ((this.props.addAssetMessage.error !== '') && (nextProps.addAssetMessage.error === '')) return false;
        else if ((this.props.addDepreciationMessage.error !== '') && (nextProps.addDepreciationMessage.error === '')) return false;
        else if ((this.props.addDepreciationMessage.message === 'success') && (nextProps.addDepreciationMessage.message === 'initial')) return false;
        else if ((this.props.updateInvestmentOpeningMessage.error !== '') && (nextProps.updateInvestmentOpeningMessage.error === '')) return false;
        else if ((this.props.updateInvestmentOpeningMessage.message === 'success') && (nextProps.updateInvestmentOpeningMessage.message === 'initial')) return false;
        else if ((this.props.updateTDSOpeningMessage.error !== '') && (nextProps.updateTDSOpeningMessage.error === '')) return false;
        else if ((this.props.updateTDSOpeningMessage.message === 'success') && (nextProps.updateTDSOpeningMessage.message === 'initial')) return false;
        else if ((this.props.deleteEntryMessage.error !== '') && (nextProps.deleteEntryMessage.error === '')) return false;
        else if ((this.props.updateRCOpeningMessage.error !== '') && (nextProps.updateRCOpeningMessage.error === '')) return false;
        else if ((this.props.updateStockOpeningMessage.error !== '') && (nextProps.updateStockOpeningMessage.error === '')) return false;
        else if ((this.props.updateStockOpeningMessage.message === 'success') && (nextProps.updateStockOpeningMessage.message === 'initial')) return false;
        else if ((this.props.updateGSTOpeningBalanceMessage.error !== '') && (nextProps.updateGSTOpeningBalanceMessage.message === '')) return false;
        else if ((this.props.updateAssetOpeningMessage.error !== '') && (nextProps.updateAssetOpeningMessage.error === '')) return false;
        else if ((this.props.updateAssetOpeningMessage.message === 'success') && (nextProps.updateAssetOpeningMessage.message === 'initial')) return false;
        else if ((this.props.addAssetAccountMessage.error !== '') && (nextProps.addAssetAccountMessage.error === '')) return false;
        else if ((this.props.addBusinessMessage.error !== '') && (nextProps.addBusinessMessage.error === '')) return false;
        else if ((this.props.addBusinessMessage.message === 'Already Registered') && (nextProps.addBusinessMessage.message === 'initial')) return false;
        else if ((this.props.addInvestmentAccountMessage.error !== '') && (nextProps.addInvestmentAccountMessage.error === '')) return false;
        else if ((this.props.updateInvestmentAccountMessage.error !== '') && (nextProps.updateInvestmentAccountMessage.error === '')) return false;
        else if ((this.props.updateIncomeTaxOpeningMessage.message === 'success') && (nextProps.updateIncomeTaxOpeningMessage.message === 'initial')) return false;
        else if ((this.props.updateIncomeTaxOpeningMessage.error !== '') && (nextProps.updateIncomeTaxOpeningMessage.error === '')) return false;
        else if ((this.props.passStockJournalMessage.error !== '') && (nextProps.passStockJournalMessage.error === '')) return false;
        else return true;
    }
    render() {
        const BusinessChangeId = ({ match }) => {
            return (
                <BusinessChange businessList={this.props.businessList} businessid={match.params.businessid} updateBusiness={this.props.updateBusiness} setDefaultBusinessChange={this.props.setDefaultBusinessChange} resetBusinessChangeMessage={this.props.resetBusinessChangeMessage} updateBusinessMessage={this.props.updateBusinessMessage} logoutMessage={this.props.logoutMessage} resetLogoutMessage={this.props.resetLogoutMessage} logout={this.props.logout} />
            );
        }
        const BusinessDetailId = ({ match }) => {
            return <BusinessDetail businessid={match.params.businessid} businessList={this.props.businessList} resetLoginForm={this.props.resetLoginForm} getBusinessList={this.props.getBusinessList} resetAddSubUserMessage={this.props.resetAddSubUserMessage} addSubUserMessage={this.props.addSubUserMessage} addFinancialYear={this.props.addFinancialYear} addFinancialYearMessage={this.props.addFinancialYearMessage} resetaddFinancialYearMessage={this.props.resetaddFinancialYearMessage} resetBusinessChange={this.props.resetBusinessChange} updateBusinessMessage={this.props.updateBusinessMessage} updateCustomerMessage={this.props.updateCustomerMessage} resetCustomerChange={this.props.resetCustomerChange} resetBusinessChange={this.props.resetBusinessChange} getSalesData={this.props.getSalesData} salesDetail={this.props.salesDetail} getCustomer={this.props.getCustomer} getCustomerList={this.props.getCustomerList} getItem={this.props.getItem} getItemList={this.props.getItemList} getBankDetail={this.props.getBankDetail} getBankDetailList={this.props.getBankDetailList} getVendor={this.props.getVendor} getVendorList={this.props.getVendorList} resetCVIB={this.props.resetCVIB} resetaddFinancialYearMessage={this.props.resetaddFinancialYearMessage} resetaddFinancialYear={this.props.resetaddFinancialYear} individualBusinessMessage={this.props.individualBusinessMessage} getIndividualBusiness={this.props.getIndividualBusiness} consumeIndividualBusinessMessage={this.props.consumeIndividualBusinessMessage} updateBusinessMessage={this.props.updateBusinessMessage} resetBusinessChangeMessage={this.props.resetBusinessChangeMessage} resetSalesDetail={this.props.resetSalesDetail} resetIndividualBusinessMessage={this.props.resetIndividualBusinessMessage} purchaseDetail={this.props.purchaseDetail} resetPurchaseDetail={this.props.resetPurchaseDetail} getExpense={this.props.getExpense} resetExpenseDetail={this.props.resetExpenseDetail} getPayment={this.props.getPayment} resetPaymentDetail={this.props.resetPaymentDetail} getBankEntry={this.props.getBankEntry} resetBankEntryDetail={this.props.resetBankEntryDetail} getOpeningClosing={this.props.getOpeningClosing} resetOpeningClosingDetail={this.props.resetOpeningClosingDetail} resetOpeningBalance={this.props.resetOpeningBalance} getOpeningBalance={this.props.getOpeningBalance} logoutMessage={this.props.logoutMessage} resetLogoutMessage={this.props.resetLogoutMessage} logout={this.props.logout} getCashInHand={this.props.getCashInHand} resetCashInHand={this.props.resetCashInHand} getEmployee={this.props.getEmployee} getEmployeeList={this.props.getEmployeeList} refreshDebitCredit={this.props.refreshDebitCredit} getDebitCredit={this.props.getDebitCredit} getExpenseHead={this.props.getExpenseHead} getExpenseHeadList={this.props.getExpenseHeadList} resetIncomeOSDetail={this.props.resetIncomeOSDetail} getIncomeOS={this.props.getIncomeOS} getBankOpeningBalance={this.props.getBankOpeningBalance} resetBankOpeningBalance={this.props.resetBankOpeningBalance} getLoanAccount={this.props.getLoanAccount} getLoanAccountList={this.props.getLoanAccountList} getLoan={this.props.getLoan} refreshLoanState={this.props.refreshLoanState} getInvestment={this.props.getInvestment} refreshInvestmentState={this.props.refreshInvestmentState} getIncomeHead={this.props.getIncomeHead} getIncomeHeadList={this.props.getIncomeHeadList} getAssetAccount={this.props.getAssetAccount} getAssetAccountList={this.props.getAssetAccountList} getLoanOpeningBalance={this.props.getLoanOpeningBalance} resetLoanOpeningBalance={this.props.resetLoanOpeningBalance} getDepreciation={this.props.getDepreciation} refreshDepreciationState={this.props.refreshDepreciationState} getAssetOpeningBalance={this.props.getAssetOpeningBalance} resetAssetOpeningBalance={this.props.resetAssetOpeningBalance} getInvestmentAccount={this.props.getInvestmentAccount} getInvestmentAccountList={this.props.getInvestmentAccountList} getTDSOpeningBalance={this.props.getTDSOpeningBalance} resetTDSOpeningBalance={this.props.resetTDSOpeningBalance} getInvestmentOpeningBalance={this.props.getInvestmentOpeningBalance} resetInvestmentOpeningBalance={this.props.resetInvestmentOpeningBalance} getIncomeTaxOpeningBalance={this.props.getIncomeTaxOpeningBalance} resetIncomeTaxOpeningBalance={this.props.resetIncomeTaxOpeningBalance} getEmployeeOpeningBalance={this.props.getEmployeeOpeningBalance} resetEmployeeOpeningBalance={this.props.resetEmployeeOpeningBalance}/>
        }
        const BusinessProcessId = ({ match }) => {
            return <BusinessProcess getSalesData={this.props.getSalesData} salesDetail={this.props.salesDetail} businessid={match.params.businessid} fyid={match.params.fyid} getCustomer={this.props.getCustomer} getCustomerList={this.props.getCustomerList} getItem={this.props.getItem} getItemList={this.props.getItemList} getBankDetail={this.props.getBankDetail} getBankDetailList={this.props.getBankDetailList} purchaseDetail={this.props.purchaseDetail} getPurchaseData={this.props.getPurchaseData} getExpenseDetail={this.props.getExpenseDetail} getExpense={this.props.getExpense} getBankEntryDetail={this.props.getBankEntryDetail} getBankEntry={this.props.getBankEntry} resetBankEntryDetail={this.props.resetBankEntryDetail} getBankEntry={this.props.getBankEntry} getBankEntryDetail={this.props.getBankEntryDetail} getOpeningClosing={this.props.getOpeningClosing} getOpeningClosingDetail={this.props.getOpeningClosingDetail} getOpeningBalance={this.props.getOpeningBalance} getPartyOpeningBalance={this.props.getPartyOpeningBalance} logoutMessage={this.props.logoutMessage} resetLogoutMessage={this.props.resetLogoutMessage} logout={this.props.logout} getCashInHandDetail={this.props.getCashInHandDetail} getCashInHand={this.props.getCashInHand} businessList={this.props.businessList} getIncomeOS={this.props.getIncomeOS} getIncomeOSDetail={this.props.getIncomeOSDetail} getBankOpeningBalanceList={this.props.getBankOpeningBalanceList} getBankOpeningBalance={this.props.getBankOpeningBalance} getEmployeeOpeningBalance={this.props.getEmployeeOpeningBalance} getEmployeeOpeningBalanceList={this.props.getEmployeeOpeningBalanceList} getEmployee={this.props.getEmployee} getEmployeeList={this.props.getEmployeeList} />;
        }
        const CustomerAdd = ({ match }) => {
            return <Customer businessid={match.params.businessid} addCustomerMessage={this.props.addCustomerMessage} addCustomer={this.props.addCustomer} getCustomer={this.props.getCustomer} getCustomerList={this.props.getCustomerList} updateCustomerMessage={this.props.updateCustomerMessage} resetCustomerChange={this.props.resetCustomerChange} refreshCustomerStateSecond={this.props.refreshCustomerStateSecond} resetAddCustomerMessageAndForm={this.props.resetAddCustomerMessageAndForm} resetAddCustomerMessage={this.props.resetAddCustomerMessage} resetCustomerChange={this.props.resetCustomerChange} updateCustomerMessage={this.props.updateCustomerMessage} refreshCustomerState={this.props.refreshCustomerState} logoutMessage={this.props.logoutMessage} resetLogoutMessage={this.props.resetLogoutMessage} logout={this.props.logout} businessList={this.props.businessList} />;
        }
        const VendorAdd = ({ match }) => {
            return <Vendor businessid={match.params.businessid} addVendorMessage={this.props.addVendorMessage} addVendor={this.props.addVendor} getVendor={this.props.getVendor} getVendorList={this.props.getVendorList} updateVendorMessage={this.props.updateVendorMessage} resetVendorChange={this.props.resetVendorChange} resetAddVendorMessage={this.props.resetAddVendorMessage} resetAddVendorMessageAndForm={this.props.resetAddVendorMessageAndForm} refreshVendorStateSecond={this.props.refreshVendorStateSecond} resetVendorChange={this.props.resetVendorChange} updateVendorMessage={this.props.updateVendorMessage} refreshVendorState={this.props.refreshVendorState} logoutMessage={this.props.logoutMessage} resetLogoutMessage={this.props.resetLogoutMessage} logout={this.props.logout} businessList={this.props.businessList} />;
        }
        const BankDetailAdd = ({ match }) => {
            return <BankDetail businessid={match.params.businessid} addBankDetailMessage={this.props.addBankDetailMessage} addBankDetail={this.props.addBankDetail} getBankDetail={this.props.getBankDetail} getBankDetailList={this.props.getBankDetailList} updateBankDetailMessage={this.props.updateBankDetailMessage} resetBankDetailChange={this.props.resetBankDetailChange} refreshBankDetailStateSecond={this.props.refreshBankDetailStateSecond} resetAddBankDetailMessage={this.props.resetAddBankDetailMessage} resetAddBankMessageAndForm={this.props.resetAddBankMessageAndForm} resetBankDetailChange={this.props.resetBankDetailChange} updateBankDetailMessage={this.props.updateBankDetailMessage} refreshBankState={this.props.refreshBankState} logoutMessage={this.props.logoutMessage} resetLogoutMessage={this.props.resetLogoutMessage} logout={this.props.logout} businessList={this.props.businessList} />;
        }
        const ItemAdd = ({ match }) => {
            return <Item businessid={match.params.businessid} addItemMessage={this.props.addItemMessage} addItem={this.props.addItem} getItem={this.props.getItem} getItemList={this.props.getItemList} updateItemMessage={this.props.updateItemMessage} resetItemChange={this.props.resetItemChange} refreshItemStateSecond={this.props.refreshItemStateSecond} resetAddItemMessage={this.props.resetAddItemMessage} resetAddItemMessageAndForm={this.props.resetAddItemMessageAndForm} updateItemMessage={this.props.updateItemMessage} refreshItemState={this.props.refreshItemState} logoutMessage={this.props.logoutMessage} resetLogoutMessage={this.props.resetLogoutMessage} logout={this.props.logout} businessList={this.props.businessList} />;
        }
        const ExpenseHeadAdd = ({ match }) => {
            return <ExpenseHead businessid={match.params.businessid} addExpenseHeadMessage={this.props.addExpenseHeadMessage} getExpenseHead={this.props.getExpenseHead} refreshExpenseHead={this.props.refreshExpenseHead} getExpenseHeadList={this.props.getExpenseHeadList} resetAddExpenseHeadMessageAndForm={this.props.resetAddExpenseHeadMessageAndForm} resetAddExpenseHeadMessage={this.props.resetAddExpenseHeadMessage} addExpenseHead={this.props.addExpenseHead} logoutMessage={this.props.logoutMessage} resetLogoutMessage={this.props.resetLogoutMessage} logout={this.props.logout} businessList={this.props.businessList} deleteEntryMessage={this.props.deleteEntryMessage} deleteEntry={this.props.deleteEntry} resetDeleteEntry={this.props.resetDeleteEntry} />;
        }
        const IncomeHeadAdd = ({ match }) => {
            return <IncomeHead businessid={match.params.businessid} addIncomeHeadMessage={this.props.addIncomeHeadMessage} getIncomeHead={this.props.getIncomeHead} refreshIncomeHead={this.props.refreshIncomeHead} getIncomeHeadList={this.props.getIncomeHeadList} resetAddIncomeHeadMessageAndForm={this.props.resetAddIncomeHeadMessageAndForm} resetAddIncomeHeadMessage={this.props.resetAddIncomeHeadMessage} addIncomeHead={this.props.addIncomeHead} logoutMessage={this.props.logoutMessage} resetLogoutMessage={this.props.resetLogoutMessage} logout={this.props.logout} businessList={this.props.businessList} deleteEntryMessage={this.props.deleteEntryMessage} deleteEntry={this.props.deleteEntry} resetDeleteEntry={this.props.resetDeleteEntry} />;
        }
        const InvoiceAdd = ({ match }) => {
            return <RaiseInvoice businessid={match.params.businessid} fyid={match.params.fyid} businessList={this.props.businessList} getBusinessList={this.props.getBusinessList} getCustomer={this.props.getCustomer} getCustomerList={this.props.getCustomerList} getItem={this.props.getItem} getItemList={this.props.getItemList} getBankDetail={this.props.getBankDetail} getBankDetailList={this.props.getBankDetailList} raiseInvoice={this.props.raiseInvoice} addInvoiceMessage={this.props.addInvoiceMessage} getSalesData={this.props.getSalesData} salesDetail={this.props.salesDetail} refreshCustomerState={this.props.refreshCustomerState} refreshItemState={this.props.refreshItemState} resetaddInvoiceMessage={this.props.resetaddInvoiceMessage} resetSalesDetail={this.props.resetSalesDetail} logoutMessage={this.props.logoutMessage} resetLogoutMessage={this.props.resetLogoutMessage} logout={this.props.logout} getPaymentDetail={this.props.getPaymentDetail} addAssetMessage={this.props.addAssetMessage} sellAsset={this.props.sellAsset} resetaddAssetMessage={this.props.resetaddAssetMessage} refreshAssetAccountState={this.props.refreshAssetAccountState} getAssetAccount={this.props.getAssetAccount} getAssetAccountList={this.props.getAssetAccountList} />;
        }
        const CustomerUpdate = ({ match }) => {
            return <CustomerChange businessid={match.params.businessid} customerid={match.params.customerid} getCustomer={this.props.getCustomer} updateCustomer={this.props.updateCustomer} updateCustomerMessage={this.props.updateCustomerMessage} resetCustomerChange={this.props.resetCustomerChange} setDefaultCustomerChange={this.props.setDefaultCustomerChange} getCustomerList={this.props.getCustomerList} refreshCustomerStateSecond={this.props.refreshCustomerStateSecond} logoutMessage={this.props.logoutMessage} resetLogoutMessage={this.props.resetLogoutMessage} logout={this.props.logout} businessList={this.props.businessList} />
        }
        const VendorUpdate = ({ match }) => {
            return <VendorChange businessid={match.params.businessid} vendorid={match.params.vendorid} getVendor={this.props.getVendor} updateVendor={this.props.updateVendor} updateVendorMessage={this.props.updateVendorMessage} resetVendorChange={this.props.resetVendorChange} setDefaultVendorChange={this.props.setDefaultVendorChange} getVendorList={this.props.getVendorList} refreshVendorStateSecond={this.props.refreshVendorStateSecond} logoutMessage={this.props.logoutMessage} resetLogoutMessage={this.props.resetLogoutMessage} logout={this.props.logout} businessList={this.props.businessList} />
        }
        const BankDetailUpdate = ({ match }) => {
            return <BankDetailChange businessid={match.params.businessid} bankdetailid={match.params.bankdetailid} getBankDetail={this.props.getBankDetail} getBankDetailList={this.props.getBankDetailList} updateBankDetail={this.props.updateBankDetail} updateBankDetailMessage={this.props.updateBankDetailMessage} resetBankDetailChange={this.props.resetBankDetailChange} setDefaultBankDetailChange={this.props.setDefaultBankDetailChange} refreshBankDetailStateSecond={this.props.refreshBankDetailStateSecond} logoutMessage={this.props.logoutMessage} resetLogoutMessage={this.props.resetLogoutMessage} logout={this.props.logout} businessList={this.props.businessList} />
        }
        const ItemUpdate = ({ match }) => {
            return <ItemChange businessid={match.params.businessid} itemid={match.params.itemid} getItem={this.props.getItem} getItemList={this.props.getItemList} updateItem={this.props.updateItem} updateItemMessage={this.props.updateItemMessage} resetItemChange={this.props.resetItemChange} setDefaultItemChange={this.props.setDefaultItemChange} refreshItemStateSecond={this.props.refreshItemStateSecond} logoutMessage={this.props.logoutMessage} resetLogoutMessage={this.props.resetLogoutMessage} logout={this.props.logout} businessList={this.props.businessList} />
        }
        const AddPurchase = ({ match }) => {
            return <Purchase businessid={match.params.businessid} fyid={match.params.fyid} addPurchaseItem={this.props.addPurchaseItem} addPurchaseMessage={this.props.addPurchaseMessage} getVendor={this.props.getVendor} getVendorList={this.props.getVendorList} getItem={this.props.getItem} getItemList={this.props.getItemList} refreshVendorState={this.props.refreshVendorState} refreshItemState={this.props.refreshItemState} businessList={this.props.businessList} raisePurchase={this.props.raisePurchase} getPurchaseData={this.props.getPurchaseData} purchaseDetail={this.props.purchaseDetail} resetPurchaseDetail={this.props.resetPurchaseDetail} resetaddPurchaseMessage={this.props.resetaddPurchaseMessage} logoutMessage={this.props.logoutMessage} resetLogoutMessage={this.props.resetLogoutMessage} logout={this.props.logout} getAssetAccountList={this.props.getAssetAccountList} getAssetAccount={this.props.getAssetAccount} refreshAssetAccountState={this.props.refreshAssetAccountState} addAssetMessage={this.props.addAssetMessage} raiseAsset={this.props.raiseAsset} resetaddAssetMessage={this.props.resetaddAssetMessage} getBankDetail={this.props.getBankDetail} getBankDetailList={this.props.getBankDetailList} refreshBankState={this.props.refreshBankState} />
        }
        const SalesDetailDisplay = ({ match }) => {
            return <SalesDetail businessid={match.params.businessid} fyid={match.params.fyid} businessList={this.props.businessList} getBusinessList={this.props.getBusinessList} getCustomer={this.props.getCustomer} getCustomerList={this.props.getCustomerList} getItem={this.props.getItem} getItemList={this.props.getItemList} getSalesData={this.props.getSalesData} salesDetail={this.props.salesDetail} refreshCustomerState={this.props.refreshCustomerState} refreshItemState={this.props.refreshItemState} resetSalesDetailSecond={this.props.resetSalesDetailSecond} refreshCustomerStateSecond={this.props.refreshCustomerStateSecond} refreshItemStateSecond={this.props.refreshItemStateSecond} cancelInvoice={this.props.cancelInvoice} cancelInvoiceMessage={this.props.cancelInvoiceMessage} resetCancelInvoiceMessage={this.props.resetCancelInvoiceMessage} restoreInvoice={this.props.restoreInvoice} restoreInvoiceMessage={this.props.restoreInvoiceMessage} resetRestoreInvoiceMessage={this.props.resetRestoreInvoiceMessage} resetSalesDetail={this.props.resetSalesDetail} logoutMessage={this.props.logoutMessage} resetLogoutMessage={this.props.resetLogoutMessage} logout={this.props.logout} refreshDebitCredit={this.props.refreshDebitCredit} getDebitCredit={this.props.getDebitCredit} getDebitCreditList={this.props.getDebitCreditList} deleteEntryMessage={this.props.deleteEntryMessage} deleteEntry={this.props.deleteEntry} resetDeleteEntry={this.props.resetDeleteEntry} />
        }
        const InvoiceUpdateMatch = ({ match }) => {
            return <InvoiceUpdate businessid={match.params.businessid} fyid={match.params.fyid} invoiceid={match.params.invoiceid} salesDetail={this.props.salesDetail} invoiceUpdate={this.props.invoiceUpdate} invoiceUpdateMessage={this.props.invoiceUpdateMessage} getSalesData={this.props.getSalesData} resetSalesDetailSecond={this.props.resetSalesDetailSecond} getCustomer={this.props.getCustomer} getItem={this.props.getItem} getItemList={this.props.getItemList} getVendorList={this.props.getVendorList} refreshCustomerStateSecond={this.props.refreshCustomerStateSecond} getItem={this.props.getItem} refreshItemStateSecond={this.props.refreshItemStateSecond} businessList={this.props.businessList} resetInvoiceUpdateMessage={this.props.resetInvoiceUpdateMessage} businessList={this.props.businessList} logoutMessage={this.props.logoutMessage} resetLogoutMessage={this.props.resetLogoutMessage} logout={this.props.logout} />
        }
        const PurchaseUpdateMatch = ({ match }) => {
            return <PurchaseUpdate businessid={match.params.businessid} fyid={match.params.fyid} purchaseid={match.params.purchaseid} purchaseDetail={this.props.purchaseDetail} purchaseUpdate={this.props.purchaseUpdate} purchaseUpdateMessage={this.props.purchaseUpdateMessage} getPurchaseData={this.props.getPurchaseData} resetPurchaseDetailSecond={this.props.resetPurchaseDetailSecond} getVendor={this.props.getVendor} getItem={this.props.getItem} getItemList={this.props.getItemList} getVendorList={this.props.getVendorList} refreshVendorStateSecond={this.props.refreshVendorStateSecond} refreshItemStateSecond={this.props.refreshItemStateSecond} businessList={this.props.businessList} resetPurchaseUpdateMessage={this.props.resetPurchaseUpdateMessage} businessList={this.props.businessList} logoutMessage={this.props.logoutMessage} resetLogoutMessage={this.props.resetLogoutMessage} logout={this.props.logout} />
        }
        const PurchaseDetailDisplay = ({ match }) => {
            return <PurchaseDetail businessid={match.params.businessid} fyid={match.params.fyid} businessList={this.props.businessList} getBusinessList={this.props.getBusinessList} getVendor={this.props.getVendor} getVendorList={this.props.getVendorList} getItem={this.props.getItem} getItemList={this.props.getItemList} getPurchaseData={this.props.getPurchaseData} purchaseDetail={this.props.purchaseDetail} refreshPurchaseState={this.props.refreshPurchaseState} refreshItemState={this.props.refreshItemState} resetPurchaseDetailSecond={this.props.resetPurchaseDetailSecond} refreshVendorState={this.props.refreshVendorState} refreshVendorStateSecond={this.props.refreshVendorStateSecond} refreshItemStateSecond={this.props.refreshItemStateSecond} cancelInvoice={this.props.cancelInvoice} cancelInvoiceMessage={this.props.cancelInvoiceMessage} resetCancelInvoiceMessage={this.props.resetCancelInvoiceMessage} deletePurchaseMessage={this.props.deletePurchaseMessage} resetDeletePurchaseMessage={this.props.resetDeletePurchaseMessage} logoutMessage={this.props.logoutMessage} resetLogoutMessage={this.props.resetLogoutMessage} logout={this.props.logout} refreshDebitCredit={this.props.refreshDebitCredit} getDebitCredit={this.props.getDebitCredit} getDebitCreditList={this.props.getDebitCreditList} resetPurchaseDetail={this.props.resetPurchaseDetail} deleteEntry={this.props.deleteEntry} resetDeleteEntry={this.props.resetDeleteEntry} deleteEntryMessage={this.props.deleteEntryMessage} />
        }
        const ExpenseDetailDisplay = ({ match }) => {
            return <ExpenseDetail businessid={match.params.businessid} fyid={match.params.fyid} businessList={this.props.businessList} getBusinessList={this.props.getBusinessList} getVendor={this.props.getVendor} getVendorList={this.props.getVendorList} refreshVendorState={this.props.refreshVendorState} refreshVendorStateSecond={this.props.refreshVendorStateSecond} logoutMessage={this.props.logoutMessage} resetLogoutMessage={this.props.resetLogoutMessage} logout={this.props.logout} getExpenseDetail={this.props.getExpenseDetail} getExpense={this.props.getExpense} resetExpenseDetail={this.props.resetExpenseDetail} deleteExpenseMessage={this.props.deleteExpenseMessage} resetDeleteExpenseMessage={this.props.resetDeleteExpenseMessage} cancelExpense={this.props.cancelExpense} deleteEntry={this.props.deleteEntry} resetDeleteEntry={this.props.resetDeleteEntry} deleteEntryMessage={this.props.deleteEntryMessage} getIncomeOS={this.props.getIncomeOS} getIncomeOSDetail={this.props.getIncomeOSDetail} resetIncomeOSDetail={this.props.resetIncomeOSDetail} getExpenseHead={this.props.getExpenseHead} getExpenseHeadList={this.props.getExpenseHeadList} refreshExpenseHead={this.props.refreshExpenseHead} />
        }
        const IncomeDetailDisplay = ({ match }) => {
            return <IncomeDetail businessid={match.params.businessid} fyid={match.params.fyid} businessList={this.props.businessList} getBusinessList={this.props.getBusinessList} getCustomer={this.props.getCustomer} getCustomerList={this.props.getCustomerList} refreshCustomerState={this.props.refreshCustomerState} logoutMessage={this.props.logoutMessage} resetLogoutMessage={this.props.resetLogoutMessage} logout={this.props.logout} deleteEntry={this.props.deleteEntry} resetDeleteEntry={this.props.resetDeleteEntry} deleteEntryMessage={this.props.deleteEntryMessage} getIncomeOS={this.props.getIncomeOS} getIncomeOSDetail={this.props.getIncomeOSDetail} resetIncomeOSDetail={this.props.resetIncomeOSDetail} getIncomeHead={this.props.getIncomeHead} getIncomeHeadList={this.props.getIncomeHeadList} refreshIncomeHead={this.props.refreshIncomeHead} />
        }
        const AddExpense = ({ match }) => {
            return <Expense businessid={match.params.businessid} fyid={match.params.fyid} addExpenseMessage={this.props.addExpenseMessage} raiseExpense={this.props.raiseExpense} resetaddExpenseMessage={this.props.resetaddExpenseMessage} getExpenseDetail={this.props.getExpenseDetail} getExpense={this.props.getExpense} resetExpenseDetail={this.props.resetExpenseDetail} logoutMessage={this.props.logoutMessage} resetLogoutMessage={this.props.resetLogoutMessage} logout={this.props.logout} businessList={this.props.businessList} getVendor={this.props.getVendor} getVendorList={this.props.getVendorList} refreshVendorState={this.props.refreshVendorState} getBankDetail={this.props.getBankDetail} getBankDetailList={this.props.getBankDetailList} refreshExpenseHead={this.props.refreshExpenseHead} getExpenseHead={this.props.getExpenseHead} getExpenseHeadList={this.props.getExpenseHeadList} refreshBankState={this.props.refreshBankState} getInvestmentAccount={this.props.getInvestmentAccount} getInvestmentAccountList={this.props.getInvestmentAccountList} refreshInvestmentAccountState={this.props.refreshInvestmentAccountState} getEmployee={this.props.getEmployee} getEmployeeList={this.props.getEmployeeList} refreshEmployeeState={this.props.refreshEmployeeState} />
        }
        const AddIncomeOS = ({ match }) => {
            return <IncomeOS businessid={match.params.businessid} fyid={match.params.fyid} addIncomeOSMessage={this.props.addIncomeOSMessage} raiseIncomeOS={this.props.raiseIncomeOS} resetaddIncomeOSMessage={this.props.resetaddIncomeOSMessage} getIncomeOSDetail={this.props.getIncomeOSDetail} getIncomeOS={this.props.getIncomeOS} resetIncomeOSDetail={this.props.resetIncomeOSDetail} logoutMessage={this.props.logoutMessage} resetLogoutMessage={this.props.resetLogoutMessage} logout={this.props.logout} businessList={this.props.businessList} getBankDetail={this.props.getBankDetail} getBankDetailList={this.props.getBankDetailList} refreshBankState={this.props.refreshBankState} getIncomeHead={this.props.getIncomeHead} getIncomeHeadList={this.props.getIncomeHeadList} refreshIncomeHead={this.props.refreshIncomeHead} getCustomer={this.props.getCustomer} getCustomerList={this.props.getCustomerList} refreshCustomerState={this.props.refreshCustomerState} getInvestmentAccount={this.props.getInvestmentAccount} getInvestmentAccountList={this.props.getInvestmentAccountList} refreshInvestmentAccountState={this.props.refreshInvestmentAccountState} />
        }
        const AddPayment = ({ match }) => {
            return <Payment businessid={match.params.businessid} fyid={match.params.fyid} addPaymentMessage={this.props.addPaymentMessage} raisePayment={this.props.raisePayment} resetaddPaymentMessage={this.props.resetaddPaymentMessage} getPaymentDetail={this.props.getPaymentDetail} getPayment={this.props.getPayment} resetPaymentDetail={this.props.resetPaymentDetail} getCustomer={this.props.getCustomer} getVendor={this.props.getVendor} refreshVendorState={this.props.refreshVendorState} refreshCustomerState={this.props.refreshCustomerState} getVendorList={this.props.getVendorList} refreshVendorState={this.props.refreshVendorState} getCustomerList={this.props.getCustomerList} refreshCustomerState={this.props.refreshCustomerState} logoutMessage={this.props.logoutMessage} resetLogoutMessage={this.props.resetLogoutMessage} logout={this.props.logout} businessList={this.props.businessList} getBankDetail={this.props.getBankDetail} getBankDetailList={this.props.getBankDetailList} refreshBankState={this.props.refreshBankState} getExpenseHead={this.props.getExpenseHead} getExpenseHeadList={this.props.getExpenseHeadList} refreshExpenseHead={this.props.refreshExpenseHead} refreshInvestmentAccountState={this.props.refreshInvestmentAccountState} getInvestmentAccount={this.props.getInvestmentAccount} getInvestmentAccountList={this.props.getInvestmentAccountList} getEmployee={this.props.getEmployee} getEmployeeList={this.props.getEmployeeList} refreshEmployeeState={this.props.refreshEmployeeState} />
        }
        const AddBankEntry = ({ match }) => {
            return <BankEntry businessid={match.params.businessid} fyid={match.params.fyid} addBankEntryMessage={this.props.addBankEntryMessage} raiseBankEntry={this.props.raiseBankEntry} resetaddBankEntryMessage={this.props.resetaddBankEntryMessage} getBankEntryDetail={this.props.getBankEntryDetail} getBankEntry={this.props.getBankEntry} resetBankEntryDetailSecond={this.props.resetBankEntryDetailSecond} getBankDetail={this.props.getBankDetail} getBankDetailList={this.props.getBankDetailList} refreshBankState={this.props.refreshBankState} resetBankEntryDetail={this.props.resetBankEntryDetail} refreshBankDetailStateSecond={this.props.refreshBankDetailStateSecond} logoutMessage={this.props.logoutMessage} resetLogoutMessage={this.props.resetLogoutMessage} logout={this.props.logout} businessList={this.props.businessList} />
        }
        const AddOpeningClosingStock = ({ match }) => {
            return (<OpeningClosing businessid={match.params.businessid} fyid={match.params.fyid} businessList={this.props.businessList} addOpeningClosingMessage={this.props.addOpeningClosingMessage} getOpeningClosing={this.props.getOpeningClosing} getOpeningClosingDetail={this.props.getOpeningClosingDetail} raiseOpeningClosing={this.props.raiseOpeningClosing} resetaddOpeningClosingMessage={this.props.resetaddOpeningClosingMessage} resetOpeningClosingDetail={this.props.resetOpeningClosingDetail} logoutMessage={this.props.logoutMessage} resetLogoutMessage={this.props.resetLogoutMessage} logout={this.props.logout} businessList={this.props.businessList} />);
        }
        const PnL = ({ match }) => {
            return (<ProfitLoss businessid={match.params.businessid} fyid={match.params.fyid} businessList={this.props.businessList} getExpense={this.props.getExpense} getOpeningClosing={this.props.getOpeningClosing} getExpenseDetail={this.props.getExpenseDetail} getSalesData={this.props.getSalesData} getPurchaseData={this.props.getPurchaseData} getOpeningClosingDetail={this.props.getOpeningClosingDetail} logoutMessage={this.props.logoutMessage} resetLogoutMessage={this.props.resetLogoutMessage} logout={this.props.logout} getSales={this.props.salesDetail} getPurchase={this.props.purchaseDetail} logoutMessage={this.props.logoutMessage} resetLogoutMessage={this.props.resetLogoutMessage} logout={this.props.logout} getExpenseHead={this.props.getExpenseHead} resetIncomeOSDetail={this.props.resetIncomeOSDetail} getIncomeOS={this.props.getIncomeOS} getIncomeOSDetail={this.props.getIncomeOSDetail} refreshDebitCredit={this.props.refreshDebitCredit} getDebitCredit={this.props.getDebitCredit} getDebitCreditList={this.props.getDebitCreditList} getIncomeHead={this.props.getIncomeHead} getIncomeHeadList={this.props.getIncomeHeadList} refreshIncomeHead={this.props.refreshIncomeHead} getInvestmentDetail={this.props.getInvestmentDetail} getInvestment={this.props.getInvestment} refreshAssetAccountState={this.props.refreshAssetAccountState} getAssetAccount={this.props.getAssetAccount} getAssetAccountList={this.props.getAssetAccountList} getAssetOpeningBalance={this.props.getAssetOpeningBalance} getAssetOpeningBalanceList={this.props.getAssetOpeningBalanceList} resetAssetOpeningBalance={this.props.resetAssetOpeningBalance} getDepreciation={this.props.getDepreciation} getDepreciationList={this.props.getDepreciationList} refreshDepreciationState={this.props.refreshDepreciationState} />);
        }
        const ConfigureOpeningBalance = ({ match }) => {
            return <OpeningBalance businessid={match.params.businessid} fyid={match.params.fyid} salesDetail={this.props.salesDetail} purchaseDetail={this.props.purchaseDetail} getPayment={this.props.getPayment} getVendorList={this.props.getVendorList} getCustomerList={this.props.getCustomerList} getSalesData={this.props.getSalesData} resetPaymentDetailSecond={this.props.resetPaymentDetailSecond} refreshVendorStateSecond={this.props.refreshVendorStateSecond} refreshCustomerStateSecond={this.props.refreshCustomerStateSecond} resetSalesDetailSecond={this.props.resetSalesDetailSecond} getCustomer={this.props.getCustomer} getVendor={this.props.getVendor} getPaymentDetail={this.props.getPaymentDetail} refreshVendorState={this.props.refreshVendorState} refreshCustomerState={this.props.refreshCustomerState} getPurchaseData={this.props.getPurchaseData} resetPurchaseDetailSecond={this.props.resetPurchaseDetailSecond} getBusinessList={this.props.getBusinessList} businessList={this.props.businessList} updatePartyOpeningBalance={this.props.updatePartyOpeningBalance} getPartyOpeningBalance={this.props.getPartyOpeningBalance} getOpeningBalance={this.props.getOpeningBalance} updatePartyOpeningMessage={this.props.updatePartyOpeningMessage} resetOpeningBalance={this.props.resetOpeningBalance} logoutMessage={this.props.logoutMessage} resetLogoutMessage={this.props.resetLogoutMessage} logout={this.props.logout} refreshUpdatepartyOpeningBalance={this.props.refreshUpdatepartyOpeningBalance} />
        }
        const ConfigureBankOpeningBalance = ({ match }) => {
            return <BankOpeningBalance businessid={match.params.businessid} fyid={match.params.fyid} businessList={this.props.businessList} updateBankOpeningBalance={this.props.updateBankOpeningBalance} getBankOpeningBalance={this.props.getBankOpeningBalance} getBankOpeningBalanceList={this.props.getBankOpeningBalanceList} updateBankOpeningMessage={this.props.updateBankOpeningMessage} resetBankOpeningBalance={this.props.resetBankOpeningBalance} logoutMessage={this.props.logoutMessage} resetLogoutMessage={this.props.resetLogoutMessage} logout={this.props.logout} refreshUpdateBankOpeningBalance={this.props.refreshUpdateBankOpeningBalance} getBankDetail={this.props.getBankDetail} getBankDetailList={this.props.getBankDetailList} refreshBankState={this.props.refreshBankState} />
        }
        const AddAccountStatement = ({ match }) => {
            return <AccountStatement businessid={match.params.businessid} fyid={match.params.fyid} salesDetail={this.props.salesDetail} purchaseDetail={this.props.purchaseDetail} getPayment={this.props.getPayment} getVendorList={this.props.getVendorList} getCustomerList={this.props.getCustomerList} getSalesData={this.props.getSalesData} resetPaymentDetail={this.props.resetPaymentDetail} refreshVendorState={this.props.refreshVendorState} refreshCustomerState={this.props.refreshCustomerState} resetSalesDetail={this.props.resetSalesDetail} getCustomer={this.props.getCustomer} getVendor={this.props.getVendor} getPaymentDetail={this.props.getPaymentDetail} refreshVendorState={this.props.refreshVendorState} refreshCustomerState={this.props.refreshCustomerState} getPurchaseData={this.props.getPurchaseData} resetPurchaseDetail={this.props.resetPurchaseDetail} getBusinessList={this.props.getBusinessList} businessList={this.props.businessList} updatePartyOpeningBalance={this.props.updatePartyOpeningBalance} getPartyOpeningBalance={this.props.getPartyOpeningBalance} getOpeningBalance={this.props.getOpeningBalance} updatePartyOpeningMessage={this.props.updatePartyOpeningMessage} resetOpeningBalance={this.props.resetOpeningBalance} logoutMessage={this.props.logoutMessage} resetLogoutMessage={this.props.resetLogoutMessage} logout={this.props.logout} refreshDebitCredit={this.props.refreshDebitCredit} getDebitCredit={this.props.getDebitCredit} getDebitCreditList={this.props.getDebitCreditList} getExpense={this.props.getExpense} getExpenseDetail={this.props.getExpenseDetail} resetExpenseDetail={this.props.resetExpenseDetail} deleteEntryMessage={this.props.deleteEntryMessage} deleteEntry={this.props.deleteEntry} resetIncomeOSDetail={this.props.resetIncomeOSDetail} getIncomeOS={this.props.getIncomeOS} getIncomeOSDetail={this.props.getIncomeOSDetail} refreshAssetAccountState={this.props.refreshAssetAccountState} getAssetAccount={this.props.getAssetAccount} getAssetAccountList={this.props.getAssetAccountList} />
        }
        const AddEmployeeAccountStatement = ({ match }) => {
            return <EmployeeAccountStatement businessid={match.params.businessid} fyid={match.params.fyid} getPayment={this.props.getPayment} resetPaymentDetail={this.props.resetPaymentDetail} getPaymentDetail={this.props.getPaymentDetail} getBusinessList={this.props.getBusinessList} businessList={this.props.businessList} logoutMessage={this.props.logoutMessage} resetLogoutMessage={this.props.resetLogoutMessage} logout={this.props.logout} getExpense={this.props.getExpense} getExpenseDetail={this.props.getExpenseDetail} resetExpenseDetail={this.props.resetExpenseDetail} refreshEmployeeState={this.props.refreshEmployeeState} getEmployee={this.props.getEmployee} getEmployeeList={this.props.getEmployeeList} getEmployeeOpeningBalance={this.props.getEmployeeOpeningBalance} getEmployeeOpeningBalanceList={this.props.getEmployeeOpeningBalanceList} resetEmployeeOpeningBalance={this.props.resetEmployeeOpeningBalance} />
        }
        const AddBusinessChoice = ({ match }) => {
            return <BusinessChoice getSalesData={this.props.getSalesData} salesDetail={this.props.salesDetail} businessid={match.params.businessid} fyid={match.params.fyid} getCustomer={this.props.getCustomer} getCustomerList={this.props.getCustomerList} getItem={this.props.getItem} getItemList={this.props.getItemList} getBankDetail={this.props.getBankDetail} getBankDetailList={this.props.getBankDetailList} purchaseDetail={this.props.purchaseDetail} getPurchaseData={this.props.getPurchaseData} getExpenseDetail={this.props.getExpenseDetail} getExpense={this.props.getExpense} getBankEntryDetail={this.props.getBankEntryDetail} getBankEntry={this.props.getBankEntry} resetBankEntryDetail={this.props.resetBankEntryDetail} getBankEntry={this.props.getBankEntry} getBankEntryDetail={this.props.getBankEntryDetail} getOpeningClosing={this.props.getOpeningClosing} getOpeningClosingDetail={this.props.getOpeningClosingDetail} getOpeningBalance={this.props.getOpeningBalance} getPartyOpeningBalance={this.props.getPartyOpeningBalance} logoutMessage={this.props.logoutMessage} resetLogoutMessage={this.props.resetLogoutMessage} logout={this.props.logout} getCashInHandDetail={this.props.getCashInHandDetail} getCashInHand={this.props.getCashInHand} businessList={this.props.businessList} getDebitCredit={this.props.getDebitCredit} getDebitCreditList={this.props.getDebitCreditList} getBankOpeningBalance={this.props.getBankOpeningBalance} getBankOpeningBalanceList={this.props.getBankOpeningBalanceList} getLoanDetail={this.props.getLoanDetail} getLoan={this.props.getLoan} getInvestment={this.props.getInvestment} getInvestmentDetail={this.props.getInvestmentDetail} refreshInvestmentState={this.props.refreshInvestmentState} getDepreciation={this.props.getDepreciation} getDepreciationList={this.props.getDepreciationList} getLoanOpeningBalance={this.props.getLoanOpeningBalance} getLoanOpeningBalanceList={this.props.getLoanOpeningBalanceList} getAssetOpeningBalanceList={this.props.getAssetOpeningBalanceList} getAssetOpeningBalance={this.props.getAssetOpeningBalance} getInvestmentOpeningBalance={this.props.getInvestmentOpeningBalance} getInvestmentOpeningBalanceList={this.props.getInvestmentOpeningBalanceList} getTDSOpeningBalance={this.props.getTDSOpeningBalance} getTDSOpeningBalanceList={this.props.getTDSOpeningBalanceList} getIncomeTaxOpeningBalance={this.props.getIncomeTaxOpeningBalance} getIncomeTaxOpeningBalanceList={this.props.getIncomeTaxOpeningBalanceList} getEmployeeOpeningBalance={this.props.getEmployeeOpeningBalance} getEmployeeOpeningBalanceList={this.props.getEmployeeOpeningBalanceList} getEmployee={this.props.getEmployee} getEmployeeList={this.props.getEmployeeList}/>
        }
        const AddCashFlow = ({ match }) => {
            return <CashFlow businessid={match.params.businessid} fyid={match.params.fyid} getPayment={this.props.getPayment} logoutMessage={this.props.logoutMessage} resetLogoutMessage={this.props.resetLogoutMessage} logout={this.props.logout} getCustomer={this.props.getCustomer} getVendor={this.props.getVendor} resetPaymentDetail={this.props.resetPaymentDetail} getPaymentDetail={this.props.getPaymentDetail} businessList={this.props.businessList} getCashInHand={this.props.getCashInHand} salesDetail={this.props.salesDetail} getSalesData={this.props.getSalesData} getExpense={this.props.getExpense} getExpenseDetail={this.props.getExpenseDetail} resetSalesDetail={this.props.resetSalesDetail} resetExpenseDetail={this.props.resetExpenseDetail} getBankEntryDetail={this.props.getBankEntryDetail} getBankEntry={this.props.getBankEntry} resetBankEntryDetail={this.props.resetBankEntryDetail} resetCashInHand={this.props.resetCashInHand} getCashInHand={this.props.getCashInHand} getCashInHandDetail={this.props.getCashInHandDetail} resetIncomeOSDetail={this.props.resetIncomeOSDetail} getIncomeOS={this.props.getIncomeOS} getIncomeOSDetail={this.props.getIncomeOSDetail} refreshLoanState={this.props.refreshLoanState} getLoan={this.props.getLoan} getLoanDetail={this.props.getLoanDetail} getInvestment={this.props.getInvestment} getInvestmentDetail={this.props.getInvestmentDetail} refreshInvestmentState={this.props.refreshInvestmentState} deleteEntryMessage={this.props.deleteEntryMessage} deleteEntry={this.props.deleteEntry} resetDeleteEntry={this.props.resetDeleteEntry} refreshAssetAccountState={this.props.refreshAssetAccountState} getAssetAccount={this.props.getAssetAccount} getAssetAccountList={this.props.getAssetAccountList} />
        }
        const AddBankBook = ({ match }) => {
            return <BankBookStatement businessid={match.params.businessid} fyid={match.params.fyid} getPayment={this.props.getPayment} logoutMessage={this.props.logoutMessage} resetLogoutMessage={this.props.resetLogoutMessage} logout={this.props.logout} resetPaymentDetail={this.props.resetPaymentDetail} getPaymentDetail={this.props.getPaymentDetail} businessList={this.props.businessList} getBankEntryDetail={this.props.getBankEntryDetail} getBankEntry={this.props.getBankEntry} resetBankEntryDetail={this.props.resetBankEntryDetail} getBankDetail={this.props.getBankDetail} getBankDetailList={this.props.getBankDetailList} refreshBankState={this.props.refreshBankState} resetBankOpeningBalance={this.props.resetBankOpeningBalance} getBankOpeningBalance={this.props.getBankOpeningBalance} getBankOpeningBalanceList={this.props.getBankOpeningBalanceList} getExpenseDetail={this.props.getExpenseDetail} getExpense={this.props.getExpense} resetExpenseDetail={this.props.resetExpenseDetail} resetIncomeOSDetail={this.props.resetIncomeOSDetail} getIncomeOS={this.props.getIncomeOS} getIncomeOSDetail={this.props.getIncomeOSDetail} refreshLoanState={this.props.refreshLoanState} getLoan={this.props.getLoan} getLoanDetail={this.props.getLoanDetail} getInvestment={this.props.getInvestment} getInvestmentDetail={this.props.getInvestmentDetail} getInvestment={this.props.getInvestment} getInvestmentDetail={this.props.getInvestmentDetail} refreshInvestmentState={this.props.refreshInvestmentState} deleteEntryMessage={this.props.deleteEntryMessage} deleteEntry={this.props.deleteEntry} resetDeleteEntry={this.props.resetDeleteEntry} />
        }
        const AddGSTStatement = ({ match }) => {
            return <GSTStatement businessid={match.params.businessid} fyid={match.params.fyid} logoutMessage={this.props.logoutMessage} resetLogoutMessage={this.props.resetLogoutMessage} logout={this.props.logout} businessList={this.props.businessList} resetPurchaseDetail={this.props.resetPurchaseDetail} resetSalesDetail={this.props.resetSalesDetail} resetGSTOpeningBalance={this.props.resetGSTOpeningBalance} purchaseDetail={this.props.purchaseDetail} salesDetail={this.props.salesDetail} getGSTOpeningBalance={this.props.getGSTOpeningBalance} getPurchaseData={this.props.getPurchaseData} getSalesData={this.props.getSalesData} getGSTOpeningBalanceDetail={this.props.getGSTOpeningBalanceDetail} refreshDebitCredit={this.props.refreshDebitCredit} getDebitCredit={this.props.getDebitCredit} getDebitCreditList={this.props.getDebitCreditList} resetPaymentDetail={this.props.resetPaymentDetail} getPayment={this.props.getPayment} getPaymentDetail={this.props.getPaymentDetail} getAssetAccount={this.props.getAssetAccount} getAssetAccountList={this.props.getAssetAccountList} refreshAssetAccountState={this.props.refreshAssetAccountState} />
        }
        const AddCashInHand = ({ match }) => {
            return <CashInHand businessid={match.params.businessid} fyid={match.params.fyid} getCashInHand={this.props.getCashInHand} getCashInHandDetail={this.props.getCashInHandDetail} updateCashInHandDetail={this.props.updateCashInHandDetail} updateCashInHandMessage={this.props.updateCashInHandMessage} resetCashInHandDetail={this.props.resetCashInHandDetail} logoutMessage={this.props.logoutMessage} resetLogoutMessage={this.props.resetLogoutMessage} logout={this.props.logout} resetCashInHand={this.props.resetCashInHand} businessList={this.props.businessList} />
        }
        const AddCashInHandStatement = ({ match }) => {
            return <CashInHandStatement businessid={match.params.businessid} fyid={match.params.fyid} getCashInHand={this.props.getCashInHand} getBankEntry={this.props.getBankEntry} getCashInHandDetail={this.props.getCashInHandDetail} resetCashInHandDetail={this.props.resetCashInHandDetail} logoutMessage={this.props.logoutMessage} resetLogoutMessage={this.props.resetLogoutMessage} logout={this.props.logout} getBankDetail={this.props.getBankDetail} refreshBankState={this.props.refreshBankState} getBankDetailList={this.props.getBankDetailList} />
        }
        const AddEditEmployees = ({ match }) => {
            return <Employee businessid={match.params.businessid} getEmployeeList={this.props.getEmployeeList} resetAddEmployeeMessage={this.props.resetAddEmployeeMessage} addEmployee={this.props.addEmployee} addEmployeeMessage={this.props.addEmployeeMessage} getEmployee={this.props.getEmployee} refreshEmployeeState={this.props.refreshEmployeeState} logoutMessage={this.props.logoutMessage} resetLogoutMessage={this.props.resetLogoutMessage} logout={this.props.logout} businessList={this.props.businessList} />
        }
        const ReviseEmployeeSalary = ({ match }) => {
            return <ReviseSalary businessid={match.params.businessid} employeeid={match.params.employeeid} getEmployee={this.props.getEmployee} reviseSalary={this.props.reviseSalary} reviseSalaryMessage={this.props.reviseSalaryMessage} resetReviseSalaryMessage={this.props.resetReviseSalaryMessage} logoutMessage={this.props.logoutMessage} resetLogoutMessage={this.props.resetLogoutMessage} logout={this.props.logout} businessList={this.props.businessList} />
        }
        const TerminateEmployee = ({ match }) => {
            return <Terminate businessid={match.params.businessid} employeeid={match.params.employeeid} getEmployee={this.props.getEmployee} terminateEmployee={this.props.terminateEmployee} terminateEmployeeMessage={this.props.terminateEmployeeMessage} resetTerminateEmployeeMessage={this.props.resetTerminateEmployeeMessage} logoutMessage={this.props.logoutMessage} resetLogoutMessage={this.props.resetLogoutMessage} logout={this.props.logout} businessList={this.props.businessList} />
        }
        const AddPayRoll = ({ match }) => {
            return <PayRollChoice businessid={match.params.businessid} fyid={match.params.fyid} logoutMessage={this.props.logoutMessage} resetLogoutMessage={this.props.resetLogoutMessage} logout={this.props.logout} businessList={this.props.businessList} getAttendance={this.props.getAttendance} resetGetAttendance={this.props.resetGetAttendance} resetGetMonthAttendance={this.props.resetGetMonthAttendance} getMonthAttendance={this.props.getMonthAttendance} getSalary={this.props.getSalary} resetGetSalary={this.props.resetGetSalary} />
        }
        const AddMarkAttendance = ({ match }) => {
            return <MarkAttendance businessid={match.params.businessid} fyid={match.params.fyid} getEmployee={this.props.getEmployee} getEmployeeList={this.props.getEmployeeList} logoutMessage={this.props.logoutMessage} resetLogoutMessage={this.props.resetLogoutMessage} logout={this.props.logout} refreshEmployeeState={this.props.refreshEmployeeState} businessList={this.props.businessList} businessList={this.props.businessList} completeDate={match.params.completedate} getAttendance={this.props.getAttendance} updateAttendanceMessage={this.props.updateAttendanceMessage} getAttendanceList={this.props.getAttendanceList} updateAttendance={this.props.updateAttendance} resetUpdateAttendanceMessage={this.props.resetUpdateAttendanceMessage} resetGetAttendance={this.props.resetGetAttendance} />
        }
        const AddViewAttendance = ({ match }) => {
            return <ViewAttendance businessid={match.params.businessid} fyid={match.params.fyid} month={match.params.month} getEmployee={this.props.getEmployee} getEmployeeList={this.props.getEmployeeList} logoutMessage={this.props.logoutMessage} resetLogoutMessage={this.props.resetLogoutMessage} logout={this.props.logout} refreshEmployeeState={this.props.refreshEmployeeState} businessList={this.props.businessList} getMonthAttendance={this.props.getMonthAttendance} getMonthAttendanceList={this.props.getMonthAttendanceList} resetGetMonthAttendance={this.props.resetGetMonthAttendance} />
        }
        const AddSalary = ({ match }) => {
            return <CalculateSalary businessid={match.params.businessid} fyid={match.params.fyid} month={match.params.month} logoutMessage={this.props.logoutMessage} resetLogoutMessage={this.props.resetLogoutMessage} logout={this.props.logout} getEmployee={this.props.getEmployee} getEmployeeList={this.props.getEmployeeList} getMonthAttendance={this.props.getMonthAttendance} getMonthAttendanceList={this.props.getMonthAttendanceList} resetGetMonthAttendance={this.props.resetGetMonthAttendance} refreshEmployeeState={this.props.refreshEmployeeState} businessList={this.props.businessList} updateSalary={this.props.updateSalary} getSalaryList={this.props.getSalaryList} updateSalaryMessage={this.props.updateSalaryMessage} getSalary={this.props.getSalary} getSalaryList={this.props.getSalaryList} resetUpdateSalaryMessage={this.props.resetUpdateSalaryMessage} resetGetSalary={this.props.resetGetSalary} />
        }
        const ConfigureGSTOpeningBalance = ({ match }) => {
            return <GSTOpeningBalance businessid={match.params.businessid} fyid={match.params.fyid} getGSTOpeningBalance={this.props.getGSTOpeningBalance} getGSTOpeningBalanceDetail={this.props.getGSTOpeningBalanceDetail} updateGSTOpeningBalanceMessage={this.props.updateGSTOpeningBalanceMessage} updateGSTOpeningBalance={this.props.updateGSTOpeningBalance} resetGSTOpeningBalanceMessage={this.props.resetGSTOpeningBalanceMessage} logoutMessage={this.props.logoutMessage} resetLogoutMessage={this.props.resetLogoutMessage} logout={this.props.logout} resetGSTOpeningBalance={this.props.resetGSTOpeningBalance} businessList={this.props.businessList} />
        }
        const AddDebitCredit = ({ match }) => {
            return <DebitCredit businessid={match.params.businessid} fyid={match.params.fyid} refreshCustomerState={this.props.refreshCustomerState} refreshVendorState={this.props.refreshVendorState} refreshDebitCredit={this.props.refreshDebitCredit} addDebitCreditMessage={this.props.addDebitCreditMessage} resetaddDebitCreditMessage={this.props.resetaddDebitCreditMessage} getCustomer={this.props.getCustomer} getVendor={this.props.getVendor} getDebitCredit={this.props.getDebitCredit} getCustomerList={this.props.getCustomerList} getVendorList={this.props.getVendorList} getDebitCreditList={this.props.getDebitCreditList} raiseDebitCreditNote={this.props.raiseDebitCreditNote} logoutMessage={this.props.logoutMessage} resetLogoutMessage={this.props.resetLogoutMessage} logout={this.props.logout} businessList={this.props.businessList} />
        }
        const ReverseChargeStatement = ({ match }) => {
            return <ReverseChargeDetail businessid={match.params.businessid} fyid={match.params.fyid} resetSalesDetail={this.props.resetSalesDetail} salesDetail={this.props.salesDetail} getSalesData={this.props.getSalesData} businessList={this.props.businessList} logoutMessage={this.props.logoutMessage} resetLogoutMessage={this.props.resetLogoutMessage} logout={this.props.logout} getPayment={this.props.getPayment} getPaymentDetail={this.props.getPaymentDetail} resetPaymentDetail={this.props.resetPaymentDetail} getRCOpeningBalance={this.props.getRCOpeningBalance} getRCOpeningBalanceList={this.props.getRCOpeningBalanceList} resetRCOpeningBalance={this.props.resetRCOpeningBalance} />
        }
        const AddChangePassword = () => {
            return <ChangePassword changePassword={this.props.changePassword} changePasswordMessage={this.props.changePasswordMessage} resetChangePasswordMessage={this.props.resetChangePasswordMessage} businessList={this.props.businessList} logoutMessage={this.props.logoutMessage} resetLogoutMessage={this.props.resetLogoutMessage} logout={this.props.logout} />
        }
        const AddLoan = ({ match }) => {
            return <LoanEntry businessid={match.params.businessid} fyid={match.params.fyid} addLoanMessage={this.props.addLoanMessage} resetaddLoanMessage={this.props.resetaddLoanMessage} raiseLoan={this.props.raiseLoan} refreshBankState={this.props.refreshBankState} getBankDetail={this.props.getBankDetail} getBankDetailList={this.props.getBankDetailList} refreshLoanState={this.props.refreshLoanState} getLoan={this.props.getLoan} getLoanDetail={this.props.getLoanDetail} logoutMessage={this.props.logoutMessage} resetLogoutMessage={this.props.resetLogoutMessage} logout={this.props.logout} businessList={this.props.businessList} getLoanAccount={this.props.getLoanAccount} getLoanAccountList={this.props.getLoanAccountList} refreshLoanAccountState={this.props.refreshLoanAccountState} getInvestmentAccount={this.props.getInvestmentAccount} getInvestmentAccountList={this.props.getInvestmentAccountList} refreshInvestmentAccountState={this.props.refreshInvestmentAccountState} />
        }
        const AddLoanAccount = ({ match }) => {
            return < LoanAccount businessid={match.params.businessid} refreshLoanAccountState={this.props.refreshLoanAccountState} getLoanAccount={this.props.getLoanAccount} getLoanAccountList={this.props.getLoanAccountList} addLoanAccountMessage={this.props.addLoanAccountMessage} resetAddLoanAccountMessage={this.props.resetAddLoanAccountMessage} addLoanAccount={this.props.addLoanAccount} logoutMessage={this.props.logoutMessage} resetLogoutMessage={this.props.resetLogoutMessage} logout={this.props.logout} businessList={this.props.businessList} resetAddLoanAccountMessageAndForm={this.props.resetAddLoanAccountMessageAndForm} deleteEntryMessage={this.props.deleteEntryMessage} deleteEntry={this.props.deleteEntry} resetDeleteEntry={this.props.resetDeleteEntry} />
        }
        const AddLoanBook = ({ match }) => {
            return < LoanBook businessid={match.params.businessid} fyid={match.params.fyid} refreshLoanState={this.props.refreshLoanState} getLoan={this.props.getLoan} getLoanDetail={this.props.getLoanDetail} logoutMessage={this.props.logoutMessage} resetLogoutMessage={this.props.resetLogoutMessage} logout={this.props.logout} businessList={this.props.businessList} getLoanAccount={this.props.getLoanAccount} getLoanAccountList={this.props.getLoanAccountList} refreshLoanAccountState={this.props.refreshLoanAccountState} businessList={this.props.businessList} getLoanOpeningBalance={this.props.getLoanOpeningBalance} getLoanOpeningBalanceList={this.props.getLoanOpeningBalanceList} resetLoanOpeningBalance={this.props.resetLoanOpeningBalance} deleteEntryMessage={this.props.deleteEntryMessage} deleteEntry={this.props.deleteEntry} resetDeleteEntry={this.props.resetDeleteEntry} />
        }
        const AddInvestment = ({ match }) => {
            return < Investment businessid={match.params.businessid} fyid={match.params.fyid} refreshInvestmentState={this.props.refreshInvestmentState} getInvestment={this.props.getInvestment} getInvestmentDetail={this.props.getInvestmentDetail} logoutMessage={this.props.logoutMessage} resetLogoutMessage={this.props.resetLogoutMessage} logout={this.props.logout} businessList={this.props.businessList} addInvestmentMessage={this.props.addInvestmentMessage} resetaddInvestmentMessage={this.props.resetaddInvestmentMessage} raiseInvestment={this.props.raiseInvestment} refreshBankState={this.props.refreshBankState} getBankDetail={this.props.getBankDetail} getBankDetailList={this.props.getBankDetailList} getInvestmentAccount={this.props.getInvestmentAccount} refreshInvestmentAccountState={this.props.refreshInvestmentAccountState} getInvestmentAccountList={this.props.getInvestmentAccountList} />
        }
        const AddAssetDepreciation = ({ match }) => {
            return < AssetDepreciation businessid={match.params.businessid} fyid={match.params.fyid} logoutMessage={this.props.logoutMessage} resetLogoutMessage={this.props.resetLogoutMessage} logout={this.props.logout} businessList={this.props.businessList} refreshAssetAccountState={this.props.refreshAssetAccountState} getAssetAccount={this.props.getAssetAccount} getAssetAccountList={this.props.getAssetAccountList} addDepreciationMessage={this.props.addDepreciationMessage} resetAddDepreciationMessage={this.props.resetAddDepreciationMessage} raiseDepreciation={this.props.raiseDepreciation} getDepreciation={this.props.getDepreciation} getDepreciationList={this.props.getDepreciationList} refreshDepreciationState={this.props.refreshDepreciationState} />
        }
        const AddInvestmentBook = ({ match }) => {
            return < InvestmentBook businessid={match.params.businessid} fyid={match.params.fyid} refreshInvestmentState={this.props.refreshInvestmentState} getInvestment={this.props.getInvestment} getInvestmentDetail={this.props.getInvestmentDetail} logoutMessage={this.props.logoutMessage} resetLogoutMessage={this.props.resetLogoutMessage} logout={this.props.logout} businessList={this.props.businessList} deleteEntryMessage={this.props.deleteEntryMessage} deleteEntry={this.props.deleteEntry} resetDeleteEntry={this.props.resetDeleteEntry} getInvestmentAccount={this.props.getInvestmentAccount} refreshInvestmentAccountState={this.props.refreshInvestmentAccountState} getInvestmentAccountList={this.props.getInvestmentAccountList} getInvestmentOpeningBalance={this.props.getInvestmentOpeningBalance} getInvestmentOpeningBalanceList={this.props.getInvestmentOpeningBalanceList} resetInvestmentOpeningBalance={this.props.resetInvestmentOpeningBalance} resetIncomeOSDetail={this.props.resetIncomeOSDetail} getIncomeOS={this.props.getIncomeOS} getIncomeOSDetail={this.props.getIncomeOSDetail} resetExpenseDetail={this.props.resetExpenseDetail} getExpense={this.props.getExpense} getExpenseDetail={this.props.getExpenseDetail} refreshLoanState={this.props.refreshLoanState} getLoan={this.props.getLoan} getLoanDetail={this.props.getLoanDetail} getPayment={this.props.getPayment} getPaymentDetail={this.props.getPaymentDetail} resetPaymentDetail={this.props.resetPaymentDetail} getSalesData={this.props.getSalesData} getSales={this.props.salesDetail} getPurchaseData={this.props.getPurchaseData} getPurchase={this.props.purchaseDetail} getOpeningClosingDetail={this.props.getOpeningClosingDetail} getDebitCreditList={this.props.getDebitCreditList} getAssetAccountList={this.props.getAssetAccountList} getAssetOpeningBalanceList={this.props.getAssetOpeningBalanceList} getDepreciationList={this.props.getDepreciationList} salesDetail={this.props.salesDetail} getPurchase={this.props.purchaseDetail} getOpeningClosing={this.props.getOpeningClosing} getDebitCredit={this.props.getDebitCredit} getInvestment={this.props.getInvestment} getAssetAccount={this.props.getAssetAccount} getAssetOpeningBalance={this.props.getAssetOpeningBalance} getDepreciation={this.props.getDepreciation} />
        }
        const AddTDSBook = ({ match }) => {
            return < TDSStatement businessid={match.params.businessid} fyid={match.params.fyid} logoutMessage={this.props.logoutMessage} resetLogoutMessage={this.props.resetLogoutMessage} logout={this.props.logout} businessList={this.props.businessList} resetPaymentDetail={this.props.resetPaymentDetail} resetExpenseDetail={this.props.resetExpenseDetail} resetIncomeOSDetail={this.props.resetIncomeOSDetail} getPayment={this.props.getPayment} getExpense={this.props.getExpense} getIncomeOS={this.props.getIncomeOS} getPaymentDetail={this.props.getPaymentDetail} getExpenseDetail={this.props.getExpenseDetail} getIncomeOSDetail={this.props.getIncomeOSDetail} getIncomeTaxOpeningBalanceList={this.props.getIncomeTaxOpeningBalanceList} getIncomeTaxOpeningBalance={this.props.getIncomeTaxOpeningBalance} resetIncomeTaxOpeningBalance={this.props.resetIncomeTaxOpeningBalance} deleteEntryMessage={this.props.deleteEntryMessage} deleteEntry={this.props.deleteEntry} resetDeleteEntry={this.props.resetDeleteEntry} />
        }
        const AddAssetAccount = ({ match }) => {
            return < AssetAccount businessid={match.params.businessid} refreshAssetAccountState={this.props.refreshAssetAccountState} getAssetAccount={this.props.getAssetAccount} getAssetAccountList={this.props.getAssetAccountList} addAssetAccountMessage={this.props.addAssetAccountMessage} resetAddAssetAccountMessage={this.props.resetAddAssetAccountMessage} addAssetAccount={this.props.addAssetAccount} logoutMessage={this.props.logoutMessage} resetLogoutMessage={this.props.resetLogoutMessage} logout={this.props.logout} businessList={this.props.businessList} resetAddAssetAccountMessageAndForm={this.props.resetAddAssetAccountMessageAndForm} />
        }
        const ConfigureLoanOpeningBalance = ({ match }) => {
            return <LoanOpeningBalance businessid={match.params.businessid} fyid={match.params.fyid} businessList={this.props.businessList} updateLoanOpeningBalance={this.props.updateLoanOpeningBalance} getLoanOpeningBalance={this.props.getLoanOpeningBalance} getLoanOpeningBalanceList={this.props.getLoanOpeningBalanceList} updateLoanOpeningMessage={this.props.updateLoanOpeningMessage} resetLoanOpeningBalance={this.props.resetLoanOpeningBalance} logoutMessage={this.props.logoutMessage} resetLogoutMessage={this.props.resetLogoutMessage} logout={this.props.logout} refreshUpdateLoanOpeningBalance={this.props.refreshUpdateLoanOpeningBalance} getLoanAccount={this.props.getLoanAccount} getLoanAccountList={this.props.getLoanAccountList} refreshLoanAccountState={this.props.refreshLoanAccountState} />
        }
        const ConfigureAssetOpeningBalance = ({ match }) => {
            return <AssetOpeningBalance businessid={match.params.businessid} fyid={match.params.fyid} businessList={this.props.businessList} updateAssetOpeningBalance={this.props.updateAssetOpeningBalance} getAssetOpeningBalance={this.props.getAssetOpeningBalance} getAssetOpeningBalanceList={this.props.getAssetOpeningBalanceList} updateAssetOpeningMessage={this.props.updateAssetOpeningMessage} resetAssetOpeningBalance={this.props.resetAssetOpeningBalance} logoutMessage={this.props.logoutMessage} resetLogoutMessage={this.props.resetLogoutMessage} logout={this.props.logout} refreshUpdateAssetOpeningBalance={this.props.refreshUpdateAssetOpeningBalance} getAssetAccount={this.props.getAssetAccount} getAssetAccountList={this.props.getAssetAccountList} refreshAssetAccountState={this.props.refreshAssetAccountState} />
        }
        const AddAssetBook = ({ match }) => {
            return <AssetBook businessid={match.params.businessid} fyid={match.params.fyid} businessList={this.props.businessList} getAssetOpeningBalance={this.props.getAssetOpeningBalance} getAssetOpeningBalanceList={this.props.getAssetOpeningBalanceList} resetAssetOpeningBalance={this.props.resetAssetOpeningBalance} logoutMessage={this.props.logoutMessage} resetLogoutMessage={this.props.resetLogoutMessage} logout={this.props.logout} getAssetAccount={this.props.getAssetAccount} getAssetAccountList={this.props.getAssetAccountList} refreshAssetAccountState={this.props.refreshAssetAccountState} getDepreciation={this.props.getDepreciation} getDepreciationList={this.props.getDepreciationList} refreshDepreciationState={this.props.refreshDepreciationState} deleteEntryMessage={this.props.deleteEntryMessage} deleteEntry={this.props.deleteEntry} resetDeleteEntry={this.props.resetDeleteEntry} />
        }
        const AddAccountReceivables = ({ match }) => {
            return <AccountReceivables businessid={match.params.businessid} fyid={match.params.fyid} salesDetail={this.props.salesDetail} getPayment={this.props.getPayment} getCustomerList={this.props.getCustomerList} getSalesData={this.props.getSalesData} resetPaymentDetail={this.props.resetPaymentDetail} refreshCustomerState={this.props.refreshCustomerState} resetSalesDetail={this.props.resetSalesDetail} getCustomer={this.props.getCustomer} getPaymentDetail={this.props.getPaymentDetail} refreshCustomerState={this.props.refreshCustomerState} getBusinessList={this.props.getBusinessList} businessList={this.props.businessList} getOpeningBalance={this.props.getOpeningBalance} resetOpeningBalance={this.props.resetOpeningBalance} getPartyOpeningBalance={this.props.getPartyOpeningBalance} logoutMessage={this.props.logoutMessage} resetLogoutMessage={this.props.resetLogoutMessage} logout={this.props.logout} refreshDebitCredit={this.props.refreshDebitCredit} getDebitCredit={this.props.getDebitCredit} getDebitCreditList={this.props.getDebitCreditList} resetIncomeOSDetail={this.props.resetIncomeOSDetail} getIncomeOS={this.props.getIncomeOS} getIncomeOSDetail={this.props.getIncomeOSDetail} refreshAssetAccountState={this.props.refreshAssetAccountState} getAssetAccount={this.props.getAssetAccount} getAssetAccountList={this.props.getAssetAccountList} />
        }
        const AddAccountPayables = ({ match }) => {
            return <AccountPayables businessid={match.params.businessid} fyid={match.params.fyid} purchaseDetail={this.props.purchaseDetail} getPayment={this.props.getPayment} getVendorList={this.props.getVendorList} getPurchaseData={this.props.getPurchaseData} resetPaymentDetail={this.props.resetPaymentDetail} refreshVendorState={this.props.refreshVendorState} resetPurchaseDetail={this.props.resetPurchaseDetail} getVendor={this.props.getVendor} getPaymentDetail={this.props.getPaymentDetail} refreshVendorState={this.props.refreshVendorState} getBusinessList={this.props.getBusinessList} businessList={this.props.businessList} getOpeningBalance={this.props.getOpeningBalance} resetOpeningBalance={this.props.resetOpeningBalance} getPartyOpeningBalance={this.props.getPartyOpeningBalance} logoutMessage={this.props.logoutMessage} resetLogoutMessage={this.props.resetLogoutMessage} logout={this.props.logout} refreshDebitCredit={this.props.refreshDebitCredit} getDebitCredit={this.props.getDebitCredit} getDebitCreditList={this.props.getDebitCreditList} getExpenseDetail={this.props.getExpenseDetail} getExpense={this.props.getExpense} resetExpenseDetail={this.props.resetExpenseDetail} refreshAssetAccountState={this.props.refreshAssetAccountState} getAssetAccount={this.props.getAssetAccount} getAssetAccountList={this.props.getAssetAccountList} />
        }
        const ConfigureInvestementOpeningBalance = ({ match }) => {
            return <InvestmentOpeningBalance businessid={match.params.businessid} fyid={match.params.fyid} businessList={this.props.businessList} updateInvestmentOpeningBalance={this.props.updateInvestmentOpeningBalance} getInvestmentOpeningBalance={this.props.getInvestmentOpeningBalance} getInvestmentOpeningBalanceList={this.props.getInvestmentOpeningBalanceList} updateInvestmentOpeningMessage={this.props.updateInvestmentOpeningMessage} resetInvestmentOpeningBalance={this.props.resetInvestmentOpeningBalance} logoutMessage={this.props.logoutMessage} resetLogoutMessage={this.props.resetLogoutMessage} logout={this.props.logout} refreshUpdateInvestmentOpeningBalance={this.props.refreshUpdateInvestmentOpeningBalance} getInvestmentAccount={this.props.getInvestmentAccount} getInvestmentAccountList={this.props.getInvestmentAccountList} refreshInvestmentAccountState={this.props.refreshInvestmentAccountState} />
        }
        const ConfigureEmployeeOpeningBalance = ({ match }) => {
            return <EmployeeOpeningBalance businessid={match.params.businessid} fyid={match.params.fyid} businessList={this.props.businessList} getEmployeeOpeningBalance={this.props.getEmployeeOpeningBalance} getEmployeeOpeningBalanceList={this.props.getEmployeeOpeningBalanceList} updateEmployeeOpeningBalance={this.props.updateEmployeeOpeningBalance} updateEmployeeOpeningMessage={this.props.updateEmployeeOpeningMessage} resetEmployeeOpeningBalance={this.props.resetEmployeeOpeningBalance} logoutMessage={this.props.logoutMessage} resetLogoutMessage={this.props.resetLogoutMessage} logout={this.props.logout} refreshUpdateEmployeeOpeningBalance={this.props.refreshUpdateEmployeeOpeningBalance} getEmployee={this.props.getEmployee} getEmployeeList={this.props.getEmployeeList} refreshEmployeeState={this.props.refreshEmployeeState} />
        }
        const AddInvestmentAccount = ({ match }) => {
            return <InvestmentAccount businessid={match.params.businessid} refreshInvestmentAccountState={this.props.refreshInvestmentAccountState} getInvestmentAccount={this.props.getInvestmentAccount} getInvestmentAccountList={this.props.getInvestmentAccountList} addInvestmentAccountMessage={this.props.addInvestmentAccountMessage} resetAddInvestmentAccountMessage={this.props.resetAddInvestmentAccountMessage} addInvestmentAccount={this.props.addInvestmentAccount} logoutMessage={this.props.logoutMessage} resetLogoutMessage={this.props.resetLogoutMessage} logout={this.props.logout} businessList={this.props.businessList} resetAddInvestmentAccountMessageAndForm={this.props.resetAddInvestmentAccountMessageAndForm} />
        }
        const ConfigureTDSOpeningBalance = ({ match }) => {
            return <TDSOpeningBalance businessid={match.params.businessid} fyid={match.params.fyid} businessList={this.props.businessList} updateTDSOpeningBalance={this.props.updateTDSOpeningBalance} getTDSOpeningBalance={this.props.getTDSOpeningBalance} getTDSOpeningBalanceList={this.props.getTDSOpeningBalanceList} updateTDSOpeningMessage={this.props.updateTDSOpeningMessage} resetTDSOpeningBalance={this.props.resetTDSOpeningBalance} logoutMessage={this.props.logoutMessage} resetLogoutMessage={this.props.resetLogoutMessage} logout={this.props.logout} refreshUpdateTDSOpeningBalance={this.props.refreshUpdateTDSOpeningBalance} />
        }
        const AddTrialBalance = ({ match }) => {
            return (<TrialBalance businessid={match.params.businessid} fyid={match.params.fyid} businessList={this.props.businessList} logoutMessage={this.props.logoutMessage} resetLogoutMessage={this.props.resetLogoutMessage} logout={this.props.logout} salesDetail={this.props.salesDetail} purchaseDetail={this.props.purchaseDetail} getExpense={this.props.getExpense} getPayment={this.props.getPayment} getBankEntry={this.props.getBankEntry} getDebitCredit={this.props.getDebitCredit} getIncomeOS={this.props.getIncomeOS} getLoan={this.props.getLoan} getInvestment={this.props.getInvestment} getCustomer={this.props.getCustomer} getVendor={this.props.getVendor} getExpenseHead={this.props.getExpenseHead} getBankDetail={this.props.getBankDetail} getLoanAccount={this.props.getLoanAccount} getInvestmentAccount={this.props.getInvestmentAccount} getAssetAccount={this.props.getAssetAccount} getAssetOpeningBalance={this.props.getAssetOpeningBalance} getDepreciation={this.props.getDepreciation} getOpeningBalance={this.props.getOpeningBalance} getCashInHand={this.props.getCashInHand} getBankOpeningBalance={this.props.getBankOpeningBalance} getGSTOpeningBalance={this.props.getGSTOpeningBalance} getLoanOpeningBalance={this.props.getLoanOpeningBalance} getAssetOpeningBalance={this.props.getAssetOpeningBalance} getInvestmentOpeningBalance={this.props.getInvestmentOpeningBalance} getTDSOpeningBalance={this.props.getTDSOpeningBalance} getRCOpeningBalance={this.props.getRCOpeningBalance} getRCOpeningBalanceList={this.props.getRCOpeningBalanceList} resetRCOpeningBalance={this.props.resetRCOpeningBalance} getOpeningClosing={this.props.getOpeningClosing} getIncomeTaxOpeningBalance={this.props.getIncomeTaxOpeningBalance} />);
        }
        const ConfigureRCOpeningBalance = ({ match }) => {
            return <RCOpeningBalance businessid={match.params.businessid} fyid={match.params.fyid} businessList={this.props.businessList} updateRCOpeningBalance={this.props.updateRCOpeningBalance} getRCOpeningBalance={this.props.getRCOpeningBalance} getRCOpeningBalanceList={this.props.getRCOpeningBalanceList} updateRCOpeningMessage={this.props.updateRCOpeningMessage} resetRCOpeningBalance={this.props.resetRCOpeningBalance} logoutMessage={this.props.logoutMessage} resetLogoutMessage={this.props.resetLogoutMessage} logout={this.props.logout} refreshUpdateRCOpeningBalance={this.props.refreshUpdateRCOpeningBalance} />
        }
        const AddInventory = ({ match }) => {
            return <Inventory businessid={match.params.businessid} fyid={match.params.fyid} logoutMessage={this.props.logoutMessage} resetLogoutMessage={this.props.resetLogoutMessage} logout={this.props.logout} businessList={this.props.businessList} getSalesData={this.props.getSalesData} salesDetail={this.props.salesDetail} resetSalesDetail={this.props.resetSalesDetail} purchaseDetail={this.props.purchaseDetail} resetPurchaseDetail={this.props.resetPurchaseDetail} getPurchaseData={this.props.getPurchaseData} getStockOpeningBalance={this.props.getStockOpeningBalance} getStockOpeningBalanceList={this.props.getStockOpeningBalanceList} resetStockOpeningBalance={this.props.resetStockOpeningBalance} getStockJournal={this.props.getStockJournal} getStockJournalList={this.props.getStockJournalList} />
        }
        const AddStock = ({ match }) => {
            return <Stock businessid={match.params.businessid} fyid={match.params.fyid} logoutMessage={this.props.logoutMessage} resetLogoutMessage={this.props.resetLogoutMessage} logout={this.props.logout} businessList={this.props.businessList} getSalesData={this.props.getSalesData} salesDetail={this.props.salesDetail} resetSalesDetail={this.props.resetSalesDetail} purchaseDetail={this.props.purchaseDetail} resetPurchaseDetail={this.props.resetPurchaseDetail} getPurchaseData={this.props.getPurchaseData} getItem={this.props.getItem} getItemList={this.props.getItemList} refreshItemState={this.props.refreshItemState} getStockOpeningBalance={this.props.getStockOpeningBalance} getStockOpeningBalanceList={this.props.getStockOpeningBalanceList} resetStockOpeningBalance={this.props.resetStockOpeningBalance} refreshGetStockJournal={this.props.refreshGetStockJournal} getStockJournal={this.props.getStockJournal} getStockJournalList={this.props.getStockJournalList} getVendor={this.props.getVendor} refreshVendorState={this.props.refreshVendorState} getVendorList={this.props.getVendorList} />
        }
        const PassStockJournal = ({ match }) => {
            return <StockJournal businessid={match.params.businessid} fyid={match.params.fyid} logoutMessage={this.props.logoutMessage} resetLogoutMessage={this.props.resetLogoutMessage} logout={this.props.logout} businessList={this.props.businessList} getItem={this.props.getItem} getItemList={this.props.getItemList} refreshItemState={this.props.refreshItemState} passStockJournal={this.props.passStockJournal} passStockJournalState={this.props.passStockJournalMessage} refreshStockJournalState={this.props.refreshStockJournalState} getStockJournal={this.props.getStockJournal} getStockJournalList={this.props.getStockJournalList} refreshGetStockJournal={this.props.refreshGetStockJournal} />
        }
        const ConfigureStockOpeningBalance = ({ match }) => {
            return <StockOpeningBalance businessid={match.params.businessid} fyid={match.params.fyid} businessList={this.props.businessList} updateStockOpeningBalance={this.props.updateStockOpeningBalance} getStockOpeningBalance={this.props.getStockOpeningBalance} getStockOpeningBalanceList={this.props.getStockOpeningBalanceList} updateStockOpeningMessage={this.props.updateStockOpeningMessage} resetStockOpeningBalance={this.props.resetStockOpeningBalance} logoutMessage={this.props.logoutMessage} resetLogoutMessage={this.props.resetLogoutMessage} logout={this.props.logout} refreshUpdateStockOpeningBalance={this.props.refreshUpdateStockOpeningBalance} getItem={this.props.getItem} getItemList={this.props.getItemList} refreshItemState={this.props.refreshItemState} />
        }
        const AddIncomeTaxBook = ({ match }) => {
            return <IncomeTaxBook businessid={match.params.businessid} fyid={match.params.fyid} businessList={this.props.businessList} logoutMessage={this.props.logoutMessage} resetLogoutMessage={this.props.resetLogoutMessage} logout={this.props.logout} getExpenseDetail={this.props.getExpenseDetail} getExpense={this.props.getExpense} resetExpenseDetail={this.props.resetExpenseDetail} getPaymentDetail={this.props.getPaymentDetail} getPayment={this.props.getPayment} resetPaymentDetail={this.props.resetPaymentDetail} deleteEntryMessage={this.props.deleteEntryMessage} deleteEntry={this.props.deleteEntry} resetDeleteEntry={this.props.resetDeleteEntry} resetIncomeOSDetail={this.props.resetIncomeOSDetail} getIncomeOS={this.props.getIncomeOS} getIncomeOSDetail={this.props.getIncomeOSDetail} getIncomeTaxOpeningBalance={this.props.getIncomeTaxOpeningBalance} getIncomeTaxOpeningBalanceList={this.props.getIncomeTaxOpeningBalanceList} resetIncomeTaxOpeningBalance={this.props.resetIncomeTaxOpeningBalance} />
        }
        const ConfigureIncomeTaxOpeningBalance = ({ match }) => {
            return <IncomeTaxOpeningBalance businessid={match.params.businessid} fyid={match.params.fyid} businessList={this.props.businessList} updateIncomeTaxOpeningBalance={this.props.updateIncomeTaxOpeningBalance} getIncomeTaxOpeningBalance={this.props.getIncomeTaxOpeningBalance} getIncomeTaxOpeningBalanceList={this.props.getIncomeTaxOpeningBalanceList} resetIncomeTaxOpeningBalance={this.props.resetIncomeTaxOpeningBalance} updateIncomeTaxOpeningMessage={this.props.updateIncomeTaxOpeningMessage} logoutMessage={this.props.logoutMessage} resetLogoutMessage={this.props.resetLogoutMessage} logout={this.props.logout} refreshUpdateIncomeTaxOpeningBalance={this.props.refreshUpdateIncomeTaxOpeningBalance} />
        }
        const AddInvestmentAccountChange = ({ match }) => {
            return <InvestmentAccountChange businessid={match.params.businessid} id={match.params.id} getInvestmentAccount={this.props.getInvestmentAccount} updateInvestmentAccountMessage={this.props.updateInvestmentAccountMessage} resetUpdateInvestmentAccountMessage={this.props.resetUpdateInvestmentAccountMessage} updateInvestmentAccount={this.props.updateInvestmentAccount} logoutMessage={this.props.logoutMessage} resetLogoutMessage={this.props.resetLogoutMessage} logout={this.props.logout} businessList={this.props.businessList} setDefaultInvestmentAccountChange={this.props.setDefaultInvestmentAccountChange} resetUpdateInvestmentAccountMessage={this.props.resetUpdateInvestmentAccountMessage} />
        }
        return (
            <div id="root">
                <React.Suspense fallback={<div className="text-center">Please Wait ...</div>}>
                    <Switch>
                        <Route exact path="/" component={() => <Login postLoginUser={this.props.postLoginUserDispatcher} postRegisterUser={this.props.postRegisterUserDispatcher} loginMessage={this.props.loginMessage} registerMessage={this.props.registerMessage} getBusinessList={this.props.getBusinessList} businessList={this.props.businessList} addBusinessMessage={this.props.addBusinessMessage} resetRegisterForm={this.props.resetRegisterForm} resetLoginForm={this.props.resetLoginForm} refreshBusinessList={this.props.refreshBusinessList} />} />
                        <Route exact path="/registeruser" component={() => <Register resetRegisterForm={this.props.resetRegisterForm} postRegisterUser={this.props.postRegisterUser} registerMessage={this.props.registerMessage} />} />
                        <Route exact path="/business" component={() => <BusinessList businessList={this.props.businessList} resetLoginForm={this.props.resetLoginForm} getBusinessList={this.props.getBusinessList} resetAddBusinessMessage={this.props.resetAddBusinessMessage} addBusinessMessage={this.props.addBusinessMessage} resetAddSubUserMessage={this.props.resetAddSubUserMessage} addSubUserMessage={this.props.addSubUserMessage} addFinancialYear={this.props.addFinancialYear} addFinancialYearMessage={this.props.addFinancialYearMessage} resetBusinessChange={this.props.resetBusinessChange} updateBusinessMessage={this.props.updateBusinessMessage} updateCustomerMessage={this.props.updateCustomerMessage} resetCustomerChange={this.props.resetCustomerChange} resetBusinessChange={this.props.resetBusinessChange} getSalesData={this.props.getSalesData} refreshBusinessList={this.props.refreshBusinessList} refreshCustomerState={this.props.refreshCustomerState} refreshVendorState={this.props.refreshVendorState} getBankDetail={this.props.getBankDetail} refreshBankState={this.props.refreshBankState} getCustomer={this.props.getCustomer} getVendor={this.props.getVendor} refreshItemState={this.props.refreshItemState} getItem={this.props.getItem} resetCVIB={this.props.resetCVIB} consumeIndividualBusinessMessage={this.props.consumeIndividualBusinessMessage} individualBusinessMessage={this.props.individualBusinessMessage} logoutMessage={this.props.logoutMessage} resetLogoutMessage={this.props.resetLogoutMessage} logout={this.props.logout} getEmployee={this.props.getEmployee} refreshEmployeeState={this.props.refreshEmployeeState} getExpenseHead={this.props.getExpenseHead} refreshExpenseHead={this.props.refreshExpenseHead} getLoanAccount={this.props.getLoanAccount} refreshLoanAccountState={this.props.refreshLoanAccountState} getAssetAccount={this.props.getAssetAccount} getAssetAccountList={this.props.getAssetAccountList} getAssetAccount={this.props.getAssetAccount} refreshAssetAccountState={this.props.refreshAssetAccountState} getInvestmentAccount={this.props.getInvestmentAccount} refreshInvestmentAccountState={this.props.refreshInvestmentAccountState} />} />
                        <Route exact path="/business/addbusiness" component={() => <AddBusiness addBusinessMessage={this.props.addBusinessMessage} postAddBusiness={this.props.postAddBusiness} resetBusinessFormAndMessage={this.props.resetBusinessFormAndMessage} pushNewBusiness={this.props.pushNewBusiness} resetAddBusinessMessage={this.props.resetAddBusinessMessage} resetAddBusinessForm={this.props.resetAddBusinessForm} businessList={this.props.businessList} logoutMessage={this.props.logoutMessage} resetLogoutMessage={this.props.resetLogoutMessage} logout={this.props.logout} />} />
                        <Route exact path="/business/addsubuser" component={() => <AddSubUser addSubUserMessage={this.props.addSubUserMessage} postAddSubUser={this.props.postAddSubUser} resetAddSubUserForm={this.props.resetAddSubUserForm} getSubUserList={this.props.getSubUserList} subUserList={this.props.subUserList} deleteSubUser={this.props.deleteSubUser} deleteSubUserState={this.props.deleteSubUserState} />} />
                        <Route exact path="/business/businessdetail/:businessid" component={BusinessDetailId} />
                        <Route exact path="/business/businessdetail/businesschange/:businessid" component={BusinessChangeId} />
                        <Route exact path="/business/businessdetail/businessprocess/:businessid/:fyid" component={BusinessProcessId} />
                        <Route exact path="/business/businessdetail/customer/:businessid" component={CustomerAdd} />
                        <Route exact path="/business/businessdetail/vendor/:businessid" component={VendorAdd} />
                        <Route exact path="/business/businessdetail/bankdetail/:businessid" component={BankDetailAdd} />
                        <Route exact path="/business/businessdetail/item/:businessid" component={ItemAdd} />
                        <Route exact path="/business/businessdetail/expensehead/:businessid" component={ExpenseHeadAdd} />
                        <Route exact path="/business/businessdetail/incomehead/:businessid" component={IncomeHeadAdd} />
                        <Route exact path="/business/businessdetail/customer/:businessid/:customerid" component={CustomerUpdate} />
                        <Route exact path="/business/businessdetail/vendor/:businessid/:vendorid" component={VendorUpdate} />
                        <Route exact path="/business/businessdetail/bankdetail/:businessid/:bankdetailid" component={BankDetailUpdate} />
                        <Route exact path="/business/businessdetail/item/:businessid/:itemid" component={ItemUpdate} />
                        <Route exact path="/business/businessdetail/raiseinvoice/:businessid/:fyid" component={InvoiceAdd} />
                        <Route exact path="/business/businessdetail/purchase/:businessid/:fyid" component={AddPurchase} />
                        <Route excat path="/business/businessdetail/salesdetail/:businessid/:fyid" component={SalesDetailDisplay} />
                        <Route exact path="/business/invoiceupdate/:businessid/:fyid/:invoiceid" component={InvoiceUpdateMatch} />
                        <Route exact path="/business/purchaseupdate/:businessid/:fyid/:purchaseid" component={PurchaseUpdateMatch} />
                        <Route excat path="/business/businessdetail/purchasedetail/:businessid/:fyid" component={PurchaseDetailDisplay} />
                        <Route excat path="/business/businessdetail/expensedetail/:businessid/:fyid" component={ExpenseDetailDisplay} />
                        <Route exact path="/business/businessdetail/expense/:businessid/:fyid" component={AddExpense} />
                        <Route excat path="/business/businessdetail/incomeosdetail/:businessid/:fyid" component={IncomeDetailDisplay} />
                        <Route exact path="/business/businessdetail/incomeos/:businessid/:fyid" component={AddIncomeOS} />
                        <Route exact path="/business/businessdetail/payments/:businessid/:fyid" component={AddPayment} />
                        <Route exact path="/business/businessdetail/bankentries/:businessid/:fyid" component={AddBankEntry} />
                        <Route exact path="/business/businessdetail/stock/:businessid/:fyid" component={AddOpeningClosingStock} />
                        <Route exact path="/business/businessdetail/pnl/:businessid/:fyid" component={PnL} />
                        <Route exact path="/business/businessdetail/openingbalance/:businessid/:fyid" component={ConfigureOpeningBalance} />
                        <Route exact path="/business/businessdetail/bankopeningbalance/:businessid/:fyid" component={ConfigureBankOpeningBalance} />
                        <Route exact path="/business/businessdetail/gstopeningbalance/:businessid/:fyid" component={ConfigureGSTOpeningBalance} />
                        <Route exact path="/business/businessdetail/accountstatement/:businessid/:fyid" component={AddAccountStatement} />
                        <Route exact path="/business/businessdetail/employeeaccountstatement/:businessid/:fyid" component={AddEmployeeAccountStatement} />
                        <Route exact path="/business/businessdetail/businesschoice/:businessid/:fyid" component={AddBusinessChoice} />
                        <Route exact path="/business/businessdetail/cashflowstatement/:businessid/:fyid" component={AddCashFlow} />
                        <Route exact path="/business/businessdetail/bankbookstatement/:businessid/:fyid" component={AddBankBook} />
                        <Route exact path="/business/businessdetail/gststatement/:businessid/:fyid" component={AddGSTStatement} />
                        <Route exact path="/business/businessdetail/cashinhand/:businessid/:fyid" component={AddCashInHand} />
                        <Route exact path="/business/businessdetail/cashinhandstatement/:businessid/:fyid" component={AddCashInHandStatement} />
                        <Route exact path="/business/businessdetail/employees/:businessid/" component={AddEditEmployees} />
                        <Route exact path="/business/businessdetail/employee/revisesalary/:businessid/:employeeid" component={ReviseEmployeeSalary} />
                        <Route exact path="/business/businessdetail/employee/terminate/:businessid/:employeeid" component={TerminateEmployee} />
                        <Route exact path="/business/businessdetail/payroll/:businessid/:fyid" component={AddPayRoll} />
                        <Route exact path="/business/businessdetail/attendance/:businessid/:fyid/:completedate" component={AddMarkAttendance} />
                        <Route exact path="/business/businessdetail/salary/:businessid/:fyid/:month" component={AddSalary} />
                        <Route exact path="/business/businessdetail/viewattendance/:businessid/:fyid/:month" component={AddViewAttendance} />
                        <Route exact path="/business/businessdetail/debitcredit/:businessid/:fyid" component={AddDebitCredit} />
                        <Route exact path="/business/businessdetail/reversechargesheet/:businessid/:fyid" component={ReverseChargeStatement} />
                        <Route exact path="/business/changepassword/" component={AddChangePassword} />
                        <Route exact path="/business/businessdetail/loan/:businessid/:fyid" component={AddLoan} />
                        <Route exact path="/business/businessdetail/loanaccount/:businessid/" component={AddLoanAccount} />
                        <Route exact path="/business/businessdetail/loanbookstatement/:businessid/:fyid" component={AddLoanBook} />
                        <Route exact path="/business/businessdetail/investment/:businessid/:fyid" component={AddInvestment} />
                        <Route exact path="/business/businessdetail/assetdepreciation/:businessid/:fyid" component={AddAssetDepreciation} />
                        <Route exact path="/business/businessdetail/investmentbookstatement/:businessid/:fyid" component={AddInvestmentBook} />
                        <Route exact path="/business/businessdetail/tdsbook/:businessid/:fyid" component={AddTDSBook} />
                        <Route exact path="/business/businessdetail/assetaccount/:businessid/" component={AddAssetAccount} />
                        <Route exact path="/business/businessdetail/loanopeningbalance/:businessid/:fyid" component={ConfigureLoanOpeningBalance} />
                        <Route exact path="/business/businessdetail/assetopeningbalance/:businessid/:fyid" component={ConfigureAssetOpeningBalance} />
                        <Route exact path="/business/businessdetail/investmentopeningbalance/:businessid/:fyid" component={ConfigureInvestementOpeningBalance} />
                        <Route exact path="/business/businessdetail/employeeopeningbalance/:businessid/:fyid" component={ConfigureEmployeeOpeningBalance} />
                        <Route exact path="/business/businessdetail/assetbook/:businessid/:fyid" component={AddAssetBook} />
                        <Route exact path="/business/businessdetail/accountreceivables/:businessid/:fyid" component={AddAccountReceivables} />
                        <Route exact path="/business/businessdetail/accountpayables/:businessid/:fyid" component={AddAccountPayables} />
                        <Route exact path="/business/businessdetail/investmentaccount/:businessid/" component={AddInvestmentAccount} />
                        <Route exact path="/business/businessdetail/tdsopeningbalance/:businessid/:fyid" component={ConfigureTDSOpeningBalance} />
                        <Route exact path="/business/businessdetail/trialbalance/:businessid/:fyid" component={AddTrialBalance} />
                        <Route exact path="/business/businessdetail/rcopeningbalance/:businessid/:fyid" component={ConfigureRCOpeningBalance} />
                        <Route exact path="/business/businessdetail/inventory/:businessid/:fyid" component={AddInventory} />
                        <Route exact path="/business/businessdetail/viewstock/:businessid/:fyid" component={AddStock} />
                        <Route exact path="/business/businessdetail/stockopeningbalance/:businessid/:fyid" component={ConfigureStockOpeningBalance} />
                        <Route exact path="/business/businessdetail/passstockjournal/:businessid/:fyid" component={PassStockJournal} />
                        <Route exact path="/business/businessdetail/incometaxbook/:businessid/:fyid" component={AddIncomeTaxBook} />
                        <Route exact path="/business/businessdetail/incometaxopeningbalance/:businessid/:fyid" component={ConfigureIncomeTaxOpeningBalance} />
                        <Route exact path="/business/businessdetail/investmentaccountchange/:businessid/:id" component={AddInvestmentAccountChange} />
                        <Redirect to="/" />
                    </Switch>
                </React.Suspense>

            </div >
        );
    }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Main));