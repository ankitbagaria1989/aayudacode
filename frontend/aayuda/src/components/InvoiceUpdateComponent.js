import React, { Component } from 'react';
import cookie from 'react-cookie';
import { Redirect } from 'react-router-dom';
import { Button } from 'reactstrap';
import { LocalForm, Control } from 'react-redux-form';
import { Header } from './HeaderComponent';
import { Footer } from './Footer';

const RenderItemComponent = function ({ disabled, state, businessid, businessList, getItem, getCustomer, handleInputChange, handleOnBlur, handleOnBlurMiniData }) {
    let itemRows = [];
    for (let x = 0; x <= state.itemno; x++) {
        let n = x;
        let billingunit = '';
        let taxrow = [];
        if (state['itemname' + n] !== '') {
            let it = JSON.parse(state['itemname' + n]);
            billingunit = it.billingunit;
            taxrow.push(
                <div className="form-group mb-2">
                    <div className="">
                        <Control.text model={".basic" + n} className="form-control form-control-sm" id={"basic" + n} name={"basic" + n} placeholder='Basic' onChange={handleInputChange} value={state["basic" + n]} onBlur={handleOnBlurMiniData} />
                    </div>
                </div>
            )
            let customer = getCustomer.data.filter(v => v.customerid === state.customerid)[0];
            let business = businessList.data.filter(b => b.businessid === businessid)[0];
            if (state.customername === "cash") {
                taxrow.push(
                    <div>
                        <div className="form-group mb-2">
                            <div className="">
                                <Control.text model={".cgst" + n} className="form-control form-control-sm" id={"cgst" + n} name={"cgst" + n} placeholder={"CGST @ " + (it.gstrate / 2)} onChange={handleInputChange} value={state["cgst" + n]} onBlur={handleOnBlurMiniData} />
                            </div>
                        </div>
                        <div className="form-group mb-2">
                            <div className="">
                                <Control.text model={".sgst" + n} className="form-control form-control-sm" id={"sgst" + n} name={"sgst" + n} placeholder={"SGST @ " + (it.gstrate / 2)} onChange={handleInputChange} value={state["sgst" + n]} onBlur={handleOnBlurMiniData} />
                            </div>
                        </div>
                    </div>
                )
            }
            else if (customer !== undefined && customer.gstin.substr(0, 2) === business.gstin.substr(0, 2)) {
                taxrow.push(
                    <div>
                        <div className="form-group mb-2">
                            <div className="">
                                <Control.text model={".cgst" + n} className="form-control form-control-sm" id={"cgst" + n} name={"cgst" + n} placeholder={"CGST @ " + (it.gstrate / 2)} onChange={handleInputChange} value={state["cgst" + n]} onBlur={handleOnBlurMiniData} />
                            </div>
                        </div>
                        <div className="form-group mb-2">
                            <div className="">
                                <Control.text model={".sgst" + n} className="form-control form-control-sm" id={"sgst" + n} name={"sgst" + n} placeholder={"SGST @ " + (it.gstrate / 2)} onChange={handleInputChange} value={state["sgst" + n]} onBlur={handleOnBlurMiniData} />
                            </div>
                        </div>
                    </div>
                )
            }
            else if (customer !== undefined && customer.gstin.substr(0, 2) !== business.gstin.substr(0, 2)) {
                taxrow.push(
                    <div className="form-group mb-2">
                        <div className="">
                            <Control.text model={".igst" + n} className="form-control form-control-sm" id={"igst" + n} name={"igst" + n} placeholder={"IGST @ " + it.gstrate} onChange={handleInputChange} value={state["igst" + n]} onBlur={handleOnBlurMiniData} />
                        </div>
                    </div>
                )
            }

            taxrow.push(
                <div className="form-group mb-2">
                    <div className="">
                        <Control.text model={".total" + n} className="form-control form-control-sm" id={"total" + n} name={"total" + n} placeholder='Total ' onChange={handleInputChange} value={state["total" + n]} onBlur={handleOnBlurMiniData} />
                    </div>
                </div>
            )
        }
        itemRows.push(
            <div className="form-group col-12 col-md-3 mb-1 mt-2">
                <div className="border border-danger p-1 ml-0">
                    <label htmlFor={"itemname" + n} className="mb-0 muted-text">({parseFloat(x) + 1}) Item Details</label>
                    <Control.select model={".itemname" + n} className="form-control form-control-sm" id={"itemname" + n} name={"itemname" + n} onChange={handleInputChange} value={state['itemname' + n]}>
                        <Option list={getItem.data} scope="item" />
                    </Control.select>
                    <div className="form-group mb-1 mt-1">
                        <div className="">
                            <Control.textarea model={'.itemdescription' + n} rows="2" className="form-control form-control-sm" id={'itemdescription' + n} name={'itemdescription' + n} value={state['itemdescription' + n]} onChange={handleInputChange} placeholder="Item Description" />
                        </div>
                    </div>
                    <div className="form-group mb-1">
                        <div className="">
                            <Control.text model={'qty' + n} className="form-control form-control-sm" id={'qty' + n} name={'qty' + n} onChange={handleInputChange} placeholder={'Qty In ' + billingunit} value={state['qty' + n]} onBlur={handleOnBlur} />
                        </div>
                    </div>
                    <div className="form-group mb-1">
                        <div className="">
                            <Control.text model={'.price' + n} className="form-control form-control-sm" id={'price' + n} name={'price' + n} onChange={handleInputChange} placeholder={'Price per ' + billingunit} value={state['price' + n]} onBlur={handleOnBlur} />
                        </div>
                    </div>
                    {taxrow}
                </div>
            </div>
        );
    }
    return itemRows;
}
const Option = function ({ list, scope }) {
    let length = list.length;
    let arr = [];
    if (scope === "customer") {
        for (let i = -1; i < length; i++) {
            if (i === -1) arr.push(<option value=''>Select Billed To :</option>);
            else arr.push(<option value={list[i].customerid + "..." + list[i].businessname}>{list[i].businessname}</option>);
        }
    }

    else if (scope === "shipped") {
        for (let i = -1; i < length; i++) {
            if (i === -1) arr.push(<option value=''>Select Shipped To :</option>);
            else arr.push(<option value={list[i].customerid + "..." + list[i].businessname}>{list[i].businessname}</option>);
        }
    }

    else if (scope === "item") {
        for (let i = -1; i < length; i++) {
            if (i === -1) arr.push(<option value=''>Select Item</option>);
            else arr.push(<option value={JSON.stringify(list[i])}>{list[i].alias ? list[i].alias : list[i].itemname}</option>);
        }
    }
    return arr;
}

class InvoiceUpdate extends Component {
    constructor(props) {
        super(props);
        this.state = {
            invoiceid: '',
            itemno: '',
            customerid: '',
            shippedtoid: '',
            invoicedate: '',
            invoicenumber: '',
            ponumber: '',
            podate: '',
            transporter: '',
            vehiclenumber: '',
            grno: '',
            destination: '',
            ewaybillnumber: '',
            freight: '',
            itemname0: '',
            itemdescription0: '',
            qty0: '',
            price0: '',
            basic0: '',
            igst0: '',
            cgst0: '',
            sgst0: '',
            excise0: '',
            vat0: '',
            total0: '',
            itemname1: '',
            itemdescription1: '',
            qty1: '',
            price1: '',
            basic1: '',
            igst1: '',
            cgst1: '',
            sgst1: '',
            excise1: '',
            vat1: '',
            total1: '',
            itemname2: '',
            itemdescription2: '',
            qty2: '',
            price2: '',
            basic2: '',
            igst2: '',
            cgst2: '',
            sgst2: '',
            excise2: '',
            vat2: '',
            total2: '',
            itemname3: '',
            itemdescription3: '',
            qty3: '',
            price3: '',
            basic3: '',
            igst3: '',
            cgst3: '',
            sgst3: '',
            excise3: '',
            vat3: '',
            total3: '',
            itemname4: '',
            itemdescription4: '',
            qty4: '',
            price4: '',
            basic4: '',
            igst4: '',
            cgst4: '',
            sgst4: '',
            excise4: '',
            vat4: '',
            total4: '',
            itemname5: '',
            itemdescription5: '',
            qty5: '',
            price5: '',
            basic5: '',
            igst5: '',
            cgst5: '',
            sgst5: '',
            excise5: '',
            vat5: '',
            total5: '',
            itemname6: '',
            itemdescription6: '',
            qty6: '',
            price6: '',
            basic6: '',
            igst6: '',
            cgst6: '',
            sgst6: '',
            excise6: '',
            vat6: '',
            total6: '',
            itemname7: '',
            itemdescription7: '',
            qty7: '',
            price7: '',
            basic7: '',
            igst7: '',
            cgst7: '',
            sgst7: '',
            excise7: '',
            vat7: '',
            total7: '',
            itemname8: '',
            itemdescription8: '',
            qty8: '',
            price8: '',
            basic8: '',
            igst8: '',
            cgst8: '',
            sgst8: '',
            excise8: '',
            vat8: '',
            total8: '',
            itemname9: '',
            itemdescription9: '',
            qty9: '',
            price9: '',
            basic9: '',
            igst9: '',
            cgst9: '',
            sgst9: '',
            excise9: '',
            vat9: '',
            total9: '',
            terms: '',
            totalamountbasic: '',
            totaligst: '',
            totalcgst: '',
            totalsgst: '',
            totalvat: '',
            totalexcise: '',
            grandtotal: '',
            cash: false,
            customername: '',
            shippedtoname: '',
            blamount: 0
        }
    }

    componentDidMount() {
        if (this.props.invoiceUpdateMessage.message === 'success') {
            alert("Invoice Updated Successfully");
            this.props.resetInvoiceUpdateMessage();
        }
        if ((this.props.salesDetail.message === 'initial') && (this.props.salesDetail.loading === false)) this.props.getSalesData(this.props.businessid, this.props.fyid);
        if ((this.props.getCustomer.message === 'initial') && (this.props.getCustomer.loading === false)) this.props.getCustomerList(this.props.businessid);
        if ((this.props.getItem.message === 'initial') && (this.props.getItem.loading === false)) this.props.getItemList(this.props.businessid);
        if (this.props.invoiceUpdateMessage.error !== '') {
            alert(this.props.invoiceUpdateMessage.error);
            this.props.resetInvoiceUpdateMessage();
        }
        if (this.props.logoutMessage.error !== '') {
            alert("Error Logging Out. Please Try Again");
            this.props.resetLogoutMessage();
        }
        if (this.props.salesDetail.data.length > 0) {
            let invoiceDetail = this.props.salesDetail.data.filter(d => {
                if ((d.businessid === this.props.businessid) && (d.financialyearid === this.props.fyid) && (d.invoiceid === this.props.invoiceid)) {
                    return d;
                }
            })[0];
            if (invoiceDetail) {
                let invno = invoiceDetail.invoicenumber;
                let invnum = invno.toString();
                if (invnum.length < 3) {
                    let l = 3 - invnum.length;
                    for (let i = 0; i < l; i++) {
                        invnum = '0' + invnum;
                    }
                }
                invoiceDetail.invoicenumber = invnum;
                let invoiceInfo = {
                    cash: invoiceDetail.cash,
                    customername: invoiceDetail.customername,
                    shippedtoname: invoiceDetail.shippedtoname,
                    invoiceid: invoiceDetail.invoiceid,
                    itemno: invoiceDetail.itemno,
                    customerid: invoiceDetail.customerid + "..." + invoiceDetail.customername,
                    shippedtoid: invoiceDetail.shippedtoid + "..." + invoiceDetail.shippedtoname,
                    invoicedate: invoiceDetail.invoicedate,
                    invoicenumber: invoiceDetail.invoicenumber,
                    ponumber: invoiceDetail.ponumber,
                    podate: invoiceDetail.podate,
                    transporter: invoiceDetail.transporter,
                    vehiclenumber: invoiceDetail.vehiclenumber,
                    grno: invoiceDetail.grno,
                    destination: invoiceDetail.destination,
                    ewaybillnumber: invoiceDetail.ewaybillnumber,
                    freight: invoiceDetail.freight,
                    terms: invoiceDetail.terms,
                    totalamountbasic: invoiceDetail.totalbasic,
                    totaligst: invoiceDetail.totaligst,
                    totalcgst: invoiceDetail.totalcgst,
                    totalsgst: invoiceDetail.totalsgst,
                    totalvat: invoiceDetail.totalvat,
                    totalexcise: invoiceDetail.totalexcise,
                    grandtotal: invoiceDetail.grandtotal,
                    blamount: invoiceDetail.blamount
                }
                if (invoiceDetail.itemobject !== '') {
                    let itemObj = JSON.parse(invoiceDetail.itemobject);
                    let keys = Object.keys(itemObj);
                    if (keys.length > 0) {
                        keys.forEach((k, i) => {
                            invoiceInfo = {
                                ...invoiceInfo,
                                ['itemname' + k]: itemObj[k].name,
                                ['itemdescription' + k]: itemObj[k].desc,
                                ['qty' + k]: itemObj[k].qty,
                                ['price' + k]: itemObj[k].price,
                                ['basic' + k]: itemObj[k].basic,
                                ['igst' + k]: itemObj[k].igst,
                                ['cgst' + k]: itemObj[k].cgst,
                                ['sgst' + k]: itemObj[k].sgst,
                                ['total' + k]: itemObj[k].total
                            }
                        })
                    }
                }
                this.setState({
                    invoiceid: invoiceInfo.invoiceid,
                    cash: invoiceInfo.cash,
                    customername: invoiceInfo.customername,
                    shippedtoname: invoiceInfo.shippedtoname,
                    itemno: invoiceInfo.itemno,
                    customerid: invoiceInfo.customerid,
                    shippedtoid: invoiceInfo.shippedtoid,
                    invoicedate: invoiceInfo.invoicedate,
                    invoicenumber: invoiceInfo.invoicenumber,
                    ponumber: invoiceInfo.ponumber ? invoiceInfo.ponumber : '',
                    podate: invoiceInfo.podate ? invoiceInfo.podate : '',
                    transporter: invoiceInfo.transporter ? invoiceInfo.transporter : '',
                    vehiclenumber: invoiceInfo.vehiclenumber ? invoiceInfo.vehiclenumber : '',
                    grno: invoiceInfo.grno ? invoiceInfo.grno : '',
                    destination: invoiceInfo.destination ? invoiceInfo.destination : '',
                    ewaybillnumber: invoiceInfo.ewaybillnumber,
                    freight: invoiceInfo.freight ? invoiceInfo.freight : 0,
                    itemname0: invoiceInfo.itemname0,
                    itemdescription0: invoiceInfo.itemdescription0,
                    qty0: invoiceInfo.qty0 ? invoiceInfo.qty0 : '',
                    price0: invoiceInfo.price0 ? invoiceInfo.price0 : '',
                    basic0: invoiceInfo.basic0 ? invoiceInfo.basic0 : '',
                    igst0: invoiceInfo.igst0 ? invoiceInfo.igst0 : '',
                    cgst0: invoiceInfo.cgst0 ? invoiceInfo.cgst0 : '',
                    sgst0: invoiceInfo.sgst0 ? invoiceInfo.sgst0 : '',
                    excise0: invoiceInfo.excise0 ? invoiceInfo.excise0 : '',
                    vat0: invoiceInfo.vat0 ? invoiceInfo.vat0 : '',
                    total0: invoiceInfo.total0 ? invoiceInfo.total0 : '',
                    itemname1: invoiceInfo.itemname1,
                    itemdescription1: invoiceInfo.itemdescription1,
                    qty1: invoiceInfo.qty1 ? invoiceInfo.qty1 : '',
                    price1: invoiceInfo.price1 ? invoiceInfo.price1 : '',
                    basic1: invoiceInfo.basic1 ? invoiceInfo.basic1 : '',
                    igst1: invoiceInfo.igst1 ? invoiceInfo.igst1 : '',
                    cgst1: invoiceInfo.cgst1 ? invoiceInfo.cgst1 : '',
                    sgst1: invoiceInfo.sgst1 ? invoiceInfo.sgst1 : '',
                    excise1: invoiceInfo.escise1 ? invoiceInfo.escise1 : '',
                    vat1: invoiceInfo.vat1 ? invoiceInfo.vat1 : '',
                    total1: invoiceInfo.total1 ? invoiceInfo.total1 : '',
                    itemname2: invoiceInfo.itemname2,
                    itemdescription2: invoiceInfo.itemdescription2,
                    qty2: invoiceInfo.qty2 ? invoiceInfo.qty2 : '',
                    price2: invoiceInfo.price2 ? invoiceInfo.price2 : '',
                    basic2: invoiceInfo.basic2 ? invoiceInfo.basic2 : '',
                    igst2: invoiceInfo.igst2 ? invoiceInfo.igst2 : '',
                    cgst2: invoiceInfo.cgst2 ? invoiceInfo.cgst2 : '',
                    sgst2: invoiceInfo.sgst2 ? invoiceInfo.sgst2 : '',
                    excise2: invoiceInfo.excise2 ? invoiceInfo.excise2 : '',
                    vat2: invoiceInfo.vat2 ? invoiceInfo.vat2 : '',
                    total2: invoiceInfo.total2 ? invoiceInfo.total2 : '',
                    itemname3: invoiceInfo.itemname3,
                    itemdescription3: invoiceInfo.itemdescription3,
                    qty3: invoiceInfo.qty3 ? invoiceInfo.qty3 : '',
                    price3: invoiceInfo.price3 ? invoiceInfo.price3 : '',
                    basic3: invoiceInfo.basic3 ? invoiceInfo.basic3 : '',
                    igst3: invoiceInfo.igst3 ? invoiceInfo.igst3 : '',
                    cgst3: invoiceInfo.cgst3 ? invoiceInfo.cgst3 : '',
                    sgst3: invoiceInfo.sgst3 ? invoiceInfo.sgst3 : '',
                    excise3: invoiceInfo.excise3 ? invoiceInfo.excise3 : '',
                    vat3: invoiceInfo.vat3 ? invoiceInfo.vat3 : '',
                    total3: invoiceInfo.total3 ? invoiceInfo.total3 : '',
                    itemname4: invoiceInfo.itemname4,
                    itemdescription4: invoiceInfo.itemdescription4,
                    qty4: invoiceInfo.qty4 ? invoiceInfo.qty4 : '',
                    price4: invoiceInfo.price4 ? invoiceInfo.price4 : '',
                    basic4: invoiceInfo.basic4 ? invoiceInfo.basic4 : '',
                    igst4: invoiceInfo.igst4 ? invoiceInfo.igst4 : '',
                    cgst4: invoiceInfo.cgst4 ? invoiceInfo.cgst4 : '',
                    sgst4: invoiceInfo.sgst4 ? invoiceInfo.sgst4 : '',
                    excise4: invoiceInfo.excise4 ? invoiceInfo.excise4 : '',
                    vat4: invoiceInfo.vat4 ? invoiceInfo.vat4 : '',
                    total4: invoiceInfo.total4 ? invoiceInfo.total4 : '',
                    itemname5: invoiceInfo.itemname5,
                    itemdescription5: invoiceInfo.itemdescription5,
                    qty5: invoiceInfo.qty5 ? invoiceInfo.qty5 : '',
                    price5: invoiceInfo.price5 ? invoiceInfo.price5 : '',
                    basic5: invoiceInfo.basic5 ? invoiceInfo.basic5 : '',
                    igst5: invoiceInfo.igst5 ? invoiceInfo.igst5 : '',
                    cgst5: invoiceInfo.cgst5 ? invoiceInfo.cgst5 : '',
                    sgst5: invoiceInfo.sgst5 ? invoiceInfo.sgst5 : '',
                    excise5: invoiceInfo.excise5 ? invoiceInfo.excise5 : '',
                    vat5: invoiceInfo.vat5 ? invoiceInfo.vat5 : '',
                    total5: invoiceInfo.total5 ? invoiceInfo.total5 : '',
                    itemname6: invoiceInfo.itemname6,
                    itemdescription6: invoiceInfo.itemdescription6,
                    qty6: invoiceInfo.qty6 ? invoiceInfo.qty6 : '',
                    price6: invoiceInfo.price6 ? invoiceInfo.price6 : '',
                    basic6: invoiceInfo.basic6 ? invoiceInfo.basic6 : '',
                    igst6: invoiceInfo.igst6 ? invoiceInfo.igst6 : '',
                    cgst6: invoiceInfo.cgst6 ? invoiceInfo.cgst6 : '',
                    sgst6: invoiceInfo.sgst6 ? invoiceInfo.sgst6 : '',
                    excise6: invoiceInfo.excise6 ? invoiceInfo.excise6 : '',
                    vat6: invoiceInfo.vat6 ? invoiceInfo.vat6 : '',
                    total6: invoiceInfo.total6 ? invoiceInfo.total6 : '',
                    itemname7: invoiceInfo.itemname7,
                    itemdescription7: invoiceInfo.itemdescription7,
                    qty7: invoiceInfo.qty7 ? invoiceInfo.qty7 : '',
                    price7: invoiceInfo.price7 ? invoiceInfo.price7 : '',
                    basic7: invoiceInfo.basic7 ? invoiceInfo.basic7 : '',
                    igst7: invoiceInfo.igst7 ? invoiceInfo.igst7 : '',
                    cgst7: invoiceInfo.cgst7 ? invoiceInfo.cgst7 : '',
                    sgst7: invoiceInfo.sgst7 ? invoiceInfo.sgst7 : '',
                    excise7: invoiceInfo.excise7 ? invoiceInfo.excise7 : '',
                    vat7: invoiceInfo.vat7 ? invoiceInfo.vat7 : '',
                    total7: invoiceInfo.total7 ? invoiceInfo.total7 : '',
                    itemname8: invoiceInfo.itemname8,
                    itemdescription8: invoiceInfo.itemdescription8,
                    qty8: invoiceInfo.qty8 ? invoiceInfo.qty8 : '',
                    price8: invoiceInfo.price8 ? invoiceInfo.price8 : '',
                    basic8: invoiceInfo.basic8 ? invoiceInfo.basic8 : '',
                    igst8: invoiceInfo.igst8 ? invoiceInfo.igst8 : '',
                    cgst8: invoiceInfo.cgst8 ? invoiceInfo.cgst8 : '',
                    sgst8: invoiceInfo.sgst8 ? invoiceInfo.sgst8 : '',
                    excise8: invoiceInfo.excise8 ? invoiceInfo.excise8 : '',
                    vat8: invoiceInfo.vat8 ? invoiceInfo.vat8 : '',
                    total8: invoiceInfo.total8 ? invoiceInfo.total8 : '',
                    itemname9: invoiceInfo.itemname9,
                    itemdescription9: invoiceInfo.itemdescription9,
                    qty9: invoiceInfo.qty9 ? invoiceInfo.qty9 : '',
                    price9: invoiceInfo.price9 ? invoiceInfo.price9 : '',
                    basic9: invoiceInfo.basic9 ? invoiceInfo.basic9 : '',
                    igst9: invoiceInfo.igst9 ? invoiceInfo.igst9 : '',
                    cgst9: invoiceInfo.cgst9 ? invoiceInfo.cgst9 : '',
                    sgst9: invoiceInfo.sgst9 ? invoiceInfo.sgst9 : '',
                    excise9: invoiceInfo.excise9 ? invoiceInfo.excise9 : '',
                    vat9: invoiceInfo.vat9 ? invoiceInfo.vat9 : '',
                    total9: invoiceInfo.total9 ? invoiceInfo.total9 : '',
                    terms: invoiceInfo.terms,
                    blamount: invoiceInfo.blamount ? invoiceInfo.blamount : 0,
                    totalamountbasic: parseFloat(invoiceInfo.totalamountbasic),
                    totaligst: invoiceInfo.totaligst ? parseFloat(invoiceInfo.totaligst) : 0,
                    totalcgst: invoiceInfo.totalcgst ? parseFloat(invoiceInfo.totalcgst) : 0,
                    totalsgst: invoiceInfo.totalsgst ? parseFloat(invoiceInfo.totalsgst) : 0,
                    totalvat: invoiceInfo.totalvat ? parseFloat(invoiceInfo.totalvat) : 0,
                    totalexcise: invoiceInfo.totalexcise ? parseFloat(invoiceInfo.totalexcise) : 0,
                    grandtotal: invoiceInfo.grandtotal ? parseFloat(invoiceInfo.grandtotal) : 0
                }, () => console.log(JSON.stringify(this.state)))
            }
        }
    }

    addInvoiceItem = () => {
        let count = this.state.itemno + 1;
        this.setState({
            ...this.state,
            itemno: count
        })
    }

    handleInputChange = (event) => {
        let target = event.target;
        let name = target.name;
        let value = target.value;
        let index = name.charAt(name.length - 1);
        if (/^cash/i.test(name)) {
            if (this.state.cash === 0) {
                this.setState({
                    ...this.state,
                    cash: 1,
                    shippedtoname: "cash",
                    customername: "cash"
                })
            }
            else {
                this.setState({
                    ...this.state,
                    cash: 0,
                    shippedtoname: "",
                    customername: "",
                })
            }
        }
        else if (/^price/i.test(name)) {
            if ((this.state['itemname' + index] === '') || (this.state['qty' + index] === '') || (this.state.customername === '')) alert("Please select Customer, Item  and fill in Quantity before filling in price");
            else {
                this.setState({
                    ...this.state,
                    [name]: value
                })
            }
        }
        else if (/^shippedtoid/i.test(name)) {
            this.setState({
                ...this.state,
                shippedtoid: value,
                shippedtoname: value.split("...")[1],
            })
        }
        else if (/^customerid/i.test(name)) {
            this.setState({
                isModalOpen: false,
                itemno: 0,
                cash: false,
                customerid: value,
                customername: value.split("...")[1],
                shippedtoid: '',
                invoicedate: this.props.salesDetail.invoicedate,
                invoicenumber: this.props.salesDetail.invoicenumber,
                ponumber: '',
                podate: '',
                transporter: '',
                vehiclenumber: '',
                grno: '',
                destination: '',
                ewaybillnumber: '',
                freight: 0,
                itemname0: '',
                itemdescription0: '',
                qty0: '',
                price0: '',
                basic0: '',
                igst0: '',
                cgst0: '',
                sgst0: '',
                excise0: '',
                vat0: '',
                total0: '',
                itemname1: '',
                itemdescription1: '',
                qty1: '',
                price1: '',
                basic1: '',
                igst1: '',
                cgst1: '',
                sgst1: '',
                excise1: '',
                vat1: '',
                total1: '',
                itemname2: '',
                itemdescription2: '',
                qty2: '',
                price2: '',
                basic2: '',
                igst2: '',
                cgst2: '',
                sgst2: '',
                excise2: '',
                vat2: '',
                total2: '',
                itemname3: '',
                itemdescription3: '',
                qty3: '',
                price3: '',
                basic3: '',
                igst3: '',
                cgst3: '',
                sgst3: '',
                excise3: '',
                vat3: '',
                total3: '',
                itemname4: '',
                itemdescription4: '',
                qty4: '',
                price4: '',
                basic4: '',
                igst4: '',
                cgst4: '',
                sgst4: '',
                excise4: '',
                vat4: '',
                total4: '',
                itemname5: '',
                itemdescription5: '',
                qty5: '',
                price5: '',
                basic5: '',
                igst5: '',
                cgst5: '',
                sgst5: '',
                excise5: '',
                vat5: '',
                total5: '',
                itemname6: '',
                itemdescription6: '',
                qty6: '',
                price6: '',
                basic6: '',
                igst6: '',
                cgst6: '',
                sgst6: '',
                excise6: '',
                vat6: '',
                total6: '',
                itemname7: '',
                itemdescription7: '',
                qty7: '',
                price7: '',
                basic7: '',
                igst7: '',
                cgst7: '',
                sgst7: '',
                excise7: '',
                vat7: '',
                total7: '',
                itemname8: '',
                itemdescription8: '',
                qty8: '',
                price8: '',
                basic8: '',
                igst8: '',
                cgst8: '',
                sgst8: '',
                excise8: '',
                vat8: '',
                total8: '',
                itemname9: '',
                itemdescription9: '',
                qty9: '',
                price9: '',
                basic9: '',
                igst9: '',
                cgst9: '',
                sgst9: '',
                excise9: '',
                vat9: '',
                total9: '',
                terms: '',
                totalamountbasic: 0,
                totaligst: 0,
                totalcgst: 0,
                totalsgst: 0,
                totalvat: 0,
                totalexcise: 0,
                grandtotal: 0,
                touched: false,
                blamount: 0
            })
        }
        else if (/^itemname/i.test(name)) {
            let totalbasic = 0, totaligst = 0, totalcgst = 0, totalsgst = 0, grandtotal = 0, totalvat = 0, totalexcise = 0;
            for (let i = 0; i < 10; i++) {
                if (parseFloat(index) !== i) {
                    if (this.state['basic' + i] !== '') totalbasic = totalbasic + parseFloat(this.state['basic' + i]);
                    if (this.state['igst' + i] !== '') totaligst = totaligst + parseFloat(this.state['igst' + i]);
                    if (this.state['sgst' + i] !== '') totalcgst = totalsgst + parseFloat(this.state['sgst' + i]);
                    if (this.state['cgst' + i] !== '') totalcgst = totalcgst + parseFloat(this.state['cgst' + i]);
                    if (this.state['total' + i] !== '') grandtotal = grandtotal + parseFloat(this.state['total' + i]);
                    if (this.state['vat' + i] !== '') totalvat = totalvat + parseFloat(this.state['vat' + i]);
                    if (this.state['excise' + i] !== '') totalexcise = totalexcise + parseFloat(this.state['excise' + i]);
                }
            }
            this.setState({
                ...this.state,
                [name]: value,
                ['qty' + index]: '',
                ['price' + index]: '',
                ['basic' + index]: '',
                ['igst' + index]: '',
                ['sgst' + index]: '',
                ['cgst' + index]: '',
                ['vat' + index]: '',
                ['excise' + index]: '',
                ['total' + index]: '',
                ['totalamountbasic']: parseFloat(totalbasic).toFixed(2),
                ['totaligst']: parseFloat(totaligst).toFixed(2),
                ['totalcgst']: parseFloat(totalcgst).toFixed(2),
                ['totalsgst']: parseFloat(totalsgst).toFixed(2),
                ['totalvat']: parseFloat(totalvat).toFixed(2),
                ['totalexcise']: parseFloat(totalexcise).toFixed(2),
                ['grandtotal']: parseFloat(parseFloat(grandtotal) + parseFloat(this.state.freight)).toFixed(2)
            })
        }
        else {
            this.setState({
                ...this.state,
                [name]: value
            })
        }
    }

    handleOnBlur = (event) => {
        let target = event.target;
        let name = target.name;
        let value = target.value;
        if (value > 0) value = parseFloat(value).toFixed(2);
        let index = parseFloat(name.charAt(name.length - 1));
        if ((/^price/i.test(name)) || (/^qty/i.test(name))) {
            if ((this.state['price' + index] === '') || (this.state['qty' + index] === '')) {
                let totalbasic = 0, totaligst = 0, totalsgst = 0, totalcgst = 0, grandtotal = 0, totalvat = 0, totalexcise = 0;
                for (let i = 0; i < 10; i++) {
                    if (index !== i) {
                        if (this.state['basic' + i] !== '') totalbasic = totalbasic + parseFloat(this.state['basic' + i]);
                        if (this.state['igst' + i] !== '') totaligst = totaligst + parseFloat(this.state['igst' + i]);
                        if (this.state['sgst' + i] !== '') totalsgst = totalsgst + parseFloat(this.state['sgst' + i]);
                        if (this.state['cgst' + i] !== '') totalcgst = totalcgst + parseFloat(this.state['cgst' + i]);
                        if (this.state['total' + i] !== '') grandtotal = grandtotal + parseFloat(this.state['total' + i]);
                        if (this.state['vat' + i] !== '') totalvat = totalvat + parseFloat(this.state['vat' + i]);
                        if (this.state['excise' + i] !== '') totalexcise = totalexcise + parseFloat(this.state['excise' + i]);
                    }
                }
                this.setState({
                    ...this.state,
                    ['basic' + index]: '',
                    ['igst' + index]: '',
                    ['cgst' + index]: '',
                    ['sgst' + index]: '',
                    ['vat' + index]: '',
                    ['totalamountbasic']: parseFloat(totalbasic).toFixed(2),
                    ['totaligst']: parseFloat(totaligst).toFixed(2),
                    ['totalcgst']: parseFloat(totalcgst).toFixed(2),
                    ['totalsgst']: parseFloat(totalsgst).toFixed(2),
                    ['totalvat']: parseFloat(totalvat).toFixed(2),
                    ['totalexcise']: parseFloat(totalexcise).toFixed(2),
                    ['grandtotal']: parseFloat(grandtotal + parseFloat(this.state.freight)).toFixed(2)
                })
            }
        }
        if ((this.state['qty' + index] !== '') && (this.state['itemname' + index] !== '') && (this.state['price' + index] !== '') && ((this.state.customerid !== "") || (this.state.customername === 'cash'))) {
            let totalbasic = 0;
            let it = JSON.parse(this.state['itemname' + index]);
            if (it.taxtype === 'gst') {
                let totalbasic = 0, grandtotal = 0, totaligst = 0, totalcgst = 0, totalsgst = 0;
                let customer = this.props.getCustomer.data.filter(v => v.customerid === this.state.customerid.split("...")[0])[0];
                let business = this.props.businessList.data.filter(b => b.businessid === this.props.businessid)[0];
                for (let i = 0; i <= 9; i++) {
                    if (i !== index) {
                        if (this.state['basic' + i] !== '') totalbasic = totalbasic + parseFloat(this.state['basic' + i]);
                        if (this.state['igst' + i] !== '') totaligst = totaligst + parseFloat(this.state['igst' + i]);
                        if (this.state['sgst' + i] !== '') totalsgst = totalsgst + parseFloat(this.state['sgst' + i]);
                        if (this.state['cgst' + i] !== '') totalcgst = totalcgst + parseFloat(this.state['cgst' + i]);
                        if (this.state['total' + i] !== '') grandtotal = grandtotal + parseFloat(this.state['total' + i]);
                    }
                }
                if (this.state.customername === 'cash') {
                    this.setState({
                        ...this.state,
                        ['basic' + index]: (parseFloat(this.state['qty' + index]) * parseFloat(this.state['price' + index])).toFixed(2),
                        ['igst' + index]: '',
                        ['sgst' + index]: (parseFloat(this.state['qty' + index]) * parseFloat(this.state['price' + index]) * (parseFloat(it.gstrate) / 200)).toFixed(2),
                        ['cgst' + index]: (parseFloat(this.state['qty' + index]) * parseFloat(this.state['price' + index]) * (parseFloat(it.gstrate) / 200)).toFixed(2),
                        ['total' + index]: ((parseFloat(this.state['qty' + index]) * parseFloat(this.state['price' + index])) + (parseFloat(this.state['qty' + index]) * parseFloat(this.state['price' + index]) * (parseFloat(it.gstrate) / 100))).toFixed(2),
                        ['totalamountbasic']: (parseFloat(this.state['qty' + index]) * parseFloat(this.state['price' + index]) + parseFloat(totalbasic)).toFixed(2),
                        ['totaligst']: parseFloat(totaligst).toFixed(2),
                        ['totalcgst']: ((parseFloat(this.state['qty' + index]) * parseFloat(this.state['price' + index]) * (parseFloat(it.gstrate) / 200)) + totalcgst).toFixed(2),
                        ['totalsgst']: ((parseFloat(this.state['qty' + index]) * parseFloat(this.state['price' + index]) * (parseFloat(it.gstrate) / 200)) + totalsgst).toFixed(2),
                        ['grandtotal']: (grandtotal + (parseFloat(this.state['qty' + index]) * parseFloat(this.state['price' + index])) + (parseFloat(this.state['qty' + index]) * parseFloat(this.state['price' + index]) * parseFloat(it.gstrate) / 100) + parseFloat(this.state.freight)).toFixed(2)
                    }, () => console.log(JSON.stringify(this.state)))
                }
                else {
                    this.setState({
                        ...this.state,
                        ['basic' + index]: (parseFloat(this.state['qty' + index]) * parseFloat(this.state['price' + index])).toFixed(2),
                        ['igst' + index]: (customer.gstin.substr(0, 2) !== '' && customer.gstin.substr(0, 2) !== business.gstin.substr(0, 2)) ? (parseFloat(this.state['qty' + index]) * parseFloat(this.state['price' + index]) * (parseFloat(it.gstrate) / 100)).toFixed(2) : '',
                        ['sgst' + index]: ((customer.gstin.substr(0, 2) === business.gstin.substr(0, 2)) || (customer.gstin === '')) ? (parseFloat(this.state['qty' + index]) * parseFloat(this.state['price' + index]) * (parseFloat(it.gstrate) / 200)).toFixed(2) : '',
                        ['cgst' + index]: ((customer.gstin.substr(0, 2) === business.gstin.substr(0, 2)) || (customer.gstin === '')) ? (parseFloat(this.state['qty' + index]) * parseFloat(this.state['price' + index]) * (parseFloat(it.gstrate) / 200)).toFixed(2) : '',
                        ['total' + index]: ((parseFloat(this.state['qty' + index]) * parseFloat(this.state['price' + index])) + (parseFloat(this.state['qty' + index]) * parseFloat(this.state['price' + index]) * (parseFloat(it.gstrate) / 100))).toFixed(2),
                        ['totalamountbasic']: (parseFloat(this.state['qty' + index]) * parseFloat(this.state['price' + index]) + parseFloat(totalbasic)).toFixed(2),
                        ['totaligst']: (customer.gstin.substr(0, 2) !== '' && customer.gstin.substr(0, 2) !== business.gstin.substr(0, 2)) ? ((parseFloat(this.state['qty' + index]) * parseFloat(this.state['price' + index]) * (parseFloat(it.gstrate) / 100)) + totaligst).toFixed(2) : parseFloat(totaligst).toFixed(2),
                        ['totalcgst']: ((customer.gstin.substr(0, 2) === business.gstin.substr(0, 2)) || (customer.gstin === '')) ? ((parseFloat(this.state['qty' + index]) * parseFloat(this.state['price' + index]) * (parseFloat(it.gstrate) / 200)) + totalcgst).toFixed(2) : parseFloat(totalcgst).toFixed(2),
                        ['totalsgst']: ((customer.gstin.substr(0, 2) === business.gstin.substr(0, 2)) || (customer.gstin === '')) ? ((parseFloat(this.state['qty' + index]) * parseFloat(this.state['price' + index]) * (parseFloat(it.gstrate) / 200)) + totalsgst).toFixed(2) : parseFloat(totalsgst).toFixed(2),
                        ['grandtotal']: (grandtotal + (parseFloat(this.state['qty' + index]) * parseFloat(this.state['price' + index])) + (parseFloat(this.state['qty' + index]) * parseFloat(this.state['price' + index]) * parseFloat(it.gstrate) / 100) + parseFloat(this.state.freight)).toFixed(2)
                    }, () => console.log(JSON.stringify(this.state)))
                }
            }
        }
    }

    handleOnBlurMiniData = (event) => {
        let target = event.target;
        let name = target.name;
        let value = target.value;
        if (value === '') value = 0;
        value = parseFloat(value);
        let index = parseFloat(name.charAt(name.length - 1));
        let n = index;
        let item = this.state['itemname' + index] !== '' ? JSON.parse(this.state['itemname' + index]) : '';
        if ((item !== '') && ((this.state.customerid !== '') || (this.state.customername === 'cash'))) {
            let totalbasic = 0, totaligst = 0, totalcgst = 0, totalsgst = 0, totalvat = 0, totalexcise = 0, grandtotal = 0;
            let customer = this.props.getCustomer.data.filter(b => b.customerid === this.state.customerid.split("...")[0])[0];
            let business = this.props.businessList.data.filter(b => b.businessid === this.props.businessid)[0];
            for (let i = 0; i < 10; i++) {
                if (i !== index) {
                    totalbasic = totalbasic + (this.state['basic' + i] !== '' ? parseFloat(this.state['basic' + i]) : 0);
                    totaligst = totaligst + (this.state['igst' + i] !== '' ? parseFloat(this.state['igst' + i]) : 0);
                    totalcgst = totalcgst + (this.state['cgst' + i] !== '' ? parseFloat(this.state['cgst' + i]) : 0);
                    totalsgst = totalsgst + (this.state['sgst' + i] !== '' ? parseFloat(this.state['sgst' + i]) : 0);
                    totalvat = totalvat + (this.state['vat' + i] !== '' ? parseFloat(this.state['vat' + i]) : 0);
                    totalexcise = totalexcise + (this.state['excise' + i] !== '' ? parseFloat(this.state['excise' + i]) : 0);
                    grandtotal = grandtotal + (this.state['total' + i] !== '' ? parseFloat(this.state['total' + i]) : 0);
                }
            }
            if (/^basic/i.test(name)) {
                if (item.taxtype === 'gst') {
                    if (this.state.customername === "cash") {
                        this.setState({
                            ...this.state,
                            ['igst' + n]: '',
                            ['sgst' + n]: value === 0 ? '' : parseFloat(value * (item.gstrate / 200)).toFixed(2),
                            ['cgst' + n]: value === 0 ? '' : parseFloat(value * (item.gstrate / 200)).toFixed(2),
                            ['total' + n]: value === 0 ? '' : parseFloat(value + (value * item.gstrate / 100)).toFixed(2),
                            ['totalamountbasic']: parseFloat(totalbasic + value).toFixed(2),
                            ['totalcgst']: parseFloat(totalcgst + (value * item.gstrate / 200)).toFixed(2),
                            ['totalsgst']: parseFloat(totalsgst / 2 + (value * item.gstrate / 200)).toFixed(2),
                            ['grandtotal']: parseFloat(grandtotal + value + (value * item.gstrate / 100) + this.state.freight).toFixed(2)
                        })
                    }
                    else if ((customer.gstin.substr(0, 2) === business.gstin.substr(0, 2)) || (customer.business === '')) {
                        this.setState({
                            ...this.state,
                            ['igst' + n]: '',
                            ['sgst' + n]: value === 0 ? '' : parseFloat(value * (item.gstrate / 200)).toFixed(2),
                            ['cgst' + n]: value === 0 ? '' : parseFloat(value * (item.gstrate / 200)).toFixed(2),
                            ['total' + n]: value === 0 ? '' : parseFloat(value + (value * item.gstrate / 100)).toFixed(2),
                            ['totalamountbasic']: parseFloat(totalbasic + value).toFixed(2),
                            ['totalcgst']: parseFloat(totalcgst + (value * item.gstrate / 200)).toFixed(2),
                            ['totalsgst']: parseFloat(totalsgst / 2 + (value * item.gstrate / 200)).toFixed(2),
                            ['grandtotal']: parseFloat(grandtotal + value + (value * item.gstrate / 100) + this.state.freight).toFixed(2)
                        })
                    }
                    else {
                        this.setState({
                            ...this.state,
                            ['igst' + n]: value === 0 ? '' : parseFloat(value * (item.gstrate / 100)).toFixed(2),
                            ['cgst' + n]: '',
                            ['sgst' + n]: '',
                            ['total' + n]: value === 0 ? '' : parseFloat(value + (value * item.gstrate / 100)).toFixed(2),
                            ['totalamountbasic']: parseFloat(totalbasic + value).toFixed(2),
                            ['totaligst']: parseFloat(totaligst + (value * item.gstrate / 100)).toFixed(2),
                            ['grandtotal']: parseFloat(grandtotal + value + (value * item.gstrate / 100) + this.state.freight).toFixed(2)
                        })
                    }
                }
            }
            else if (/^total/i.test(name)) {
                let total = 0;
                for (let i = 0; i < 10; i++) {
                    if (i !== index) total = total + (this.state['total' + i] !== '' ? parseFloat(this.state['total' + i]) : 0);
                }
                this.setState({
                    ...this.state,
                    ['grandtotal']: parseFloat(total + parseFloat(value) + parseFloat(this.state.freight)).toFixed(2)
                })
            }
            else if (/^cgst/i.test(name)) {
                let b = this.state['basic' + n] === '' ? 0 : this.state['basic' + n];
                let x = this.state['sgst' + n] === '' ? 0 : this.state['sgst' + n];
                this.setState({
                    ...this.state,
                    ['total' + n]: parseFloat(parseFloat(value) + parseFloat(x) + parseFloat(b)).toFixed(2),
                    ['totalcgst']: parseFloat(totalcgst + parseFloat(value)).toFixed(2),
                    ['grandtotal']: parseFloat(grandtotal + parseFloat(value) + parseFloat(this.state['sgst' + n]) + parseFloat(b) + parseFloat(this.state.freight)).toFixed(2)
                })
            }
            else if (/^sgst/i.test(name)) {
                let b = this.state['basic' + n] === '' ? 0 : this.state['basic' + n];
                let x = this.state['sgst' + n] === '' ? 0 : this.state['sgst' + n];
                this.setState({
                    ...this.state,
                    ['total' + n]: parseFloat(parseFloat(value) + parseFloat(x) + parseFloat(b)).toFixed(2),
                    ['totalsgst']: parseFloat(totalsgst + parseFloat(value)).toFixed(2),
                    ['grandtotal']: parseFloat(grandtotal + parseFloat(value) + parseFloat(this.state['cgst' + n]) + parseFloat(b) + parseFloat(this.state.freight)).toFixed(2)
                })
            }
            else if (/^igst/i.test(name)) {
                let b = this.state['basic' + n] === '' ? 0 : this.state['basic' + n];
                this.setState({
                    ...this.state,
                    ['total' + n]: parseFloat(parseFloat(value) + parseFloat(b)).toFixed(2),
                    ['totaligst']: parseFloat(totaligst + parseFloat(value)).toFixed(2),
                    ['grandtotal']: parseFloat(grandtotal + parseFloat(value) + parseFloat(b) + parseFloat(this.state.freight)).toFixed(2)
                })
            }
            else if (/^vat/i.test(name)) {
                let b = this.state['basic' + n] === '' ? 0 : this.state['basic' + n];
                this.setState({
                    ...this.state,
                    ['total' + n]: parseFloat(parseFloat(value) + parseFloat(b)).toFixed(2),
                    ['totalvat']: parseFloat(totalvat + parseFloat(value)).toFixed(2),
                    ['grandtotal']: parseFloat(grandtotal + parseFloat(value) + parseFloat(b) + parseFloat(this.state.freight)).toFixed(2)
                })
            }
            else if (/^excise/i.test(name)) {
                let b = this.state['basic' + n] === '' ? 0 : this.state['basic' + n];
                this.setState({
                    ...this.state,
                    ['total' + n]: parseFloat(parseFloat(value) + parseFloat(b)).toFixed(2),
                    ['totalexcise']: parseFloat(totalexcise + parseFloat(value)).toFixed(2),
                    ['grandtotal']: parseFloat(grandtotal + parseFloat(value) + parseFloat(b) + parseFloat(this.state.freight)).toFixed(2)
                })
            }
            else;
        }
    }

    handleSubmit = () => {
        if (this.state.customerid === '') alert("Please Select Customer");
        else if (this.state.shippedtoid === '') alert("Please Select Shipped to");
        else if (!(/^[a-zA-Z]{3} [0-9]{2} [0-9]{4}$/i.test(this.state.invoicedate))) alert("Date Must Be In (MON DD YYYY) Format");
        else if (isNaN(parseInt(this.state.invoicenumber))) alert("Invoice Number Must Be In Digits ");
        else if (this.state.ponumber.length > 14) alert("PO Number Must Be Within 15 Characters");
        else if (this.state.podate.length > 14) alert("PO Date Must Be Within 15 Characters");
        else if (this.state.transporter.length > 74) alert("PO Date Must Be Within 75 Characters");
        else if (this.state.vehiclenumber.length > 14) alert("Vehicle Number Must Be Within 15 Characters");
        else if (this.state.grno.length > 14) alert("GR/BL No Number Must Be Within 15 Characters");
        else if (this.state.destination.length > 29) alert("Destination Must Be Within 30 Characters");
        else if (this.state.terms.length > 14) alert("Terms & Condiions Must Be WIthin 500 Characters");
        else if (isNaN(parseInt(this.state.freight)) || (this.state.freight < 0)) alert("Freight Charges Must Be Greater Than or Equal To 0");
        else if (isNaN(parseInt(this.state.totalamountbasic)) || (this.state.totalamountbasic <= 0)) alert("Amount Basic Must Be Greater Than 0");
        else if (isNaN(parseInt(this.state.totaligst)) || (this.state.totaligst < 0)) alert("Total IGST Must Be Greater Than or Equal To 0");
        else if (isNaN(parseInt(this.state.totalcgst)) || (this.state.totalcgst < 0)) alert("Total CGST Must Be Greater Than or Equal To 0");
        else if (isNaN(parseInt(this.state.totalsgst)) || (this.state.totalsgst < 0)) alert("Total SGST Must Be Greater Than or Equal To 0");
        else if (isNaN(parseInt(this.state.grandtotal)) || (this.state.grandtotal <= 0)) alert("Grand Total Must Be Greater Than 0");
        else if (isNaN(parseInt(this.state.blamount)) || (this.state.blamount < 0)) alert("BL Amount Must Be Greater Than or Equal To 0");
        else this.props.invoiceUpdate(this.props.businessid, this.props.fyid, this.state);
    }

    handleOnBlurMegadata = (event) => {
        let target = event.target;
        let name = target.name;
        let value = target.value;
        if (value === '') value = 0;
        value = parseFloat(value).toFixed(2);
        if (/^totalamountbasic/i.test(name)) {
            this.setState({
                [name]: value,
                ['grandtotal']: (parseFloat(this.state.totaligst) + parseFloat(this.state.totalcgst) + parseFloat(this.state.totalsgst) + parseFloat(this.state.freight) + parseFloat(value)).toFixed(2)
            })
        }
        else if (/^totaligst/i.test(name)) {
            this.setState({
                [name]: value,
                ['grandtotal']: (parseFloat(this.state.totalamountbasic) + parseFloat(this.state.totalcgst) + parseFloat(this.state.totalsgst) + parseFloat(this.state.freight) + parseFloat(value)).toFixed(2)
            })
        }
        else if (/^totalcgst/i.test(name)) {
            this.setState({
                [name]: value,
                ['grandtotal']: (parseFloat(this.state.totalamountbasic) + parseFloat(this.state.totaligst) + parseFloat(this.state.totalsgst) + parseFloat(this.state.totalvat) + parseFloat(this.state.totalexcise) + parseFloat(this.state.freight) + parseFloat(value)).toFixed(2)
            })
        }
        else if (/^totalsgst/i.test(name)) {
            this.setState({
                [name]: value,
                ['grandtotal']: (parseFloat(this.state.totalamountbasic) + parseFloat(this.state.totalcgst) + parseFloat(this.state.totaligst) + parseFloat(this.state.totalvat) + parseFloat(this.state.totalexcise) + parseFloat(this.state.freight) + parseFloat(value)).toFixed(2)
            })
        }
        else if (/^totalvat/i.test(name)) {
            this.setState({
                [name]: value,
                ['grandtotal']: (parseFloat(this.state.totalamountbasic) + parseFloat(this.state.totalcgst) + parseFloat(this.state.totalsgst) + parseFloat(this.state.totaligst) + parseFloat(this.state.totalexcise) + parseFloat(this.state.freight) + parseFloat(value)).toFixed(2)
            })
        }
        else if (/^totalexcise/i.test(name)) {
            this.setState({
                [name]: value,
                ['grandtotal']: (parseFloat(this.state.totalamountbasic) + parseFloat(this.state.totalcgst) + parseFloat(this.state.totalsgst) + parseFloat(this.state.totalvat) + parseFloat(this.state.totaligst) + parseFloat(this.state.freight) + parseFloat(value)).toFixed(2)
            })
        }
        else if (/^freight/i.test(name)) {
            this.setState({
                [name]: value,
                ['grandtotal']: (parseFloat(this.state.totalamountbasic) + parseFloat(this.state.totalcgst) + parseFloat(this.state.totalsgst) + parseFloat(this.state.totalvat) + parseFloat(this.state.totalexcise) + parseFloat(this.state.totaligst) + parseFloat(value)).toFixed(2)
            })
        }
        else;
    }
    render() {
        if (!cookie.load('userservice')) return <Redirect to="/" />;
        if ((this.props.invoiceUpdateMessage.loading === true) || (this.props.salesDetail.loading === true) || (this.props.getCustomer.loading === true) || (this.props.getItem.loading === true) || (this.props.logoutMessage.loading === true)) {
            return (
                <div>
                    <Header logout={this.props.logout} />
                    <div className="mt-3 d-flex justify-content-center">
                        <button className="btn btn-danger">Loading... </button>
                    </div>
                </div>
            );
        }
        else if ((this.props.salesDetail.error !== '') || (this.props.getCustomer.error !== '') || (this.props.getItem.error !== '')) {
            return (
                <div>
                    <Header logout={this.props.logout} />
                    <div className="mt-3 d-flex justify-content-center">
                        <button className="btn btn-danger">Error.. Please Refresh and Try Agin </button>
                    </div>
                </div>
            );
        }
        else {
            let business = this.props.businessList.data.filter(b => b.businessid === this.props.businessid)[0];
            let fy = business.financial.filter(f => f.fyid === this.props.fyid)[0].financialyear;
            return (
                <div>
                    <Header logout={this.props.logout} />
                    <div style={{ minHeight: "93vh", display: "flex", flexDirection: "column" }}>
                        <div className="container" style={{ flex: "1" }}>
                            <h6 className="text-center pl-2 pr-2 pt-4" style={{ lineHeight: "1" }}><strong>{business.businessname} {fy}</strong></h6>
                            <h6 className="text-center pl-2 pr-2 mb-4" style={{ lineHeight: "1" }}>{business.alias ? "(" + business.alias + ")" : ''}</h6>
                            <h6 className="text-center pl-2 pr-2"><strong><u>Update Invoice</u></strong></h6>
                            <div className="" >
                                <LocalForm className="small" onSubmit={this.handleSubmit}>
                                    <div className="row mt-4">
                                        <div className="col-12 mb-1">
                                            <input name="cash" type="checkbox" checked={this.state.cash} onChange={this.handleInputChange} />&nbsp;&nbsp;
                                            <span htmlFor="cash" className="mb-0 muted-text col-8 p-0"><strong>Raise Invoice To Cash</strong></span>
                                        </div>
                                        <div class="col-12 col-md-9 row">
                                            {this.state.cash === 0 ? <div className="form-group mb-1 col-12 col-md-3">
                                                <label htmlFor="customerid" className="mb-0 text"><strong>Select Billed To:</strong></label>
                                                <Control.select model=".customerid" className="form-control form-control-sm" id="customerid" name="customerid" value={this.state.customerid} onChange={this.handleInputChange}>
                                                    <Option list={this.props.getCustomer.data} scope="customer" />
                                                </Control.select>
                                            </div>
                                                : ''}
                                            {this.state.cash === 0 ? <div className="form-group mb-1 col-12 col-md-3">
                                                <label htmlFor="shippedtoid" className="mb-0 muted-text"><strong>Select Shipped To:</strong></label>
                                                <Control.select model=".shippedtoid" className="form-control form-control-sm" id="shippedtoid" name="shippedtoid" value={this.state.shippedtoid} onChange={this.handleInputChange}>
                                                    <Option list={this.props.getCustomer.data} scope="shipped" />
                                                </Control.select>
                                            </div> : ''}

                                            <div className="form-group mb-1 col-6 col-md-3">
                                                <label htmlFor="invoicedate" className="mb-0 muted-text"><strong>Invoice Date</strong></label>
                                                <Control.text model=".invoicedate" className="form-control form-control-sm" id="invoicedate" name="invoicedate" value={this.state.invoicedate} onChange={this.handleInputChange} />
                                            </div>
                                            <div className="form-group mb-1 col-6 col-md-3">
                                                <label htmlFor="invoicenumber" className="mb-0 muted-text"><strong>Invoice Number</strong></label>
                                                <Control.text model=".invoicenumber" className="form-control form-control-sm" id="invoicenumber" name="invoicenumber" value={this.state.invoicenumber} onChange={this.handleInputChange} />
                                            </div>
                                            <div className="form-group mb-1 col-6 col-md-3">
                                                <label htmlFor="ponumber" className="mb-0 muted-text"><strong>P.O. Number</strong></label>
                                                <Control.text model=".ponumber" className="form-control form-control-sm" id="ponumber" name="ponumber" value={this.state.ponumber} onChange={this.handleInputChange} />
                                            </div>
                                            <div className="form-group mb-1 col-6 col-md-3">
                                                <label htmlFor="podate" className="mb-0 muted-text"><strong>P.O. Date</strong></label>
                                                <Control.text model=".podate" className="form-control form-control-sm" id="podate" name="podate" value={this.state.podate} onChange={this.handleInputChange} />
                                            </div>
                                            <div className="form-group mb-1 col-6 col-md-3">
                                                <label htmlFor="transporter" className="mb-0 muted-text"><strong>Transporter Name</strong></label>
                                                <Control.text model=".transporter" className="form-control form-control-sm" id="transporter" name="transporter" value={this.state.transporter} onChange={this.handleInputChange} />
                                            </div>
                                            <div className="form-group mb-1 col-6 col-md-3">
                                                <label htmlFor="vehiclenumber" className="mb-0 muted-text"><strong>Vehicle No.</strong></label>
                                                <Control.text model=".vehiclenumber" className="form-control form-control-sm" id="vehiclenumber" name="vehiclenumber" value={this.state.vehiclenumber} onChange={this.handleInputChange} />
                                            </div>
                                            <div className="form-group mb-1 col-6 col-md-3">
                                                <label htmlFor="grno" className="mb-0 muted-text"><strong>G.R. No/B.L. No</strong></label>
                                                <Control.text model=".grno" className="form-control form-control-sm" id="grno" name="grno" value={this.state.grno} onChange={this.handleInputChange} />
                                            </div>
                                            <div className="form-group mb-1 col-6 col-md-3">
                                                <label htmlFor="destination" className="mb-0 muted-text"><strong>Destination</strong></label>
                                                <Control.text model=".destination" className="form-control form-control-sm" id="destination" name="destination" value={this.state.destination} onChange={this.handleInputChange} />
                                            </div>
                                            <div className="form-group mb-1 col-6 col-md-3">
                                                <label htmlFor="freight" className="mb-0 muted-text"><strong>Freight Charges</strong></label>
                                                <Control.text model=".freight" className="form-control form-control-sm" id="freight" name="freight" value={this.state.freight} onChange={this.handleInputChange} onBlur={this.handleOnBlurMegadata} />
                                            </div>
                                        </div>
                                        <div className="col-12 col-md-3 row">
                                            <div className="form-group mb-1 col-12">
                                                <label htmlFor="terms" className="mb-0 muted-text"><strong>Terms and Condition</strong></label>
                                                <Control.textarea model=".terms" rows="4" className="form-control form-control-sm" id="terms" name="terms" value={this.state.terms} onChange={this.handleInputChange} />
                                            </div>
                                        </div>
                                        <div className="row col-12">
                                            <RenderItemComponent state={this.state} businessid={this.props.businessid} businessList={this.props.businessList} getItem={this.props.getItem} getCustomer={this.props.getCustomer} handleInputChange={this.handleInputChange} handleOnBlur={this.handleOnBlur} handleOnBlurMiniData={this.handleOnBlurMiniData} />
                                        </div>
                                        <div className="col-12">
                                            <Button className="btn mt-1 btn-sm" style={{ backgroundColor: "#fff", border: "1px solid #28a745", color: "#28a745" }} onClick={this.addInvoiceItem}>Add More Item </Button>
                                        </div>
                                        <div className="row col-12 mt-3">
                                            <div className="form-group mb-1 col-6 col-md-2">
                                                <label htmlFor="totalamountbasic" className="mb-0 muted-text"><strong>Amount (Basic)</strong></label>
                                                <Control.text model=".totalamountbasic" className="form-control form-control-sm" id="totalamountbasic" name="totalamountbasic" onChange={this.handleInputChange} onBlur={this.handleOnBlurMegadata} value={this.state.totalamountbasic} />
                                            </div>
                                            <div className="form-group mb-1 col-6 col-md-2">
                                                <label htmlFor="totaligst" className="mb-0 muted-text"><strong>IGST Amount</strong></label>
                                                <Control.text model=".totaligst" className="form-control form-control-sm" id="totaligst" name="totaligst" onChange={this.handleInputChange} value={this.state.totaligst} onBlur={this.handleOnBlurMegadata} />
                                            </div>
                                            <div className="form-group mb-1 col-6 col-md-2">
                                                <label htmlFor="totalcgst" className="mb-0 muted-text"><strong>CGST Amount</strong></label>
                                                <Control.text model=".totalcgst" className="form-control form-control-sm" id="totalcgst" name="totalcgst" onChange={this.handleInputChange} value={this.state.totalcgst} onBlur={this.handleOnBlurMegadata} />
                                            </div>
                                            <div className="form-group mb-1 col-6 col-md-2">
                                                <label htmlFor="totalsgst" className="mb-0 muted-text"><strong>SGST Amount</strong></label>
                                                <Control.text model=".totalsgst" className="form-control form-control-sm" id="totalsgst" name="totalsgst" onChange={this.handleInputChange} value={this.state.totalsgst} onBlur={this.handleOnBlurMegadata} />
                                            </div>
                                            <div className="form-group mb-1 col-6 col-md-2">
                                                <label htmlFor="GrandTotal" className="mb-0 muted-text"><strong>Grand Total</strong></label>
                                                <Control.text model=".grandtotal" className="form-control form-control-sm" id="grandtotal" name="grandtotal" onChange={this.handleInputChange} value={this.state.grandtotal} />
                                            </div>
                                            <div className="col-12 mt-1">
                                                <div className="form-group mb-1 col-12 col-md-3 p-2" style={{ backgroundColor: "pink" }}>
                                                    <label htmlFor="blamount" className="mb-0 muted-text"><strong>BL Amount <small>(for ReverseCharge Calculation)</small></strong></label>
                                                    <Control.text model=".blamount" className="form-control form-control-sm" id="blamount" name="blamount" onChange={this.handleInputChange} value={this.state.blamount} />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-12 mt-1">
                                        <button type="submit" className="row btn btn-success mt-1 btn-sm small" >Update Invoice</button>
                                    </div>
                                </LocalForm>
                            </div>
                        </div>
                        <Footer />
                    </div>
                </div>
            );
        }
    }
}

export default InvoiceUpdate;