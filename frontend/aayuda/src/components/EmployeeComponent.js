import React, { Component, useState } from 'react';
import cookie from 'react-cookie';
import { Redirect, Link } from 'react-router-dom';
import { Button, Collapse } from 'reactstrap';
import { LocalForm, Control, Errors } from 'react-redux-form';
import { Header } from './HeaderComponent';
import Calendar from 'react-calendar';
import 'react-calendar/dist/Calendar.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faAngleDoubleDown } from '@fortawesome/free-solid-svg-icons';
import { Footer } from './Footer';

const OTFixedHourRate = function ({ state, handleInputChange }) {
    if (state.otmethod === 'fixed') {
        return (
            <div className="form-group col-md-10 mb-1">
                <label htmlFor="othourrate" className="mb-0 muted-text">OT Per Hour Rate</label>
                <div className="">
                    <Control.text model=".othourrate" className="form-control form-control-sm" id="othourrate" name="othourrate" placeholder="OT Hour Rate" onChange={handleInputChange} value={state.othourrate} />
                </div>
            </div>
        )
    }
    else return <div></div>
}

const OTCalculationMethod = function ({ state, handleInputChange }) {
    if (state.ot === 'yes') {
        return (
            <div className="form-group col-md-10 mb-1">
                <div className="field">
                    <label>OT Calculation Method</label><br />
                &nbsp;&nbsp;&nbsp;&nbsp;<label><Control.radio model=".otmethod" name="otmethod" value="wage" onChange={handleInputChange} /> * Per Hour Wage</label>&nbsp;&nbsp;&nbsp;&nbsp;
                <label><Control.radio model=".otmethod" name="otmethod" value="fixed" onChange={handleInputChange} /> * Fixed OT Hour Rate</label>
                </div>
            </div>
        )
    }
    else return <div></div>
}

const AddEmployee = function ({ handleSubmit, addEmployeeMessage, handleInputChange, state, setDate }) {
    let [isOpen, setIsOpen] = useState(false);
    let toggle = () => setIsOpen((!isOpen));
    if (addEmployeeMessage.error !== '') isOpen = true;
    return (
        <div className="col-12">
            <Button className="btn-sm mt-3 col-12" style={{ backgroundColor: "#fff", border: "1px solid #28a745", color: "#28a745" }} onClick={toggle}><div className="d-flex"><span className="col-11 text-center">Add New Employee</span><span className="col-1 p-0 text-right"><FontAwesomeIcon size="sm" icon={faAngleDoubleDown} /></span></div></Button>
            <Collapse isOpen={isOpen}>
                <LocalForm onSubmit={((values) => handleSubmit(values))}>
                    <div className="justify-content-center mt-4">
                        <div className="form-group col-12">
                            <label htmlFor="employeeno" className="mb-0"><span className="muted-text">Employee No<span className="text-danger">*</span></span></label>
                            <div className="">
                                <Control.text model=".employeenameno" className="form-control form-control-sm pt-3 pb-3" id="employeeno" name="employeeno" placeholder="Employee No" onChange={handleInputChange} value={state.employeeno} />
                            </div>
                        </div>
                        <div className="form-group col-12">
                            <label htmlFor="employeename" className="mb-0"><span className="muted-text">Employee Name<span className="text-danger">*</span></span></label>
                            <div className="">
                                <Control.text model=".employeename" className="form-control form-control-sm pt-3 pb-3" id="employeename" name="employeename" placeholder="Employee Name" onChange={handleInputChange} value={state.employeename} />
                            </div>
                        </div>
                        <div className="form-group col-12">
                            <label htmlFor="aadharnumber" className="mb-0"><span className="muted-text">Aadhar Number</span></label>
                            <div className="">
                                <Control.text model=".aadharnumber" className="form-control form-control-sm pt-3 pb-3" id="aadharnumber" name="aadharnumber" placeholder="Aadhar Number" onChange={handleInputChange} value={state.aadharnumber} />
                            </div>
                        </div>
                        <div className="form-group col-12">
                            <label htmlFor="salary" className="mb-0"><span className="muted-text">Monthly Salary<span className="text-danger">*</span></span></label>
                            <div className="">
                                <Control.text model=".salary" className="form-control form-control-sm pt-3 pb-3" id="salary" name="salary" placeholder="Salary" onChange={handleInputChange} value={state.salary} />
                            </div>
                        </div>
                        <div className="form-group col-12">
                            <div>Date Of Joining<span className="text-danger">*</span></div>
                        </div>
                        <div className="col-12">
                            <div className="">
                                <Calendar onChange={setDate} value={state.date} />
                            </div>
                        </div>
                        <OTCalculationMethod state={state} handleInputChange={handleInputChange} />
                        <OTFixedHourRate state={state} handleInputChange={handleInputChange} />
                        <div className="form-group col-12">
                            <div className="mt-3"><button type="submit" className="btn btn-success btn-sm pl-4 pr-4" >Add</button></div>
                        </div>
                    </div>
                </LocalForm>
            </Collapse>
        </div >
    );
}

const EmployeeListCard = function ({ employee, businessid }) {
    let salary = JSON.parse(employee.salary);
    let k = Object.keys(salary);
    let [isOpen, setIsOpen] = useState(false);
    let toggle = () => setIsOpen((!isOpen));
    return (
        <div className="col-12 justify-content-center">
            <div className="">
                <Button className="btn-sm btn mt-2 col-12 btn-success" onClick={toggle}><div className="d-flex"><span className="col-11">{employee.employeename}</span><span className="col-1 p-0 text-right"><FontAwesomeIcon size="sm" icon={faAngleDoubleDown} /></span></div></Button>
                <Collapse isOpen={isOpen}>
                    <div className="card card-body ">
                        <div>Aadhar Number: {employee.aadharnumber}</div>
                        <div>DOJ : {employee.doj}</div>
                        <div>Salary: Rs. {JSON.parse(salary[k.length - 1]).salary} from {JSON.parse(salary[k.length - 1]).date}</div>
                        <div><Link to={`/business/businessdetail/employee/revisesalary/${businessid}/${employee.employeeid}`}><button className="btn btn-sm justify-content-right mt-2 pr-2 pl-2" role="button" style={{ backgroundColor: "#fff", border: "1px solid #28a745", color: "#28a745" }}>Revise Salary</button></Link><Link to={`/business/businessdetail/employee/terminate/${businessid}/${employee.employeeid}`}><button className="btn btn-sm justify-content-right ml-2 mt-2 pr-2 pl-2" role="button" style={{ backgroundColor: "#fff", border: "1px solid red", color: "red" }}>Terminate</button></Link></div>
                    </div>
                </Collapse>
            </div>
        </div>
    );
}

const TerminatedEmployeeListCard = function ({ employee, businessid }) {
    return (
        <div className="col-12 justify-content-center">
            <div className="">
                <Button className="btn-sm btn mt-2 col-12 btn-danger"><div className="d-flex"><span className="col-11">{employee.employeename} - {employee.dot}</span></div></Button>
            </div>
        </div>
    );
}

const EmployeeList = function ({ getEmployee, businessid }) {
    let returnrows = [];
    if (getEmployee.error === '') {
        if (getEmployee.data.length > 0) {
            returnrows.push(getEmployee.data.map(e => e.active === 1 ? <EmployeeListCard employee={e} businessid={businessid} /> : ''));
            returnrows.push(getEmployee.data.map(e => e.active === 0 ? <TerminatedEmployeeListCard employee={e} businessid={businessid} /> : ''));
            return returnrows;
        }
        else return <h6 className="col-12 text-center text-muted mt-2 pb-4">No Employee Added Yet</h6>;
    }
    else return <div></div>
}

class Employee extends Component {
    constructor(props) {
        super(props);
        this.state = {
            employeeno: this.props.getEmployee.employeeno ? this.props.getEmployee.employeeno : 1,
            employeename: '',
            aadharnumber: '',
            salary: 0,
            ot: "no",
            otmethod: "wage",
            othourrate: 0,
            active: 1,
            date: new Date(),
            doj: new Date().toDateString().split(' ').slice(1).join(' ')
        }
        window.onload = () => {
            this.props.refreshEmployeeState();
        }
    }

    componentDidMount() {
        if ((this.props.getEmployee.message === 'initial') && (this.props.getEmployee.loading === false)) this.props.getEmployeeList(this.props.businessid);
        if (this.props.addEmployeeMessage.error !== '') {
            alert("Error " + this.props.addEmployeeMessage.error);
            this.setState({
                ...this.props.addEmployeeMessage.state
            });
            this.props.resetAddEmployeeMessage();
        }
        if (this.props.addEmployeeMessage.message === 'success') {
            alert("Employee Added Successfully");
            this.props.resetAddEmployeeMessage();
        }
        if (this.props.addEmployeeMessage.message === 'Already Registered') {
            alert("Employee Already Registered");
            this.props.resetAddEmployeeMessage();
        }
        if (this.props.logoutMessage.error !== '') {
            alert("Error Logging Out. Please Try Again");
            this.props.resetLogoutMessage();
        }
    }

    handleInputChange = (event) => {
        const target = event.target;
        const name = target.name;
        const value = target.value;
        this.setState({
            [name]: value,
        });
    }

    setDate = (date) => {
        this.setState({
            ...this.state,
            date: date,
            doj: date.toDateString().split(' ').slice(1).join(' ')
        })

    }

    handleSubmit = () => {
        if (this.state.employeeno === '') alert("Employee No is Mandatory");
        else if (isNaN(this.state.employeeno)) alert("Employee No Must Be A Number");
        else if (this.state.emloyeename === '') alert ("Employee Name Is Mandatory");
        else if (this.state.employeename.length > 99) alert("Employee Name Must Be Within 100 Characters");
        else if (this.state.aadharnumber > 15) alert("Aadhar Number Length Must be Within 15 Characters");
        else if (this.state.salary === '') alert ("Salary Is Mandatory");
        else if (isNaN(this.state.salary)) alert ("Salary Must Be A Number");
        else if (this.state.doj === '') alert ("Date Of Joining Is Mandatory");
        else this.props.addEmployee(this.state, this.props.businessid);
    }

    render() {
        if (!cookie.load('userservice')) return <Redirect to="/" />
        else {
            if ((this.props.getEmployee.loading === true) || (this.props.addEmployeeMessage.loading === true) || (this.props.logoutMessage.loading === true)) {
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div className="mt-3 d-flex justify-content-center">
                            <button className="btn btn-danger">Loading... </button>
                        </div>
                    </div>
                );
            }
            else if (this.props.getEmployee.error !== '') {
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div className="mt-3 d-flex justify-content-center">
                            <button className="btn btn-danger">Error.. Please Refresh and Try Agin </button>
                        </div>
                    </div>
                );
            }
            else {
                let business = this.props.businessList.data.filter(b => b.businessid === this.props.businessid)[0];
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div style={{ minHeight: "93vh", display: "flex", flexDirection: "column" }}>
                            <div className="container d-flex justify-content-center pr-0 pl-0">
                                <div className="col-12 col-md-5">
                                    <div className="small pr-0 pl-0">
                                        <h6 className="text-center pl-2 pr-2 pt-4" style={{ lineHeight: "1" }}><strong>{business.businessname}</strong></h6>
                                        <h6 className="text-center pl-2 pr-2 mb-4" style={{ lineHeight: "1" }}>{business.alias ? "(" + business.alias + ")" : ''}</h6>
                                        <h6 className="text-center pl-2 pr-2 mb-2"><strong><u>Employees</u></strong></h6>
                                        <AddEmployee handleSubmit={this.handleSubmit} addEmployeeMessage={this.props.addEmployeeMessage} handleInputChange={this.handleInputChange} state={this.state} setDate={this.setDate} />
                                    </div>
                                </div>
                            </div>
                            <div className="container d-flex justify-content-center pr-0 pl-0 mt-4 pb-4" style={{ flex: "1" }}>
                                <div className="col-12 col-md-5">
                                    <div className="small pr-0 pl-0">
                                        <EmployeeList getEmployee={this.props.getEmployee} addEmployeeMessage={this.props.addEmployeeMessage} businessid={this.props.businessid} />
                                    </div>
                                </div>
                            </div>
                            <Footer />
                        </div>
                    </div>
                );
            }
        }
    }
}

export default Employee;