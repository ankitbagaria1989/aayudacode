import React, { Component } from 'react';
import cookie from 'react-cookie';
import { Redirect } from 'react-router-dom';
import { Header } from './HeaderComponent';
import { Footer } from './Footer';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faFilePdf } from '@fortawesome/free-solid-svg-icons';
import { Page, Text, View, Document, pdf } from '@react-pdf/renderer';
import { pdfstyles } from '../sylesheet/stylesheet';
import { saveAs } from 'file-saver';
const DataDisplay = function ({ salesDetail, getPayment, fyid, businessList, businessid, fy, getDebitCredit, getIncomeOS, getAssetAccount, getOpeningBalance, debtors }) {
    let returnrows = [];
    let debit = 0;
    let credit = 0;
    let i = 2;
    returnrows.push(<div><div className="d-flex small mt-4 p-1 d-md-none d-lg-none">
        <div className="col-6 col-md-5 p-1"></div>
        <div className="col-3 col-md-6 p-0 pt-1 text-right"><strong>Debit</strong></div>
        <div className="col-3 col-md-1 p-0 pt-1 text-right"><strong>Credit</strong></div>
    </div>
        <div className="container small mt-3 d-none d-md-block p-0">
            <div className="d-flex">
                <div className="col-6 p-0"><strong>Customer</strong></div>
                <div className="col-3 p-0 text-right"><strong>Debit</strong></div>
                <div className="col-3 p-0 text-right"><strong>Credit</strong></div>
            </div>
        </div></div>)
    salesDetail.data.forEach(d => {
        if ((d.type === 'sales') && (d.customerid)) debtors[d.customerid].sold = debtors[d.customerid].sold + parseFloat(d.grandtotal);
    })
    getPayment.data.forEach(d => {
        if (Object.keys(debtors).includes(d.customerid)) debtors[d.customerid].received = debtors[d.customerid].received + parseFloat(d.amount);
    })
    getDebitCredit.data.forEach(d => {
        if ((d.issue === "received") && (d.debitcredit === 'debit')) debtors[d.partyid].sold = debtors[d.partyid].sold - parseFloat(d.amount);
        else if ((d.issue === "issue") && (d.debitcredit === 'credit')) debtors[d.partyid].sold = debtors[d.partyid].sold - parseFloat(d.amount);
    })
    getAssetAccount.data.forEach(d => {
        if (d.soldfyid === fyid) {
            if (d.customerid !== 'cash') debtors[d.customerid.split("...")[0]].sold = debtors[d.customerid.split("...")[0]].sold + parseFloat(d.soldtotal);
        }
    })
    getIncomeOS.data.forEach(d => {
        if (d.customerid) debtors[d.customerid].sold = debtors[d.customerid].sold + parseFloat(d.amount);
    })
    getOpeningBalance.data.forEach(d => {
        if (d.party === 'customer') debtors[d.partyid].sold = debtors[d.customerid].sold + parseFloat(d.amount);
    })
    Object.keys(debtors).forEach(k => {
        returnrows.push(<div><div className="d-flex small p-1 d-md-none d-lg-none" style={i % 2 === 0 ? { backgroundColor: "rgb(181, 253, 233)" } : {}}>
            <div className="col-6 col-md-5 p-0 text-left"><strong>{debtors[k].customername}</strong></div>
            <div className="col-3 col-md-6 p-0 pt-1 text-right">{debtors[k].sold - debtors[k].received >= 0 ? parseFloat(debtors[k].sold - debtors[k].received).toFixed(2) : ''}</div>
            <div className="col-3 col-md-1 p-0 pt-1 text-right">{debtors[k].sold - debtors[k].received < 0 ? parseFloat(debtors[k].received - debtors[k].sold).toFixed(2) : ''}</div>
        </div>
            <div className="container small d-none d-md-block p-1" style={i % 2 === 0 ? { backgroundColor: "rgb(181, 253, 233)" } : {}}>
                <div className="d-flex">
                    <div className="col-6 p-0 text-left"><strong>{debtors[k].customername}</strong></div>
                    <div className="col-3 p-0 text-right">{debtors[k].sold - debtors[k].received >= 0 ? parseFloat(debtors[k].sold - debtors[k].received).toFixed(2) : ''}</div>
                    <div className="col-3 p-0 text-right">{debtors[k].sold - debtors[k].received < 0 ? parseFloat(debtors[k].received - debtors[k].sold).toFixed(2) : ''}</div>
                </div>
            </div></div>)
        debit = debit + (debtors[k].sold - debtors[k].received >= 0 ? debtors[k].sold - debtors[k].received : 0);
        credit = credit + (debtors[k].sold - debtors[k].received < 0 ? debtors[k].received - debtors[k].sold : 0);
        i++;
    })

    returnrows.push(<div><div className="d-flex small p-1 d-md-none d-lg-none mt-1" >
        <div className="col-6 col-md-5 p-1"></div>
        <div className="col-3 col-md-6 p-0 pt-1 border-top broder-dark text-right">{debit !== 0 ? debit : ''}</div>
        <div className="col-3 col-md-1 p-0 pt-1 border-top border-dark text-right">{credit !== 0 ? credit : ''}</div>
    </div>
        <div className="container small d-none d-md-block p-0 mt-1">
            <div className="d-flex">
                <div className="col-6 p-0"><strong></strong></div>
                <div className="col-3 p-0 border-top border-dark text-right">{debit !== 0 ? parseFloat(debit).toFixed(2) : ''}</div>
                <div className="col-3 p-0 border-top border-dark text-right">{credit !== 0 ? parseFloat(credit).toFixed(2) : ''}</div>
            </div>
        </div></div>)
    return returnrows;
}
let debit = 0, credit = 0, debtors = {};
const DebtorsLedgerPage = function ({ y }) {
    let returnrows = [];
    let keys = Object.keys(debtors);
    if (y > 1) {
        returnrows.push(
            <View style={pdfstyles.itemSectionLedger}>
                <View style={pdfstyles.itemHeader}><Text style={{ width: '375px', textAlign: "left" }}>...Carry Forward</Text><Text style={{ width: '100px', textAlign: "right" }}>{parseFloat(debit).toFixed(2)}</Text><Text style={{ width: '100px', textAlign: "right" }}>{parseFloat(credit).toFixed(2)}</Text></View>
            </View>
        )
    }
    for (let x = (y - 1) * 50; x < y * 50 && x < keys.length; x++) {
        if (debtors[keys[x]].sold >= debtors[keys[x]].received) {
            debit = debit + parseFloat(debtors[keys[x]].sold - debtors[keys[x]].received);
            returnrows.push(<View style={pdfstyles.itemSectionLedger, { fontFamily: "Helvetica", }}>
                <View style={pdfstyles.itemHeader}><Text style={{ width: '375px', textAlign: "left" }}>{debtors[keys[x]].customername}</Text><Text style={{ width: '100px', textAlign: "right" }}>{parseFloat(debtors[keys[x]].sold - debtors[keys[x]].received).toFixed(2)}</Text><Text style={{ width: '100px', textAlign: "right" }}></Text></View>
            </View>)
        }
        else if (debtors[keys[x]].sold < debtors[keys[x]].received) {
            credit = credit + parseFloat(debtors[keys[x]].received - debtors[keys[x]].sold);
            returnrows.push(<View style={pdfstyles.itemSectionLedger, { fontFamily: "Helvetica", }}>
                <View style={pdfstyles.itemHeader}><Text style={{ width: '375px', textAlign: "left" }}>{debtors[keys[x]].customername}</Text><Text style={{ width: '100px', textAlign: "right" }}></Text><Text style={{ width: '100px', textAlign: "right" }}>{parseFloat(debtors[keys[x]].received - debtors[keys[x]].sold).toFixed(2)}</Text></View>
            </View>)
        }
    }
    if (y === Math.ceil(keys.length / 50)) {
        returnrows.push(<View style={pdfstyles.itemSectionLedger, { fontFamily: "Helvetica-Bold", borderTop: "1px solid black", borderBottom: "1px solid black" }}>
            <View style={pdfstyles.itemHeader}><Text style={{ width: '375px', textAlign: "left" }}>Total</Text><Text style={{ width: '100px', textAlign: "right" }}>{debit > 0 ? parseFloat(debit).toFixed(2) : ''}</Text><Text style={{ width: '100px', textAlign: "right" }}>{credit > 0 ? parseFloat(credit).toFixed(2) : ''}</Text></View>
        </View>
        )
    }
    else {
        returnrows.push(<View><View style={pdfstyles.itemSectionLedger, { fontFamily: "Helvetica-Bold" }}>
            <View style={pdfstyles.itemHeader}><Text style={{ width: '375px', textAlign: "left" }}></Text><Text style={{ width: '100px', borderTop: "1px solid black", textAlign: "right" }}>{parseFloat(debit).toFloat(2)}</Text><Text style={{ width: '100px', borderTop: "1px solid black", textAlign: "right" }}>{parseFloat(credit).toFixed(2)}</Text></View>
        </View>
            <View style={pdfstyles.itemSectionLedger, { fontFamily: "Helvetica", marginTop: "5px", textAlign: "right" }}><Text></Text>...Continued</View></View>)
    }
    return returnrows;
}
const Report = function ({ business, l, n, period }) {
    let returnrows = [];
    for (let y = 1; y <= n; y++) {
        returnrows.push(<Page size="A4" style={pdfstyles.page}>
            {y === 1 ? <View><View style={pdfstyles.taxInvoiceHeader, { marginTop: "20px", textAlign: "center" }}>
                <View style={{ fontFamily: "Helvetica-Bold", width: "500px" }}><Text>{business.businessname}</Text></View>
                <View style={{ width: "500px" }}><Text>{business.address}</Text></View>
            </View>
                <View style={pdfstyles.taxInvoiceHeader, { marginTop: "10px", textAlign: "center" }}>
                    <View style={{ fontFamily: "Helvetica-Bold", textDecoration: "underline", width: "500px" }}><Text>Sundry Debtors</Text></View>
                    <View style={{ width: "500px" }}><Text>{period}</Text></View>
                </View></View> : ''}
            <View style={pdfstyles.itemSectionLedger, { marginTop: "20px", marginBottom: "5px", fontFamily: "Helvetica-Bold", textAlign: "center", borderTop:"1px solid black", borderBottom:"1px solid black" }}>
                <View style={pdfstyles.itemHeader}><Text style={{ width: '375px', textAlign: "left" }}>Particulars</Text><Text style={{ width: '100px', textAlign: "right" }}>Debit</Text><Text style={{ width: '100px', textAlign: "right" }}>Credit</Text></View>
            </View>
            <DebtorsLedgerPage y={y} />
        </Page >)
    }
    return returnrows;
}
const DebtorsLedger = function ({ business, fy }) {
    let period = fy;
    let l = Object.keys(debtors).length, i = 0, n = Math.ceil(l / 50), y = 0;
    return (
        <Document>
            <Report business={business} l={l} n={n} period={period} />
        </Document>
    )
}
class AccountReceivables extends Component {
    constructor(props) {
        super(props);
        window.onload = () => {
            this.props.resetPaymentDetail();
            this.props.resetOpeningBalance();
            this.props.refreshCustomerState();
            this.props.resetSalesDetail();
            this.props.refreshDebitCredit();
            this.props.resetIncomeOSDetail();
            this.props.refreshAssetAccountState();
        }
        let debtors = {}
        this.props.getCustomer.data.forEach(d => {
            debtors = {
                ...debtors,
                [d.customerid]: {
                    customername: d.businessname,
                    sold: 0,
                    received: 0
                }
            }
        })
        this.state = {
            debtors: debtors
        }
    }
    generatePdf = async (business, fy) => {
        try {
            debit = 0;
            credit = 0;
            const doc = <DebtorsLedger business={business} fy={fy} />;
            const blobPdf = pdf(doc);
            blobPdf.updateContainer(doc);
            const result = await blobPdf.toBlob();
            let filename = "SundryDebtors.pdf";
            saveAs(result, filename);
        }
        catch (err) {
            alert(err);
        }
    }
    componentDidMount() {
        if ((this.props.getCustomer.message === 'initial') && (this.props.getCustomer.loading === false)) this.props.getCustomerList(this.props.businessid);
        if ((this.props.salesDetail.message === 'initial') && (this.props.salesDetail.loading === false)) this.props.getSalesData(this.props.businessid, this.props.fyid);
        if ((this.props.getPayment.message === 'initial') && (this.props.getPayment.loading === false)) this.props.getPaymentDetail(this.props.businessid, this.props.fyid);
        if ((this.props.getOpeningBalance.message === 'initial') && (this.props.getOpeningBalance.loading === false)) this.props.getPartyOpeningBalance(this.props.businessid, this.props.fyid);
        if (this.props.getDebitCredit.message === 'initial') this.props.getDebitCreditList(this.props.businessid, this.props.fyid);
        if (this.props.getIncomeOS.message === 'initial') this.props.getIncomeOSDetail(this.props.businessid, this.props.fyid);
        if (this.props.getAssetAccount.message === 'initial') this.props.getAssetAccountList(this.props.businessid);
        if (this.props.logoutMessage.error !== '') {
            alert("Error Logging Out. Please Try Again");
            this.props.resetLogoutMessage();
        }
    }
    render() {
        if (!cookie.load('userservice')) return <Redirect to="/" />;
        else {
            if ((this.props.getPayment.loading === true) || (this.props.getOpeningBalance.loading === true) || (this.props.getCustomer.loading === true) || (this.props.salesDetail.loading === true) || (this.props.logoutMessage.loading === true) || (this.props.getDebitCredit.loading === true) || (this.props.getIncomeOS.loading === true) || (this.props.getAssetAccount.loading === true)) {
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div className="mt-3 d-flex justify-content-center">
                            <button className="btn btn-danger">Loading... </button>
                        </div>
                    </div>
                );
            }
            else if ((this.props.getPayment.error !== '') || (this.props.getOpeningBalance.error !== '') || (this.props.getCustomer.error !== '') || (this.props.salesDetail.error !== '') || (this.props.getDebitCredit.error !== '') || (this.props.getIncomeOS.error !== '') || (this.props.getAssetAccount.error !== '')) {
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div className="mt-3 d-flex justify-content-center">
                            <button className="btn btn-danger">Error.. Please Refresh and Try Again </button>
                        </div>
                    </div>
                );
            }
            else {
                let business = this.props.businessList.data.filter(b => b.businessid === this.props.businessid)[0];
                let fy = business.financial.filter(f => f.fyid === this.props.fyid)[0].financialyear;
                debtors = this.state.debtors;
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div style={{ minHeight: "93vh", display: "flex", flexDirection: "column" }}>
                            <div className="container" style={{ flex: "1" }}>
                                <h6 className="text-center pl-2 pr-2 pt-4" style={{ lineHeight: "1" }}><strong>{business.businessname} {fy}</strong></h6>
                                <h6 className="text-center pl-2 pr-2 mb-4" style={{ lineHeight: "1" }}>{business.alias ? "(" + business.alias + ")" : ''}</h6>
                                <h6 className="text-center pl-2 pr-2"><strong><u>Account Receivables (Sundry Debtors)</u></strong>&nbsp;&nbsp;&nbsp;<FontAwesomeIcon icon={faFilePdf} size="lg" onClick={() => this.generatePdf(business, fy)} />&nbsp;&nbsp;&nbsp;</h6>
                                <div className="col-12 p-0">
                                    {Object.keys(this.state.debtors).length > 0 ? <DataDisplay salesDetail={this.props.salesDetail} getPayment={this.props.getPayment} fyid={this.props.fyid} businessList={this.props.businessList} businessid={this.props.businessid} fy={fy} getDebitCredit={this.props.getDebitCredit} getIncomeOS={this.props.getIncomeOS} getAssetAccount={this.props.getAssetAccount} getOpeningBalance={this.props.getOpeningBalance} debtors={debtors} /> : ''}
                                </div>
                            </div>
                            <Footer />
                        </div>
                    </div>
                )
            }
        }
    }
}
export default AccountReceivables;