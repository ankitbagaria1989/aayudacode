import React, { Component } from 'react';
import { Form, Control } from 'react-redux-form';
import cookie from 'react-cookie';
import { Redirect } from 'react-router-dom'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faPhone, faEnvelope } from '@fortawesome/free-solid-svg-icons'
import { style } from '../sylesheet/stylesheet';

const LoginFormComponent = function ({ handleSubmit, loginMessage }) {
    return (
        <div>
            <Form model="login" onSubmit={((values) => handleSubmit(values))}>
                <div className="row d-flex justify-content-center">
                    <div className="col-12 col-md-4">
                        <div className="form-group col-12">
                            <div className="">
                                <Control.text model=".email" className="form-control form-control-sm pt-4 pb-4" id="email" name="email" placeholder="Email" autoComplete="new-password" />
                            </div>
                        </div>
                        <div className="form-group col-12">
                            <div className="">
                                <Control type="password" model=".password" className="form-control form-control-sm pt-4 pb-4" id="password" placeholder="Password" name="password" autoComplete="new-password" />
                            </div>
                        </div>
                        <div className="col-12"><button type="submit" className="btn btn-sm btn-success col-12" disabled={loginMessage.disabled} >Login</button></div>
                    </div>
                </div>
            </Form>

        </div>

    )
}

class Login extends Component {

    handleSubmitLogin = (values) => {
        this.props.postLoginUser(values.email, values.password);
    }

    componentDidMount() {
        if (cookie.load('userservice')) {
            if (this.props.businessList.message === 'initial') this.props.getBusinessList();
        }
        if (!cookie.load('userservice')) {
            if (this.props.businessList.message !== 'initial') this.props.refreshBusinessList();
        }
        if (this.props.registerMessage.message === 'success') {
            alert("Login to Continue - Account Created Successfully");
            this.props.resetRegisterForm()
        }
        if (this.props.registerMessage.error !== '') {
            alert(this.props.registerMessage.error);
        }
        if (this.props.loginMessage.error !== '') {
            alert(this.props.loginMessage.error);
        }
        if (this.props.loginMessage.message === 'success') {
            this.props.resetLoginForm();
        }
    }
    render() {
        if (this.props.loginMessage.loading === true) {
            return (
                <div>
                    <div className="mt-3 d-flex justify-content-center">
                        <button className="btn btn-danger">Logging In...  </button>
                    </div>
                </div>
            );
        }
        else if (cookie.load('userservice')) return <Redirect to="/business" />;
        else return (
            <div>
                <div className="d-flex justify-content-center" style={{ minHeight: "86vh", display: "flex", flexDirection: "column" }}>
                    <div className="container mt-5" style={{ flex: "1" }}>
                        <div className="row">
                            <div className="col-12 text-center" style={{ color: "#563d7c" }}><strong><h3>Aayuda</h3></strong></div>
                            <div className="col-12 text-center mt-3"><h3>Welcome To World of Business Managment</h3></div>
                            <div className="col-12 text-center mt-0" style={{ color: "grey" }}>(Accounting + HR + Inventory + Document Managment System)</div>
                            <div className="col-12 text-center mt-4">
                                <LoginFormComponent handleSubmit={this.handleSubmitLogin} loginMessage={this.props.loginMessage} />
                            </div>
                        </div>
                    </div>
                </div>
                <div className="d-flex justify-content-center mt-4" style={{ borderTop: "1px solid rgb(86, 61, 124)", color: "rgb(86, 61, 124)", flexShrink: "0" }}>
                    <div className="col-md-10 mt-2 small pt-2 pb-3" style={{ color: "rgb(86, 61, 124)" }}>
                        <div className="row">
                            <div className="col-12 col-md-4 d-none d-md-block">&copy; Aayuda Technologies</div>
                            <div className="col-12 col-md-4 text-center d-md-none">&copy; Aayuda Technologies</div>
                            <div className="col-md-4 d-none d-md-block"></div>
                            <div className="col-12 col-md-4 text-center">
                                <FontAwesomeIcon icon={faPhone} style={{}} /> &nbsp;+91 - 807 - 6212109
                        &nbsp;&nbsp;<FontAwesomeIcon icon={faEnvelope} style={{}} /> &nbsp;care@aayuda.com
                    </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Login;


