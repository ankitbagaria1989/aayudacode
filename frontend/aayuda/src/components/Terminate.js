import React, { Component } from 'react';
import cookie from 'react-cookie';
import { Redirect } from 'react-router-dom';
import { LocalForm } from 'react-redux-form';
import { Header } from './HeaderComponent';
import Calendar from 'react-calendar';
import 'react-calendar/dist/Calendar.css';
import { Footer } from './Footer';

class Terminate extends Component {
    constructor(props) {
        super(props);
        this.state = {
            date: new Date(),
            dot: new Date().toDateString().split(' ').slice(1).join(' ')
        }
    }
    setDate = (date) => {
        this.setState({
            ...this.state,
            date: date,
            dot: date.toDateString().split(' ').slice(1).join(' ')
        })
    }
    componentDidMount() {
        if (this.props.terminateEmployeeMessage.message === 'success') {
            alert("Employee Terminated SUccessfully");
            this.props.resetTerminateEmployeeMessage();
        }
        else if (this.props.terminateEmployeeMessage.error !== '') {
            alert(this.props.terminateEmployeeMessage.error);
            this.setState({
                ...this.props.terminateEmployeeMessage.state
            })
            this.props.resetTerminateEmployeeMessage();
        }
        if (this.props.logoutMessage.error !== '') {
            alert("Error Logging Out. Please Try Again");
            this.props.resetLogoutMessage();
        }
    }

    handleSubmit = () => {
        let employee = this.props.getEmployee.data.filter(e => e.employeeid === this.props.employeeid)[0];
        this.props.terminateEmployee(this.props.businessid, employee.employeeid, this.state);
    }
    render() {
        if (!cookie.load('userservice')) return <Redirect to="/" />
        else {
            if ((this.props.terminateEmployeeMessage.loading === true) || (this.props.logoutMessage.loading === true)) {
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div className="mt-3 d-flex justify-content-center">
                            <button className="btn btn-danger">Loading... </button>
                        </div>
                    </div>
                );
            }
            else {
                let business = this.props.businessList.data.filter(b => b.businessid === this.props.businessid)[0];
                let employee = this.props.getEmployee.data.filter(e => e.employeeid === this.props.employeeid)[0];
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div style={{ minHeight: "93vh", display: "flex", flexDirection: "column" }}>
                            <div className="container d-flex justify-content-center pr-0 pl-0" style={{ flex: "1" }}>
                                <div className="col-12 col-md-4">
                                    <div className="small pr-0 pl-0">
                                        <h6 className="text-center pl-2 pr-2 pt-4" style={{ lineHeight: "1" }}><strong>{business.businessname}</strong></h6>
                                        <h6 className="text-center pl-2 pr-2 mb-4" style={{ lineHeight: "1" }}>{business.alias ? "(" + business.alias + ")" : ''}</h6><h6 className="text-center pl-2 pr-2 mb-2"><strong><u>Select Termination Date</u></strong></h6>
                                        <h6 className="text-center pl-2 pr-2 mb-2"><strong><u>{employee.employeename}</u></strong></h6>
                                        <h6 className="text-center pl-2 pr-2 mb-2 text-danger"><strong><u>{employee.active === 0 ? "Terminated from " + employee.dot : ''}</u></strong></h6>
                                        <LocalForm onSubmit={(() => this.handleSubmit())}>
                                            <div className="justify-content-center mt-4">
                                                <div className="col-12">
                                                    <div className="">
                                                        <Calendar onChange={this.setDate} value={this.state.date} />
                                                    </div>
                                                </div>
                                                <div className="form-group col-12">
                                                    <div className="mt-3">{employee.active === 1 ? <button type="submit" className="btn btn-success btn-sm pl-4 pr-4" >Terminate</button> : ''}</div>
                                                </div>
                                            </div>
                                        </LocalForm>
                                    </div>
                                </div>
                            </div>
                            <Footer />
                        </div>
                    </div>
                );
            }
        }
    }
}

export default Terminate;