import React, { Component, useState } from 'react';
import cookie from 'react-cookie';
import { Redirect, Link } from 'react-router-dom';
import { Button, Collapse } from 'reactstrap';
import { Header } from './HeaderComponent';
import Calendar from 'react-calendar';
import 'react-calendar/dist/Calendar.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faAngleDoubleDown } from '@fortawesome/free-solid-svg-icons';
import { Footer } from './Footer';

const MarkAttendance = function ({ businessid, fyid, businessList, changeRedirectStatus, state }) {
    let business = businessList.data.filter(b => b.businessid === businessid)[0];
    let financialyear = undefined;
    business.financial.forEach(f => {
        if (f.fyid === fyid) financialyear = f.financialyear;
    })
    let date = new Date();
    let redirect = (newdate) => {
        date = newdate;
        let flag = true;
        if ([4, 5, 6, 7, 8, 9, 10, 11, 12].includes(date.getMonth())) {
            if (date.getFullYear() !== parseInt(financialyear.split("-")[0])) flag = false;
        }
        else {
            if (date.getFullYear() !== parseInt(financialyear.split("-")[1])) flag = false;
        }
        if (flag === true) {
            changeRedirectStatus(date);
        }
        else alert("Date must be in FY " + financialyear);
    }
    let [isOpen, setIsOpen] = useState(false);
    let toggle = () => setIsOpen((!isOpen));
    return (
        <div className="d-flex justify-content-center">
            <div className="col-12 mt-2">
                <Button className="mb-1 mt-3 col-12 btn-sm" style={{ backgroundColor: "white", color: "rgb(40, 167, 69)", border: "1px solid rgb(40, 167, 69)" }} onClick={toggle}><div className="d-flex"><span className="col-11" style={{ fontSize: "18px" }}>Mark Attendance</span><span className="col-1 pr-0 text-align-right"><FontAwesomeIcon size="sm" icon={faAngleDoubleDown} /></span></div></Button>
                <Collapse isOpen={isOpen}>
                    <Calendar onChange={redirect} value={state.date} className = "w-100" />
                </Collapse>
            </div>
        </div>);
}

const CalculateSalary = function ({ businessid, fyid }) {
    let [isOpen, setIsOpen] = useState(false);
    let toggle = () => setIsOpen((!isOpen));
    return (
        <div className="d-flex justify-content-center">
            <div className="col-12 pb-4 ">
                <Button className="mb-1 col-12 btn-sm mt-2" style={{ backgroundColor: "white", color: "rgb(40, 167, 69)", border: "1px solid rgb(40, 167, 69)" }} onClick={toggle}><div className="d-flex"><span className="col-11" style={{ fontSize: "18px" }}>Salary Sheet</span><span className="col-1 pr-0 text-align-right"><FontAwesomeIcon size="sm" icon={faAngleDoubleDown} /></span></div></Button>
                <Collapse isOpen={isOpen}>
                    <div className="mt-1 mb-1 pt-3 pb-3" style={{ backgroundColor: "#fff" }}>
                        <div className="row pl-2 pr-2">
                            <div className="col-3">
                                <Link to={`/business/businessdetail/salary/${businessid}/${fyid}/${'apr'}`}><Button className="btn-sm col-12 mb-1" style={{ backgroundColor: "#fff", border: "1px solid rgb(255, 123, 26)", color: "rgb(255, 123, 26)" }} >Apr</Button></Link>
                            </div>
                            <div className="col-3">
                                <Link to={`/business/businessdetail/salary/${businessid}/${fyid}/${'may'}`}><Button className="btn-sm col-12 mb-1" style={{ backgroundColor: "#fff", border: "1px solid rgb(255, 123, 26)", color: "rgb(255, 123, 26)" }} >May</Button></Link>
                            </div>
                            <div className="col-3">
                                <Link to={`/business/businessdetail/salary/${businessid}/${fyid}/${'jun'}`}><Button className="btn-sm col-12 mb-1" style={{ backgroundColor: "#fff", border: "1px solid rgb(255, 123, 26)", color: "rgb(255, 123, 26)" }} >Jun</Button></Link>
                            </div>
                            <div className="col-3">
                                <Link to={`/business/businessdetail/salary/${businessid}/${fyid}/${'jul'}`}><Button className="btn-sm col-12 mb-1" style={{ backgroundColor: "#fff", border: "1px solid rgb(255, 123, 26)", color: "rgb(255, 123, 26)" }} >Jul</Button></Link>
                            </div>
                            <div className="col-3">
                                <Link to={`/business/businessdetail/salary/${businessid}/${fyid}/${'aug'}`}><Button className="btn-sm col-12 mb-1" style={{ backgroundColor: "#fff", border: "1px solid rgb(255, 123, 26)", color: "rgb(255, 123, 26)" }} >Aug</Button></Link>
                            </div>
                            <div className="col-3">
                                <Link to={`/business/businessdetail/salary/${businessid}/${fyid}/${'sep'}`}><Button className="btn-sm col-12 mb-1" style={{ backgroundColor: "#fff", border: "1px solid rgb(255, 123, 26)", color: "rgb(255, 123, 26)" }} >Sep</Button></Link>
                            </div>
                            <div className="col-3">
                                <Link to={`/business/businessdetail/salary/${businessid}/${fyid}/${'oct'}`}><Button className="btn-sm col-12 mb-1" style={{ backgroundColor: "#fff", border: "1px solid rgb(255, 123, 26)", color: "rgb(255, 123, 26)" }} >Oct</Button></Link>
                            </div>
                            <div className="col-3">
                                <Link to={`/business/businessdetail/salary/${businessid}/${fyid}/${'nov'}`}><Button className="btn-sm col-12 mb-1" style={{ backgroundColor: "#fff", border: "1px solid rgb(255, 123, 26)", color: "rgb(255, 123, 26)" }} >Nov</Button></Link>
                            </div>
                            <div className="col-3">
                                <Link to={`/business/businessdetail/salary/${businessid}/${fyid}/${'dec'}`}><Button className="btn-sm col-12 mb-1" style={{ backgroundColor: "#fff", border: "1px solid rgb(255, 123, 26)", color: "rgb(255, 123, 26)" }} >Dec</Button></Link>
                            </div>
                            <div className="col-3">
                                <Link to={`/business/businessdetail/salary/${businessid}/${fyid}/${'jan'}`}><Button className="btn-sm col-12 mb-1" style={{ backgroundColor: "#fff", border: "1px solid rgb(255, 123, 26)", color: "rgb(255, 123, 26)" }} >Jan</Button></Link>
                            </div>
                            <div className="col-3">
                                <Link to={`/business/businessdetail/salary/${businessid}/${fyid}/${'feb'}`}><Button className="btn-sm col-12 mb-1" style={{ backgroundColor: "#fff", border: "1px solid rgb(255, 123, 26)", color: "rgb(255, 123, 26)" }} >Feb</Button></Link>
                            </div>
                            <div className="col-3">
                                <Link to={`/business/businessdetail/salary/${businessid}/${fyid}/${'mar'}`}><Button className="btn-sm col-12 mb-1" style={{ backgroundColor: "#fff", border: "1px solid rgb(255, 123, 26)", color: "rgb(255, 123, 26)" }} >Mar</Button></Link>
                            </div>
                        </div>
                    </div>
                </Collapse>
            </div>
        </div>);
}


const ViewAttendance = function ({ businessid, fyid }) {
    let [isOpen, setIsOpen] = useState(false);
    let toggle = () => setIsOpen((!isOpen));
    return (
        <div className="d-flex justify-content-center">
            <div className="col-12">
                <Button className="mb-1 col-12 btn-sm mt-2" style={{ backgroundColor: "white", color: "rgb(40, 167, 69)", border: "1px solid rgb(40, 167, 69)" }} onClick={toggle}><div className="d-flex"><span className="col-11" style={{ fontSize: "18px" }}>View Attendance</span><span className="col-1 pr-0 text-align-right"><FontAwesomeIcon size="sm" icon={faAngleDoubleDown} /></span></div></Button>
                <Collapse isOpen={isOpen}>
                    <div className="mt-1 mb-1 pt-3 pb-3" style={{ backgroundColor: "#fff" }}>
                        <div className="row pl-2 pr-2">
                            <div className="col-3">
                                <Link to={`/business/businessdetail/viewattendance/${businessid}/${fyid}/${'Apr'}`}><Button className="btn-sm col-12 mb-1" style={{ backgroundColor: "#fff", border: "1px solid rgb(255, 123, 26)", color: "rgb(255, 123, 26)" }} ><strong>Apr</strong></Button></Link>
                            </div>
                            <div className="col-3">
                                <Link to={`/business/businessdetail/viewattendance/${businessid}/${fyid}/${'May'}`}><Button className="btn-sm col-12 mb-1" style={{ backgroundColor: "#fff", border: "1px solid rgb(255, 123, 26)", color: "rgb(255, 123, 26)" }} ><strong>May</strong></Button></Link>
                            </div>
                            <div className="col-3">
                                <Link to={`/business/businessdetail/viewattendance/${businessid}/${fyid}/${'Jun'}`}><Button className="btn-sm col-12 mb-1 btn-info" style={{ backgroundColor: "#fff", border: "1px solid rgb(255, 123, 26)", color: "rgb(255, 123, 26)" }} ><strong>Jun</strong></Button></Link>
                            </div>
                            <div className="col-3">
                                <Link to={`/business/businessdetail/viewattendance/${businessid}/${fyid}/${'Jul'}`}><Button className="btn-sm col-12 mb-1 btn-info" style={{ backgroundColor: "#fff", border: "1px solid rgb(255, 123, 26)", color: "rgb(255, 123, 26)" }} ><strong>Jul</strong></Button></Link>
                            </div>
                            <div className="col-3">
                                <Link to={`/business/businessdetail/viewattendance/${businessid}/${fyid}/${'Aug'}`}><Button className="btn-sm col-12 mb-1 btn-info" style={{ backgroundColor: "#fff", border: "1px solid rgb(255, 123, 26)", color: "rgb(255, 123, 26)" }} ><strong>Aug</strong></Button></Link>
                            </div>
                            <div className="col-3">
                                <Link to={`/business/businessdetail/viewattendance/${businessid}/${fyid}/${'Sep'}`}><Button className="btn-sm col-12 mb-1 btn-info" style={{ backgroundColor: "#fff", border: "1px solid rgb(255, 123, 26)", color: "rgb(255, 123, 26)" }} ><strong>Sep</strong></Button></Link>
                            </div>
                            <div className="col-3">
                                <Link to={`/business/businessdetail/viewattendance/${businessid}/${fyid}/${'Oct'}`}><Button className="btn-sm col-12 mb-1 btn-info" style={{ backgroundColor: "#fff", border: "1px solid rgb(255, 123, 26)", color: "rgb(255, 123, 26)" }} ><strong>Oct</strong></Button></Link>
                            </div>
                            <div className="col-3">
                                <Link to={`/business/businessdetail/viewattendance/${businessid}/${fyid}/${'Nov'}`}><Button className="btn-sm col-12 mb-1 btn-info" style={{ backgroundColor: "#fff", border: "1px solid rgb(255, 123, 26)", color: "rgb(255, 123, 26)" }} ><strong>Nov</strong></Button></Link>
                            </div>
                            <div className="col-3">
                                <Link to={`/business/businessdetail/viewattendance/${businessid}/${fyid}/${'Dec'}`}><Button className="btn-sm col-12 mb-1 btn-info" style={{ backgroundColor: "#fff", border: "1px solid rgb(255, 123, 26)", color: "rgb(255, 123, 26)" }} ><strong>Dec</strong></Button></Link>
                            </div>
                            <div className="col-3">
                                <Link to={`/business/businessdetail/viewattendance/${businessid}/${fyid}/${'Jan'}`}><Button className="btn-sm col-12 mb-1 btn-info" style={{ backgroundColor: "#fff", border: "1px solid rgb(255, 123, 26)", color: "rgb(255, 123, 26)" }} ><strong>Jan</strong></Button></Link>
                            </div>
                            <div className="col-3">
                                <Link to={`/business/businessdetail/viewattendance/${businessid}/${fyid}/${'Feb'}`}><Button className="btn-sm col-12 mb-1 btn-info" style={{ backgroundColor: "#fff", border: "1px solid rgb(255, 123, 26)", color: "rgb(255, 123, 26)" }} ><strong>Feb</strong></Button></Link>
                            </div>
                            <div className="col-3">
                                <Link to={`/business/businessdetail/viewattendance/${businessid}/${fyid}/${'Mar'}`}><Button className="btn-sm col-12 mb-1 btn-info" style={{ backgroundColor: "#fff", border: "1px solid rgb(255, 123, 26)", color: "rgb(255, 123, 26)" }} ><strong>Mar</strong></Button></Link>
                            </div>
                        </div>
                    </div>
                </Collapse>
            </div>
        </div>);
}

class PayRollChoice extends Component {
    constructor(props) {
        super(props);
        this.state = {
            date: new Date(),
            redirect: false
        }
    }
    changeRedirectStatus = (value) => {
        this.setState({
            date: value,
            redirect: true
        })
    }
    componentDidMount() {
        if (this.props.logoutMessage.error !== '') {
            alert("Error Logging Out. Please Try Again");
            this.props.resetLogoutMessage();
        }
        if (this.props.getMonthAttendance.message !== 'initial') {
            this.props.resetGetMonthAttendance();
        }
        if (this.props.getSalary.message !== 'initial') {
            this.props.resetGetSalary();
        }
        if (this.props.getAttendance.message !== 'initial') {
            this.props.resetGetAttendance();
        }
    }

    render() {
        if (!cookie.load('userservice')) return <Redirect to="/" />
        if (this.state.redirect) {
            let completedate = this.state.date.toDateString().split(' ').slice(1).join(' ');
            let url = "/business/businessdetail/attendance/" + this.props.businessid + "/" + this.props.fyid + "/" + completedate;
            return <Redirect to={url} />
        }
        else {
            if ((this.props.logoutMessage.loading === true)) {
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div className="mt-3 d-flex justify-content-center">
                            <button className="btn btn-danger">Loading... </button>
                        </div>
                    </div>
                );
            }
            else {
                let business = this.props.businessList.data.filter(b => b.businessid === this.props.businessid)[0];
                let fy = business.financial.filter(f => f.fyid === this.props.fyid)[0].financialyear;
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div style={{ minHeight: "93vh", display: "flex", flexDirection: "column" }}>
                            <div className="container d-flex justify-content-center pr-0 pl-0" style={{ flex: "1" }}>
                                <div className="col-12 col-md-5">
                                    <div className="small pr-0 pl-0">
                                        <h6 className="text-center pl-2 pr-2 pt-4" style={{ lineHeight: "1" }}><strong>{business.businessname} {fy}</strong></h6>
                                        <h6 className="text-center pl-2 pr-2 mb-4" style={{ lineHeight: "1" }}>{business.alias ? "(" + business.alias + ")" : ''}</h6>
                                        <h6 className="text-center pl-2 pr-2"><strong><u>Attendance Register</u></strong></h6>
                                        <MarkAttendance businessid={this.props.businessid} fyid={this.props.fyid} businessList={this.props.businessList} changeRedirectStatus={this.changeRedirectStatus} state={this.state} />
                                        <ViewAttendance businessid={this.props.businessid} fyid={this.props.fyid} businessList={this.props.businessList} changeRedirectStatus={this.changeRedirectStatus} state={this.state} />
                                        <CalculateSalary businessid={this.props.businessid} fyid={this.props.fyid} businessList={this.props.businessList} />
                                    </div>
                                </div>
                            </div>
                            <Footer />
                        </div>
                    </div>
                );
            }
        }
    }
}

export default PayRollChoice;