import React, { Component } from 'react';
import cookie from 'react-cookie';
import { Redirect } from 'react-router-dom';
import { LocalForm, Control } from 'react-redux-form';
import { Header } from './HeaderComponent';
import { Footer } from './Footer';

class AssetDepreciation extends Component {
    constructor(props) {
        super(props);
        this.state = ({
            asset: '',
            remark: '',
            amount: 0,
            depreciateappreciate: ''
        })
        window.onload = () => {
            this.props.refreshAssetAccountState();
            this.props.refreshDepreciationState();
        }
    }
    componentDidMount() {
        if (this.props.addDepreciationMessage.message === 'success') {
            alert("Depreciation Added Successfully");
            this.setState({
                ...this.props.addDepreciationMessage.state
            })
            this.props.resetAddDepreciationMessage();
        }
        else if (this.props.addDepreciationMessage.error !== '') {
            alert(this.props.addDepreciationMessage.error);
            this.setState({
                ...this.props.addDepreciationMessage.state
            })
            this.props.resetAddDepreciationMessage();
        }
        if (this.props.getDepreciation.message === 'initial') this.props.getDepreciationList(this.props.businessid, this.props.fyid);
        if (this.props.getAssetAccount.message === 'initial') this.props.getAssetAccountList(this.props.businessid);
        if (this.props.logoutMessage.error !== '') {
            alert("Error Logging Out. Please Try Again");
            this.props.resetLogoutMessage();
        }
    }
    handleInputChange = (event) => {
        let target = event.target;
        let name = target.name;
        let value = target.value;
        let match = 0;
        if (name === 'asset') {
            this.setState ({
                asset: '',
                remark: '',
                amount: 0,
                depreciateappreciate: ''
            })
            this.props.getDepreciation.data.forEach(d => {
                if (d.asset === value) {
                    match = 1;
                    this.setState({
                        ...this.state,
                        asset: value,
                        amount: d.amount,
                        depreciateappreciate: d.depreciateappreciate
                    })
                }
            })
            this.props.getAssetAccount.data.forEach(b => {
                if (b.id === value) b.depreciating === '1' ? this.setState({
                    ...this.state,
                    asset:value,
                    depreciateappreciate : 'depreciate'}) : 
                    this.setState({
                        ...this.state,
                        asset : value,
                        depreciateappreciate : 'appreciate'
                    });
            })
        }
        else {
            this.setState({
                ...this.state,
                [name]: value
            })
        }
    }
    handleSubmit = () => {
        if (this.state.asset === '') alert("Please Select Asset");
        else if (isNaN(parseInt(this.state.amount)) || (this.state.amount < 0)) alert("Amount is mandatory and Must Be Positive Number");
        else if (this.state.remark.length > 199) alert("Remark Must Be Within 200 Characters");
        else this.props.raiseDepreciation(this.props.businessid, this.props.fyid, this.state);
    }
    render() {
        if (!cookie.load('userservice')) return <Redirect to="/" />;
        else {
            if ((this.props.addDepreciationMessage.loading === true) || (this.props.getDepreciation.loading === true) || (this.props.getAssetAccount.loading === true) || (this.props.logoutMessage.loading === true)) {
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div className="mt-3 d-flex justify-content-center">
                            <button className="btn btn-danger">Loading... </button>
                        </div>
                    </div>
                );
            }
            else if ((this.props.getDepreciation.error !== '') || (this.props.getAssetAccount.error !== '')) {
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div className="mt-3 d-flex justify-content-center">
                            <button className="btn btn-danger">Error.. Please Refresh and Try Agin </button>
                        </div>
                    </div>
                );
            }
            else {
                let business = this.props.businessList.data.filter(b => b.businessid === this.props.businessid)[0];
                let fy = business.financial.filter(f => f.fyid === this.props.fyid)[0].financialyear;
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div style={{ minHeight: "93vh", display: "flex", flexDirection: "column" }}>
                            <div className="container" style={{ flex: "1" }}>
                                <h6 className="text-center pl-2 pr-2 pt-4" style={{ lineHeight: "1" }}><strong>{business.businessname} {fy}</strong></h6>
                                <h6 className="text-center pl-2 pr-2 mb-4" style={{ lineHeight: "1" }}>{business.alias ? "(" + business.alias + ")" : ''}</h6>
                                <h6 className="text-center pl-2 pr-2"><strong><u>Asset Depreciation/Appreciation for {fy}</u></strong></h6>
                                <div className="row" >
                                    <LocalForm className="container small" onSubmit={this.handleSubmit}>
                                        <div className="mt-4 d-flex justify-content-center">
                                            <div class="row col-12 col-md-4">
                                                <div className="form-group mb-1 col-12">
                                                    <label htmlFor="asset" className="mb-0 muted-text"><strong>Select Asset Account<span className="text-danger">*</span></strong></label>
                                                    <Control.select model=".asset" className="form-control form-control-sm" id="asset" name="asset" onChange={this.handleInputChange} value={this.state.asset}>
                                                        <option value="">Select Asset Account</option>
                                                        {this.props.getAssetAccount.data.map(b => {
                                                            if ((!b.sold) || (b.sold === "1") && (new Date(b.solddate) >= new Date("01 Apr " + fy.split("-")[0]))) return <option value={b.id}>{b.assetaccount}</option>
                                                        })}
                                                    </Control.select>
                                                </div>
                                                {this.state.asset !== '' ? <div className="form-group mb-1 col-12">
                                                    <label htmlFor="amount" className="mb-0 muted-text"><strong>Amount ({this.state.depreciateappreciate})<span className="text-danger">*</span></strong></label>
                                                    <Control.text model=".amount" className="form-control form-control-sm" id="amount" name="amount" onChange={this.handleInputChange} value={this.state.amount} />
                                                </div> : ''}
                                                {this.state.asset !== '' ? <div className="form-group mb-1 col-12">
                                                    <label htmlFor="remark" className="mb-0 muted-text"><strong>Remarks</strong></label>
                                                    <Control.text model=".remark" className="form-control form-control-sm" id="remark" name="remark" onChange={this.handleInputChange} value={this.state.remark} />
                                                </div> : ''}
                                                {this.state.asset !== '' ? <div className="form-group mb-1 col-12">
                                                    <button type="submit" className="btn btn-success mt-1 btn-sm small" >Update</button>
                                                </div> : ''}
                                            </div>
                                        </div>
                                    </LocalForm>
                                </div>
                            </div>
                            <Footer />
                        </div>
                    </div>
                )
            }
        }
    }
}

export default AssetDepreciation;