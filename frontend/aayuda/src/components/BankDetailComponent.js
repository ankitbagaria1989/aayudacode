import React, { Component, useState } from 'react';
import cookie from 'react-cookie';
import { Redirect, Link } from 'react-router-dom';
import { Button, Collapse } from 'reactstrap';
import { Form, Errors, Control } from 'react-redux-form';
import { Header } from './HeaderComponent';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faAngleDoubleDown } from '@fortawesome/free-solid-svg-icons';
import { Footer } from './Footer';

const required = (val) => val && val.length;
const maxLength = (len) => val => val ? val.length < len : true;
const isNumber = (val) => !isNaN(parseInt(val));
const ErrorComponent = function ({ children }) {
    return (
        <div style={{ lineHeight: "1" }}>{children}</div>
    )
}
const AddBankDetail = function ({ handleSubmit, addBankDetailMessage, businessid }) {
    let [isOpen, setIsOpen] = useState(false);
    let toggle = () => setIsOpen((!isOpen));
    if (addBankDetailMessage.error !== '') {
        isOpen = true;
    }
    return (
        <div className="col-12">
            <Button className="btn-sm mt-3 col-12" style={{ backgroundColor: "#fff", border: "1px solid #28a745", color: "#28a745" }} onClick={toggle}><div className="d-flex"><span className="col-11 text-center">Add a New Bank Account</span><span className="col-1 p-0 text-right"><FontAwesomeIcon size="sm" icon={faAngleDoubleDown} /></span></div></Button>
            <Collapse isOpen={isOpen}>
                <Form model="addBankDetail" onSubmit={((values) => handleSubmit(values, businessid))}>
                    <div className="justify-content-center mt-4">
                        <div className="form-group col-12 mb-1">
                            <label htmlFor="bankname" className="mb-0"><span className="muted-text">Bank Name<span className="text-danger">*</span></span></label>
                            <div className="">
                                <Control.text model=".bankname" className="form-control form-control-sm pt-3 pb-3" id="bankname" name="bankname" placeholder="Bank Name" validators={{ required, maxLength: maxLength(100) }} />
                                <Errors className="text-danger" model=".bankname" show="touched" messages={{
                                    required: "Bank Name is Mandatory to Register",
                                    maxLength: "length must be within 100 characters"
                                }}
                                    component={ErrorComponent}
                                />
                            </div>
                        </div>
                        <div className="form-group col-12 mb-1">
                            <label htmlFor="accountnumber" className="mb-0 muted-text">Account Number<span className="text-danger">*</span></label>
                            <div className="">
                                <Control.text model=".accountnumber" className="form-control form-control-sm pt-3 pb-3" id="accountnumber" name="accountnumber" placeholder="Account Number" validators={{ required, isNumber, maxLength: maxLength(20) }} />
                                <Errors className="text-danger" model=".accountnumber" show="touched" messages={{
                                    required: "Account Number is mandatory",
                                    isnumber: "Only Number Allowed",
                                    maxLength: "length must be within 20 digits"
                                }}
                                    component={ErrorComponent}
                                />
                            </div>
                            <div className="mt-3"><button type="submit" className="btn btn-success btn-sm pl-4 pr-4" >Add</button></div>
                        </div>
                    </div>
                </Form>
            </Collapse>
        </div>
    );
}

const BankDetailListCard = function ({ bankDetail }) {
    let [isOpen, setIsOpen] = useState(false);
    let toggle = () => setIsOpen((!isOpen));

    return (
        <div className="col-12 justify-content-center">
            <div className="">
                <Button className="btn-sm btn mt-2 col-12 btn-success" onClick={toggle}><div className="d-flex"><span className="col-11">{bankDetail.bankname}</span><span className="col-1 p-0 text-right"><FontAwesomeIcon size="sm" icon={faAngleDoubleDown} /></span></div></Button>
                <Collapse isOpen={isOpen}>
                    <div className="card card-body ">
                        <div>Account Number : {bankDetail.accountnumber}</div>
                        <div><Link to={`/business/businessdetail/bankdetail/${bankDetail.businessid}/${bankDetail.bankdetailid}`}> <button className="btn btn-sm p-1 justify-content-right mt-2 pl-4 pr-4" role="button" style={{ backgroundColor: "#fff", border: "1px solid #28a745", color: "#28a745" }}>Edit</button></Link></div>
                    </div>
                </Collapse>
            </div>
        </div>
    );
}

const BankDetailList = function ({ getBankDetail }) {
    if ((getBankDetail.error === '')) {
        if (getBankDetail.data.length > 0) return getBankDetail.data.map((bankDetail) => <BankDetailListCard bankDetail={bankDetail} />);
        else return <h6 className="col-12 text-center text-muted mt-2 pb-4">No Bank Account Added Yet</h6>;
    }
    else return <div classname="row col-12 card text-center"></div>;
}

class BankDetail extends Component {
    componentDidMount() {
        window.onload = () => {
            this.props.refreshBankState();
        }
        if (this.props.getBankDetail.message === 'initial') this.props.getBankDetailList(this.props.businessid);
        if (this.props.addBankDetailMessage.error !== '') {
            alert("Error " + this.props.addBankDetailMessage.error);
            this.props.resetAddBankDetailMessage();
        }
        if (this.props.addBankDetailMessage.message === 'success') {
            alert("Bank Detail Added Successfully");
            this.props.resetAddBankMessageAndForm();
        }
        if (this.props.logoutMessage.error !== '') {
            alert("Error Logging Out. Please Try Again");
            this.props.resetLogoutMessage();
        }
    }

    handleSubmit = (values, businessid) => {
        this.props.addBankDetail(values, businessid);
        //if (this.props.addFinancialYear.message === "success") window.location.reload();
    }

    render() {
        if (!cookie.load('userservice')) return <Redirect to="/" />
        else {
            if ((this.props.getBankDetail.loading === true) || (this.props.addBankDetailMessage.loading === true) || (this.props.logoutMessage.loading === true)) {
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div className="mt-3 d-flex justify-content-center">
                            <button className="btn btn-danger">Loading... </button>
                        </div>
                    </div>
                );
            }
            else if (this.props.getBankDetail.error !== '') {
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div className="mt-3 d-flex justify-content-center">
                            <button className="btn btn-danger">Error.. Please Refresh and Try Agin </button>
                        </div>
                    </div>
                );
            }
            else {
                let business = this.props.businessList.data.filter(b => b.businessid === this.props.businessid)[0];
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        
                        <div style={{ minHeight: "93vh", display: "flex", flexDirection: "column" }}>
                            <div className="container d-flex justify-content-center pr-0 pl-0">
                                <div className="col-12 col-md-5">
                                    <div className="small pr-0 pl-0">
                                        <h6 className="text-center pl-2 pr-2 pt-4" style={{ lineHeight: "1" }}><strong>{business.businessname}</strong></h6>
                                        <h6 className="text-center pl-2 pr-2 mb-4" style={{ lineHeight: "1" }}>{business.alias ? "(" + business.alias + ")" : ''}</h6>
                                        <h6 className="text-center pl-2 pr-2 mb-2"><strong><u>Bank Accounts</u></strong></h6>
                                        <AddBankDetail handleSubmit={this.handleSubmit} addBankDetailMessage={this.props.addBankDetailMessage} businessid={this.props.businessid} />
                                    </div>
                                </div>
                            </div>
                            <div className="container d-flex justify-content-center pr-0 pl-0 mt-4 pb-4" style={{ flex: "1" }}>
                                <div className="col-12 col-md-5">
                                    <div className="small pr-0 pl-0">
                                        <BankDetailList getBankDetail={this.props.getBankDetail} />
                                    </div>
                                </div>
                            </div>
                            <Footer />
                        </div>
                    </div>
                );
            }
        }
    }
}

export default BankDetail;