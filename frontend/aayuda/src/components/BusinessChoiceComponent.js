import React, { Component } from 'react';
import cookie from 'react-cookie';
import { Redirect, Link } from 'react-router-dom';
import { Button } from 'reactstrap';
import { Header } from './HeaderComponent';
import { Footer } from './Footer';

const Accounting = function ({ handleSubmit, businessid, fyid }) {
    return (
        <div className="d-flex justify-content-center mt-4">
            <div className="col-12 mt-2">
                <Link to={`/business/businessdetail/businessprocess/${businessid}/${fyid}`}><Button className="col-12 mb-3 pt-1 pb-1 btn-sm" style={{ backgroundColor: "white", color: "rgb(40, 167, 69)", border: "1px solid rgb(40, 167, 69)", fontSize: "18px" }}>Accounting</Button></Link>
            </div>
        </div>);
}
const PayRoll = function ({ handleSubmit, businessid, fyid }) {
    return (
        <div className="d-flex justify-content-center">
            <div className="col-12 pb-4">
                <Link to={`/business/businessdetail/payroll/${businessid}/${fyid}`}><Button className="col-12 mb-2 pt-1 pb-1 btn-sm" style={{ backgroundColor: "white", color: "rgb(40, 167, 69)", border: "1px solid rgb(40, 167, 69)", fontSize: "18px" }}>Attendance Register</Button></Link>
            </div>
        </div>);
}
const Inventory = function ({ handleSubmit, businessid, fyid }) {
    return (
        <div className="d-flex justify-content-center">
            <div className="col-12">
                <Link to={`/business/businessdetail/inventory/${businessid}/${fyid}`}><Button className="col-12 mb-3 pt-1 pb-1 btn-sm" style={{ backgroundColor: "white", color: "rgb(40, 167, 69)", border: "1px solid rgb(40, 167, 69)", fontSize: "18px" }}>Inventory</Button></Link>
            </div>
        </div>);
}
class BusinessChoice extends Component {
    componentDidMount() {
        if ((this.props.getCustomer.message === 'initial') && (this.props.getCustomer.loading === false)) this.props.getCustomerList(this.props.businessid);
        if ((this.props.getItem.message === 'initial') && (this.props.getItem.loading === false)) this.props.getItemList(this.props.businessid);
        if ((this.props.getBankDetail.message === 'initial') && (this.props.getBankDetail.loading === false)) this.props.getBankDetailList(this.props.businessid);
        if ((this.props.salesDetail.message === 'initial') && (this.props.salesDetail.loading === false)) this.props.getSalesData(this.props.businessid, this.props.fyid);
        if ((this.props.purchaseDetail.message === 'initial') && (this.props.purchaseDetail.loading === false)) this.props.getPurchaseData(this.props.businessid, this.props.fyid);
        if ((this.props.getExpense.message === 'initial') && (this.props.getExpense.loading === false)) this.props.getExpenseDetail(this.props.businessid, this.props.fyid);
        if ((this.props.getBankEntry.message === 'initial') && (this.props.getBankEntry.loading === false)) this.props.getBankEntryDetail(this.props.businessid, this.props.fyid);
        if ((this.props.getOpeningClosing.message === 'initial') && (this.props.getOpeningClosing.loading === false)) this.props.getOpeningClosingDetail(this.props.businessid, this.props.fyid);
        if ((this.props.getOpeningBalance.message === 'initial') && (this.props.getOpeningBalance.loading === false)) this.props.getPartyOpeningBalance(this.props.businessid, this.props.fyid);
        if ((this.props.getCashInHand.message === 'initial') && (this.props.getCashInHand.loading === false)) this.props.getCashInHandDetail(this.props.businessid, this.props.fyid);
        if ((this.props.getDebitCredit.message === 'initial') && (this.props.getDebitCredit.loading === false)) this.props.getDebitCreditList(this.props.businessid, this.props.fyid);
        if (this.props.getBankOpeningBalance.message === 'initial') this.props.getBankOpeningBalanceList(this.props.businessid, this.props.fyid);
        if (this.props.getLoan.message === 'initial') this.props.getLoanDetail(this.props.businessid, this.props.fyid);
        if (this.props.getInvestment.message === 'initial') this.props.getInvestmentDetail(this.props.businessid, this.props.fyid);
        if (this.props.getLoanOpeningBalance.message === 'initial') this.props.getLoanOpeningBalanceList(this.props.businessid, this.props.fyid);
        if (this.props.getDepreciation.message === 'initial') this.props.getDepreciationList(this.props.businessid, this.props.fyid);
        if (this.props.getAssetOpeningBalance.message === 'initial') this.props.getAssetOpeningBalanceList(this.props.businessid, this.props.fyid);
        if (this.props.getInvestmentOpeningBalance.message === 'initial') this.props.getInvestmentOpeningBalanceList(this.props.businessid, this.props.fyid);
        if (this.props.getTDSOpeningBalance.message === 'initial') this.props.getTDSOpeningBalanceList(this.props.businessid, this.props.fyid);
        if (this.props.getIncomeTaxOpeningBalance.message === 'initial') this.props.getIncomeTaxOpeningBalanceList(this.props.businessid, this.props.fyid);
        if (this.props.getEmployee.message === 'initial') this.props.getEmployeeList(this.props.businessid);
        if (this.props.getEmployeeOpeningBalance.message === 'initial') this.props.getEmployeeOpeningBalanceList(this.props.businessid, this.props.fyid);
        if (this.props.logoutMessage.error !== '') {
            alert("Error Logging Out. Please Try Again");
            this.props.resetLogoutMessage();
        }
    }

    render() {
        if (!cookie.load('userservice')) return <Redirect to="/" />
        else {
            if ((this.props.logoutMessage.loading === true)) {
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div className="mt-3 d-flex justify-content-center">
                            <button className="btn btn-danger">Loading... </button>
                        </div>
                    </div>
                );
            }
            else {
                let business = this.props.businessList.data.filter(b => b.businessid === this.props.businessid)[0];
                let fy = business.financial.filter(f => f.fyid === this.props.fyid)[0].financialyear;
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div style={{ minHeight: "93vh", display: "flex", flexDirection: "column" }}>
                            <div className="container d-flex justify-content-center pr-0 pl-0" style={{ flex: "1" }}>
                                <div className="col-12 col-md-5">
                                    <div className="pr-0 pl-0">
                                        <h6 className="text-center pl-2 pr-2 pt-4" style={{ lineHeight: "1" }}><strong>{business.businessname} {fy}</strong></h6>
                                        <h6 className="text-center pl-2 pr-2 mb-4" style={{ lineHeight: "1" }}>{business.alias ? "(" + business.alias + ")" : ''}</h6>
                                        <h6 className="text-center pl-2 pr-2 mb-4 "><strong><u>Select Activity</u></strong></h6>
                                        <Accounting businessid={this.props.businessid} fyid={this.props.fyid} />
                                        <Inventory businessid={this.props.businessid} fyid={this.props.fyid} />
                                        <PayRoll businessid={this.props.businessid} fyid={this.props.fyid} />
                                    </div>
                                </div>
                            </div>
                            <Footer />
                        </div>
                    </div>
                );
            }
        }
    }
}

export default BusinessChoice;