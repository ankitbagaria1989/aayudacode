import React, { Component } from 'react';
import cookie from 'react-cookie';
import { Redirect } from 'react-router-dom';
import { LocalForm, Control } from 'react-redux-form';
import { Header } from './HeaderComponent';
import { Footer } from './Footer';

const DataDisplay = function ({ state, salesDetail, purchaseDetail, getPayment, fyid, businessList, businessid, handleInputChange, handleSubmit }) {
    let returnrows = [];
    if (state.name !== '') {
        if (state.customerorvendor === 'customer') {
            let business = businessList.data.filter(b => b.businessid === businessid)[0];
            let financial = business.financial.filter(f => f.fyid === fyid)[0];
            returnrows.push(
                <LocalForm className="small col-12" onSubmit={handleSubmit}>
                    <div className="form-group mb-1 col-12">
                        <label><input type="radio" name="amounttype" id="amounttype" value="receivable" checked={state.amounttype === "receivable" ? true : false} onChange={handleInputChange} /> Receivable </label>&nbsp;&nbsp;&nbsp;&nbsp;
                        <label><input type="radio" name="amounttype" id="amounttype" value="receivedadvance" checked={state.amounttype === "receivedadvance" ? true : false} onChange={handleInputChange} /> Received Advance</label><br />
                    </div>
                    <div className="form-group mb-1">
                        <label htmlFor="openingbalance" className="mb-0 muted-text"><strong>Amount<span className="text-danger">*</span></strong></label>
                        <Control.text model=".openingbalance" className="form-control form-control-sm" id="openingbalance" name="openingbalance" onChange={handleInputChange} value={state.openingbalance} />
                        <button type="submit" className="btn btn-success mt-1 btn-sm small" >Update</button>
                    </div>
                </LocalForm>
            )
        }
        else {
            let business = businessList.data.filter(b => b.businessid === businessid)[0];
            let financial = business.financial.filter(f => f.fyid === fyid)[0];
            returnrows.push(
                <LocalForm className="small col-12" onSubmit={handleSubmit}>
                    <div className="form-group mb-1 col-12">
                        <label><input type="radio" name="amounttype" id="amounttype" value="payable" checked={state.amounttype === "payable" ? true : false} onChange={handleInputChange} /> Payable </label>&nbsp;&nbsp;&nbsp;&nbsp;
                        <label><input type="radio" name="amounttype" id="amounttype" value="paidadvance" checked={state.amounttype === "paidadvance" ? true : false} onChange={handleInputChange} /> Paid Advance</label><br />
                    </div>
                    <div className="form-group mb-1">
                        <label htmlFor="openingbalance" className="mb-0 muted-text"><strong>Amount<span className="text-danger">*</span></strong></label>
                        <Control.text model=".openingbalance" className="form-control form-control-sm" id="openingbalance" name="openingbalance" onChange={handleInputChange} value={state.openingbalance} />
                        <button type="submit" className="btn btn-success mt-1 btn-sm small" >Update</button>
                    </div>
                </LocalForm>
            )
        }
    }
    return returnrows;
}

const SelectParty = function ({ customer, vendor, state, handleInputChange }) {
    let returnrows = [];
    if (state.customerorvendor === 'vendor') {
        returnrows.push(<div className="form-group mb-1 col-12">
            <label htmlFor="vendorid" className="mb-0 muted-text"><strong>Select Vendor:<span className="text-danger">*</span></strong></label>
            <Control.select model=".vendorid" className="form-control form-control-sm" id="name" name="name" onChange={handleInputChange} value={state.name}>
                <option value="">Select Vendor</option>
                {vendor.map(v => <option value={v.vendorid + "...." + v.businessname}>{v.businessname}</option>)}
            </Control.select>
        </div>)
    }
    else if (state.customerorvendor === 'customer') {
        returnrows.push(<div className="form-group mb-1 col-12">
            <label htmlFor="customerid" className="mb-0 muted-text"><strong>Select Customer:<span className="text-danger">*</span></strong></label>
            <Control.select model=".customerid" className="form-control form-control-sm" id="customerid" name="name" onChange={handleInputChange} value={state.name}>
                <option value="">Select Customer</option>
                {customer.map(c => <option value={c.customerid + "...." + c.businessname}>{c.businessname}</option>)}
            </Control.select>
        </div>)
    }
    return returnrows;
}

class OpeningBalance extends Component {
    constructor(props) {
        super(props);
        this.state = ({
            customerorvendor: '',
            name: '',
            openingbalance: 0,
            amounttype: ''
        })
        window.onload = () => {
            this.props.refreshVendorState();
            this.props.refreshCustomerState();
            this.props.resetOpeningBalance();
        }
    }
    handleSubmit = () => {
        if ((this.state.openingbalance === '') || (this.state.openingbalance < 0) || (isNaN(parseInt(this.state.openingbalance)))) alert(" Opening Balance muste be greater than equal to 0 ");
        else this.props.updatePartyOpeningBalance(this.state, this.props.businessid, this.props.fyid);
    }
    componentDidMount() {
        if (this.props.updatePartyOpeningMessage.error !== '') {
            alert("Error Updating Data");
            this.setState({
                ...this.props.updatePartyOpeningMessage.state
            })
            this.props.refreshUpdatepartyOpeningBalance();
        }
        if (this.props.updatePartyOpeningMessage.message === 'success') {
            alert("Data Updated Successfully");
            this.setState({
                ...this.props.updatePartyOpeningMessage.state
            })
            this.props.refreshUpdatepartyOpeningBalance();
        }
        if ((this.props.getVendor.message === 'initial') && (this.props.getVendor.loading === false)) this.props.getVendorList(this.props.businessid);
        if ((this.props.getCustomer.message === 'initial') && (this.props.getCustomer.loading === false)) this.props.getCustomerList(this.props.businessid);
        if ((this.props.getOpeningBalance.message === 'initial') && (this.props.getOpeningBalance.loading === false)) this.props.getPartyOpeningBalance(this.props.businessid, this.props.fyid);
        if (this.props.logoutMessage.error !== '') {
            alert("Error Logging Out. Please Try Again");
            this.props.resetLogoutMessage();
        }
    }

    handleInputChange = (event) => {
        let target = event.target;
        let name = target.name;
        let value = target.value;
        if (/^customerorvendor/i.test(name)) {
            this.setState({
                ...this.state,
                customerorvendor: value,
                name: '',
                openingbalance: '',
                amounttype: ''
            })
        }
        else if (/^name/i.test(name)) {
            let data = this.props.getOpeningBalance.data.filter(d => d.businessid === this.props.businessid && d.fyid === this.props.fyid && d.partyid === value.split("....")[0])[0];
            let openingbalance = data ? data.amount : 0
            this.setState({
                ...this.state,
                openingbalance: openingbalance,
                name: value,
                amounttype : data ? data.amounttype : ''
            })
        }
        else {
            this.setState({
                ...this.state,
                [name]: value
            })
        }
    }

    render() {
        if (!cookie.load('userservice')) return <Redirect to="/" />;
        else {
            if ((this.props.updatePartyOpeningMessage.loading === true) || (this.props.getOpeningBalance.loading === true) || (this.props.getVendor.loading === true) || (this.props.getCustomer.loading === true)) {
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div className="mt-3 d-flex justify-content-center">
                            <button className="btn btn-danger">Loading... </button>
                        </div>
                    </div>
                );
            }
            else if ((this.props.getOpeningBalance.error !== '') || (this.props.getVendor.error !== '') || (this.props.getCustomer.error !== '')) {
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div className="mt-3 d-flex justify-content-center">
                            <button className="btn btn-danger">Error. Please Refresh and Try Again</button>
                        </div>
                    </div>
                );
            }
            else {
                let business = this.props.businessList.data.filter(b => b.businessid === this.props.businessid)[0];
                let fy = business.financial.filter(f => f.fyid === this.props.fyid)[0].financialyear;
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div style={{ minHeight: "93vh", display: "flex", flexDirection: "column" }}>
                            <div className="container" style={{ flex: "1" }}>
                                <h6 className="text-center pl-2 pr-2 pt-4" style={{ lineHeight: "1" }}><strong>{business.businessname} {fy}</strong></h6>
                                <h6 className="text-center pl-2 pr-2 mb-4" style={{ lineHeight: "1" }}>{business.alias ? "(" + business.alias + ")" : ''}</h6>
                                <h6 className="text-center pl-2 pr-2"><strong><u>Opening Balance for Vendor/Customer</u></strong></h6>
                                <div className="row" >
                                    <LocalForm className="small col-12">
                                        <div className="container mt-4 d-flex justify-content-center">
                                            <div class="row col-12 col-md-4">
                                                <div className="form-group mb-1 col-12">
                                                    <label htmlFor="paymenttype" className="mb-0 muted-text"><strong>Customer/Vendor:<span className="text-danger">*</span></strong></label>
                                                    <Control.select model=".paymenttype" className="form-control form-control-sm" id="paymenttype" name="customerorvendor" onChange={this.handleInputChange} value={this.state.customerorvendor}>
                                                        <option value="">Select Vendor/Customer</option>
                                                        <option value="vendor">Vendor</option>
                                                        <option value="customer">Customer</option>
                                                    </Control.select>
                                                </div>
                                                < SelectParty customer={this.props.getCustomer.data} vendor={this.props.getVendor.data} state={this.state} handleInputChange={this.handleInputChange} />
                                            </div>
                                        </div>
                                    </LocalForm>
                                </div>
                                <div className="row">
                                    <div className="container mt-4 d-flex justify-content-center">
                                        <div class="row col-12 col-md-4">
                                            <DataDisplay state={this.state} salesDetail={this.props.salesDetail} purchaseDetail={this.props.purchaseDetail} getPayment={this.props.getPayment} fyid={this.props.fyid} businessList={this.props.businessList} businessid={this.props.businessid} handleInputChange={this.handleInputChange} handleSubmit={this.handleSubmit} />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <Footer />
                        </div>
                    </div>
                )
            }
        }
    }
}

export default OpeningBalance;