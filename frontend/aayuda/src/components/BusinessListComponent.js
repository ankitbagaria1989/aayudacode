import React, { Component } from 'react';
import cookie from 'react-cookie';
import { Redirect, Link } from 'react-router-dom';
import { Header } from './HeaderComponent';
import { Footer } from './Footer';

const BusinessCard = function ({ businessList, handleSubmit, message }) {
    if (businessList.data.length === 0) {
        return (
            <div className="card col-12">
                <div className="card-body">
                    <h5 className="card-title mb-2 text-muted text-center">You have not added any Business. Please add a Business. </h5>
                </div>
            </div>
        );
    }
    else if (businessList.data.length > 0) {
        return (
            businessList.data.map((business) => {
                return (
                    <div className="col-12 mt-2 p-0">
                        <div className="col-12 p-0">
                            <div className="p-1" style={{ backgroundColor: "white", color: "rgb(40, 167, 69)", border: "1px solid rgb(40, 167, 69)" }}>
                                <Link to={`/business/businessdetail/${business.businessid}`} style={{ textDecoration: "none" }}><div className="card-title mb-0 text-center" style={{ "color": "rgb(40, 167, 69)" }}>{business.businessname.toUpperCase()}</div></Link>
                            </div>
                        </div>
                    </div>
                );
            })
        );
    }
}

class BusinessList extends Component {
    constructor(props) {
        super(props);
        window.onload = () => {
            this.props.refreshBusinessList();
        }
    }
    componentDidMount() {
        if ((this.props.businessList.message === "initial") && (this.props.businessList.error === "") && (this.props.businessList.loading === false)) this.props.getBusinessList();
        if (this.props.getCustomer.message !== 'initial') this.props.refreshCustomerState();
        if (this.props.getVendor.message !== 'initial') this.props.refreshVendorState();
        if (this.props.getItem.message !== 'initial') this.props.refreshItemState();
        if (this.props.getBankDetail.message !== 'initial') this.props.refreshBankState();
        if (this.props.getExpenseHead.message !== 'initial') this.props.refreshExpenseHead();
        if (this.props.getEmployee.message !== 'initial') this.props.refreshEmployeeState();
        if (this.props.businessList.message === 'success' && this.props.individualBusinessMessage.message === 'initial') this.props.consumeIndividualBusinessMessage();
        if (this.props.getLoanAccount.message !== 'initial') this.props.refreshLoanAccountState();
        if (this.props.getAssetAccount.message !== 'initial') this.props.refreshAssetAccountState();
        if (this.props.getInvestmentAccount.message !== 'initial') this.props.refreshInvestmentAccountState();

        if (this.props.logoutMessage.error !== '') {
            alert("Error Logging Out. Please Try Again");
            this.props.resetLogoutMessage();
        }
    }
    render() {
        if (!cookie.load('userservice')) return <Redirect to="/" />
        else {
            if ((this.props.businessList.loading === true) || (this.props.logoutMessage.loading === true)) {
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div className="mt-3 d-flex justify-content-center">
                            <button className="btn btn-danger">Loading...  </button>
                        </div>
                    </div>
                );
            }
            else if (this.props.businessList.error !== "") {
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div className="mt-3 d-flex justify-content-center">
                            <button className="btn btn-danger">Error. Please Refresh and Try Again</button>
                        </div>
                    </div>
                );
            }
            else {
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div style={{ minHeight: "81vh", display: "flex", flexDirection: "column" }}>
                            <div className="mt-3 d-flex justify-content-center">
                                <Link to={`/business/addbusiness`} style={{ textDecoration: "none" }}><button className="btn btn-success btn-sm pl-3 pr-3 pt-1 pb-1 " >Add a New Business</button></Link>
                            </div>
                            <div className="container pt-3 d-flex justify-content-center">
                                <div className="col-md-6 p-0">
                                    <div className="">
                                        <BusinessCard businessList={this.props.businessList} handleSubmit={this.handleSubmit} message={this.props.addFinancialYearMessage.message} />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <Footer />
                    </div >
                );
            }
        }
    }
}

export default BusinessList;