import React, { Component } from 'react';
import cookie from 'react-cookie';
import { Redirect } from 'react-router-dom';
import { LocalForm, Control } from 'react-redux-form';
import { Header } from './HeaderComponent';
import { Footer } from './Footer';
const Option = function ({ data, type }) {
    let arr = [];
    if (type === 'employee') {
        for (let i = -1; i < data.length; i++) {
            if (i === -1) arr.push(<option value=''>Select Employee Account:</option>);
            else arr.push(<option value={data[i].employeeid + "..." + data[i].employeename}>{data[i].employeename}</option>);
        }
    }
    else {
        for (let i = -1; i < data.length; i++) {
            if (i === -1) arr.push(<option value=''>Select Partner Account:</option>);
            else arr.push(<option value={data[i].id + "..." + data[i].investmentaccount}>{data[i].investmentaccount}</option>);
        }
    }

    return arr;
}

const SelectParty = function ({ customer, vendor, state, handleInputChange, expensehead }) {
    let returnrows = [];
    if (state.paymenttype === 'paymentmade') {
        returnrows.push(<div className="form-group mb-1 col-12">
            <label htmlFor="vendorid" className="mb-0 muted-text"><strong>Select Vendor Account:<span className="text-danger">*</span></strong></label>
            <Control.select model=".vendorid" className="form-control form-control-sm" id="vendorid" name="vendorid" onChange={handleInputChange} value={state.vendorid}>
                <option value="">Select Vendor</option>
                {vendor.map(v => <option value={v.vendorid + "..." + v.businessname}>{v.alias ? v.alias : v.businessname}</option>)}
            </Control.select>
        </div>);
    }
    else if (state.paymenttype === 'paymentreceived') {
        returnrows.push(<div className="form-group mb-1 col-12">
            <label htmlFor="customerid" className="mb-0 muted-text"><strong>Select Customer Account:<span className="text-danger">*</span></strong></label>
            <Control.select model=".customerid" className="form-control form-control-sm" id="customerid" name="customerid" onChange={handleInputChange} value={state.customerid}>
                <option value="">Select Customer</option>
                {customer.map(c => <option value={c.customerid + "..." + c.businessname}>{c.alias ? c.alias : c.businessname}</option>)}
            </Control.select>
        </div>)
    }
    return returnrows;
}

class Payment extends Component {
    constructor(props) {
        super(props);
        this.state = ({
            paymenttype: '',
            customerid: '',
            vendorid: '',
            remark: '',
            paymentmode: '',
            date: new Date().toDateString().split(' ').slice(1).join(' '),
            amount: 0,
            voucherno: this.props.getPayment.voucherno ? this.props.getPayment.voucherno : 1,
            transactionno: '',
            bankdetail: '',
            specialpayment: '',
            previouscurrent: ''
        })
        window.onload = () => {
            this.props.refreshVendorState();
            this.props.refreshCustomerState();
            this.props.resetPaymentDetail();
            this.props.refreshBankState();
            this.props.refreshExpenseHead();
            this.props.refreshInvestmentAccountState();
            this.props.refreshEmployeeState();
        }
    }
    componentDidMount() {
        if (this.props.addPaymentMessage.message === 'success') {
            alert("Payment Added Successfully");
            this.props.resetaddPaymentMessage('success');
        }
        else if (this.props.addPaymentMessage.message === 'Already Registered') {
            alert("Voucher Number " + (parseInt(this.props.getPayment.voucherno) - 1) + "already registered");
            this.props.resetaddPaymentMessage('success');
        }
        if (this.props.addPaymentMessage.error !== '') {
            alert(this.props.addPaymentMessage.error);
            this.setState({
                ...this.props.addPaymentMessage.state
            })
            this.props.resetaddPaymentMessage('error');
        }
        if ((this.props.getPayment.message === 'initial') && (this.props.getPayment.loading === false)) this.props.getPaymentDetail(this.props.businessid, this.props.fyid);
        if ((this.props.getVendor.message === 'initial') && (this.props.getVendor.loading === false)) this.props.getVendorList(this.props.businessid);
        if ((this.props.getCustomer.message === 'initial') && (this.props.getCustomer.loading === false)) this.props.getCustomerList(this.props.businessid);
        if (this.props.getBankDetail.message === 'initial') this.props.getBankDetailList(this.props.businessid);
        if (this.props.getExpenseHead.message === 'initial') this.props.getExpenseHeadList(this.props.businessid);
        if (this.props.getInvestmentAccount.message === 'initial') this.props.getInvestmentAccountList(this.props.businessid);
        if (this.props.logoutMessage.error !== '') {
            alert("Error Logging Out. Please Try Again");
            this.props.resetLogoutMessage();
        }
        if (this.props.getEmployee.message === 'initial') this.props.getEmployeeList(this.props.businessid);
    }
    handleInputChange = (event) => {
        let target = event.target;
        let name = target.name;
        let value = target.value;
        if (name === 'specialpayment') {
            if (value === 'partner') {
                this.setState({
                    ...this.state,
                    [name]: value
                })
            }
            else if (value === 'tdsreceived') {
                this.setState({
                    ...this.state,
                    [name]: value,
                    paymenttype: 'paymentreceived',
                    customerid: value,
                })
            }
            else {
                this.setState({
                    ...this.state,
                    [name]: value,
                    paymenttype: 'paymentmade',
                    vendorid: value,
                })
            }
        }
        else {
            this.setState({
                ...this.state,
                [name]: value
            })
        }
    }
    handleSubmit = () => {
        let business = this.props.businessList.data.filter(b => b.businessid === this.props.businessid)[0];
        let fy = business.financial.filter(f => f.fyid === this.props.fyid)[0].financialyear;
        if (isNaN(parseInt(this.state.voucherno)) || (this.state.voucherno < 0)) alert("Voucher No is mandatory and Must Be Greater Than Zero");
        else if (this.state.paymenttype === '') alert("Please Select Payment/Receipt");
        else if ((this.state.paymenttype === 'paymentmade') && (this.state.vendorid === '')) alert("Please Select Vendor Account");
        else if ((this.state.paymenttype === 'paymentreceived') && (this.state.customerid === '')) alert("Please Select Customer ACcount");
        else if (this.state.paymentmode === '') alert("Please Select Payment Mode")
        else if ((this.state.paymentmode !== 'cash') && (this.state.bankdetail === '')) alert("Please Select Bank Account");
        else if (!(/^[a-zA-Z]{3} [0-9]{2} [0-9]{4}$/i.test(this.state.date))) alert("Date Must Be In (MON DD YYYY) Format");
        else if ((new Date(this.state.date) < new Date("01 Apr " + fy.split("-")[0])) || (new Date(this.state.date) > new Date("31 Mar " + fy.split("-")[1]))) alert("Invalid Date. Date Must Be Within FY " + fy);
        else if (isNaN(parseInt(this.state.amount)) || (this.state.amount <= 0)) alert("Amount Must Be Greater Than 0");
        else if (this.state.remark.length > 49) alert("Remark Must Be Within 50 Characters");
        else if (this.state.transactionno.length > 19) alert("Transaction No Must Be Within 20 Characters");
        else this.props.raisePayment(this.props.businessid, this.props.fyid, this.state);
    }
    render() {
        if (!cookie.load('userservice')) return <Redirect to="/" />;
        else {
            if ((this.props.addPaymentMessage.loading === true) || (this.props.getVendor.loading === true) || (this.props.getCustomer.loading === true) || (this.props.getPayment.loading === true) || (this.props.logoutMessage.loading === true) || (this.props.getBankDetail.loading === true) || (this.props.getExpenseHead.loading === true) || (this.props.getInvestmentAccount.loading === true)) {
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div className="mt-3 d-flex justify-content-center">
                            <button className="btn btn-danger">Loading... </button>
                        </div>
                    </div>
                );
            }
            else if ((this.props.getVendor.error !== '') || (this.props.getCustomer.error !== '') || (this.props.getPayment.error !== '') || (this.props.getBankDetail.error !== '') || (this.props.getExpenseHead.error !== '') || (this.props.getInvestmentAccount.error !== '')) {
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div className="mt-3 d-flex justify-content-center">
                            <button className="btn btn-danger">Error.. Please Refresh and Try Agin </button>
                        </div>
                    </div>
                );
            }
            else {
                let business = this.props.businessList.data.filter(b => b.businessid === this.props.businessid)[0];
                let fy = business.financial.filter(f => f.fyid === this.props.fyid)[0].financialyear;
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div style={{ minHeight: "93vh", display: "flex", flexDirection: "column" }}>
                            <div className="container" style={{ flex: "1" }}>
                                <h6 className="text-center pl-2 pr-2 pt-4" style={{ lineHeight: "1" }}><strong>{business.businessname} {fy}</strong></h6>
                                <h6 className="text-center pl-2 pr-2 mb-4" style={{ lineHeight: "1" }}>{business.alias ? "(" + business.alias + ")" : ''}</h6>
                                <h6 className="text-center pl-2 pr-2"><strong><u>Payment & Receipts Entry</u></strong></h6>
                                <div className="row" >
                                    <LocalForm className="small" onSubmit={this.handleSubmit}>
                                        <div className="container mt-4 d-flex justify-content-center">
                                            <div class="row col-12 col-md-4">
                                                <div className="form-group mb-1 col-12">
                                                    <label className="col-6 p-0" ><input type="radio" name="specialpayment" id="specialpayment" value="reversecharge" checked={this.state.specialpayment === "reversecharge" ? true : false} onChange={this.handleInputChange} /> Pay Reverse Charge</label>
                                                    <label className="col-6 p-0"><input type="radio" name="specialpayment" id="specialpayment" value="gst" checked={this.state.specialpayment === "gst" ? true : false} onChange={this.handleInputChange} /> Pay GST</label>
                                                    <label className="col-6 p-0"><input type="radio" name="specialpayment" id="specialpayment" value="paytds" checked={this.state.specialpayment === "paytds" ? true : false} onChange={this.handleInputChange} /> Pay TDS</label>
                                                    <label className="col-6 p-0"><input type="radio" name="specialpayment" id="specialpayment" value="tdsreceived" checked={this.state.specialpayment === "tdsreceived" ? true : false} onChange={this.handleInputChange} /> TDS Refund</label>
                                                    <label className="col-6 p-0"><input type="radio" name="specialpayment" id="specialpayment" value="payit" checked={this.state.specialpayment === "payit" ? true : false} onChange={this.handleInputChange} /> Pay Income Tax</label>
                                                    {business.businesstype === 'partnership' ? <label className="col-6 p-0"><input type="radio" name="specialpayment" id="specialpayment" value="partner" checked={this.state.specialpayment === "partner" ? true : false} onChange={this.handleInputChange} /> Partner Account</label> : ''}
                                                    <label className="col-6 p-0"><input type="radio" name="specialpayment" id="specialpayment" value="payemployee" checked={this.state.specialpayment === "payemployee" ? true : false} onChange={this.handleInputChange} /> Pay Employee</label>
                                                </div>
                                                <div className="form-group mb-1 col-12">
                                                    <label htmlFor="voucherno" className="mb-0 muted-text"><strong>Voucher No</strong><span className="text-danger">*</span></label>
                                                    <Control.text model=".voucherno" className="form-control form-control-sm" id="voucherno" name="voucherno" onChange={this.handleInputChange} value={this.state.voucherno} />
                                                </div>
                                                {(this.state.specialpayment === '' || this.state.specialpayment === 'partner') ? <div className="form-group mb-1 col-12">
                                                    <label htmlFor="paymenttype" className="mb-0 muted-text"><strong>Payment/Receipt:<span className="text-danger">*</span></strong></label>
                                                    <Control.select model=".paymenttype" className="form-control form-control-sm" id="paymenttype" name="paymenttype" onChange={this.handleInputChange} value={this.state.paymenttype}>
                                                        <option value="">Payment/Receipt</option>
                                                        <option value="paymentmade">Payment</option>
                                                        <option value="paymentreceived">Receipt</option>
                                                    </Control.select>
                                                </div> : ''}
                                                {this.state.specialpayment === 'payit' || this.state.specialpayment === 'paytds' || this.state.specialpayment === 'tdsreceived' ? <div className="form-group mb-1 col-12">
                                                    <label htmlFor="previouscurrent" className="mb-0 muted-text"><strong>Previous Year/ Current Year<span className="text-danger">*</span></strong></label>
                                                    <Control.select model=".previouscurrent" className="form-control form-control-sm" id="previouscurrent" name="previouscurrent" onChange={this.handleInputChange} value={this.state.previouscurrent}>
                                                        <option value="">Previous Year / Current Year</option>
                                                        <option value="previous">Previous Year</option>
                                                        <option value="current">Current Year</option>
                                                    </Control.select>
                                                </div> : ''}
                                                <div className="form-group mb-1 col-12">
                                                    <label htmlFor="date" className="mb-0 muted-text"><strong>Date<span className="text-danger">*</span></strong></label>
                                                    <Control.text model=".date" className="form-control form-control-sm" id="date" name="date" onChange={this.handleInputChange} value={this.state.date} />
                                                </div>
                                                {this.state.specialpayment === '' ? < SelectParty customer={this.props.getCustomer.data} vendor={this.props.getVendor.data} state={this.state} handleInputChange={this.handleInputChange} expensehead={this.props.getExpenseHead.data} /> : ''}
                                                {this.state.specialpayment === 'partner' && this.state.paymenttype === 'paymentmade' ?
                                                    <div className="form-group mb-1 col-12">
                                                        <label htmlFor="vendorid" className="mb-0 muted-text"><strong>Select Partner</strong></label>
                                                        <Control.select model=".vendorid" className="form-control form-control-sm" id="vendorid" name="vendorid" value={this.state.vendorid} onChange={this.handleInputChange}>
                                                            <Option data={this.props.getInvestmentAccount.data} />
                                                        </Control.select>
                                                    </div>
                                                    : ''}
                                                {this.state.specialpayment === 'partner' && this.state.paymenttype === 'paymentreceived' ?
                                                    <div className="form-group mb-1 col-12">
                                                        <label htmlFor="customerid" className="mb-0 muted-text"><strong>Select Partner</strong></label>
                                                        <Control.select model=".customerid" className="form-control form-control-sm" id="customerid" name="customerid" value={this.state.customerid} onChange={this.handleInputChange}>
                                                            <Option data={this.props.getInvestmentAccount.data} />
                                                        </Control.select>
                                                    </div>
                                                    : ''}
                                                {this.state.specialpayment === 'payemployee' ?
                                                    <div className="form-group mb-1 col-12">
                                                        <label htmlFor="vendorid" className="mb-0 muted-text"><strong>Select Employee</strong></label>
                                                        <Control.select model=".vendorid" className="form-control form-control-sm" id="vendorid" name="vendorid" value={this.state.vendorid} onChange={this.handleInputChange}>
                                                            <Option data={this.props.getEmployee.data} type="employee" />
                                                        </Control.select>
                                                    </div>
                                                    : ''}
                                                <div className="form-group mb-1 col-12">
                                                    <label htmlFor="amount" className="mb-0 muted-text"><strong>Amount<span className="text-danger">*</span></strong></label>
                                                    <Control.text model=".amount" className="form-control form-control-sm" id="amount" name="amount" onChange={this.handleInputChange} value={this.state.amount} />
                                                </div>
                                                <div className="form-group mb-1 col-12">
                                                    <label htmlFor="paymentmode" className="mb-0 muted-text"><strong>Payment Mode:</strong><span className="text-danger">*</span></label>
                                                    <Control.select model=".paymentmode" className="form-control form-control-sm" id="paymentmode" name="paymentmode" onChange={this.handleInputChange} value={this.state.paymentmode}>
                                                        <option value="">Payment Mode</option>
                                                        {this.state.specialpayment === '' ? <option value="cash">Cash</option> : ''}
                                                        <option value="cheque">Cheque</option>
                                                        <option value="rtgs">RTGS/NEFT</option>
                                                        <option value="other">Other digital payment mode</option>
                                                    </Control.select>
                                                </div>
                                                {(this.state.paymentmode === 'cheque' || this.state.paymentmode === 'rtgs' || this.state.paymentmode === 'other') ? <div className="form-group mb-1 col-12">
                                                    <label htmlFor="transactionno" className="mb-0 muted-text"><strong>Transaction No <small>(Cheque No or Rtgs No...)</small></strong></label>
                                                    <Control.text model=".transactionno" className="form-control form-control-sm" id="transactionno" name="transactionno" onChange={this.handleInputChange} value={this.state.transactionno} />
                                                </div> : ''}
                                                {(this.state.paymentmode === 'cheque' || this.state.paymentmode === 'rtgs' || this.state.paymentmode === 'other') ? <div className="form-group mb-1 col-12">
                                                    <label htmlFor="bankdetail" className="mb-0 muted-text"><strong>Select Bank:</strong><span className="text-danger">*</span></label>
                                                    <Control.select model=".bankdetail" className="form-control form-control-sm" id="bankdetail" name="bankdetail" onChange={this.handleInputChange} value={this.state.bankdetail}>
                                                        <option value="">Select Bank Name</option>
                                                        {this.props.getBankDetail.data.map(b => <option value={b.bankdetailid + "..." + b.bankname}>{b.bankname}</option>)}
                                                    </Control.select>
                                                </div> : ''}

                                                <div className="form-group mb-1 col-12">
                                                    <label htmlFor="remark" className="mb-0 muted-text"><strong>Remarks</strong></label>
                                                    <Control.text model=".remark" className="form-control form-control-sm" id="remark" name="remark" onChange={this.handleInputChange} value={this.state.remark} />
                                                </div>
                                                <div className="form-group mb-1 col-12">
                                                    <button type="submit" className="btn btn-success mt-1 btn-sm small" >Enter Payment</button>
                                                </div>
                                            </div>
                                        </div>
                                    </LocalForm>
                                </div>
                            </div>
                            <Footer />
                        </div>
                    </div>
                )
            }
        }
    }
}

export default Payment;