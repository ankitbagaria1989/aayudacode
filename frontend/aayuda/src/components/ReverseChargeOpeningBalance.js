import React, { Component } from 'react';
import cookie from 'react-cookie';
import { Redirect } from 'react-router-dom';
import { LocalForm, Control } from 'react-redux-form';
import { Header } from './HeaderComponent';
import { Footer } from './Footer';

class RCOpeningBalance extends Component {
    constructor(props) {
        super(props);
        this.state = ({
            amount: 0
        })
        window.onload = () => {
            this.props.resetRCOpeningBalance();
        }
    }
    handleSubmit = () => {
        if ((this.state.amount === '') || (this.state.amount < 0) || (isNaN(parseInt(this.state.amount)))) alert(" Amount Must Be Greater Than Equal To 0 ");
        else this.props.updateRCOpeningBalance(this.state, this.props.businessid, this.props.fyid);
    }
    componentDidMount() {
        if (this.props.updateRCOpeningMessage.error !== '') {
            alert(this.props.updateRCOpeningMessage.error);
            this.setState({
                amount : this.props.updateRCOpeningMessage.state.amount
            })
            this.props.refreshUpdateRCOpeningBalance();
        }
        if (this.props.updateRCOpeningMessage.message === 'success') {
            alert("Data Updated Successfully");
            this.setState({
                ...this.props.updateRCOpeningMessage.state
            })
        }
        if (this.props.getRCOpeningBalance.message === 'initial') this.props.getRCOpeningBalanceList(this.props.businessid, this.props.fyid);
        if (this.props.logoutMessage.error !== '') {
            alert("Error Logging Out. Please Try Again");
            this.props.resetLogoutMessage();
        }
        if ((this.props.getRCOpeningBalance.message === 'success') && (this.props.updateRCOpeningMessage.error === '')) {
            if (this.props.getRCOpeningBalance.data) {
                this.setState({
                    amount: this.props.getRCOpeningBalance.data
                })
            }
        }
    }
    handleInputChange = (event) => {
        let target = event.target;
        let name = target.name;
        let value = target.value;
        this.setState({
            [name]: value
        })
    }
    render() {
        if (!cookie.load('userservice')) return <Redirect to="/" />;
        else {
            if ((this.props.updateRCOpeningMessage.loading === true) || (this.props.getRCOpeningBalance.loading === true) || (this.props.logoutMessage.loading === true)) {
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div className="mt-3 d-flex justify-content-center">
                            <button className="btn btn-danger">Loading... </button>
                        </div>
                    </div>
                );
            }
            else if (this.props.getRCOpeningBalance.error !== '') {
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div className="mt-3 d-flex justify-content-center">
                            <button className="btn btn-danger">{this.props.getRCOpeningBalance.error}</button>
                        </div>
                    </div>
                );
            }
            else {
                let business = this.props.businessList.data.filter(b => b.businessid === this.props.businessid)[0];
                let fy = business.financial.filter(f => f.fyid === this.props.fyid)[0].financialyear;
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div style={{ minHeight: "93vh", display: "flex", flexDirection: "column" }}>
                            <div className="container" style={{ flex: "1" }}>
                                <h6 className="text-center pl-2 pr-2 pt-4" style={{ lineHeight: "1" }}><strong>{business.businessname} {fy}</strong></h6>
                                <h6 className="text-center pl-2 pr-2 mb-4" style={{ lineHeight: "1" }}>{business.alias ? "(" + business.alias + ")" : ''}</h6>
                                <h6 className="text-center pl-2 pr-2 col-12"><strong><u>Reverse Charge Payable - Opening Balance</u></strong></h6>
                                <div className="row">
                                    <LocalForm className="small col-12" onSubmit={this.handleSubmit}>
                                        <div className="container mt-4 d-flex justify-content-center">
                                            <div class="row col-12 col-md-4">
                                                <div className="form-group mb-1 col-12">
                                                    <label htmlFor="amount" className="mb-0 muted-text"><strong>Opening Value For Reverse Charge Payable</strong><span className="text-danger">*</span></label>
                                                    <Control.text model=".amount" className="form-control form-control-sm" id="amount" name="amount" onChange={this.handleInputChange} value={this.state.amount} />
                                                </div>
                                                <div className="form-group mb-1 col-12">
                                                    <button type="submit" className="btn btn-success mt-1 btn-sm small" >Update</button>
                                                </div>
                                            </div>
                                        </div>
                                    </LocalForm>
                                </div>
                            </div>
                            <Footer />
                        </div>
                    </div>
                )
            }
        }
    }
}
export default RCOpeningBalance;