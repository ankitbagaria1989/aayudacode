import React, { Component } from 'react';
import cookie from 'react-cookie';
import { Redirect } from 'react-router-dom';
import { LocalForm, Control } from 'react-redux-form';
import { Header } from './HeaderComponent';
import { Footer } from './Footer';

class ChangePassword extends Component {
    constructor(props) {
        super(props);
        this.state = ({
            oldpassword: '',
            newpassword: '',
            confirmnewpassword: ''
        })
    }
    componentDidMount() {
        if (this.props.changePasswordMessage.error !== '') {
            alert(this.props.changePasswordMessage.error);
            this.setState({
                ...this.props.changePasswordMessage.state
            })
        }
        if (this.props.changePasswordMessage.message === 'success') {
            alert("Password Updated Successfully. Please Re Login To Continue");
            this.props.resetChangePasswordMessage();
        }
    }
    handleInputChange = (event) => {
        let target = event.target;
        let name = target.name;
        let value = target.value;
        this.setState({
            ...this.state,
            [name]: value
        })
    }
    handleSubmit = () => {
        if (this.state.oldpassword === '') alert("Please Fill Existing Password");
        else if (this.state.newpassword === '') alert("Please Fill New Password");
        else if (this.state.confirmnewpassword === '') alert("Please Confirm New Password");
        else if (this.state.newpassword !== this.state.confirmnewpassword) alert("New Password and Confirm New Password - DO NOT MATCH");
        else this.props.changePassword(this.state);
    }

    render() {
        
        if (!cookie.load('userservice')) return <Redirect to="/" />;
        else {
            if ((this.props.changePasswordMessage.loading === true)) {
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div className="mt-3 d-flex justify-content-center">
                            <button className="btn btn-danger">Loading... </button>
                        </div>
                    </div>
                );
            }
            else {
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div style={{ minHeight: "93vh", display: "flex", flexDirection: "column" }}>
                            <div className="container" style={{ flex: "1" }}>
                                <h6 className="text-center pl-2 pr-2 mt-4"><strong><u>Change Password</u></strong></h6>
                                <div className="row" >
                                    <LocalForm className="small col-12" onSubmit={this.handleSubmit}>
                                        <div className="container mt-4 d-flex justify-content-center">
                                            <div class="row col-12 col-md-4">
                                                <div className="form-group mb-1 col-12">
                                                    <label htmlFor="oldpassword" className="mb-0 muted-text"><strong>Existing Password</strong><span className="text-danger">*</span></label>
                                                    <Control type="password" model=".oldpassword" className="form-control form-control-sm" id="oldpassword" name="oldpassword" onChange={this.handleInputChange} value={this.state.oldpassword} autoComplete="new-password" />
                                                </div>
                                                <div className="form-group mb-1 col-12">
                                                    <label htmlFor="newpassword" className="mb-0 muted-text"><strong>New Password<span className="text-danger">*</span></strong></label>
                                                    <Control type="password" model=".newpassword" className="form-control form-control-sm" id="newpassword" name="newpassword" onChange={this.handleInputChange} value={this.state.newpassword} autoComplete="new-password" />
                                                </div>
                                                <div className="form-group mb-1 col-12">
                                                    <label htmlFor="confirmnewpassword" className="mb-0 muted-text"><strong>Confirm New Password<span className="text-danger">*</span></strong></label>
                                                    <Control.text model=".confirmnewpassword" className="form-control form-control-sm" id="confirmnewpassword" name="confirmnewpassword" onChange={this.handleInputChange} value={this.state.confirmnewpassword} autoComplete="new-password" />
                                                </div>
                                                <div className="form-group mb-1 col-12">
                                                    <button type="submit" className="btn btn-success mt-2 btn-sm small" >Change Password</button>
                                                </div>
                                            </div>
                                        </div>
                                    </LocalForm>
                                </div>
                            </div>
                            <Footer/>
                        </div>
                    </div>
                )
            }
        }
    }
}

export default ChangePassword;