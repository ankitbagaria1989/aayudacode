import React, { Component, useState } from 'react';
import cookie from 'react-cookie';
import { Redirect, Link } from 'react-router-dom';
import { Button, Collapse } from 'reactstrap';
import { Header } from './HeaderComponent';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faAngleDoubleDown } from '@fortawesome/free-solid-svg-icons';
import { Footer } from './Footer';

const RaiseInvoice = function ({ handleSubmit, businessid, fyid }) {
    return (
        <div className="d-flex justify-content-center">
            <div className="col-12">
                <Link to={`/business/businessdetail/raiseinvoice/${businessid}/${fyid}`}><Button className="btn-sm col-12 mb-1" style={{ backgroundColor: "#fff", border: "1px solid rgb(255, 123, 26)", color: "rgb(255, 123, 26)" }} ><strong>Sales Entry</strong></Button></Link>
            </div>
        </div>);
}

const EnterPurchase = function ({ handleSubmit, businessid, fyid }) {

    return (
        <div className="d-flex justify-content-center mt-2">
            <div className="col-12">
                <Link to={`/business/businessdetail/purchase/${businessid}/${fyid}`}><Button className="btn-sm col-12 mb-1" style={{ backgroundColor: "#fff", border: "1px solid rgb(255, 123, 26)", color: "rgb(255, 123, 26)" }} ><strong>Purchase Entry</strong></Button></Link>
            </div>
        </div>);
}
const ExpensesIncurred = function ({ businessid, fyid }) {
    return (
        <div className="d-flex justify-content-center">
            <div className="col-12">
                <Link to={`/business/businessdetail/expense/${businessid}/${fyid}`}><Button className="btn-sm col-12 mb-1" style={{ backgroundColor: "#fff", border: "1px solid rgb(255, 123, 26)", color: "rgb(255, 123, 26)" }} ><strong>Expense Entry</strong></Button></Link>
            </div>
        </div>
    );
}
const IncomeOS = function ({ businessid, fyid }) {
    return (
        <div className="d-flex justify-content-center">
            <div className="col-12">
                <Link to={`/business/businessdetail/incomeos/${businessid}/${fyid}`}><Button className="btn-sm col-12 mb-1" style={{ backgroundColor: "#fff", border: "1px solid rgb(255, 123, 26)", color: "rgb(255, 123, 26)" }} ><strong>Income Other Sources </strong></Button></Link>
            </div>
        </div>
    );
}
const Payment = function ({ businessid, fyid }) {
    return (
        <div className="d-flex justify-content-center mt-2">
            <div className="col-12">
                <Link to={`/business/businessdetail/payments/${businessid}/${fyid}`}><Button className="btn-sm col-12 mb-1" style={{ backgroundColor: "#fff", border: "1px solid rgb(255, 123, 26)", color: "rgb(255, 123, 26)" }} ><strong>Payments & Receipts</strong></Button></Link>
            </div>
        </div>
    );
}
const Loan = function ({ businessid, fyid }) {
    return (
        <div className="d-flex justify-content-center mt-2">
            <div className="col-12">
                <Link to={`/business/businessdetail/loan/${businessid}/${fyid}`}><Button className="btn-sm col-12 mb-1" style={{ backgroundColor: "#fff", border: "1px solid rgb(255, 123, 26)", color: "rgb(255, 123, 26)" }} ><strong>Loan & Repayments (Given/Received)</strong></Button></Link>
            </div>
        </div>
    );
}
const Investment = function ({ businessid, fyid, business }) {
    if (business.businesstype === 'partnership') return <div className="d-flex justify-content-center">
        <div className="col-12">
            <Link to={`/business/businessdetail/investment/${businessid}/${fyid}`}><Button className="btn-sm col-12 mb-1" style={{ backgroundColor: "#fff", border: "1px solid rgb(255, 123, 26)", color: "rgb(255, 123, 26)" }} ><strong>Partners Capital Entry</strong></Button></Link>
        </div>
    </div>
    else if (business.businesstype === 'ltd') return <div className="d-flex justify-content-center">
        <div className="col-12">
            <Link to={`/business/businessdetail/investment/${businessid}/${fyid}`}><Button className="btn-sm col-12 mb-1" style={{ backgroundColor: "#fff", border: "1px solid rgb(255, 123, 26)", color: "rgb(255, 123, 26)" }} ><strong>Share Holders Capital Entry</strong></Button></Link>
        </div>
    </div>
    else if (business.businesstype === 'propritorship') return <div className="d-flex justify-content-center">
        <div className="col-12">
            <Link to={`/business/businessdetail/investment/${businessid}/${fyid}`}><Button className="btn-sm col-12 mb-1" style={{ backgroundColor: "#fff", border: "1px solid rgb(255, 123, 26)", color: "rgb(255, 123, 26)" }} ><strong>Capital Account Entry</strong></Button></Link>
        </div>
    </div>
}
const AssetDepreciation = function ({ businessid, fyid }) {
    return (
        <div className="d-flex justify-content-center">
            <div className="col-12">
                <Link to={`/business/businessdetail/assetdepreciation/${businessid}/${fyid}`}><Button className="btn-sm col-12 mb-1" style={{ backgroundColor: "#fff", border: "1px solid rgb(255, 123, 26)", color: "rgb(255, 123, 26)" }} ><strong>Asset Depreciate/Appreciate</strong></Button></Link>
            </div>
        </div>
    );
}
const BankEntries = function ({ businessid, fyid }) {
    return (
        <div className="d-flex justify-content-center">
            <div className="col-12">
                <Link to={`/business/businessdetail/bankentries/${businessid}/${fyid}`}><Button className="btn-sm col-12 mb-1" style={{ backgroundColor: "#fff", border: "1px solid rgb(255, 123, 26)", color: "rgb(255, 123, 26)" }} ><strong>Cash Deposit/Withdrawl From Bank</strong></Button></Link>
            </div>
        </div>
    );
}
const DebitCredit = function ({ businessid, fyid }) {
    return (
        <div className="d-flex justify-content-center mt-2">
            <div className="col-12">
                <Link to={`/business/businessdetail/debitcredit/${businessid}/${fyid}`}><Button className="btn-sm col-12 mb-1" style={{ backgroundColor: "#fff", border: "1px solid rgb(255, 123, 26)", color: "rgb(255, 123, 26)" }} ><strong>Debit/Credit Note</strong></Button></Link>
            </div>
        </div>
    );
}
const TotalSales = function ({ businessid, fyid }) {
    return (
        <div className="d-flex justify-content-center">
            <div className="col-12">
                <Link to={`/business/businessdetail/salesdetail/${businessid}/${fyid}`}><Button className="btn-sm col-12 mb-1" style={{ backgroundColor: "#fff", border: "1px solid rgb(255, 123, 26)", color: "rgb(255, 123, 26)" }} ><strong>Sales Ledger</strong></Button></Link>
            </div>
        </div>
    );
}

const TotalPurchase = function ({ businessid, fyid }) {
    return (
        <div className="d-flex justify-content-center">
            <div className="col-12">
                <Link to={`/business/businessdetail/purchasedetail/${businessid}/${fyid}`}><Button className="btn-sm col-12 mb-1" style={{ backgroundColor: "#fff", border: "1px solid rgb(255, 123, 26)", color: "rgb(255, 123, 26)" }} ><strong>Purchase Ledger</strong></Button></Link>
            </div>
        </div>
    );
}

const TotalExpense = function ({ businessid, fyid }) {
    return (
        <div className="d-flex justify-content-center">
            <div className="col-12">
                <Link to={`/business/businessdetail/expensedetail/${businessid}/${fyid}`}><Button className="btn-sm col-12 mb-1" style={{ backgroundColor: "#fff", border: "1px solid rgb(255, 123, 26)", color: "rgb(255, 123, 26)" }} ><strong>Expense Ledger</strong></Button></Link>
            </div>
        </div>
    );
}
const TotalIncomeOS = function ({ businessid, fyid }) {
    return (
        <div className="d-flex justify-content-center">
            <div className="col-12">
                <Link to={`/business/businessdetail/incomeosdetail/${businessid}/${fyid}`}><Button className="btn-sm col-12 mb-2" style={{ backgroundColor: "#fff", border: "1px solid rgb(255, 123, 26)", color: "rgb(255, 123, 26)" }} ><strong>Income(OS) Ledger</strong></Button></Link>
            </div>
        </div>
    );
}
const OpeningClosing = function ({ businessid, fyid }) {
    return (
        <div className="d-flex justify-content-center">
            <div className="col-12">
                <Link to={`/business/businessdetail/stock/${businessid}/${fyid}`}><Button className="btn-sm col-12 mb-1" style={{ backgroundColor: "#fff", border: "1px solid rgb(255, 123, 26)", color: "rgb(255, 123, 26)" }} ><strong>Stock - Opening & Closing Balance</strong></Button></Link>
            </div>
        </div>
    );
}
const CashInHand = function ({ businessid, fyid }) {
    return (
        <div className="d-flex justify-content-center">
            <div className="col-12">
                <Link to={`/business/businessdetail/cashinhand/${businessid}/${fyid}`}><Button className="btn-sm col-12 mb-1" style={{ backgroundColor: "#fff", border: "1px solid rgb(255, 123, 26)", color: "rgb(255, 123, 26)" }} ><strong>Cash In  Hand - Opening Balance</strong></Button></Link>
            </div>
        </div>
    );
}
const PnLStatement = function ({ businessid, fyid }) {
    return (
        <div className="d-flex justify-content-center">
            <div className="col-12">
                <Link to={`/business/businessdetail/pnl/${businessid}/${fyid}`}><Button className="btn-sm col-12 mb-1" style={{ backgroundColor: "#fff", border: "1px solid rgb(255, 123, 26)", color: "rgb(255, 123, 26)" }} ><strong>Profit & Loss Statement</strong></Button></Link>
            </div>
        </div>
    );
}
const TrialBalance = function ({ businessid, fyid }) {
    return (
        <div className="d-flex justify-content-center">
            <div className="col-12">
                <Link to={`/business/businessdetail/trialbalance/${businessid}/${fyid}`}><Button className="btn-sm col-12 mb-1" style={{ backgroundColor: "#fff", border: "1px solid rgb(255, 123, 26)", color: "rgb(255, 123, 26)" }} ><strong>Trial Balance</strong></Button></Link>
            </div>
        </div>
    );
}
const CashFlowStatement = function ({ handleSubmit, businessid, fyid }) {
    return (
        <div className="d-flex justify-content-center">
            <div className="col-12">
                <Link to={`/business/businessdetail/cashflowstatement/${businessid}/${fyid}`}><Button className="btn-sm col-12 mb-1" style={{ backgroundColor: "#fff", border: "1px solid rgb(255, 123, 26)", color: "rgb(255, 123, 26)" }} ><strong>Cash Book</strong></Button></Link>
            </div>
        </div>
    );
}
const BankBook = function ({ handleSubmit, businessid, fyid }) {
    return (
        <div className="d-flex justify-content-center">
            <div className="col-12">
                <Link to={`/business/businessdetail/bankbookstatement/${businessid}/${fyid}`}><Button className="btn-sm col-12 mb-1" style={{ backgroundColor: "#fff", border: "1px solid rgb(255, 123, 26)", color: "rgb(255, 123, 26)" }} ><strong>Bank Ledger</strong></Button></Link>
            </div>
        </div>
    );
}
const LoanBook = function ({ businessid, fyid }) {
    return (
        <div className="d-flex justify-content-center">
            <div className="col-12">
                <Link to={`/business/businessdetail/loanbookstatement/${businessid}/${fyid}`}><Button className="btn-sm col-12 mb-1" style={{ backgroundColor: "#fff", border: "1px solid rgb(255, 123, 26)", color: "rgb(255, 123, 26)" }} ><strong>Loan Book</strong></Button></Link>
            </div>
        </div>
    );
}
const InvestmentBook = function ({ businessid, fyid, business }) {
    if (business.businesstype === 'propritorship' || business.businesstype === 'partnership') {
        return (
            <div className="d-flex justify-content-center">
                <div className="col-12">
                    <Link to={`/business/businessdetail/investmentbookstatement/${businessid}/${fyid}`}><Button className="btn-sm col-12 mb-1" style={{ backgroundColor: "#fff", border: "1px solid rgb(255, 123, 26)", color: "rgb(255, 123, 26)" }} ><strong>{business.businesstype === 'propritorship' ? 'Capital Account Book' : ''}{business.businesstype === 'partnership' ? "Partner's Capital Account Book" : ''}</strong></Button></Link>
                </div>
            </div>
        );
    }
    else return <div></div>;
}
const TDSBook = function ({ businessid, fyid }) {
    return (
        <div className="d-flex justify-content-center">
            <div className="col-12">
                <Link to={`/business/businessdetail/tdsbook/${businessid}/${fyid}`}><Button className="btn-sm col-12 mb-1" style={{ backgroundColor: "#fff", border: "1px solid rgb(255, 123, 26)", color: "rgb(255, 123, 26)" }} ><strong>TDS Payable Ledger</strong></Button></Link>
            </div>
        </div>
    );
}

const AssetBook = function ({ businessid, fyid }) {
    return (
        <div className="d-flex justify-content-center">
            <div className="col-12">
                <Link to={`/business/businessdetail/assetbook/${businessid}/${fyid}`}><Button className="btn-sm col-12 mb-1" style={{ backgroundColor: "#fff", border: "1px solid rgb(255, 123, 26)", color: "rgb(255, 123, 26)" }} ><strong>Asset Book</strong></Button></Link>
            </div>
        </div>
    );
}
const IncomeTaxBook = function ({ businessid, fyid }) {
    return (
        <div className="d-flex justify-content-center">
            <div className="col-12">
                <Link to={`/business/businessdetail/incometaxbook/${businessid}/${fyid}`}><Button className="btn-sm col-12 mb-2" style={{ backgroundColor: "#fff", border: "1px solid rgb(255, 123, 26)", color: "rgb(255, 123, 26)" }} ><strong>TDS Receivable Ledger</strong></Button></Link>
            </div>
        </div>
    );
}
const GSTStatement = function ({ businessid, fyid }) {
    return (
        <div className="d-flex justify-content-center">
            <div className="col-12">
                <Link to={`/business/businessdetail/gststatement/${businessid}/${fyid}`}><Button className="btn-sm col-12 mb-1" style={{ backgroundColor: "#fff", border: "1px solid rgb(255, 123, 26)", color: "rgb(255, 123, 26)" }} ><strong>GST Statement</strong></Button></Link>
            </div>
        </div>
    );
}
const ReverseChargeSheet = function ({ businessid, fyid }) {
    return (
        <div className="d-flex justify-content-center">
            <div className="col-12">
                <Link to={`/business/businessdetail/reversechargesheet/${businessid}/${fyid}`}><Button className="btn-sm col-12 mb-1" style={{ backgroundColor: "#fff", border: "1px solid rgb(255, 123, 26)", color: "rgb(255, 123, 26)" }} ><strong>Reverse Charge Sheet</strong></Button></Link>
            </div>
        </div>
    );
}

const AccountStatement = function ({ businessid, fyid }) {
    return (
        <div className="d-flex justify-content-center">
            <div className="col-12 mt-2">
                <Link to={`/business/businessdetail/accountstatement/${businessid}/${fyid}`}><Button className="btn-sm col-12 mb-1" style={{ backgroundColor: "#fff", border: "1px solid rgb(255, 123, 26)", color: "rgb(255, 123, 26)" }} ><strong>Parties Account Statement</strong></Button></Link>
            </div>
        </div>
    );
}
const EmployeeAccountStatement = function ({ businessid, fyid }) {
    return (
        <div className="d-flex justify-content-center">
            <div className="col-12">
                <Link to={`/business/businessdetail/employeeaccountstatement/${businessid}/${fyid}`}><Button className="btn-sm col-12 mb-1" style={{ backgroundColor: "#fff", border: "1px solid rgb(255, 123, 26)", color: "rgb(255, 123, 26)" }} ><strong>Employee Account Statement</strong></Button></Link>
            </div>
        </div>
    );
}

const AccountReceivables = function ({ businessid, fyid }) {
    return (
        <div className="d-flex justify-content-center">
            <div className="col-12 mt-2">
                <Link to={`/business/businessdetail/accountreceivables/${businessid}/${fyid}`}><Button className="btn-sm col-12 mb-1" style={{ backgroundColor: "#fff", border: "1px solid rgb(255, 123, 26)", color: "rgb(255, 123, 26)" }} ><strong>Account Receivables (Debtors)</strong></Button></Link>
            </div>
        </div>
    );
}
const AccountPayables = function ({ businessid, fyid }) {
    return (
        <div className="d-flex justify-content-center">
            <div className="col-12">
                <Link to={`/business/businessdetail/accountpayables/${businessid}/${fyid}`}><Button className="btn-sm col-12 mb-1" style={{ backgroundColor: "#fff", border: "1px solid rgb(255, 123, 26)", color: "rgb(255, 123, 26)" }} ><strong>Account Payables (Creditors)</strong></Button></Link>
            </div>
        </div>
    );
}
const ConfigureAccountStatement = function ({ businessid, fyid }) {
    return (
        <div className="d-flex justify-content-center">
            <div className="col-12">
                <Link to={`/business/businessdetail/openingbalance/${businessid}/${fyid}`}><Button className="btn-sm col-12 mb-1" style={{ backgroundColor: "#fff", border: "1px solid rgb(255, 123, 26)", color: "rgb(255, 123, 26)" }} ><strong>Parties Account Opening Balance Value</strong></Button></Link>
            </div>
        </div>
    );
}
const ConfigureBankAccountStatement = function ({ businessid, fyid }) {
    return (
        <div className="d-flex justify-content-center">
            <div className="col-12">
                <Link to={`/business/businessdetail/bankopeningbalance/${businessid}/${fyid}`}><Button className="btn-sm col-12 mb-1" style={{ backgroundColor: "#fff", border: "1px solid rgb(255, 123, 26)", color: "rgb(255, 123, 26)" }} ><strong>Bank Account Opening Balance</strong></Button></Link>
            </div>
        </div>
    );
}
const ConfigureLoanAccountStatement = function ({ businessid, fyid }) {
    return (
        <div className="d-flex justify-content-center">
            <div className="col-12">
                <Link to={`/business/businessdetail/loanopeningbalance/${businessid}/${fyid}`}><Button className="btn-sm col-12 mb-1" style={{ backgroundColor: "#fff", border: "1px solid rgb(255, 123, 26)", color: "rgb(255, 123, 26)" }} ><strong>Loan Account Opening Balance</strong></Button></Link>
            </div>
        </div>
    );
}
const ConfigureAssetAccountStatement = function ({ businessid, fyid }) {
    return (
        <div className="d-flex justify-content-center">
            <div className="col-12">
                <Link to={`/business/businessdetail/assetopeningbalance/${businessid}/${fyid}`}><Button className="btn-sm col-12 mb-1" style={{ backgroundColor: "#fff", border: "1px solid rgb(255, 123, 26)", color: "rgb(255, 123, 26)" }} ><strong>Asset Account Opening Value</strong></Button></Link>
            </div>
        </div>
    );
}
const ConfigureInvestmentAccountStatement = function ({ businessid, fyid, business }) {
    if (business.businesstype === 'partnership') return (
        <div className="d-flex justify-content-center">
            <div className="col-12">
                <Link to={`/business/businessdetail/investmentopeningbalance/${businessid}/${fyid}`}><Button className="btn-sm col-12 mb-1" style={{ backgroundColor: "#fff", border: "1px solid rgb(255, 123, 26)", color: "rgb(255, 123, 26)" }} ><strong>Partners Capital Account Opening Balance</strong></Button></Link>
            </div>
        </div>
    );
    else if (business.businesstype === 'propritorship') return (
        <div className="d-flex justify-content-center">
            <div className="col-12">
                <Link to={`/business/businessdetail/investmentopeningbalance/${businessid}/${fyid}`}><Button className="btn-sm col-12 mb-1" style={{ backgroundColor: "#fff", border: "1px solid rgb(255, 123, 26)", color: "rgb(255, 123, 26)" }} ><strong>Capital Account Opening Balance</strong></Button></Link>
            </div>
        </div>
    );
    else return (<div></div>);
}
const ConfigureEmployeeAccountStatement = function ({ businessid, fyid, business }) {
    return (
        <div className="d-flex justify-content-center">
            <div className="col-12">
                <Link to={`/business/businessdetail/employeeopeningbalance/${businessid}/${fyid}`}><Button className="btn-sm col-12 mb-1" style={{ backgroundColor: "#fff", border: "1px solid rgb(255, 123, 26)", color: "rgb(255, 123, 26)" }} ><strong>Employee Account Opening Balance</strong></Button></Link>
            </div>
        </div>
    );
}
const ConfigureTDSOpeningBalance = function ({ businessid, fyid }) {
    return (
        <div className="d-flex justify-content-center">
            <div className="col-12">
                <Link to={`/business/businessdetail/tdsopeningbalance/${businessid}/${fyid}`}><Button className="btn-sm col-12 mb-1" style={{ backgroundColor: "#fff", border: "1px solid rgb(255, 123, 26)", color: "rgb(255, 123, 26)" }} ><strong>TDS Payable Opening Balance</strong></Button></Link>
            </div>
        </div>
    );
}
const ConfigureIncomeTaxOpeningBalance = function ({ businessid, fyid }) {
    return (
        <div className="d-flex justify-content-center">
            <div className="col-12">
                <Link to={`/business/businessdetail/incometaxopeningbalance/${businessid}/${fyid}`}><Button className="btn-sm col-12 mb-1" style={{ backgroundColor: "#fff", border: "1px solid rgb(255, 123, 26)", color: "rgb(255, 123, 26)" }} ><strong>TDS Receivable Opening Balance</strong></Button></Link>
            </div>
        </div>
    );
}
const ConfigureRCOpeningBalance = function ({ businessid, fyid }) {
    return (
        <div className="d-flex justify-content-center">
            <div className="col-12">
                <Link to={`/business/businessdetail/rcopeningbalance/${businessid}/${fyid}`}><Button className="btn-sm col-12 mb-1" style={{ backgroundColor: "#fff", border: "1px solid rgb(255, 123, 26)", color: "rgb(255, 123, 26)" }} ><strong>Reverse Charge Payable -  Opening Balance</strong></Button></Link>
            </div>
        </div>
    );
}
const ConfigureGSTOpeningBalance = function ({ businessid, fyid }) {
    return (
        <div className="d-flex justify-content-center">
            <div className="col-12">
                <Link to={`/business/businessdetail/gstopeningbalance/${businessid}/${fyid}`}><Button className="btn-sm col-12 mb-1" style={{ backgroundColor: "#fff", border: "1px solid rgb(255, 123, 26)", color: "rgb(255, 123, 26)" }} ><strong>GST Opening Blance</strong></Button></Link>
            </div>
        </div>
    );
}
const DataEntry = function ({ handleSubmit, businessid, fyid, business }) {
    let [isOpen, setIsOpen] = useState(false);
    let toggle = () => setIsOpen((!isOpen));
    return (
        <div className="d-flex justify-content-center mt-4">
            <div className="pl-2 pr-2 col-12 col-12 pl-0 pr-0">
                <Button className="mb-1 mt-2 col-12 btn-sm" style={{ backgroundColor: "white", color: "rgb(40, 167, 69)", border: "1px solid rgb(40, 167, 69)" }} onClick={toggle}><div className="d-flex"><span className="col-11" style={{ fontSize: "18px" }}>Accounts Data Entry</span><span className="col-1 pr-0 text-align-right"><FontAwesomeIcon size="sm" icon={faAngleDoubleDown} /></span></div></Button>
                <Collapse isOpen={isOpen}>
                    <div className="mt-1 mb-1 pt-3 pb-3" style={{ backgroundColor: "#fff" }}>
                        <RaiseInvoice handleSubmit={handleSubmit} businessid={businessid} fyid={fyid} />
                        <IncomeOS businessid={businessid} fyid={fyid} />
                        <EnterPurchase handleSubmit={handleSubmit} businessid={businessid} fyid={fyid} />
                        <ExpensesIncurred businessid={businessid} fyid={fyid} />
                        <Payment businessid={businessid} fyid={fyid} />
                        <BankEntries businessid={businessid} fyid={fyid} />
                        <DebitCredit businessid={businessid} fyid={fyid} />
                        <AssetDepreciation businessid={businessid} fyid={fyid} />
                        <Loan businessid={businessid} fyid={fyid} />
                        <Investment businessid={businessid} fyid={fyid} business={business} />
                    </div>
                </Collapse>
            </div>
        </div>
    );
}

const Reports = function ({ handleSubmit, businessid, fyid, business }) {
    let [isOpen, setIsOpen] = useState(false);
    let toggle = () => setIsOpen((!isOpen));
    return (
        <div className="d-flex justify-content-center">
            <div className="pl-2 pr-2 col-12">
                <Button className="mb-1 mt-2 col-12 btn-sm" style={{ backgroundColor: "white", color: "rgb(40, 167, 69)", border: "1px solid rgb(40, 167, 69)" }} onClick={toggle}><div className="d-flex"><span className="col-11" style={{ fontSize: "18px" }}> Accounting Reports</span><span className="col-1 pr-0 text-align-right"><FontAwesomeIcon size="sm" icon={faAngleDoubleDown} /></span></div></Button>
                <Collapse isOpen={isOpen}>
                    <div className="mt-1 mb-1 pt-3 pb-3" style={{ backgroundColor: "#fff" }}>
                        <TotalSales businessid={businessid} fyid={fyid} />
                        <TotalPurchase businessid={businessid} fyid={fyid} />
                        <TotalExpense businessid={businessid} fyid={fyid} />
                        <TotalIncomeOS businessid={businessid} fyid={fyid} />
                        <AccountStatement businessid={businessid} fyid={fyid} />
                        <EmployeeAccountStatement businessid={businessid} fyid={fyid} />
                        <GSTStatement businessid={businessid} fyid={fyid} />
                        <ReverseChargeSheet businessid={businessid} fyid={fyid} />
                        <CashFlowStatement handleSubmit={handleSubmit} businessid={businessid} fyid={fyid} />
                        <BankBook handleSubmit={handleSubmit} businessid={businessid} fyid={fyid} />
                        <LoanBook businessid={businessid} fyid={fyid} />
                        <AssetBook businessid={businessid} fyid={fyid} />
                        <TDSBook businessid={businessid} fyid={fyid} />
                        <IncomeTaxBook businessid={businessid} fyid={fyid} />
                        <AccountReceivables businessid={businessid} fyid={fyid} />
                        <AccountPayables businessid={businessid} fyid={fyid} />
                        <PnLStatement businessid={businessid} fyid={fyid} />
                        <InvestmentBook businessid={businessid} fyid={fyid} business={business} />
                        <TrialBalance businessid={businessid} fyid={fyid} />
                    </div>
                </Collapse>
            </div>
        </div>
    );
}

const ConfigureFYData = function ({ handleSubmit, businessid, fyid, fy, business }) {
    let [isOpen, setIsOpen] = useState(false);
    let toggle = () => setIsOpen((!isOpen));
    return (
        <div className="d-flex justify-content-center">
            <div className="pl-2 pr-2 pb-4 col-12">
                <Button className="mb-1 mt-2 col-12 btn-sm" style={{ backgroundColor: "white", color: "rgb(40, 167, 69)", border: "1px solid rgb(40, 167, 69)" }} onClick={toggle}><div className="d-flex"><span className="col-11" style={{ fontSize: "18px" }}>Configure Accounts Opening Balance</span><span className="col-1 pr-0 text-align-right"><FontAwesomeIcon size="sm" icon={faAngleDoubleDown} /></span></div></Button>
                <Collapse isOpen={isOpen}>
                    <div className="mt-1 mb-1 pt-3 pb-3" style={{ backgroundColor: "#fff" }}>
                        <ConfigureAccountStatement businessid={businessid} fyid={fyid} />
                        <OpeningClosing businessid={businessid} fyid={fyid} />
                        <CashInHand businessid={businessid} fyid={fyid} />
                        <ConfigureBankAccountStatement businessid={businessid} fyid={fyid} />
                        <ConfigureGSTOpeningBalance businessid={businessid} fyid={fyid} />
                        <ConfigureTDSOpeningBalance businessid={businessid} fyid={fyid} />
                        <ConfigureIncomeTaxOpeningBalance businessid={businessid} fyid={fyid} />
                        <ConfigureRCOpeningBalance businessid={businessid} fyid={fyid} />
                        <ConfigureAssetAccountStatement businessid={businessid} fyid={fyid} />
                        <ConfigureLoanAccountStatement businessid={businessid} fyid={fyid} />
                        <ConfigureInvestmentAccountStatement businessid={businessid} fyid={fyid} business={business} />
                        <ConfigureEmployeeAccountStatement businessid={businessid} fyid={fyid} business={business} />
                    </div>
                </Collapse>
            </div>
        </div>
    );
}

class BusinessProcess extends Component {
    handleSubmit = (values, businessid) => {
        let financialYear = values.fy;
        this.props.addFinancialYear(financialYear, businessid);
    }
    componentDidMount() {
        if ((this.props.getCustomer.message === 'initial') && (this.props.getCustomer.loading === false)) this.props.getCustomerList(this.props.businessid);
        if ((this.props.getItem.message === 'initial') && (this.props.getItem.loading === false)) this.props.getItemList(this.props.businessid);
        if ((this.props.getBankDetail.message === 'initial') && (this.props.getBankDetail.loading === false)) this.props.getBankDetailList(this.props.businessid);
        if ((this.props.salesDetail.message === 'initial') && (this.props.salesDetail.loading === false)) this.props.getSalesData(this.props.businessid, this.props.fyid);
        if ((this.props.purchaseDetail.message === 'initial') && (this.props.purchaseDetail.loading === false)) this.props.getPurchaseData(this.props.businessid, this.props.fyid);
        if ((this.props.getExpense.message === 'initial') && (this.props.getExpense.loading === false)) this.props.getExpenseDetail(this.props.businessid, this.props.fyid);
        if ((this.props.getBankEntry.message === 'initial') && (this.props.getBankEntry.loading === false)) this.props.getBankEntryDetail(this.props.businessid, this.props.fyid);
        if ((this.props.getOpeningClosing.message === 'initial') && (this.props.getOpeningClosing.loading === false)) this.props.getOpeningClosingDetail(this.props.businessid, this.props.fyid);
        if ((this.props.getOpeningBalance.message === 'initial') && (this.props.getOpeningBalance.loading === false)) this.props.getPartyOpeningBalance(this.props.businessid, this.props.fyid);
        if ((this.props.getCashInHand.message === 'initial') && (this.props.getCashInHand.loading === false)) this.props.getCashInHandDetail(this.props.businessid, this.props.fyid);
        if (this.props.getIncomeOS.message === 'initial') this.props.getIncomeOSDetail(this.props.businessid, this.props.fyid);
        if (this.props.getBankOpeningBalance.message === 'initial') this.props.getBankOpeningBalanceList(this.props.businessid, this.props.fyid);
        if (this.props.getEmployee.message === 'initial') this.props.getEmployeeList(this.props.businessid);
        if (this.props.getEmployeeOpeningBalance.message === 'initial') this.props.getEmployeeOpeningBalanceList(this.props.businessid, this.props.fyid);
        if (this.props.logoutMessage.error !== '') {
            alert("Error Logging Out. Please Try Again");
            this.props.resetLogoutMessage();
        }
    }
    render() {
        if (!cookie.load('userservice')) return <Redirect to="/" />
        else {
            if ((this.props.logoutMessage.loading === true)) {
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div className="mt-3 d-flex justify-content-center">
                            <button className="btn btn-danger">Loading... </button>
                        </div>
                    </div>
                );
            }
            else {
                let business = this.props.businessList.data.filter(b => b.businessid === this.props.businessid)[0];
                let fy = business.financial.filter(f => f.fyid === this.props.fyid)[0].financialyear;
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div style={{ minHeight: "93vh", display: "flex", flexDirection: "column" }}>
                            <div className="container d-flex justify-content-center pr-0 pl-0" style={{ flex: "1" }}>
                                <div className="col-12 col-md-5">
                                    <div className="pr-0 pl-0">
                                        <h6 className="text-center pl-2 pr-2 pt-4" style={{ lineHeight: "1" }}><strong>{business.businessname} {fy}</strong></h6>
                                        <h6 className="text-center pl-2 pr-2 mb-4" style={{ lineHeight: "1" }}>{business.alias ? "(" + business.alias + ")" : ''}</h6>
                                        <h6 className="text-center pl-2 pr-2"><strong><u>Accounts</u></strong></h6>
                                        <DataEntry handleSubmit={this.handleSubmit} businessid={this.props.businessid} fyid={this.props.fyid} business={business} />
                                        <Reports handleSubmit={this.handleSubmit} businessid={this.props.businessid} fyid={this.props.fyid} business={business} />
                                        <ConfigureFYData handleSubmit={this.handleSubmit} businessid={this.props.businessid} fyid={this.props.fyid} fy={fy} business={business} />
                                    </div>
                                </div>
                            </div>
                            <Footer />
                        </div>
                    </div >
                );
            }
        }
    }
}

export default BusinessProcess;