import React, { Component } from 'react';
import cookie from 'react-cookie';
import { Redirect } from 'react-router-dom';
import { LocalForm, Control } from 'react-redux-form';
import { Header } from './HeaderComponent';
import { sortData, sortDataPurchase } from '../redux/sortArrayofObjects';
import { Footer } from './Footer';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faFilePdf } from '@fortawesome/free-solid-svg-icons';
import { Page, Text, View, Document, pdf } from '@react-pdf/renderer';
import { pdfstyles } from '../sylesheet/stylesheet';
import { saveAs } from 'file-saver';

const DataRowPurchase = function ({ state, purchaseDetail, getPayment, fyyear, getDebitCredit, getExpense, businessid, fyid, deleteEntry, getAssetAccount }) {
    let returnrows = [];
    let id = state.name ? state.name.split("....")[0] : '';
    let purchaseData = purchaseDetail.data.filter(p => p.vendorid === id);
    let paymentData = getPayment.data.filter(p => p.vendorid === id);
    let debitcredit = getDebitCredit.data.filter(d => d.partyid === id);
    let expenseData = getExpense.data.filter(d => d.vendorid === id);
    let assetData = getAssetAccount.data.filter(d => d.purchasefyid === fyid);
    let data = [];
    data = data.concat(purchaseData);
    data = data.concat(paymentData);
    data = data.concat(debitcredit);
    data = data.concat(expenseData);
    data = data.concat(assetData);
    data = data.sort(sortDataPurchase);
    let x = 0, l = data.length, i = 2;
    let creditamount = 0, debitamount = 0;
    if (state.amounttype === 'paidadvance') debitamount = debitamount + parseFloat(state.openingbalance);
    if (state.amounttype === 'payable') creditamount = creditamount + parseFloat(state.openingbalance);
    returnrows.push(
        <div>
            {state.amounttype === 'paidadvance' || state.amounttype === 'payable' ? <div className="d-flex small p-1 d-md-none d-lg-none" style={{ backgroundColor: "rgb(181, 253, 233)" }}>
                <div className="col-5 col-md-5 p-1">
                    <div className="col-12 col-md-6 p-0">Apr/Opening Bal</div>
                </div>
                <div className="col-3 col-md-1 text-right">{state.amounttype === 'paidadvance' ? parseFloat(state.openingbalance).toFixed(2) : ''}
                </div>
                <div className="col-3 col-md-6 p-1">
                    <div className="col-12 col-md-2 p-0 text-right">{state.amounttype === 'payable' ? parseFloat(state.openingbalance).toFixed(2) : ''}</div>
                </div>
                <div className="col-1"></div>
            </div> : ''}
            {state.amounttype === 'paidadvance' || state.amounttype === 'payable' ? <div className="container small mt-3 d-none d-md-block p-1" style={{ backgroundColor: "rgb(181, 253, 233)" }}>
                <div className="d-flex">
                    <div className="col-2 p-0">Apr Opening Balance</div>
                    <div className="col-4 p-0"></div>
                    <div className="col-2">{state.amounttype === 'paidadvance' ? parseFloat(state.openingbalance).toFixed(2) : ''}</div>
                    <div className="col-3">{state.amounttype === 'payable' ? parseFloat(state.openingbalance).toFixed(2) : ''}</div>
                </div>
            </div> : ''}
        </div>
    )
    if (state.amounttype === 'paidadvance' || state.amounttype === 'payable') i++;
    while (x < l) {
        if (data[x].type === 'purchase') {
            creditamount = creditamount + parseFloat(data[x].grandtotal);
            returnrows.push(
                <div>
                    <div className="d-flex small p-1 d-md-none d-lg-none" style={i % 2 === 0 ? { backgroundColor: "rgb(181, 253, 233)" } : {}}>
                        <div className="col-5 col-md-5 p-1">
                            <div className="col-12 col-md-6 p-0"><strong>{data[x].invoicenumber}</strong></div>
                            <div className="col-12 col-md-6 p-0"><strong>{data[x].invoicedate}</strong></div>
                            <div className="col-12 col-md-6 p-0">{"By Purchase"}</div>
                        </div>
                        <div className="col-3 col-md-1">
                        </div>
                        <div className="col-3 col-md-6 p-1">
                            <div className="col-12 col-md-2 p-0 text-right">{parseFloat(data[x].grandtotal).toFixed(2)}</div>
                        </div>
                        <div className="col-1"></div>
                    </div>
                    <div className="container small d-none d-md-block p-1" style={i % 2 === 0 ? { backgroundColor: "rgb(181, 253, 233)" } : {}}>
                        <div className="d-flex">
                            <div className="col-1 p-0">{data[x].invoicedate}</div>
                            <div className="col-1 p-0">{data[x].invoicenumber}</div>
                            <div className="col-4 p-0"><strong>By Purchase</strong></div>
                            <div className="col-2"></div>
                            <div className="col-3 text-right">{parseFloat(data[x].grandtotal).toFixed(2)}</div>
                        </div>
                    </div>
                </div>
            )
        }
        else if (data[x].type === 'asset') {
            creditamount = creditamount + parseFloat(data[x].grandtotal);
            returnrows.push(
                <div>
                    <div className="d-flex small p-1 d-md-none d-lg-none" style={i % 2 === 0 ? { backgroundColor: "rgb(181, 253, 233)" } : {}}>
                        <div className="col-5 col-md-5 p-1">
                            <div className="col-12 col-md-6 p-0"><strong>{data[x].purchasevoucherno}</strong></div>
                            <div className="col-12 col-md-6 p-0"><strong>{data[x].purchasevoucherdate}</strong></div>
                            <div className="col-12 col-md-6 p-0"><div>{data[x].assetaccount}</div><div>Asset Purchase</div></div>
                        </div>
                        <div className="col-3 col-md-1 text-right">
                        </div>
                        <div className="col-3 col-md-6 p-1 text-right">
                            <div className="col-12 col-md-2 p-0">{parseFloat(data[x].purchasetotal).toFixed(2)}</div>
                        </div>
                        <div className="col-1"></div>
                    </div>
                    <div className="container small d-none d-md-block p-1" style={i % 2 === 0 ? { backgroundColor: "rgb(181, 253, 233)" } : {}}>
                        <div className="d-flex">
                            <div className="col-1 p-0">{data[x].purchasedate}</div>
                            <div className="col-1 p-0">{data[x].purchasevoucherno}</div>
                            <div className="col-4 p-0"><div><strong>By{data[x].assetaccount}</strong></div><div>Asset Purchase</div></div>
                            <div className="col-2 text-right"></div>
                            <div className="col-3 text-right">{parseFloat(data[x].purchasetotal).toFixed(2)}</div>
                        </div>
                    </div>
                </div>
            )
        }
        else if (data[x].type === 'payment') {
            debitamount = debitamount + parseFloat(data[x].amount);
            returnrows.push(
                <div>
                    <div className="d-flex small p-1 d-md-none d-lg-none" style={i % 2 === 0 ? { backgroundColor: "rgb(181, 253, 233)" } : {}}>
                        <div className="col-5 col-md-5 p-1">
                            <div className="col-12 col-md-6 p-0"><strong>{data[x].voucherno}/{data[x].date}</strong></div>
                            <div className="col-12 col-md-6 p-0 ">To {data[x].paymentmode === 'cash' ? 'Cash' : data[x].bankdetail.split("...")[1]}</div>
                            <div className="col-12 col-md-6 p-0 ">Payment</div>
                            <div className="col-12 col-md-6 p-0 ">{data[x].transactionno}</div>
                            <div className="col-12 col-md-6 p-0 ">{data[x].remark}</div>
                        </div>
                        <div className="col-3 col-md-1 text-right">{parseFloat(data[x].amount).toFixed(2)}</div>
                        <div className="col-3 col-md-6 p-1 text-right"></div>
                    </div>
                    <div className="container small d-none d-md-block p-1" style={i % 2 === 0 ? { backgroundColor: "rgb(181, 253, 233)" } : {}}>
                        <div className="d-flex">
                            <div className="col-1 p-0">{data[x].date}</div>
                            <div className="col-1 p-0">{data[x].voucherno}</div>
                            <div className="col-4 p-0">
                                <div className="col-12 col-md-6 p-0 "><strong>To {data[x].paymentmode === 'cash' ? 'Cash' : data[x].bankdetail.split("...")[1]}</strong></div>
                                <div className="col-12 col-md-6 p-0 ">Payment</div>
                                <div className="col-12 col-md-6 p-0 ">{data[x].transactionno}</div>
                                <div className="col-12 col-md-6 p-0 ">{data[x].remark}</div>
                            </div>
                            <div className="col-2 text-right">{parseFloat(data[x].amount).toFixed(2)}</div>
                            <div className="col-3 text-right"><strong></strong></div>
                        </div>
                    </div>
                </div >
            )
        }
        else if (data[x].type === 'debitcredit') {
            debitamount = debitamount + parseFloat(data[x].amount);
            returnrows.push(
                <div>
                    <div className="d-flex small p-1 d-md-none d-lg-none" style={i % 2 === 0 ? { backgroundColor: "rgb(181, 253, 233)" } : {}}>
                        <div className="col-5 col-md-5 p-1">
                            <div className="col-12 col-md-6 p-0 "><strong>{data[x].voucherno}/{data[x].date}</strong></div>
                            <div className="col-12 col-md-6 p-0 ">To Purchase</div>
                            <div className="col-12 col-md-6 p-0 ">{data[x].debitcredit.toUpperCase() + " NOTE / " + data[x].issuereceived}</div>
                            <div className="col-12 col-md-6 p-0 ">{data[x].remarks}</div>
                        </div>
                        <div className="col-3 col-md-1 text-right">{parseFloat(data[x].total).toFixed(2)}</div>
                        <div className="col-3 col-md-6 p-1"></div>
                    </div>
                    <div className="container small d-none d-md-block p-1" style={i % 2 === 0 ? { backgroundColor: "rgb(181, 253, 233)" } : {}}>
                        <div className="d-flex">
                            <div className="col-1 p-0">{data[x].date}</div>
                            <div className="col-1 p-0">{data[x].voucherno}</div>
                            <div className="col-4 p-0">
                                <div className="col-12"><strong>To Purchase</strong></div>
                                <div className="col-12">{data[x].debitcredit.toUpperCase() + " NOTE / " + data[x].issuereceived}</div>
                                <div className="col-12">{data[x].remarks}</div>
                            </div>
                            <div className="col-2 text-right">{parseFloat(data[x].total).toFixed(2)}</div>
                            <div className="col-3"><strong></strong></div>
                        </div>
                    </div>
                </div>
            )
        }
        else if (data[x].type === 'Expense') {
            returnrows.push(
                <div>
                    <div className=" d-flex small mb-1 p-1 d-md-none d-lg-none" style={i % 2 === 0 ? { backgroundColor: "rgb(181, 253, 233)" } : {}}>
                        <div className="col-5 col-md-5 p-1">
                            <div className="col-12 col-md-6 p-0"><strong>{data[x].voucherno}/{data[x].date}</strong></div>
                            <div className="col-12 col-md-6 p-0 " style={{ lineHeight: "1" }}>By {data[x].category + " " + data[x].type}</div>
                            <div className="col-12 col-md-6 p-0 " style={{ lineHeight: "1" }}>{data[x].description}</div>
                            {data[x].tds === "1" ? <div className="col-12 p-0">{"TDS Amount :" + data[x].tdsamount}</div> : ''}
                        </div>
                        <div className="col-3 col-md-6 p-0 pt-1">
                        </div>
                        <div className="col-3 col-md-1 text-right">
                            <div className="col-12 col-md-2 p-0">{data[x].tds === "1" ? parseFloat(parseFloat(data[x].amount) - parseFloat(data[x].tdsamount)).toFixed(2) : parseFloat(data[x].amount).toFixed(2)}</div>
                        </div>
                    </div>
                    <div className="container d-none d-md-block small mb-1 p-1" style={i % 2 === 0 ? { backgroundColor: "rgb(181, 253, 233)" } : {}}>
                        <div className="d-flex">
                            <div className="col-1 p-0">{data[x].date}</div>
                            <div className="col-1 p-0">{data[x].voucherno}</div>
                            <div className="col-4 p-0">
                                <div className="col-12 p-0"><strong>By {data[x].category + " " + data[x].type}</strong></div>
                                <div className="col-12 p-0">{data[x].description}</div>
                                {data[x].tds === "1" ? <div className="col-12 p-0">{"TDS Amount :" + data[x].tdsamount}</div> : ''}
                            </div>
                            <div className="col-2"></div>
                            <div className="col-3 text-right">{data[x].tds === '1' ? parseFloat(parseFloat(data[x].amount) - parseFloat(data[x].tdsamount)).toFixed(2) : parseFloat(data[x].amount).toFixed(2)}</div>
                            <div className="col-1"></div>
                        </div>
                    </div>
                </div>
            );
            creditamount = creditamount + (data[x].tds === '1' ? (parseFloat(data[x].amount) - parseFloat(data[x].tdsamount)) : parseFloat(data[x].amount));
        }
        x++;
        i++;
    }
    returnrows.push(
        <div>
            <div className="d-flex small d-md-none mt-1">
                <div className="col-5 p-1"></div>
                <div className="col-3 p-0 border-top border-dark">{parseFloat(debitamount).toFixed(2)}</div>
                <div className="col-3 p-0 border-top border-dark">{parseFloat(creditamount).toFixed(2)}</div>
            </div>
            <div className="row small d-md-none mt-1">
                <div className="col-5"><strong>Payable</strong></div>
                <div className="col-3 border-top border-dark p-0 text-right">{parseFloat(creditamount - debitamount).toFixed(2)}</div>
                <div className="col-3 p-0"></div>
            </div>
            <div className="container small mt-2 d-none d-md-block p-0">
                <div className="d-flex">
                    <div className="col-2 p-0"></div>
                    <div className="col-4 p-0"></div>
                    <div className="col-2 border-top border-dark text-right">{parseFloat(debitamount).toFixed(2)}</div>
                    <div className="col-3 border-top border-dark text-right">{parseFloat(creditamount).toFixed(2)}</div>
                </div>
            </div>
            <div className="container small mt-2 d-none d-md-block p-0 ">
                <div className="d-flex">
                    <div className="col-2 p-0"></div>
                    <div className="col-4 p-0"><strong>Payable</strong></div>
                    <div className="col-2 border-top border-dark text-right">{parseFloat(creditamount - debitamount).toFixed(2)}</div>
                    <div className="col-3 text-right"></div>
                </div>
            </div>
        </div>
    )
    return returnrows;
}
const DataRowSales = function ({ state, salesDetail, getPayment, fyyear, getDebitCredit, businessid, fyid, deleteEntry, getIncomeOS, getAssetAccount }) {
    let returnrows = [];
    let id = state.name ? state.name.split("....")[0] : '';
    let salesData = salesDetail.data.filter(s => s.customerid === id && s.active == 0 && s.type === 'sales');
    let incomeData = getIncomeOS.data.filter(i => i.customerid === id);
    let paymentData = getPayment.data.filter(p => p.customerid === id);
    let debitcredit = getDebitCredit.data.filter(d => d.partyid === id);
    let assetData = getAssetAccount.data.filter(d => d.soldfyid === fyid && d.customerid.split("...")[0] === id);
    let data = [];
    data = data.concat(salesData);
    data = data.concat(paymentData);
    data = data.concat(debitcredit);
    data = data.concat(incomeData);
    data = data.concat(assetData);
    data = data.sort(sortData);
    let x = 0, l = data.length, i = 2;
    let creditamount = 0, debitamount = 0;
    if (state.amounttype === 'receivable') debitamount = debitamount + parseFloat(state.openingbalance);
    if (state.amounttype === 'receivedadvance') creditamount = creditamount + parseFloat(state.openingbalance);
    returnrows.push(
        <div>
            {state.amounttype === 'receivable' || state.amounttype === 'receivedadvance' ? <div className="d-flex small p-1 d-md-none d-lg-none" style={{ backgroundColor: "rgb(181, 253, 233)" }}>
                <div className="col-5 col-md-5 p-1">
                    <div className="col-12 col-md-6 p-0"><strong>Apr/Opening Bal</strong></div>
                </div>
                <div className="col-3 col-md-1 text-right">{state.amounttype === 'receivable' ? parseFloat(state.openingbalance).toFixed(2) : ''}</div>
                <div className="col-3 col-md-6 p-1 text-right">{state.amounttype === 'receivedadvance' ? parseFloat(state.openingbalance).toFixed(2) : ''}</div>
            </div> : ''}
            {state.amounttype === 'receivable' || state.amounttype === 'receivedadvance' ? <div className="container small d-none d-md-block p-1" style={{ backgroundColor: "rgb(181, 253, 233)" }}>
                <div className="d-flex">
                    <div className="col-2 p-0">Apr Opening Balance</div>
                    <div className="col-4 p-0"></div>
                    <div className="col-2 text-right">{state.amounttype === 'receivable' ? parseFloat(state.openingbalance).toFixed(2) : ''}</div>
                    <div className="col-3 text-right">{state.amounttype === 'receivedadvance' ? parseFloat(state.openingbalance).toFixed(2) : ''}</div>
                </div>
            </div> : ''}
        </div>
    )
    if (state.amounttype === 'receivable' || state.amounttype === 'receivedadvance') i++;
    while (x < l) {
        if ((data[x].type === 'sales')) {
            debitamount = debitamount + parseFloat(data[x].grandtotal);
            returnrows.push(
                <div>
                    <div className="d-flex small p-1 d-md-none d-lg-none" style={i % 2 === 0 ? { backgroundColor: "rgb(181, 253, 233)" } : {}}>
                        <div className="col-5 col-md-5 p-1">
                            <div className="col-12 col-md-6 p-0"><strong>{data[x].invoicenumber}/{data[x].invoicedate}</strong></div>
                            <div className="col-12 col-md-6 p-0">To Sales</div>
                        </div>
                        <div className="col-3 col-md-1 text-right">
                            <div className="col-12 col-md-2 p-0">{parseFloat(data[x].grandtotal).toFixed(2)}</div>
                        </div>
                        <div className="col-3 col-md-6 p-1 text-right">
                        </div>
                    </div>
                    <div className="container small d-none d-md-block p-1" style={i % 2 === 0 ? { backgroundColor: "rgb(181, 253, 233)" } : {}}>
                        <div className="d-flex">
                            <div className="col-1 p-0">{data[x].invoicedate}</div>
                            <div className="col-1 p-0">{data[x].invoicenumber}</div>
                            <div className="col-4 p-0"><strong>To Sales</strong></div>
                            <div className="col-2 text-right">{parseFloat(data[x].grandtotal).toFixed(2)}</div>
                            <div className="col-3 text-right"></div>
                        </div>
                    </div>
                </div>
            )
        }
        else if (data[x].type === 'asset') {
            debitamount = debitamount + parseFloat(data[x].soldtotal);
            returnrows.push(
                <div>
                    <div className="d-flex small p-1 d-md-none d-lg-none" style={i % 2 === 0 ? { backgroundColor: "rgb(181, 253, 233)" } : {}}>
                        <div className="col-5 col-md-5 p-1">
                            <div className="col-12 col-md-6 p-0"><strong>{data[x].soldvoucherno}/{data[x].solddate}</strong></div>
                            <div className="col-12 col-md-6 p-0"><div>To {data[x].assetaccount}</div><div>Asset Sales</div></div>
                        </div>
                        <div className="col-3 col-md-1">
                            <div className="col-12 col-md-2 p-0 text-right">{parseFloat(data[x].soldtotal).toFixed(2)}</div>
                        </div>
                        <div className="col-3 col-md-6 p-1">
                        </div>
                    </div>
                    <div className="container small d-none d-md-block p-1" style={i % 2 === 0 ? { backgroundColor: "rgb(181, 253, 233)" } : {}}>
                        <div className="d-flex">
                            <div className="col-1 p-0">{data[x].solddate}</div>
                            <div className="col-1 p-0">{data[x].soldvoucherno}</div>
                            <div className="col-4 p-0"><div><strong>To{data[x].assetaccount}</strong></div><div>Asset Sales</div></div>
                            <div className="col-2 text-right">{parseFloat(data[x].soldtotal).toFixed(2)}</div>
                            <div className="col-3 text-right"></div>
                        </div>
                    </div>
                </div>
            )
        }
        else if (data[x].type === 'payment') {
            creditamount = creditamount + parseFloat(data[x].amount);
            returnrows.push(
                <div>
                    <div className="d-flex small p-1 d-md-none d-lg-none" style={i % 2 === 0 ? { backgroundColor: "rgb(181, 253, 233)" } : {}}>
                        <div className="col-5 col-md-5 p-1">
                            <div className="col-12 col-md-6 p-0"><strong>{data[x].voucherno}/{data[x].date}</strong></div>
                            <div className="col-12 col-md-6 p-0 ">By {data[x].paymentmode === 'cash' ? 'Cash' : data[x].bankdetail.split("...")[1]}</div>
                            <div className="col-12 col-md-6 p-0 ">Receipt</div>
                            <div className="col-12 col-md-6 p-0 ">{data[x].transactionno}</div>
                            <div className="col-12 col-md-6 p-0 ">{data[x].remark}</div>
                        </div>
                        <div className="col-3 col-md-1"></div>
                        <div className="col-3 col-md-6 p-1 text-right">{parseFloat(data[x].amount).toFixed(2)}</div>
                    </div>
                    <div className="container small d-none d-md-block p-1" style={i % 2 === 0 ? { backgroundColor: "rgb(181, 253, 233)" } : {}}>
                        <div className="d-flex">
                            <div className="col-1 p-0">{data[x].date}</div>
                            <div className="col-1 p-0">{data[x].voucherno}</div>
                            <div className="col-4 p-0">
                                <div className="col-12 p-0"><strong>By {data[x].paymentmode === 'cash' ? 'Cash' : data[x].bankdetail.split("...")[1]}</strong></div>
                                <div className="col-12 p-0">Receipt</div>
                                <div className="col-12 p-0">{data[x].transactionno}</div>
                                <div className="col-12 p-0">{data[x].remark}</div>
                            </div>
                            <div className="col-2 text-right"></div>
                            <div className="col-3 text-right">{parseFloat(data[x].amount).toFixed(2)}</div>
                        </div>
                    </div>
                </div>
            )
        }
        else if (data[x].type === 'debitcredit') {
            creditamount = creditamount + parseFloat(data[x].amount);
            returnrows.push(
                <div>
                    <div className="d-flex small p-1 d-md-none d-lg-none" style={i % 2 === 0 ? { backgroundColor: "rgb(181, 253, 233)" } : {}}>
                        <div className="col-5 col-md-5 p-1">
                            <div className="col-12 col-md-6 p-0 "><strong>{data[x].voucherno}/{data[x].date}</strong></div>
                            <div className="col-12 col-md-6 p-0 "><div><strong>By Sales</strong></div><div>{data[x].debitcredit.toUpperCase() + " NOTE / " + data[x].issuereceived}</div></div>
                            <div className="col-12 col-md-6 p-0 ">{data[x].remarks}</div>
                        </div>
                        <div className="col-3 col-md-1 text-right"></div>
                        <div className="col-3 col-md-6 p-1 text-right">{parseFloat(data[x].total).toFixed(2)}</div>
                    </div>
                    <div className="container small d-none d-md-block p-1" style={i % 2 === 0 ? { backgroundColor: "rgb(181, 253, 233)" } : {}}>
                        <div className="d-flex">
                            <div className="col-1 p-0">{data[x].date}</div>
                            <div className="col-1 p-0">{data[x].voucherno}</div>
                            <div className="col-4 p-0">
                                <div className="col-12"><div><strong>By Sales</strong></div><div>{data[x].debitcredit.toUpperCase() + " NOTE / " + data[x].issuereceived}</div></div>
                                <div className="col-12">{data[x].remarks}</div>
                            </div>
                            <div className="col-2 text-right"></div>
                            <div className="col-3 text-right">{parseFloat(data[x].total).toFixed(2)}</div>
                        </div>
                    </div>
                </div>
            )
        }
        else if (data[x].type === 'Income') {
            debitamount = debitamount + (data[x].tds === '1' ? (parseFloat(data[x].amount) - parseFloat(data[x].tdsamount)) : parseFloat(data[x].amount));
            returnrows.push(
                <div>
                    <div className="d-flex small p-1 d-md-none d-lg-none" style={i % 2 === 0 ? { backgroundColor: "rgb(181, 253, 233)" } : {}}>
                        <div className="col-5 col-md-5 p-1">
                            <div className="col-12 col-md-6 p-0"><strong>{data[x].voucherno}/{data[x].date}</strong></div>
                            <div className="col-12 col-md-6 p-0 " style={{ lineHeight: "1" }}>To {data[x].category + "-" + data[x].type}</div>
                            <div className="col-12 col-md-6 p-0 " style={{ lineHeight: "1" }}>{data[x].description}</div>
                            {data[x].tds === "1" ? <div className="col-12 p-0">{"TDS Amount :" + data[x].tdsamount}</div> : ''}
                        </div>
                        <div className="col-3 col-md-1 text-right">
                            <div className="col-12 col-md-2 p-0">{data[x].tds === "1" ? <div className="col-12 p-0">{parseFloat(data[x].amount - data[x].tdsamount).toFixed(2)}</div> : <div className="col-12 p-0">{parseFloat(data[x].amount).toFixed(2)}</div>}</div>
                        </div>
                        <div className="col-3 col-md-6 p-1">
                        </div>
                    </div>
                    <div className="container small d-none d-md-block p-1" style={i % 2 === 0 ? { backgroundColor: "rgb(181, 253, 233)" } : {}}>
                        <div className="d-flex">
                            <div className="col-1 p-0">{data[x].date}</div>
                            <div className="col-1 p-0">{data[x].voucherno}</div>
                            <div className="col-4 p-0">
                                <strong>To {data[x].category + "-" + data[x].type}</strong>
                                {data[x].tds === "1" ? <div className="col-12 p-0">{"TDS Amount :" + data[x].tdsamount}</div> : ''}
                            </div>
                            <div className="col-2 text-right">
                                <div className="col-12 col-md-2 p-0">{data[x].tds === "1" ? <div className="col-12 p-0">{parseFloat(data[x].amount - data[x].tdsamount).toFixed(2)}</div> : <div className="col-12 p-0">{parseFloat(data[x].amount).toFixed(2)}</div>}</div>
                            </div>
                            <div className="col-3"></div>
                        </div>
                    </div>
                </div>
            )
        }
        x++;
        i++;
    }
    returnrows.push(
        <div>
            <div className="d-flex small d-md-none mt-1">
                <div className="col-5 p-1"></div>
                <div className="col-3 border-top border-dark text-right">{parseFloat(debitamount).toFixed(2)}</div>
                <div className="col-3 border-top border-dark text-right">{parseFloat(creditamount).toFixed(2)}</div>
            </div>
            <div className="d-flex small d-md-none mt-1">
                <div className="col-5 p-1"><strong>{debitamount >= creditamount ? "Receivables" : 'Advance Received'}</strong></div>
                <div className="col-3 border-top border-dark text-right">{debitamount < creditamount ? parseFloat(creditamount - debitamount).toFixed(2) : ''}</div>
                <div className="col-3 border-top border-dark text-right">{debitamount >= creditamount ? parseFloat(debitamount - creditamount).toFixed(2) : ''}</div>
            </div>
            <div className="container small mt-2 d-none d-md-block p-0">
                <div className="d-flex">
                    <div className="col-2 p-0"></div>
                    <div className="col-4 p-0"></div>
                    <div className="col-2 border-top border-dark text-right">{parseFloat(debitamount).toFixed(2)}</div>
                    <div className="col-3 border-top border-dark text-right">{parseFloat(creditamount).toFixed(2)}</div>
                </div>
            </div>
            <div className="container small mt-2 d-none d-md-block p-0">
                <div className="d-flex">
                    <div className="col-2 p-0"><strong></strong></div>
                    <div className="col-4 p-0"><strong>{debitamount >= creditamount ? "Receivables" : 'Advance Received'}</strong></div>
                    <div className="col-2 border-top border-dark text-right">{debitamount < creditamount ? parseFloat(creditamount - debitamount).toFixed(2) : ''}</div>
                    <div className="col-3 border-top border-dark text-right">{debitamount >= creditamount ? parseFloat(debitamount - creditamount).toFixed(2) : ''}</div>
                </div>
            </div>
        </div>
    )
    return returnrows;
}

let salestotalcredit = 0, salestotaldebit = 0, salesbalance = 0;
const SalesDataPage = function ({ data, y, state }) {
    let returnrows = [];
    if (y === 1 && state.amounttype === 'receivable') {
        salestotaldebit = salestotaldebit + parseFloat(state.openingbalance);
        salesbalance = salesbalance + parseFloat(state.openingbalance);
    }
    else if (y === 1 && state.amounttype === 'receivedadvance') {
        salestotalcredit = parseFloat(state.openingbalance);
        salesbalance = salesbalance - parseFloat(state.openingbalance);
    }

    if (y === 1 && state.amounttype === 'receivable') returnrows.push(<View style={pdfstyles.itemSectionLedger, { textAlign: "center" }}>
        <View style={pdfstyles.itemHeader}><Text style={{ width: '75px', textAlign: "left" }}></Text><Text style={{ width: '75px', textAlign: "left" }}></Text><Text style={{ width: '200px', textAlign: "left", fontFamily: "Helvetica-Bold" }}>Opening Balance</Text><Text style={{ width: '75px', textAlign: "right" }}>{parseFloat(state.openingbalance).toFixed(2)}</Text><Text style={{ width: '75px', textAlign: "right" }}></Text><Text style={{ width: '75px', textAlign: "right" }}>{parseFloat(state.openingbalance).toFixed(2)}</Text></View>
    </View>)
    if (y === 1 && state.amounttype === 'receivedadvance') returnrows.push(<View style={pdfstyles.itemSectionLedger, { textAlign: "center" }}>
        <View style={pdfstyles.itemHeader}><Text style={{ width: '75px', textAlign: "left" }}></Text><Text style={{ width: '75px', textAlign: "left" }}></Text><Text style={{ width: '200px', textAlign: "left", fontFamily: "Helvetica-Bold" }}>Opening Balance (Advance)</Text><Text style={{ width: '75px', textAlign: "right" }}></Text><Text style={{ width: '75px', textAlign: "right" }}>{parseFloat(state.openingbalance).toFixed(2)}</Text><Text style={{ width: '75px', textAlign: "left" }}>{parseFloat(-1 * parseFloat(state.openingbalance)).toFixed(2)}</Text></View>
    </View>)

    if (y > 1) {
        returnrows.push(
            <View style={pdfstyles.itemSectionLedger, { textAlign: "center" }}>
                <View style={pdfstyles.itemHeader}><Text style={{ width: '75px', textAlign: "left" }}></Text><Text style={{ width: '75px', textAlign: "left" }}></Text><Text style={{ width: '200px', textAlign: "left", fontFamily: "Helvetica-Bold" }}>...Carry Forward</Text><Text style={{ width: '75px', textAlign: "right" }}>{parseFloat(salestotaldebit).toFixed(2)}</Text><Text style={{ width: '75px', textAlign: "right" }}>{parseFloat(salestotalcredit).toFixed(2)}</Text><Text style={{ width: '75px', textAlign: "right" }}>{parseFloat(salesbalance).toFixed(2)}</Text></View>
            </View>
        )
    }
    for (let x = (y - 1) * 50; x < y * 50 && x < data.length; x++) {
        let d = data[x];
        if ((d.type === 'sales')) {
            salestotaldebit = salestotaldebit + parseFloat(d.grandtotal);
            salesbalance = salesbalance + parseFloat(d.grandtotal);
            returnrows.push(
                <View style={pdfstyles.itemSectionLedger, { textAlign: "center" }}>
                    <View style={pdfstyles.itemHeader}><Text style={{ width: '75px', textAlign: "left" }}>{d.invoicedate}</Text><Text style={{ width: '75px', textAlign: "left" }}>{d.invoicenumber}</Text><Text style={{ width: '200px', textAlign: "left", fontFamily: "Helvetica-Bold" }}>To Sales</Text><Text style={{ width: '75px', textAlign: "right" }}>{parseFloat(d.grandtotal).toFixed(2)}</Text><Text style={{ width: '75px', textAlign: "right" }}></Text><Text style={{ width: '75px', textAlign: "right" }}>{parseFloat(salesbalance).toFixed(2)}</Text></View>
                </View>
            )
        }
        else if (d.type === 'asset') {
            salestotaldebit = salestotaldebit + parseFloat(d.soldtotal);
            salesbalance = salesbalance + parseFloat(d.soldtotal);
            returnrows.push(
                <View style={pdfstyles.itemSectionLedger, { textAlign: "center" }}>
                    <View style={pdfstyles.itemHeader}><Text style={{ width: '75px', textAlign: "left" }}>{d.solddate}</Text><Text style={{ width: '75px', textAlign: "left" }}>{d.soldvoucherno}</Text><View><Text style={{ width: '200px', textAlign: "left", fontFamily: "Helvetica-Bold" }}>To {data[x].assetaccount}</Text><Text style={{ width: '200px', textAlign: "left", paddingLeft: "15px" }}>Asset Sales</Text></View><Text style={{ width: '75px', textAlign: "right" }}>{parseFloat(d.soldtotal).toFixed(2)}</Text><Text style={{ width: '75px', textAlign: "right" }}></Text><Text style={{ width: '75px', textAlign: "right" }}>{parseFloat(salesbalance).toFixed(2)}</Text></View>
                </View>
            )
        }
        else if (d.type === 'payment') {
            salestotalcredit = salestotalcredit + parseFloat(d.amount);
            salesbalance = salesbalance - parseFloat(d.amount);
            returnrows.push(
                <View style={pdfstyles.itemSectionLedger, { textAlign: "center" }}>
                    <View style={pdfstyles.itemHeader}><Text style={{ width: '75px', textAlign: "left" }}>{d.date}</Text><Text style={{ width: '75px', textAlign: "left" }}>{d.voucherno}</Text><View style={{ width: "200px" }}><Text style={{ width: '200px', textAlign: "left", fontFamily: "Helvetica-Bold"  }}>By {data[x].paymentmode === 'cash' ? 'Cash' : data[x].bankdetail.split("...")[1]}</Text><Text style={{ width: '200px', textAlign: "left", paddingLeft:"15px" }}>Receipt</Text></View><Text style={{ width: '75px', textAlign: "right" }}></Text><Text style={{ width: '75px', textAlign: "right" }}>{parseFloat(d.amount).toFixed(2)}</Text><Text style={{ width: '75px', textAlign: "right" }}>{parseFloat(salesbalance).toFixed(2)}</Text></View>
                </View>
            )
        }
        else if (d.type === 'debitcredit') {
            salestotalcredit = salestotalcredit + parseFloat(d.amount);
            salesbalance = salesbalance - parseFloat(d.amount);
            returnrows.push(
                <View style={pdfstyles.itemSectionLedger, { textAlign: "center" }}>
                    <View style={pdfstyles.itemHeader}><Text style={{ width: '75px', textAlign: "left" }}>{d.date}</Text><Text style={{ width: '75px', textAlign: "left" }}>{d.voucherno}</Text><Text style={{ width: '200px', textAlign: "left", fontFamily:"Helvetica-Bold" }}>By Credit Note</Text><Text style={{ width: '75px', textAlign: "right" }}></Text><Text style={{ width: '75px', textAlign: "right" }}>{parseFloat(d.amount).toFixed(2)}</Text><Text style={{ width: '75px', textAlign: "right" }}>{parseFloat(salesbalance).toFixed(2)}</Text></View>
                </View>
            )
        }
        else if (d.type === 'Income') {
            let income = 0;
            d.tds === "1" ? income = parseFloat(d.amount) - parseFloat(d.tdsamount) : income = d.amount;
            salestotaldebit = salestotaldebit + income;
            salesbalance = salesbalance + parseFloat(income);
            returnrows.push(
                <View style={pdfstyles.itemSectionLedger, { textAlign: "center" }}>
                    <View style={pdfstyles.itemHeader}><Text style={{ width: '75px', textAlign: "left" }}>{d.date}</Text><Text style={{ width: '75px', textAlign: "left" }}>{d.voucherno}</Text><Text style={{ width: '200px', textAlign: "left", fontFamily:"Helvetica-Bold" }}>To {data[x].category + "-" + data[x].type}</Text><Text style={{ width: '75px', textAlign: "right" }}>{parseFloat(income).toFixed(2)}</Text><Text style={{ width: '75px', textAlign: "right" }}></Text><Text style={{ width: '75px', textAlign: "right" }}>{parseFloat(salesbalance).toFixed(2)}</Text></View>
                </View>
            )
        }
    }
    if (y === Math.ceil(data.length / 50)) {
        returnrows.push(<View><View style={pdfstyles.itemSectionLedger, { marginTop: "5px", paddingBottom: "2px" }}>
            <View style={pdfstyles.itemHeader}><Text style={{ width: '75px', textAlign: "left" }}></Text><Text style={{ width: '75px', textAlign: "left" }}></Text><Text style={{ width: '200px', textAlign: "left" }}></Text><Text style={{ width: '75px', borderTop: "1px solid black", textAlign: "right" }}>{parseFloat(salestotaldebit).toFixed(2)}</Text><Text style={{ width: '75px', borderTop: "1px solid black", textAlign: "right" }}>{parseFloat(salestotalcredit).toFixed(2)}</Text><Text style={{ width: '75px',textAlign: "right" }}></Text></View>
        </View>
            <View style={pdfstyles.itemSectionLedger, { marginTop: "5px", paddingBottom: "2px" }}>
                <View style={pdfstyles.itemHeader}><Text style={{ width: '75px', textAlign: "left" }}></Text><Text style={{ width: '75px', textAlign: "left" }}></Text><Text style={{ width: '200px', textAlign: "left" }}>{salesbalance >= 0 ? "Receivable" : "Advance Received"}</Text><Text style={{ width: '75px', textAlign: "right" }}>{salesbalance < 0 ? parseFloat(salesbalance).toFixed(2) : ""}</Text><Text style={{ width: '75px', textAlign: "right" }}>{salesbalance >= 0 ? parseFloat(salesbalance).toFixed(2) : ""}</Text></View>
            </View><View style={pdfstyles.itemSectionLedger, { marginTop: "5px", paddingBottom: "2px", borderTop: "1px solid black", borderBottom: "1px solid black", fontFamily: "Helvetica-Bold" }}>
                <View style={pdfstyles.itemHeader}><Text style={{ width: '75px', textAlign: "left" }}></Text><Text style={{ width: '75px', textAlign: "left" }}></Text><Text style={{ width: '200px', textAlign: "left", fontFamily: "Helvetica-Bold" }}>Total</Text><Text style={{ width: '75px', textAlign: "right", fontFamily:"Helvetica-Bold" }}>{salestotaldebit > salestotalcredit ? parseFloat(salestotaldebit).toFixed(2) : parseFloat(salestotalcredit).toFixed(2)}</Text><Text style={{ width: '75px', textAlign: "right" }}>{salestotaldebit > salestotalcredit ? parseFloat(salestotaldebit).toFixed(2) : parseFloat(salestotalcredit).toFixed(2)}</Text></View>
            </View></View>)
    }
    else {
        returnrows.push(<View><View style={pdfstyles.itemSectionLedger, { fontFamily: "Helvetica-Bold", marginTop: "2px" }}>
            <View style={pdfstyles.itemHeader}><Text style={{ width: '75px', textAlign: "left" }}></Text><Text style={{ width: '75px', textAlign: "left" }}></Text><Text style={{ width: '200px', textAlign: "left" }}></Text><Text style={{ width: '75px', borderTop: "1px solid black", textAlign: "right" }}>{parseFloat(salestotaldebit).toFloat(2)}</Text><Text style={{ width: '75px', borderTop: "1px solid black", textAlign: "right" }}>{parseFloat(salestotalcredit).toFixed(2)}</Text><Text style={{ width: '75px', borderTop: "1px solid black", textAlign: "right" }}>{parseFloat(salesbalance).toFixed(2)}</Text></View>
        </View>
            <View style={pdfstyles.itemSectionLedger, { fontFamily: "Helvetica", marginTop: "5px", textAlign: "right" }}><Text></Text>...Continued</View></View>)
    }
    return returnrows;
}

let purchasetotalcredit = 0, purchasetotaldebit = 0, purchasebalance = 0;
const PurchaseDataPage = function ({ data, y, state }) {
    let returnrows = [];
    if (y === 1 && state.amounttype === 'paidadvance') {
        purchasetotaldebit = purchasetotaldebit + parseFloat(state.openingbalance);
        purchasebalance = purchasebalance - parseFloat(state.openingbalance);
    }
    else if (y === 1 && state.amounttype === 'payable') {
        purchasetotalcredit = parseFloat(state.openingbalance);
        purchasebalance = purchasebalance + parseFloat(state.openingbalance);
    }
    if (y === 1 && state.amounttype === 'paidadvance') returnrows.push(<View style={pdfstyles.itemSectionLedger, { textAlign: "center" }}>
        <View style={pdfstyles.itemHeader}><Text style={{ width: '75px', textAlign: "left" }}></Text><Text style={{ width: '75px', textAlign: "left" }}></Text><Text style={{ width: '200px', textAlign: "left", fontFamily: "Helvetica-Bold" }}>Opening Balance</Text><Text style={{ width: '75px', textAlign: "right" }}></Text><Text style={{ width: '75px', textAlign: "right" }}>{parseFloat(state.openingbalance).toFixed(2)}</Text><Text style={{ width: '75px', textAlign: "right" }}>{parseFloat(-1 * parseFloat(state.openingbalance)).toFixed(2)}</Text></View>
    </View>)
    if (y === 1 && state.amounttype === 'receivedadvance') returnrows.push(<View style={pdfstyles.itemSectionLedger, { textAlign: "center" }}>
        <View style={pdfstyles.itemHeader}><Text style={{ width: '75px', textAlign: "left" }}></Text><Text style={{ width: '75px', textAlign: "left" }}></Text><Text style={{ width: '200px', textAlign: "left", fontFamily: "Helvetica-Bold" }}>Opening Balance (Received Advance)</Text><Text style={{ width: '75px', textAlign: "right" }}>{parseFloat(state.openingbalance).toFixed(2)}</Text><Text style={{ width: '75px', textAlign: "left" }}></Text><Text style={{ width: '75px', textAlign: "left" }}>{parseFloat(state.openingbalance).toFixed(2)}</Text></View>
    </View>)
    if (y > 1) {
        returnrows.push(
            <View style={pdfstyles.itemSectionLedger, { textAlign: "center" }}>
                <View style={pdfstyles.itemHeader}><Text style={{ width: '75px', textAlign: "left" }}></Text><Text style={{ width: '75px', textAlign: "left" }}></Text><Text style={{ width: '200px', textAlign: "left", fontFamily: "Helvetica-Bold" }}>...Carry Forward</Text><Text style={{ width: '75px', textAlign: "right" }}>{parseFloat(purchasetotaldebit).toFixed(2)}</Text><Text style={{ width: '75px', textAlign: "right" }}>{parseFloat(purchasetotalcredit).toFixed(2)}</Text><Text style={{ width: '75px', textAlign: "right" }}>{parseFloat(purchasebalance).toFixed(2)}</Text></View>
            </View>
        )
    }
    for (let x = (y - 1) * 50; x < y * 50 && x < data.length; x++) {
        let d = data[x];
        if ((d.type === 'purchase')) {
            purchasetotalcredit = purchasetotalcredit + parseFloat(d.grandtotal);
            purchasebalance = purchasebalance + parseFloat(d.grandtotal);
            returnrows.push(
                <View style={pdfstyles.itemSectionLedger, { textAlign: "center" }}>
                    <View style={pdfstyles.itemHeader}><Text style={{ width: '75px', textAlign: "left" }}>{d.invoicedate}</Text><Text style={{ width: '75px', textAlign: "left" }}>{d.invoicenumber}</Text><Text style={{ width: '200px', textAlign: "left", fontFamily: "Helvetica-Bold" }}>By Purchase</Text><Text style={{ width: '75px', textAlign: "right" }}></Text><Text style={{ width: '75px', textAlign: "right" }}>{parseFloat(d.grandtotal).toFixed(2)}</Text><Text style={{ width: '75px', textAlign: "right" }}>{parseFloat(purchasebalance).toFixed(2)}</Text></View>
                </View>
            )
        }
        else if (d.type === 'asset') {
            purchasetotalcredit = purchasetotalcredit + parseFloat(d.purchasetotal);
            purchasebalance = purchasebalance + parseFloat(d.purchasetotal);
            returnrows.push(
                <View style={pdfstyles.itemSectionLedger, { textAlign: "center" }}>
                    <View style={pdfstyles.itemHeader}><Text style={{ width: '75px', textAlign: "left" }}>{d.purchasedate}</Text><Text style={{ width: '75px', textAlign: "left" }}>{d.purchasevoucherno}</Text><View><Text style={{ width: '200px', textAlign: "left", fontFamily: "Helvetica-Bold" }}>By {data[x].assetaccount}</Text><Text style={{ width: '200px', textAlign: "left", paddingLeft: "15px" }}>Asset Purchase</Text></View><Text style={{ width: '75px', textAlign: "right" }}></Text><Text style={{ width: '75px', textAlign: "right" }}>{parseFloat(d.purchasetotal).toFixed(2)}</Text><Text style={{ width: '75px', textAlign: "right" }}>{parseFloat(purchasebalance).toFixed(2)}</Text></View>
                </View>
            )
        }
        else if (d.type === 'payment') {
            purchasetotaldebit = purchasetotaldebit + parseFloat(d.amount);
            purchasebalance = purchasebalance - parseFloat(d.amount);
            returnrows.push(
                <View style={pdfstyles.itemSectionLedger, { textAlign: "center" }}>
                    <View style={pdfstyles.itemHeader}><Text style={{ width: '75px', textAlign: "left" }}>{d.date}</Text><Text style={{ width: '75px', textAlign: "left" }}>{d.voucherno}</Text><View style={{ width: "200px" }}><Text style={{ width: '200px', textAlign: "left" }}>To {data[x].paymentmode === 'cash' ? 'Cash' : data[x].bankdetail.split("...")[1]}</Text><Text style={{ width: '200px', textAlign: "left" }}>&nbsp;&nbsp;&nbsp;Payment</Text></View><Text style={{ width: '75px', textAlign: "left" }}>{parseFloat(d.amount).toFixed(2)}</Text><Text style={{ width: '75px', textAlign: "left" }}></Text><Text style={{ width: '75px', textAlign: "left" }}>{parseFloat(purchasebalance).toFixed(2)}</Text></View>
                </View>
            )
        }
        else if (d.type === 'debitcredit') {
            purchasetotaldebit = purchasetotaldebit + parseFloat(d.amount);
            purchasebalance = purchasebalance - parseFloat(d.amount);
            returnrows.push(
                <View style={pdfstyles.itemSectionLedger, { textAlign: "center" }}>
                    <View style={pdfstyles.itemHeader}><Text style={{ width: '75px', textAlign: "left" }}>{d.date}</Text><Text style={{ width: '75px', textAlign: "left" }}>{d.voucherno}</Text><Text style={{ width: '200px', textAlign: "left", fontFamily: "Helvetica-Bold" }}>By Credit Note</Text><Text style={{ width: '75px', textAlign: "right" }}>{parseFloat(d.amount).toFixed(2)}</Text><Text style={{ width: '75px', textAlign: "right" }}></Text><Text style={{ width: '75px', textAlign: "right" }}>{parseFloat(purchasebalance).toFixed(2)}</Text></View>
                </View>
            )
        }
        else if (d.type === 'Expense') {
            let expense = 0;
            d.tds === "1" ? expense = parseFloat(d.amount) - parseFloat(d.tdsamount) : expense = parseFloat(d.amount);
            purchasetotalcredit = purchasetotalcredit + parseFloat(expense);
            purchasebalance = purchasebalance + parseFloat(expense);
            returnrows.push(
                <View style={pdfstyles.itemSectionLedger, { textAlign: "center" }}>
                    <View style={pdfstyles.itemHeader}><Text style={{ width: '75px', textAlign: "left" }}>{d.date}</Text><Text style={{ width: '75px', textAlign: "left" }}>{d.voucherno}</Text><Text style={{ width: '200px', textAlign: "left", fontFamily: "Helvetica-Bold" }}>By {data[x].category + "-" + data[x].type}</Text><Text style={{ width: '75px', textAlign: "right" }}></Text><Text style={{ width: '75px', textAlign: "right" }}>{parseFloat(expense).toFixed(2)}</Text><Text style={{ width: '75px', textAlign: "right" }}>{parseFloat(purchasebalance).toFixed(2)}</Text></View>
                </View>
            )
        }
    }
    if (y === Math.ceil(data.length / 50)) {
        returnrows.push(<View><View style={pdfstyles.itemSectionLedger, { marginTop: "5px", paddingBottom: "2px" }}>
            <View style={pdfstyles.itemHeader}><Text style={{ width: '75px', textAlign: "left" }}></Text><Text style={{ width: '75px', textAlign: "left" }}></Text><Text style={{ width: '200px', textAlign: "left" }}></Text><Text style={{ width: '75px', borderTop: "1px solid black", textAlign: "right" }}>{parseFloat(purchasetotaldebit).toFixed(2)}</Text><Text style={{ width: '75px', borderTop: "1px solid black", textAlign: "right" }}>{parseFloat(purchasetotalcredit).toFixed(2)}</Text><Text style={{ width: '75px' }}></Text></View>
        </View>
            <View style={pdfstyles.itemSectionLedger, { marginTop: "5px", paddingBottom: "2px" }}>
                <View style={pdfstyles.itemHeader}><Text style={{ width: '75px', textAlign: "left" }}></Text><Text style={{ width: '75px', textAlign: "left" }}></Text><Text style={{ width: '200px', textAlign: "left" }}>{purchasebalance >= 0 ? "Payable" : "Advance Paid"}</Text><Text style={{ width: '75px', textAlign: "right" }}>{purchasebalance >= 0 ? parseFloat(purchasebalance).toFixed(2) : ""}</Text><Text style={{ width: '75px', textAlign: "right" }}>{purchasebalance < 0 ? parseFloat(purchasebalance).toFixed(2) : ""}</Text></View>
            </View>
            <View style={pdfstyles.itemSectionLedger, { marginTop: "5px", paddingBottom: "2px", borderTop: "1px solid black", borderBottom: "1px solid black", fontFamily: "Helvetica-Bold" }}>
                <View style={pdfstyles.itemHeader}><Text style={{ width: '75px', textAlign: "left" }}></Text><Text style={{ width: '75px', textAlign: "left" }}></Text><Text style={{ width: '200px', textAlign: "left", fontFamily: "Helvetica-Bold" }}>Total</Text><Text style={{ width: '75px', textAlign: "right" }}>{purchasetotaldebit > purchasetotalcredit ? parseFloat(purchasetotaldebit).toFixed(2) : parseFloat(purchasetotalcredit).toFixed(2)}</Text><Text style={{ width: '75px', textAlign: "right" }}>{purchasetotaldebit > purchasetotalcredit ? parseFloat(purchasetotaldebit).toFixed(2) : parseFloat(purchasetotalcredit).toFixed(2)}</Text></View>
            </View></View>)
    }
    else {
        returnrows.push(<View><View style={pdfstyles.itemSectionLedger, { fontFamily: "Helvetica-Bold", marginTop: "2px" }}>
            <View style={pdfstyles.itemHeader}><Text style={{ width: '75px', textAlign: "left" }}></Text><Text style={{ width: '75px', textAlign: "left" }}></Text><Text style={{ width: '200px', textAlign: "left" }}></Text><Text style={{ width: '75px', borderTop: "1px solid black", textAlign: "right" }}>{parseFloat(purchasetotaldebit).toFloat(2)}</Text><Text style={{ width: '75px', borderTop: "1px solid black", textAlign: "right" }}>{parseFloat(purchasetotalcredit).toFixed(2)}</Text><Text style={{ width: '75px', borderTop: "1px solid black", textAlign: "right" }}>{parseFloat(purchasebalance).toFixed(2)}</Text></View>
        </View>
            <View style={pdfstyles.itemSectionLedger, { fontFamily: "Helvetica", marginTop: "5px", textAlign: "right" }}><Text></Text>...Continued</View></View>)
    }
    return returnrows;
}

const Report = function ({ business, data, l, n, period, state, type }) {
    let returnrows = [];
    for (let y = 1; y <= n; y++) {
        returnrows.push(<Page size="A4" style={pdfstyles.page}>
            {y === 1 ? <View><View style={pdfstyles.taxInvoiceHeader, { marginTop: "20px", textAlign: "center" }}>
                <View style={{ fontFamily: "Helvetica-Bold", width: "500px" }}><Text>{business.businessname}</Text></View>
                <View style={{ width: "500px" }}><Text>{business.address}</Text></View>
            </View>
                <View style={pdfstyles.taxInvoiceHeader, { marginTop: "10px", textAlign: "center" }}>
                    <View style={{ fontFamily: "Helvetica-Bold", textDecoration: "underline", width: "500px" }}><Text>{state.name.split("....")[1] + " Ledger"}</Text></View>
                    <View style={{ width: "500px" }}><Text>{period}</Text></View>
                </View></View> : ''}
            <View style={pdfstyles.itemSectionLedger, { marginTop: "20px", marginBottom: "5px", fontFamily: "Helvetica-Bold", textAlign: "center", borderTop: "1px solid black", borderBottom: "1px solid black" }}>
                <View style={pdfstyles.itemHeader}><Text style={{ width: '75px', textAlign: "left" }}>Date</Text><Text style={{ width: '75px', textAlign: "left" }}>Voucher No</Text><Text style={{ width: '200px', textAlign: "left" }}>Particulars</Text><Text style={{ width: '75px', textAlign: "right" }}>Debit</Text><Text style={{ width: '75px', textAlign: "right" }}>Credit</Text><Text style={{ width: '75px', textAlign: "right" }}>Balance</Text></View>
            </View>
            {type === 'vendor' ? <PurchaseDataPage data={data} y={y} state={state} /> : <SalesDataPage data={data} y={y} state={state} />}
        </Page >)
    }
    return returnrows;
}
const CustomerLedger = function ({ data, business, fy, state }) {
    let period = fy;
    let l = data.length, i = 0, n = Math.ceil(l / 50), y = 0;
    return (
        <Document>
            <Report business={business} data={data} l={l} n={n} period={period} state={state} />
        </Document>
    )
}
const VendorLedger = function ({ data, business, fy, state }) {
    let period = fy;
    let l = data.length, i = 0, n = Math.ceil(l / 50), y = 0;
    return (
        <Document>
            <Report business={business} data={data} l={l} n={n} period={period} state={state} type="vendor" />
        </Document>
    )
}
const genereateSalesPdf = async (business, fy, state, salesDetail, getPayment, getDebitCredit, businessid, fyid, deleteEntry, getIncomeOS, getAssetAccount) => {
    try {
        salestotalcredit = 0;
        salestotaldebit = 0;
        salesbalance = 0;
        let id = state.name ? state.name.split("....")[0] : '';
        let salesData = salesDetail.data.filter(s => s.customerid === id && s.active == 0 && s.type === 'sales');
        let incomeData = getIncomeOS.data.filter(i => i.customerid === id);
        let paymentData = getPayment.data.filter(p => p.customerid === id);
        let debitcredit = getDebitCredit.data.filter(d => d.partyid === id);
        let assetData = getAssetAccount.data.filter(d => d.soldfyid === fyid && d.customerid.split("...")[0] === id);
        let data = [];
        data = data.concat(salesData);
        data = data.concat(paymentData);
        data = data.concat(debitcredit);
        data = data.concat(incomeData);
        data = data.concat(assetData);
        data = data.sort(sortData);
        const doc = <CustomerLedger data={data} business={business} fy={fy} state={state} />;
        const blobPdf = pdf(doc);
        blobPdf.updateContainer(doc);
        const result = await blobPdf.toBlob();
        let filename = state.name.split("....")[1].toUpperCase() + " Ledger.pdf";
        saveAs(result, filename);
    }
    catch (err) {
        alert(err);
    }
}
const genereatePurchasePdf = async (business, fy, state, purchaseDetail, getPayment, getDebitCredit, businessid, fyid, deleteEntry, getExpense, getAssetAccount) => {
    try {
        purchasetotalcredit = 0;
        purchasetotaldebit = 0;
        purchasebalance = 0;
        let id = state.name ? state.name.split("....")[0] : '';
        let purchaseData = purchaseDetail.data.filter(p => p.vendorid === id);
        let paymentData = getPayment.data.filter(p => p.vendorid === id);
        let debitcredit = getDebitCredit.data.filter(d => d.partyid === id);
        let expenseData = getExpense.data.filter(d => d.vendorid === id);
        let assetData = getAssetAccount.data.filter(d => d.purchasefyid === fyid);
        let data = [];
        data = data.concat(purchaseData);
        data = data.concat(paymentData);
        data = data.concat(debitcredit);
        data = data.concat(expenseData);
        data = data.concat(assetData);
        data = data.sort(sortDataPurchase);
        const doc = <VendorLedger data={data} business={business} fy={fy} state={state} />;
        const blobPdf = pdf(doc);
        blobPdf.updateContainer(doc);
        const result = await blobPdf.toBlob();
        let filename = state.name.split("....")[1].toUpperCase() + " Ledger.pdf";
        saveAs(result, filename);
    }
    catch (err) {
        alert(err);
    }
}

const DataDisplay = function ({ state, salesDetail, purchaseDetail, getPayment, businessList, handleInputChange, handleSubmit, fy, getDebitCredit, getExpense, businessid, fyid, deleteEntry, getIncomeOS, getAssetAccount, business }) {
    let returnrows = [];
    if (state.name !== '') {
        if (state.name && state.customerorvendor === 'customer') {
            returnrows.push(
                <div>
                    <h6 className="text-center mt-2 mb-2"><u>{state.name.split("....")[1]}</u>&nbsp;&nbsp;&nbsp;<FontAwesomeIcon icon={faFilePdf} size="lg" onClick={() => genereateSalesPdf(business, fy, state, salesDetail, getPayment, getDebitCredit, businessid, fyid, deleteEntry, getIncomeOS, getAssetAccount)} />&nbsp;&nbsp;&nbsp;</h6>
                    <div className="d-flex small mb-1 p-1 d-md-none d-lg-none">
                        <div className="col-5 col-md-5 p-1">
                        </div>
                        <div className="col-3 col-md-1 text-right"><strong>Debit</strong></div>
                        <div className="col-3 col-md-6 p-1 text-right"><strong>Credit</strong></div>
                        <div className="col-1 p-0"></div>
                    </div>
                    <div className="container small mt-3 d-none d-md-block p-0">
                        <div className="d-flex">
                            <div className="col-1 p-0"><strong>Date</strong></div>
                            <div className="col-1 p-0"><strong>Voucher No</strong></div>
                            <div className="col-4 p-0"><strong>Particulars</strong></div>
                            <div className="col-2 text-right"><strong>Debit</strong></div>
                            <div className="col-3 text-right"><strong>Credit</strong></div>
                        </div>
                    </div>
                </div>)
            returnrows.push(<DataRowSales state={state} salesDetail={salesDetail} getPayment={getPayment} fyyear={fy} getDebitCredit={getDebitCredit} businessid={businessid} fyid={fyid} deleteEntry={deleteEntry} getIncomeOS={getIncomeOS} getAssetAccount={getAssetAccount} />)
        }
        else if (state.name && state.customerorvendor === 'vendor') {
            returnrows.push(
                <div>
                    <h6 className="text-center mt-2 mb-2"><u>{state.name.split("....")[1]}</u>&nbsp;&nbsp;&nbsp;<FontAwesomeIcon icon={faFilePdf} size="lg" onClick={() => genereatePurchasePdf(business, fy, state, purchaseDetail, getPayment, getDebitCredit, businessid, fyid, deleteEntry, getExpense, getAssetAccount)} />&nbsp;&nbsp;&nbsp;</h6>
                    <div className="d-flex small mb-1 p-1 d-md-none d-lg-none">
                        <div className="col-5 col-md-5 p-1">
                        </div>
                        <div className="col-3 col-md-1 text-right"><strong>Debit</strong></div>
                        <div className="col-3 col-md-6 p-1 text-right"><strong>Credit</strong></div>
                        <div className="col-1 p-0"></div>
                    </div>
                    <div className="container small mt-3 d-none d-md-block p-1">
                        <div className="d-flex">
                            <div className="col-1 p-0"><strong>Date</strong></div>
                            <div className="col-1 p-0"><strong>Voucher No</strong></div>
                            <div className="col-4 p-0"><strong>Particulars</strong></div>
                            <div className="col-2 text-right"><strong>Debit</strong></div>
                            <div className="col-3 text-right"><strong>Credit</strong></div>
                        </div>
                    </div>
                </div>)
            returnrows.push(<DataRowPurchase state={state} purchaseDetail={purchaseDetail} getPayment={getPayment} fyyear={fy} getDebitCredit={getDebitCredit} getExpense={getExpense} businessid={businessid} fyid={fyid} deleteEntry={deleteEntry} getAssetAccount={getAssetAccount} />)
        }
    }
    return returnrows;
}

const SelectParty = function ({ customer, vendor, state, handleInputChange }) {
    let returnrows = [];
    if (state.customerorvendor === 'vendor') {
        returnrows.push(<div className="form-group mb-0 col-12 p-1">
            <label htmlFor="name" className="mb-0 muted-text"><strong>Select Vendor:<span className="text-danger">*</span></strong></label>
            <Control.select model=".name" className="form-control form-control-sm" id="name" name="name" onChange={handleInputChange} value={state.name}>
                <option value="">Select Vendor</option>
                {vendor.map(v => <option value={v.vendorid + "...." + v.businessname}>{v.alias ? v.alias : v.businessname}</option>)}
            </Control.select>
        </div>)
    }
    else if (state.customerorvendor === 'customer') {
        returnrows.push(<div className="form-group mb-0 col-12 p-1">
            <label htmlFor="name" className="mb-0 muted-text"><strong>Select Customer:<span className="text-danger">*</span></strong></label>
            <Control.select model=".name" className="form-control form-control-sm" id="name" name="name" onChange={handleInputChange} value={state.name}>
                <option value="">Select Customer</option>
                {customer.map(c => <option value={c.customerid + "...." + c.businessname}>{c.alias ? c.alias : c.businessname}</option>)}
            </Control.select>
        </div>)
    }
    return returnrows;
}

class AccountStatement extends Component {
    constructor(props) {
        super(props);
        this.state = ({
            customerorvendor: '',
            name: '',
            openingbalance: 0,
            amounttype: ''
        })
        window.onload = () => {
            this.props.resetPaymentDetail();
            this.props.resetOpeningBalance();
            this.props.refreshVendorState();
            this.props.refreshCustomerState();
            this.props.resetSalesDetail();
            this.props.resetPurchaseDetail();
            this.props.refreshDebitCredit();
            this.props.resetExpenseDetail();
            this.props.resetIncomeOSDetail();
            this.props.refreshAssetAccountState();
        }
    }
    componentDidMount() {
        if ((this.props.getVendor.message === 'initial') && (this.props.getVendor.loading === false)) this.props.getVendorList(this.props.businessid);
        if ((this.props.getCustomer.message === 'initial') && (this.props.getCustomer.loading === false)) this.props.getCustomerList(this.props.businessid);
        if ((this.props.salesDetail.message === 'initial') && (this.props.salesDetail.loading === false)) this.props.getSalesData(this.props.businessid, this.props.fyid);
        if ((this.props.purchaseDetail.message === 'initial') && (this.props.purchaseDetail.loading === false)) this.props.getPurchaseData(this.props.businessid, this.props.fyid);
        if ((this.props.getPayment.message === 'initial') && (this.props.getPayment.loading === false)) this.props.getPaymentDetail(this.props.businessid, this.props.fyid);
        if ((this.props.getOpeningBalance.message === 'initial') && (this.props.getOpeningBalance.loading === false)) this.props.getPartyOpeningBalance(this.props.businessid, this.props.fyid);
        if (this.props.getDebitCredit.message === 'initial') this.props.getDebitCreditList(this.props.businessid, this.props.fyid);
        if (this.props.getExpense.message === 'initial') this.props.getExpenseDetail(this.props.businessid, this.props.fyid);
        if (this.props.getIncomeOS.message === 'initial') this.props.getIncomeOSDetail(this.props.businessid, this.props.fyid);
        if (this.props.getAssetAccount.message === 'initial') this.props.getAssetAccountList(this.props.businessid);
        if (this.props.logoutMessage.error !== '') {
            alert("Error Logging Out. Please Try Again");
            this.props.resetLogoutMessage();
        }
        if (this.props.deleteEntryMessage.message === "success") {
            alert("Purchase Entry Deleted Successfully");
            this.props.resetDeleteEntry();
        }
        if (this.props.deleteEntryMessage.error !== '') {
            alert("Error Deleting Purchase Entry - Please Try Again");
            this.props.resetDeleteEntry();
        }
    }
    handleInputChange = (event) => {
        let target = event.target;
        let name = target.name;
        let value = target.value;
        if (/^customerorvendor/i.test(name)) {
            this.setState({
                ...this.state,
                customerorvendor: value,
                name: '',
                openingbalance: 0,
                amounttype: ''
            })
        }
        else if (/^name/i.test(name)) {
            let data = this.props.getOpeningBalance.data.filter(d => d.businessid === this.props.businessid && d.fyid === this.props.fyid && d.partyid === value.split("....")[0])[0];
            let openingbalance = data ? data.amount : 0;
            let amounttype = data ? data.amounttype : '';
            this.setState({
                ...this.state,
                openingbalance: openingbalance,
                [name]: value,
                amounttype: amounttype
            })
        }
        else {
            this.setState({
                ...this.state,
                [name]: value
            })
        }
    }
    render() {
        if (!cookie.load('userservice')) return <Redirect to="/" />;
        else {
            if ((this.props.getPayment.loading === true) || (this.props.getOpeningBalance.loading === true) || (this.props.getCustomer.loading === true) || (this.props.getVendor.loading === true) || (this.props.salesDetail.loading === true) || (this.props.purchaseDetail.loading === true) || (this.props.logoutMessage.loading === true) || (this.props.getDebitCredit.loading === true) || (this.props.getExpense.loading === true) || (this.props.deleteEntryMessage.loading === true) || (this.props.getIncomeOS.loading === true) || (this.props.getAssetAccount.loading === true)) {
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div className="mt-3 d-flex justify-content-center">
                            <button className="btn btn-danger">Loading... </button>
                        </div>
                    </div>
                );
            }
            else if ((this.props.getPayment.error !== '') || (this.props.getOpeningBalance.error !== '') || (this.props.getCustomer.error !== '') || (this.props.getVendor.error !== '') || (this.props.salesDetail.error !== '') || (this.props.purchaseDetail.error !== '') || (this.props.getDebitCredit.error !== '') || (this.props.getExpense.error !== '') || (this.props.getIncomeOS.error !== '') || (this.props.getAssetAccount.error !== '')) {
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div className="mt-3 d-flex justify-content-center">
                            <button className="btn btn-danger">Error.. Please Refresh and Try Again </button>
                        </div>
                    </div>
                );
            }
            else {
                let business = this.props.businessList.data.filter(b => b.businessid === this.props.businessid)[0];
                let fy = business.financial.filter(f => f.fyid === this.props.fyid)[0].financialyear;
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div style={{ minHeight: "93vh", display: "flex", flexDirection: "column" }}>
                            <div className="container" style={{ flex: "1" }}>
                                <h6 className="text-center pl-2 pr-2 pt-4" style={{ lineHeight: "1" }}><strong>{business.businessname} {fy}</strong></h6>
                                <h6 className="text-center pl-2 pr-2 mb-4" style={{ lineHeight: "1" }}>{business.alias ? "(" + business.alias + ")" : ''}</h6>
                                <h6 className="text-center pl-2 pr-2"><strong><u>Account Statement</u></strong></h6>
                                <div className="container col-12 col-md-4" >
                                    <LocalForm className="small">
                                        <div className="row justify-content-center mt-1">
                                            <div className="form-group mb-0 col-12 p-1">
                                                <label htmlFor="paymenttype" className="mb-0 muted-text"><strong>Vendor/Customer<span className="text-danger">*</span></strong></label>
                                                <Control.select model=".paymenttype" className="form-control form-control-sm" id="paymenttype" name="customerorvendor" onChange={this.handleInputChange} value={this.state.customerorvendor}>
                                                    <option value="">Select Vendor/Customer</option>
                                                    <option value="vendor">Vendor</option>
                                                    <option value="customer">Customer</option>
                                                </Control.select>
                                            </div>
                                            < SelectParty customer={this.props.getCustomer.data} vendor={this.props.getVendor.data} state={this.state} handleInputChange={this.handleInputChange} />
                                        </div>
                                    </LocalForm>
                                </div>
                                <div className="col-12 p-0">
                                    <DataDisplay state={this.state} salesDetail={this.props.salesDetail} purchaseDetail={this.props.purchaseDetail} getPayment={this.props.getPayment} fyid={this.props.fyid} businessList={this.props.businessList} businessid={this.props.businessid} handleInputChange={this.handleInputChange} handleSubmit={this.handleSubmit} fy={fy} getDebitCredit={this.props.getDebitCredit} getExpense={this.props.getExpense} businessid={this.props.businessid} fyid={this.props.fyid} deleteEntry={this.props.deleteEntry} getIncomeOS={this.props.getIncomeOS} getAssetAccount={this.props.getAssetAccount} business={business} />
                                </div>
                            </div>
                            <Footer />
                        </div>
                    </div>
                )
            }
        }
    }
}
export default AccountStatement;