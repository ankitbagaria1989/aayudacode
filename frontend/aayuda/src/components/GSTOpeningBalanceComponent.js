import React, { Component } from 'react';
import cookie from 'react-cookie';
import { Redirect } from 'react-router-dom';
import { LocalForm, Control } from 'react-redux-form';
import { Header } from './HeaderComponent';
import { Footer } from './Footer';

class GSTOpeningBalance extends Component {
    constructor(props) {
        super(props);
        this.state = ({
            inputpayable: this.props.getGSTOpeningBalance.inputpayable ? this.props.getGSTOpeningBalance.inputpayable : '',
            gst: this.props.getGSTOpeningBalance.gst ? this.props.getGSTOpeningBalance.gst :0
        })
        window.onload = () => {
            this.props.resetGSTOpeningBalance();
        }
    }
    componentDidMount() {
        if (this.props.updateGSTOpeningBalanceMessage.message === 'success') {
            alert("GST Opening Balance Updated Successfully");
            this.props.resetGSTOpeningBalanceMessage();
        }
        if (this.props.updateGSTOpeningBalanceMessage.error !== '') {
            alert(this.props.updateGSTOpeningBalanceMessage.error);
            this.setState({
                ...this.props.updateGSTOpeningBalanceMessage.state
            })
            this.props.resetGSTOpeningBalanceMessage();
        }
        if (this.props.getGSTOpeningBalance.message === 'initial') this.props.getGSTOpeningBalanceDetail(this.props.businessid, this.props.fyid);
        if (this.props.logoutMessage.error !== '') {
            alert("Error Logging Out. Please Try Again");
            this.props.resetLogoutMessage();
        }
    }
    handleInputChange = (event) => {
        let target = event.target;
        let name = target.name;
        let value = target.value;
            this.setState({
                ...this.state,
                [name]: value
            })
    }
    handleSubmit = () => {
        if ((this.state.gst === '') || (this.state.gst < 0) || (isNaN(parseInt(this.state.gst)))) alert(" GST Opening Balance Muste Be Greater Than Equal To 0 ");
        else this.props.updateGSTOpeningBalance(this.props.businessid, this.props.fyid, this.state);
    }

    render() {
        if (!cookie.load('userservice')) return <Redirect to="/" />;
        else {
            if ((this.props.getGSTOpeningBalance.loading === true) || (this.props.logoutMessage.loading === true) || (this.props.updateGSTOpeningBalanceMessage.loading === true)) {
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div className="mt-3 d-flex justify-content-center">
                            <button className="btn btn-danger">Loading... </button>
                        </div>
                    </div>
                );
            }
            else if (this.props.getGSTOpeningBalance.error !== '') {
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div className="mt-3 d-flex justify-content-center">
                            <button className="btn btn-danger">Error.. Please Refresh and Try Agin </button>
                        </div>
                    </div>
                );
            }
            else {
                let business = this.props.businessList.data.filter(b => b.businessid === this.props.businessid)[0];
                let fy = business.financial.filter(f => f.fyid === this.props.fyid)[0].financialyear;
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div style={{ minHeight: "93vh", display: "flex", flexDirection: "column" }}>
                            <div className="container" style={{ flex: "1" }}>
                                <h6 className="text-center pl-2 pr-2 pt-4" style={{ lineHeight: "1" }}><strong>{business.businessname} {fy}</strong></h6>
                                <h6 className="text-center pl-2 pr-2 mb-4" style={{ lineHeight: "1" }}>{business.alias ? "(" + business.alias + ")" : ''}</h6>
                                <h6 className="text-center pl-2 pr-2"><strong><u>GST Opening Balance</u></strong></h6>
                                <div className="" >
                                    <LocalForm className="container col-12 col-md-4 small mt-4" onSubmit={this.handleSubmit}>
                                        <div className="form-group mb-1 col-12">
                                            <label><input type="radio" name="inputpayable" id="inputpayable" value="input" checked={this.state.inputpayable === "input" ? true : false} onChange={this.handleInputChange} /> GST Input </label>&nbsp;&nbsp;&nbsp;&nbsp;
                                                <label><input type="radio" name="inputpayable" id="inputpayable" value="payable" checked={this.state.inputpayable === "payable" ? true : false} onChange={this.handleInputChange} /> GST Payable</label><br />
                                        </div>
                                        <div className="form-group mb-1 col-12">
                                            <label htmlFor="gst" className="mb-0 muted-text"><strong>Opening Balance  <span className="text-danger">*</span></strong></label>
                                            <Control.text model=".gst" className="form-control form-control-sm" id="gst" name="gst" onChange={this.handleInputChange} value={this.state.gst} />
                                        </div>
                                        <div className="col-12 mb-5 mt-2 pb-5">
                                            <button type="submit" className="btn btn-success mt-1 btn-sm small" >Update</button>
                                        </div>
                                    </LocalForm>
                                </div>
                            </div>
                            <Footer />
                        </div>
                    </div>
                )
            }
        }
    }
}

export default GSTOpeningBalance;