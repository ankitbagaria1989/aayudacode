import React, { Component } from 'react';
import { Form, Errors, Control } from 'react-redux-form';
import cookie from 'react-cookie';
import { Redirect } from 'react-router-dom';
import { Header } from './HeaderComponent';
import { Footer } from './Footer';

const required = (val) => val && val.length;
const maxLength = (len) => val => val ? val.length < len : true;
const length = (len) => val => (val) ? val.length === len : true;
const validEmail = (val) => /^(?:[A-Za-z0-9.+-_%$#*]+@[A-Za-z0-9.+-_%$#*]+\.[A-Za-z]{2,4}|)$/i.test(val);

const ErrorComponent = function ({ children }) {
    return (
        <div className="pb-1 small">{children}</div>
    );
}

const FormElement = function ({ handleSubmit }) {
    return (
        <Form model="vendorChange" onSubmit={((values) => handleSubmit(values))}>
            <div className="justify-content-center mt-4">
                <div className="form-group col-12 mb-1">
                    <label htmlFor="businessname" className="mb-0"><span className="muted-text">Business Name<span className="text-danger">*</span></span></label>
                    <div className="">
                        <Control.text model=".businessname" className="form-control form-control-sm pt-3 pb-3" id="businessname" name="businessname" placeholder="Business Name" validators={{ required, maxLength: maxLength(100) }} />
                        <Errors className="text-danger" model=".businessname" show="touched" messages={{
                            required: "Business Name is Mandatory to Register",
                            maxLength: "length must be within 100 characters"
                        }}
                            component={ErrorComponent}
                        />
                    </div>
                </div>
                <div className="form-group col-12 mb-1">
                    <label htmlFor="alias" className="mb-0"><span className="muted-text">Alias</span></label>
                    <div className="">
                        <Control.text model=".alias" className="form-control form-control-sm pt-3 pb-3" id="alias" name="alias" placeholder="Alias" validators={{ maxLength: maxLength(100) }} />
                        <Errors className="text-danger" model=".alias" show="touched" messages={{
                            maxLength: "length must be within 100 characters"
                        }}
                            component={ErrorComponent}
                        />
                    </div>
                </div>
                <div className="form-group col-12 mb-1">
                    <label htmlFor="gstin" className="mb-0 muted-text">Gstin   </label>
                    <div className="">
                        <Control.text model=".gstin" className="form-control form-control-sm pt-3 pb-3" id="gstin" name="gstin" placeholder="GSTIN" validators={{ length: length(15) }} />
                        <Errors className="text-danger" model=".gstin" show="touched" messages={{
                            length: "length must be 15 characters"
                        }}
                            component={ErrorComponent}
                        />
                    </div>
                </div>
                <div className="form-group col-12 mb-1">
                    <label htmlFor="state" className="mb-0 muted-text">State</label>
                    <div className="">
                        <Control.text model=".state" className="form-control form-control-sm pt-3 pb-3" id="state" name="state" placeholder="State" validators={{ maxLength: maxLength(15) }} />
                        <Errors className="text-danger" model=".state" show="touched" messages={{
                            maxLength: "length must be within 15 characters"
                        }}
                            component={ErrorComponent}
                        />
                    </div>
                </div>
                <div className="form-group col-12 mb-1">
                    <label htmlFor="address" className="mb-0 muted-text">Address</label>
                    <div className="">
                        <Control.text model=".address" className="form-control form-control-sm pt-3 pb-3" id="address" name="address" placeholder="Address" validators={{ maxLength: maxLength(300) }} />
                        <Errors className="text-danger" model=".address" show="touched" messages={{
                            maxLength: "length must be within 300 characters"
                        }}
                            component={ErrorComponent}
                        />
                    </div>
                </div>
                <div className="form-group col-12 mb-1">
                    <label htmlFor="pincode" className="mb-0 muted-text">Pincode</label>
                    <div className="">
                        <Control.text model=".pincode" className="form-control form-control-sm pt-3 pb-3" id="pincode" name="pincode" placeholder="Pincode" validators={{ length: length(6) }} />
                        <Errors className="text-danger" model=".pincode" show="touched" messages={{
                            length: "length must be 6 digits"
                        }}
                            component={ErrorComponent}
                        />
                    </div>
                </div>
                <div className="form-group col-12 mb-1">
                    <label htmlFor="mobile" className="mb-0 muted-text">Mobile Number</label>
                    <div className="">
                        <Control.text model=".mobile" className="form-control form-control-sm pt-3 pb-3" id="mobile" name="mobile" placeholder="Mobile Number" validators={{ maxLength: maxLength(20) }} />
                        <Errors className="text-danger" model=".mobile" show="touched" messages={{
                            maxLength: "Length must be within 20 characters"
                        }}
                            component={ErrorComponent}
                        />
                    </div>
                </div>
                <div className="form-group col-12 mb-1">
                    <label htmlFor="landline" className="mb-0 muted-text">LandLine Number</label>
                    <div className="">
                        <Control.text model=".landline" className="form-control form-control-sm pt-3 pb-3" id="landline" name="landline" placeholder="LandLine eg: 01202571836" validators={{ maxLength: maxLength(20) }} />
                        <Errors className="text-danger" model=".landline" show="touched" messages={{
                            maxLength: "Length must be within 20 characters"
                        }}
                            component={ErrorComponent}
                        />
                    </div>
                </div>
                <div className="form-group col-12">
                    <label htmlFor="Email" className="mb-0 muted-text">Email</label>
                    <div className="">
                        <Control type="Email" model=".email" className="form-control form-control-sm pt-3 pb-3" id="email" name="email" placeholder="Email" validators={{ validEmail }} />
                        <Errors className="text-danger" model=".email" show="touched" messages={{
                            validEmail: "Must be a Valid Email Format"
                        }}
                            component={ErrorComponent}
                        />
                        <div className="mt-3"><button type="submit" className="btn btn-success btn-sm pl-4 pr-4" >Update</button></div>
                    </div>
                </div>
            </div>
        </Form>
    );
}

class VendorChange extends Component {
    constructor(props) {
        super(props);
        this.state = {
            vendor: this.props.getVendor.data.filter((vend) => {
                if ((vend.businessid === this.props.businessid) && (vend.vendorid === this.props.vendorid)) return vend;
            })[0]
        };
        console.log("State " + JSON.stringify(this.state.vendoor));
        if ((this.state.vendor !== undefined) && (this.props.updateVendorMessage.error === '') && (this.props.updateVendorMessage.loading === false)) this.props.setDefaultVendorChange(this.state.vendor);
    }

    componentDidMount() {
        if (this.props.updateVendorMessage.error !== '') {
            alert(this.props.updateVendorMessage.error + 'Please try again');
            this.props.resetVendorChange();
        }
        if (this.props.updateVendorMessage.message === 'success') {
            alert('Customer Update Successfully');
            this.props.resetVendorChange();
        }
        if (this.props.logoutMessage.error !== '') {
            alert("Error Logging Out. Please Try Again");
            this.props.resetLogoutMessage();
        }
    }

    handleSubmit = (values) => {
        this.props.updateVendor(this.state.vendor.businessid, this.state.vendor.vendorid, values);
    }

    render() {
        if (!cookie.load('userservice')) return <Redirect to="/" />;
        else {
            if ((this.props.logoutMessage.loading === true) || (this.props.updateVendorMessage.loading === true)) {
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div className="mt-3 d-flex justify-content-center">
                            <button className="btn btn-danger">Loading... </button>
                        </div>
                    </div>
                );
            }
            else {
                let business = this.props.businessList.data.filter(b => b.businessid === this.props.businessid)[0];
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div style={{ minHeight: "93vh", display: "flex", flexDirection: "column" }}>
                            <div className="container small d-flex justify-content-center pr-0 pl-0" style={{ flex: "1" }}>
                                <div className="col-12 col-md-6">
                                    <div className="pr-0 pl-0">
                                    <h6 className="text-center pl-2 pr-2 pt-4" style={{ lineHeight: "1" }}><strong>{business.businessname}</strong></h6>
                                        <h6 className="text-center pl-2 pr-2 mb-4" style={{ lineHeight: "1" }}>{business.alias ? "(" + business.alias + ")" : ''}</h6>
                                        <h6 className="mt-2 text-center">
                                            <strong><u>Update Vendor Detail</u></strong>
                                        </h6>
                                        <FormElement handleSubmit={this.handleSubmit} />
                                    </div>
                                </div>
                            </div>
                            <Footer />
                        </div>
                    </div>
                );
            }
        }
    };
}

export default VendorChange;