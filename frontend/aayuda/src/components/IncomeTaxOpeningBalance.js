import React, { Component } from 'react';
import cookie from 'react-cookie';
import { Redirect } from 'react-router-dom';
import { LocalForm, Errors, Control } from 'react-redux-form';
import { Header } from './HeaderComponent';
import { Footer } from './Footer';

class IncomeTaxOpeningBalance extends Component {
    constructor(props) {
        super(props);
        this.state = ({
            payreceive: '',
            amount : ''
        })
        window.onload = () => {
            this.props.resetIncomeTaxOpeningBalance();
        }
    }
    handleSubmit = () => {
        if (this.state.payreceive === '') alert("Please Select Income Tax Payable or Receivable");
        else {
            if ((this.state.amount === '') || (this.state.amount < 0) || (isNaN(parseInt(this.state.amount)))) alert(" Amount Must Be Greater Than Equal To 0 ");
            else this.props.updateIncomeTaxOpeningBalance(this.state, this.props.businessid, this.props.fyid);
        }
    }
    handleInputChange = (event) => {
        let target = event.target;
        let name = target.name;
        let value = target.value;
        if (name === 'payreceive') {
            let amount = '';
            this.props.getIncomeTaxOpeningBalance.data.forEach(d => d.payreceive === value ? amount = d.amount : '');
            if (this.props.getIncomeTaxOpeningBalance.data.length > 0) {
                if (this.props.getIncomeTaxOpeningBalance.data[0].payreceive) {
                    this.setState({
                        payreceive: value,
                        amount : amount
                    })            
                }
            }
            else this.setState({
                payreceive : value,
                amount : ''
            })
        }
        else this.setState({
            ...this.state,
            [name]: value
        })
    }
    componentDidMount() {
        if (this.props.updateIncomeTaxOpeningMessage.error !== '') {
            alert(this.props.updateIncomeTaxOpeningMessage.error);
            this.setState({
                ...this.props.updateIncomeTaxOpeningMessage.state
            })
            this.props.refreshUpdateIncomeTaxOpeningBalance();
        }
        if (this.props.updateIncomeTaxOpeningMessage.message === 'success') {
            alert("Data Updated Successfully");
            this.setState({
                ...this.props.getIncomeTaxOpeningBalance.state
            })
            this.props.refreshUpdateIncomeTaxOpeningBalance();
        }
        if (this.props.getIncomeTaxOpeningBalance.message === 'initial') this.props.getIncomeTaxOpeningBalanceList(this.props.businessid, this.props.fyid);
        if (this.props.logoutMessage.error !== '') {
            alert("Error Logging Out. Please Try Again");
            this.props.resetLogoutMessage();
        }
    }
    render() {
        if (!cookie.load('userservice')) return <Redirect to="/" />;
        else {
            if ((this.props.updateIncomeTaxOpeningMessage.loading === true) || (this.props.getIncomeTaxOpeningBalance.loading === true) || (this.props.logoutMessage.loading === true)) {
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div className="mt-3 d-flex justify-content-center">
                            <button className="btn btn-danger">Loading... </button>
                        </div>
                    </div>
                );
            }
            else if (this.props.getIncomeTaxOpeningBalance.error !== '') {
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div className="mt-3 d-flex justify-content-center">
                            <button className="btn btn-danger">Error. Please Refresh and Try Again</button>
                        </div>
                    </div>
                );
            }
            else {
                let business = this.props.businessList.data.filter(b => b.businessid === this.props.businessid)[0];
                let fy = business.financial.filter(f => f.fyid === this.props.fyid)[0].financialyear;
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div style={{ minHeight: "93vh", display: "flex", flexDirection: "column" }}>
                            <div className="container" style={{ flex: "1" }}>
                                <h6 className="text-center pl-2 pr-2 pt-4" style={{ lineHeight: "1" }}><strong>{business.businessname} {fy}</strong></h6>
                                <h6 className="text-center pl-2 pr-2 mb-4" style={{ lineHeight: "1" }}>{business.alias ? "(" + business.alias + ")" : ''}</h6>
                                <h6 className="text-center pl-2 pr-2 col-12"><strong><u>TDS / Income Tax Opening Balance</u></strong></h6>
                                <div className="row">
                                    <LocalForm className="small col-12 d-flex justify-content-center" onSubmit={this.handleSubmit}>
                                        <div className="container mt-4 col-12 col-md-4">
                                            <div className="form-group mb-1 col-12">
                                                <label htmlFor="payreceive" className="mb-0 muted-text"><strong>Select Payable/Receivable<span className="text-danger">*</span></strong></label>
                                                <Control.select model=".payreceive" className="form-control form-control-sm" id="payreceive" name="payreceive" onChange={this.handleInputChange} value={this.state.payreceive}>
                                                    <option value="">Payable/Receivable</option>
                                                    <option value="paytds">TDS Payable</option>
                                                    <option value="receivetds">TDS Receivable</option>
                                                    <option value="payit">Income Tax Payable</option>
                                                </Control.select>
                                            </div>
                                            {this.state.payreceive !== '' ? <div class="col-12 p-0">
                                                <div className="form-group mb-1 col-12">
                                                    <label htmlFor="amount" className="mb-0 muted-text"><strong>Opening Value Amount</strong><span className="text-danger">*</span></label>
                                                    <Control.text model=".amount" className="form-control form-control-sm" id="amount" name="amount" onChange={this.handleInputChange} value={this.state.amount} />
                                                </div>
                                                <div className="form-group mb-1 col-12">
                                                    <button type="submit" className="btn btn-success mt-1 btn-sm small" >Update</button>
                                                </div>
                                            </div> : ''} 
                                        </div>
                                    </LocalForm>
                                </div>
                            </div>
                            <Footer />
                        </div>
                    </div>
                )
            }
        }
    }
}
export default IncomeTaxOpeningBalance;