import React, { Component } from 'react';
import cookie from 'react-cookie';
import { Redirect, Link } from 'react-router-dom';
import { Button } from 'reactstrap';
import { Header } from './HeaderComponent';
import 'react-calendar/dist/Calendar.css';
import { Footer } from './Footer';

const ViewInventory = function ({ businessid, fyid }) {
    return (
        <div className="d-flex justify-content-center">
            <div className="col-12">
                <Link to={`/business/businessdetail/viewstock/${businessid}/${fyid}`}><Button className="col-12 mb-3 pt-1 pb-1 btn-sm mt-4" style={{ backgroundColor: "white", color: "rgb(40, 167, 69)", border: "1px solid rgb(40, 167, 69)", fontSize: "18px" }}>View Stock </Button></Link>
            </div>
        </div>);
}

const PassStockJournal = function ({ businessid, fyid }) {
    return (
        <div className="d-flex justify-content-center">
            <div className="col-12">
                <Link to={`/business/businessdetail/passstockjournal/${businessid}/${fyid}`}><Button className="col-12 mb-3 pt-1 pb-1 btn-sm" style={{ backgroundColor: "white", color: "rgb(40, 167, 69)", border: "1px solid rgb(40, 167, 69)", fontSize: "18px" }}>Production Stock Journal</Button></Link>
            </div>
        </div>);
}


const StockOpeningBalance = function ({ businessid, fyid }) {
    return (
        <div className="d-flex justify-content-center">
            <div className="col-12">
                <Link to={`/business/businessdetail/stockopeningbalance/${businessid}/${fyid}`}><Button className="col-12 mb-3 pt-1 pb-1 btn-sm" style={{ backgroundColor: "white", color: "rgb(40, 167, 69)", border: "1px solid rgb(40, 167, 69)", fontSize: "18px" }}>Stock Opening Balance</Button></Link>
            </div>
        </div>);
}

class Inventory extends Component {
    componentDidMount() {
        window.onload = () => {
            this.props.resetSalesDetail();
            this.props.resetPurchaseDetail();
            this.props.resetStockOpeningBalance();
        }
        if (this.props.logoutMessage.error !== '') {
            alert("Error Logging Out. Please Try Again");
            this.props.resetLogoutMessage();
        }
        if (this.props.getStockOpeningBalance.message === 'initial') this.props.getStockOpeningBalanceList(this.props.businessid, this.props.fyid);
        if (this.props.purchaseDetail.message === 'initial') this.props.getPurchaseData();
        if (this.props.salesDetail.message === 'initial') this.props.getSalesData();
        if (this.props.getStockJournal.message === 'initial') this.props.getStockJournalList(this.props.businessid, this.props.fyid);
    }

    render() {
        if (!cookie.load('userservice')) return <Redirect to="/" />
        else {
            if ((this.props.logoutMessage.loading === true) || (this.props.purchaseDetail.loading === true) || (this.props.salesDetail.loading === true) || (this.props.getStockOpeningBalance.loading === true)) {
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div className="mt-3 d-flex justify-content-center">
                            <button className="btn btn-danger">Loading... </button>
                        </div>
                    </div>
                );
            }
            else if ((this.props.purchaseDetail.error !== '') || (this.props.salesDetail.error !== '') || (this.props.getStockOpeningBalance.error !== '')) {
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div className="mt-3 d-flex justify-content-center">
                            <button className="btn btn-danger">Error... Please Try Again </button>
                        </div>
                    </div>
                );
            }
            else {
                let business = this.props.businessList.data.filter(b => b.businessid === this.props.businessid)[0];
                let fy = business.financial.filter(f => f.fyid === this.props.fyid)[0].financialyear;
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div style={{ minHeight: "93vh", display: "flex", flexDirection: "column" }}>
                            <div className="container d-flex justify-content-center pr-0 pl-0" style={{ flex: "1" }}>
                                <div className="col-12 col-md-5">
                                    <div className="small pr-0 pl-0">
                                        <h6 className="text-center pl-2 pr-2 pt-4" style={{ lineHeight: "1" }}><strong>{business.businessname} {fy}</strong></h6>
                                        <h6 className="text-center pl-2 pr-2 mb-4" style={{ lineHeight: "1" }}>{business.alias ? "(" + business.alias + ")" : ''}</h6>
                                        <h6 className="text-center pl-2 pr-2"><strong><u>Inventory</u></strong></h6>
                                        <ViewInventory businessid={this.props.businessid} fyid={this.props.fyid} />
                                        <PassStockJournal businessid={this.props.businessid} fyid={this.props.fyid} />
                                        <StockOpeningBalance businessid={this.props.businessid} fyid={this.props.fyid} />
                                    </div>
                                </div>
                            </div>
                            <Footer />
                        </div>
                    </div>
                );
            }
        }
    }
}

export default Inventory;