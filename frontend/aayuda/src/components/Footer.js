import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPhone, faEnvelope } from '@fortawesome/free-solid-svg-icons';

export const Footer = function () {
    return (
        <div className="d-flex justify-content-center mt-4" style={{borderTop:"1px solid rgb(86, 61, 124)", backgroundColor:"rgb(86, 61, 124)", flexShrink:"0"}}>
            <div className="col-md-10 mt-2 small pt-2 pb-3" style={{color:"#fff"}}>
                <div className="row">
                    <div className="col-12 col-md-4 d-none d-md-block">&copy; Aayuda Technologies</div>
                    <div className="col-12 col-md-4 text-center d-md-none">&copy; Aayuda Technologies</div>
                    <div className="col-md-4 d-none d-md-block"></div>
                    <div className="col-12 col-md-4 text-center">
                        <FontAwesomeIcon icon={faPhone} style={{}} /> &nbsp;+91 - 807 - 6212109
                        &nbsp;&nbsp;<FontAwesomeIcon icon={faEnvelope} style={{}} /> &nbsp;care@aayuda.com
                    </div>
                </div>
            </div>
        </div>
    );
}