import React, { Component } from 'react';
import cookie from 'react-cookie';
import { Redirect } from 'react-router-dom';
import { LocalForm, Control } from 'react-redux-form';
import { Header } from './HeaderComponent';
import { Footer } from './Footer';

class IncomeOS extends Component {
    constructor(props) {
        super(props);
        this.state = ({
            category: '',
            description: '',
            voucherno: this.props.getIncomeOS.voucherno ? this.props.getIncomeOS.voucherno : 1,
            date: new Date().toDateString().split(' ').slice(1).join(' '),
            amount: 0,
            paymentmode: '',
            transactionno: '',
            bankid: '',
            tds: false,
            tdsamount: 0,
            cash: false,
            customerid: '',
            partner: false
        })
        window.onload = () => {
            this.props.resetIncomeOSDetail();
            this.props.refreshIncomeHead();
            this.props.refreshBankState();
            this.props.refreshCustomerState();
            this.props.refreshInvestmentAccountState();
        }
    }
    componentDidMount() {
        if (this.props.addIncomeOSMessage.message === 'success') {
            alert("Income Added Successfully");
            this.props.resetaddIncomeOSMessage();
        }
        else if (this.props.addIncomeOSMessage.message === 'Already Registered') {
            alert("Voucher Number " + parseInt(this.props.getExpense.voucherno) - 1 + " already registered");
            this.props.resetaddIncomeOSMessage('success');
        }
        else if (this.props.addIncomeOSMessage.error !== '') {
            alert(this.props.addIncomeOSMessage.error);
            this.setState({
                ...this.props.addIncomeOSMessage.state,
            })
            this.props.resetaddIncomeOSMessage();
        }
        if (this.props.logoutMessage.error !== '') {
            alert("Error Logging Out. Please Try Again");
            this.props.resetLogoutMessage();
        }
        if (this.props.getCustomer.message === 'initial') this.props.getCustomerList(this.props.businessid);
        if (this.props.getBankDetail.message === 'initial') this.props.getBankDetailList(this.props.businessid);
        if (this.props.getIncomeOS.message === 'initial') this.props.getIncomeOSDetail(this.props.businessid, this.props.fyid);
        if (this.props.getIncomeHead.message === 'initial') this.props.getIncomeHeadList(this.props.businessid);
        if (this.props.getInvestmentAccount.message === 'initial') this.props.getInvestmentAccountList(this.props.businessid);
    }
    handleInputChange = (event) => {
        let target = event.target;
        let name = target.name;
        let value = target.value;
        if (name === 'tds') {
            this.setState({
                ...this.state,
                tds: !this.state.tds,
                cash: false,
                partner: false
            })
        }
        else if (name === 'cash') {
            this.setState({
                ...this.state,
                cash: !this.state.cash,
                tds: false,
                partner: false
            })
        }
        else if (name === 'partner') {
            this.setState({
                ...this.state,
                partner: !this.state.cash,
                tds: false,
                cash : false
            })
        }
        else {
            this.setState({
                ...this.state,
                [name]: value
            })
        }
    }
    handleSubmit = () => {
        let business = this.props.businessList.data.filter(b => b.businessid === this.props.businessid)[0];
        let fy = business.financial.filter(f => f.fyid === this.props.fyid)[0].financialyear;
        if ((this.state.cash === false) && (this.state.customerid === '')) alert("Please Select Customer Account");
        else if (this.state.category === '') alert("Please Select Income Head");
        else if (isNaN(parseInt(this.state.voucherno)) || (this.state.voucherno < 0)) alert("Voucher No is mandatory and Must Be In Digits");
        else if (!(/^[a-zA-Z]{3} [0-9]{2} [0-9]{4}$/i.test(this.state.date))) alert("Date Must Be In (MON DD YYYY) Format");
        else if ((new Date(this.state.date) < new Date("01 Apr " + fy.split("-")[0])) || (new Date(this.state.date) > new Date("31 Mar " + fy.split("-")[1]))) alert("Invalid Date. Date Must Be Within FY " + fy);
        else if (this.state.description.length > 99) alert("Remark Must Be Within 100 Characters");
        else if (isNaN(parseInt(this.state.amount)) || (this.state.amount <= 0)) alert("Income Amount Must Be Greater Than or Equal To 0");
        else if ((this.state.tds) && ((isNaN(parseInt(this.state.tdsamount)) || (this.state.tdsamount <= 0)))) alert("TDS Amount Must Be Greater Than 0");
        else this.props.raiseIncomeOS(this.props.businessid, this.props.fyid, this.state);
    }
    render() {
        if (!cookie.load('userservice')) return <Redirect to="/" />;
        else {
            if ((this.props.addIncomeOSMessage.loading === true) || (this.props.getIncomeOS.loading === true) || (this.props.logoutMessage.loading === true) || (this.props.getIncomeHead.loading === true) || (this.props.getBankDetail.loading === '') || (this.props.getCustomer.loading === true)) {
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div className="mt-3 d-flex justify-content-center">
                            <button className="btn btn-danger">Loading... </button>
                        </div>
                    </div>
                );
            }
            else if ((this.props.getIncomeOS.error !== '') || (this.props.getBankDetail.error !== '') || (this.props.getIncomeHead.error !== '') || (this.props.getCustomer.error !== '')) {
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div className="mt-3 d-flex justify-content-center">
                            <button className="btn btn-danger">Error.. Please Refresh and Try Agin </button>
                        </div>
                    </div>
                );
            }
            else {
                let business = this.props.businessList.data.filter(b => b.businessid === this.props.businessid)[0];
                let fy = business.financial.filter(f => f.fyid === this.props.fyid)[0].financialyear;
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div style={{ minHeight: "93vh", display: "flex", flexDirection: "column" }}>
                            <div className="container" style={{ flex: "1" }}>
                                <h6 className="text-center pl-2 pr-2 pt-4" style={{ lineHeight: "1" }}><strong>{business.businessname}- {fy}</strong></h6>
                                <h6 className="text-center pl-2 pr-2 mb-4" style={{ lineHeight: "1" }}>{business.alias ? "(" + business.alias + ")" : ''}</h6>
                                <h6 className="text-center pl-2 pr-2"><strong><u>Income From Other Sources Entry</u></strong></h6>
                                <div className="row" >
                                    <LocalForm className="small" onSubmit={this.handleSubmit}>
                                        <div className="container mt-4 d-flex justify-content-center">
                                            <div class="row col-12 col-md-4">
                                                <div className="form-group mb-0 col-6">
                                                    <label><input type="checkbox" name="cash" id="cash" checked={this.state.cash} onChange={this.handleInputChange} /> <strong>Income To Cash</strong></label>
                                                </div>
                                                <div className="form-group mb-1 col-6">
                                                    <label><input type="checkbox" name="tds" id="tds" checked={this.state.tds} onChange={this.handleInputChange} /> <strong>Tds Deducted</strong></label>
                                                </div>
                                                {business.businesstype === 'partnership' ? <div className="form-group mb-1 col-6 pr-0">
                                                    <label><input type="checkbox" name="partner" id="partner" checked={this.state.partner} onChange={this.handleInputChange} /> <strong>Income By Partner</strong></label>
                                                </div> : ''}
                                                <div className="form-group mb-1 col-12">
                                                    <label htmlFor="category" className="mb-0 muted-text"><strong>Select Income Head:</strong><span className="text-danger">*</span></label>
                                                    <Control.select model=".category" className="form-control form-control-sm" id="category" name="category" onChange={this.handleInputChange} value={this.state.category}>
                                                        <option value="">Select Income Head</option>
                                                        {this.props.getIncomeHead.data.map(d => {
                                                            return <option value={d.incomeheadname}>{d.incomeheadname}</option>
                                                        })}
                                                    </Control.select>
                                                </div>
                                                {this.state.cash === false && this.state.partner === false ? <div className="form-group mb-1 col-12">
                                                    <label htmlFor="customerid" className="mb-0 muted-text"><strong>Select Customer Account:</strong><span className="text-danger">*</span></label>
                                                    <Control.select model=".customerid" className="form-control form-control-sm" id="customerid" name="customerid" onChange={this.handleInputChange} value={this.state.customerid}>
                                                        <option value="">Select Customer Account</option>
                                                        {this.props.getCustomer.data.map(d => {
                                                            return <option value={d.customerid + "..." + d.businessname}>{d.alias ? d.alias : d.businessname}</option>
                                                        })}
                                                    </Control.select>
                                                </div> : ''}
                                                {this.state.partner === true ? <div className="form-group mb-1 col-12">
                                                    <label htmlFor="customerid" className="mb-0 muted-text"><strong>{business.businesstype === 'partnership' ? 'Select Partner Account' : ''}{business.businesstype === 'ltd' ? 'Select Share Holders Account' : ''}</strong><span className="text-danger">*</span></label>
                                                    <Control.select model=".customerid" className="form-control form-control-sm" id="customerid" name="customerid" onChange={this.handleInputChange} value={this.state.customerid}>
                                                        <option value="">Select Partner Account</option>
                                                        {this.props.getInvestmentAccount.data.map(b => <option value={b.id + "..." + b.investmentaccount}>{b.investmentaccount}</option>)}
                                                    </Control.select>
                                                </div> : ''}
                                                <div className="form-group mb-1 col-12">
                                                    <label htmlFor="voucherno" className="mb-0 muted-text"><strong>Voucher No</strong><span className="text-danger">*</span></label>
                                                    <Control.text model=".voucherno" className="form-control form-control-sm" id="voucherno" name="voucherno" onChange={this.handleInputChange} value={this.state.voucherno} />
                                                </div>
                                                <div className="form-group mb-1 col-12">
                                                    <label htmlFor="date" className="mb-0 muted-text"><strong>Income Date</strong><span className="text-danger">*</span></label>
                                                    <Control.text model=".date" className="form-control form-control-sm" id="date" name="date" onChange={this.handleInputChange} value={this.state.date} />
                                                </div>
                                                <div className="form-group mb-1 col-12">
                                                    <label htmlFor="description" className="mb-0 muted-text"><strong>Remark</strong></label>
                                                    <Control.text model=".description" className="form-control form-control-sm" id="description" name="description" onChange={this.handleInputChange} value={this.state.description} />
                                                </div>
                                                <div className="form-group mb-1 col-12">
                                                    <label htmlFor="amount" className="mb-0 muted-text"><strong>Income Amount</strong><span className="text-danger">*</span></label>
                                                    <Control.text model=".amount" className="form-control form-control-sm" id="amount" name="amount" onChange={this.handleInputChange} value={this.state.amount} />
                                                </div>
                                                {this.state.tds ? <div className="form-group mb-1 col-12 pt-2 pb-2" style={{ backgroundColor: "antiquewhite" }}>
                                                    <label htmlFor="tdsamount" className="mb-0 muted-text"><strong>TDS Deduction Amount</strong><span className="text-danger">*</span></label>
                                                    <Control.text model=".tdsamount" className="form-control form-control-sm" id="tdsamount" name="tdsamount" onChange={this.handleInputChange} value={this.state.tdsamount} />
                                                </div> : ''}
                                                <div className="form-group mb-1 col-12">
                                                    <button type="submit" className="btn btn-success mt-1 btn-sm small" >Enter Income</button>
                                                </div>
                                            </div>
                                        </div>
                                    </LocalForm>
                                </div>
                            </div>
                            <Footer />
                        </div>
                    </div>
                )
            }
        }
    }
}

export default IncomeOS;