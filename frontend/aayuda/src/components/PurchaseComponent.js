import React, { Component } from 'react';
import cookie from 'react-cookie';
import { Redirect } from 'react-router-dom';
import { Button } from 'reactstrap';
import { LocalForm, Control } from 'react-redux-form';
import { Header } from './HeaderComponent';
import { Footer } from './Footer';

const Option = function ({ list, scope }) {
    let length = list.length;
    let arr = [];
    if (scope === 'item') {
        for (let i = -1; i < length; i++) {
            if (i === -1) arr.push(<option value=''> Select Item</option>);
            else arr.push(<option value={JSON.stringify(list[i])}>{list[i].alias ? list[i].alias : list[i].itemname}</option>);
        }
    }
    else if (scope === 'asset') {
        for (let i = -1; i < length; i++) {
            if (i === -1) arr.push(<option value=''>Select Asset:</option>);
            else { (!list[i].solddate) ? arr.push(<option value={list[i].id}>{list[i].assetaccount}</option>) : arr.push('') };
        }
    }
    else {
        for (let i = -1; i < length; i++) {
            if (i === -1) arr.push(<option value=''>Select Vendor Account :</option>);
            else arr.push(<option value={list[i].vendorid + "..." + list[i].businessname}>{list[i].alias ? list[i].alias : list[i].businessname}</option>);
        }
    }
    return arr;
}

const RenderItemComponent = function ({ getItem, getVendor, businessid, businessList, itemno, handleInputChange, handleOnBlur, handleOnBlurMiniData, state }) {
    let itemRows = [];
    for (let x = 0; x <= state.itemno; x++) {
        let n = x;
        let billingunit = '';
        let taxrow = [];
        if ((state['itemname' + n] !== '') && (state.vendorid !== '')) {
            let it = JSON.parse(state['itemname' + n]);
            billingunit = it.billingunit;
            taxrow.push(
                <div className="form-group mb-2">
                    <div className="d-flex">
                        <label htmlFor={'basic' + n} className="mb-0 text col-7 p-1"><strong>{"Basic "}</strong><span className="text-danger">*</span></label>
                        <Control.text model={".basic" + n} className="form-control form-control-sm col-5" id={"basic" + n} name={"basic" + n} onChange={handleInputChange} value={state["basic" + n]} onBlur={handleOnBlurMiniData} />
                    </div>
                </div>
            )
            if (it.taxtype === 'gst') {
                let vendor = getVendor.data.filter(v => v.vendorid === state.vendorid.split("...")[0])[0];
                let business = businessList.data.filter(b => b.businessid === businessid)[0];
                if ((vendor.gstin.substr(0, 2) === business.gstin.substr(0, 2)) || (vendor.gstin === '')) {
                    taxrow.push(
                        <div>
                            <div className="form-group mb-2">
                                <div className="d-flex">
                                    <label htmlFor={'cgst' + n} className="mb-0 text col-7 p-1"><strong>{"CGST @ " + (it.gstrate / 2)}</strong><span className="text-danger">*</span></label>
                                    <Control.text model={".cgst" + n} className="form-control form-control-sm col-5" id={"cgst" + n} name={"cgst" + n} placeholder={"CGST @ " + (it.gstrate / 2)} onChange={handleInputChange} value={state["cgst" + n]} onBlur={handleOnBlurMiniData} />
                                </div>
                            </div>
                            <div className="form-group mb-2">
                                <div className="d-flex">
                                    <label htmlFor={'sgst' + n} className="mb-0 text col-7 p-1"><strong>{"SGST @ " + (it.gstrate / 2)}</strong><span className="text-danger">*</span></label>
                                    <Control.text model={".sgst" + n} className="form-control form-control-sm col-5" id={"sgst" + n} name={"sgst" + n} placeholder={"SGST @ " + (it.gstrate / 2)} onChange={handleInputChange} value={state["sgst" + n]} onBlur={handleOnBlurMiniData} />
                                </div>
                            </div>
                        </div>
                    )
                }
                else {
                    taxrow.push(
                        <div className="form-group mb-2">
                            <div className="d-flex">
                                <label htmlFor={'igst' + n} className="mb-0 text col-7 p-1"><strong>{"IGST @ " + it.gstrate}</strong><span className="text-danger">*</span></label>
                                <Control.text model={".igst" + n} className="form-control form-control-sm col-5" id={"igst" + n} name={"igst" + n} onChange={handleInputChange} value={state["igst" + n]} onBlur={handleOnBlurMiniData} />
                            </div>
                        </div>
                    )
                }
            }
            taxrow.push(
                <div className="form-group mb-2">
                    <div className="d-flex">
                        <label htmlFor={'total' + n} className="mb-0 text col-7 p-1"><strong>Total</strong><span className="text-danger">*</span></label>
                        <Control.text model={".total" + n} className="form-control form-control-sm col-5" id={"total" + n} name={"total" + n} placeholder='' onChange={handleInputChange} value={state["total" + n]} onBlur={handleOnBlurMiniData} />
                    </div>
                </div>
            )
        }
        itemRows.push(
            <div className="form-group col-12 col-md-3 mb-1 mt-2 pl-0">
                <div className="border border-danger p-1 ml-0">
                    <label htmlFor={"itemname" + n} className="mb-0 muted-text">({parseFloat(x) + 1}) Item Details</label>
                    <Control.select model={".itemname" + n} className="form-control form-control-sm mb-1" id={"itemname" + n} name={"itemname" + n} onChange={handleInputChange} value={state['itemname' + n]}>
                        <Option list={getItem.data} scope="item" />
                    </Control.select>
                    <div className="form-group mb-1">
                        <div className="d-flex">
                            <label htmlFor={'qty' + n} className="mb-0 text col-7 p-1"><strong>{'Qty In ' + billingunit}</strong><span className="text-danger">*</span></label>
                            <Control.text model={'.qty' + n} className="form-control form-control-sm col-5" id={'qty' + n} name={'qty' + n} onChange={handleInputChange} value={state['qty' + n]} onBlur={handleOnBlur} />
                        </div>
                    </div>
                    <div className="form-group mb-1">
                        <div className="d-flex">
                            <label htmlFor={'price' + n} className="mb-0 text col-7 p-1"><strong>{'Price per ' + billingunit}</strong><span className="text-danger">*</span></label>
                            <Control.text model={'.price' + n} className="form-control form-control-sm col-5 " id={'price' + n} name={'price' + n} onChange={handleInputChange} value={state['price' + n]} onBlur={handleOnBlur} />
                        </div>
                    </div>
                    {taxrow}
                </div>
            </div>
        );
    }
    return itemRows;
}

class Purchase extends Component {
    constructor(props) {
        super(props);
        window.onload = () => {
            this.props.refreshVendorState();
            this.props.refreshItemState();
            this.props.resetPurchaseDetail();
            this.props.refreshAssetAccountState();
            this.props.refreshBankState();
        }
        this.state = ({
            itemno: 0,
            vendorid: '',
            invoicedate: new Date().toDateString().split(' ').slice(1).join(' '),
            invoicenumber: '',
            paymentduedate: '',
            itemname0: '',
            qty0: '',
            price0: '',
            basic0: '',
            gst0: '',
            excise0: '',
            vat0: '',
            total0: '',
            itemname1: '',
            qty1: '',
            price1: '',
            basic1: '',
            gst1: '',
            excise1: '',
            vat1: '',
            total1: '',
            itemname2: '',
            qty2: '',
            price2: '',
            basic2: '',
            gst2: '',
            excise2: '',
            vat2: '',
            total2: '',
            itemname3: '',
            qty3: '',
            price3: '',
            basic3: '',
            gst3: '',
            excise3: '',
            vat3: '',
            total3: '',
            itemname4: '',
            qty4: '',
            price4: '',
            basic4: '',
            gst4: '',
            excise4: '',
            vat4: '',
            total4: '',
            itemname5: '',
            qty5: '',
            price5: '',
            basic5: '',
            gst5: '',
            excise5: '',
            vat5: '',
            total5: '',
            itemname6: '',
            qty6: '',
            price6: '',
            basic6: '',
            gst6: '',
            excise6: '',
            vat6: '',
            total6: '',
            itemname7: '',
            qty7: '',
            price7: '',
            basic7: '',
            gst7: '',
            excise7: '',
            vat7: '',
            total7: '',
            itemname8: '',
            qty8: '',
            price8: '',
            basic8: '',
            gst8: '',
            excise8: '',
            vat8: '',
            total8: '',
            itemname9: '',
            qty9: '',
            price9: '',
            basic9: '',
            gst9: '',
            excise9: '',
            vat9: '',
            total9: '',
            totalamountbasic: 0,
            totaligst: 0,
            totalcgst: 0,
            totalsgst: 0,
            totalvat: 0,
            totalexcise: 0,
            importduty: 0,
            freight: 0,
            grandtotal: 0,
            asset: false,
            assetid: ''
        })
    }

    componentDidMount() {
        if (this.props.addPurchaseMessage.message === 'success') {
            alert("Purchase Invoice registered Successfully");
            this.props.resetaddPurchaseMessage('success');
        }
        else if (this.props.addPurchaseMessage.message === 'Already Registered') {
            alert("Purchase Invoice from " + this.props.purchaseDetail.vendorname + ", Invoice No: " + this.props.purchaseDetail.invoicenumber + " Already Registered");
            this.props.resetaddPurchaseMessage('success');
        }
        if ((this.props.purchaseDetail.message === 'initial') && (this.props.purchaseDetail.loading === false)) this.props.getPurchaseData(this.props.businessid, this.props.fyid);
        if (this.props.getVendor.message === 'initial') this.props.getVendorList(this.props.businessid);
        if (this.props.getItem.message === 'initial') this.props.getItemList(this.props.businessid);
        if (this.props.getAssetAccount.message === 'initial') this.props.getAssetAccountList(this.props.businessid);
        if (this.props.getBankDetail.message === 'initial') this.props.getBankDetailList(this.props.businessid);
        if (this.props.addPurchaseMessage.error !== '') {
            alert(this.props.addPurchaseMessage.error);
            this.setState({
                ...this.props.addPurchaseMessage.state
            })
            this.props.resetaddPurchaseMessage('error');
        }
        if (this.props.addAssetMessage.error !== '') {
            alert(this.props.addAssetMessage.error);
            this.setState({
                ...this.props.addAssetMessage.state
            })
            this.props.resetaddAssetMessage();
        }
        if (this.props.addAssetMessage.message === 'success') {
            alert("Asset Added Successfully");
            this.props.resetaddAssetMessage();
        }
        if (this.props.logoutMessage.error !== '') {
            alert("Error Logging Out. Please Try Again");
            this.props.resetLogoutMessage();
        }
    }

    addPurchaseItem = () => {
        let c = this.state.itemno + 1;
        this.setState({
            ...this.state,
            itemno: c
        })
    }

    handleInputChange = (event) => {
        let target = event.target;
        let name = target.name;
        let value = target.value;
        let index = name.charAt(name.length - 1);
        if (name === 'asset') {
            this.setState({
                asset: !this.state.asset,
                itemno: 0,
            vendorid: '',
            invoicedate: new Date().toDateString().split(' ').slice(1).join(' '),
            invoicenumber: '',
            paymentduedate: '',
            itemname0: '',
            qty0: '',
            price0: '',
            basic0: '',
            gst0: '',
            excise0: '',
            vat0: '',
            total0: '',
            itemname1: '',
            qty1: '',
            price1: '',
            basic1: '',
            gst1: '',
            excise1: '',
            vat1: '',
            total1: '',
            itemname2: '',
            qty2: '',
            price2: '',
            basic2: '',
            gst2: '',
            excise2: '',
            vat2: '',
            total2: '',
            itemname3: '',
            qty3: '',
            price3: '',
            basic3: '',
            gst3: '',
            excise3: '',
            vat3: '',
            total3: '',
            itemname4: '',
            qty4: '',
            price4: '',
            basic4: '',
            gst4: '',
            excise4: '',
            vat4: '',
            total4: '',
            itemname5: '',
            qty5: '',
            price5: '',
            basic5: '',
            gst5: '',
            excise5: '',
            vat5: '',
            total5: '',
            itemname6: '',
            qty6: '',
            price6: '',
            basic6: '',
            gst6: '',
            excise6: '',
            vat6: '',
            total6: '',
            itemname7: '',
            qty7: '',
            price7: '',
            basic7: '',
            gst7: '',
            excise7: '',
            vat7: '',
            total7: '',
            itemname8: '',
            qty8: '',
            price8: '',
            basic8: '',
            gst8: '',
            excise8: '',
            vat8: '',
            total8: '',
            itemname9: '',
            qty9: '',
            price9: '',
            basic9: '',
            gst9: '',
            excise9: '',
            vat9: '',
            total9: '',
            totalamountbasic: 0,
            totaligst: 0,
            totalcgst: 0,
            totalsgst: 0,
            totalvat: 0,
            totalexcise: 0,
            importduty: 0,
            freight: 0,
            grandtotal: 0,
            assetid: ''
            })
        }
        else if (/^price/i.test(name)) {
            if ((this.state['itemname' + index] === '') || (this.state['qty' + index] === '') || (this.state.vendorid === '')) alert("Please select Vendor, Item  and fill in Quantity before filling in price");
            else {
                this.setState({
                    ...this.state,
                    [name]: value
                })
            }
        }
        else if (/^vendorid/i.test(name)) {
            this.setState({
                itemno: 0,
                vendorid: value,
                invoicedate: new Date().toDateString().split(' ').slice(1).join(' '),
                invoicenumber: '',
                paymentduedate: '',
                itemname0: '',
                qty0: '',
                price0: '',
                basic0: '',
                igst0: '',
                cgst0: '',
                sgst0: '',
                excise0: '',
                vat0: '',
                total0: '',
                itemname1: '',
                qty1: '',
                price1: '',
                basic1: '',
                igst1: '',
                cgst1: '',
                sgst1: '',
                excise1: '',
                vat1: '',
                total1: '',
                itemname2: '',
                qty2: '',
                price2: '',
                basic2: '',
                igst2: '',
                cgst2: '',
                sgst2: '',
                excise2: '',
                vat2: '',
                total2: '',
                itemname3: '',
                qty3: '',
                price3: '',
                basic3: '',
                igst3: '',
                cgst3: '',
                sgst3: '',
                excise3: '',
                vat3: '',
                total3: '',
                itemname4: '',
                qty4: '',
                price4: '',
                basic4: '',
                igst4: '',
                cgst4: '',
                sgst4: '',
                excise4: '',
                vat4: '',
                total4: '',
                itemname5: '',
                qty5: '',
                price5: '',
                basic5: '',
                igst5: '',
                cgst5: '',
                sgst5: '',
                excise5: '',
                vat5: '',
                total5: '',
                itemname6: '',
                qty6: '',
                price6: '',
                basic6: '',
                igst6: '',
                cgst6: '',
                sgst6: '',
                excise6: '',
                vat6: '',
                total6: '',
                itemname7: '',
                qty7: '',
                price7: '',
                basic7: '',
                igst7: '',
                cgst7: '',
                sgst7: '',
                excise7: '',
                vat7: '',
                total7: '',
                itemname8: '',
                qty8: '',
                price8: '',
                basic8: '',
                igst8: '',
                cgst8: '',
                sgst8: '',
                excise8: '',
                vat8: '',
                total8: '',
                itemname9: '',
                qty9: '',
                price9: '',
                basic9: '',
                igst9: '',
                cgst9: '',
                sgst9: '',
                excise9: '',
                vat9: '',
                total9: '',
                totalamountbasic: 0,
                totaligst: 0,
                totalcgst: 0,
                totalsgst: 0,
                totalvat: 0,
                totalexcise: 0,
                importduty: 0,
                freight: 0,
                grandtotal: 0,
                asset: this.state.asset,
                assetid: ''
            })
        }
        else if (/^itemname/i.test(name)) {
            let totalbasic = 0, totaltax = 0, grandtotal = 0, totalvat = 0, totalexcise = 0;
            for (let i = 0; i < 10; i++) {
                if (parseFloat(index) !== i) {
                    if (this.state['basic' + i] !== '') totalbasic = totalbasic + parseFloat(this.state['basic' + i]);
                    if (this.state['gst' + i] !== '') totaltax = totaltax + parseFloat(this.state['gst' + i]);
                    if (this.state['total' + i] !== '') grandtotal = grandtotal + parseFloat(this.state['total' + i]);
                    if (this.state['vat' + i] !== '') totalvat = totalvat + parseFloat(this.state['vat' + i]);
                    if (this.state['excise' + i] !== '') totalexcise = totalexcise + parseFloat(this.state['excise' + i]);
                }
            }
            this.setState({
                ...this.state,
                [name]: value,
                ['qty' + index]: '',
                ['price' + index]: '',
                ['basic' + index]: '',
                ['gst' + index]: '',
                ['total' + index]: '',
                ['totalamountbasic']: parseFloat(totalbasic).toFixed(2),
                ['totaligst']: this.state.totaligst > 0 ? parseFloat(totaltax).toFixed(2) : 0,
                ['totalcgst']: this.state.totalcgst > 0 ? parseFloat(totaltax / 2).toFixed(2) : 0,
                ['totalsgst']: this.state.totalsgst > 0 ? parseFloat(totaltax / 2).toFixed(2) : 0,
                ['totalvat']: parseFloat(totalvat).toFixed(2),
                ['totalexcise']: parseFloat(totalexcise).toFixed(2),
                ['grandtotal']: parseFloat(parseFloat(grandtotal) + parseFloat(this.state.freight)).toFixed(2)
            })
        }
        else {
            this.setState({
                ...this.state,
                [name]: value
            })
        }
    }

    handleOnBlur = (event) => {
        let target = event.target;
        let name = target.name;
        let value = target.value;
        if (value > 0) value = parseFloat(value).toFixed(2);
        let index = parseFloat(name.charAt(name.length - 1));
        if ((/^price/i.test(name)) || (/^qty/i.test(name))) {
            if ((this.state['price' + index] === '') || (this.state['qty' + index] === '')) {
                let totalbasic = 0, totaligst = 0, totalsgst = 0, totalcgst = 0, grandtotal = 0, totalvat = 0, totalexcise = 0;
                for (let i = 0; i < 10; i++) {
                    if (index !== i) {
                        if (this.state['basic' + i] !== '') totalbasic = totalbasic + parseFloat(this.state['basic' + i]);
                        if (this.state['igst' + i] !== '') totaligst = totaligst + parseFloat(this.state['igst' + i]);
                        if (this.state['sgst' + i] !== '') totalsgst = totalsgst + parseFloat(this.state['sgst' + i]);
                        if (this.state['cgst' + i] !== '') totalcgst = totalcgst + parseFloat(this.state['cgst' + i]);
                        if (this.state['total' + i] !== '') grandtotal = grandtotal + parseFloat(this.state['total' + i]);
                        if (this.state['vat' + i] !== '') totalvat = totalvat + parseFloat(this.state['vat' + i]);
                        if (this.state['excise' + i] !== '') totalexcise = totalexcise + parseFloat(this.state['excise' + i]);
                    }
                }
                this.setState({
                    ...this.state,
                    ['basic' + index]: '',
                    ['igst' + index]: '',
                    ['cgst' + index]: '',
                    ['igst' + index]: '',
                    ['vat' + index]: '',
                    ['excise' + index]: '',
                    ['total' + index]: '',
                    ['totalamountbasic']: parseFloat(totalbasic).toFixed(2),
                    ['totaligst']: parseFloat(totaligst).toFixed(2),
                    ['totalcgst']: parseFloat(totalcgst).toFixed(2),
                    ['totalsgst']: parseFloat(totalsgst).toFixed(2),
                    ['totalvat']: parseFloat(totalvat).toFixed(2),
                    ['totalexcise']: parseFloat(totalexcise).toFixed(2),
                    ['grandtotal']: parseFloat(grandtotal + parseFloat(this.state.freight)).toFixed(2)
                })
            }
        }
        if ((this.state['qty' + index] !== '') && (this.state['itemname' + index] !== '') && (this.state['price' + index] !== '') && (this.state.vendorid !== "")) {
            let totalbasic = 0;
            let it = JSON.parse(this.state['itemname' + index]);
            if (it.taxtype === 'gst') {
                let totalbasic = 0, grandtotal = 0, totaligst = 0, totalcgst = 0, totalsgst = 0;
                let vendor = this.props.getVendor.data.filter(v => v.vendorid === this.state.vendorid.split("...")[0])[0];
                let business = this.props.businessList.data.filter(b => b.businessid === this.props.businessid)[0];
                for (let i = 0; i <= 9; i++) {
                    if (i !== index) {
                        if (this.state['basic' + i] !== '') totalbasic = totalbasic + parseFloat(this.state['basic' + i]);
                        if (this.state['igst' + i] !== '') totaligst = totaligst + parseFloat(this.state['igst' + i]);
                        if (this.state['sgst' + i] !== '') totalsgst = totalsgst + parseFloat(this.state['sgst' + i]);
                        if (this.state['cgst' + i] !== '') totalcgst = totalcgst + parseFloat(this.state['cgst' + i]);
                        if (this.state['total' + i] !== '') grandtotal = grandtotal + parseFloat(this.state['total' + i]);
                    }
                }
                this.setState({
                    ...this.state,
                    ['basic' + index]: (parseFloat(this.state['qty' + index]) * parseFloat(this.state['price' + index])).toFixed(2),
                    ['igst' + index]: (vendor.gstin.substr(0, 2) !== '' && vendor.gstin.substr(0, 2) !== business.gstin.substr(0, 2)) ? (parseFloat(this.state['qty' + index]) * parseFloat(this.state['price' + index]) * (parseFloat(it.gstrate) / 100)).toFixed(2) : '',
                    ['sgst' + index]: ((vendor.gstin.substr(0, 2) === business.gstin.substr(0, 2)) || (vendor.gstin === '')) ? (parseFloat(this.state['qty' + index]) * parseFloat(this.state['price' + index]) * (parseFloat(it.gstrate) / 200)).toFixed(2) : '',
                    ['cgst' + index]: ((vendor.gstin.substr(0, 2) === business.gstin.substr(0, 2)) || (vendor.gstin === '')) ? (parseFloat(this.state['qty' + index]) * parseFloat(this.state['price' + index]) * (parseFloat(it.gstrate) / 200)).toFixed(2) : '',
                    ['total' + index]: ((parseFloat(this.state['qty' + index]) * parseFloat(this.state['price' + index])) + (parseFloat(this.state['qty' + index]) * parseFloat(this.state['price' + index]) * (parseFloat(it.gstrate) / 100))).toFixed(2),
                    ['totalamountbasic']: (parseFloat(this.state['qty' + index]) * parseFloat(this.state['price' + index]) + parseFloat(totalbasic)).toFixed(2),
                    ['totaligst']: (vendor.gstin.substr(0, 2) !== '' && vendor.gstin.substr(0, 2) !== vendor.gstin.substr(0, 2)) ? ((parseFloat(this.state['qty' + index]) * parseFloat(this.state['price' + index]) * (parseFloat(it.gstrate) / 100)) + totaligst).toFixed(2) : parseFloat(totaligst).toFixed(2),
                    ['totalcgst']: ((vendor.gstin.substr(0, 2) === business.gstin.substr(0, 2)) || (vendor.gstin === '')) ? ((parseFloat(this.state['qty' + index]) * parseFloat(this.state['price' + index]) * (parseFloat(it.gstrate) / 200)) + totalcgst).toFixed(2) : parseFloat(totalcgst).toFixed(2),
                    ['totalsgst']: ((vendor.gstin.substr(0, 2) === business.gstin.substr(0, 2)) || (vendor.gstin === '')) ? ((parseFloat(this.state['qty' + index]) * parseFloat(this.state['price' + index]) * (parseFloat(it.gstrate) / 200)) + totalsgst).toFixed(2) : parseFloat(totalsgst).toFixed(2),
                    ['grandtotal']: (grandtotal + (parseFloat(this.state['qty' + index]) * parseFloat(this.state['price' + index])) + (parseFloat(this.state['qty' + index]) * parseFloat(this.state['price' + index]) * parseFloat(it.gstrate) / 100) + parseFloat(this.state.freight)).toFixed(2)
                })
            }
            else {
                let totalexcise = 0;
                let totalvat = 0;
                let grandtotal = 0;
                for (let i = 0; i <= 9; i++) {
                    if (i !== index) {
                        if (this.state['basic' + i] !== '') totalbasic = totalbasic + parseFloat(this.state['basic' + i]);
                        if (this.state['vat' + i] !== '') totalvat = totalvat + parseFloat(this.state['vat' + i]);
                        if (this.state['excise' + i] !== '') totalexcise = totalexcise + parseFloat(this.state['excise' + i]);
                        if (this.state['total' + i] !== '') grandtotal = grandtotal + parseFloat(this.state['total' + i]);

                    }
                }
                this.setState
                    ({
                        ...this.state,
                        ['basic' + index]: parseFloat(this.state['qty' + index] * this.state['price' + index]).toFixed(2),
                        ['vat' + index]: parseFloat(this.state['qty' + index] * this.state['price' + index] * (it.vatrate / 100)).toFixed(2),
                        ['excise' + index]: parseFloat(this.state['qty' + index] * this.state['price' + index] * (it.exciserate / 100)).toFixed(2),
                        ['total' + index]: parseFloat(parseFloat(this.state['qty' + index] * this.state['price' + index]) + parseFloat(this.state['qty' + index] * this.state['price' + index] * (it.vatrate / 100)) + parseFloat(this.state['qty' + index] * this.state['price' + index] * (it.exciserate / 100))).toFixed(2),
                        ['totalamountbasic']: parseFloat((this.state['qty' + index] * this.state['price' + index]) + totalbasic).toFixed(2),
                        ['totalvat']: parseFloat((this.state['qty' + index] * this.state['price' + index] * (it.vatrate / 100)) + totalvat).toFixed(2),
                        ['totalexcise']: parseFloat((this.state['qty' + index] * this.state['price' + index] * (it.exciserate / 100)) + totalexcise).toFixed(2),
                        ['grandtotal']: parseFloat(grandtotal + ((this.state['qty' + index] * this.state['price' + index]) + (this.state['qty' + index] * this.state['price' + index] * (it.vatrate / 100)) + (this.state['qty' + index] * this.state['price' + index] * (it.exciserate / 100))) + this.state.freight).toFixed(2)
                    })
            }
        }
    }
    handleSubmit = () => {
        let business = this.props.businessList.data.filter(b => b.businessid === this.props.businessid)[0];
        let fy = business.financial.filter(f => f.fyid === this.props.fyid)[0].financialyear;
        if (this.state.vendorid === '') alert("Please Select Vendor");
        else if (!(/^[a-zA-Z]{3} [0-9]{2} [0-9]{4}$/i.test(this.state.invoicedate))) alert("Date Must Be In (MON DD YYYY) Format");
        else if ((this.state.invoicenumber.length > 14) || (this.state.invoicenumber === '')) alert("Voucher Number is Mandatory & Must Be Less Than 15 Characters  ");
        else if ((new Date(this.state.invoicedate) < new Date("01 Apr " + fy.split("-")[0])) || (new Date(this.state.invoicedate) > new Date("31 Mar " + fy.split("-")[1]))) alert("Invalid Date. Date Must Be Within FY " + fy);
        else if (isNaN(parseInt(this.state.totalamountbasic)) || (this.state.totalamountbasic <= 0)) alert("Amount Basic Must Be Greater Than 0");
        else if (isNaN(parseInt(this.state.totaligst)) || (this.state.totaligst < 0)) alert("Total IGST Must Be Greater Than or Equal To 0");
        else if (isNaN(parseInt(this.state.totalcgst)) || (this.state.totalcgst < 0)) alert("Total CGST Must Be Greater Than or Equal To 0");
        else if (isNaN(parseInt(this.state.totalsgst)) || (this.state.totalsgst < 0)) alert("Total SGST Must Be Greater Than or Equal To 0");
        else if (isNaN(parseInt(this.state.grandtotal)) || (this.state.grandtotal <= 0)) alert("Grand Total Must Be Greater Than 0");
        else if (this.state.asset && !this.state.assetid === '') alert("Please Select Asset Account");
        else {
            this.state.asset === false ? this.props.raisePurchase(this.props.businessid, this.props.fyid, this.state, this.props.getVendor) : this.props.raiseAsset(this.props.businessid, this.props.fyid, this.state);
        }
    }
    handleOnBlurMiniData = (event) => {
        let target = event.target;
        let name = target.name;
        let value = target.value;
        if (value === '') value = 0;
        value = parseFloat(value);
        let index = parseFloat(name.charAt(name.length - 1));
        let n = index;
        let item = this.state['itemname' + index] !== '' ? JSON.parse(this.state['itemname' + index]) : '';
        if ((item !== '') && (this.state.vendorid !== '')) {
            let totalbasic = 0, totaligst = 0, totalcgst = 0, totalsgst = 0, totalvat = 0, totalexcise = 0, grandtotal = 0;
            let vendor = this.props.getVendor.data.filter(b => b.vendorid === this.state.vendorid.split("...")[0])[0];
            let business = this.props.businessList.data.filter(b => b.businessid === this.props.businessid)[0];
            for (let i = 0; i < 10; i++) {
                if (i !== index) {
                    totalbasic = totalbasic + (this.state['basic' + i] !== '' ? parseFloat(this.state['basic' + i]) : 0);
                    totaligst = totaligst + (this.state['igst' + i] !== '' ? parseFloat(this.state['igst' + i]) : 0);
                    totalcgst = totalcgst + (this.state['cgst' + i] !== '' ? parseFloat(this.state['cgst' + i]) : 0);
                    totalsgst = totalsgst + (this.state['sgst' + i] !== '' ? parseFloat(this.state['sgst' + i]) : 0);
                    totalvat = totalvat + (this.state['vat' + i] !== '' ? parseFloat(this.state['vat' + i]) : 0);
                    totalexcise = totalexcise + (this.state['excise' + i] !== '' ? parseFloat(this.state['excise' + i]) : 0);
                    grandtotal = grandtotal + (this.state['total' + i] !== '' ? parseFloat(this.state['total' + i]) : 0);
                }
            }
            if (/^basic/i.test(name)) {
                if (item.taxtype === 'gst') {
                    if ((vendor.gstin.substr(0, 2) === business.gstin.substr(0, 2)) || (vendor.gstin === '')) {
                        this.setState({
                            ...this.state,
                            ['igst' + n]: '',
                            ['sgst' + n]: value === 0 ? '' : parseFloat(value * (item.gstrate / 200)).toFixed(2),
                            ['cgst' + n]: value === 0 ? '' : parseFloat(value * (item.gstrate / 200)).toFixed(2),
                            ['total' + n]: value === 0 ? '' : parseFloat(value + (value * item.gstrate / 100)).toFixed(2),
                            ['totalamountbasic']: parseFloat(totalbasic + value).toFixed(2),
                            ['totalcgst']: parseFloat(totalcgst + (value * item.gstrate / 200)).toFixed(2),
                            ['totalsgst']: parseFloat(totalsgst / 2 + (value * item.gstrate / 200)).toFixed(2),
                            ['grandtotal']: parseFloat(grandtotal + value + (value * item.gstrate / 100) + this.state.freight).toFixed(2)
                        })
                    }
                    else {
                        this.setState({
                            ...this.state,
                            ['igst' + n]: value === 0 ? '' : parseFloat(value * (item.gstrate / 100)).toFixed(2),
                            ['cgst' + n]: '',
                            ['sgst' + n]: '',
                            ['total' + n]: value === 0 ? '' : parseFloat(value + (value * item.gstrate / 100)).toFixed(2),
                            ['totalamountbasic']: parseFloat(totalbasic + value).toFixed(2),
                            ['totaligst']: parseFloat(totaligst + (value * item.gstrate / 100)).toFixed(2),
                            ['grandtotal']: parseFloat(grandtotal + value + (value * item.gstrate / 100) + this.state.freight).toFixed(2)
                        })
                    }
                }
                else {
                    this.setState({
                        ...this.state,
                        ['vat' + n]: value === 0 ? '' : parseFloat(value * (item.vatrate / 100)).toFixed(2),
                        ['excise' + n]: value === 0 ? '' : parseFloat(value * (item.exciserate / 100)).toFixed(2),
                        ['total' + n]: value === 0 ? '' : parseFloat(value + (value * item.vatrate / 100) + (value * item.exciserate / 100)).toFixed(2),
                        ['totalamountbasic']: parseFloat(totalbasic + value).toFixed(2),
                        ['totalvat']: parseFloat(totalvat + (value * item.vatrate / 100)).toFixed(2),
                        ['totalexcise']: parseFloat(totalexcise + (value * item.exciserate / 100)).toFixed(2),
                        ['grandtotal']: parseFloat(totalbasic + totalvat + totalexcise + value + (value * item.vatrate / 100) + (value * item.exciserate / 100)).toFixed(2)
                    })
                }
            }
            else if (/^total/i.test(name)) {
                let total = 0;
                for (let i = 0; i < 10; i++) {
                    if (i !== index) total = total + (this.state['total' + i] !== '' ? parseFloat(this.state['total' + i]) : 0);
                }
                this.setState({
                    ...this.state,
                    ['grandtotal']: parseFloat(total + parseFloat(value) + parseFloat(this.state.freight)).toFixed(2)
                })
            }
            else if (/^cgst/i.test(name)) {
                let b = this.state['basic' + n] === '' ? 0 : this.state['basic' + n];
                let x = this.state['sgst' + n] === '' ? 0 : this.state['sgst' + n];
                this.setState({
                    ...this.state,
                    ['total' + n]: parseFloat(parseFloat(value) + parseFloat(b) + parseFloat(x)).toFixed(2),
                    ['totalcgst']: parseFloat(totalcgst + parseFloat(value)).toFixed(2),
                    ['grandtotal']: parseFloat(grandtotal + parseFloat(value) + parseFloat(this.state['sgst' + n]) + parseFloat(b) + parseFloat(this.state.freight)).toFixed(2)
                })
            }
            else if (/^sgst/i.test(name)) {
                let b = this.state['basic' + n] === '' ? 0 : this.state['basic' + n];
                let x = this.state['cgst' + n] === '' ? 0 : this.state['cgst' + n];
                this.setState({
                    ...this.state,
                    ['total' + n]: parseFloat(parseFloat(value) + parseFloat(b) + parseFloat(x)).toFixed(2),
                    ['totalsgst']: parseFloat(totalsgst + parseFloat(value)).toFixed(2),
                    ['grandtotal']: parseFloat(grandtotal + parseFloat(value) + parseFloat(this.state['cgst' + n]) + parseFloat(b) + parseFloat(this.state.freight)).toFixed(2)
                })
            }
            else if (/^igst/i.test(name)) {
                let b = this.state['basic' + n] === '' ? 0 : this.state['basic' + n];
                this.setState({
                    ...this.state,
                    ['total' + n]: parseFloat(parseFloat(value) + parseFloat(b)).toFixed(2),
                    ['totaligst']: parseFloat(totaligst + parseFloat(value)).toFixed(2),
                    ['grandtotal']: parseFloat(grandtotal + parseFloat(value) + parseFloat(b) + parseFloat(this.state.freight)).toFixed(2)
                })
            }
            else if (/^vat/i.test(name)) {
                let b = this.state['basic' + n] === '' ? 0 : this.state['basic' + n];
                this.setState({
                    ...this.state,
                    ['total' + n]: parseFloat(parseFloat(value) + parseFloat(b)).toFixed(2),
                    ['totalvat']: parseFloat(totalvat + parseFloat(value)).toFixed(2),
                    ['grandtotal']: parseFloat(grandtotal + parseFloat(value) + parseFloat(b) + parseFloat(this.state.freight)).toFixed(2)
                })
            }
            else if (/^excise/i.test(name)) {
                let b = this.state['basic' + n] === '' ? 0 : this.state['basic' + n];
                this.setState({
                    ...this.state,
                    ['total' + n]: parseFloat(parseFloat(value) + parseFloat(b)).toFixed(2),
                    ['totalexcise']: parseFloat(totalexcise + parseFloat(value)).toFixed(2),
                    ['grandtotal']: parseFloat(grandtotal + parseFloat(value) + parseFloat(b) + parseFloat(this.state.freight)).toFixed(2)
                })
            }
            else;
        }
    }

    handleOnBlurMegadata = (event) => {
        let target = event.target;
        let name = target.name;
        let value = target.value;
        if (value === '') value = 0;
        value = parseFloat(value).toFixed(2);
        if (/^totalamountbasic/i.test(name)) {
            this.setState({
                [name]: value,
                ['grandtotal']: (parseFloat(this.state.totaligst) + parseFloat(this.state.totalcgst) + parseFloat(this.state.totalsgst) + parseFloat(this.state.totalvat) + parseFloat(this.state.totalexcise) + parseFloat(this.state.freight) + parseFloat(value)).toFixed(2)
            })
        }
        else if (/^totaligst/i.test(name)) {
            this.setState({
                [name]: value,
                ['grandtotal']: (parseFloat(this.state.totalamountbasic) + parseFloat(this.state.totalcgst) + parseFloat(this.state.totalsgst) + parseFloat(this.state.totalvat) + parseFloat(this.state.totalexcise) + parseFloat(this.state.freight) + parseFloat(value)).toFixed(2)
            })
        }
        else if (/^totalcgst/i.test(name)) {
            this.setState({
                [name]: value,
                ['grandtotal']: (parseFloat(this.state.totalamountbasic) + parseFloat(this.state.totaligst) + parseFloat(this.state.totalsgst) + parseFloat(this.state.totalvat) + parseFloat(this.state.totalexcise) + parseFloat(this.state.freight) + parseFloat(value)).toFixed(2)
            })
        }
        else if (/^totalsgst/i.test(name)) {
            this.setState({
                [name]: value,
                ['grandtotal']: (parseFloat(this.state.totalamountbasic) + parseFloat(this.state.totalcgst) + parseFloat(this.state.totaligst) + parseFloat(this.state.totalvat) + parseFloat(this.state.totalexcise) + parseFloat(this.state.freight) + parseFloat(value)).toFixed(2)
            })
        }
        else if (/^totalvat/i.test(name)) {
            this.setState({
                [name]: value,
                ['grandtotal']: (parseFloat(this.state.totalamountbasic) + parseFloat(this.state.totalcgst) + parseFloat(this.state.totalsgst) + parseFloat(this.state.totaligst) + parseFloat(this.state.totalexcise) + parseFloat(this.state.freight) + parseFloat(value)).toFixed(2)
            })
        }
        else if (/^totalexcise/i.test(name)) {
            this.setState({
                [name]: value,
                ['grandtotal']: (parseFloat(this.state.totalamountbasic) + parseFloat(this.state.totalcgst) + parseFloat(this.state.totalsgst) + parseFloat(this.state.totalvat) + parseFloat(this.state.totaligst) + parseFloat(this.state.freight) + parseFloat(value)).toFixed(2)
            })
        }
        else if (/^freight/i.test(name)) {
            this.setState({
                [name]: value,
                ['grandtotal']: (parseFloat(this.state.totalamountbasic) + parseFloat(this.state.totalcgst) + parseFloat(this.state.totalsgst) + parseFloat(this.state.totalvat) + parseFloat(this.state.totalexcise) + parseFloat(this.state.totaligst) + parseFloat(value)).toFixed(2)
            })
        }
        else;
    }

    render() {
        if (!cookie.load('userservice')) return <Redirect to="/" />;
        else {
            if ((this.props.addPurchaseMessage.loading === true) || (this.props.purchaseDetail.loading === true) || (this.props.getVendor.loading === true) || (this.props.getItem.loading === true) || (this.props.logoutMessage.loading === true) || (this.props.addAssetMessage.loading === true) || (this.props.getAssetAccount.loading === true)) {
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div className="mt-3 d-flex justify-content-center">
                            <button className="btn btn-danger">Loading... </button>
                        </div>
                    </div>
                );
            }
            else if ((this.props.purchaseDetail.error !== '') || (this.props.getVendor.error !== '') || (this.props.getItem.error !== '') || (this.props.getAssetAccount.error !== '')) {
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div className="mt-3 d-flex justify-content-center">
                            <button className="btn btn-danger">Error.. Please Refresh and Try Agin </button>
                        </div>
                    </div>
                );
            }
            else {
                let business = this.props.businessList.data.filter(b => b.businessid === this.props.businessid)[0];
                let fy = business.financial.filter(f => f.fyid === this.props.fyid)[0].financialyear;
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div style={{ minHeight: "93vh", display: "flex", flexDirection: "column" }}>
                            <div className="container" style={{ flex: "1" }}>
                                <h6 className="text-center pl-2 pr-2 pt-4" style={{ lineHeight: "1" }}><strong>{business.businessname} {fy}</strong></h6>
                                <h6 className="text-center pl-2 pr-2 mb-4" style={{ lineHeight: "1" }}>{business.alias ? "(" + business.alias + ")" : ''}</h6>
                                <h6 className="text-center pl-2 pr-2"><strong><u>Purchase Entry</u></strong></h6>
                                <div className="row" >
                                    <LocalForm className="small" onSubmit={this.handleSubmit}>
                                        <div className="container mt-4">
                                            <div class="row">
                                                <div className="col-12 mb-1">
                                                    <input name="asset" type="checkbox" checked={this.state.asset} onChange={this.handleInputChange} />&nbsp;&nbsp;
                                                    <span htmlFor="asset" className="mb-0 muted-text col-8 p-0"><strong>Purchase Asset</strong></span>
                                                </div>
                                                <div className="form-group mb-1 col-12 col-md-3 ">
                                                    <label htmlFor="vendorid" className="mb-0 muted-text"><strong>Select Vendor Account:</strong><span className="text-danger">*</span></label>
                                                    <Control.select model=".vendorid" className="form-control form-control-sm" id="vendorid" name="vendorid" onChange={this.handleInputChange} value={this.state.vendorid}>
                                                        <Option list={this.props.getVendor.data} scope={"vendor"} />
                                                    </Control.select>
                                                </div>
                                                <div className="form-group mb-1 col-6 col-md-3 ">
                                                    <label htmlFor="invoicedate" className="mb-0 muted-text"><strong>Date</strong><span className="text-danger">*</span></label>
                                                    <Control.text model=".invoicedate" className="form-control form-control-sm" id="invoicedate" name="invoicedate" onChange={this.handleInputChange} value={this.state.invoicedate} />
                                                </div>
                                                <div className="form-group mb-1 col-6 col-md-3 ">
                                                    <label htmlFor="invoicenumber" className="mb-0 muted-text"><strong>Voucher Number</strong><span className="text-danger">*</span></label>
                                                    <Control.text model=".invoicenumber" className="form-control form-control-sm" id="invoicenumber" name="invoicenumber" onChange={this.handleInputChange} value={this.state.invoicenumber} />
                                                </div>
                                            </div>
                                            <div className="row col-12 mt-2 ">
                                                {this.state.asset === false ? <RenderItemComponent getItem={this.props.getItem} getVendor={this.props.getVendor} businessid={this.props.businessid} businessList={this.props.businessList} itemno={this.state.itemno} handleInputChange={this.handleInputChange} handleOnBlur={this.handleOnBlur} handleOnBlurMiniData={this.handleOnBlurMiniData} state={this.state} /> : <div className="form-group mb-1 col-12 col-md-3 p-0 ">
                                                    <label htmlFor="assetid" className="mb-0 muted-text"><strong>Select Asset:</strong><span className="text-danger">*</span></label>
                                                    <Control.select model=".assetid" className="form-control form-control-sm" id="assetid" name="assetid" onChange={this.handleInputChange} value={this.state.assetid}>
                                                        <Option list={this.props.getAssetAccount.data} scope={"asset"} />
                                                    </Control.select>
                                                </div>}
                                            </div>
                                            {this.state.asset === false ? <Button disabled={this.props.addPurchaseMessage.disabled} className="btn mt-1 btn-sm" style={{ backgroundColor: "#fff", border: "1px solid #28a745", color: "#28a745" }} onClick={this.addPurchaseItem}>Add More Item </Button> : ''}
                                            <div className="row mt-3">
                                                <div className="form-group mb-1 col-6 col-md-2">
                                                    <label htmlFor="totalamountbasic" className="mb-0 muted-text"><strong>Amount (Basic)</strong></label>
                                                    <Control.text model=".totalamountbasic" className="form-control form-control-sm" id="totalamountbasic" name="totalamountbasic" onChange={this.handleInputChange} value={this.state.totalamountbasic} onBlur={this.handleOnBlurMegadata} />
                                                </div>
                                                <div className="form-group mb-1 col-6 col-md-2">
                                                    <label htmlFor="totaligst" className="mb-0 muted-text"><strong>IGST Amount</strong></label>
                                                    <Control.text model=".totaligst" className="form-control form-control-sm" id="totaligst" name="totaligst" onChange={this.handleInputChange} value={this.state.totaligst} onBlur={this.handleOnBlurMegadata} />
                                                </div>
                                                <div className="form-group mb-1 col-6 col-md-2">
                                                    <label htmlFor="totalcgst" className="mb-0 muted-text"><strong>CGST Amount</strong></label>
                                                    <Control.text model=".totalcgst" className="form-control form-control-sm" id="totalcgst" name="totalcgst" onChange={this.handleInputChange} value={this.state.totalcgst} onBlur={this.handleOnBlurMegadata} />
                                                </div>

                                                <div className="form-group mb-1 col-6 col-md-2">
                                                    <label htmlFor="totalsgst" className="mb-0 muted-text"><strong>SGST Amount</strong></label>
                                                    <Control.text model=".totalsgst" className="form-control form-control-sm" id="totalsgst" name="totalsgst" onChange={this.handleInputChange} value={this.state.totalsgst} onBlur={this.handleOnBlurMegadata} />
                                                </div>
                                                <div className="form-group mb-1 col-6 col-md-2">
                                                    <label htmlFor="GrandTotal" className="mb-0 muted-text"><strong>Grand total</strong></label>
                                                    <Control.text model=".grandtotal" className="form-control form-control-sm" id="grandtotal" name="grandtotal" onChange={this.handleInputChange} value={this.state.grandtotal} />
                                                </div>
                                            </div>
                                            {this.state.asset === false ? <div className="row col-12 mt-2 pb-5"><button type="submit" className="btn btn-success mt-1 btn-sm small" >Enter purchase</button> </div> : <div className="row col-12 mt-2 pb-5"><button type="submit" className="btn btn-success mt-1 btn-sm small" >Enter Asset</button> </div>}
                                        </div>
                                    </LocalForm>
                                </div>
                            </div>
                            <Footer />
                        </div>
                    </div >
                )
            }
        }
    }
}

export default Purchase;