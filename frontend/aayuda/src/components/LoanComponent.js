import React, { Component } from 'react';
import cookie from 'react-cookie';
import { Redirect } from 'react-router-dom';
import { LocalForm, Control } from 'react-redux-form';
import { Header } from './HeaderComponent';
import { Footer } from './Footer';

class LoanEntry extends Component {
    constructor(props) {
        super(props);
        this.state = ({
            loanrepayment: '',
            loantype: '',
            paymentmode: '',
            remark: '',
            date: new Date().toDateString().split(' ').slice(1).join(' '),
            bankdetail: '',
            amount: '',
            loanaccount: '',
            voucherno: this.props.getLoan.voucherno ? this.props.getLoan.voucherno : 1,
            partner: false
        })
        window.onload = () => {
            this.props.refreshBankState();
            this.props.refreshLoanState();
            this.props.refreshLoanAccountState();
            this.props.refreshInvestmentAccountState();
        }
    }
    componentDidMount() {
        if (this.props.addLoanMessage.message === 'success') {
            alert("Loan Detail Added Successfully");
            this.props.resetaddLoanMessage();
        }
        else if (this.props.addLoanMessage.message === 'Already Registered') {
            alert("Voucher Number " + (parseInt(this.props.getPayment.voucherno) - 1) + "already registered");
            this.props.resetaddLoanMessage();
        }
        if (this.props.addLoanMessage.error !== '') {
            alert(this.props.addLoanMessage.error);
            this.setState({
                ...this.props.addLoanMessage.state
            })
            this.props.resetaddLoanMessage();
        }
        if (this.props.getLoan.message === 'initial') this.props.getLoanDetail(this.props.businessid, this.props.fyid);
        if (this.props.getLoanAccount.message === 'initial') this.props.getLoanAccountList(this.props.businessid);
        if (this.props.getBankDetail.message === 'initial') this.props.getBankDetailList(this.props.businessid);
        if (this.props.getInvestmentAccount.message === 'initial') this.props.getInvestmentAccountList(this.props.businessid);
        if (this.props.logoutMessage.error !== '') {
            alert("Error Logging Out. Please Try Again");
            this.props.resetLogoutMessage();
        }
    }
    handleInputChange = (event) => {
        let target = event.target;
        let name = target.name;
        let value = target.value;
        if (name === 'partner') {
            this.setState({
                ...this.state,
                partner: !this.state.partner
            })
        }
        else this.setState({
            ...this.state,
            [name]: value
        })
    }

    handleSubmit = () => {
        let business = this.props.businessList.data.filter(b => b.businessid === this.props.businessid)[0];
        let fy = business.financial.filter(f => f.fyid === this.props.fyid)[0].financialyear;
        if (isNaN(parseInt(this.state.voucherno)) || (this.state.voucherno < 0)) alert("Voucher No is mandatory and Must Be In Digits");
        else if (this.state.loanrepayment === '') alert("Please Select Loan/Repayment");
        else if (this.state.loanaccount === '') alert("Please Select Loan Account");
        else if (this.state.loantype === '') alert("Please Select Received/Given");
        else if (this.state.paymentmode === '') alert("Please Select Payment Mode");
        else if (!(/^[a-zA-Z]{3} [0-9]{2} [0-9]{4}$/i.test(this.state.date))) alert("Date Must Be In (MON DD YYYY) Format");
        else if ((new Date(this.state.date) < new Date("01 Apr " + fy.split("-")[0])) || (new Date(this.state.date) > new Date("31 Mar " + fy.split("-")[1]))) alert("Invalid Date. Date Must Be Within FY " + fy);
        else if (isNaN(parseInt(this.state.amount)) || (this.state.amount < 0)) alert("Amount is mandatory and Must Be Positive Number");
        else if (this.state.remark.length > 199) alert("Remark Must Be Within 200 Characters");
        else if (this.state.paymentmode !== 'cash') {
            if (this.state.bankdetail === '') alert("Please Select Bank Account");
            else this.props.raiseLoan(this.props.businessid, this.props.fyid, this.state, fy);
        }
        else this.props.raiseLoan(this.props.businessid, this.props.fyid, this.state, fy);
    }
    render() {
        if (!cookie.load('userservice')) return <Redirect to="/" />;
        else {
            if ((this.props.addLoanMessage.loading === true) || (this.props.getLoan.loading === true) || (this.props.logoutMessage.loading === true) || (this.props.getBankDetail.loading === true) || (this.props.getLoanAccount.loading === true) || (this.props.getInvestmentAccount.loading === true)) {
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div className="mt-3 d-flex justify-content-center">
                            <button className="btn btn-danger">Loading... </button>
                        </div>
                    </div>
                );
            }
            else if ((this.props.getLoan.error !== '') || (this.props.getBankDetail.error !== '') || (this.props.getLoanAccount.error !== '') || (this.props.getInvestmentAccount.error !== '')) {
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div className="mt-3 d-flex justify-content-center">
                            <button className="btn btn-danger">Error.. Please Refresh and Try Agin </button>
                        </div>
                    </div>
                );
            }
            else {
                let business = this.props.businessList.data.filter(b => b.businessid === this.props.businessid)[0];
                let fy = business.financial.filter(f => f.fyid === this.props.fyid)[0].financialyear;
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div style={{ minHeight: "93vh", display: "flex", flexDirection: "column" }}>
                            <div className="container" style={{ flex: "1" }}>
                                <h6 className="text-center pl-2 pr-2 pt-4" style={{ lineHeight: "1" }}><strong>{business.businessname} {fy}</strong></h6>
                                <h6 className="text-center pl-2 pr-2 mb-4" style={{ lineHeight: "1" }}>{business.alias ? "(" + business.alias + ")" : ''}</h6>
                                <h6 className="text-center pl-2 pr-2"><strong><u>Loan/Repayment Entry</u></strong></h6>
                                <div className="row" >
                                    <LocalForm className="small" onSubmit={this.handleSubmit}>
                                        <div className="container mt-4 d-flex justify-content-center">
                                            <div class="row col-12 col-md-4">
                                                
                                                <div className="form-group mb-1 col-12">
                                                    <label htmlFor="voucherno" className="mb-0 muted-text"><strong>Voucher No</strong><span className="text-danger">*</span></label>
                                                    <Control.text model=".voucherno" className="form-control form-control-sm" id="voucherno" name="voucherno" onChange={this.handleInputChange} value={this.state.voucherno} />
                                                </div>
                                                <div className="form-group mb-1 col-12">
                                                    <label htmlFor="loanrepayment" className="mb-0 muted-text"><strong>Loan/Repayment:<span className="text-danger">*</span></strong></label>
                                                    <Control.select model=".loanrepayment" className="form-control form-control-sm" id="loanrepayment" name="loanrepayment" onChange={this.handleInputChange} value={this.state.loanrepayment}>
                                                        <option value="">Loan/Repayment</option>
                                                        <option value="loan">Loan </option>
                                                        <option value="repayment">Repayment</option>
                                                    </Control.select>
                                                </div>
                                                <div className="form-group mb-1 col-12">
                                                    <label htmlFor="loantype" className="mb-0 muted-text"><strong>Given/Received:<span className="text-danger">*</span></strong></label>
                                                    <Control.select model=".loantype" className="form-control form-control-sm" id="loantype" name="loantype" onChange={this.handleInputChange} value={this.state.loantype}>
                                                        <option value="">Given/Received</option>
                                                        <option value="given">Given</option>
                                                        <option value="received">Received</option>
                                                    </Control.select>
                                                </div>
                                                {this.state.partner === false ? <div className="form-group mb-1 col-12">
                                                    <label htmlFor="loanaccount" className="mb-0 muted-text"><strong>Select Loan Account:</strong><span className="text-danger">*</span></label>
                                                    <Control.select model=".loanaccount" className="form-control form-control-sm" id="loanaccount" name="loanaccount" onChange={this.handleInputChange} value={this.state.loanaccount}>
                                                        <option value="">Select Loan Account</option>
                                                        {this.props.getLoanAccount.data.map(ac => <option value={ac.id + "..." + ac.loanaccount}>{ac.loanaccount}</option>)}
                                                    </Control.select>
                                                </div> : ''}
                                                {this.state.partner === true ? <div className="form-group mb-1 col-12">
                                                    <label htmlFor="loanaccount" className="mb-0 muted-text"><strong>{business.businesstype === 'partnership' ? 'Select Partner Account' : ''}</strong><span className="text-danger">*</span></label>
                                                    <Control.select model=".loanaccount" className="form-control form-control-sm" id="loanaccount" name="loanaccount" onChange={this.handleInputChange} value={this.state.loanaccount}>
                                                        <option value="">Select Partner Account</option>
                                                        {this.props.getInvestmentAccount.data.map(b => <option value={b.id + "..." + b.investmentaccount}>{b.investmentaccount}</option>)}
                                                    </Control.select>
                                                </div> : ''}
                                                <div className="form-group mb-1 col-12">
                                                    <label htmlFor="paymentmode" className="mb-0 muted-text"><strong>Payment Mode:</strong><span className="text-danger">*</span></label>
                                                    <Control.select model=".paymentmode" className="form-control form-control-sm" id="paymentmode" name="paymentmode" onChange={this.handleInputChange} value={this.state.paymentmode}>
                                                        <option value="">Payment Mode</option>
                                                        <option value="cash">Cash</option>
                                                        <option value="cheque">Cheque</option>
                                                        <option value="rtgs">RTGS/NEFT</option>
                                                        <option value="other">Other digital payment mode</option>
                                                    </Control.select>
                                                </div>
                                                {(this.state.paymentmode !== 'cash') ? <div className="form-group mb-1 col-12">
                                                    <label htmlFor="bankdetail" className="mb-0 muted-text"><strong>Select Transcation Bank Account:</strong><span className="text-danger">*</span></label>
                                                    <Control.select model=".bankdetail" className="form-control form-control-sm" id="bankdetail" name="bankdetail" onChange={this.handleInputChange} value={this.state.bankdetail}>
                                                        <option value="">Select Bank Name</option>
                                                        {this.props.getBankDetail.data.map(b => <option value={b.bankdetailid + "..." + b.bankname}>{b.bankname}</option>)}
                                                    </Control.select>
                                                </div> : ''}
                                                <div className="form-group mb-1 col-12">
                                                    <label htmlFor="date" className="mb-0 muted-text"><strong>Date<span className="text-danger">*</span></strong></label>
                                                    <Control.text model=".date" className="form-control form-control-sm" id="date" name="date" onChange={this.handleInputChange} value={this.state.date} />
                                                </div>
                                                <div className="form-group mb-1 col-12">
                                                    <label htmlFor="amount" className="mb-0 muted-text"><strong>Amount<span className="text-danger">*</span></strong></label>
                                                    <Control.text model=".amount" className="form-control form-control-sm" id="amount" name="amount" onChange={this.handleInputChange} value={this.state.amount} />
                                                </div>
                                                <div className="form-group mb-1 col-12">
                                                    <label htmlFor="remark" className="mb-0 muted-text"><strong>Remarks</strong></label>
                                                    <Control.text model=".remark" className="form-control form-control-sm" id="remark" name="remark" onChange={this.handleInputChange} value={this.state.remark} />
                                                </div>
                                                <div className="form-group mb-1 col-12">
                                                    <button type="submit" className="btn btn-success mt-1 btn-sm small" >Enter</button>
                                                </div>
                                            </div>
                                        </div>
                                    </LocalForm>
                                </div>
                            </div>
                            <Footer />
                        </div>
                    </div>
                )
            }
        }
    }
}

export default LoanEntry;