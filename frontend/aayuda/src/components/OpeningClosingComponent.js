import React, { Component } from 'react';
import cookie from 'react-cookie';
import { Redirect } from 'react-router-dom';
import { LocalForm, Control } from 'react-redux-form';
import { Header } from './HeaderComponent';
import { Footer } from './Footer';

const DisplayOpeningClosingForm = function ({ state, handleInputChange, handleSubmit }) {
    let returnrows = [];
    if (state.configvalue !== '') {
        if (state.configvalue === 'fy') {
            returnrows.push(<div>
                <div className="form-group mb-1 p-1">
                    <label htmlFor="openingstock" className="mb-0 muted-text"><strong>Opening Stock as on 1st April {state.fy.split('-')[0]}<span className="text-danger">*</span></strong></label>
                    <Control.text model=".openingstock" className="form-control form-control-sm" id="openingstock" name="aprilopening" onChange={handleInputChange} value={state.aprilopening} />
                </div>
                <div className="form-group mb-1 p-1">
                    <label htmlFor="closingstock" className="mb-0 muted-text"><strong>Closing Stock as of 31st March {state.fy.split('-')[1]}<span className="text-danger">*</span></strong></label>
                    <Control.text model=".closingstock" className="form-control form-control-sm" id="closingstock" name="marchclosing" onChange={handleInputChange} value={state.marchclosing} />
                </div>
            </div>)
        }
        else if (state.configvalue === '1qtr') {
            returnrows.push(<div>
                <div className="form-group mb-1 p-1">
                    <label htmlFor="openingstock" className="mb-0 muted-text"><strong>Opening Stock as on 1st April {state.fy.split('-')[0]}<span className="text-danger">*</span></strong></label>
                    <Control.text model=".openingstock" className="form-control form-control-sm" id="openingstock" name="aprilopening" onChange={handleInputChange} value={state.aprilopening} />
                </div>
                <div className="form-group mb-1 p-1">
                    <label htmlFor="closingstock" className="mb-0 muted-text"><strong>Closing Stock as of 30th June {state.fy.split('-')[0]}<span className="text-danger">*</span></strong></label>
                    <Control.text model=".closingstock" className="form-control form-control-sm" id="closingstock" name="juneclosing" onChange={handleInputChange} value={state.juneclosing} />
                </div>
            </div>)
        }
        else if (state.configvalue === '2qtr') {
            returnrows.push(<div>
                <div className="form-group mb-1 p-1">
                    <label htmlFor="openingstock" className="mb-0 muted-text"><strong>Opening Stock as on 1st July {state.fy.split('-')[0]}<span className="text-danger">*</span></strong></label>
                    <Control.text model=".openingstock" className="form-control form-control-sm" id="openingstock" name="julyopening" onChange={handleInputChange} value={state.julyopening} />
                </div>
                <div className="form-group mb-1 p-1">
                    <label htmlFor="closingstock" className="mb-0 muted-text"><strong>Closing Stock as of 30th September {state.fy.split('-')[0]}<span className="text-danger">*</span></strong></label>
                    <Control.text model=".closingstock" className="form-control form-control-sm" id="closingstock" name="septemberclosing" onChange={handleInputChange} value={state.septemberclosing} />
                </div>
            </div>)
        }
        else if (state.configvalue === '3qtr') {
            returnrows.push(<div>
                <div className="form-group mb-1 p-1">
                    <label htmlFor="openingstock" className="mb-0 muted-text"><strong>Opening Stock as on 1st October {state.fy.split('-')[0]}<span className="text-danger">*</span></strong></label>
                    <Control.text model=".openingstock" className="form-control form-control-sm" id="openingstock" name="octoberopening" onChange={handleInputChange} value={state.octoberopening} />
                </div>
                <div className="form-group mb-1 p-1">
                    <label htmlFor="closingstock" className="mb-0 muted-text"><strong>Closing Stock as of 31st December {state.fy.split('-')[0]}<span className="text-danger">*</span></strong></label>
                    <Control.text model=".closingstock" className="form-control form-control-sm" id="closingstock" name="decemberclosing" onChange={handleInputChange} value={state.decemberclosing} />

                </div>
            </div>)
        }
        else if (state.configvalue === '4qtr') {
            returnrows.push(<div>
                <div className="form-group mb-1 p-1">
                    <label htmlFor="openingstock" className="mb-0 muted-text"><strong>Opening Stock as on 1st January {state.fy.split('-')[1]}<span className="text-danger">*</span></strong></label>
                    <Control.text model=".openingstock" className="form-control form-control-sm" id="openingstock" name="januaryopening" onChange={handleInputChange} value={state.januaryopening} />

                </div>
                <div className="form-group mb-1 p-1">
                    <label htmlFor="closingstock" className="mb-0 muted-text"><strong>Closing Stock as of 31st March {state.fy.split('-')[1]}<span className="text-danger">*</span></strong></label>
                    <Control.text model=".closingstock" className="form-control form-control-sm" id="closingstock" name="marchclosing" onChange={handleInputChange} value={state.marchclosing} />

                </div>
            </div>)
        }
        else if (state.configvalue === 'january') {
            returnrows.push(<div>
                <div className="form-group mb-1 p-1">
                    <label htmlFor="openingstock" className="mb-0 muted-text"><strong>Opening Stock as on 1st January {state.fy.split('-')[1]}<span className="text-danger">*</span></strong></label>
                    <Control.text model=".openingstock" className="form-control form-control-sm" id="openingstock" name="januaryopening" onChange={handleInputChange} value={state.januaryopening} />
                </div>
                <div className="form-group mb-1 p-1">
                    <label htmlFor="closingstock" className="mb-0 muted-text"><strong>Closing Stock as of 31st January {state.fy.split('-')[1]}<span className="text-danger">*</span></strong></label>
                    <Control.text model=".closingstock" className="form-control form-control-sm" id="closingstock" name="januaryclosing" onChange={handleInputChange} value={state.januaryclosing} />

                </div>
            </div>)
        }
        else if (state.configvalue === 'feburary') {
            returnrows.push(<div>
                <div className="form-group mb-1 p-1">
                    <label htmlFor="openingstock" className="mb-0 muted-text"><strong>Opening Stock as on 1st feburary {state.fy.split('-')[1]}<span className="text-danger">*</span></strong></label>
                    <Control.text model=".openingstock" className="form-control form-control-sm" id="openingstock" name="feburaryopening" onChange={handleInputChange} value={state.feburaryopening} />

                </div>
                <div className="form-group mb-1 p-1">
                    <label htmlFor="closingstock" className="mb-0 muted-text"><strong>Closing Stock as of end of February {state.fy.split('-')[1]}<span className="text-danger">*</span></strong></label>
                    <Control.text model=".closingstock" className="form-control form-control-sm" id="closingstock" name="feburaryclosing" onChange={handleInputChange} value={state.feburaryclosing} />

                </div>
            </div>)
        }
        else if (state.configvalue === 'march') {
            returnrows.push(<div>
                <div className="form-group mb-1 p-1">
                    <label htmlFor="openingstock" className="mb-0 muted-text"><strong>Opening Stock as on 1st March {state.fy.split('-')[1]}<span className="text-danger">*</span></strong></label>
                    <Control.text model=".openingstock" className="form-control form-control-sm" id="openingstock" name="marchopening" onChange={handleInputChange} value={state.marchopening} />
                </div>
                <div className="form-group mb-1 p-1">
                    <label htmlFor="closingstock" className="mb-0 muted-text"><strong>Closing Stock as of 31st March {state.fy.split('-')[1]}<span className="text-danger">*</span></strong></label>
                    <Control.text model=".closingstock" className="form-control form-control-sm" id="closingstock" name="marchclosing" onChange={handleInputChange} value={state.marchclosing} />
                </div>
            </div>)
        }
        else if (state.configvalue === 'april') {
            returnrows.push(<div>
                <div className="form-group mb-1 p-1">
                    <label htmlFor="openingstock" className="mb-0 muted-text"><strong>Opening Stock as on 1st April {state.fy.split('-')[0]}</strong><span className="text-danger">*</span></label>
                    <Control.text model=".openingstock" className="form-control form-control-sm" id="openingstock" name="aprilopening" onChange={handleInputChange} value={state.aprilopening} />
                </div>
                <div className="form-group mb-1 p-1">
                    <label htmlFor="closingstock" className="mb-0 muted-text"><strong>Closing Stock as of 30th April {state.fy.split('-')[0]}</strong><span className="text-danger">*</span></label>
                    <Control.text model=".closingstock" className="form-control form-control-sm" id="closingstock" name="aprilclosing" onChange={handleInputChange} value={state.aprilclosing} />
                </div>
            </div>)
        }
        else if (state.configvalue === 'may') {
            returnrows.push(<div>
                <div className="form-group mb-1 p-1">
                    <label htmlFor="openingstock" className="mb-0 muted-text"><strong>Opening Stock as on 1st May {state.fy.split('-')[0]}</strong><span className="text-danger">*</span></label>
                    <Control.text model=".openingstock" className="form-control form-control-sm" id="openingstock" name="mayopening" onChange={handleInputChange} value={state.mayopening} />
                </div>
                <div className="form-group mb-1 p-1">
                    <label htmlFor="closingstock" className="mb-0 muted-text"><strong>Closing Stock as of 31st May {state.fy.split('-')[0]}</strong><span className="text-danger">*</span></label>
                    <Control.text model=".closingstock" className="form-control form-control-sm" id="closingstock" name="mayclosing" onChange={handleInputChange} value={state.mayclosing} />

                </div>
            </div>)
        }
        else if (state.configvalue === 'june') {
            returnrows.push(<div>
                <div className="form-group mb-1 p-1">
                    <label htmlFor="openingstock" className="mb-0 muted-text"><strong>Opening Stock as on 1st Jun {state.fy.split('-')[0]}</strong><span className="text-danger">*</span></label>
                    <Control.text model=".openingstock" className="form-control form-control-sm" id="openingstock" name="juneopening" onChange={handleInputChange} value={state.juneopening} />
                </div>
                <div className="form-group mb-1 p-1">
                    <label htmlFor="closingstock" className="mb-0 muted-text"><strong>Closing Stock as of 30th June {state.fy.split('-')[0]}</strong><span className="text-danger">*</span></label>
                    <Control.text model=".closingstock" className="form-control form-control-sm" id="closingstock" name="juneclosing" onChange={handleInputChange} value={state.juneclosing} />

                </div>
            </div>)
        }
        else if (state.configvalue === 'july') {
            returnrows.push(<div>
                <div className="form-group mb-1 p-1">
                    <label htmlFor="openingstock" className="mb-0 muted-text"><strong>Opening Stock as on 1st July {state.fy.split('-')[0]}</strong><span className="text-danger">*</span></label>
                    <Control.text model=".openingstock" className="form-control form-control-sm" id="openingstock" name="julyopening" onChange={handleInputChange} value={state.julyopening} />

                </div>
                <div className="form-group mb-1 p-1">
                    <label htmlFor="closingstock" className="mb-0 muted-text"><strong>Closing Stock as of 31st July {state.fy.split('-')[0]}</strong><span className="text-danger">*</span></label>
                    <Control.text model=".closingstock" className="form-control form-control-sm" id="closingstock" name="julyclosing" onChange={handleInputChange} value={state.julyclosing} />

                </div>
            </div>)
        }
        else if (state.configvalue === 'august') {
            returnrows.push(<div>
                <div className="form-group mb-1 p-1">
                    <label htmlFor="openingstock" className="mb-0 muted-text"><strong>Opening Stock as on 1st August {state.fy.split('-')[0]}</strong><span className="text-danger">*</span></label>
                    <Control.text model=".openingstock" className="form-control form-control-sm" id="openingstock" name="augustopening" onChange={handleInputChange} value={state.augustopening} />
                </div>
                <div className="form-group mb-1 p-1">
                    <label htmlFor="closingstock" className="mb-0 muted-text"><strong>Closing Stock as of 31st August {state.fy.split('-')[0]}</strong><span className="text-danger">*</span></label>
                    <Control.text model=".closingstock" className="form-control form-control-sm" id="closingstock" name="augustclosing" onChange={handleInputChange} value={state.augustclosing} />
                </div>
            </div>)
        }
        else if (state.configvalue === 'september') {
            returnrows.push(<div>
                <div className="form-group mb-1 p-1">
                    <label htmlFor="openingstock" className="mb-0 muted-text"><strong>Opening Stock as on 1st September {state.fy.split('-')[0]}</strong><span className="text-danger">*</span></label>
                    <Control.text model=".openingstock" className="form-control form-control-sm" id="openingstock" name="septemberopening" onChange={handleInputChange} value={state.septemberopening} />
                </div>
                <div className="form-group mb-1 p-1">
                    <label htmlFor="closingstock" className="mb-0 muted-text"><strong>Closing Stock as of 30th September {state.fy.split('-')[0]}</strong><span className="text-danger">*</span></label>
                    <Control.text model=".closingstock" className="form-control form-control-sm" id="closingstock" name="septemberclosing" onChange={handleInputChange} value={state.septemberclosing} />
                </div>
            </div>)
        }
        else if (state.configvalue === 'october') {
            returnrows.push(<div>
                <div className="form-group mb-1 p-1">
                    <label htmlFor="openingstock" className="mb-0 muted-text"><strong>Opening Stock as on 1st October {state.fy.split('-')[0]}</strong><span className="text-danger">*</span></label>
                    <Control.text model=".openingstock" className="form-control form-control-sm" id="openingstock" name="octoberopening" onChange={handleInputChange} value={state.octoberopening} />
                </div>
                <div className="form-group mb-1 p-1">
                    <label htmlFor="closingstock" className="mb-0 muted-text"><strong>Closing Stock as of 31st October {state.fy.split('-')[0]}</strong><span className="text-danger">*</span></label>
                    <Control.text model=".closingstock" className="form-control form-control-sm" id="closingstock" name="octoberclosing" onChange={handleInputChange} value={state.octoberclosing} />
                </div>
            </div>)
        }
        else if (state.configvalue === 'november') {
            returnrows.push(<div>
                <div className="form-group mb-1 p-1">
                    <label htmlFor="openingstock" className="mb-0 muted-text"><strong>Opening Stock as on 1st November {state.fy.split('-')[0]}</strong><span className="text-danger">*</span></label>
                    <Control.text model=".openingstock" className="form-control form-control-sm" id="openingstock" name="novemberopening" onChange={handleInputChange} value={state.novemberopening} />
                </div>
                <div className="form-group mb-1 p-1">
                    <label htmlFor="closingstock" className="mb-0 muted-text"><strong>Closing Stock as of 30th November {state.fy.split('-')[0]}</strong><span className="text-danger">*</span></label>
                    <Control.text model=".closingstock" className="form-control form-control-sm" id="closingstock" name="novemberclosing" onChange={handleInputChange} value={state.novemberclosing} />
                </div>
            </div>)
        }
        else if (state.configvalue === 'december') {
            returnrows.push(<div>
                <div className="form-group mb-1 p-1">
                    <label htmlFor="openingstock" className="mb-0 muted-text"><strong>Opening Stock as on 1st December {state.fy.split('-')[0]}</strong><span className="text-danger">*</span></label>
                    <Control.text model=".openingstock" className="form-control form-control-sm" id="openingstock" name="decemberopening" onChange={handleInputChange} value={state.decemberopening} />
                </div>
                <div className="form-group mb-1 p-1">
                    <label htmlFor="closingstock" className="mb-0 muted-text"><strong>Closing Stock as of 31st December {state.fy.split('-')[0]}</strong><span className="text-danger">*</span></label>
                    <Control.text model=".closingstock" className="form-control form-control-sm" id="closingstock" name="decemberclosing" onChange={handleInputChange} value={state.decemberclosing} />
                </div>
            </div>)
        }
    }
    return returnrows;
}

class OpeningClosing extends Component {
    constructor(props) {
        super(props);
        let business = this.props.businessList.data.filter(b => b.businessid === this.props.businessid)[0];
        let financial = business.financial.filter(f => f.fyid === this.props.fyid)[0];
        this.state = ({
            fy: financial.financialyear,
            configvalue: this.props.getOpeningClosing.data.configvalue ? this.props.getOpeningClosing.data.configvalue : "fy",
            aprilopening: this.props.getOpeningClosing.data.aprilopening,
            aprilclosing: this.props.getOpeningClosing.data.aprilclosing,
            mayopening: this.props.getOpeningClosing.data.mayopening,
            mayclosing: this.props.getOpeningClosing.data.mayclosing,
            juneopening: this.props.getOpeningClosing.data.juneopening,
            juneclosing: this.props.getOpeningClosing.data.juneclosing,
            julyopening: this.props.getOpeningClosing.data.julyopening,
            julyclosing: this.props.getOpeningClosing.data.julyclosing,
            augustopening: this.props.getOpeningClosing.data.augustopening,
            augustclosing: this.props.getOpeningClosing.data.augustclosing,
            septemberopening: this.props.getOpeningClosing.data.septemberopening,
            septemberclosing: this.props.getOpeningClosing.data.septemberclosing,
            octoberopening: this.props.getOpeningClosing.data.octoberopening,
            octoberclosing: this.props.getOpeningClosing.data.octoberclosing,
            novemberopening: this.props.getOpeningClosing.data.novemberopening,
            novemberclosing: this.props.getOpeningClosing.data.novemberclosing,
            decemberopening: this.props.getOpeningClosing.data.decemberopening,
            decemberclosing: this.props.getOpeningClosing.data.decemberclosing,
            januaryopening: this.props.getOpeningClosing.data.januaryopening,
            januaryclosing: this.props.getOpeningClosing.data.januaryclosing,
            feburaryopening: this.props.getOpeningClosing.data.feburaryopening,
            feburaryclosing: this.props.getOpeningClosing.data.feburaryclosing,
            marchopening: this.props.getOpeningClosing.data.marchopening,
            marchclosing: this.props.getOpeningClosing.data.marchclosing
        })
        window.onload = () => {
            this.props.resetOpeningClosingDetail();
        }
    }

    componentDidMount() {
        if (this.props.addOpeningClosingMessage.message === 'success') {
            alert("Data Updated Successfully");
            this.props.resetaddOpeningClosingMessage('success');
        }
        if (this.props.addOpeningClosingMessage.error !== '') {
            alert("Error Occured - Please Try Again");
            this.setState({
                ...this.props.addOpeningClosingMessage.state
            })
            this.props.resetaddOpeningClosingMessage('error');
        }
        if (this.props.logoutMessage.error !== '') {
            alert("Error Logging Out. Please Try Again");
            this.props.resetLogoutMessage();
        }
        if ((this.props.getOpeningClosing.message === 'initial') && (this.props.getOpeningClosing.loading === false)) this.props.getOpeningClosingDetail(this.props.businessid, this.props.fyid);
    }

    handleInputChange = (event) => {
        let target = event.target;
        let name = target.name;
        let value = target.value;
        this.setState({
            ...this.state,
            [name]: value
        })
    }

    handleSubmit = () => {
        if (this.state.configvalue === 'fy') {
            if ((this.state.aprilopening === '') || (this.state.marchclosing === '') || (this.state.aprilopening < 0) || (this.state.marchclosing < 0) || isNaN(parseInt(this.state.aprilopening)) || isNaN(parseInt(this.state.marchclosing))) alert(" Opening and Closing Values must be greater than or equal to 0 ");
            else this.props.raiseOpeningClosing(this.props.businessid, this.props.fyid, this.state);
        }
        else if (this.state.configvalue === '1qtr') {
            if ((this.state.aprilopening === '') || (this.state.juneclosing === '') || (this.state.aprilopening < 0) || (this.state.juneclosing < 0) || isNaN(parseInt(this.state.aprilopening)) || isNaN(parseInt(this.state.juneclosing))) alert(" Opening and Closing Values must be greater than or equal to 0 ");
            else this.props.raiseOpeningClosing(this.props.businessid, this.props.fyid, this.state);
        }
        else if (this.state.configvalue === '2qtr') {
            if ((!this.state.julyopening === '') || (this.state.septemberclosing === '') || (this.state.julyopening < 0) || (this.state.septemberclosing < 0) || isNaN(parseInt(this.state.julyopening)) || isNaN(parseInt(this.state.septemberclosing))) alert(" Opening and Closing Values must be greater than or equal to 0 ");
            else this.props.raiseOpeningClosing(this.props.businessid, this.props.fyid, this.state);
        }
        else if (this.state.configvalue === '3qtr') {
            if ((this.state.octoberopening === '') || (this.state.decemberclosing === '') || (this.state.octoberopening < 0) || (this.state.decemberclosing < 0) || isNaN(parseInt(this.state.octoberopening)) || isNaN(parseInt(this.state.decemberclosing))) alert(" Opening and Closing Values must be greater than or equal to 0 ");
            else this.props.raiseOpeningClosing(this.props.businessid, this.props.fyid, this.state);
        }
        else if (this.state.configvalue === '4qtr') {
            if ((this.state.januaryopening === '') || (this.state.marchclosing === '') || (this.state.januaryopening < 0) || (this.state.marchclosing < 0) || isNaN(parseInt(this.state.januaryopening)) || isNaN(parseInt(this.state.marchclosing))) alert(" Opening and Closing Values must be greater than or equal to 0 ");
            else this.props.raiseOpeningClosing(this.props.businessid, this.props.fyid, this.state);
        }
        else if (this.state.configvalue === 'april') {
            if ((this.state.aprilopening === '') || (this.state.aprilclosing === '') || (this.state.aprilopening < 0) || (this.state.aprilclosing < 0) || isNaN(parseInt(this.state.aprilopening)) || isNaN(parseInt(this.state.aprilclosing))) alert(" Opening and Closing Values must be greater than or equal to 0 ");
            else this.props.raiseOpeningClosing(this.props.businessid, this.props.fyid, this.state);
        }
        else if (this.state.configvalue === 'may') {
            if ((this.state.mayopening === '') || (this.state.mayclosing === '') || (this.state.mayopening < 0) || (this.state.mayclosing < 0) || isNaN(parseInt(this.state.mayopening)) || isNaN(parseInt(this.state.mayclosing))) alert(" Opening and Closing Values must be greater than or equal to 0 ");
            else this.props.raiseOpeningClosing(this.props.businessid, this.props.fyid, this.state);
        }
        else if (this.state.configvalue === 'june') {
            if ((this.state.juneopening === '') || (this.state.juneclosing === '') || (this.state.juneopening < 0) || (this.state.juneclosing < 0) || isNaN(parseInt(this.state.juneopening)) || isNaN(parseInt(this.state.juneclosing))) alert(" Opening and Closing Values must be greater than or equal to 0 ");
            else this.props.raiseOpeningClosing(this.props.businessid, this.props.fyid, this.state);
        }
        else if (this.state.configvalue === 'july') {
            if ((this.state.julyopening === '') || (this.state.julyclosing === '') || (this.state.julyopening < 0) || (this.state.julyclosing < 0) || isNaN(parseInt(this.state.julyopening)) || isNaN(parseInt(this.state.julyclosing))) alert(" Opening and Closing Values must be greater than or equal to 0 ");
            else this.props.raiseOpeningClosing(this.props.businessid, this.props.fyid, this.state);
        }
        else if (this.state.configvalue === 'august') {
            if ((this.state.augustopening === '') || (this.state.augustclosing === '') || (this.state.augustopening < 0) || (this.state.augustclosing < 0) || isNaN(parseInt(this.state.augustopening)) || isNaN(parseInt(this.state.augustclosing))) alert(" Opening and Closing Values must be greater than or equal to 0 ");
            else this.props.raiseOpeningClosing(this.props.businessid, this.props.fyid, this.state);
        }
        else if (this.state.configvalue === 'september') {
            if ((this.state.septemberopening === '') || (this.state.septemberclosing === '') || (this.state.septemberopening < 0) || (this.state.septemberclosing < 0) || isNaN(parseInt(this.state.septemberopening)) || isNaN(parseInt(this.state.septemberclosing))) alert(" Opening and Closing Values must be greater than or equal to 0 ");
            else this.props.raiseOpeningClosing(this.props.businessid, this.props.fyid, this.state);
        }
        else if (this.state.configvalue === 'october') {
            if ((this.state.octoberopening === '') || (this.state.octoberclosing === '') || (this.state.octoberopening < 0) || (this.state.octoberclosing < 0) || isNaN(parseInt(this.state.octoberopening)) || isNaN(parseInt(this.state.octoberclosing))) alert(" Opening and Closing Values must be greater than or equal to 0 ");
            else this.props.raiseOpeningClosing(this.props.businessid, this.props.fyid, this.state);
        }
        else if (this.state.configvalue === 'november') {
            if ((this.state.novemberopening === '') || (this.state.novemberclosing === '') || (this.state.novemberopening < 0) || (this.state.novemberclosing < 0) || isNaN(parseInt(this.state.novemberopening)) || isNaN(parseInt(this.state.novemberclosing))) alert(" Opening and Closing Values must be greater than or equal to 0 ");
            else this.props.raiseOpeningClosing(this.props.businessid, this.props.fyid, this.state);
        }
        else if (this.state.configvalue === 'december') {
            if ((this.state.decemberopening === '') || (this.state.decemberclosing === '') || (this.state.decemberopening < 0) || (this.state.decemberclosing < 0) || isNaN(parseInt(this.state.decemberopening)) || isNaN(parseInt(this.state.decemberclosing))) alert(" Opening and Closing Values must be greater than or equal to 0 ");
            else this.props.raiseOpeningClosing(this.props.businessid, this.props.fyid, this.state);
        }
        else if (this.state.configvalue === 'january') {
            if ((this.state.januaryopening === '') || (this.state.januaryclosing === '') || (this.state.januaryopening < 0) || (this.state.januaryclosing < 0) || isNaN(parseInt(this.state.januaryopening)) || isNaN(parseInt(this.state.januaryclosing))) alert(" Opening and Closing Values must be greater than or equal to 0 ");
            else this.props.raiseOpeningClosing(this.props.businessid, this.props.fyid, this.state);
        }
        else if (this.state.configvalue === 'feburary') {
            if ((this.state.feburaryclosing === '') || (this.state.feburaryclosing === '') || (this.state.feburaryopening < 0) || (this.state.feburaryclosing < 0) || isNaN(parseInt(this.state.feburaryopening)) || isNaN(parseInt(this.state.feburaryclosing))) alert(" Opening and Closing Values must be greater than or equal to 0 ");
            else this.props.raiseOpeningClosing(this.props.businessid, this.props.fyid, this.state);
        }
        else if (this.state.configvalue === 'march') {
            if ((this.state.marchclosing === '') || (this.state.marchclosing === '') || (this.state.marchopening < 0) || (this.state.marchclosing < 0) || isNaN(parseInt(this.state.marchopening)) || isNaN(parseInt(this.state.marchclosing))) alert(" Opening and Closing Values must be greater than or equal to 0 ");
            else this.props.raiseOpeningClosing(this.props.businessid, this.props.fyid, this.state);
        }
    }

    render() {
        if (!cookie.load('userservice')) return <Redirect to="/" />;
        else {
            if ((this.props.addOpeningClosingMessage.loading === true) || (this.props.logoutMessage.loading === true) || (this.props.getOpeningClosing.loading === true)) {
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div className="mt-3 d-flex justify-content-center">
                            <button className="btn btn-danger">Loading... </button>
                        </div>
                    </div>
                );
            }
            else if (this.props.getOpeningClosing.error !== '') {
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div className="mt-3 d-flex justify-content-center">
                            <button className="btn btn-danger">Error. Please Refresh and Try Again</button>
                        </div>
                    </div>
                );
            }
            else {
                let business = this.props.businessList.data.filter(b => b.businessid === this.props.businessid)[0];
                let fy = business.financial.filter(f => f.fyid === this.props.fyid)[0].financialyear;
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div style={{ minHeight: "93vh", display: "flex", flexDirection: "column" }}>
                            <div className="container" style={{ flex: "1" }}>
                                <h6 className="text-center pl-2 pr-2 pt-4" style={{ lineHeight: "1" }}><strong>{business.businessname} {fy}</strong></h6>
                                <h6 className="text-center pl-2 pr-2 mb-4" style={{ lineHeight: "1" }}>{business.alias ? "(" + business.alias + ")" : ''}</h6>
                                <h6 className="text-center pl-2 pr-2"><strong><u>Opening & Closing Stock Value</u></strong></h6>
                                <div className="row" >
                                    <LocalForm className="small col-12" onSubmit={this.handleSubmit}>
                                        <div className="container mt-4 d-flex justify-content-center">
                                            <div class="row col-12 col-md-4">
                                                <div className="form-group mb-1 col-12">
                                                    <label htmlFor="configvalue" className="mb-0 muted-text"><strong>Select Period<span className="text-danger">*</span></strong></label>
                                                    <Control.select model=".configvalue" className="form-control form-control-sm" id="configvalue" name="configvalue" onChange={this.handleInputChange} value={this.state.configvalue}>
                                                        <option value="fy">{"for FY" + this.state.fy}</option>
                                                        <option value="1qtr">{'1st Qtr ' + this.state.fy}</option>
                                                        <option value="2qtr">{'2nd Qtr ' + this.state.fy}</option>
                                                        <option value="3qtr">{'3rd Qtr ' + this.state.fy}</option>
                                                        <option value="4qtr">{'4th Qtr ' + this.state.fy}</option>
                                                    </Control.select>
                                                </div>
                                                <div className="mb-1 col-12">
                                                    <DisplayOpeningClosingForm state={this.state} handleInputChange={this.handleInputChange} handleSubmit={this.handleSubmit} />
                                                </div>
                                                <div className=" mb-1 col-12">
                                                    <button type="submit" className="btn btn-success mt-1 btn-sm small" >Update</button>
                                                </div>
                                            </div>
                                        </div>
                                    </LocalForm>
                                </div>
                            </div>
                            <Footer />
                        </div>
                    </div>
                )
            }
        }
    }
}

export default OpeningClosing;