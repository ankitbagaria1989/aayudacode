import React, { Component } from 'react';
import cookie from 'react-cookie';
import { Redirect } from 'react-router-dom';
import { LocalForm, Control } from 'react-redux-form';
import { Header } from './HeaderComponent';
import { sortData } from '../redux/sortArrayofObjects';
import { Footer } from './Footer';

const Option = function () {
    let arr = [];
    for (let i = -1; i < 12; i++) {
        if (i === -1) arr.push(<option value=''>Select Month Filter :</option>);
        else if (i === 0) arr.push(<option value="Jan">Jan</option>);
        else if (i === 1) arr.push(<option value="Feb">Feb</option>);
        else if (i === 2) arr.push(<option value="Mar">Mar</option>);
        else if (i === 3) arr.push(<option value="Apr">Apr</option>);
        else if (i === 4) arr.push(<option value="May">May</option>);
        else if (i === 5) arr.push(<option value="Jun">Jun</option>);
        else if (i === 6) arr.push(<option value="Jul">Jul</option>);
        else if (i === 7) arr.push(<option value="Aug">Aug</option>);
        else if (i === 8) arr.push(<option value="Sep">Sep</option>);
        else if (i === 9) arr.push(<option value="Oct">Oct</option>);
        else if (i === 10) arr.push(<option value="NoV">Nov</option>);
        else if (i === 11) arr.push(<option value="Dec">Dec</option>);
    }
    return arr;
}

const DataRow = function ({ data, businessid, businessList, fyid, i }) {
    if (data.type === 'sales') {
        return (
            <div>
                <div className=" d-flex small p-1 d-md-none d-lg-none" style={i % 2 === 0 ? { backgroundColor: "rgb(181, 253, 233)" } : {}}>
                    <div className="col-5 col-md-5 p-1">
                        <div className="col-12 col-md-6 p-0"><strong>{data.invoicenumber}/{data.invoicedate}</strong></div>
                        <div className="col-12 col-md-6 p-0 ">GR No: {data.grno ? data.grno : "NA"}</div>
                    </div>
                    <div className="col-3 col-md-6 p-0 pt-1">
                        <div className="col-12 col-md-2 p-0"></div>
                    </div>
                    <div className="col-3 col-md-1 p-0 text-right">({parseFloat(data.blamount).toFixed(2)}) {parseFloat(0.05 * parseFloat(data.blamount)).toFixed(2)}</div>
                </div>
                <div className="container d-none d-md-block small p-1" style={i % 2 === 0 ? { backgroundColor: "rgb(181, 253, 233)" } : {}}>
                    <div className="d-flex">
                        <div className="col-1 p-0">{data.invoicedate}</div>
                        <div className="col-1 p-0">{data.invoicenumber}</div>
                        <div className="col-4 p-0">
                            <div className="col-12 p-0"><strong>GR No: {data.grno ? data.grno : "NA"}</strong></div>
                        </div>
                        <div className="col-2 text-right"></div>
                        <div className="col-3 text-right">({parseFloat(data.blamount).toFixed(2)}) {parseFloat(0.05 * parseFloat(data.blamount)).toFixed(2)}</div>
                        <div className="col-1 pt-1"></div>
                    </div>
                </div>
            </div>
        )
    }
    else if (data.type === "payment") {
        return (
            <div>
                <div className=" d-flex small p-1 d-md-none d-lg-none" style={i % 2 === 0 ? { backgroundColor: "rgb(181, 253, 233)" } : {}}>
                    <div className="col-5 col-md-5 p-1">
                        <div className="col-12 col-md-6 p-0"><strong>{data.voucherno}/{data.date}</strong></div>
                        <div className="col-12 col-md-6 p-0">By {data.bankdetail.split("...")[1]}</div>
                        <div className="col-12 col-md-6 p-0">{data.vendorid.toUpperCase()} Payment</div>
                        <div className="col-12 col-md-6 p-0">{data.transactionno}</div>
                        <div className="col-12 col-md-6 p-0">{data.remark}</div>
                    </div>
                    <div className="col-3 col-md-6 p-0 pt-1">{parseFloat(data.amount).toFixed(2)}</div>
                    <div className="col-3 col-md-1"></div>
                </div>
                <div className="container d-none d-md-block small p-1" style={i % 2 === 0 ? { backgroundColor: "rgb(181, 253, 233)" } : {}}>
                    <div className="d-flex">
                        <div className="col-1 p-0">{data.date}</div>
                        <div className="col-1 p-0">{data.voucherno}</div>
                        <div className="col-4 p-0">
                            <div className="col-12 p-0">
                                <div className="col-12 p-0"><strong>By {data.bankdetail.split("...")[1]}</strong></div>
                                <div className="col-12 p-0">{data.vendorid.toUpperCase()} Payment</div>
                                <div className="col-12 p-0">{data.transactionno}</div>
                                <div className="col-12 p-0">{data.remark}</div>
                            </div>
                        </div>
                        <div className="col-2 p-0">{parseFloat(data.amount).toFixed(2)}</div>
                        <div className="col-3"></div>
                        <div className="col-1 pt-1"></div>
                    </div>
                </div>
            </div>
        )
    }
    else return '';

}
const DisplayData = function ({ state, salesDetail, businessid, businessList, fyid, getPayment, getRCOpeningBalance }) {
    let returndata = [];
    let paymentdata = getPayment.data.filter(d => d.vendorid === "reversecharge");
    let data = salesDetail.data;
    data = data.concat(paymentdata);
    data = data.sort(sortData);
    if (state.monthSelection !== '') {
        data = data.filter(d => d.invoicedate.split(' ')[0] === state.monthSelection)
    }
    let totalrcamount = parseFloat(getRCOpeningBalance.data);
    let totalrcpayment = 0;
    let i = 1;
    if (getRCOpeningBalance.data) {
        returndata.push(<div><div className="d-flex small p-1 d-md-none d-lg-none" style={{ backgroundColor: "rgb(181, 253, 233)" }}>
            <div className="col-5 col-md-5 p-1">
                <div className="col-12 col-md-6 p-0"><strong>Apr/Opening Bal</strong></div>
            </div>
            <div className="col-3 col-md-1">
            </div>
            <div className="col-3 col-md-6 p-1">
                <div className="col-12 col-md-2 p-0"><strong>{parseFloat(totalrcamount).toFixed(2)}</strong></div>
            </div>
            <div className="col-1"></div>
        </div>
            <div className="container small mt-3 d-none d-md-block p-0">
                <div className="d-flex">
                    <div className="col-2 p-0"><strong>Apr Opening Bal</strong></div>
                    <div className="col-4 p-0"><strong></strong></div>
                    <div className="col-2"><strong></strong></div>
                    <div className="col-3"><strong>{parseFloat(totalrcamount).toFixed(2)}</strong></div>
                </div>
            </div>
        </div>)
        i++;
    }
    let temp = '';
    if (data.length > 0) {
        temp =
            <div>
                <div>
                    {data.map(d => {
                        i++;
                        (d.type === "sales" && d.active == 0) ? totalrcamount = totalrcamount + 0.05 * parseFloat(d.blamount) : d.type === 'payment' ? totalrcpayment = totalrcpayment + parseFloat(d.amount) : totalrcamount = totalrcamount;
                        return <DataRow data={d} businessid={businessid} businessList={businessList} fyid={fyid} i={i} />
                    })}
                </div>
            </div>
        returndata.push(temp);
    }

    else if (data.length === 0) {
        temp =
            <div className="container mt-2">
                <div className="card border">
                    <div className="text-center">No Data to Display</div>
                </div>
            </div>
        returndata.push(temp)
    }
    let t2 = <div>
        <div className=" d-flex small p-1 d-md-none d-lg-none">
            <div className="col-5 col-md-5 p-1"></div>
            <div className="col-3 col-md-1 p-0 text-right"><strong>Debit</strong></div>
            <div className="col-3 col-md-1 p-0 text-right"><strong>Credit</strong></div>
        </div>
        <div className="container small mt-3 d-none d-md-block p-0">
            <div className="d-flex">
                <div className="col-1 p-0"><strong>Date</strong></div>
                <div className="col-1 p-0"><strong>Voucher No</strong></div>
                <div className="col-4 p-0"><strong>Particulars</strong></div>
                <div className="col-2 text-right"><strong>Debit</strong></div>
                <div className="col-3 text-right"><strong>Credit</strong></div>
            </div>
        </div>
    </div>

    returndata.push(<div>
        <div className=" d-flex small p-1 d-md-none d-lg-none">
            <div className="col-5 col-md-5 p-1"></div>
            <div className="col-3 col-md-1 border-top border-dark text-right p-0">{parseFloat(totalrcpayment).toFixed(2)}</div>
            <div className="col-3 col-md-1 border-top border-dark text-right p-0">{parseFloat(totalrcamount).toFixed(2)}</div>
        </div>
        <div className=" d-flex small mb-1 p-1 d-md-none d-lg-none">
            <div className="col-5 col-md-5"><strong>{(totalrcamount) - (totalrcpayment) > 0 ? "RC Payable" : "Input"}</strong></div>
            <div className="col-3 col-md-1 text-right p-0">{(totalrcamount) - (totalrcpayment) > 0 ? parseFloat((totalrcamount) - (totalrcpayment)).toFixed(2) : ""}</div>
            <div className="col-3 col-md-1 text-right p-0">{(totalrcamount) - (totalrcpayment) <= 0 ? parseFloat((totalrcpayment) - (totalrcamount)).toFixed(2) : ""}</div>
        </div>
        <div className="container small d-none d-md-block p-1">
            <div className="d-flex">
                <div className="col-1 p-0"><strong></strong></div>
                <div className="col-1 p-0"><strong></strong></div>
                <div className="col-4 p-0"><strong></strong></div>
                <div className="col-2 border-top border-dark text-right">{parseFloat(totalrcpayment).toFixed(2)}</div>
                <div className="col-3 border-top border-dark text-right">{parseFloat(totalrcamount).toFixed(2)}</div>
            </div>
        </div>
        <div className="container small d-none d-md-block p-1">
            <div className="d-flex">
                <div className="col-1 p-0"><strong></strong></div>
                <div className="col-1 p-0"><strong></strong></div>
                <div className="col-4 p-0"><strong>{(totalrcamount) - (totalrcpayment) > 0 ? "RC Payable" : "Input"}</strong></div>
                <div className="col-2 text-right">{(totalrcamount) - (totalrcpayment) > 0 ? parseFloat((totalrcamount) - (totalrcpayment)).toFixed(2) : ""}</div>
                <div className="col-3 text-right">{(totalrcamount) - (totalrcpayment) <= 0 ? parseFloat((totalrcpayment) - (totalrcamount)).toFixed(2) : ""}</div>
            </div>
        </div>
    </div>)


    let returndatatemp = [];
    returndatatemp.push(t2);
    returndatatemp.push(returndata);
    return returndatatemp;
}

const DisplayReverseChargeData = function ({ businessid, businessList, salesDetail, state, handleOnChange, fyid, getPayment, getRCOpeningBalance }) {
    return (
        <div>
            <LocalForm className="small">
                <div className="d-flex justify-content-center mt-4">
                    <div className="form-group mb-1 col-12 col-md-3">
                        <label htmlFor="monthSelection" className="mb-0 muted-text"><strong>Filter Data by Month:</strong></label>
                        <Control.select model=".monthSelection" className="form-control form-control-sm" id="monthSelection" name="monthSelection" value={state.monthSelection} onChange={handleOnChange}>
                            <Option />
                        </Control.select>
                    </div>
                </div>
            </LocalForm>
            <DisplayData state={state} salesDetail={salesDetail} businessid={businessid} businessList={businessList} fyid={fyid} getPayment={getPayment} getRCOpeningBalance={getRCOpeningBalance} />
        </div>
    )
}
class ReverseChargeDetail extends Component {
    constructor(props) {
        super(props);
        this.state = ({
            monthSelection: ''
        })
        window.onload = () => {
            this.props.resetSalesDetail();
            this.props.resetPaymentDetail();
            this.props.resetRCOpeningBalance();
        }
    }
    handleOnChange = (event) => {
        let target = event.target;
        let name = target.name;
        let value = target.value;
        this.setState({
            ...this.state,
            [name]: value
        })
    }
    componentDidMount() {
        if ((this.props.salesDetail.message === 'initial') && (this.props.salesDetail.loading === false)) this.props.getSalesData(this.props.businessid, this.props.fyid);
        if (this.props.getPayment.message === 'initial') this.props.getPaymentDetail(this.props.businessid, this.props.fyid);
        if (this.props.getRCOpeningBalance.message === 'initial') this.props.getRCOpeningBalanceList(this.props.businessid, this.props.fyid);
        if (this.props.logoutMessage.error !== '') {
            alert("Error Logging Out. Please Try Again");
            this.props.resetLogoutMessage();
        }
    }
    render() {
        if (!cookie.load('userservice')) return <Redirect to="/" />;
        else {
            if ((this.props.salesDetail.loading === true) || (this.props.getPayment.loading === 'true') || (this.props.logoutMessage.loading === true) || (this.props.getRCOpeningBalance.loading === true)) {
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div className="mt-3 d-flex justify-content-center">
                            <button className="btn btn-danger">Loading... </button>
                        </div>
                    </div>
                );
            }
            else if ((this.props.salesDetail.error !== '') || (this.props.getPayment.error !== '') || (this.props.getRCOpeningBalance.error !== '')) {
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div className="mt-3 d-flex justify-content-center">
                            <button className="btn btn-danger">Error.. Please Refresh and Try Agin </button>
                        </div>
                    </div>
                );
            }
            else {
                let business = this.props.businessList.data.filter(b => b.businessid === this.props.businessid)[0];
                let fy = business.financial.filter(f => f.fyid === this.props.fyid)[0].financialyear;
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div style={{ minHeight: "93vh", display: "flex", flexDirection: "column" }}>
                            <div className="container" style={{ flex: "1" }}>
                                <h6 className="text-center pl-2 pr-2 pt-4" style={{ lineHeight: "1" }}><strong>{business.businessname} {fy}</strong></h6>
                                <h6 className="text-center pl-2 pr-2 mb-4" style={{ lineHeight: "1" }}>{business.alias ? "(" + business.alias + ")" : ''}</h6>
                                <h6 className="text-center pl-2 pr-2"><strong><u>Reverse Charge Sheet</u></strong></h6>
                                < DisplayReverseChargeData businessid={this.props.businessid} businessList={this.props.businessList} salesDetail={this.props.salesDetail} state={this.state} handleOnChange={this.handleOnChange} fyid={this.props.fyid} getPayment={this.props.getPayment} getRCOpeningBalance={this.props.getRCOpeningBalance} />
                            </div>
                            <Footer />
                        </div>
                    </div>
                )
            }
        }
    }
}
export default ReverseChargeDetail;
