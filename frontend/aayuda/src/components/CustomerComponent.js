import React, { Component, useState } from 'react';
import cookie from 'react-cookie';
import { Redirect, Link } from 'react-router-dom';
import { Button, Collapse } from 'reactstrap';
import { Form, Errors, Control } from 'react-redux-form';
import { Header } from './HeaderComponent';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faAngleDoubleDown } from '@fortawesome/free-solid-svg-icons';
import { Footer } from './Footer';

const required = (val) => val && val.length;
const maxLength = (len) => val => val ? val.length < len : true;
const length = (len) => val => (val) ? val.length === len : true;
const isNumber = (val) => !isNaN(parseInt(val));
const validEmail = (val) => /^(?:[A-Za-z0-9.+-_%$#*]+@[A-Za-z0-9.+-_%$#*]+\.[A-Za-z]{2,4}|)$/i.test(val);

const ErrorComponent = function ({ children }) {
    return (
        <div style={{ lineHeight: "1" }}>{children}</div>
    )
}

const AddCustomer = function ({ handleSubmit, businessid, addCustomerMessage }) {
    let [isOpen, setIsOpen] = useState(false);
    let toggle = () => setIsOpen((!isOpen));
    if (addCustomerMessage.error !== '') {
        isOpen = true;
    }
    return (
        <div className="col-12">
            <Button className="btn-sm mt-3 col-12" style={{ backgroundColor: "#fff", border: "1px solid #28a745", color: "#28a745" }} onClick={toggle}><div className="d-flex"><span className="col-11 text-center">Add a New Customer</span><span className="col-1 p-0 text-right"><FontAwesomeIcon size="sm" icon={faAngleDoubleDown} /></span></div></Button>
            <Collapse isOpen={isOpen}>
                <Form className="" model="addCustomer" onSubmit={((values) => handleSubmit(values, businessid))}>
                    <div className="justify-content-center mt-4">
                        <div className="form-group col-12 mb-1">
                            <label htmlFor="businessname" className="mb-0"><span className="muted-text">Customer Business Name<span className="text-danger">*</span></span></label>
                            <div className="">
                                <Control.text model=".businessname" className="form-control form-control-sm pt-3 pb-3" id="businessname" name="businessname" placeholder="Business Name" validators={{ required, maxLength: maxLength(100) }} />
                                <Errors className="text-danger" model=".businessname" show="touched" messages={{
                                    required: "Business Name is Mandatory to Register",
                                    maxLength: "length must be within 100 characters"
                                }}
                                    component={ErrorComponent}
                                />
                            </div>
                        </div>
                        <div className="form-group col-12 mb-1">
                            <label htmlFor="alias" className="mb-0"><span className="muted-text">Alias</span></label>
                            <div className="">
                                <Control.text model=".alias" className="form-control form-control-sm pt-3 pb-3" id="alias" name="alias" placeholder="Alias" validators={{ maxLength: maxLength(100) }} />
                                <Errors className="text-danger" model=".alias" show="touched" messages={{
                                    maxLength: "length must be within 100 characters"
                                }}
                                    component={ErrorComponent}
                                />
                            </div>
                        </div>
                        <div className="form-group col-12 mb-1">
                            <label htmlFor="gstin" className="mb-0 muted-text">Gstin   </label>
                            <div className="">
                                <Control.text model=".gstin" className="form-control form-control-sm" id="gstin" name="gstin" placeholder="GSTIN" validators={{ length: length(15) }} />
                                <Errors className="text-danger" model=".gstin" show="touched" messages={{
                                    length: "length must be 15 characters"
                                }}
                                    component={ErrorComponent}
                                />
                            </div>
                        </div>
                        <div className="form-group col-12 mb-1">
                            <label htmlFor="state" className="mb-0 muted-text">State</label>
                            <div className="">
                                <Control.text model=".state" className="form-control form-control-sm pt-3 pb-3" id="state" name="state" placeholder="State" validators={{ maxLength: maxLength(15) }} />
                                <Errors className="text-danger" model=".state" show="touched" messages={{
                                    maxLength: "length must be within 15 characters"
                                }}
                                    component={ErrorComponent}
                                />
                            </div>
                        </div>
                        <div className="form-group col-12 mb-1">
                            <label htmlFor="address" className="mb-0 muted-text">Address</label>
                            <div className="">
                                <Control.textarea model=".address" rows="3" className="form-control form-control-sm pt-3 pb-3" id="address" name="address" placeholder="Address" validators={{ maxLength: maxLength(300) }} />
                                <Errors className="text-danger" model=".address" show="touched" messages={{
                                    maxLength: "length must be within 300 characters"
                                }}
                                    component={ErrorComponent}
                                />
                            </div>
                        </div>
                        <div className="form-group col-12 mb-1">
                            <label htmlFor="pincode" className="mb-0 muted-text">Pincode</label>
                            <div className="">
                                <Control.text model=".pincode" className="form-control form-control-sm pt-3 pb-3" id="pincode" name="pincode" placeholder="Pincode" validators={{ length: length(6) }} />
                                <Errors className="text-danger" model=".pincode" show="touched" messages={{
                                    length: "length must be 6 digits"
                                }}
                                    component={ErrorComponent}
                                />
                            </div>
                        </div>
                        <div className="form-group col-12 mb-1">
                            <label htmlFor="mobile" className="mb-0 muted-text">Mobile Number</label>
                            <div className="">
                                <Control.text model=".mobile" className="form-control form-control-sm pt-3 pb-3" id="mobile" name="mobile" placeholder="Mobile Number" validators={{ maxLength: maxLength(20) }} />
                                <Errors className="text-danger" model=".mobile" show="touched" messages={{
                                    maxLength: "Length must be within 20 characters"
                                }}
                                    component={ErrorComponent}
                                />
                            </div>
                        </div>
                        <div className="form-group col-12 mb-1">
                            <label htmlFor="landline" className="mb-0 muted-text">LandLine Number</label>
                            <div className="">
                                <Control.text model=".landline" className="form-control form-control-sm pt-3 pb-3" id="landline" name="landline" placeholder="LandLine eg: 01202571836" validators={{ maxLength: maxLength(20) }} />
                                <Errors className="text-danger" model=".landline" show="touched" messages={{
                                    maxLength: "Length must be within 20 characters"
                                }}
                                    component={ErrorComponent}
                                />
                            </div>
                        </div>
                        <div className="form-group col-12">
                            <label htmlFor="Email" className="mb-0 muted-text">Email</label>
                            <div className="">
                                <Control type="Email" model=".email" className="form-control form-control-sm pt-3 pb-3" id="email" name="email" placeholder="Email" validators={{ validEmail }} />
                                <Errors className="text-danger" model=".email" show="touched" messages={{
                                    validEmail: "Must be a Valid Email Format"
                                }}
                                    component={ErrorComponent}
                                />
                                <div className="mt-3"><button type="submit" className="btn btn-success btn-sm pl-4 pr-4" >Add</button></div>
                            </div>
                        </div>
                    </div>
                </Form>
            </Collapse>
        </div>
    );
}

const CustomerListCard = function ({ cust }) {
    let [isOpen, setIsOpen] = useState(false);
    let toggle = () => setIsOpen((!isOpen));

    return (<div className="">
        <div className="col-12">
            <Button className="btn-sm btn mt-2 col-12 btn-success" onClick={toggle}><div className="d-flex"><span className="col-11"><div>{cust.businessname}</div><div>{cust.alias ? "(" + cust.alias + ")" : ''}</div></span><span className="col-1 p-0 text-right"><FontAwesomeIcon size="sm" icon={faAngleDoubleDown} /></span></div></Button>
            <Collapse isOpen={isOpen}>
                <div className="card card-body mt-1">
                    <div><span className="text-secondary">Address :</span> {cust.address}</div>
                    <div><span className="text-secondary">State :</span> {cust.state}</div>
                    <div><span className="text-secondary">Pincode :</span> {cust.pincode}</div>
                    <div><span className="text-secondary">GSTIN :</span> {cust.gstin}</div>
                    <div><span className="text-secondary">Mobile : </span>{cust.mobile}</div>
                    <div><span className="text-secondary">Email : </span>{cust.email}</div>
                    <div><span className="text-secondary">Landline : </span>{cust.landline}</div>
                    <div><Link to={`/business/businessdetail/customer/${cust.businessid}/${cust.customerid}`}> <button className="btn btn-sm justify-content-right mt-2 pl-4 pr-4" role="button" style={{ backgroundColor: "#fff", border: "1px solid #28a745", color: "#28a745" }}>Edit</button></Link></div>
                </div>
            </Collapse>
        </div>
    </div>);
}

const CustomerList = function ({ getCustomer }) {
    if (getCustomer.error === '') {
        if (getCustomer.data.length > 0) return getCustomer.data.map((cust) => <CustomerListCard cust={cust} />);
        else return <h6 className="col-12 text-center text-muted mt-2 pb-4">No Customers Added Yet</h6>;
    }
    else return <div></div>
}

class Customer extends Component {
    constructor(props) {
        super(props);
        window.onload = () => {
            this.props.refreshCustomerState();
        }
    }

    componentDidMount() {
        if (this.props.getCustomer.message === 'initial') this.props.getCustomerList(this.props.businessid);
        if (this.props.addCustomerMessage.error !== '') {
            alert("Error " + this.props.addCustomerMessage.error);
            this.props.resetAddCustomerMessage();
        }
        if (this.props.addCustomerMessage.message === 'success') {
            alert("Customer Added Successfully");
            this.props.resetAddCustomerMessageAndForm();
        }
        if (this.props.addCustomerMessage.message === 'Already Registered') {
            alert("Customer Already registered");
            this.props.resetAddCustomerMessageAndForm();
        }
        if (this.props.updateCustomerMessage.error !== '') {
            this.props.resetCustomerChange();
        }
        if (this.props.logoutMessage.error !== '') {
            alert("Error Logging Out. Please Try Again");
            this.props.resetLogoutMessage();
        }
    }

    handleSubmit = (values, businessid) => {
        this.props.addCustomer(values, businessid);
    }

    render() {
        if (!cookie.load('userservice')) return <Redirect to="/" />
        else {
            if ((this.props.getCustomer.loading === true) || (this.props.addCustomerMessage.loading === true) || (this.props.logoutMessage.loading === true)) {
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div className="mt-3 d-flex justify-content-center">
                            <button className="btn btn-danger">Loading... </button>
                        </div>
                    </div>
                );
            }
            else if (this.props.getCustomer.error !== '') {
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div className="mt-3 d-flex justify-content-center">
                            <button className="btn btn-danger">Error.. Please Refresh Try Agin </button>
                        </div>
                    </div>
                );
            }
            else {
                let business = this.props.businessList.data.filter(b => b.businessid === this.props.businessid)[0];
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div style={{ minHeight: "93vh", display: "flex", flexDirection: "column" }}>
                            <div className="container d-flex justify-content-center pr-0 pl-0">
                                <div className="col-12 col-md-5">
                                    <div className="small pr-0 pl-0">
                                        <h6 className="text-center pl-2 pr-2 pt-4" style={{ lineHeight: "1" }}><strong>{business.businessname}</strong></h6>
                                        <h6 className="text-center pl-2 pr-2 mb-4" style={{ lineHeight: "1" }}>{business.alias ? "(" + business.alias + ")" : ''}</h6>
                                        <h6 className="text-center pl-2 pr-2 mb-2"><strong><u>Customers</u></strong></h6>
                                        <AddCustomer handleSubmit={this.handleSubmit} businessid={this.props.businessid} addCustomerMessage={this.props.addCustomerMessage} />
                                    </div>
                                </div>
                            </div>
                            <div className="container d-flex justify-content-center pr-0 pl-0 mt-4 pb-4" style={{ flex: "1" }}>
                                <div className="col-12 col-md-5">
                                    <div className="small pr-0 pl-0">
                                        <CustomerList getCustomer={this.props.getCustomer} addCustomerMessage={this.props.addCustomerMessage} />
                                    </div>
                                </div>
                            </div>
                            <Footer />
                        </div>
                    </div>
                );
            }
        }
    }
}

export default Customer;