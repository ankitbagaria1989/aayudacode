import React, { Component } from 'react';
import cookie from 'react-cookie';
import { Redirect, Link } from 'react-router-dom';
import { LocalForm, Control } from 'react-redux-form';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTrash, faEdit, faFilePdf, faFileExcel } from '@fortawesome/free-solid-svg-icons';
import { Header } from './HeaderComponent';
import { Footer } from './Footer';
import { Page, Text, View, Document, pdf } from '@react-pdf/renderer';
import { pdfstyles } from '../sylesheet/stylesheet';
import { saveAs } from 'file-saver';

const Option = function ({ list, scope }) {
    let arr = [];
    if (scope === "purchaseDetail") {
        for (let i = -1; i < 12; i++) {
            if (i === -1) arr.push(<option value=''>Select Month Filter :</option>);
            else if (i === 0) arr.push(<option value="Jan">Jan</option>);
            else if (i === 1) arr.push(<option value="Feb">Feb</option>);
            else if (i === 2) arr.push(<option value="Mar">Mar</option>);
            else if (i === 3) arr.push(<option value="Apr">Apr</option>);
            else if (i === 4) arr.push(<option value="May">May</option>);
            else if (i === 5) arr.push(<option value="Jun">Jun</option>);
            else if (i === 6) arr.push(<option value="Jul">Jul</option>);
            else if (i === 7) arr.push(<option value="Aug">Aug</option>);
            else if (i === 8) arr.push(<option value="Sep">Sep</option>);
            else if (i === 9) arr.push(<option value="Oct">Oct</option>);
            else if (i === 10) arr.push(<option value="NoV">Nov</option>);
            else if (i === 11) arr.push(<option value="Dec">Dec</option>);
        }
    }
    return arr;
}
let totalcredit = 0, totaldebit = 0;
const PurchaseDataPage = function ({ data, y, vendor }) {
    let debit = 0, credit = 0;
    let returnrows = [];
    if (y > 1) {
        returnrows.push(
            <View style={pdfstyles.itemSectionLedger, { textAlign: "center" }}>
                <View style={pdfstyles.itemHeader}><Text style={{ width: '75px', textAlign: "left" }}></Text><Text style={{ width: '75px', textAlign: "left" }}></Text><Text style={{ width: '225px', textAlign: "left", fontFamily:"Helvetica-Bold" }}>...Carry Forward</Text><Text style={{ width: '100px', textAlign: "right" }}>{parseFloat(totaldebit).toFixed(2)}</Text><Text style={{ width: '100px', textAlign: "right" }}>{parseFloat(totalcredit).toFixed(2)}</Text></View>
            </View>
        )
        debit = totaldebit;
        credit = totalcredit;
    }
    for (let i = (y - 1) * 50; i < y * 50 && i < data.length; i++) {
        let vendorname = vendor.filter(v => v.vendorid === data[i].vendorid)[0].businessname;
        if (data[i].type === 'purchase') {
            let d = data[i];
            debit = debit + parseFloat(d.totalbasic);
            totaldebit = totaldebit + parseFloat(d.totalbasic);
            returnrows.push(
                <View style={pdfstyles.itemSectionLedger, { textAlign: "center" }}>
                    <View style={pdfstyles.itemHeader}><Text style={{ width: '75px', textAlign: "left" }}>{d.invoicedate}</Text><Text style={{ width: '75px', textAlign: "left" }}>{d.invoicenumber}</Text><Text style={{ width: '225px', textAlign: "left", fontFamily:"Helvetica-Bold" }}>To {vendorname}</Text><Text style={{ width: '100px', textAlign: "right" }}>{parseFloat(d.totalbasic).toFixed(2)}</Text><Text style={{ width: '100px', textAlign: "right" }}></Text></View>
                </View>
            )
        }
        else {
            let d = data[i];
            credit = credit + parseFloat(d.basic);
            totalcredit = totalcredit + parseFloat(d.basic);
            returnrows.push(
                <View style={pdfstyles.itemSectionLedger, { textAlign: "center" }}>
                    <View style={pdfstyles.itemHeader}><Text style={{ width: '75px', textAlign: "left" }}>{d.date}</Text><Text style={{ width: '75px', textAlign: "left" }}>{d.voucherno}</Text><Text style={{ width: '225px', textAlign: "left", fontFamily:"Helvetica-Bold" }}>By Credit Note</Text><Text style={{ width: '100px', textAlign: "right" }}></Text><Text style={{ width: '100px', textAlign: "right" }}>{parseFloat(d.basic).toFixed(2)}</Text></View>
                </View>
            )
        }
    }
    if (y === Math.ceil(data.length / 50)) {
        returnrows.push(<View><View style={pdfstyles.itemSectionLedger, { marginTop: "5px", paddingBottom: "2px" }}>
            <View style={pdfstyles.itemHeader}><Text style={{ width: '75px', textAlign: "left" }}></Text><Text style={{ width: '75px', textAlign: "left" }}></Text><Text style={{ width: '225px', textAlign: "left" }}></Text><Text style={{ width: '100px', borderTop: "1px solid black", textAlign: "right" }}>{parseFloat(totaldebit).toFixed(2)}</Text><Text style={{ width: '100px', borderTop: "1px solid black", textAlign: "right" }}>{parseFloat(totalcredit).toFixed(2)}</Text></View>
        </View>
            <View style={pdfstyles.itemSectionLedger, { fontFamily: "Helvetica-Bold", marginTop: "5px", paddingBottom: "2px", borderTop:"1px solid black", borderBottom:"1px solid black" }}>
                <View style={pdfstyles.itemHeader}><Text style={{ width: '75px', textAlign: "left" }}></Text><Text style={{ width: '75px', textAlign: "left" }}></Text><Text style={{ width: '225px', textAlign: "left" }}>Net Purchase</Text><Text style={{ width: '100px', textAlign: "right" }}>{totaldebit >= totalcredit ? parseFloat(totaldebit - totalcredit).toFixed(2) : ''}</Text><Text style={{ width: '100px', textAlign: "right" }}>{totalcredit > totaldebit ? parseFloat(totalcredit - totaldebit).toFixed(2) : ''}</Text></View>
            </View></View>)
    }
    else {
        returnrows.push(<View><View style={pdfstyles.itemSectionLedger, { fontFamily: "Helvetica-Bold", marginTop: "2px" }}>
            <View style={pdfstyles.itemHeader}><Text style={{ width: '75px', textAlign: "left" }}></Text><Text style={{ width: '75px', textAlign: "left" }}></Text><Text style={{ width: '225px', textAlign: "left" }}></Text><Text style={{ width: '100px', borderTop: "1px solid black", textAlign: "right" }}>{parseFloat(debit).toFixed(2)}</Text><Text style={{ width: '100px', borderTop: "1px solid black", textAlign: "left" }}>{parseFloat(credit).toFixed(2)}</Text></View>
        </View>
            <View style={pdfstyles.itemSectionLedger, { fontFamily: "Helvetica", marginTop: "5px", textAlign: "right" }}><Text></Text>...Continued</View></View>)
    }
    return returnrows;
}
const Report = function ({ business, data, l, n, period, vendor }) {
    let returnrows = [];
    for (let y = 1; y <= n; y++) {
        returnrows.push(<Page size="A4" style={pdfstyles.page}>
            {y === 1 ? <View><View style={pdfstyles.taxInvoiceHeader, { marginTop: "20px", textAlign: "center" }}>
                <View style={{ fontFamily: "Helvetica-Bold", width: "500px" }}><Text>{business.businessname}</Text></View>
                <View style={{ width: "500px" }}><Text>{business.address}</Text></View>
            </View>
                <View style={pdfstyles.taxInvoiceHeader, { marginTop: "10px", textAlign: "center" }}>
                    <View style={{ fontFamily: "Helvetica-Bold", textDecoration: "underline", width: "500px" }}><Text>Purchase Ledger</Text></View>
                    <View style={{ width: "500px" }}><Text>{period}</Text></View>
                </View></View> : ''}
            <View style={pdfstyles.itemSectionLedger, { marginTop: "20px", marginBottom: "5px", fontFamily: "Helvetica-Bold", textAlign: "center", borderTop:"1px solid black", borderBottom : "1px solid black" }}>
                <View style={pdfstyles.itemHeader}><Text style={{ width: '75px', textAlign: "left" }}>Date</Text><Text style={{ width: '75px', textAlign: "left" }}>Voucher No</Text><Text style={{ width: '225px', textAlign: "left" }}>Particulars</Text><Text style={{ width: '100px', textAlign: "right" }}>Debit</Text><Text style={{ width: '100px', textAlign: "right" }}>Credit</Text></View>
            </View>
            <PurchaseDataPage data={data} y={y} vendor={vendor} />
        </Page >)
    }
    return returnrows;
}
const PurchaseLedger = function ({ data, debitcreditdata, business, fy, month, getVendor }) {
    totalcredit = 0;
    totaldebit = 0;
    let period = fy;
    data.concat(debitcreditdata);
    let l = data.length, i = 0, n = Math.ceil(l / 50), y = 0;
    if (month) {
        if (month.toLowerCase() === 'jan' || month.toLowerCase() === 'feb' || month.toLowerCase() === 'mar') {
            period = month.toUpperCase() + " " + fy.split("-")[1];
        }
        else period = month.toUpperCase() + " " + fy.split("-")[0];
    }
    return (
        <Document>
            <Report business={business} data={data} l={l} n={n} period={period} vendor={getVendor} />
        </Document>
    )
}

const DataRow = function ({ data, getVendor, businessid, businessList, deleteEntry, fyid, i }) {
    let vendor = getVendor.data.filter(cust => cust.vendorid === data.vendorid)[0];
    let cancel = function () {
        if (window.confirm("Delete invoice number " + data.invoicenumber)) deleteEntry(data.purchaseid, businessid, fyid, 'purchase');
    }
    if (vendor !== undefined) {
        if (i % 2 === 0) {
            return (
                <div>
                    <div className=" d-flex small border-bottom mb-2 p-1 d-md-none d-lg-none" style={{ backgroundColor: "rgb(181, 253, 233)" }}>
                        <div className="col-5 col-md-5 p-1">
                            <div className="col-12 col-md-6 p-0"><strong>{data.invoicenumber}</strong></div>
                            <div className="col-12 col-md-6 p-0"><strong>{data.invoicedate}</strong></div>
                            <div className="col-12 col-md-6 p-0">To {vendor.businessname}</div>
                        </div>
                        <div className="col-5 col-md-6 p-0 text-right">
                            <div className="col-12 col-md-2 p-0"><strong>{"Basic: " + parseFloat(data.totalbasic).toFixed(2)}</strong></div>
                            <div className="col-12 col-md-2 p-0">{parseInt(data.totaligst) > 0 ? "IGST: " + parseFloat(data.totaligst).toFixed(2) : ''}</div>
                            <div className="cl-12 col-md-2 p-0">{parseInt(data.totalcgst) > 0 ? "CGST: " + parseFloat(data.totalcgst).toFixed(2) : ''}</div>
                            <div className="col-12 col-md-2 p-0">{parseInt(data.totalsgst) > 0 ? "SGST: " + parseFloat(data.totalsgst).toFixed(2) : ''}</div>
                            <div className="col-12 col-md-2 p-0">{parseInt(data.freight) > 0 ? "Freight: " + parseFloat(data.freight).toFixed(2) : ''}</div>
                            <div className="col-12 col-md-2 p-0">{parseInt(data.grandtotal) > 0 ? "Tot: " + parseFloat(data.grandtotal).toFixed(2) : ''}</div>
                        </div>
                        <div className="col-2 col-md-1">
                            <div className="col-12 mt-1"><Link to={`/business/purchaseupdate/${businessid}/${fyid}/${data.purchaseid}`}><FontAwesomeIcon icon={faEdit} size="lg" style={{ color: "black" }} /></Link></div>
                            <div className="col-12 mt-2"><FontAwesomeIcon icon={faTrash} onClick={() => cancel()} size="lg" /></div>
                        </div>
                    </div>
                    <div className="container d-none d-md-block small mb-1 p-1" style={{ backgroundColor: "rgb(181, 253, 233)" }}>
                        <div className="d-flex">
                            <div className="col-1 p-0">{data.invoicedate}</div>
                            <div className="col-1 p-0">{data.invoicenumber}</div>
                            <div className="col-4 p-0"><strong>To {vendor.businessname}</strong></div>
                            <div className="col-2 text-right">
                                <div className="col-12 p-0"><strong>{"Basic: " + parseFloat(data.totalbasic).toFixed(2)}</strong></div>
                                <div className="col-12 p-0">{parseInt(data.totaligst) > 0 ? "IGST: " + parseFloat(data.totaligst).toFixed(2) : ''}</div>
                                <div className="col-12 p-0">{parseInt(data.totalcgst) > 0 ? "CGST: " + parseFloat(data.totalcgst).toFixed(2) : ''}</div>
                                <div className="col-12 p-0">{parseInt(data.totalsgst) > 0 ? "SGST: " + parseFloat(data.totalsgst).toFixed(2) : ''}</div>
                                <div className="col-12 p-0">{parseInt(data.freight) > 0 ? "Freight: " + parseFloat(data.freight).toFixed(2) : ''}</div>
                                <div className="col-12 p-0">{parseInt(data.grandtotal) > 0 ? "Tot: " + parseFloat(data.grandtotal).toFixed(2) : ''}</div>
                            </div>
                            <div className="col-3 text-right"></div>
                            <div className="col-1 p-0 row">
                                <div className="col-4 mt-1"><Link to={`/business/purchaseupdate/${businessid}/${fyid}/${data.purchaseid}`}><FontAwesomeIcon icon={faEdit} size="lg" style={{ color: "black" }} /></Link></div>
                                <div className="col-4 mt-1"><FontAwesomeIcon icon={faTrash} onClick={() => cancel()} size="lg" /></div>
                            </div>
                        </div>
                    </div>
                </div>
            )
        }
        else {
            return (
                <div>
                    <div className=" d-flex small border-bottom mb-2 p-1 d-md-none d-lg-none">
                        <div className="col-5 col-md-5 p-1">
                            <div className="col-12 col-md-6 p-0">{data.invoicenumber}</div>
                            <div className="col-12 col-md-6 p-0">{data.invoicedate}</div>
                            <div className="col-12 col-md-6 p-0">To {vendor.businessname}</div>
                        </div>
                        <div className="col-5 col-md-6 p-0 text-right">
                            <div className="col-12 col-md-2 p-0"><strong>{"Basic: " + parseFloat(data.totalbasic).toFixed(2)}</strong></div>
                            <div className="col-12 col-md-2 p-0">{parseInt(data.totaligst) > 0 ? "IGST: " + parseFloat(data.totaligst).toFixed(2) : ''}</div>
                            <div className="cl-12 col-md-2 p-0">{parseInt(data.totalcgst) > 0 ? "CGST: " + parseFloat(data.totalcgst).toFixed(2) : ''}</div>
                            <div className="col-12 col-md-2 p-0">{parseInt(data.totalsgst) > 0 ? "SGST: " + parseFloat(data.totalsgst).toFixed(2) : ''}</div>
                            <div className="col-12 col-md-2 p-0">{parseInt(data.freight) > 0 ? "Freight: " + parseFloat(data.freight).toFixed(2) : ''}</div>
                            <div className="col-12 col-md-2 p-0">{parseInt(data.grandtotal) > 0 ? "Tot: " + parseFloat(data.grandtotal).toFixed(2) : ''}</div>
                        </div>
                        <div className="col-2 col-md-1">
                            <div className="col-12 mt-1"><Link to={`/business/invoiceupdate/${businessid}/${fyid}/${data.invoiceid}`}><FontAwesomeIcon icon={faEdit} size="lg" style={{ color: "black" }} /></Link></div>
                            <div className="col-12 mt-2"><FontAwesomeIcon icon={faTrash} onClick={() => cancel()} size="lg" /></div>
                        </div>
                    </div>
                    <div className="container d-none d-md-block small mb-1 p-1" style={{ backgroundColor: "rgb(181, 253, 233)" }}>
                        <div className="d-flex">
                            <div className="col-1 p-0">{data.invoicedate}</div>
                            <div className="col-1 p-0">{data.invoicenumber}</div>
                            <div className="col-4 p-0"><strong>To {vendor.businessname}</strong></div>
                            <div className="col-2 text-right">
                                <div className="col-12 p-0"><strong>{"Basic: " + parseFloat(data.totalbasic).toFixed(2)}</strong></div>
                                <div className="col-12 p-0">{parseInt(data.totaligst) > 0 ? "IGST: " + parseFloat(data.totaligst).toFixed(2) : ''}</div>
                                <div className="col-12 p-0">{parseInt(data.totalcgst) > 0 ? "CGST: " + parseFloat(data.totalcgst).toFixed(2) : ''}</div>
                                <div className="col-12 p-0">{parseInt(data.totalsgst) > 0 ? "SGST: " + parseFloat(data.totalsgst).toFixed(2) : ''}</div>
                                <div className="col-12 p-0">{parseInt(data.freight) > 0 ? "Freight: " + parseFloat(data.freight).toFixed(2) : ''}</div>
                                <div className="col-12 p-0">{parseInt(data.grandtotal) > 0 ? "Tot: " + parseFloat(data.grandtotal).toFixed(2) : ''}</div>
                            </div>
                            <div className="col-3"></div>
                            <div className="col-1 p-0 row">
                                <div className="col-4 mt-1"><Link to={`/business/purchaseupdate/${businessid}/${fyid}/${data.purchaseid}`}><FontAwesomeIcon icon={faEdit} size="lg" style={{ color: "black" }} /></Link></div>
                                <div className="col-4 mt-1"><FontAwesomeIcon icon={faTrash} onClick={() => cancel()} size="lg" /></div>
                            </div>
                        </div>
                    </div>
                </div>
            )
        }
    }
    else return <div></div>
}
const DisplayData = function ({ state, purchaseDetail, getVendor, businessid, businessList, deleteEntry, fyid, getDebitCredit }) {
    let returndata = [];
    let data = purchaseDetail.data;
    let debitcreditdata = getDebitCredit.data;
    if (state.monthSelection !== '') {
        data = data.filter(d => d.invoicedate.split(' ')[0] === state.monthSelection);
        debitcreditdata = debitcreditdata.filter(d => d.date.split(' ')[0] === state.monthSelection);
    }
    let totalpurchase = 0;
    let temp = '';
    if (data.length > 0) {
        let i = 1;
        temp =
            <div className="mt-1">
                <div className="">
                    {data.map(d => {
                        i++;
                        totalpurchase = totalpurchase + parseFloat(d.totalbasic);
                        return <DataRow data={d} getVendor={getVendor} businessid={businessid} businessList={businessList} deleteEntry={deleteEntry} fyid={fyid} i={i} />;
                    })}
                </div>
            </div>
        returndata.push(temp);
    }
    let totalcredit = 0;
    let i = 0;
    debitcreditdata.map(d => {
        if ((d.issuereceived === 'issue' && d.debitcredit === 'debit') || (d.issuereceived === 'received' && d.debitcredit === 'credit')) {
            let cancel = function () {
                if (window.confirm("Delete Debit/Credit Note ")) deleteEntry(d.id, businessid, fyid, 'debitcredit');
            }
            i++;
            totalcredit = totalcredit + parseFloat(d.basic);
            temp = <div>
                <div className="">
                    <div className=" d-flex small mb-1 p-1 d-md-none d-lg-none" style={i % 2 === 0 ? { backgroundColor: "rgb(181, 253, 233)" } : {}}>
                        <div className="col-5 col-md-5 p-1">
                            <div className="col-12 col-md-6 p-0">{d.voucherno}/{d.date}</div>
                            <div className="col-12 col-md-6 p-0">By {d.debitcredit.toUpperCase() + "NOTE / " + d.issuereceived}</div>
                            <div className="col-12 col-md-6 p-0">{d.businessname}</div>
                            <div className="col-12 col-md-6 p-0">{d.remarks}</div>
                        </div>
                        <div className="col-5 col-md-6 p-0 text-right">
                            <div className="col-12 col-md-2 p-0"><strong>{parseInt(d.basic) > 0 ? "Basic: " + parseFloat(d.basic).toFixed(2) : ''}</strong></div>
                            <div className="col-12 col-md-2 p-0">{parseInt(d.igst) > 0 ? "IGST: " + parseFloat(d.igst).toFixed(2) : ''}</div>
                            <div className="cl-12 col-md-2 p-0">{parseInt(d.cgst) > 0 ? "CGST: " + parseFloat(d.cgst).toFixed(2) : ''}</div>
                            <div className="col-12 col-md-2 p-0">{parseInt(d.sgst) > 0 ? "SGST: " + parseFloat(d.sgst).toFixed(2) : ''}</div>
                            <div className="col-12 col-md-2 p-0">{parseInt(d.total) > 0 ? "Tot: " + parseFloat(d.total).toFixed(2) : ''}</div>
                        </div>
                        <div className="col-2 col-md-1">
                            <div className="col-12 mt-1"></div>
                            <div className="col-12 mt-2"><FontAwesomeIcon icon={faTrash} onClick={() => cancel()} size="lg" /></div>
                        </div>
                    </div>
                </div>
                <div className="container d-none d-md-block small mb-1 p-1">
                    <div className="d-flex">
                        <div className="col-1 p-0">{d.date}</div>
                        <div className="col-1 p-0">{d.voucherno}</div>
                        <div className="col-4 p-0 text-right">
                            <strong><div className="col-12 col-md-6 p-0">By {d.debitcredit.toUpperCase() + "NOTE / " + d.issuereceived}</div></strong>
                            <div>{d.businessname}</div>
                            <div>{d.remarks}</div>
                        </div>
                        <div className="col-2 text-right">
                        </div>
                        <div className="col-3 text-right">
                            <div className="col-12 p-0"><strong>{"Basic: " + parseFloat(d.basic).toFixed(2)}</strong></div>
                            <div className="col-12 p-0">{parseInt(d.igst) > 0 ? "IGST: " + parseFloat(d.igst).toFixed(2) : ''}</div>
                            <div className="col-12 p-0">{parseInt(d.cgst) > 0 ? "CGST: " + parseFloat(d.cgst).toFixed(2) : ''}</div>
                            <div className="col-12 p-0">{parseInt(d.sgst) > 0 ? "SGST: " + parseFloat(d.sgst).toFixed(2) : ''}</div>
                            <div className="col-12 p-0">{parseInt(d.total) > 0 ? "Tot: " + parseFloat(d.total).toFixed(2) : ''}</div>
                        </div>
                        <div className="col-1 p-0 row">
                            <div className="col-4 mt-1"></div>
                            <div className="col-4 mt-1"><FontAwesomeIcon icon={faTrash} onClick={() => cancel()} size="lg" /></div>
                        </div>
                    </div>
                </div>
            </div>;
            returndata.push(temp);
            i++;
        }
    })

    if ((data.length === 0) && (debitcreditdata.length === 0)) {
        temp =
            <div className="container mt-2">
                <div className="card border">
                    <div className="text-center">No Purchase Data to Display</div>
                </div>
            </div>
        returndata.push(temp)
    }
    let t = <div className="container mt-2 small"><strong>Purchase : {totalpurchase}</strong></div>;
    let t1 = <div className="container small"><strong>Debit/Credit : {totalcredit}</strong></div>;
    let t2 = <div className="container  small"><strong>Net : {totalpurchase - totalcredit}</strong></div>;
    let t3 = <div className="container small mt-3 d-none d-md-block p-0">
        <div className="d-flex">
            <div className="col-1 p-0"><strong>Date</strong></div>
            <div className="col-1 p-0"><strong>Invoice No</strong></div>
            <div className="col-4 p-0"><strong>Particulars</strong></div>
            <div className="col-2 text-right"><div className="col-12"><strong>Debit</strong></div></div>
            <div className="col-3 text-right"><div className="col-12"><strong>Credit</strong></div></div>
        </div>
    </div>

    let t4 = <div>
        <div className="container small d-none d-md-block p-1 mt-1">
            <div className="d-flex">
                <div className="col-1 p-0"><strong></strong></div>
                <div className="col-1 p-0"><strong></strong></div>
                <div className="col-4 p-0"><strong></strong></div>
                <div className="col-2 border-top border-dark text-right">{parseFloat(totalpurchase).toFixed(2)}</div>
                <div className="col-3 border-top border-dark text-right">{parseFloat(totalcredit).toFixed(2)}</div>
            </div>
        </div>
        <div className="container small d-none d-md-block p-1">
            <div className="d-flex">
                <div className="col-1 p-0"><strong></strong></div>
                <div className="col-1 p-0"><strong></strong></div>
                <div className="col-4 p-0"><strong>{"Net Purchase"}</strong></div>
                <div className="col-2 border-top border-dark text-right">{parseFloat(totalpurchase - totalcredit).toFixed(2)}</div>
                <div className="col-3 text-right"></div>
            </div>
        </div>
    </div>

    let returndatatemp = [];
    returndatatemp.push(t);
    returndatatemp.push(t1);
    returndatatemp.push(t2);
    returndatatemp.push(t3);
    returndatatemp.push(returndata);
    returndatatemp.push(t4);
    return returndatatemp;
}
const DisplayPurchaseData = function ({ getItem, businessid, businessList, getVendor, purchaseDetail, state, handleOnChange, deleteEntry, fyid, getDebitCredit }) {
    return (
        <div>
            <LocalForm className="small">
                <div className="d-flex justify-content-center mt-4">
                    <div className="form-group mb-1 col-12 col-md-3">
                        <label htmlFor="monthSelection" className="mb-0 muted-text"><strong>Filter Data by Month:</strong></label>
                        <Control.select model=".monthSelection" className="form-control form-control-sm" id="monthSelection" name="monthSelection" value={state.monthSelection} onChange={handleOnChange}>
                            <Option list={purchaseDetail.data} scope="purchaseDetail" />
                        </Control.select>
                    </div>
                </div>
            </LocalForm>
            <DisplayData state={state} purchaseDetail={purchaseDetail} getVendor={getVendor} businessid={businessid} businessList={businessList} deleteEntry={deleteEntry} fyid={fyid} getDebitCredit={getDebitCredit} />
        </div>
    )
}
class PurchaseDetail extends Component {
    constructor(props) {
        super(props);
        this.state = ({
            vendorSelection: '',
            monthSelection: '',
            quarterSelection: ''
        })
        window.onload = () => {
            this.props.refreshVendorState();
            this.props.refreshItemState();
            this.props.resetPurchaseDetail();
            this.props.refreshDebitCredit();
        }
    }

    cancelOnClick = ({ invoiceid }) => {
        alert(invoiceid);
        this.props.cancelPurchase(this.props.businessid, this.props.fyid, invoiceid, "purchase");
    }
    handleOnChange = (event) => {
        let target = event.target;
        let name = target.name;
        let value = target.value;
        this.setState({
            ...this.state,
            [name]: value
        })
    }

    componentDidMount() {
        if (this.props.getItem.message === 'initial') this.props.getItemList(this.props.businessid);
        if ((this.props.getVendor.message === 'initial') && (this.props.getVendor.loading === false)) this.props.getVendorList(this.props.businessid);
        if ((this.props.purchaseDetail.message === 'initial') && (this.props.purchaseDetail.loading === false)) this.props.getPurchaseData(this.props.businessid, this.props.fyid);
        if (this.props.getDebitCredit.message === 'initial') this.props.getDebitCreditList(this.props.businessid, this.props.fyid);
        if (this.props.deleteEntryMessage.message === "success") {
            alert("Entry Deleted Successfully");
            this.props.resetDeleteEntry();
        }
        if (this.props.deleteEntryMessage.error !== '') {
            alert("Error Deleting Entry - Please Try Again");
            this.props.resetDeleteEntry();
        }
        if (this.props.logoutMessage.error !== '') {
            alert("Error Logging Out. Please Try Again");
            this.props.resetLogoutMessage();
        }
    }
    genereateSalesPdf = async (business, fy) => {
        try {
            let data = this.props.purchaseDetail.data;
            let debitcreditdata = this.props.getDebitCredit.data.filter(d => ((d.issuereceived === 'issue' && d.debitcredit === 'debit') || (d.issuereceived === 'received' && d.debitcredit === 'credit')));
            if (this.state.monthSelection !== '') {
                data = data.filter(d => d.invoicedate.split(' ')[0] === this.state.monthSelection);
                debitcreditdata = debitcreditdata.filter(d => d.date.split(' ')[0] === this.state.monthSelection);
            }
            const doc = <PurchaseLedger data={data} debitcreditdata={debitcreditdata} business={business} fy={fy} month={this.state.monthSelection} getVendor={this.props.getVendor.data} />;
            const blobPdf = pdf(doc);
            blobPdf.updateContainer(doc);
            const result = await blobPdf.toBlob();
            let filename = "PurchaseLedger.pdf";
            saveAs(result, filename);
        }
        catch (err) {
            alert(err);
        }
    }
    render() {
        if (!cookie.load('userservice')) return <Redirect to="/" />;
        else {
            if ((this.props.purchaseDetail.loading === true) || (this.props.getVendor.loading === true) || (this.props.getItem.loading === true) || (this.props.logoutMessage.loading === true) || (this.props.deleteEntryMessage.loading === true) || (this.props.getDebitCredit.loading === true)) {
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div className="mt-3 d-flex justify-content-center">
                            <button className="btn btn-danger">Loading... </button>
                        </div>
                    </div>
                );
            }
            else if ((this.props.purchaseDetail.error !== '') || (this.props.getVendor.error !== '') || (this.props.getItem.error !== '') || (this.props.getDebitCredit.error !== '')) {
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div className="mt-3 d-flex justify-content-center">
                            <button className="btn btn-danger">Error.. Please Refresh and Try Agin </button>
                        </div>
                    </div>
                );
            }
            else {
                let business = this.props.businessList.data.filter(b => b.businessid === this.props.businessid)[0];
                let fy = business.financial.filter(f => f.fyid === this.props.fyid)[0].financialyear;
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div style={{ minHeight: "93vh", display: "flex", flexDirection: "column" }}>
                            <div className="container" style={{ flex: "1" }}>
                                <h6 className="text-center pl-2 pr-2 pt-4" style={{ lineHeight: "1" }}><strong>{business.businessname} {fy}</strong></h6>
                                <h6 className="text-center pl-2 pr-2 mb-4" style={{ lineHeight: "1" }}>{business.alias ? "(" + business.alias + ")" : ''}</h6>
                                <h6 className="text-center pl-2 pr-2"><strong><u>Purchase Ledger</u></strong>&nbsp;&nbsp;&nbsp;<FontAwesomeIcon icon={faFilePdf} size="lg" onClick={() => this.genereateSalesPdf(business, fy)} />&nbsp;&nbsp;&nbsp;<FontAwesomeIcon icon={faFileExcel} size="lg" /></h6>
                                < DisplayPurchaseData getItem={this.props.getItem} businessid={this.props.businessid} businessList={this.props.businessList} getVendor={this.props.getVendor} purchaseDetail={this.props.purchaseDetail} state={this.state} handleOnChange={this.handleOnChange} deleteEntry={this.props.deleteEntry} fyid={this.props.fyid} getDebitCredit={this.props.getDebitCredit} />
                            </div>
                            <Footer />
                        </div>
                    </div>
                )
            }
        }
    }
}

export default PurchaseDetail;