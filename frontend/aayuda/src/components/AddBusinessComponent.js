import React, { Component } from 'react';
import { LocalForm, Errors, Control } from 'react-redux-form';
import cookie from 'react-cookie';
import { Redirect } from 'react-router-dom'
import { Header } from './HeaderComponent';
import { Footer } from './Footer';

const AddBusinessFormComponent = function ({ handleSubmit, resetForm, state, handleInputChange }) {
    return (
        <div className="col-12">
            <h6 className="mt-3 text-center">
                <strong><u>Add a New Business</u></strong>
            </h6>
            <LocalForm className="small" onSubmit={(() => handleSubmit())}>
                <div className="row justify-content-center mt-3">
                    <div className="form-group mb-1 col-md-10">
                        <label className="col-6 col-md-3 p-0" ><input type="radio" name="businesstype" id="businesstype" value="propritorship" checked={state.businesstype === "propritorship" ? true : false} onChange={handleInputChange} />Propritorship</label>&nbsp;&nbsp;&nbsp;&nbsp;
                                                <label className="col-6 col-md-3 p-0"><input type="radio" name="businesstype" id="businesstype" value="partnership" checked={state.businesstype === "partnership" ? true : false} onChange={handleInputChange} />Partnership</label>
                        <label className="col-6 col-md-4 p-0"><input type="radio" name="businesstype" id="businesstype" value="ltd" checked={state.businesstype === "ltd" ? true : false} onChange={handleInputChange} /> Pvt Ltd / Ltd</label>&nbsp;&nbsp;&nbsp;&nbsp;
                    </div>
                    <div className="form-group col-md-10 mb-1">
                        <label htmlFor="businessname" className="mb-0"><span className="muted-text">Business Name<span className="text-danger">*</span></span></label>
                        <div className="">
                            <Control.text model=".businessname" className="form-control form-control-sm pt-3 pb-3" id="businessname" name="businessname" placeholder="Business Name" value={state.businessname} onChange={handleInputChange} />
                        </div>
                    </div>
                    <div className="form-group col-md-10 mb-1">
                        <label htmlFor="alias" className="mb-0"><span className="muted-text">Alias</span></label>
                        <div className="">
                            <Control.text model=".alias" className="form-control form-control-sm pt-3 pb-3" id="alias" name="alias" placeholder="Alias" value={state.alias} onChange={handleInputChange} />
                        </div>
                    </div>
                    <div className="form-group col-md-10 mb-1">
                        <label htmlFor="gstin" className="mb-0 muted-text">Gstin<span className="text-danger">*</span></label>
                        <div className="">
                            <Control.text model=".gstin" className="form-control form-control-sm pt-3 pb-3" id="gstin" name="gstin" placeholder="GSTIN" value={state.gstin} onChange={handleInputChange} />
                        </div>
                    </div>
                    <div className="form-group col-md-10 mb-1">
                        <label htmlFor="pan" className="mb-0 muted-text">Pan No</label>
                        <div className="">
                            <Control.text model=".pan" className="form-control form-control-sm pt-3 pb-3" id="pan" name="pan" placeholder="Pan No" value={state.pan} onChange={handleInputChange} />
                        </div>
                    </div>
                    <div className="form-group col-md-10 mb-1 mt-1">
                        <label htmlFor="state" className="mb-0 muted-text">State<span className="text-danger">*</span></label>
                        <div className="">
                            <Control.text model=".state" className="form-control form-control-sm pt-3 pb-3" id="state" name="state" placeholder="State" value={state.state} onChange={handleInputChange} />
                        </div>
                    </div>
                    <div className="form-group col-md-10 mb-1">
                        <label htmlFor="address" className="mb-0 muted-text">Address <span className="text-danger">*</span></label>
                        <div className="">
                            <Control.text model=".address" className="form-control form-control-sm pt-3 pb-3" id="address" name="address" placeholder="Address" value={state.address} onChange={handleInputChange} />
                        </div>
                    </div>
                    <div className="form-group col-md-10 mb-1">
                        <label htmlFor="pincode" className="mb-0 muted-text">Pincode</label>
                        <div className="">
                            <Control.text model=".pincode" className="form-control form-control-sm pt-3 pb-3" id="pincode" name="pincode" placeholder="Pincode" value={state.pincode} onChange={handleInputChange} />
                        </div>
                    </div>
                    <div className="form-group col-md-10 mb-1">
                        <label htmlFor="mobile" className="mb-0 muted-text">Mobile Number</label>
                        <div className="">
                            <Control.text model=".mobile" className="form-control form-control-sm pt-3 pb-3" id="mobile" name="mobile" placeholder="Mobile Number" value={state.mobile} onChange={handleInputChange} />
                        </div>
                    </div>
                    <div className="form-group col-md-10 mb-1">
                        <label htmlFor="landline" className="mb-0 muted-text">LandLine Number</label>
                        <div className="">
                            <Control.text model=".landline" className="form-control form-control-sm pt-3 pb-3" id="landline" name="landline" placeholder="LandLine eg: 01202571836" value={state.landline} onChange={handleInputChange} />
                        </div>
                    </div>
                    <div className="form-group col-md-10">
                        <label htmlFor="email" className="mb-0 muted-text">Email</label>
                        <div className="">
                            <Control type="Email" model=".email" className="form-control form-control-sm pt-3 pb-3" id="email" name="email" placeholder="Email" value={state.email} onChange={handleInputChange} />
                        </div>
                    </div>
                    {state.businesstype === 'ltd' ? <div className="form-group col-md-10 mb-1">
                        <label htmlFor="noofshares" className="mb-0 muted-text">No Of Shares Issued<span className="text-danger">*</span></label>
                        <div className="">
                            <Control.text model=".noofshares" className="form-control form-control-sm pt-3 pb-3" id="noofshares" name="noofshares" placeholder="No Of Shares Issued" value={state.noofshares} onChange={handleInputChange} />
                        </div>
                    </div> : ''}
                    {state.businesstype === 'ltd' ? <div className="form-group col-md-10 mb-1">
                        <label htmlFor="fv" className="mb-0 muted-text">Face Value of Each Share<span className="text-danger">*</span></label>
                        <div className="">
                            <Control.text model=".fv" className="form-control form-control-sm pt-3 pb-3" id="fv" name="fv" placeholder="FV of Each Share Issued" value={state.fv} onChange={handleInputChange} />
                        </div>
                    </div> : ''}
                    <div className="col-md-10 mt-1"><button type="submit" className="btn btn-success btn-sm" >Add</button></div>
                </div>
            </LocalForm>
        </div>
    )
}

class AddBusiness extends Component {
    constructor(props) {
        super(props);
        this.state = ({
            businesstype: '',
            businessname: '',
            alias: '',
            gstin: '',
            stateut: 'state',
            state: '',
            address: '',
            pincode: '',
            mobile: '',
            landline: '',
            email: '',
            pan: '',
            noofshares: 0,
            fv: 0
        })
    }
    componentDidMount() {
        if ((this.props.addBusinessMessage.message === 'success')) {
            alert("Business Added Successfully.");
            this.props.resetAddBusinessMessage();
            window.location.replace("/");
        }
        else if (this.props.addBusinessMessage.message === 'Already Registered') {
            alert("Business for GSTIN: " + this.props.businessList.gstin + " Already Registered");
            this.setState({
                ...this.props.addBusinessMessage.state
            })
            this.props.resetAddBusinessMessage();
        }
        if (this.props.addBusinessMessage.error !== '') {
            alert(this.props.addBusinessMessage.error + " - Please Try Again");
            this.setState({
                ...this.props.addBusinessMessage.state
            })
            this.props.resetAddBusinessMessage();
        }
        if (this.props.logoutMessage.error !== '') {
            alert("Error Logging Out. Please Try Again");
            this.props.resetLogoutMessage();
        }
    }
    handleInputChange = (event) => {
        let target = event.target;
        let name = target.name;
        let value = target.value;
        if ((name === 'businesstype') && ((value === 'partnership') || (value === 'propritorship'))) {
            this.setState({
                ...this.state,
                [name]: value,
                noofshares: 0,
                fv: 0
            })
        }
        else this.setState({
            ...this.state,
            [name]: value
        })
    }
    resetForm = () => {
        this.props.resetAddBusinessForm();
    }

    handleSubmit = () => {
        if (this.state.businesstype === '') alert("Please Select Business Type");
        else if (this.state.businessname === '') alert("Business Name Is Mandatory");
        else if (this.state.businessname.length > 100) alert("Business Name Must Be Within 100 Characters");
        else if (this.state.alias.length > 100) alert("Alias Must Be Within 100 Characters");
        else if (this.state.gstin === '') alert("GSTIN Is Mandatory");
        else if (this.state.gstin.length !== 15) alert("GSTIN ust Be 15 Characters In Length");
        else if (this.state.pan.length > 10) alert("Pan No Must Be Within 10 Characters");
        else if (this.state.pincode.length > 10) alert("PinCode Must Be Within 10 Characters");
        else if (this.state.state === '') alert("State Name Is Mandatory");
        else if (this.state.state.length > 25) alert("State Name Must Be Within 25 Characters");
        else if (this.state.mobile.length > 20) alert("Mobile Number Must Be Within 20 Characters");
        else if (this.state.email.length > 50) alert("Email Must Be Within 150 Characters");
        else if (this.state.address === '') alert("Address Is Mandatory");
        else if (this.state.address.length > 300) alert("Address Must Be Within 300 Characters");
        else if (this.state.landline.length > 20) alert("landLine Must Be Within 20 Characters");
        else if (this.state.businesstype === 'ltd') {
            if (this.state.noofshares === '') alert("No Of Shares is Mandatory");
            else if (isNaN(this.state.noofshares)) alert("No Of Shares Must Be A Number");
            else if (this.state.fv === '') alert("Face Value is Mandatory");
            else if (isNaN(this.state.fv)) alert("Face Value Must Be A Number");
            else this.props.postAddBusiness(this.state.businessname, this.state.alias, this.state.gstin, this.state.state, this.state.address, this.state.pincode, this.state.mobile, this.state.landline, this.state.email, this.state.stateut, this.state.pan, this.state.businesstype, this.state.noofshares, this.state.fv);
        }
        else this.props.postAddBusiness(this.state.businessname, this.state.alias, this.state.gstin, this.state.state, this.state.address, this.state.pincode, this.state.mobile, this.state.landline, this.state.email, this.state.stateut, this.state.pan, this.state.businesstype, this.state.noofshares, this.state.fv);
    }

    render() {
        if (!cookie.load('userservice')) return <Redirect to="/" />
        else {
            if ((this.props.addBusinessMessage.loading === true) || (this.props.logoutMessage.loading === true)) {
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div className="mt-3 d-flex justify-content-center">
                            <button className="btn btn-danger">Loading... </button>
                        </div>
                    </div>
                );
            }
            else {
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div style={{ minHeight: "93vh", display: "flex", flexDirection: "column" }}>
                            <div className="container d-flex justify-content-center pr-0 pl-0" style={{ flex: "1" }}>
                                <div className="col-12 col-md-6">
                                    <div className="pr-0 pl-0">
                                        <AddBusinessFormComponent handleSubmit={this.handleSubmit} resetForm={this.resetForm} state={this.state} handleInputChange={this.handleInputChange} />
                                    </div>
                                </div>
                            </div>
                            <Footer />
                        </div>
                    </div>
                )
            }
        }
    }
}

export default AddBusiness;