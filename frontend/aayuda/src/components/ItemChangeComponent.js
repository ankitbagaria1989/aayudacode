import React, { Component } from 'react';
import { Form, Errors, Control } from 'react-redux-form';
import cookie from 'react-cookie';
import { Redirect } from 'react-router-dom';
import { Header } from './HeaderComponent';
import { Footer } from './Footer';

const required = (val) => val && val.length;
const maxLength = (len) => val => val ? val.length < len : true;
const isNumber = (val) => !isNaN(parseInt(val));
const length = (len) => val => (val) ? val.length === len : true;

const ErrorComponent = function ({ children }) {
    return (
        <div className="pb-1 small">{children}</div>
    );
}

const FormElement = function ({ handleSubmit, taxtype }) {
    return (
        <Form model="itemChange" onSubmit={((values) => handleSubmit(values))}>
            <div className="justify-content-center mt-4">
                <div className="form-group col-12 mb-1">
                    <label htmlFor="itemname" className="mb-0"><span>Item Name</span><span className="text-danger">*</span></label>
                    <div className="">
                        <Control.text model=".itemname" className="form-control form-control-sm" id="itemname" name="itemname" placeholder="Item Name" validators={{ required, maxLength: maxLength(100) }} />
                        <Errors className="text-danger" model=".itemname" show="touched" messages={{
                            required: "Bank Name is Mandatory to Register",
                            maxLength: "length must be within 100 characters"
                        }}
                            component={ErrorComponent}
                        />
                    </div>
                </div>
                <div className="form-group col-12 mb-1">
                    <label htmlFor="alias" className="mb-0"><span>Alias</span></label>
                    <div className="">
                        <Control.text model=".alias" className="form-control form-control-sm" id="alias" name="alias" placeholder="Alias" validators={{ maxLength: maxLength(100) }} />
                        <Errors className="text-danger" model=".alias" show="touched" messages={{
                            maxLength: "length must be within 100 characters"
                        }}
                            component={ErrorComponent}
                        />
                    </div>
                </div>
                <div className="form-group col-12 mb-1">
                    <label className="mb-0" htmlFor="hsncode">HSN Code</label>
                    <div className="">
                        <Control.text model=".hsncode" className="form-control form-control-sm" id="hsncode" name="hsncode" placeholder="HSN Code" validators={{ isNumber, maxLength: length(8) }} />
                        <Errors className="text-danger" model=".hsncode" show="touched" messages={{
                            isNumber: "Only numbers allowed ",
                            maxLength: "length must be within 8 characters"
                        }}
                            component={ErrorComponent}
                        />
                    </div>
                </div>
                <div className="form-group col-12 mb-1">
                    <label className="mb-0" htmlFor="gstrate">GST Rate (Eg: 18 for 18%)</label>
                    <div className="">
                        <Control.text model=".gstrate" className="form-control form-control-sm" id="gstrate" name="gstrate" placeholder="GST Rate" validators={{ isNumber, maxLength: maxLength(3) }} />
                        <Errors className="text-danger" model=".gstrate" show="touched" messages={{
                            isNumber: "Only numbers allowed ",
                            maxLength: "length must 2 digits or less"
                        }}
                            component={ErrorComponent}
                        />
                    </div>
                </div>
                <div className="form-group col-12 mb-1">
                    <label className="mb-0" htmlFor="billingunit">Billing Unit</label>
                    <div className="">
                        <Control.text model=".billingunit" className="form-control form-control-sm" id="billingunit" name="billingunit" placeholder="eg: 1000 pcs OR kg or MT etc..etc.." validators={{ maxLength: maxLength(20) }} />
                        <Errors className="text-danger" model=".billingunit" show="touched" messages={{
                            maxLength: "length must be with in 20 characters"
                        }}
                            component={ErrorComponent}
                        />
                    </div>
                    <div className="mt-3"><button type="submit" className="btn btn-success btn-sm pl-4 pr-4" >Update</button></div>
                </div>
            </div>
        </Form>
    );
}

class ItemChange extends Component {
    constructor(props) {
        super(props);

        this.state = {
            item: this.props.getItem.data.filter((item) => {
                if ((item.businessid === this.props.businessid) && (item.itemid === this.props.itemid)) return item;
            })[0]
        };
        if ((this.state.item !== undefined) && (this.props.updateItemMessage.error === '') && (this.props.updateItemMessage.loading === false)) this.props.setDefaultItemChange(this.state.item);
        if (this.state.item === undefined) {
            this.state = {
                item: {
                    taxtype: "gst"
                }
            }
        }
    }

    componentDidMount() {
        if (this.props.updateItemMessage.error !== '') {
            alert(this.props.updateItemMessage.error + 'Please try again');
            this.props.resetItemChange();
        }
        if (this.props.updateItemMessage.message === 'success') {
            alert('Item Detail Update Successfully');
            this.props.resetItemChange();
        }
        if (this.props.logoutMessage.error !== '') {
            alert("Error Logging Out. Please Try Again");
            this.props.resetLogoutMessage();
        }
    }


    handleSubmit = (values) => {
        this.props.updateItem(this.state.item.businessid, this.state.item.itemid, values);
    }

    render() {
        if (!cookie.load('userservice')) return <Redirect to="/" />;
        else {
            if ((this.props.logoutMessage.loading === true) || (this.props.updateItemMessage.loading === true)) {
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div className="mt-3 d-flex justify-content-center">
                            <button className="btn btn-danger">Loading... </button>
                        </div>
                    </div>
                );
            }

            else {
                let business = this.props.businessList.data.filter(b => b.businessid === this.props.businessid)[0];
                return (
                    <div >
                        <Header logout={this.props.logout} />
                        <div style={{ minHeight: "93vh", display: "flex", flexDirection: "column" }}>
                            <div className="container small d-flex justify-content-center pr-0 pl-0" style={{ flex: "1" }}>
                                <div className="col-12 col-md-6 pb-4">
                                    <div className="pr-0 pl-0">
                                        <h6 className="text-center pl-2 pr-2 pt-4" style={{ lineHeight: "1" }}><strong>{business.businessname}</strong></h6>
                                        <h6 className="text-center pl-2 pr-2 mb-4" style={{ lineHeight: "1" }}>{business.alias ? "(" + business.alias + ")" : ''}</h6>
                                        <h6 className="mt-2 text-center">
                                            <strong><u>Update Item Detail</u></strong>
                                        </h6>
                                        <FormElement handleSubmit={this.handleSubmit} taxtype={this.state.item.taxtype} />
                                    </div>
                                </div>
                            </div>
                            <Footer />
                        </div>
                    </div>
                );
            }
        }
    };
}

export default ItemChange;