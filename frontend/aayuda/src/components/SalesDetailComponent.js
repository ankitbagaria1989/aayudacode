import React, { Component } from 'react';
import cookie from 'react-cookie';
import { Redirect, Link } from 'react-router-dom';
import { LocalForm, Control } from 'react-redux-form';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTimesCircle, faDownload, faCheckCircle, faEdit, faTrash, faFilePdf, faFileExcel } from '@fortawesome/free-solid-svg-icons';
import { Page, Text, View, Document, pdf } from '@react-pdf/renderer';
import { pdfstyles } from '../sylesheet/stylesheet';
import { saveAs } from 'file-saver';
import { Header } from './HeaderComponent';
import { Footer } from './Footer';
const numWords = require('num-words');
const Option = function ({ list, scope }) {
    let length = list.length;
    let arr = [];
    if (scope === "customer") {
        for (let i = -1; i < length; i++) {
            if (i === -1) arr.push(<option value=''>Select Customer Filter :</option>);
            else arr.push(<option value={list[i].customerid}>{list[i].businessname}</option>);
        }
    }
    else if (scope === "salesDetail") {
        for (let i = -1; i < 12; i++) {
            if (i === -1) arr.push(<option value=''>Select Month Filter :</option>);
            else if (i === 0) arr.push(<option value="Jan">Jan</option>);
            else if (i === 1) arr.push(<option value="Feb">Feb</option>);
            else if (i === 2) arr.push(<option value="Mar">Mar</option>);
            else if (i === 3) arr.push(<option value="Apr">Apr</option>);
            else if (i === 4) arr.push(<option value="May">May</option>);
            else if (i === 5) arr.push(<option value="Jun">Jun</option>);
            else if (i === 6) arr.push(<option value="Jul">Jul</option>);
            else if (i === 7) arr.push(<option value="Aug">Aug</option>);
            else if (i === 8) arr.push(<option value="Sep">Sep</option>);
            else if (i === 9) arr.push(<option value="Oct">Oct</option>);
            else if (i === 10) arr.push(<option value="NoV">Nov</option>);
            else if (i === 11) arr.push(<option value="Dec">Dec</option>);
        }
    }
    else if (scope === 'quarter') {
        for (let i = -1; i < 4; i++) {
            if (i === -1) arr.push(<option value=''>Select Quarter Filter :</option>);
            else if (i === 0) arr.push(<option value="1">1st Quarter</option>);
            else if (i === 1) arr.push(<option value="2">2nd Quarter</option>);
            else if (i === 2) arr.push(<option value="3">3rd Quarter</option>);
            else if (i === 3) arr.push(<option value="4">4th Quarter</option>);
        }
    }
    return arr;
}

let totalcredit = 0, totaldebit = 0;
const SalesDataPage = function ({ data, y }) {
    let debit = 0, credit = 0;
    let returnrows = [];
    if (y > 1) {
        returnrows.push(
            <View style={pdfstyles.itemSectionLedger, { textAlign: "center" }}>
                <View style={pdfstyles.itemHeader}><Text style={{ width: '75px', textAlign: "left" }}></Text><Text style={{ width: '75px', textAlign: "left" }}></Text><Text style={{ width: '225px', textAlign: "left", fontFamily: "Helvetica_bold" }}>...Carry Forward</Text><Text style={{ width: '100px', textAlign: "right" }}>{parseFloat(totaldebit).toFixed(2)}</Text><Text style={{ width: '100px', textAlign: "right" }}>{parseFloat(totalcredit).toFixed(2)}</Text></View>
            </View>
        )
        debit = totaldebit;
        credit = totalcredit;
    }
    for (let i = (y - 1) * 50; i < y * 50 && i < data.length; i++) {
        if (data[i].type === 'sales') {
            let d = data[i];
            credit = credit + parseFloat(d.totalbasic);
            totalcredit = totalcredit + parseFloat(d.totalbasic);
            returnrows.push(
                <View style={pdfstyles.itemSectionLedger, { textAlign: "center" }}>
                    <View style={pdfstyles.itemHeader}><Text style={{ width: '75px', textAlign: "left" }}>{d.invoicedate}</Text><Text style={{ width: '75px', textAlign: "left" }}>{d.invoicenumber}</Text><Text style={{ width: '225px', textAlign: "left", fontFamily: "Helvetica-Bold" }}>By {d.customername}</Text><Text style={{ width: '100px', textAlign: "right" }}></Text><Text style={{ width: '100px', textAlign: "right" }}>{parseFloat(d.totalbasic).toFixed(2)}</Text></View>
                </View>
            )
        }
        else {
            let d = data[i];
            debit = debit + parseFloat(d.basic);
            totaldebit = totaldebit + parseFloat(d.basic);
            returnrows.push(
                <View style={pdfstyles.itemSectionLedger, { textAlign: "center" }}>
                    <View style={pdfstyles.itemHeader}><Text style={{ width: '75px', textAlign: "left" }}>{d.date}</Text><Text style={{ width: '75px', textAlign: "left" }}>{d.voucherno}</Text><Text style={{ width: '225px', textAlign: "left", fontFamily: "Helvetica-Bold" }}>To Credit Note</Text><Text style={{ width: '100px', textAlign: "right" }}>{parseFloat(d.basic).toFixed(2)}</Text><Text style={{ width: '100px', textAlign: "right" }}></Text></View>
                </View>
            )
        }
    }
    if (y === Math.ceil(data.length / 50)) {
        returnrows.push(<View><View style={pdfstyles.itemSectionLedger, { marginTop: "5px", paddingBottom: "2px" }}>
            <View style={pdfstyles.itemHeader}><Text style={{ width: '75px', textAlign: "left" }}></Text><Text style={{ width: '75px', textAlign: "left" }}></Text><Text style={{ width: '225px', textAlign: "left" }}></Text><Text style={{ width: '100px', borderTop: "1px solid black", textAlign: "right" }}>{parseFloat(totaldebit).toFixed(2)}</Text><Text style={{ width: '100px', borderTop: "1px solid black", textAlign: "right" }}>{parseFloat(totalcredit).toFixed(2)}</Text></View>
        </View>
            <View style={pdfstyles.itemSectionLedger, { fontFamily: "Helvetica-Bold", marginTop: "5px", paddingBottom: "2px", borderTop: "1px solid black", borderBottom: "1px solid black" }}>
                <View style={pdfstyles.itemHeader}><Text style={{ width: '75px', textAlign: "left" }}></Text><Text style={{ width: '75px', textAlign: "left" }}></Text><Text style={{ width: '225px', textAlign: "left" }}>Net Sales</Text><Text style={{ width: '100px', textAlign: "right" }}>{totaldebit > totalcredit ? parseFloat(totaldebit - totalcredit).toFixed(2) : ''}</Text><Text style={{ width: '100px', textAlign: "right" }}>{totalcredit >= totaldebit ? parseFloat(totalcredit - totaldebit).toFixed(2) : ''}</Text></View>
            </View></View>)
    }
    else {
        returnrows.push(<View><View style={pdfstyles.itemSectionLedger, { fontFamily: "Helvetica-Bold", marginTop: "2px" }}>
            <View style={pdfstyles.itemHeader}><Text style={{ width: '75px', textAlign: "left" }}></Text><Text style={{ width: '75px', textAlign: "left" }}></Text><Text style={{ width: '225px', textAlign: "left" }}></Text><Text style={{ width: '100px', borderTop: "1px solid black", textAlign: "left" }}>{debit}</Text><Text style={{ width: '100px', borderTop: "1px solid black", textAlign: "left" }}>{credit}</Text></View>
        </View>
            <View style={pdfstyles.itemSectionLedger, { fontFamily: "Helvetica", marginTop: "5px", textAlign: "right" }}><Text></Text>...Continued</View></View>)
    }
    return returnrows;
}
const Report = function ({ business, data, l, n, period }) {
    let returnrows = [];
    for (let y = 1; y <= n; y++) {
        returnrows.push(<Page size="A4" style={pdfstyles.page}>
            {y === 1 ? <View><View style={pdfstyles.taxInvoiceHeader, { marginTop: "20px", textAlign: "center" }}>
                <View style={{ fontFamily: "Helvetica-Bold", width: "500px" }}><Text>{business.businessname}</Text></View>
                <View style={{ width: "500px" }}><Text>{business.address}</Text></View>
            </View>
                <View style={pdfstyles.taxInvoiceHeader, { marginTop: "10px", textAlign: "center" }}>
                    <View style={{ fontFamily: "Helvetica-Bold", textDecoration: "underline", width: "500px" }}><Text>Sales Ledger</Text></View>
                    <View style={{ width: "500px" }}><Text>{period}</Text></View>
                </View></View> : ''}
            <View style={pdfstyles.itemSectionLedger, { marginTop: "20px", marginBottom: "5px", fontFamily: "Helvetica-Bold", textAlign: "center", borderTop: "1px solid black", borderBottom: "1px solid black" }}>
                <View style={pdfstyles.itemHeader}><Text style={{ width: '75px', textAlign: "left" }}>Date</Text><Text style={{ width: '75px', textAlign: "left" }}>Voucher No</Text><Text style={{ width: '225px', textAlign: "left" }}>Particulars</Text><Text style={{ width: '100px', textAlign: "right" }}>Debit</Text><Text style={{ width: '100px', textAlign: "right" }}>Credit</Text></View>
            </View>
            <SalesDataPage data={data} y={y} />
        </Page >)
    }
    return returnrows;
}
const SalesLedger = function ({ data, debitcreditdata, business, fy, month }) {
    totalcredit = 0;
    totaldebit = 0;
    let period = fy;
    data.concat(debitcreditdata);
    let l = data.length, i = 0, n = Math.ceil(l / 50), y = 0;
    if (month) {
        if (month.toLowerCase() === 'jan' || month.toLowerCase() === 'feb' || month.toLowerCase() === 'mar') {
            period = month.toUpperCase() + " " + fy.split("-")[1];
        }
        else period = month.toUpperCase() + " " + fy.split("-")[0];
    }
    return (
        <Document>
            <Report business={business} data={data} l={l} n={n} period={period} />
        </Document>
    )
}

const TaxRowPdf = function ({ text, rate, amount, margin }) {
    return (
        <View style={pdfstyles.itemDetailTax}><Text style={{ width: '30px', marginTop: margin }}></Text><Text style={{ width: '220px', textAlign: "right", paddingRight: "5px" }}>{text + " " + rate + "%"}</Text><Text style={{ width: '60px', textAlign: "center" }}></Text><Text style={{ width: '50px', textAlign: "center" }}></Text><Text style={{ width: '60px', textAlign: "center" }}></Text><Text style={{ width: '80px', textAlign: "center", fontFamily: "Helvetica-Oblique", }}>{amount}</Text></View>
    );
}

const TaxRowPdfHandler = function ({ it }) {
    let rows = []
    if (it.igst !== '') rows.push(<TaxRowPdf text="Output IGST @" rate={JSON.parse(it.name).gstrate} amount={parseFloat(it.igst).toFixed(2)} margin="2px" />);
    if (it.cgst !== '') rows.push(<TaxRowPdf text="Output CGST @" rate={(JSON.parse(it.name).gstrate) / 2} amount={parseFloat(it.cgst).toFixed(2)} margin="2px" />);
    if (it.sgst !== '') rows.push(<TaxRowPdf text="Output SGST @" rate={(JSON.parse(it.name).gstrate) / 2} amount={parseFloat(it.sgst).toFixed(2)} margin="" />);
    return rows;
}
const StateRow = function ({ state, gstin }) {
    if ((state !== '') && (gstin !== '') && (state !== undefined) && (gstin !== undefined))
        return (
            <View style={{ display: 'flex', flexDirection: 'row' }}><Text style={pdfstyles.grey1}>State : </Text><Text>{state}</Text><Text style={pdfstyles.grey1}> &nbsp;&nbsp;, Code: </Text><Text>{gstin.substr(0, 2)}</Text></View>
        );
    else return <View></View>;
}

const GstinRow = function ({ gstin }) {
    if ((gstin !== '') && (gstin !== undefined)) return <View style={{ display: 'flex', flexDirection: 'row' }}><Text style={pdfstyles.grey1}>GSTIN : </Text><Text>{gstin}</Text></View>
    else return <View></View>;
}

const TaxElaborateRow = function ({ data, totaltaxable }) {
    let totaligst = parseFloat(data.totaligst);
    let totalcgst = parseFloat(data.totalcgst);
    let totalsgst = parseFloat(data.totalsgst);
    if ((totaligst + totalcgst + totalsgst) > 0) {
        return (
            <View style={{ width: "500px" }}>
                <View style={{ borderBottom: "1px solid black", borderLeft: "1px solid black", borderRight: "1px solid black", display: "flex", flexDirection: "row" }}>
                    <View style={{ width: "100px", borderRight: "1px solid black", textAlign: "center" }}><Text style={{ padding: "2px" }}>Taxable Amount</Text></View>
                    <View style={{ width: "100px", borderRight: "1px solid black", textAlign: "center" }}><Text style={{ padding: "2px" }}>Total IGST</Text></View>
                    <View style={{ width: "100px", borderRight: "1px solid black", textAlign: "center" }}><Text style={{ padding: "2px" }}>Total CGST</Text></View>
                    <View style={{ width: "100px", borderRight: "1px solid black", textAlign: "center" }}><Text style={{ padding: "2px" }}>Total SGST</Text></View>
                    <View style={{ width: "100px", textAlign: "center" }}><Text style={{ padding: "2px" }}>Grand Total (INR)</Text></View>
                </View>
                <View style={{ borderBottom: "1px solid black", borderLeft: "1px solid black", borderRight: "1px solid black", display: "flex", flexDirection: "row", fontFamily: "Helvetica-Bold" }}>
                    <View style={{ width: "100px", borderRight: "1px solid black", textAlign: "center" }}><Text style={{ padding: "2px" }}>{parseFloat(totaltaxable).toFixed(2)}</Text></View>
                    <View style={{ width: "100px", borderRight: "1px solid black", textAlign: "center" }}><Text style={{ padding: "2px" }}>{totaligst.toFixed(2)}</Text></View>
                    <View style={{ width: "100px", borderRight: "1px solid black", textAlign: "center" }}><Text style={{ padding: "2px" }}>{totalcgst.toFixed(2)}</Text></View>
                    <View style={{ width: "100px", borderRight: "1px solid black", textAlign: "center" }}><Text style={{ padding: "2px" }}>{totalsgst.toFixed(2)}</Text></View>
                    <View style={{ width: "100px", textAlign: "center", display: "flex", flexDirection: "row" }}><Text style={{ padding: "2px" }}>{parseFloat(data.grandtotal).toFixed(2)}</Text></View>
                </View>
            </View>
        )
    }
    else return <View></View>;
}

const Terms = function ({ terms }) {
    let term = terms.split("\n");
    if (terms.length > 0) {
        return (
            term.map(t => <Text>{t}</Text>)
        );
    }
    else return <View></View>
}
const PageInvoice = function ({ data, fy, customer, shipped, business, message }) {
    let totaltaxable = 0;
    let wholePart = numWords(parseInt(data.grandtotal));
    let decimalPart = parseFloat(data.grandtotal) - wholePart;
    decimalPart = decimalPart > 0 ? " & " + numWords(decimalPart) + " paisa Only" : ' Only'
    let grandinWords = wholePart + decimalPart;
    let item = JSON.parse(data.itemobject);
    let keys = item !== '' ? Object.keys(item) : '';
    let billingunit = '';
    if (keys.length > 0) billingunit = JSON.parse(item[0].name).billingunit;
    return (
        <Page size="A4" style={pdfstyles.page}>
            <View style={pdfstyles.taxInvoiceHeader}>
                <View style={pdfstyles.taxInvoiceSection}><Text>Tax Invoice</Text></View>
                <View style={pdfstyles.typeSection}><Text>({message})</Text></View>
            </View>
            <View style={pdfstyles.businessDetailSection}>
                <View style={pdfstyles.businessName}><Text style={{ fontFamily: "Helvetica-Bold", }}>{business.businessname}</Text></View>
                <View style={pdfstyles.businessAddress}><Text style={{ textTransform: "uppercase" }}>{business.address} , {business.state}</Text></View>
                <View style={pdfstyles.businessFineDetail}><Text>{business.gstin ? "GSTIN : " + business.gstin : ''} {business.pan ? " PAN NO : " + business.pan : ''}</Text></View>
                <View style={pdfstyles.businessFineDetail}><Text>{business.email ? " EMAIL : " + business.email : ''} {business.email ? " MOBILE : " + business.mobile : ''}</Text></View>
            </View>
            <View style={{ marginTop: '15px' }}>
                <View style={pdfstyles.invoiceDetailSection}>
                    <View style={pdfstyles.invoiceFineDetailWidth}><Text style={pdfstyles.grey}>Invoice No:</Text><Text>{data.invoicenumber + " / " + fy}</Text></View><View style={pdfstyles.invoiceFineDetailWidth}><Text style={pdfstyles.grey}>Invoice Date:</Text><Text>{data.invoicedate}</Text></View><View style={pdfstyles.invoiceFineDetailWidth}><Text style={pdfstyles.grey}>PO No:</Text><Text>{data.ponumber ? data.ponumber : ' '}</Text></View><View style={pdfstyles.invoiceFineDetailWidth}><Text style={pdfstyles.grey}>PO Date:</Text><Text>{data.podate ? data.podate : ' '}</Text></View>
                </View>
                <View style={pdfstyles.invoiceDetailSectionSecond}>
                    <View style={pdfstyles.invoiceFineDetailWidth}><Text style={pdfstyles.grey}>Vehicle No:</Text><Text>{data.vehiclenumber}</Text></View><View style={pdfstyles.invoiceFineDetailWidth}><Text style={pdfstyles.grey}>Transporter:</Text><Text>{data.transporter ? data.transporter : ' '}</Text></View><View style={pdfstyles.invoiceFineDetailWidth}><Text style={pdfstyles.grey}>GR/BL No:</Text><Text>{data.grno ? data.grno : ' '}</Text></View><View style={pdfstyles.invoiceFineDetailWidth}><Text style={pdfstyles.grey}>Destination:</Text><Text>{data.destination}</Text></View>
                </View>
            </View>
            <View style={pdfstyles.billedToSection}>
                <View style={pdfstyles.customerSection}>
                    <Text style={{ fontFamily: "Helvetica", marginBottom: "2px" }}>Billed To : </Text>
                    <Text style={{ fontFamily: "Helvetica-Bold" }}>{customer.businessname}</Text>
                    <Text style={{ textTransform: "uppercase" }}>{customer.address}</Text>
                    <StateRow state={customer.state} gstin={customer.gstin} />
                    <GstinRow gstin={customer.gstin} />
                </View>
                <View style={pdfstyles.customerSectionSecond}>
                    <Text style={{ fontFamily: "Helvetica", marginBottom: "2px" }}>Shipped To : </Text>
                    <Text style={{ fontFamily: "Helvetica-Bold" }}>{shipped.businessname}</Text>
                    <Text style={{ textTransform: "uppercase" }}>{shipped.address}</Text>
                    <StateRow state={shipped.state} gstin={shipped.gstin} />
                    <GstinRow gstin={shipped.gstin} />
                </View>
            </View>
            <View style={pdfstyles.itemSection}>
                <View style={{ position: "absolute", display: "flex", flexDirection: "row" }}><Text style={{ width: '30px', height: "270px", borderRight: "1px solid black", }}></Text><Text style={{ width: '220px', height: "270px", borderRight: "1px solid black", }}></Text><Text style={{ width: '60px', height: "270px", borderRight: "1px solid black", }}></Text><Text style={{ width: '50px', height: "270px", borderRight: "1px solid black", }}></Text><Text style={{ width: '60px', height: "270px", borderRight: "1px solid black", }}></Text><Text style={{ width: '80px', height: "270px" }}></Text></View>
                <View style={pdfstyles.itemHeader}><Text style={{ width: '30px', padding: '2px', borderBottom: "1px solid black", fontSize: "11px" }}>S.No</Text><Text style={{ width: '220px', padding: "2px", borderBottom: "1px solid black", fontSize: "11px" }}>Item Description</Text><Text style={{ width: '60px', textAlign: "center", borderBottom: "1px solid black", fontSize: "11px" }}>HSN</Text><Text style={{ width: '50px', textAlign: "center", borderBottom: "1px solid black", fontSize: "11px" }}>Qty in {billingunit}</Text><Text style={{ width: '60px', textAlign: "center", borderBottom: "1px solid black", fontSize: "11px" }}>Price per {billingunit}</Text><Text style={{ width: '80px', textAlign: "center", borderBottom: "1px solid black", fontSize: "11px" }}>Amount (INR)</Text></View>
                {keys.map(k => {
                    let it = item[k];
                    totaltaxable = totaltaxable + parseFloat(it.basic)
                    return (
                        <View>
                            <View style={pdfstyles.itemDetail}><Text style={{ textAlign: "center", width: '30px' }}>{parseFloat(k) + 1}</Text><Text style={{ width: '220px', paddingLeft: "5px", fontFamily: "Helvetica-Bold" }}>{JSON.parse(it.name).itemname}</Text><Text style={{ width: '60px', textAlign: "center" }}>{JSON.parse(it.name).hsncode}</Text><Text style={{ width: '50px', textAlign: "center" }}>{it.qty}</Text><Text style={{ width: '60px', textAlign: "center", }}>{parseFloat(it.price).toFixed(2)}</Text><Text style={{ width: '80px', textAlign: "center" }}>{parseFloat(it.basic).toFixed(2)}</Text></View>
                            <View style={pdfstyles.itemDetailDescription}><Text style={{ width: '30px' }}></Text><Text style={{ width: '220px', paddingLeft: "5px" }}>{it.desc}</Text><Text style={{ width: '60px', textAlign: "center" }}></Text><Text style={{ width: '50px', textAlign: "center" }}></Text><Text style={{ width: '60px', textAlign: "center" }}></Text><Text style={{ width: '80px', textAlign: "center" }}></Text></View>
                            <TaxRowPdfHandler it={it} />
                        </View>
                    )
                })}
            </View>
            <View style={{ width: "500px", display: "flex", flexDirection: "row", borderLeft: "1px solid, black", borderRight: "1px soid black", borderBottom: "1px solid black" }}><Text style={{ width: '32px', borderRight: "1px solid black" }}></Text><Text style={{ width: '407px', textAlign: "right", paddingRight: "5px", borderRight: "1px solid black" }}><Text style={{ padding: "2px" }}>Freight</Text></Text><Text style={{ width: '80px', textAlign: "center" }}><Text style={{ padding: "2px" }}>{parseFloat(data.freight).toFixed(2)}</Text></Text></View>
            <View style={{ width: "500px", display: "flex", flexDirection: "row", borderLeft: "1px solid, black", borderRight: "1px soid black", borderBottom: "1px solid black" }}><Text style={{ width: '32px', borderRight: "1px solid black" }}></Text><Text style={{ width: '407px', textAlign: "right", paddingRight: "5px", borderRight: "1px solid black" }}><Text style={{ padding: "2px" }}>Grand Total (INR)</Text></Text><View style={{ width: '80px', textAlign: "center" }}><Text style={{ padding: "2px" }}>{parseFloat(data.grandtotal).toFixed(2)}</Text></View></View>
            <View style={{ width: "500px", borderLeft: "1px solid black", borderRight: "1px solid black" }}><Text style={{ padding: "2px" }}>Amount in Words</Text></View>
            <View style={{ width: "500px", borderLeft: "1px solid black", borderRight: "1px solid black", borderBottom: "1px solid black", fontFamily: "Helvetica-Bold", display: "flex", flexDirection: "row" }}><Text style={{ padding: "2px" }}>INR</Text><Text style={{ padding: "2px", textTransform: "capitalize" }}>{grandinWords}</Text></View>
            <TaxElaborateRow data={data} totaltaxable={totaltaxable} />
            <View style={{ width: "500px", borderLeft: "1px solid black", borderRight: "1px solid black" }}><Text style={{ padding: "2px", fontFamily: "Helvetica-Bold" }}>Terms and Conditions : </Text></View>
            <View style={{ width: "500px", borderLeft: "1px solid black", borderRight: "1px solid black", borderBottom: "1px solid black", display: "flex", flexDirection: "row" }}>
                <View style={{ width: "270px", padding: "2px" }}>
                    <Terms terms={data.terms} />
                </View>
                <View style={{ width: "10px" }}></View>
                <View style={{ width: "220px", textAlign: "right", padding: "2px", verticalAlign: "bottom" }}>
                    <Text style={{ fontFamily: "Helvetica-Bold" }}>for {business.businessname}</Text>
                    <Text> </Text>
                    <Text> </Text>
                    <Text> </Text>
                    <Text style={{ fontFamily: "Helvetica-Bold" }}>Authorised Signatory</Text>
                </View>
            </View>
            <View style={{ width: "500px", textAlign: "center", marginTop: "2px" }}><Text>All Disputes Subject To {business.state} Juridisction</Text></View>
        </Page>
    );
}
const Invoice = ({ data, fy, customer, shipped, business }) => {
    let message = ['Original for Recepient', 'Duplicate for Transporter', 'Triplicate for Consignor', 'Extra'];
    return (
        <Document>
            {message.map(m => <PageInvoice data={data} fy={fy} customer={customer} shipped={shipped} business={business} message={m} />)}
        </Document>
    );
}

const DataRow = function ({ data, getCustomer, businessid, businessList, cancelInvoice, fyid, myInvoice, i }) {
    let business = businessList.data.filter(buss => buss.businessid === businessid)[0];
    let cancel = function () {
        if (window.confirm("Cancel invoice number " + data.invoicenumber)) cancelInvoice(data.invoiceid, businessid, fyid);
    }
    let invnum = data.invoicenumber;
    invnum = invnum.toString();
    if (invnum.length < 3) {
        let l = 3 - invnum.length;
        for (let i = 0; i < l; i++) {
            invnum = '0' + invnum;
        }
    }
    data.invoicenumber = invnum;
    let generatePdf = async () => {
        let customer = getCustomer.data.filter(d => d.customerid === data.customerid)[0];
        let shipped = getCustomer.data.filter(d => d.customerid === data.shippedtoid)[0];
        let year = parseInt(data.invoicedate.split(' ')[2]);
        let month = data.invoicedate.split(' ')[0];
        let financialyear = '';
        if (month === 'Jan' || month === 'Feb' || month === 'Mar') financialyear = (parseInt(year) - 1) + "-" + year;
        else financialyear = year + "-" + (parseInt(year) + 1);
        const doc = <Invoice data={data} fy={financialyear} customer={customer} shipped={shipped} business={business} />;
        const blobPdf = await pdf(doc);
        blobPdf.updateContainer(doc);
        const result = await blobPdf.toBlob();
        let filename = invnum + "_" + financialyear + ".pdf";
        saveAs(result, filename);
    }

    if (i % 2 === 0) {
        return (
            <div>
                <div className=" d-flex small mb-1 p-1 d-md-none d-lg-none" style={{ backgroundColor: "rgb(181, 253, 233)" }}>
                    <div className="col-5 p-1">
                        <div className="col-12 p-0"><strong>{data.invoicenumber}/{data.invoicedate}</strong></div>
                        <div className="col-12 p-0 font-italic">By {data.customername}</div>
                    </div>
                    <div className="col-5 p-0 text-right">
                        <div className="col-12 p-0"><strong>{"Basic: " + parseFloat(data.totalbasic).toFixed(2)}</strong></div>
                        <div className="col-12 p-0 font-italic"><strong>{parseInt(data.freight) > 0 ? "Freight: " + parseFloat(data.freight).toFixed(2) : ''}</strong></div>
                        <div className="col-12 p-0 font-italic">{parseInt(data.totaligst) > 0 ? "IGST: " + parseFloat(data.totaligst).toFixed(2) : ''}</div>
                        <div className="cl-12 p-0 font-italic">{parseInt(data.totalcgst) > 0 ? "CGST: " + parseFloat(data.totalcgst).toFixed(2) : ''}</div>
                        <div className="col-12 p-0 font-italic">{parseInt(data.totalsgst) > 0 ? "SGST: " + parseFloat(data.totalsgst).toFixed(2) : ''}</div>
                        <div className="col-12 p-0">{"Tot: " + parseFloat(data.grandtotal).toFixed(2)}</div>
                    </div>
                    <div className="col-2 col-md-1">
                        <div className="col-12"><FontAwesomeIcon icon={faDownload} size="lg" onClick={generatePdf} /></div><div className="col-12 mt-1"><Link to={`/business/invoiceupdate/${businessid}/${fyid}/${data.invoiceid}`}><FontAwesomeIcon icon={faEdit} size="lg" style={{ color: "black" }} /></Link></div><div className="col-12 mt-1"><FontAwesomeIcon icon={faTimesCircle} onClick={() => cancel()} size="lg" /></div>
                    </div>
                </div>
                <div className="d-none d-md-block small mb-1 p-1" style={{ backgroundColor: "rgb(181, 253, 233)" }}>
                    <div className="d-flex">
                        <div className="col-1 p-0">{data.invoicedate}</div>
                        <div className="col-1 p-0">{data.invoicenumber}</div>
                        <div className="col-4 p-0"><strong>By {data.customername}</strong></div>
                        <div className="col-2">
                        </div>
                        <div className="col-3 text-right">
                            <div className="col-12 p-0"><strong>{"Basic: " + parseFloat(data.totalbasic).toFixed(2)}</strong></div>
                            <div className="col-12 p-0"><strong>{parseInt(data.freight) > 0 ? "Freight: " + parseFloat(data.freight).toFixed(2) : ''}</strong></div>
                            <div className="col-12 p-0">{parseInt(data.totaligst) > 0 ? "IGST: " + parseFloat(data.totaligst).toFixed(2) : ''}</div>
                            <div className="col-12 p-0">{parseInt(data.totalcgst) > 0 ? "CGST: " + parseFloat(data.totalcgst).toFixed(2) : ''}</div>
                            <div className="col-12 p-0">{parseInt(data.totalsgst) > 0 ? "SGST: " + parseFloat(data.totalsgst).toFixed(2) : ''}</div>
                            <div className="col-12 p-0">{"Tot: " + parseFloat(data.grandtotal).toFixed(2)}</div>
                        </div>
                        <div className="col-1 p-0 row"><div className="col-4"><FontAwesomeIcon icon={faDownload} size="lg" onClick={generatePdf} /></div><div className="col-4"><Link to={`/business/invoiceupdate/${businessid}/${fyid}/${data.invoiceid}`}><FontAwesomeIcon icon={faEdit} size="lg" style={{ color: "black" }} /></Link></div><div className="col-4"><FontAwesomeIcon icon={faTimesCircle} onClick={() => cancel()} size="lg" /></div></div>
                    </div>
                </div>
            </div>
        )
    }
    else {
        return (
            <div>
                <div className=" d-flex small mb-1 p-1 d-md-none d-lg-none">
                    <div className="col-5 p-1">
                        <div className="col-12 p-0"><strong>{data.invoicenumber}/{data.invoicedate}</strong></div>
                        <div className="col-12 p-0 font-italic">By {data.customername}</div>
                    </div>
                    <div className="col-5 p-0 text-right">
                        <div className="col-12 p-0"><strong>{"Basic: " + parseFloat(data.totalbasic).toFixed(2)}</strong></div>
                        <div className="col-12 p-0 font-italic"><strong>{parseInt(data.freight) > 0 ? "Freight: " + parseFloat(data.freight).toFixed(2) : ''}</strong></div>
                        <div className="col-12 p-0 font-italic">{parseInt(data.totaligst) > 0 ? "IGST: " + parseFloat(data.totaligst).toFixed(2) : ''}</div>
                        <div className="cl-12 p-0 font-italic">{parseInt(data.totalcgst) > 0 ? "CGST: " + parseFloat(data.totalcgst).toFixed(2) : ''}</div>
                        <div className="col-12 p-0 font-italic">{parseInt(data.totalsgst) > 0 ? "SGST: " + parseFloat(data.totalsgst).toFixed(2) : ''}</div>
                        <div className="col-12 p-0">{"Tot: " + parseFloat(data.grandtotal).toFixed(2)}</div>
                    </div>
                    <div className="col-2 col-md-1">
                        <div className="col-12"><FontAwesomeIcon icon={faDownload} size="lg" onClick={generatePdf} /></div><div className="col-12 mt-1"><Link to={`/business/invoiceupdate/${businessid}/${fyid}/${data.invoiceid}`}><FontAwesomeIcon icon={faEdit} size="lg" style={{ color: "black" }} /></Link></div><div className="col-12 mt-1"><FontAwesomeIcon icon={faTimesCircle} onClick={() => cancel()} size="lg" /></div>
                    </div>
                </div>
                <div className=" d-none d-md-block small mb-1 p-1">
                    <div className="d-flex">
                        <div className="col-1 p-0">{data.invoicedate}</div>
                        <div className="col-1 p-0">{data.invoicenumber}</div>
                        <div className="col-4 p-0"><strong>By {data.customername}</strong></div>
                        <div className="col-2">
                        </div>
                        <div className="col-3 text-right">
                            <div className="col-12 p-0"><strong>{"Basic: " + parseFloat(data.totalbasic).toFixed(2)}</strong></div>
                            <div className="col-12 p-0"><strong>{parseInt(data.freight) > 0 ? "Freight: " + parseFloat(data.freight).toFixed(2) : ''}</strong></div>
                            <div className="col-12 p-0">{parseInt(data.totaligst) > 0 ? "IGST: " + parseFloat(data.totaligst).toFixed(2) : ''}</div>
                            <div className="col-12 p-0">{parseInt(data.totalcgst) > 0 ? "CGST: " + parseFloat(data.totalcgst).toFixed(2) : ''}</div>
                            <div className="col-12 p-0">{parseInt(data.totalsgst) > 0 ? "SGST: " + parseFloat(data.totalsgst).toFixed(2) : ''}</div>
                            <div className="col-12 p-0">{"Tot: " + parseFloat(data.grandtotal).toFixed(2)}</div>
                        </div>
                        <div className="col-1 p-0 row"><div className="col-4"><FontAwesomeIcon icon={faDownload} size="lg" onClick={generatePdf} /></div><div className="col-4"><Link to={`/business/invoiceupdate/${businessid}/${fyid}/${data.invoiceid}`}><FontAwesomeIcon icon={faEdit} size="lg" style={{ color: "black" }} /></Link></div><div className="col-4"><FontAwesomeIcon icon={faTimesCircle} onClick={() => cancel()} size="lg" /></div></div>
                    </div>
                </div>
            </div>
        )
    }
}

const DataRowCancelled = function ({ data, getCustomer, businessid, businessList, cancelInvoice, fyid, restoreInvoice, i }) {
    let restore = () => {
        restoreInvoice(data.invoiceid, businessid, fyid);
    }
    let invnum = data.invoicenumber;
    invnum = invnum.toString();
    if (invnum.length < 3) {
        let l = 3 - invnum.length;
        for (let i = 0; i < l; i++) {
            invnum = '0' + invnum;
        }
    }
    data.invoicenumber = invnum;
    return (
        <div>
            <div className=" d-flex small border-bottom p-1 d-md-none d-lg-none" style={{ backgroundColor: "rgb(247, 182, 150)" }}>
                <div className="col-5 p-1">
                    <div className="col-12 p-0"><strong>{data.invoicenumber}/{data.invoicedate}</strong></div>
                    <div className="col-12 p-0 font-italic">{data.customername}</div>
                </div>
                <div className="col-5 p-0 text-right">
                    <div className="col-12 p-0">{parseInt(data.grandtotal) > 0 ? "Tot: " + parseFloat(data.grandtotal).toFixed(2) : ''}</div>
                </div>
                <div className="col-2 ">
                    <div className="col-12"><FontAwesomeIcon icon={faCheckCircle} onClick={restore} size="lg" /></div>
                </div>
            </div>
            <div className=" d-none d-md-block border-bottom small p-1" style={{ backgroundColor: "rgb(247, 182, 150)" }}>
                <div className="d-flex">
                    <div className="col-1 p-0">{data.invoicedate}</div>
                    <div className="col-1 p-0">{data.invoicenumber}</div>
                    <div className="col-4 p-0"><strong>{data.customername}</strong></div>
                    <div className="col-2">
                    </div>
                    <div className="col-3 text-right">
                        <div className="col-12 p-0">{parseInt(data.grandtotal) > 0 ? "Tot: " + parseFloat(data.grandtotal).toFixed(2) : ''}</div>
                    </div>
                    <div className="col-1 d-flex justify-content-center">
                        <div className="col-8 mt-1"></div>
                        <div className="col-4 mt-1"><FontAwesomeIcon icon={faCheckCircle} onClick={restore} size="lg" /></div>
                    </div>
                </div>
            </div>
        </div>
    )
}
const DisplayData = function ({ state, salesDetail, getCustomer, businessid, businessList, cancelInvoice, fyid, myInvoice, restoreInvoice, getDebitCredit, deleteEntry }) {
    let returndata = [];
    let data = salesDetail.data;
    let debitcreditdata = getDebitCredit.data;
    if (state.monthSelection !== '') {
        data = data.filter(d => d.invoicedate.split(' ')[0] === state.monthSelection)
        debitcreditdata = debitcreditdata.filter(d => d.date.split(' ')[0] === state.monthSelection);
    }
    let activedata = [];
    let inactivedata = [];
    let totalsales = 0;
    let totaldebit = 0;
    let temp;
    data.forEach(d => {
        if (d.type === 'sales') {
            if (d.active == 0) activedata.push(d);
            else inactivedata.push(d);
        }
    });
    if (activedata.length > 0) {
        let i = 1;
        temp =
            <div className="mt-1 container">
                <div className="">
                    {activedata.map(d => {
                        i++;
                        totalsales = totalsales
                            + parseFloat(d.totalbasic) + parseFloat(d.freight);
                        return <DataRow data={d} getCustomer={getCustomer} businessid={businessid} businessList={businessList} cancelInvoice={cancelInvoice} fyid={fyid} myInvoice={myInvoice} i={i} />;
                    })}
                </div>
            </div>
        returndata.push(temp);
    }
    let i = 0;
    debitcreditdata.map(d => {
        if ((d.issuereceived === 'issue' && d.debitcredit === 'credit') || (d.issuereceived === 'received' && d.debitcredit === 'debit')) {
            let cancel = function () {
                if (window.confirm("Delete Debit/Credit Note ")) deleteEntry(d.id, businessid, fyid, 'debitcredit');
            }
            i++;
            totaldebit = totaldebit + parseFloat(d.total);
            temp =
                <div>
                    <div className="">
                        <div className=" d-flex small mb-1 p-1 d-md-none d-lg-none" style={i % 2 === 0 ? { backgroundColor: "rgb(181, 253, 233)" } : {}}>
                            <div className="col-5 p-1">
                                <div className="col-12 col-md-6 p-0 font-italic">{d.voucherno}/{d.date}</div>
                                <div className="col-12 col-md-6 p-0 font-italic">To {d.debitcredit.toUpperCase() + "NOTE / " + d.issuereceived}</div>
                                <div className="col-12 col-md-6 p-0 font-italic">{d.businessname}</div>
                                <div className="col-12 col-md-6 p-0 font-italic">{d.remarks}</div>
                            </div>
                            <div className="col-5 p-0 text-right">
                                <div className="col-12 col-md-2 p-0 font-italic">{parseInt(d.basic) > 0 ? "Basic: " + parseFloat(d.basic).toFixed(2) : ''}</div>
                                <div className="col-12 col-md-2 p-0 font-italic">{parseInt(d.igst) > 0 ? "IGST: " + parseFloat(d.igst).toFixed(2) : ''}</div>
                                <div className="cl-12 col-md-2 p-0 font-italic">{parseInt(d.cgst) > 0 ? "CGST: " + parseFloat(d.cgst).toFixed(2) : ''}</div>
                                <div className="col-12 col-md-2 p-0 font-italic">{parseInt(d.sgst) > 0 ? "SGST: " + parseFloat(d.sgst).toFixed(2) : ''}</div>
                                <div className="col-12 col-md-2 p-0">{parseInt(d.total) > 0 ? "Tot: " + parseFloat(d.total).toFixed(2) : ''}</div>
                            </div>
                            <div className="col-2 col-md-1">
                                <div className="col-12 mt-1"></div>
                                <div className="col-12 mt-2"><FontAwesomeIcon icon={faTrash} onClick={() => cancel()} size="lg" /></div>
                            </div>
                        </div>
                    </div>
                    <div className=" d-none d-md-block small mb-1 p-1">
                        <div className="d-flex">
                            <div className="col-1 p-0">{d.date}</div>
                            <div className="col-1 p-0">{d.voucherno}</div>
                            <div className="col-4 p-0">
                                <div className="col-12 col-md-6 p-0"><strong>To {d.debitcredit.toUpperCase() + "NOTE / " + d.issuereceived}</strong></div>
                                <div>{d.businessname}</div>
                                <div>{d.remarks}</div>
                            </div>
                            <div className="col-2">
                                <div className="col-12 p-0"><strong>{"Basic: " + parseFloat(d.basic).toFixed(2)}</strong></div>
                                <div className="col-12 p-0">{parseInt(d.igst) > 0 ? "IGST: " + parseFloat(d.igst).toFixed(2) : ''}</div>
                                <div className="col-12 p-0">{parseInt(d.cgst) > 0 ? "CGST: " + parseFloat(d.cgst).toFixed(2) : ''}</div>
                                <div className="col-12 p-0">{parseInt(d.sgst) > 0 ? "SGST: " + parseFloat(d.sgst).toFixed(2) : ''}</div>
                                <div className="col-12 p-0">{parseInt(d.total) > 0 ? "Tot: " + parseFloat(d.total).toFixed(2) : ''}</div>
                            </div>
                            <div className="col-3">
                            </div>
                            <div className="col-1 p-0 row">
                                <div className="col-4 mt-1"></div>
                                <div className="col-4 mt-1"></div>
                                <div className="col-4 mt-1"><FontAwesomeIcon icon={faTrash} onClick={() => cancel()} size="lg" /></div>
                            </div>
                        </div>
                    </div>
                </div>;
            returndata.push(temp);
            i++;
        }
    })
    let temp1 = '';
    if (inactivedata.length > 0) {
        let i = 1;
        temp1 =
            <div className="mt-3">
                <div className="pt-0">
                    {inactivedata.map(d => {
                        i++;
                        return <DataRowCancelled data={d} getCustomer={getCustomer} businessid={businessid} businessList={businessList} cancelInvoice={cancelInvoice} fyid={fyid} restoreInvoice={restoreInvoice} i={i} />
                    })}
                </div>
            </div>
    }
    if ((data.length === 0) && (debitcreditdata.length === 0)) {
        temp =
            <div className="container mt-2">
                <div className="card border">
                    <div className="text-center">No Sales Data to Display</div>
                </div>
            </div>
        returndata.push(temp)
    }
    let t = <div className="col-12 mt-2 small"><strong>Sales :</strong> {parseFloat(totalsales).toFixed(2)}</div>;
    let t1 = <div className="col-12 small"><strong>Debit/Credit Note :</strong> {parseFloat(totaldebit).toFixed(2)}</div>;
    let t2 = <div className="col-12 small"><strong>Net :</strong> {parseFloat(totalsales - totaldebit).toFixed(2)}</div>;
    let t3 = <div className="small mt-3 d-none d-md-block">
        <div className="d-flex">
            <div className="col-1"><strong>Date</strong></div>
            <div className="col-1"><strong>Inv No</strong></div>
            <div className="col-4"><strong>Customer Name</strong></div>
            <div className="col-2 text-right"><div className="col-12"><strong>Debit</strong></div></div>
            <div className="col-3 text-right"><div className="col-12"><strong>Credit</strong></div></div>
        </div>
    </div>
    let t4 = <div>
        <div className="container small d-none d-md-block p-1 mt-1">
            <div className="d-flex">
                <div className="col-1 p-0"><strong></strong></div>
                <div className="col-1 p-0"><strong></strong></div>
                <div className="col-4 p-0"><strong></strong></div>
                <div className="col-2 border-top border-dark text-right"><div className="col-12">{parseFloat(totaldebit).toFixed(2)}</div></div>
                <div className="col-3 border-top border-dark text-right"><div className="col-12">{parseFloat(totalsales).toFixed(2)}</div></div>
            </div>
        </div>
        <div className="container small d-none d-md-block p-1">
            <div className="d-flex">
                <div className="col-1 p-0"><strong></strong></div>
                <div className="col-1 p-0"><strong></strong></div>
                <div className="col-4"><strong>{"Net Sales"}</strong></div>
                <div className="col-2"></div>
                <div className="col-3 border-top border-dark text-right"><div className="col-12">{parseFloat(totalsales - totaldebit).toFixed(2)}</div></div>
            </div>
        </div>
    </div>

    let returndatatemp = [];
    returndatatemp.push(t);
    returndatatemp.push(t1);
    returndatatemp.push(t2);
    returndatatemp.push(t3);
    returndatatemp.push(returndata);
    returndatatemp.push(t4);
    returndatatemp.push(temp1);
    return returndatatemp;
}

const DisplaySalesData = function ({ getItem, businessid, businessList, getCustomer, salesDetail, state, handleOnChange, cancelInvoice, fyid, myInvoice, restoreInvoice, getDebitCredit, deleteEntry }) {
    return (
        <div>
            <LocalForm className="small">
                <div className="d-flex justify-content-center mt-2">
                    <div className="form-group mb-1 col-12 col-md-3">
                        <label htmlFor="monthSelection" className="mb-0 muted-text"><strong>Filter Data by Month:</strong></label>
                        <Control.select model=".monthSelection" className="form-control form-control-sm" id="monthSelection" name="monthSelection" value={state.monthSelection} onChange={handleOnChange}>
                            <Option list={salesDetail.data} scope="salesDetail" />
                        </Control.select>
                    </div>
                </div>
            </LocalForm>
            <DisplayData state={state} salesDetail={salesDetail} getCustomer={getCustomer} businessid={businessid} businessList={businessList} cancelInvoice={cancelInvoice} fyid={fyid} myInvoice={myInvoice} restoreInvoice={restoreInvoice} getDebitCredit={getDebitCredit} deleteEntry={deleteEntry} />
        </div>
    )
}

class SalesDetail extends Component {
    constructor(props) {
        super(props);
        this.state = ({
            customerSelection: '',
            monthSelection: '',
            quarterSelection: ''
        })
        window.onload = () => {
            this.props.refreshCustomerState();
            this.props.refreshItemState();
            this.props.resetSalesDetail();
            this.props.refreshDebitCredit();
        }
    }

    handleOnChange = (event) => {
        let target = event.target;
        let name = target.name;
        let value = target.value;
        this.setState({
            ...this.state,
            [name]: value
        })
    }

    componentDidMount() {
        if (this.props.getItem.message === 'initial') this.props.getItemList(this.props.businessid);
        if ((this.props.getCustomer.message === 'initial') && (this.props.getCustomer.loading === false)) this.props.getCustomerList(this.props.businessid);
        if ((this.props.salesDetail.message === 'initial') && (this.props.salesDetail.loading === false)) this.props.getSalesData(this.props.businessid, this.props.fyid);
        if (this.props.getDebitCredit.message === 'initial') this.props.getDebitCreditList(this.props.businessid, this.props.fyid);
        if (this.props.cancelInvoiceMessage.error !== '') {
            alert("Error cancelling invoice - Please Try Again");
            this.props.resetCancelInvoiceMessage();
        }
        if (this.props.cancelInvoiceMessage.message === 'success') {
            alert("Invoice Cancelled Successfully");
            this.props.resetCancelInvoiceMessage()
        }
        if (this.props.restoreInvoiceMessage.message === 'success') {
            alert("Invoice Restored Successfully");
            this.props.resetRestoreInvoiceMessage();
        }
        if (this.props.restoreInvoiceMessage.error !== '') {
            alert("Error Restoring invoice - Please Try Again");
            this.props.resetRestoreInvoiceMessage();
        }
        if (this.props.deleteEntryMessage.message === "success") {
            alert("Entry Deleted Successfully");
            this.props.resetDeleteEntry();
        }
        if (this.props.deleteEntryMessage.error !== '') {
            alert("Error Deleting Entry - Please Try Again");
            this.props.resetDeleteEntry();
        }

        if (this.props.logoutMessage.error !== '') {
            alert("Error Logging Out. Please Try Again");
            this.props.resetLogoutMessage();
        }
    }
    genereateSalesPdf = async (business, fy) => {
        try {
            let data = this.props.salesDetail.data.filter(d => d.type === 'sales' && d.active == 0);
            let debitcreditdata = this.props.getDebitCredit.data.filter(d => ((d.issuereceived === 'issue' && d.debitcredit === 'credit') || (d.issuereceived === 'received' && d.debitcredit === 'debit')));
            if (this.state.monthSelection !== '') {
                data = data.filter(d => d.invoicedate.split(' ')[0] === this.state.monthSelection);
                debitcreditdata = debitcreditdata.filter(d => d.date.split(' ')[0] === this.state.monthSelection);
            }
            const doc = <SalesLedger data={data} debitcreditdata={debitcreditdata} business={business} fy={fy} month={this.state.monthSelection} />;
            const blobPdf = pdf(doc);
            blobPdf.updateContainer(doc);
            const result = await blobPdf.toBlob();
            let filename = "SalesLedger.pdf";
            saveAs(result, filename);
        }
        catch (err) {
            alert(err);
        }
    }
    render() {
        if (!cookie.load('userservice')) return <Redirect to="/" />;
        else {
            if ((this.props.getItem.loading === true) || (this.props.salesDetail.loading === true) || (this.props.getCustomer.loading === true) || (this.props.logoutMessage.loading === true) || (this.props.cancelInvoiceMessage.loading === true) || (this.props.restoreInvoiceMessage.loading === true) || (this.props.getDebitCredit.loading === true)) {
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div className="mt-3 d-flex justify-content-center">
                            <button className="btn btn-danger">Loading... </button>
                        </div>
                    </div>
                );
            }
            else if ((this.props.getItem.error !== '') || (this.props.salesDetail.error !== '') || (this.props.getCustomer.error !== '') || (this.props.getDebitCredit.error !== '')) {
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div className="mt-3 d-flex justify-content-center">
                            <button className="btn btn-danger">Error.. Please Refresh and Try Agin </button>
                        </div>
                    </div>
                );
            }
            else {
                let business = this.props.businessList.data.filter(b => b.businessid === this.props.businessid)[0];
                let fy = business.financial.filter(f => f.fyid === this.props.fyid)[0].financialyear;
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div style={{ minHeight: "93vh", display: "flex", flexDirection: "column" }}>
                            <div className="container pl-0 pr-0" style={{ flex: "1" }}>
                                <h6 className="text-center pl-2 pr-2 pt-4" style={{ lineHeight: "1" }}><strong>{business.businessname} {fy}</strong></h6>
                                <h6 className="text-center pl-2 pr-2 mb-2" style={{ lineHeight: "1" }}>{business.alias ? "(" + business.alias + ")" : ''}</h6>
                                <h6 className="text-center pl-2 pr-2"><strong><u>Sales Ledger</u></strong>&nbsp;&nbsp;&nbsp;<FontAwesomeIcon icon={faFilePdf} size="lg" onClick={() => this.genereateSalesPdf(business, fy)} />&nbsp;&nbsp;&nbsp;<FontAwesomeIcon icon={faFileExcel} size="lg" /></h6>
                                < DisplaySalesData getItem={this.props.getItem} businessid={this.props.businessid} businessList={this.props.businessList} getCustomer={this.props.getCustomer} salesDetail={this.props.salesDetail} state={this.state} handleOnChange={this.handleOnChange} cancelInvoice={this.props.cancelInvoice} fyid={this.props.fyid} myInvoice={this.myInvoice} restoreInvoice={this.props.restoreInvoice} getDebitCredit={this.props.getDebitCredit} deleteEntry={this.props.deleteEntry} />
                            </div>
                            <Footer />
                        </div>
                    </div>
                )
            }
        }
    }
}
export default SalesDetail;