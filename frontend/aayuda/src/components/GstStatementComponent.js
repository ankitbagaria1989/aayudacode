import React, { Component } from 'react';
import cookie from 'react-cookie';
import { Redirect } from 'react-router-dom';
import { Header } from './HeaderComponent';
import { sortData } from '../redux/sortArrayofObjects';
import { Footer } from './Footer';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faFilePdf } from '@fortawesome/free-solid-svg-icons';
import { Page, Text, View, Document, pdf } from '@react-pdf/renderer';
import { pdfstyles } from '../sylesheet/stylesheet';
import { saveAs } from 'file-saver';

const DataDisplay = function ({ monthWiseGSTSale, monthWiseGSTPurchase, monthWiseOpeningClosing, monthWiseDebitCredit, getGSTOpeningBalance }) {
    let datarow = [];
    let i = 3;
    let igstdt = getGSTOpeningBalance.inputpayable === 'input' ? parseFloat(getGSTOpeningBalance.gst) : 0, cgstdt = 0, sgstdt = 0;
    let igstct = getGSTOpeningBalance.inputpayable === 'payable' ? parseFloat(getGSTOpeningBalance.gst) : 0, cgstct = 0, sgstct = 0;
    let month = ['apr', 'may', 'jun', 'jul', 'aug', 'sep', 'oct', 'nov', 'dec', 'jan', 'feb', 'mar'];
    datarow.push(<div><div className="d-flex small mt-4 p-1 d-md-none d-lg-none">
        <div className="col-4 col-md-5 p-1">
            <div className="col-12 col-md-6 p-0 "></div>
        </div>
        <div className="col-4 col-md-6 pt-1 text-right"><strong>Debit</strong></div>
        <div className="col-4 col-md-1 pt-1 text-right"><strong>Credit</strong></div>
    </div>
        <div className="container small mt-3 d-none d-md-block p-0">
            <div className="d-flex">
                <div className="col-1 p-0"><strong>Date</strong></div>
                <div className="col-1 p-0"><strong>Voucher No</strong></div>
                <div className="col-4 p-0"><strong>Particulars</strong></div>
                <div className="col-2 text-right"><strong>Debit</strong></div>
                <div className="col-3 text-right"><strong>Credit</strong></div>
            </div>
        </div></div>)
    datarow.push(<div><div className="d-flex small p-1 d-md-none d-lg-none" style={{ backgroundColor: "rgb(181, 253, 233)" }}>
        <div className="col-4 col-md-5 p-1">
            <div className="col-12 col-md-6 p-0 "><strong>Apr-Opening Bal</strong></div>
            <div className="col-12 col-md-6 p-0 ">{getGSTOpeningBalance.inputpayable}</div>
        </div>
        <div className="col-4 col-md-1 p-1">
            <div className="col-12 p-0 text-right">{getGSTOpeningBalance.inputpayable === 'input' ? parseFloat(getGSTOpeningBalance.gst).toFixed(2) : ''}</div>
        </div>
        <div className="col-4 col-md-1  text-right">{getGSTOpeningBalance.inputpayable === 'payable' ? parseFloat(getGSTOpeningBalance.gst).toFixed(2) : ''}</div>
    </div>
        <div className="container small mt-1 d-none d-md-block p-1" style={{ backgroundColor: "rgb(181, 253, 233)" }}>
            <div className="d-flex">
                <div className="col-2 p-0">Apr Opening Bal</div>
                <div className="col-4 p-0"><strong>{getGSTOpeningBalance.inputpayable}</strong></div>
                <div className="col-2">
                    <div className="col-12 p-0  text-right">{getGSTOpeningBalance.inputpayable === 'input' ? parseFloat(getGSTOpeningBalance.gst).toFixed(2) : ''}</div>
                </div>
                <div className="col-3 text-right">{getGSTOpeningBalance.inputpayable === 'payable' ? parseFloat(getGSTOpeningBalance.gst).toFixed(2) : ''}</div>
            </div>
        </div></div>)
    month.forEach(month => {
        let x = 0;
        let data = [];
        data = data.concat(monthWiseGSTSale[month]);
        data = data.concat(monthWiseGSTPurchase[month]);
        data = data.concat(monthWiseDebitCredit[month]);
        data = data.sort(sortData);
        let igstd = 0, cgstd = 0, sgstd = 0;
        let igstc = 0, cgstc = 0, sgstc = 0;
        while (x < data.length) {
            if ((data[x].type === 'sales') && (data[x].totaligst > 0 || data[x].totalcgst > 0 || data[x].totalsgst > 0)) {
                datarow.push(<div><div className=" d-flex small p-1 d-md-none d-lg-none" style={i % 2 === 0 ? { backgroundColor: "rgb(181, 253, 233)" } : {}}>
                    <div className="col-4 col-md-5 p-1">
                        <div className="col-12 col-md-6 p-0"><strong>{data[x].invoicenumber}/{data[x].invoicedate}</strong></div>
                        <div className="col-12 col-md-6 p-0 ">Sales</div>
                    </div>
                    <div className="col-4 col-md-1">
                    </div>
                    <div className="col-4 col-md-6 p-0 text-right">
                        <div className="col-12 p-0">{parseFloat(data[x].totaligst) > 0 ? "I: " + parseFloat(data[x].totaligst).toFixed(2) : ''}</div>
                        <div className="col-12 p-0">{parseFloat(data[x].totalsgst) > 0 ? "C: " + parseFloat(data[x].totalcgst).toFixed(2) : ''}</div>
                        <div className="col-12 p-0">{parseFloat(data[x].totalsgst) > 0 ? "S: " + parseFloat(data[x].totalsgst).toFixed(2) : ''}</div>
                    </div>
                </div>
                    <div className="container small d-none d-md-block p-1" style={i % 2 === 0 ? { backgroundColor: "rgb(181, 253, 233)" } : {}}>
                        <div className="d-flex">
                            <div className="col-1 p-0">{data[x].invoicedate}</div>
                            <div className="col-1 p-0">{data[x].invoicenumber}</div>
                            <div className="col-4 p-0"><strong>Sales</strong></div>
                            <div className="col-2"><strong>
                            </strong></div>
                            <div className="col-3 text-right">
                                <div className="col-12 p-0">{parseFloat(data[x].totaligst) > 0 ? "I: " + parseFloat(data[x].totaligst).toFixed(2) : ''}</div>
                                <div className="col-12 p-0">{parseFloat(data[x].totalsgst) > 0 ? "C: " + parseFloat(data[x].totalcgst).toFixed(2) : ''}</div>
                                <div className="col-12 p-0">{parseFloat(data[x].totalsgst) > 0 ? "S: " + parseFloat(data[x].totalsgst).toFixed(2) : ''}</div>
                            </div>
                        </div>
                    </div></div>)
                igstc = igstc + parseFloat(data[x].totaligst);
                cgstc = cgstc + parseFloat(data[x].totalcgst);
                sgstc = sgstc + parseFloat(data[x].totalsgst);
            }
            if (data[x].type === 'asset') {
                if ((data[x].type1 === 'assetsell') && (data[x].soldigst > 0 || data[x].soldcgst > 0 || data[x].soldsgst > 0)) {
                    datarow.push(<div><div className=" d-flex small p-1 d-md-none d-lg-none" style={i % 2 === 0 ? { backgroundColor: "rgb(181, 253, 233)" } : {}}>
                        <div className="col-4 col-md-5 p-1">
                            <div className="col-12 col-md-6 p-0"><strong>{data[x].voucherno}/{data[x].solddate}</strong></div>
                            <div className="col-12 col-md-6 p-0 ">Asset Sales</div>
                        </div>
                        <div className="col-4 col-md-1">
                        </div>
                        <div className="col-4 col-md-6 p-0 text-right">
                            <div className="col-12 p-0">{parseFloat(data[x].soldigst) > 0 ? "I: " + parseFloat(data[x].soldigst).toFixed(2) : ''}</div>
                            <div className="col-12 p-0">{parseFloat(data[x].soldsgst) > 0 ? "C: " + parseFloat(data[x].soldcgst).toFixed(2) : ''}</div>
                            <div className="col-12 p-0">{parseFloat(data[x].soldsgst) > 0 ? "S: " + parseFloat(data[x].soldsgst).toFixed(2) : ''}</div>
                        </div>
                    </div>
                        <div className="container small d-none d-md-block p-1" style={i % 2 === 0 ? { backgroundColor: "rgb(181, 253, 233)" } : {}}>
                            <div className="d-flex">
                                <div className="col-1 p-0">{data[x].solddate}</div>
                                <div className="col-1 p-0">{data[x].voucherno}</div>
                                <div className="col-4 p-0"><strong>Asset Sales</strong></div>
                                <div className="col-2"><strong>
                                </strong></div>
                                <div className="col-3 text-right">
                                    <div className="col-12 p-0">{parseFloat(data[x].soldigst) > 0 ? "I: " + parseFloat(data[x].soldigst).toFixed(2) : ''}</div>
                                    <div className="col-12 p-0">{parseFloat(data[x].soldcgst) > 0 ? "C: " + parseFloat(data[x].soldcgst).toFixed(2) : ''}</div>
                                    <div className="col-12 p-0">{parseFloat(data[x].soldsgst) > 0 ? "S: " + parseFloat(data[x].soldsgst).toFixed(2) : ''}</div>
                                </div>
                            </div>
                        </div></div>)
                    igstc = igstc + parseFloat(data[x].soldigst);
                    cgstc = cgstc + parseFloat(data[x].soldcgst);
                    sgstc = sgstc + parseFloat(data[x].soldsgst);
                }
                if ((data[x].type2 === 'assetpurchase') && (data[x].purchaseigst > 0 || data[x].purchasecgst > 0 || data[x].purchasesgst > 0)) {
                    datarow.push(<div><div className=" d-flex small p-1 d-md-none d-lg-none" style={i % 2 === 0 ? { backgroundColor: "rgb(181, 253, 233)" } : {}}>
                        <div className="col-4 col-md-5 p-1">
                            <div className="col-12 col-md-6 p-0"><strong>{data[x].voucherno}</strong></div>
                            <div className="col-12 col-md-6 p-0"><strong>{data[x].purchasedate}</strong></div>
                            <div className="col-12 col-md-6 p-0 ">Asset Purchase</div>
                        </div>
                        <div className="col-4 col-md-1 text-right">
                            <div className="col-12 col-md-2 p-0">{parseFloat(data[x].purchaseigst) > 0 ? "I: " + parseFloat(data[x].purchaseigst).toFixed(2) : ''}</div>
                            <div className="col-12 col-md-2 p-0">{parseFloat(data[x].purchasecgst) > 0 ? "C: " + parseFloat(data[x].purchasecgst).toFixed(2) : ''}</div>
                            <div className="col-12 col-md-2 p-0">{parseFloat(data[x].purchasesgst) > 0 ? "S: " + parseFloat(data[x].purchasesgst).toFixed(2) : ''}</div>
                        </div>
                        <div className="col-4 col-md-6 p-0">
                        </div>
                    </div>
                        <div className="container small d-none d-md-block p-1">
                            <div className="d-flex">
                                <div className="col-1 p-0">{data[x].purchasedate}</div>
                                <div className="col-1 p-0">{data[x].voucherno}</div>
                                <div className="col-4 p-0"><strong>Asset Purchase</strong></div>
                                <div className="col-2 text-right">
                                    <div className="col-12 p-0" style={i % 2 === 0 ? { backgroundColor: "rgb(181, 253, 233)" } : {}}>{parseFloat(data[x].purchaseigst) > 0 ? "I: " + parseFloat(data[x].purchaseigst).toFixed(2) : ''}</div>
                                    <div className="col-12 p-0" style={i % 2 === 0 ? { backgroundColor: "rgb(181, 253, 233)" } : {}}>{parseFloat(data[x].purchasecgst) > 0 ? "C: " + parseFloat(data[x].purchasecgst).toFixed(2) : ''}</div>
                                    <div className="col-12 p-0" style={i % 2 === 0 ? { backgroundColor: "rgb(181, 253, 233)" } : {}}>{parseFloat(data[x].purchasesgst) > 0 ? "S: " + parseFloat(data[x].purchasesgst).toFixed(2) : ''}</div>
                                </div>
                                <div className="col-3"><strong></strong></div>
                            </div>
                        </div></div>
                    )
                    igstd = igstd + parseFloat(data[x].purchaseigst);
                    cgstd = cgstd + parseFloat(data[x].purchasecgst);
                    sgstd = sgstd + parseFloat(data[x].purchasesgst);
                }
            }
            else if ((data[x].type === 'purchase') && (data[x].totaligst > 0 || data[x].totalcgst > 0 || data[x].totalsgst > 0)) {
                datarow.push(<div><div className=" d-flex small p-1 d-md-none d-lg-none" style={i % 2 === 0 ? { backgroundColor: "rgb(181, 253, 233)" } : {}}>
                    <div className="col-4 col-md-5 p-1">
                        <div className="col-12 col-md-6 p-0"><strong>{data[x].invoicenumber}</strong></div>
                        <div className="col-12 col-md-6 p-0"><strong>{data[x].invoicedate}</strong></div>
                        <div className="col-12 col-md-6 p-0 ">Purchase</div>
                    </div>
                    <div className="col-4 col-md-1 text-right">
                        <div className="col-12 col-md-2 p-0">{parseFloat(data[x].totaligst) > 0 ? "I: " + parseFloat(data[x].totaligst).toFixed(2) : ''}</div>
                        <div className="col-12 col-md-2 p-0">{parseFloat(data[x].totalcgst) > 0 ? "C: " + parseFloat(data[x].totalcgst).toFixed(2) : ''}</div>
                        <div className="col-12 col-md-2 p-0">{parseFloat(data[x].totalsgst) > 0 ? "S: " + parseFloat(data[x].totalsgst).toFixed(2) : ''}</div>
                    </div>
                    <div className="col-4 col-md-6 p-0">
                    </div>
                </div>
                    <div className="container small d-none d-md-block p-1" style={i % 2 === 0 ? { backgroundColor: "rgb(181, 253, 233)" } : {}}>
                        <div className="d-flex">
                            <div className="col-1 p-0">{data[x].invoicedate}</div>
                            <div className="col-1 p-0">{data[x].invoicenumber}</div>
                            <div className="col-4 p-0"><strong>Purchase</strong></div>
                            <div className="col-2 text-right">
                                <div className="col-12 p-0">{parseFloat(data[x].totaligst) > 0 ? "I: " + parseFloat(data[x].totaligst).toFixed(2) : ''}</div>
                                <div className="col-12 p-0">{parseFloat(data[x].totalcgst) > 0 ? "C: " + parseFloat(data[x].totalcgst).toFixed(2) : ''}</div>
                                <div className="col-12 p-0">{parseFloat(data[x].totalsgst) > 0 ? "S: " + parseFloat(data[x].totalsgst).toFixed(2) : ''}</div>
                            </div>
                            <div className="col-3"><strong></strong></div>
                        </div>
                    </div></div>
                )
                igstd = igstd + parseFloat(data[x].totaligst);
                cgstd = cgstd + parseFloat(data[x].totalcgst);
                sgstd = sgstd + parseFloat(data[x].totalsgst);
            }
            else if (data[x].type === 'payment') {
                datarow.push(<div><div className=" d-flex small d-md-none d-lg-none" style={i % 2 === 0 ? { backgroundColor: "rgb(181, 253, 233)" } : {}}>
                    <div className="col-4 col-md-5 p-1">
                        <div className="col-12 col-md-6 p-0"><strong>{data[x].voucherno}/{data[x].date}</strong></div>
                        <div className="col-12 col-md-6 p-0 ">{data[x].vendorid === 'reversecharge' ? 'Reverse Charge Payment' : 'GST Payment'}</div>
                        <div className="col-12 col-md-6 p-0" style={{ lineHeight: "1" }}>{data[x].remark}</div>
                    </div>
                    <div className="col-4 col-md-1 text-right">
                        <div className="col-12 col-md-2 p-0">{data[x].amount}</div>
                    </div>
                    <div className="col-4 col-md-6 p-0">
                    </div>
                </div>
                    <div className="container small d-none d-md-block p-1" style={i % 2 === 0 ? { backgroundColor: "rgb(181, 253, 233)" } : {}}>
                        <div className="d-flex">
                            <div className="col-1 p-0">{data[x].date}</div>
                            <div className="col-1 p-0">{data[x].voucherno}</div>
                            <div className="col-4 p-0">
                                <div className="col-12 p-0"><strong>{data[x].vendorid === 'reversecharge' ? 'Reverse Charge Payment' : 'GST Payment'}</strong></div>
                                <div className="col-12 p-0" style={{ lineHeight: "1" }}><strong>{data[x].remark}</strong></div>
                            </div>
                            <div className="col-2 text-right">{data[x].amount}</div>
                            <div className="col-3"><strong></strong></div>
                        </div>
                    </div></div>
                )
                igstd = igstd + parseFloat(data[x].amount);
            }
            else if (data[x].type === 'debitcredit') {
                if (((data[x].issuereceived === 'received') && (data[x].debitcredit === 'debit')) || ((data[x].issuereceived === 'issue') && (data[x].debitcredit === 'credit'))) {
                    datarow.push(<div><div className=" d-flex small p-1 d-md-none d-lg-none" style={i % 2 === 0 ? { backgroundColor: "rgb(181, 253, 233)" } : {}}>
                        <div className="col-4 col-md-5 p-1">
                            <div className="col-12 col-md-6 p-0"><strong>{data[x].voucherno}/{data[x].date}</strong></div>
                            <div className="col-12 col-md-6 p-0" style={{ lineHeight: "1" }}>{data[x].debitcredit.toUpperCase() + " NOTE / " + data[x].issuereceived}</div>
                        </div>
                        <div className="col-4 col-md-1 text-right">
                            <div className="col-12 p-0">{parseFloat(data[x].igst) > 0 ? "I: " + parseFloat(data[x].igst).toFixed(2) : ''}</div>
                            <div className="col-12 p-0">{parseFloat(data[x].cgst) > 0 ? "C: " + parseFloat(data[x].cgst).toFixed(2) : ''}</div>
                            <div className="col-12 p-0">{parseFloat(data[x].sgst) > 0 ? "S: " + parseFloat(data[x].sgst).toFixed(2) : ''}</div>
                        </div>
                        <div className="col-4 col-md-6 p-0">
                        </div>
                    </div>
                        <div className="container d-none d-md-block p-1" style={i % 2 === 0 ? { backgroundColor: "rgb(181, 253, 233)" } : {}}>
                            <div className="d-flex small">
                                <div className="col-1 p-0">{data[x].date}</div>
                                <div className="col-1 p-0">{data[x].voucherno}</div>
                                <div className="col-4 p-0"><strong>{data[x].debitcredit.toUpperCase() + " NOTE / " + data[x].issuereceived}</strong></div>
                                <div className="col-2 text-right"><div className="col-12 p-0">{parseFloat(data[x].igst) > 0 ? "I: " + parseFloat(data[x].igst).toFixed(2) : ''}</div>
                                    <div className="col-12 p-0">{parseFloat(data[x].cgst) > 0 ? "C: " + parseFloat(data[x].cgst).toFixed(2) : ''}</div>
                                    <div className="col-12 p-0">{parseFloat(data[x].sgst) > 0 ? "S: " + parseFloat(data[x].sgst).toFixed(2) : ''}</div></div>
                                <div className="col-3" >
                                </div>
                            </div>
                        </div></div>)
                    igstd = igstd + parseFloat(data[x].igst);
                    cgstd = cgstd + parseFloat(data[x].cgst);
                    sgstd = sgstd + parseFloat(data[x].sgst);
                }
                else if (((data[x].issuereceived === 'issue') && (data[x].debitcredit === 'debit')) || ((data[x].issuereceived === 'received') && (data[x].debitcredit === 'credit'))) {
                    datarow.push(<div><div className=" d-flex small p-1 d-md-none d-lg-none" style={i % 2 === 0 ? { backgroundColor: "rgb(181, 253, 233)" } : {}}>
                        <div className="col-4 col-md-5 p-1">
                            <div className="col-12 col-md-6 p-0"><strong>{data[x].voucherno}/{data[x].date}</strong></div>
                            <div className="col-12 col-md-6 p-0" style={{ lineHeight: "1" }}>{data[x].debitcredit.toUpperCase() + " NOTE / " + data[x].issuereceived}</div>
                        </div>
                        <div className="col-4 col-md-1">
                        </div>
                        <div className="col-4 col-md-6 p-0">
                            <div className="col-12 p-0">{parseFloat(data[x].igst) > 0 ? "I: " + parseFloat(data[x].igst).toFixed(2) : ''}</div>
                            <div className="col-12 p-0">{parseFloat(data[x].cgst) > 0 ? "C: " + parseFloat(data[x].cgst).toFixed(2) : ''}</div>
                            <div className="col-12 p-0">{parseFloat(data[x].sgst) > 0 ? "S: " + parseFloat(data[x].sgst).toFixed(2) : ''}</div>
                        </div>
                    </div>
                        <div className="container d-none d-md-block p-1" style={i % 2 === 0 ? { backgroundColor: "rgb(181, 253, 233)" } : {}}>
                            <div className="d-flex small">
                                <div className="col-1 p-0">{data[x].date}</div>
                                <div className="col-1 p-0">{data[x].voucherno}</div>
                                <div className="col-4 p-0"><strong>{data[x].debitcredit.toUpperCase() + " NOTE / " + data[x].issuereceived}</strong></div>
                                <div className="col-2"><strong></strong></div>
                                <div className="col-3 text-right"><div className="col-12 p-0">{parseFloat(data[x].igst) > 0 ? "I: " + parseFloat(data[x].igst).toFixed(2) : ''}</div>
                                    <div className="col-12 p-0">{parseFloat(data[x].cgst) > 0 ? "C: " + parseFloat(data[x].cgst).toFixed(2) : ''}</div>
                                    <div className="col-12 p-0">{parseFloat(data[x].sgst) > 0 ? "S: " + parseFloat(data[x].sgst).toFixed(2) : ''}</div></div>
                            </div>
                        </div></div>)
                    igstc = igstc + parseFloat(data[x].igst);
                    cgstc = cgstc + parseFloat(data[x].cgst);
                    sgstc = sgstc + parseFloat(data[x].sgst);
                }
            }
            x++;
            i++;
        }

        igstdt = igstdt + igstd; cgstdt = cgstdt + cgstd; sgstdt = sgstdt + sgstd;
        igstct = igstct + igstc; cgstct = cgstct + cgstc; sgstct = sgstct + sgstc;
    })
    datarow.push(<div>
        <div className=" d-flex small p-1 d-md-none d-lg-none">
            <div className="col-4 col-md-5 p-1"></div>
            <div className="col-4 col-md-1 border-top border-dark text-right">{parseFloat(igstdt + cgstdt + sgstdt).toFixed(2)}</div>
            <div className="col-4 col-md-1 border-top border-dark text-right">{parseFloat(igstct + cgstct + sgstct).toFixed(2)}</div>
        </div>
        <div className=" d-flex small mb-1 p-1 d-md-none d-lg-none">
            <div className="col-4 col-md-5 p-1">{(igstct + cgstct + sgstct) - (igstdt + cgstdt + sgstdt) <= 0 ? "GST Input" : "GST Payable"}</div>
            <div className="col-4 col-md-1 text-right">{(igstct + cgstct + sgstct) - (igstdt + cgstdt + sgstdt) > 0 ? parseFloat((igstct + cgstct + sgstct) - (igstdt + cgstdt + sgstdt)).toFixed(2) : ""}</div>
            <div className="col-4 col-md-1 text-right">{(igstct + cgstct + sgstct) - (igstdt + cgstdt + sgstdt) <= 0 ? parseFloat((igstdt + cgstdt + sgstdt) - (igstct + cgstct + sgstct)).toFixed(2) : ""}</div>
        </div>
        <div className="container small d-none d-md-block p-1">
            <div className="d-flex">
                <div className="col-1 p-0"><strong></strong></div>
                <div className="col-1 p-0"><strong></strong></div>
                <div className="col-4 p-0"><strong></strong></div>
                <div className="col-2 border-top border-dark text-right">{parseFloat(igstdt + cgstdt + sgstdt).toFixed(2)}</div>
                <div className="col-3 border-top border-dark text-right">{parseFloat(igstct + cgstct + sgstct).toFixed(2)}</div>
            </div>
        </div>
        <div className="container small d-none d-md-block p-1">
            <div className="d-flex">
                <div className="col-1 p-0"><strong></strong></div>
                <div className="col-1 p-0"><strong></strong></div>
                <div className="col-4 p-0"><strong>{(igstct + cgstct + sgstct) - (igstdt + cgstdt + sgstdt) <= 0 ? "GST Input" : "GST Payable"}</strong></div>
                <div className="col-2 border-top border-dark text-right">{(igstct + cgstct + sgstct) - (igstdt + cgstdt + sgstdt) > 0 ? parseFloat((igstct + cgstct + sgstct) - (igstdt + cgstdt + sgstdt)).toFixed(2) : ""}</div>
                <div className="col-3 border-top border-dark text-right">{(igstct + cgstct + sgstct) - (igstdt + cgstdt + sgstdt) <= 0 ? parseFloat((igstdt + cgstdt + sgstdt) - (igstct + cgstct + sgstct)).toFixed(2) : ""}</div>
            </div>
        </div>
    </div>)
    return datarow;
}

let igstdt = 0, cgstdt = 0, sgstdt = 0, igstct = 0, cgstct = 0, sgstct = 0, gstbalance = 0;
const GSTDataPage = function ({ data, y, getGSTOpeningBalance }) {
    let returnrows = [];
    if (y > 1) {
        returnrows.push(
            <View style={pdfstyles.itemSectionLedger, { textAlign: "center", marginTop: "3px" }}>
                <View style={pdfstyles.itemHeader}><Text style={{ width: '75px', textAlign: "left" }}></Text><Text style={{ width: '75px', textAlign: "left" }}></Text><Text style={{ width: '200px', textAlign: "left", fontFamily: "Helvetica-Bold" }}>Carry Forward</Text><View style={{ width: '75px' }}><Text style={{ width: '75px', textAlign: "right" }}>{"I : " + parseFloat(igstdt).toFixed(2)}</Text><Text style={{ width: '75px', textAlign: "right" }}>{"C : " + parseFloat(cgstdt).toFixed(2)}</Text><Text style={{ width: '75px', textAlign: "right" }}>{"S : " + parseFloat(sgstdt).toFixed(2)}</Text></View><View style={{ width: '75px' }}><Text style={{ width: '75px', textAlign: "right" }}>{"I : " + parseFloat(igstct).toFixed(2)}</Text><Text style={{ width: '75px', textAlign: "right" }}>{"C : " + parseFloat(cgstct).toFixed(2)}</Text><Text style={{ width: '75px', textAlign: "right" }}>{"S : " + parseFloat(sgstct).toFixed(2)}</Text></View><Text style={{ width: '75px', textAlign: "right" }}>{parseFloat(gstbalance).toFixed(2)}</Text></View>
            </View>
        )
    }
    else if (y === 1 && getGSTOpeningBalance.inputpayable === 'input') {
        returnrows.push(
            <View style={pdfstyles.itemSectionLedger, { textAlign: "center", marginTop: "3px" }}>
                <View style={pdfstyles.itemHeader}><Text style={{ width: '75px', textAlign: "left" }}></Text><Text style={{ width: '75px', textAlign: "left" }}></Text><Text style={{ width: '200px', textAlign: "left", fontFamily: "Helvetica-Bold" }}>Opening Balance (Input)</Text><Text style={{ width: '75px', textAlign: "right" }}>{parseFloat(getGSTOpeningBalance.gst).toFixed(2)}</Text><Text style={{ width: '75px', textAlign: "right" }}></Text><Text style={{ width: '75px', textAlign: "right" }}>{parseFloat(gstbalance).toFixed(2)}</Text></View>
            </View>
        )
    }
    else if (y === 1 && getGSTOpeningBalance.inputpayable === 'payable') {
        returnrows.push(
            <View style={pdfstyles.itemSectionLedger, { textAlign: "center", marginTop: "3px" }}>
                <View style={pdfstyles.itemHeader}><Text style={{ width: '75px', textAlign: "left" }}></Text><Text style={{ width: '75px', textAlign: "left" }}></Text><Text style={{ width: '200px', textAlign: "left", fontFamily: "Helvetica-Bold" }}>Opening Balance (Payable)</Text><Text style={{ width: '75px', textAlign: "right" }}></Text><Text style={{ width: '75px', textAlign: "right" }}>{parseFloat(getGSTOpeningBalance.gst).toFixed(2)}</Text><Text style={{ width: '75px', textAlign: "right" }}>{parseFloat(gstbalance).toFixed(2)}</Text></View>
            </View>
        )
    }
    for (let x = (y - 1) * 50; x < y * 50 && x < data.length; x++) {
        if ((data[x].type === 'sales') && (data[x].totaligst > 0 || data[x].totalcgst > 0 || data[x].totalsgst > 0)) {
            igstct = igstct + parseFloat(data[x].totaligst);
            cgstct = cgstct + parseFloat(data[x].totalcgst);
            sgstct = sgstct + parseFloat(data[x].totalsgst);
            gstbalance = gstbalance - parseFloat(data[x].totaligst) - parseFloat(data[x].totalcgst) - parseFloat(data[x].totalsgst);
            returnrows.push(
                <View style={pdfstyles.itemSectionLedger, { textAlign: "center", marginTop: "3px" }}>
                    <View style={pdfstyles.itemHeader}><Text style={{ width: '75px', textAlign: "left" }}>{data[x].invoicedate}</Text><Text style={{ width: '75px', textAlign: "left" }}>{data[x].invoicenumber}</Text><Text style={{ width: '200px', textAlign: "left", fontFamily: "Helvetica-Bold" }}>Sales</Text><View style={{ width: '75px' }}><Text style={{ width: '75px', textAlign: "right" }}></Text><Text style={{ width: '75px', textAlign: "right" }}></Text><Text style={{ width: '75px', textAlign: "right" }}></Text></View><View style={{ width: '75px' }}><Text style={{ width: '75px', textAlign: "right" }}>{"I : " + parseFloat(data[x].totaligst).toFixed(2)}</Text><Text style={{ width: '75px', textAlign: "right" }}>{"C : " + parseFloat(data[x].totalcgst).toFixed(2)}</Text><Text style={{ width: '75px', textAlign: "right" }}>{"S : " + parseFloat(data[x].totalsgst).toFixed(2)}</Text></View><Text style={{ width: '75px', textAlign: "right" }}>{parseFloat(gstbalance).toFixed(2)}</Text></View>
                </View>
            )
        }
        else if ((data[x].type === 'purchase') && (data[x].totaligst > 0 || data[x].totalcgst > 0 || data[x].totalsgst > 0)) {
            igstdt = igstdt + parseFloat(data[x].totaligst);
            cgstdt = cgstdt + parseFloat(data[x].totalcgst);
            sgstdt = sgstdt + parseFloat(data[x].totalsgst);
            gstbalance = gstbalance + parseFloat(data[x].totaligst) + parseFloat(data[x].totalcgst) + parseFloat(data[x].totalsgst);
            returnrows.push(
                <View style={pdfstyles.itemSectionLedger, { textAlign: "center", marginTop: "3px" }}>
                    <View style={pdfstyles.itemHeader}><Text style={{ width: '75px', textAlign: "left" }}>{data[x].invoicedate}</Text><Text style={{ width: '75px', textAlign: "left" }}>{data[x].invoicenumber}</Text><Text style={{ width: '200px', textAlign: "left", fontFamily: "Helvetica-Bold" }}>Purchase</Text><View style={{ width: '75px' }}><Text style={{ width: '75px', textAlign: "right" }}>{"I : " + parseFloat(data[x].totaligst).toFixed(2)}</Text><Text style={{ width: '75px', textAlign: "right" }}>{"C : " + parseFloat(data[x].totalcgst).toFixed(2)}</Text><Text style={{ width: '75px', textAlign: "right" }}>{"S : " + parseFloat(data[x].totalsgst).toFixed(2)}</Text></View><View style={{ width: '75px' }}><Text style={{ width: '75px', textAlign: "left" }}></Text><Text style={{ width: '75px', textAlign: "left" }}></Text><Text style={{ width: '75px', textAlign: "left" }}></Text></View><Text style={{ width: '75px', textAlign: "right" }}>{parseFloat(gstbalance).toFixed(2)}</Text></View>
                </View>
            )
        }
        else if (data[x].type === 'asset') {
            if ((data[x].type1 === 'assetsell') && (data[x].soldigst > 0 || data[x].soldcgst > 0 || data[x].soldsgst > 0)) {
                igstct = igstct + parseFloat(data[x].soldigst);
                cgstct = cgstct + parseFloat(data[x].soldcgst);
                sgstct = sgstct + parseFloat(data[x].soldsgst);
                gstbalance = gstbalance - parseFloat(data[x].soldigst) - parseFloat(data[x].soldcgst) - parseFloat(data[x].soldsgst);
                returnrows.push(
                    <View style={pdfstyles.itemSectionLedger, { textAlign: "center", marginTop: "3px" }}>
                        <View style={pdfstyles.itemHeader}><Text style={{ width: '75px', textAlign: "left" }}>{data[x].date}</Text><Text style={{ width: '75px', textAlign: "left" }}>{data[x].voucherno}</Text><Text style={{ width: '200px', textAlign: "left", fontFamily: "Helvetica-Bold" }}>Asset Sales</Text><View style={{ width: '75px' }}><Text style={{ width: '75px', textAlign: "right" }}></Text><Text style={{ width: '75px', textAlign: "right" }}></Text><Text style={{ width: '75px', textAlign: "right" }}></Text></View><View style={{ width: '75px' }}><Text style={{ width: '75px', textAlign: "right" }}>{"I : " + parseFloat(data[x].soldigst).toFixed(2)}</Text><Text style={{ width: '75px', textAlign: "right" }}>{"C : " + parseFloat(data[x].soldcgst).toFixed(2)}</Text><Text style={{ width: '75px', textAlign: "right" }}>{"S : " + parseFloat(data[x].soldsgst).toFixed(2)}</Text></View><Text style={{ width: '75px', textAlign: "right" }}>{parseFloat(gstbalance).toFixed(2)}</Text></View>
                    </View>
                )
            }
            if ((data[x].type2 === 'assetpurchase') && (data[x].purchaseigst > 0 || data[x].purchasecgst > 0 || data[x].purchasesgst > 0)) {
                igstdt = igstdt + parseFloat(data[x].purchaseigst);
                cgstdt = cgstdt + parseFloat(data[x].purchasecgst);
                sgstdt = sgstdt + parseFloat(data[x].purchasesgst);
                gstbalance = gstbalance + parseFloat(data[x].purchaseigst) + parseFloat(data[x].purchasecgst) + parseFloat(data[x].purchasesgst);
                returnrows.push(
                    <View style={pdfstyles.itemSectionLedger, { textAlign: "center", marginTop: "3px" }}>
                        <View style={pdfstyles.itemHeader}><Text style={{ width: '75px', textAlign: "left" }}>{data[x].date}</Text><Text style={{ width: '75px', textAlign: "left" }}>{data[x].voucherno}</Text><Text style={{ width: '200px', textAlign: "left", fontFamily: "Helvetica-Bold" }}>Asset Purchase</Text><View style={{ width: '75px' }}><Text style={{ width: '75px', textAlign: "right" }}>{"I : " + parseFloat(data[x].purchaseigst).toFixed(2)}</Text><Text style={{ width: '75px', textAlign: "right" }}>{"C : " + parseFloat(data[x].purchasecgst).toFixed(2)}</Text><Text style={{ width: '75px', textAlign: "right" }}>{"S : " + parseFloat(data[x].purchasesgst).toFixed(2)}</Text></View><View style={{ width: '75px' }}><Text style={{ width: '75px', textAlign: "right" }}></Text><Text style={{ width: '75px', textAlign: "right" }}></Text><Text style={{ width: '75px', textAlign: "right" }}></Text></View><Text style={{ width: '75px', textAlign: "right" }}>{parseFloat(gstbalance).toFixed(2)}</Text></View>
                    </View>
                )
            }
        }
        else if (data[x].type === 'payment') {
            igstdt = igstdt + parseFloat(data[x].amount);
            gstbalance = gstbalance + parseFloat(data[x].amount);
            returnrows.push(
                <View style={pdfstyles.itemSectionLedger, { textAlign: "center", marginTop: "3px" }}>
                    <View style={pdfstyles.itemHeader}><Text style={{ width: '75px', textAlign: "left" }}>{data[x].date}</Text><Text style={{ width: '75px', textAlign: "left" }}>{data[x].voucherno}</Text><Text style={{ width: '200px', textAlign: "left", fontFamily: "Helvetica-Bold" }}>Payment - {data[x].vendorid}</Text><View style={{ width: '75px' }}><Text style={{ width: '75px', textAlign: "right" }}>{data[x].amount}</Text><Text style={{ width: '75px', textAlign: "right" }}></Text><Text style={{ width: '75px', textAlign: "right" }}></Text></View><View style={{ width: '75px' }}><Text style={{ width: '75px', textAlign: "right" }}></Text><Text style={{ width: '75px', textAlign: "right" }}></Text><Text style={{ width: '75px', textAlign: "right" }}></Text></View><Text style={{ width: '75px', textAlign: "right" }}>{parseFloat(gstbalance).toFixed(2)}</Text></View>
                </View>
            )
        }
        else if (data[x].type === 'debitcredit') {
            if (((data[x].issuereceived === 'received') && (data[x].debitcredit === 'debit')) || ((data[x].issuereceived === 'issue') && (data[x].debitcredit === 'credit'))) {
                igstdt = igstdt + parseFloat(data[x].igst);
                cgstdt = cgstdt + parseFloat(data[x].cgst);
                sgstdt = sgstdt + parseFloat(data[x].sgst);
                gstbalance = gstbalance + parseFloat(data[x].igst) + parseFloat(data[x].cgst) + parseFloat(data[x].sgst);
                returnrows.push(
                    <View style={pdfstyles.itemSectionLedger, { textAlign: "center", marginTop: "3px" }}>
                        <View style={pdfstyles.itemHeader}><Text style={{ width: '75px', textAlign: "left" }}>{data[x].date}</Text><Text style={{ width: '75px', textAlign: "left" }}>{data[x].voucherno}</Text><Text style={{ width: '200px', textAlign: "left", fontFamily: "Helvetica-Bold" }}>Credit Note - Against Sale</Text><View style={{ width: '75px' }}><Text style={{ width: '75px', textAlign: "right" }}>{"I : " + parseFloat(data[x].igst).toFixed(2)}</Text><Text style={{ width: '75px', textAlign: "right" }}>{"C : " + parseFloat(data[x].cgst).toFixed(2)}</Text><Text style={{ width: '75px', textAlign: "right" }}>{"S : " + parseFloat(data[x].sgst).toFixed(2)}</Text></View><View style={{ width: '75px' }}><Text style={{ width: '75px', textAlign: "right" }}></Text><Text style={{ width: '75px', textAlign: "right" }}></Text><Text style={{ width: '75px', textAlign: "right" }}></Text></View><Text style={{ width: '75px', textAlign: "right" }}>{parseFloat(gstbalance).toFixed(2)}</Text></View>
                    </View>
                )
            }
            else if (((data[x].issuereceived === 'issue') && (data[x].debitcredit === 'debit')) || ((data[x].issuereceived === 'received') && (data[x].debitcredit === 'credit'))) {
                igstct = igstct + parseFloat(data[x].igst);
                cgstct = cgstct + parseFloat(data[x].cgst);
                sgstct = sgstct + parseFloat(data[x].sgst);
                gstbalance = gstbalance - parseFloat(data[x].igst) - parseFloat(data[x].cgst) - parseFloat(data[x].sgst);
                returnrows.push(
                    <View style={pdfstyles.itemSectionLedger, { textAlign: "center", marginTop: "3px" }}>
                        <View style={pdfstyles.itemHeader}><Text style={{ width: '75px', textAlign: "left" }}>{data[x].date}</Text><Text style={{ width: '75px', textAlign: "left" }}>{data[x].voucherno}</Text><Text style={{ width: '200px', textAlign: "left", fontFamily: "Helvetica-Bold" }}>Credit Note - Against Purchase</Text><View style={{ width: '75px' }}><Text style={{ width: '75px', textAlign: "right" }}></Text><Text style={{ width: '75px', textAlign: "right" }}></Text><Text style={{ width: '75px', textAlign: "right" }}></Text></View><View style={{ width: '75px' }}><Text style={{ width: '75px', textAlign: "right" }}>{"I : " + parseFloat(data[x].igst).toFixed(2)}</Text><Text style={{ width: '75px', textAlign: "right" }}>{"C : " + parseFloat(data[x].cgst).toFixed(2)}</Text><Text style={{ width: '75px', textAlign: "right" }}>{"S : " + parseFloat(data[x].sgst).toFixed(2)}</Text></View><Text style={{ width: '75px', textAlign: "right" }}>{parseFloat(gstbalance).toFixed(2)}</Text></View>
                    </View>
                )
            }
        }
    }

    if (y === Math.ceil(data.length / 50)) {
        returnrows.push(<View><View style={pdfstyles.itemSectionLedger}>
            <View style={pdfstyles.itemHeader}><Text style={{ width: '75px', textAlign: "left" }}></Text><Text style={{ width: '75px', textAlign: "left" }}></Text><Text style={{ width: '200px', textAlign: "left" }}></Text><Text style={{ width: '75px', borderTop: "1px solid black", textAlign: "right" }}>{parseFloat(igstdt + cgstdt + sgstdt).toFixed(2)}</Text><Text style={{ width: '75px', borderTop: "1px solid black", textAlign: "right" }}>{parseFloat(igstct + cgstct + sgstct).toFixed(2)}</Text></View>
        </View>
            <View style={pdfstyles.itemSectionLedger}>
                <View style={pdfstyles.itemHeader}><Text style={{ width: '75px', textAlign: "left" }}></Text><Text style={{ width: '75px', textAlign: "left" }}></Text><Text style={{ width: '200px', textAlign: "left" }}></Text><Text style={{ width: '75px', textAlign: "right" }}>{gstbalance < 0 ? parseFloat(-1 * gstbalance).toFixed(2) : ''}</Text><Text style={{ width: '75px', textAlign: "right" }}>{gstbalance >= 0 ? parseFloat(gstbalance).toFixed(2) : ''}</Text><Text style={{ width: '75px', textAlign: "right" }}></Text></View>
            </View>
            <View style={pdfstyles.itemSectionLedger, { fontFamily:"Helvetica-Bold", borderTop: "1px solid black", borderBottom: "1px solid black" }}>
                <View style={pdfstyles.itemHeader}><Text style={{ width: '75px', textAlign: "left" }}></Text><Text style={{ width: '75px', textAlign: "left" }}></Text><Text style={{ width: '200px', textAlign: "left", fontFamily: "Helvetica-Bold" }}>Total</Text><Text style={{ width: '75px', textAlign: "right" }}>{Math.abs(igstdt + cgstdt + sgstdt) > Math.abs(igstct + cgstct + sgstct) ? Math.abs(igstdt + cgstdt + sgstdt).toFixed(2) : Math.abs(igstct + cgstct + sgstct).toFixed(2)}</Text><Text style={{ width: '75px', textAlign: "right" }}>{Math.abs(igstdt + cgstdt + sgstdt) > Math.abs(igstct + cgstct + sgstct) ? Math.abs(igstdt + cgstdt + sgstdt).toFixed(2) : Math.abs(igstct + cgstct + sgstct).toFixed(2)}</Text><Text style={{ width: '75px', textAlign: "right" }}></Text></View>
            </View></View>)
    }
    else {
        returnrows.push(<View><View style={pdfstyles.itemSectionLedger, { fontFamily: "Helvetica-Bold", marginTop: "2px" }}>
            <View style={pdfstyles.itemHeader}><Text style={{ width: '75px', textAlign: "left" }}></Text><Text style={{ width: '75px', textAlign: "left" }}></Text><Text style={{ width: '200px', textAlign: "left" }}></Text><Text style={{ width: '75px', borderTop: "1px solid black", textAlign: "left" }}>{parseFloat(igstdt + cgstdt + sgstdt).toFixed(2)}</Text><Text style={{ width: '75px', borderTop: "1px solid black", textAlign: "right" }}>{parseFloat(igstct + cgstct + sgstct).toFixed(2)}</Text><Text style={{ width: '75px', borderTop: "1px solid black", textAlign: "right" }}>{parseFloat(gstbalance).toFixed(2)}</Text><Text style={{ width: '75px', textAlign: "right" }}></Text></View>
        </View>
            <View style={pdfstyles.itemSectionLedger, { fontFamily: "Helvetica", marginTop: "5px", textAlign: "right" }}><Text></Text>...Continued</View></View>)
    }
    return returnrows;
}
const Report = function ({ business, data, l, n, period, getGSTOpeningBalance }) {
    let returnrows = [];
    igstdt = 0;
    cgstdt = 0;
    sgstdt = 0;
    igstct = 0;
    cgstct = 0;
    sgstct = 0;
    igstdt = getGSTOpeningBalance.inputpayable === 'input' ? parseFloat(getGSTOpeningBalance.gst) : 0;
    igstct = getGSTOpeningBalance.inputpayable === 'payable' ? parseFloat(getGSTOpeningBalance.gst) : 0;
    if (getGSTOpeningBalance.inputpayable === 'input') gstbalance = gstbalance + parseFloat(getGSTOpeningBalance.gst);
    else if (getGSTOpeningBalance.inputpayable === 'payable') gstbalance = gstbalance - parseFloat(getGSTOpeningBalance.gst);

    for (let y = 1; y <= n; y++) {
        returnrows.push(<Page size="A4" style={pdfstyles.page}>
            {y === 1 ? <View><View style={pdfstyles.taxInvoiceHeader, { marginTop: "20px", textAlign: "center" }}>
                <View style={{ fontFamily: "Helvetica-Bold", width: "500px" }}><Text>{business.businessname}</Text></View>
                <View style={{ width: "500px" }}><Text>{business.address}</Text></View>
            </View>
                <View style={pdfstyles.taxInvoiceHeader, { marginTop: "10px", textAlign: "center" }}>
                    <View style={{ fontFamily: "Helvetica-Bold", textDecoration: "underline", width: "500px" }}><Text>GST Ledger</Text></View>
                    <View style={{ width: "500px" }}><Text>{period}</Text></View>
                </View></View> : ''}
            <View style={pdfstyles.itemSectionLedger, { marginTop: "20px", marginBottom: "5px", fontFamily: "Helvetica-Bold", textAlign: "center", borderTop: "1px solid black", borderBottom: "1px solid black" }}>
                <View style={pdfstyles.itemHeader}><Text style={{ width: '75px', textAlign: "left" }}>Date</Text><Text style={{ width: '75px', textAlign: "left" }}>Voucher No</Text><Text style={{ width: '200px', textAlign: "left" }}>Particulars</Text><Text style={{ width: '75px', textAlign: "right" }}>Debit</Text><Text style={{ width: '75px', textAlign: "right" }}>Credit</Text><Text style={{ width: '75px', textAlign: "right" }}>Balance</Text></View>
            </View>
            <GSTDataPage data={data} y={y} getGSTOpeningBalance={getGSTOpeningBalance} />
        </Page >)
    }
    return returnrows;
}
const GSTLedger = function ({ data, business, fy, getGSTOpeningBalance }) {
    let l = data.length, i = 0, n = Math.ceil(l / 50), y = 0;
    return (
        <Document>
            <Report business={business} data={data} l={l} n={n} period={fy} getGSTOpeningBalance={getGSTOpeningBalance} />
        </Document>
    )
}
class GSTStatement extends Component {
    constructor(props) {
        super(props);
        this.state = {
            monthSelection: ''
        }
        window.onload = () => {
            this.props.resetPurchaseDetail();
            this.props.resetSalesDetail();
            this.props.resetGSTOpeningBalance();
            this.props.refreshDebitCredit();
            this.props.resetPaymentDetail();
            this.props.refreshAssetAccountState();
        }
    }
    componentDidMount() {
        if (this.props.purchaseDetail.message === 'initial') this.props.getPurchaseData(this.props.businessid, this.props.fyid);
        if (this.props.salesDetail.message === 'initial') this.props.getSalesData(this.props.businessid, this.props.fyid);
        if (this.props.getGSTOpeningBalance.message === 'initial') this.props.getGSTOpeningBalanceDetail(this.props.businessid, this.props.fyid);
        if (this.props.getAssetAccount.message === 'initial') this.props.getAssetAccountList(this.props.businessid);
        if (this.props.getDebitCredit.message === 'initial') this.props.getDebitCreditList(this.props.businessid, this.props.fyid);
        if (this.props.getPayment.message === 'initial') this.props.getPaymentDetail(this.props.businessid, this.props.fyid);
        if (this.props.logoutMessage.error !== '') {
            alert("Error Logging Out. Please Try Again");
            this.props.resetLogoutMessage();
        }
    }
    handleInputChange = (event) => {
        let target = event.target;
        let name = target.name;
        let value = target.value;
        this.setState({
            ...this.state,
            [name]: value
        })
    }
    genereateSalesPdf = async (business, fy, monthWiseGSTDebit, monthWiseGSTCredit, monthWiseDebitCredit, getGSTOpeningBalance) => {
        try {
            let month = ['apr', 'may', 'jun', 'jul', 'aug', 'sep', 'oct', 'nov', 'dec', 'jan', 'feb', 'mar'];
            let x = 0;
            let data = [];
            month.forEach(month => {
                data = data.concat(monthWiseGSTDebit[month]);
                data = data.concat(monthWiseGSTCredit[month]);
                data = data.concat(monthWiseDebitCredit[month]);
            })
            data = data.sort(sortData);
            const doc = <GSTLedger data={data} business={business} fy={fy} getGSTOpeningBalance={getGSTOpeningBalance} />;
            const blobPdf = pdf(doc);
            blobPdf.updateContainer(doc);
            const result = await blobPdf.toBlob();
            let filename = "GST Ledger.pdf";
            saveAs(result, filename);
        }
        catch (err) {
            alert(err);
        }
    }
    render() {
        if (!cookie.load('userservice')) return <Redirect to="/" />;
        else {
            if ((this.props.logoutMessage.loading === true) || (this.props.salesDetail.loading === true) || (this.props.getGSTOpeningBalance.loading === true) || (this.props.logoutMessage.loading === true) || (this.props.getPayment.loading === true)) {
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div className="mt-3 d-flex justify-content-center">
                            <button className="btn btn-danger">Loading... </button>
                        </div>
                    </div>
                );
            }
            else if ((this.props.salesDetail.error !== '') || (this.props.getGSTOpeningBalance.error !== '') || (this.props.getPayment.error !== '')) {
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div className="mt-3 d-flex justify-content-center">
                            <button className="btn btn-danger">Error.. Please Refresh and Try Again </button>
                        </div>
                    </div>
                );
            }
            else {
                let business = this.props.businessList.data.filter(b => b.businessid === this.props.businessid)[0];
                let fy = business.financial.filter(f => f.fyid === this.props.fyid)[0].financialyear;
                let monthWiseGSTDebit = {
                    apr: [],
                    may: [],
                    jun: [],
                    jul: [],
                    aug: [],
                    sep: [],
                    oct: [],
                    nov: [],
                    dec: [],
                    jan: [],
                    feb: [],
                    mar: []
                }
                let monthWiseGSTCredit = {
                    apr: [],
                    may: [],
                    jun: [],
                    jul: [],
                    aug: [],
                    sep: [],
                    oct: [],
                    nov: [],
                    dec: [],
                    jan: [],
                    feb: [],
                    mar: []
                }
                let monthWiseDebitCredit = {
                    apr: [],
                    may: [],
                    jun: [],
                    jul: [],
                    aug: [],
                    sep: [],
                    oct: [],
                    nov: [],
                    dec: [],
                    jan: [],
                    feb: [],
                    mar: []
                }

                let monthWiseOpeningClosing = {
                    apr: { gstopening: this.props.getGSTOpeningBalance.gst, cgstclosing: '', sgstclosing: '', igstclosing: '', td: 0, tc: 0 },
                    may: { igstopening: '', sgstopening: '', cgstopening: '', cgstclosing: '', sgstclosing: '', igstclosing: '', td: 0, tc: 0 },
                    jun: { igstopening: '', sgstopening: '', cgstopening: '', cgstclosing: '', sgstclosing: '', igstclosing: '', td: 0, tc: 0 },
                    jul: { igstopening: '', sgstopening: '', cgstopening: '', cgstclosing: '', sgstclosing: '', igstclosing: '', td: 0, tc: 0 },
                    aug: { igstopening: '', sgstopening: '', cgstopening: '', cgstclosing: '', sgstclosing: '', igstclosing: '', td: 0, tc: 0 },
                    sep: { igstopening: '', sgstopening: '', cgstopening: '', cgstclosing: '', sgstclosing: '', igstclosing: '', td: 0, tc: 0 },
                    oct: { igstopening: '', sgstopening: '', cgstopening: '', cgstclosing: '', sgstclosing: '', igstclosing: '', td: 0, tc: 0 },
                    nov: { igstopening: '', sgstopening: '', cgstopening: '', cgstclosing: '', sgstclosing: '', igstclosing: '', td: 0, tc: 0 },
                    dec: { igstopening: '', sgstopening: '', cgstopening: '', cgstclosing: '', sgstclosing: '', igstclosing: '', td: 0, tc: 0 },
                    jan: { igstopening: '', sgstopening: '', cgstopening: '', cgstclosing: '', sgstclosing: '', igstclosing: '', td: 0, tc: 0 },
                    feb: { igstopening: '', sgstopening: '', cgstopening: '', cgstclosing: '', sgstclosing: '', igstclosing: '', td: 0, tc: 0 },
                    mar: { igstopening: '', sgstopening: '', cgstopening: '', cgstclosing: '', sgstclosing: '', igstclosing: '', td: 0, tc: 0 },
                }
                this.props.salesDetail.data.map(d => {
                    if (d.type === "sales" && d.active == 0) monthWiseGSTCredit[d.invoicedate.split(" ")[0].toLowerCase()].push(d)
                })
                this.props.getAssetAccount.data.map(d => {
                    if (d.soldfyid === this.props.fyid) {
                        d.typesell = "assetsell";
                        monthWiseGSTCredit[d.solddate.split(" ")[0].toLowerCase()].push(d);
                    }
                })
                this.props.purchaseDetail.data.map(d => {
                    monthWiseGSTDebit[d.invoicedate.split(" ")[0].toLowerCase()].push(d);
                })
                this.props.getAssetAccount.data.map(d => {
                    if (d.purchasefyid === this.props.fyid) {
                        d.typepurchase = "assetpurchase";
                        monthWiseGSTDebit[d.purchasedate.split(" ")[0].toLowerCase()].push(d);
                    }
                })
                this.props.getPayment.data.map(d => {
                    if ((d.vendorid === 'reversecharge') || (d.vendorid === 'gst')) {
                        monthWiseGSTDebit[d.date.split(" ")[0].toLowerCase()].push(d);
                    }
                })
                this.props.getDebitCredit.data.map(d => {
                    monthWiseDebitCredit[d.date.split(" ")[0].toLowerCase()].push(d);
                })
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div style={{ minHeight: "93vh", display: "flex", flexDirection: "column" }}>
                            <div className="container" style={{ flex: "1" }}>
                                <h6 className="text-center pl-2 pr-2 pt-4" style={{ lineHeight: "1" }}><strong>{business.businessname} {fy}</strong></h6>
                                <h6 className="text-center pl-2 pr-2 mb-4" style={{ lineHeight: "1" }}>{business.alias ? "(" + business.alias + ")" : ''}</h6>
                                <h6 className="text-center pl-2 pr-2"><strong><u>GST Statement</u></strong>&nbsp;&nbsp;&nbsp;<FontAwesomeIcon icon={faFilePdf} size="lg" onClick={() => this.genereateSalesPdf(business, fy, monthWiseGSTDebit, monthWiseGSTCredit, monthWiseDebitCredit, this.props.getGSTOpeningBalance)} />&nbsp;&nbsp;&nbsp;</h6>
                                <DataDisplay monthWiseGSTSale={monthWiseGSTCredit} monthWiseGSTPurchase={monthWiseGSTDebit} monthWiseOpeningClosing={monthWiseOpeningClosing} monthWiseDebitCredit={monthWiseDebitCredit} getGSTOpeningBalance={this.props.getGSTOpeningBalance} />
                            </div>
                            <Footer />
                        </div>
                    </div>
                )
            }
        }
    }
}
export default GSTStatement;