import React from 'react';
import { style } from '../sylesheet/stylesheet';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSignOutAlt, faKey, faArrowCircleLeft, faArrowCircleRight } from '@fortawesome/free-solid-svg-icons';
import { Link } from 'react-router-dom';
import { useHistory } from "react-router";

export const Header = function ({ logout }) {
    const history = useHistory();
    return (
        <div className="d-flex justify-content-center" style={style.loginHeader}>
            <div className="col-md-10 mt-2 text-white">
                <div className="row">
                    <div className="col-6 col-md-4 text-white"><Link to={`/`} style={{ textDecoration: "none", cursor: "pointer" }}><h4 className="text-white">Aayuda</h4></Link></div>
                    <div className="col-md-4 d-none d-md-block"></div>
                    <div className="col-6 col-md-4 text-right">
                        <FontAwesomeIcon icon={faArrowCircleLeft} size="lg" style={{ color: "white", marginRight: "15px", cursor: "pointer" }} onClick={() => history.goBack()} />
                        <FontAwesomeIcon icon={faArrowCircleRight} size="lg" style={{ color: "white", marginRight: "15px", cursor: "pointer" }} onClick={() => history.goForward()} />
                        <Link to={`/business/changepassword`} style={{ color: "white" }}><FontAwesomeIcon icon={faKey} size="lg" style={style.logout} className="mr-3" /></Link>
                        <FontAwesomeIcon icon={faSignOutAlt} size="lg" style={style.logout} onClick={() => logout()} />
                    </div>
                </div>
            </div>
        </div>
    );
}
