import React, { Component } from 'react';
import cookie from 'react-cookie';
import { Redirect } from 'react-router-dom';
import { LocalForm, Control } from 'react-redux-form';
import { Header } from './HeaderComponent';
import { sortData } from '../redux/sortArrayofObjects';
import { Footer } from './Footer';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faFilePdf } from '@fortawesome/free-solid-svg-icons';
import { Page, Text, View, Document, pdf } from '@react-pdf/renderer';
import { pdfstyles } from '../sylesheet/stylesheet';
import { saveAs } from 'file-saver';

const DataRow = function ({ state, getPayment, fyid, businessList, businessid, getExpense, business, getEmployeeOpeningBalance }) {
    if (state.employee !== '') {
        let returnrows = [];
        let paymentData = getPayment.data.filter(p => p.specialpayment === 'payemployee' && p.vendorid === state.employee.split("...")[0]);
        let expenseData = getExpense.data.filter(p => p.employee === "1" && p.vendorid === state.employee.split("...")[0]);
        let data = [];
        data = data.concat(paymentData);
        data = data.concat(expenseData);
        data = data.sort(sortData);
        let x = 0, l = data.length, i = 2;
        let creditamount = 0, debitamount = 0;
        let openingbalance = getEmployeeOpeningBalance.data.filter(d => d.employeeaccount === state.employee)[0];
        if (openingbalance) {
            returnrows.push(
                <div>
                    <div className="d-flex small p-1 d-md-none d-lg-none" style={{ backgroundColor: "rgb(181, 253, 233)" }}>
                        <div className="col-5 col-md-5 p-1">
                            <div className="col-12 col-md-6 p-0"><strong>Apr/Opening Bal</strong></div>
                        </div>
                        <div className="col-3 col-md-1 text-right">{openingbalance.payablepaidadvance === 'paidadvance' ? parseFloat(openingbalance.amount).toFixed(2) : ''}
                        </div>
                        <div className="col-3 col-md-6 p-1">
                            <div className="col-12 col-md-2 p-0 text-right">{openingbalance.payablepaidadvance === 'payable' ? parseFloat(openingbalance.amount).toFixed(2) : ''}</div>
                        </div>
                        <div className="col-1"></div>
                    </div>
                    <div className="container small d-none d-md-block p-1" style={i % 2 === 0 ? { backgroundColor: "rgb(181, 253, 233)" } : {}}>
                        <div className="d-flex">
                            <div className="col-2 p-0">Apr/Opening Bal</div>
                            <div className="col-4 p-0">
                                <div className="col-12 col-md-6 p-0 "><strong>{openingbalance.payablepaidadvance === 'payable' ? "Payable" : "Paid Advance"}</strong></div>
                            </div>
                            <div className="col-2 text-right">{openingbalance.payablepaidadvance === 'paidadvance' ? parseFloat(openingbalance.amount).toFixed(2) : ''}</div>
                            <div className="col-3 text-right">{openingbalance.payablepaidadvance === 'payable' ? parseFloat(openingbalance.amount).toFixed(2) : ''}</div>
                        </div>
                    </div>
                </div>
            )
            i++;
            openingbalance.payablepaidadvance === 'paidadvance' ? debitamount = debitamount + parseFloat(openingbalance.amount) : creditamount = creditamount + parseFloat(openingbalance.amount);
        }
        while (x < l) {
            if (data[x].type === 'payment') {
                debitamount = debitamount + parseFloat(data[x].amount);
                returnrows.push(
                    <div>
                        <div className="d-flex small p-1 d-md-none d-lg-none" style={i % 2 === 0 ? { backgroundColor: "rgb(181, 253, 233)" } : {}}>
                            <div className="col-5 col-md-5 p-1">
                                <div className="col-12 col-md-6 p-0"><strong>{data[x].voucherno}/{data[x].date}</strong></div>
                                <div className="col-12 col-md-6 p-0 ">To {data[x].paymentmode === 'cash' ? 'Cash' : data[x].bankdetail.split("...")[1]}</div>
                                <div className="col-12 col-md-6 p-0 ">Payment</div>
                                <div className="col-12 col-md-6 p-0 ">{data[x].transactionno}</div>
                                <div className="col-12 col-md-6 p-0 ">{data[x].remark}</div>
                            </div>
                            <div className="col-3 col-md-1 text-right">{parseFloat(data[x].amount).toFixed(2)}</div>
                            <div className="col-3 col-md-6 p-1 text-right"></div>
                        </div>
                        <div className="container small d-none d-md-block p-1" style={i % 2 === 0 ? { backgroundColor: "rgb(181, 253, 233)" } : {}}>
                            <div className="d-flex">
                                <div className="col-1 p-0">{data[x].date}</div>
                                <div className="col-1 p-0">{data[x].voucherno}</div>
                                <div className="col-4 p-0">
                                    <div className="col-12 col-md-6 p-0 "><strong>To {data[x].paymentmode === 'cash' ? 'Cash' : data[x].bankdetail.split("...")[1]}</strong></div>
                                    <div className="col-12 col-md-6 p-0 ">Payment</div>
                                    <div className="col-12 col-md-6 p-0 ">{data[x].transactionno}</div>
                                    <div className="col-12 col-md-6 p-0 ">{data[x].remark}</div>
                                </div>
                                <div className="col-2 text-right">{parseFloat(data[x].amount).toFixed(2)}</div>
                                <div className="col-3 text-right"></div>
                            </div>
                        </div>
                    </div >
                )
            }
            else if (data[x].type === 'Expense') {
                creditamount = creditamount + (data[x].tds === '1' ? (parseFloat(data[x].amount) - parseFloat(data[x].tdsamount)) : parseFloat(data[x].amount));
                returnrows.push(
                    <div>
                        <div className=" d-flex small mb-1 p-1 d-md-none d-lg-none" style={i % 2 === 0 ? { backgroundColor: "rgb(181, 253, 233)" } : {}}>
                            <div className="col-5 col-md-5 p-1">
                                <div className="col-12 col-md-6 p-0"><strong>{data[x].voucherno}/{data[x].date}</strong></div>
                                <div className="col-12 col-md-6 p-0 " style={{ lineHeight: "1" }}>By {data[x].category + " " + data[x].type}</div>
                                <div className="col-12 col-md-6 p-0 " style={{ lineHeight: "1" }}>{data[x].description}</div>
                                {data[x].tds === "1" ? <div className="col-12 p-0">{"TDS Deducted :" + data[x].tdsamount}</div> : ''}
                            </div>
                            <div className="col-3 col-md-6 p-0 pt-1">
                            </div>
                            <div className="col-3 col-md-1 text-right">
                                <div className="col-12 col-md-2 p-0">{data[x].tds === "1" ? parseFloat(parseFloat(data[x].amount) - parseFloat(data[x].tdsamount)).toFixed(2) : parseFloat(data[x].amount).toFixed(2)}</div>
                            </div>
                        </div>
                        <div className="container d-none d-md-block small mb-1 p-1" style={i % 2 === 0 ? { backgroundColor: "rgb(181, 253, 233)" } : {}}>
                            <div className="d-flex">
                                <div className="col-1 p-0">{data[x].date}</div>
                                <div className="col-1 p-0">{data[x].voucherno}</div>
                                <div className="col-4 p-0">
                                    <div className="col-12 p-0"><strong>By {data[x].category + " " + data[x].type}</strong></div>
                                    <div className="col-12 p-0">{data[x].description}</div>
                                    {data[x].tds === "1" ? <div className="col-12 p-0">{"TDS Amount :" + data[x].tdsamount}</div> : ''}
                                </div>
                                <div className="col-2"></div>
                                <div className="col-3 text-right">{data[x].tds === '1' ? parseFloat(parseFloat(data[x].amount) - parseFloat(data[x].tdsamount)).toFixed(2) : parseFloat(data[x].amount).toFixed(2)}</div>
                                <div className="col-1"></div>
                            </div>
                        </div>
                    </div>
                );
            }
            x++;
            i++;
        }
        returnrows.push(
            <div>
                <div className="d-flex small d-md-none mt-1">
                    <div className="col-5 p-1"></div>
                    <div className="col-3 p-0 border-top border-dark">{parseFloat(debitamount).toFixed(2)}</div>
                    <div className="col-3 p-0 border-top border-dark">{parseFloat(creditamount).toFixed(2)}</div>
                </div>
                <div className="row small d-md-none mt-1">
                    <div className="col-5"><strong>{debitamount > creditamount ? "Advance Paid" : "Payable"}</strong></div>
                    <div className="col-3 border-top border-dark p-0 text-right">{creditamount >= debitamount ? parseFloat(creditamount - debitamount).toFixed(2) : ''}</div>
                    <div className="col-3 p-0 border-top border-dark text-right">{debitamount > creditamount ? parseFloat(debitamount - creditamount).toFixed(2) : ''}</div>
                </div>
                <div className="container small mt-2 d-none d-md-block p-0">
                    <div className="d-flex">
                        <div className="col-2 p-0"></div>
                        <div className="col-4 p-0"></div>
                        <div className="col-2 border-top border-dark text-right">{parseFloat(debitamount).toFixed(2)}</div>
                        <div className="col-3 border-top border-dark text-right">{parseFloat(creditamount).toFixed(2)}</div>
                    </div>
                </div>
                <div className="container small mt-2 d-none d-md-block p-0 ">
                    <div className="d-flex">
                        <div className="col-2 p-0"></div>
                        <div className="col-4 p-0"><strong>{debitamount > creditamount ? "Advance Paid" : "Payable"}</strong></div>
                        <div className="col-2 border-top border-dark text-right">{creditamount >= debitamount ? parseFloat(creditamount - debitamount).toFixed(2) : ''}</div>
                        <div className="col-3 text-right border-top border-dark text-right">{debitamount > creditamount ? parseFloat(debitamount - creditamount).toFixed(2) : ''}</div>
                    </div>
                </div>
            </div>
        )
        return returnrows;
    }
    else return;

}

let purchasetotalcredit = 0, purchasetotaldebit = 0, purchasebalance = 0;
/*const PurchaseDataPage = function ({ data, y, state }) {
    let returnrows = [];
    if (y === 1 && state.amounttype === 'paidadvance') {
        purchasetotaldebit = purchasetotaldebit + parseFloat(state.openingbalance);
        purchasebalance = purchasebalance - parseFloat(state.openingbalance);
    }
    else if (y === 1 && state.amounttype === 'payable') {
        purchasetotalcredit = parseFloat(state.openingbalance);
        purchasebalance = purchasebalance + parseFloat(state.openingbalance);
    }
    if (y === 1 && state.amounttype === 'paidadvance') returnrows.push(<View style={pdfstyles.itemSectionLedger, { textAlign: "center" }}>
        <View style={pdfstyles.itemHeader}><Text style={{ width: '75px', textAlign: "left" }}></Text><Text style={{ width: '75px', textAlign: "left" }}></Text><Text style={{ width: '200px', textAlign: "left", fontFamily: "Helvetica-Bold" }}>Opening Balance</Text><Text style={{ width: '75px', textAlign: "right" }}></Text><Text style={{ width: '75px', textAlign: "right" }}>{parseFloat(state.openingbalance).toFixed(2)}</Text><Text style={{ width: '75px', textAlign: "right" }}>{parseFloat(-1 * parseFloat(state.openingbalance)).toFixed(2)}</Text></View>
    </View>)
    if (y === 1 && state.amounttype === 'receivedadvance') returnrows.push(<View style={pdfstyles.itemSectionLedger, { textAlign: "center" }}>
        <View style={pdfstyles.itemHeader}><Text style={{ width: '75px', textAlign: "left" }}></Text><Text style={{ width: '75px', textAlign: "left" }}></Text><Text style={{ width: '200px', textAlign: "left", fontFamily: "Helvetica-Bold" }}>Opening Balance (Received Advance)</Text><Text style={{ width: '75px', textAlign: "right" }}>{parseFloat(state.openingbalance).toFixed(2)}</Text><Text style={{ width: '75px', textAlign: "left" }}></Text><Text style={{ width: '75px', textAlign: "left" }}>{parseFloat(state.openingbalance).toFixed(2)}</Text></View>
    </View>)
    if (y > 1) {
        returnrows.push(
            <View style={pdfstyles.itemSectionLedger, { textAlign: "center" }}>
                <View style={pdfstyles.itemHeader}><Text style={{ width: '75px', textAlign: "left" }}></Text><Text style={{ width: '75px', textAlign: "left" }}></Text><Text style={{ width: '200px', textAlign: "left", fontFamily: "Helvetica-Bold" }}>...Carry Forward</Text><Text style={{ width: '75px', textAlign: "right" }}>{parseFloat(purchasetotaldebit).toFixed(2)}</Text><Text style={{ width: '75px', textAlign: "right" }}>{parseFloat(purchasetotalcredit).toFixed(2)}</Text><Text style={{ width: '75px', textAlign: "right" }}>{parseFloat(purchasebalance).toFixed(2)}</Text></View>
            </View>
        )
    }
    for (let x = (y - 1) * 50; x < y * 50 && x < data.length; x++) {
        let d = data[x];
        if ((d.type === 'purchase')) {
            purchasetotalcredit = purchasetotalcredit + parseFloat(d.grandtotal);
            purchasebalance = purchasebalance + parseFloat(d.grandtotal);
            returnrows.push(
                <View style={pdfstyles.itemSectionLedger, { textAlign: "center" }}>
                    <View style={pdfstyles.itemHeader}><Text style={{ width: '75px', textAlign: "left" }}>{d.invoicedate}</Text><Text style={{ width: '75px', textAlign: "left" }}>{d.invoicenumber}</Text><Text style={{ width: '200px', textAlign: "left", fontFamily: "Helvetica-Bold" }}>By Purchase</Text><Text style={{ width: '75px', textAlign: "right" }}></Text><Text style={{ width: '75px', textAlign: "right" }}>{parseFloat(d.grandtotal).toFixed(2)}</Text><Text style={{ width: '75px', textAlign: "right" }}>{parseFloat(purchasebalance).toFixed(2)}</Text></View>
                </View>
            )
        }
        else if (d.type === 'asset') {
            purchasetotalcredit = purchasetotalcredit + parseFloat(d.purchasetotal);
            purchasebalance = purchasebalance + parseFloat(d.purchasetotal);
            returnrows.push(
                <View style={pdfstyles.itemSectionLedger, { textAlign: "center" }}>
                    <View style={pdfstyles.itemHeader}><Text style={{ width: '75px', textAlign: "left" }}>{d.purchasedate}</Text><Text style={{ width: '75px', textAlign: "left" }}>{d.purchasevoucherno}</Text><View><Text style={{ width: '200px', textAlign: "left", fontFamily: "Helvetica-Bold" }}>By {data[x].assetaccount}</Text><Text style={{ width: '200px', textAlign: "left", paddingLeft: "15px" }}>Asset Purchase</Text></View><Text style={{ width: '75px', textAlign: "right" }}></Text><Text style={{ width: '75px', textAlign: "right" }}>{parseFloat(d.purchasetotal).toFixed(2)}</Text><Text style={{ width: '75px', textAlign: "right" }}>{parseFloat(purchasebalance).toFixed(2)}</Text></View>
                </View>
            )
        }
        else if (d.type === 'payment') {
            purchasetotaldebit = purchasetotaldebit + parseFloat(d.amount);
            purchasebalance = purchasebalance - parseFloat(d.amount);
            returnrows.push(
                <View style={pdfstyles.itemSectionLedger, { textAlign: "center" }}>
                    <View style={pdfstyles.itemHeader}><Text style={{ width: '75px', textAlign: "left" }}>{d.date}</Text><Text style={{ width: '75px', textAlign: "left" }}>{d.voucherno}</Text><View style={{ width: "200px" }}><Text style={{ width: '200px', textAlign: "left" }}>To {data[x].paymentmode === 'cash' ? 'Cash' : data[x].bankdetail.split("...")[1]}</Text><Text style={{ width: '200px', textAlign: "left" }}>&nbsp;&nbsp;&nbsp;Payment</Text></View><Text style={{ width: '75px', textAlign: "left" }}>{parseFloat(d.amount).toFixed(2)}</Text><Text style={{ width: '75px', textAlign: "left" }}></Text><Text style={{ width: '75px', textAlign: "left" }}>{parseFloat(purchasebalance).toFixed(2)}</Text></View>
                </View>
            )
        }
        else if (d.type === 'debitcredit') {
            purchasetotaldebit = purchasetotaldebit + parseFloat(d.amount);
            purchasebalance = purchasebalance - parseFloat(d.amount);
            returnrows.push(
                <View style={pdfstyles.itemSectionLedger, { textAlign: "center" }}>
                    <View style={pdfstyles.itemHeader}><Text style={{ width: '75px', textAlign: "left" }}>{d.date}</Text><Text style={{ width: '75px', textAlign: "left" }}>{d.voucherno}</Text><Text style={{ width: '200px', textAlign: "left", fontFamily: "Helvetica-Bold" }}>By Credit Note</Text><Text style={{ width: '75px', textAlign: "right" }}>{parseFloat(d.amount).toFixed(2)}</Text><Text style={{ width: '75px', textAlign: "right" }}></Text><Text style={{ width: '75px', textAlign: "right" }}>{parseFloat(purchasebalance).toFixed(2)}</Text></View>
                </View>
            )
        }
        else if (d.type === 'Expense') {
            let expense = 0;
            d.tds === "1" ? expense = parseFloat(d.amount) - parseFloat(d.tdsamount) : expense = parseFloat(d.amount);
            purchasetotalcredit = purchasetotalcredit + parseFloat(expense);
            purchasebalance = purchasebalance + parseFloat(expense);
            returnrows.push(
                <View style={pdfstyles.itemSectionLedger, { textAlign: "center" }}>
                    <View style={pdfstyles.itemHeader}><Text style={{ width: '75px', textAlign: "left" }}>{d.date}</Text><Text style={{ width: '75px', textAlign: "left" }}>{d.voucherno}</Text><Text style={{ width: '200px', textAlign: "left", fontFamily: "Helvetica-Bold" }}>By {data[x].category + "-" + data[x].type}</Text><Text style={{ width: '75px', textAlign: "right" }}></Text><Text style={{ width: '75px', textAlign: "right" }}>{parseFloat(expense).toFixed(2)}</Text><Text style={{ width: '75px', textAlign: "right" }}>{parseFloat(purchasebalance).toFixed(2)}</Text></View>
                </View>
            )
        }
    }
    if (y === Math.ceil(data.length / 50)) {
        returnrows.push(<View><View style={pdfstyles.itemSectionLedger, { marginTop: "5px", paddingBottom: "2px" }}>
            <View style={pdfstyles.itemHeader}><Text style={{ width: '75px', textAlign: "left" }}></Text><Text style={{ width: '75px', textAlign: "left" }}></Text><Text style={{ width: '200px', textAlign: "left" }}></Text><Text style={{ width: '75px', borderTop: "1px solid black", textAlign: "right" }}>{parseFloat(purchasetotaldebit).toFixed(2)}</Text><Text style={{ width: '75px', borderTop: "1px solid black", textAlign: "right" }}>{parseFloat(purchasetotalcredit).toFixed(2)}</Text><Text style={{ width: '75px' }}></Text></View>
        </View>
            <View style={pdfstyles.itemSectionLedger, { marginTop: "5px", paddingBottom: "2px" }}>
                <View style={pdfstyles.itemHeader}><Text style={{ width: '75px', textAlign: "left" }}></Text><Text style={{ width: '75px', textAlign: "left" }}></Text><Text style={{ width: '200px', textAlign: "left" }}>{purchasebalance >= 0 ? "Payable" : "Advance Paid"}</Text><Text style={{ width: '75px', textAlign: "right" }}>{purchasebalance >= 0 ? parseFloat(purchasebalance).toFixed(2) : ""}</Text><Text style={{ width: '75px', textAlign: "right" }}>{purchasebalance < 0 ? parseFloat(purchasebalance).toFixed(2) : ""}</Text></View>
            </View>
            <View style={pdfstyles.itemSectionLedger, { marginTop: "5px", paddingBottom: "2px", borderTop: "1px solid black", borderBottom: "1px solid black", fontFamily: "Helvetica-Bold" }}>
                <View style={pdfstyles.itemHeader}><Text style={{ width: '75px', textAlign: "left" }}></Text><Text style={{ width: '75px', textAlign: "left" }}></Text><Text style={{ width: '200px', textAlign: "left", fontFamily: "Helvetica-Bold" }}>Total</Text><Text style={{ width: '75px', textAlign: "right" }}>{purchasetotaldebit > purchasetotalcredit ? parseFloat(purchasetotaldebit).toFixed(2) : parseFloat(purchasetotalcredit).toFixed(2)}</Text><Text style={{ width: '75px', textAlign: "right" }}>{purchasetotaldebit > purchasetotalcredit ? parseFloat(purchasetotaldebit).toFixed(2) : parseFloat(purchasetotalcredit).toFixed(2)}</Text></View>
            </View></View>)
    }
    else {
        returnrows.push(<View><View style={pdfstyles.itemSectionLedger, { fontFamily: "Helvetica-Bold", marginTop: "2px" }}>
            <View style={pdfstyles.itemHeader}><Text style={{ width: '75px', textAlign: "left" }}></Text><Text style={{ width: '75px', textAlign: "left" }}></Text><Text style={{ width: '200px', textAlign: "left" }}></Text><Text style={{ width: '75px', borderTop: "1px solid black", textAlign: "right" }}>{parseFloat(purchasetotaldebit).toFloat(2)}</Text><Text style={{ width: '75px', borderTop: "1px solid black", textAlign: "right" }}>{parseFloat(purchasetotalcredit).toFixed(2)}</Text><Text style={{ width: '75px', borderTop: "1px solid black", textAlign: "right" }}>{parseFloat(purchasebalance).toFixed(2)}</Text></View>
        </View>
            <View style={pdfstyles.itemSectionLedger, { fontFamily: "Helvetica", marginTop: "5px", textAlign: "right" }}><Text></Text>...Continued</View></View>)
    }
    return returnrows;
}
*/
/*const Report = function ({ business, data, l, n, period, state, type }) {
    let returnrows = [];
    for (let y = 1; y <= n; y++) {
        returnrows.push(<Page size="A4" style={pdfstyles.page}>
            {y === 1 ? <View><View style={pdfstyles.taxInvoiceHeader, { marginTop: "20px", textAlign: "center" }}>
                <View style={{ fontFamily: "Helvetica-Bold", width: "500px" }}><Text>{business.businessname}</Text></View>
                <View style={{ width: "500px" }}><Text>{business.address}</Text></View>
            </View>
                <View style={pdfstyles.taxInvoiceHeader, { marginTop: "10px", textAlign: "center" }}>
                    <View style={{ fontFamily: "Helvetica-Bold", textDecoration: "underline", width: "500px" }}><Text>{state.name.split("....")[1] + " Ledger"}</Text></View>
                    <View style={{ width: "500px" }}><Text>{period}</Text></View>
                </View></View> : ''}
            <View style={pdfstyles.itemSectionLedger, { marginTop: "20px", marginBottom: "5px", fontFamily: "Helvetica-Bold", textAlign: "center", borderTop: "1px solid black", borderBottom: "1px solid black" }}>
                <View style={pdfstyles.itemHeader}><Text style={{ width: '75px', textAlign: "left" }}>Date</Text><Text style={{ width: '75px', textAlign: "left" }}>Voucher No</Text><Text style={{ width: '200px', textAlign: "left" }}>Particulars</Text><Text style={{ width: '75px', textAlign: "right" }}>Debit</Text><Text style={{ width: '75px', textAlign: "right" }}>Credit</Text><Text style={{ width: '75px', textAlign: "right" }}>Balance</Text></View>
            </View>
            {type === 'vendor' ? <PurchaseDataPage data={data} y={y} state={state} /> : <SalesDataPage data={data} y={y} state={state} />}
        </Page >)
    }
    return returnrows;
}

const VendorLedger = function ({ data, business, fy, state }) {
    let period = fy;
    let l = data.length, i = 0, n = Math.ceil(l / 50), y = 0;
    return (
        <Document>
            <Report business={business} data={data} l={l} n={n} period={period} state={state} type="vendor" />
        </Document>
    )
}
*/
/*const genereatePurchasePdf = async (business, fy, state, purchaseDetail, getPayment, getDebitCredit, businessid, fyid, deleteEntry, getExpense, getAssetAccount) => {
    try {
        purchasetotalcredit = 0;
        purchasetotaldebit = 0;
        purchasebalance = 0;
        let id = state.name ? state.name.split("....")[0] : '';
        let purchaseData = purchaseDetail.data.filter(p => p.vendorid === id);
        let paymentData = getPayment.data.filter(p => p.vendorid === id);
        let debitcredit = getDebitCredit.data.filter(d => d.partyid === id);
        let expenseData = getExpense.data.filter(d => d.vendorid === id);
        let assetData = getAssetAccount.data.filter(d => d.purchasefyid === fyid);
        let data = [];
        data = data.concat(purchaseData);
        data = data.concat(paymentData);
        data = data.concat(debitcredit);
        data = data.concat(expenseData);
        data = data.concat(assetData);
        data = data.sort(sortDataPurchase);
        const doc = <VendorLedger data={data} business={business} fy={fy} state={state} />;
        const blobPdf = pdf(doc);
        blobPdf.updateContainer(doc);
        const result = await blobPdf.toBlob();
        let filename = state.name.split("....")[1].toUpperCase() + " Ledger.pdf";
        saveAs(result, filename);
    }
    catch (err) {
        alert(err);
    }
}*/

const DataDisplay = function ({ state, getPayment, fyid, businessList, businessid, getExpense, business, getEmployeeOpeningBalance }) {
    let returnrows = [];
    if (state.employee !== '') {
        returnrows.push(
            <div>
                <h6 className="text-center mt-2 mb-2"><u>{state.employee.split("...")[1]}</u>&nbsp;&nbsp;&nbsp;</h6>
                <div className="d-flex small mb-1 p-1 d-md-none d-lg-none">
                    <div className="col-5 col-md-5 p-1"></div>
                    <div className="col-3 col-md-1 text-right"><strong>Debit</strong></div>
                    <div className="col-3 col-md-6 p-1 text-right"><strong>Credit</strong></div>
                    <div className="col-1 p-0"></div>
                </div>
                <div className="container small mt-3 d-none d-md-block p-1">
                    <div className="d-flex">
                        <div className="col-1 p-0"><strong>Date</strong></div>
                        <div className="col-1 p-0"><strong>Voucher No</strong></div>
                        <div className="col-4 p-0"><strong>Particulars</strong></div>
                        <div className="col-2 text-right"><strong>Debit</strong></div>
                        <div className="col-3 text-right"><strong>Credit</strong></div>
                    </div>
                </div>
            </div>)
        returnrows.push(<DataRow state={state} getPayment={getPayment} fyid={fyid} businessList={businessList} businessid={businessid} getExpense={getExpense} business={business} getEmployeeOpeningBalance={getEmployeeOpeningBalance} />)
    }
    return returnrows;
}

const SelectEmployee = function ({ employee, state, handleInputChange }) {
    let returnrows = [];
    returnrows.push(<div className="form-group mb-0 col-12 p-1">
        <label htmlFor="name" className="mb-0 muted-text"><strong>Select Employee:<span className="text-danger">*</span></strong></label>
        <Control.select model=".employee" className="form-control form-control-sm" id="employee" name="employee" onChange={handleInputChange} value={state.employee}>
            <option value="">Select Employee</option>
            {employee.map(e => <option value={e.employeeid + "..." + e.employeename}>{e.employeename}</option>)}
        </Control.select>
    </div>)
    return returnrows;
}

class EmployeeAccountStatement extends Component {
    constructor(props) {
        super(props);
        this.state = ({
            employee: ''
        })
        window.onload = () => {
            this.props.resetPaymentDetail();
            this.props.resetExpenseDetail();
            this.props.refreshEmployeeState();
            this.props.resetEmployeeOpeningBalance();
        }
    }
    componentDidMount() {
        if (this.props.getPayment.message === 'initial') this.props.getPaymentDetail(this.props.businessid, this.props.fyid);
        if (this.props.getExpense.message === 'initial') this.props.getExpenseDetail(this.props.businessid, this.props.fyid);
        if (this.props.getEmployee.message === 'initial') this.props.getEmployeeList(this.props.businessid);
        if (this.props.getEmployeeOpeningBalance.message === 'initial') this.props.getEmployeeOpeningBalanceList(this.props.businessid, this.props.fyid);
        if (this.props.logoutMessage.error !== '') {
            alert("Error Logging Out. Please Try Again");
            this.props.resetLogoutMessage();
        }
    }
    handleInputChange = (event) => {
        let target = event.target;
        let name = target.name;
        let value = target.value;
        this.setState({
            ...this.state,
            employee: value,
        })
    }
    render() {
        if (!cookie.load('userservice')) return <Redirect to="/" />;
        else {
            if ((this.props.getPayment.loading === true) || (this.props.logoutMessage.loading === true) || (this.props.getExpense.loading === true) || (this.props.getEmployee.loading === true) || (this.props.getEmployeeOpeningBalance.loading === true)) {
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div className="mt-3 d-flex justify-content-center">
                            <button className="btn btn-danger">Loading... </button>
                        </div>
                    </div>
                );
            }
            else if ((this.props.getPayment.error !== '') || (this.props.getExpense.error !== '') || (this.props.getEmployee.error !== '') || (this.props.getEmployeeOpeningBalance.error !== '')) {
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div className="mt-3 d-flex justify-content-center">
                            <button className="btn btn-danger">Error.. Please Refresh and Try Again </button>
                        </div>
                    </div>
                );
            }
            else {
                let business = this.props.businessList.data.filter(b => b.businessid === this.props.businessid)[0];
                let fy = business.financial.filter(f => f.fyid === this.props.fyid)[0].financialyear;
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div style={{ minHeight: "93vh", display: "flex", flexDirection: "column" }}>
                            <div className="container" style={{ flex: "1" }}>
                                <h6 className="text-center pl-2 pr-2 pt-4" style={{ lineHeight: "1" }}><strong>{business.businessname} {fy}</strong></h6>
                                <h6 className="text-center pl-2 pr-2 mb-4" style={{ lineHeight: "1" }}>{business.alias ? "(" + business.alias + ")" : ''}</h6>
                                <h6 className="text-center pl-2 pr-2"><strong><u>Employee Statement</u></strong></h6>
                                <div className="container col-12 col-md-4" >
                                    <LocalForm className="small">
                                        <div className="row justify-content-center mt-1">
                                            < SelectEmployee employee={this.props.getEmployee.data} state={this.state} handleInputChange={this.handleInputChange} />
                                        </div>
                                    </LocalForm>
                                </div>
                                <div className="col-12 p-0">
                                    <DataDisplay state={this.state} getPayment={this.props.getPayment} fyid={this.props.fyid} businessList={this.props.businessList} businessid={this.props.businessid} getExpense={this.props.getExpense} business={business} getEmployeeOpeningBalance={this.props.getEmployeeOpeningBalance} />
                                </div>
                            </div>
                            <Footer />
                        </div>
                    </div>
                )
            }
        }
    }
}
export default EmployeeAccountStatement;