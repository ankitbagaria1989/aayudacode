import React, { Component } from 'react';
import cookie from 'react-cookie';
import { Redirect } from 'react-router-dom';
import { Button } from 'reactstrap';
import { Header } from './HeaderComponent';
import { Footer } from './Footer';
import { LocalForm, Control } from 'react-redux-form';
import { sortDataStock } from '../redux/sortArrayofObjects';

const Item = function ({ itemid, getItem }) {
    let itemname = '';
    getItem.data.forEach(item => {
        if (item.itemid === itemid) itemname = item.alias ? item.alias : item.itemname;
    });
    return itemname;
}

const DataDisplay = function ({ data, getItem }) {
    let returnrows = [];
    data = JSON.parse(data);
    let keys = Object.keys(data);
    for (let i = 1; i <= keys.length; i++) {
        returnrows.push(<div className="col-12 p-0"><Item itemid={data[i].itemid.split("...")[0]} getItem={getItem} /> - {data[i].qty} * ({data[i].itemid.split("...")[1]})</div>)
    }
    return returnrows;
}

const StockJournalData = function ({ getStockJournal, getItem }) {
    let t1 = <div className="mb-2">
        <div className="d-flex">
            <div className="col-2 p-0"><strong>Date</strong></div>
            <div className="col-5 p-0"><strong>Consumption Detail</strong></div>
            <div className="col-5 p-0"><strong>Production Detail</strong></div>
        </div>
    </div>
    let returnrows = [];
    returnrows.push(t1);
    let data = getStockJournal.data;
    data = data.sort(sortDataStock);
    for (let i = 0; i < getStockJournal.data.length; i++) {
        returnrows.push(<div className="" style={i % 2 === 0 ? { backgroundColor: "rgb(181, 253, 233)" } : {}}>
            <div className="d-flex">
                <div className="col-2 p-0">{getStockJournal.data[i].date}</div>
                <div className="col-5 p-0"><DataDisplay data={getStockJournal.data[i].consumptiondetail} getItem={getItem} /></div>
                <div className="col-5 p-0"><DataDisplay data={getStockJournal.data[i].productiondetail} getItem={getItem} /></div>
            </div>
        </div>)
    }
    return returnrows;
}

const RenderConsumptionItemComponent = function ({ state, getItem, handleInputChange }) {
    let itemRows = [];
    for (let x = 1; x <= state.cn; x++) {
        let billingunit = state.consumptiondetail[x]['itemid'] ? state.consumptiondetail[x]['itemid'].split("...")[1] : '';
        itemRows.push(<div className="mt-1" style={{ backgroundColor: "rgb(255 149 149)", paddingTop: "1px", paddingBottom: "1px" }}><div className="form-group mb-1 col-12">
            <label htmlFor={"consumptionitemid-" + x} className="mb-0 muted-text"><strong>({x}) Select Item:<span className="text-danger">*</span></strong></label>
            <Control.select model={".consumptionitemid-" + x} className="form-control form-control-sm" id={"consumptionitemid-" + x} name={"consumptionitemid-" + x} onChange={handleInputChange} value={state.consumptiondetail[x]['itemid']}>
                <option value="">Select Item</option>
                {getItem.data.map(b => <option value={b.itemid + "..." + b.billingunit}>{b.alias ? b.alias : b.itemname}</option>)}
            </Control.select>
        </div>
            <div className="form-group mb-1 col-12">
                <label htmlFor={'consumptionqty-' + x} className="mb-0"><strong>{'Consumed Qty In ' + billingunit}</strong><span className="text-danger">*</span></label>
                <Control.text model={'.consumptionqty-' + x} className="form-control form-control-sm col-5" id={'consumptionqty-' + x} name={'consumptionqty-' + x} onChange={handleInputChange} value={state.consumptiondetail[x]['qty']} />
            </div></div>)
    }
    return itemRows;
}
const RenderProductionItemComponent = function ({ state, getItem, handleInputChange }) {
    let itemRows = [];
    for (let x = 1; x <= state.pn; x++) {
        let billingunit = state.productiondetail[x]['itemid'] ? state.productiondetail[x]['itemid'].split("...")[1] : '';
        itemRows.push(<div className="mt-1" style={{ backgroundColor: "rgb(124 207 255)", paddingTop: "1px", paddingBottom: "1px" }}><div className="form-group mb-1 col-12">
            <label htmlFor={"productionitemid-" + x} className="mb-0 muted-text"><strong>({x}) Select Item:<span className="text-danger">*</span></strong></label>
            <Control.select model={".productionitemid-" + x} className="form-control form-control-sm" id={"productionitemid-" + x} name={"productionitemid-" + x} onChange={handleInputChange} value={state.productiondetail[x]['itemid']}>
                <option value="">Select Item</option>
                {getItem.data.map(b => <option value={b.itemid + "..." + b.billingunit}>{b.alias ? b.alias : b.itemname}</option>)}
            </Control.select>
        </div>
            <div className="form-group mb-1 col-12">
                <label htmlFor={'productionqty-' + x} className="mb-0"><strong>{'Produced Qty In ' + billingunit}</strong><span className="text-danger">*</span></label>
                <Control.text model={'.productionqty-' + x} className="form-control form-control-sm col-5" id={'productionqty-' + x} name={'productionqty-' + x} onChange={handleInputChange} value={state.productiondetail[x]['qty']} />
            </div></div>)
    }
    return itemRows;
}
class StockJournal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            cn: 1,
            pn: 1,
            consumptiondetail: {
                1: {
                    itemid: '',
                    qty: ''
                }
            },
            productiondetail: {
                1: {
                    itemid: '',
                    qty: ''
                }
            },
            date: new Date().toDateString().split(' ').slice(1).join(' ')
        }
    }

    handleSubmit = () => {
        let flag1 = 0, flag2 = 0, flag3 = 0;
        let business = this.props.businessList.data.filter(b => b.businessid === this.props.businessid)[0];
        let fy = business.financial.filter(f => f.fyid === this.props.fyid)[0].financialyear;
        if ((new Date(this.state.date) < new Date("01 Apr " + fy.split("-")[0])) || (new Date(this.state.date) > new Date("31 Mar " + fy.split("-")[1])) || (this.state.date === '')) {
            alert("Invalid Date. Date Must Be Within FY " + fy);
            flag3 = 1;
        }
        for (let i = 1; i <= this.state.cn; i++) {
            if (flag3 === 0 && flag1 === 0) {
                if ((this.state.consumptiondetail[i].itemid === '') || (this.state.consumptiondetail[i].qty === '')) {
                    alert("Please Fill In All Field For Column (" + i + ") Under Consumption Detail");
                    flag1 = 1;
                }
                else if (isNaN(this.state.consumptiondetail[i].qty)) {
                    alert("Quantity For Column (" + i + ") Under Consumption Detail Must Be A Number");
                    flag1 = 1;
                }
            }
        }
        for (let i = 1; i <= this.state.pn; i++) {
            if (flag3 === 0 && flag1 === 0 && flag2 === 0) {
                if ((this.state.productiondetail[i].itemid === '') || (this.state.productiondetail[i].qty === '')) {
                    alert("Please Fill In All Field For Column (" + i + ") Under Production Detail");
                    flag2 = 1;
                }
                else if (isNaN(this.state.productiondetail[i].qty)) {
                    alert("Quantity For Column (" + i + ") Under Production Detail Must Be A Number");
                    flag2 = 1;
                }
            }
        }
        if (flag1 === 0 && flag2 === 0 && flag3 === 0) this.props.passStockJournal(this.props.businessid, this.props.fyid, this.state)

    }

    addConsumedItem = () => {
        this.setState({
            ...this.state,
            cn: this.state.cn + 1,
            consumptiondetail: {
                ...this.state.consumptiondetail,
                [this.state.cn + 1]: {
                    itemid: '',
                    qty: ''
                }
            }
        })
    }
    addProductionItem = () => {
        this.setState({
            ...this.state,
            pn: this.state.pn + 1,
            productiondetail: {
                ...this.state.productiondetail,
                [this.state.pn + 1]: {
                    itemid: '',
                    qty: ''
                }
            }
        })
    }
    handleInputChange = (event) => {
        let target = event.target;
        let name = target.name;
        let value = target.value;
        if (/^consumptionitemid-/i.test(name)) {
            let id = name.split("-")[1];
            this.setState({
                ...this.state,
                consumptiondetail: {
                    ...this.state.consumptiondetail,
                    [id]: {
                        itemid: value,
                        qty: this.state.consumptiondetail[id]['qty']
                    }
                }
            })
        }
        else if (/^consumptionqty-/i.test(name)) {
            let id = name.split("-")[1];
            this.setState({
                ...this.state,
                consumptiondetail: {
                    ...this.state.consumptiondetail,
                    [id]: {
                        itemid: this.state.consumptiondetail[id]['itemid'],
                        qty: value
                    }
                }
            })
        }
        else if (/^productionitemid-/i.test(name)) {
            let id = name.split("-")[1];
            this.setState({
                ...this.state,
                productiondetail: {
                    ...this.state.productiondetail,
                    [id]: {
                        itemid: value,
                        qty: this.state.productiondetail[id]['qty']
                    }
                }
            })
        }
        else if (/^productionqty-/i.test(name)) {
            let id = name.split("-")[1];
            this.setState({
                ...this.state,
                productiondetail: {
                    ...this.state.productiondetail,
                    [id]: {
                        itemid: this.state.productiondetail[id]['itemid'],
                        qty: value
                    }
                }
            })
        }
        else {
            this.setState({
                ...this.state,
                [name]: value
            })
        }
    }
    componentDidMount() {
        window.onload = () => {
            this.props.refreshItemState();
            this.props.refreshGetStockJournal();
        }
        if (this.props.logoutMessage.error !== '') {
            alert("Error Logging Out. Please Try Again");
            this.props.resetLogoutMessage();
        }
        if (this.props.getItem.message === 'initial') this.props.getItemList(this.props.businessid, this.props.fyid);
        if (this.props.getStockJournal.message === 'initial') this.props.getStockJournalList(this.props.businessid, this.props.fyid);
        if (this.props.passStockJournalState.message === 'success') {
            alert("Stock Journal Entry Added Successfully");
            this.props.refreshStockJournalState();
        }
        if (this.props.passStockJournalState.error !== '') {
            alert(this.props.passStockJournalState.error);
            this.setState({
                ...this.props.passStockJournalState.state
            })
            this.props.refreshStockJournalState();
        }
    }
    render() {
        if (!cookie.load('userservice')) return <Redirect to="/" />
        else {
            if ((this.props.logoutMessage.loading === true) || (this.props.getItem.loading === true) || (this.props.passStockJournalState.loading === true) || (this.props.getStockJournal.loading === true)) {
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div className="mt-3 d-flex justify-content-center">
                            <button className="btn btn-danger">Loading... </button>
                        </div>
                    </div>
                );
            }
            else if ((this.props.getItem.error !== '') || (this.props.getStockJournal.error !== '')) {
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div className="mt-3 d-flex justify-content-center">
                            <button className="btn btn-danger">Error Fetching Data. Please Try Again </button>
                        </div>
                    </div>
                );
            }
            else {
                let business = this.props.businessList.data.filter(b => b.businessid === this.props.businessid)[0];
                let fy = business.financial.filter(f => f.fyid === this.props.fyid)[0].financialyear;
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div style={{ minHeight: "93vh", display: "flex", flexDirection: "column" }}>
                            <div className="container d-flex justify-content-center pr-0 pl-0" style={{ flex: "1" }}>
                                <div className="col-12">
                                    <div className="small pr-0 pl-0">
                                        <h6 className="text-center pl-2 pr-2 pt-4" style={{ lineHeight: "1" }}><strong>{business.businessname} {fy}</strong></h6>
                                        <h6 className="text-center pl-2 pr-2 mb-4" style={{ lineHeight: "1" }}>{business.alias ? "(" + business.alias + ")" : ''}</h6>
                                        <h6 className="text-center pl-2 pr-2"><strong><u>Stock Journal Entry</u></strong></h6>
                                        <div className="row mt-2 justify-content-center" >
                                            <LocalForm className="col-12 " onSubmit={this.handleSubmit}>
                                                <div className="container form-group mb-1 col-12 col-md-4">
                                                    <label htmlFor="date" className="mb-0 muted-text"><strong>Date</strong><span className="text-danger">*</span></label>
                                                    <Control.text model=".date" className="form-control form-control-sm" id="date" name="date" value={this.state.date} onChange={this.handleInputChange} />
                                                </div>
                                                <div className=" d-none d-md-block container">
                                                    <div className=" d-flex">
                                                        <div className="col-12 col-md-4 mt-3 p-0">
                                                            <h6 className="text-center"><u>Consumption Detail</u></h6>
                                                            <RenderConsumptionItemComponent state={this.state} getItem={this.props.getItem} handleInputChange={this.handleInputChange} />
                                                            <div className="col-12">
                                                                <Button className="btn mt-1 btn-sm" style={{ backgroundColor: "white", border: "1px solid rgb(255 149 149)", color: "rgb(255 149 149)" }} onClick={this.addConsumedItem}><strong>Add More Item</strong></Button>
                                                            </div>
                                                        </div>
                                                        <div className="col-md-4"></div>
                                                        <div className="col-12 col-md-4 mt-3 p-0">
                                                            <h6 className="text-center"><u>Production Detail</u></h6>
                                                            <RenderProductionItemComponent state={this.state} getItem={this.props.getItem} handleInputChange={this.handleInputChange} />
                                                            <div className="col-12">
                                                                <Button className="btn mt-1 btn-sm" style={{ backgroundColor: "#fff", border: "1px solid rgb(124 207 255)", color: "rgb(124 207 255)" }} onClick={this.addProductionItem}><strong>Add More Item</strong></Button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="container d-md-none d-lg-none">
                                                    <div className="col-12 col-md-4 mt-3 p-0">
                                                        <h6 className="text-center"><u>Consumption Detail</u></h6>
                                                        <RenderConsumptionItemComponent state={this.state} getItem={this.props.getItem} handleInputChange={this.handleInputChange} />
                                                        <div className="col-12">
                                                            <Button className="btn mt-1 btn-sm" style={{ backgroundColor: "white", border: "1px solid rgb(255 149 149)", color: "rgb(255 149 149)" }} onClick={this.addConsumedItem}><strong>Add More Item</strong></Button>
                                                        </div>
                                                    </div>
                                                    <div className="col-md-4"></div>
                                                    <div className="col-12 col-md-4 mt-3 p-0">
                                                        <h6 className="text-center"><u>Production Detail</u></h6>
                                                        <RenderProductionItemComponent state={this.state} getItem={this.props.getItem} handleInputChange={this.handleInputChange} />
                                                        <div className="col-12">
                                                            <Button className="btn mt-1 btn-sm" style={{ backgroundColor: "#fff", border: "1px solid rgb(124 207 255)", color: "rgb(124 207 255)" }} onClick={this.addProductionItem}><strong>Add More Item</strong></Button>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="container col-12 mt-2 pb-5">
                                                    <Button onClick={this.handleSubmit} className=" btn btn-success mt-1 btn-sm small" >Pass Journal Entry</Button>
                                                </div>
                                            </LocalForm>
                                        </div>
                                        <StockJournalData getStockJournal={this.props.getStockJournal} getItem={this.props.getItem} />
                                    </div>
                                </div>
                            </div>
                            <Footer />
                        </div>
                    </div>
                );
            }
        }
    }
}

export default StockJournal;