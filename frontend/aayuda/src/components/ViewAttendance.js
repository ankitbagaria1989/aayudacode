import React, { Component } from 'react';
import cookie from 'react-cookie';
import { Redirect } from 'react-router-dom';
import { Header } from './HeaderComponent';
import { Footer } from './Footer';
import { style } from '../sylesheet/stylesheet';

const GetEmployeeList = function ({ getEmployee, year, month }) {
    let returnrows = [];
    let i = 1;
    getEmployee.data.forEach(e => {
        if ((((new Date(e.doj.split(" ")[0] + " 01 " + e.doj.split(" ")[2]) <= new Date(month + " 01 " + year)) && (e.active == 1)) || ((new Date(e.doj.split(" ")[0] + " 01 " + e.doj.split(" ")[2]) <= new Date(month + " 01 " + year)) && (e.active == 0) && (new Date(e.dot.split(" ")[0] + " 01 " + e.dot.split(" ")[2]) > new Date(month + " 01 " + year))))) {
            if (i % 2 !== 0) returnrows.push(<div className="p-2 row" style={style.height1, { backgroundColor: "rgb(181, 253, 233)", whiteSpace: "nowrap", overflow: "hidden" }}>{e.employeename}</div>)
            else returnrows.push(<div className="p-2 row" style={style.height1, { whiteSpace: "nowrap", overflow: "hidden" }}>{e.employeename}</div>)
            i++;
        }
    })
    return returnrows;
}

const GetDateList = function ({ getEmployee, year, month, i, attendanceobject, count }) {
    let returnrows = [];
    let y = 1;
    getEmployee.data.forEach(e => {
        if ((((new Date(e.doj.split(" ")[0] + " 01 " + e.doj.split(" ")[2]) <= new Date(month + " 01 " + year)) && (e.active == 1)) || ((new Date(e.doj.split(" ")[0] + " 01 " + e.doj.split(" ")[2]) <= new Date(month + " 01 " + year)) && (e.active == 0) && (new Date(e.dot.split(" ")[0] + " 01 " + e.dot.split(" ")[2]) > new Date(month + " 01 " + year))))) {
            let x = (count[e.employeeid] && attendanceobject[e.employeeid]) ? (attendanceobject[e.employeeid].present ? count[e.employeeid].p++ : count[e.employeeid].a++) : '';
            x = (count[e.employeeid] && attendanceobject[e.employeeid]) ? (attendanceobject[e.employeeid].ot > 0 ? count[e.employeeid].ot = parseInt(count[e.employeeid].ot) + parseInt(attendanceobject[e.employeeid].ot) : '') : '';
            if (y % 2 !== 0) returnrows.push(<div className="p-2" style={style.card, { backgroundColor: "rgb(181, 253, 233)" }}><span className="small">{i} - </span><span className="small">{attendanceobject[e.employeeid] ? attendanceobject[e.employeeid].present ? 'P' : 'A' : ''} {attendanceobject[e.employeeid] ? attendanceobject[e.employeeid].ot > 0 ? " + " + attendanceobject[e.employeeid].ot + " hrs" : '' : ''}</span></div>)
            else returnrows.push(<div className="p-2" style={style.card}><span className="small">{i} - </span><span className="small">{attendanceobject[e.employeeid] ? attendanceobject[e.employeeid].present ? 'P' : 'A' : ''} {attendanceobject[e.employeeid] ? attendanceobject[e.employeeid].ot > 0 ? " + " + attendanceobject[e.employeeid].ot + " hrs" : '' : ''}</span></div>)
            y++;
        }
    })
    return returnrows;
}

const Report = function ({ getEmployee, count, month, year }) {
    let returnrows = [];
    let y = 1;
    getEmployee.data.forEach(e => {
        if (new Date(e.doj.split(" ")[0] + " 01 " + e.doj.split(" ")[2]) <= new Date(month + " 01 " + year)) {
            if (y % 2 !== 0) returnrows.push(<div className="p-2" style={style.cardReport, { backgroundColor: "rgb(247, 182, 150)" }}><span className="small">{count[e.employeeid] ? "P-" + count[e.employeeid].p + " A-" + count[e.employeeid].a + " OT-" + count[e.employeeid].ot : ''}</span></div>)
            else returnrows.push(<div className="p-2" style={style.cardReport}><span className="small">{count[e.employeeid] ? "P-" + count[e.employeeid].p + "  A-" + count[e.employeeid].a + " OT-" + count[e.employeeid].ot : ''}</span></div>)
            y++;
        }
    })
    return returnrows;
}

const IterateOverDate = function ({ getEmployee, year, month, days, getMonthAttendance, count }) {
    let returnrows = [];
    let attendanceobject = {};
    let y = 0;
    for (let i = 1; i <= days; i++) {
        attendanceobject = {};
        if (getMonthAttendance.data[y] ? parseInt(getMonthAttendance.data[y].date.split(" ")[1]) == i : false) {
            attendanceobject = JSON.parse(getMonthAttendance.data[y].attendanceobject);
            y++;
        }
        returnrows.push(
            <div className="" style={style.cardMain}>
                <GetDateList getEmployee={getEmployee} year={year} month={month} i={i} attendanceobject={attendanceobject} count={count} />
            </div>
        )
    }
    returnrows.push(<div style={{ display: "inline-block", width: "150px" }}><Report getEmployee={getEmployee} count={count} month={month} year={year} /></div>)
    return returnrows;
}

const AttendanceChart = function ({ getEmployee, year, month, days, getMonthAttendance, count }) {
    return (
        <div className="row">
            <div className="col-4 col-md-2">
                <GetEmployeeList getEmployee={getEmployee} year={year} month={month} />
            </div>
            <div className="col-8 col-md-10 mb-5 pr-0 pl-1">
                <div style={style.box}>
                    <IterateOverDate getEmployee={getEmployee} year={year} month={month} days={days} getMonthAttendance={getMonthAttendance} count={count} />
                </div>
            </div>
        </div>
    )
}

class ViewAttendance extends Component {
    constructor(props) {
        super(props);
        window.onload = () => {
            this.props.refreshEmployeeState();
        }
        let business = this.props.businessList.data.filter(b => b.businessid === this.props.businessid)[0];
        let fy = business.financial.filter(f => f.fyid === this.props.fyid)[0].financialyear;
        let year = fy.split("-")[0];
        let days = 30;
        switch (this.props.month.toLowerCase()) {
            case 'apr': days = 30; year = fy.split("-")[0];
                break;
            case 'may': days = 31; year = fy.split("-")[0];
                break;
            case 'jun': days = 30; year = fy.split("-")[0];
                break;
            case 'jul': days = 31; year = fy.split("-")[0];
                break;
            case 'aug': days = 31; year = fy.split("-")[0];
                break;
            case 'sep': days = 30; year = fy.split("-")[0];
                break;
            case 'oct': days = 31; year = fy.split("-")[0];
                break;
            case 'nov': days = 30; year = fy.split("-")[0];
                break;
            case 'dec': days = 31; year = fy.split("-")[0];
                break;
            case 'jan': days = 31; year = fy.split("-")[1];
                break;
            case 'feb':
                year = fy.split("-")[1];
                let result = fy.split("-")[1] % 100 ? fy.split("-")[1] % 400 : fy.split("-")[1] % 4;
                if (result) days = 29;
                else days = 28;
                break;
            case 'mar': days = 31; year = fy.split("-")[1];
                break;
        }
        this.state = {
            year: year,
            days: days,
            fy: fy,
            business: business
        }
    }

    componentDidMount() {
        window.onload = () => {
            this.props.refreshEmployeeState();
        }
        if ((this.props.getEmployee.message === 'initial') && (this.props.getEmployee.loading === false)) this.props.getEmployeeList(this.props.businessid);
        if (this.props.logoutMessage.error !== '') {
            alert("Error Logging Out. Please Try Again");
            this.props.resetLogoutMessage();
        }
        if (this.props.getMonthAttendance.message === 'initial') this.props.getMonthAttendanceList(this.props.fyid, this.props.month, this.state.year);
        if (this.props.getMonthAttendance.message === 'success') {
            this.props.resetGetMonthAttendance();
        }
    }
    render() {
        if (!cookie.load('userservice')) return <Redirect to="/" />
        else {
            let count = {};
            this.props.getEmployee.data.forEach(e => {
                count = {
                    ...count,
                    [e.employeeid]: {
                        a: 0,
                        p: 0,
                        ot: 0
                    }
                }
            });
            if ((this.props.logoutMessage.loading === true) || (this.props.getEmployee.loading === true) || (this.props.getMonthAttendance.loading === true)) {
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div className="mt-3 d-flex justify-content-center">
                            <button className="btn btn-danger">Loading... </button>
                        </div>
                    </div>
                );
            }
            else if ((this.props.getEmployee.error !== "") || (this.props.getMonthAttendance.error !== '')) {
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div className="mt-3 d-flex justify-content-center">
                            <button className="btn btn-danger">Error. Please Refresh and Try Again</button>
                        </div>
                    </div>
                );
            }
            else {
                let present = false;
                this.props.getEmployee.data.forEach(e => {
                    if (new Date(e.doj.split(" ")[0] + " 01 " + e.doj.split(" ")[2]) <= new Date(this.props.month + " 01 " + this.state.year)) present = true;
                })
                if (present) {
                    return (
                        <div>
                            <Header logout={this.props.logout} />
                            <div style={{ minHeight: "93vh", display: "flex", flexDirection: "column" }}>
                                <div className="row" style={{ flex: "1" }}>
                                    <div className="container">
                                        <h6 className="text-center pl-2 pr-2 pt-4" style={{ lineHeight: "1" }}><strong>{this.state.business.businessname} {this.state.fy}</strong></h6>
                                        <h6 className="text-center pl-2 pr-2 mb-4" style={{ lineHeight: "1" }}>{this.state.business.alias ? "(" + this.state.business.alias + ")" : ''}</h6>
                                        <h6 className="text-center mb-4"><u>Attendance Report - {this.props.month.toUpperCase()} {this.state.year}</u></h6>
                                        <div className="col-12 d-flex justify-content-center">
                                            <div className="col-12">
                                                <AttendanceChart getEmployee={this.props.getEmployee} year={this.state.year} month={this.props.month} days={this.state.days} getMonthAttendance={this.props.getMonthAttendance} count={count} />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <Footer />
                            </div>
                        </div>
                    );
                }
                else {
                    return (
                        <div>
                            <Header logout={this.props.logout} />
                            <div style={{ minHeight: "93vh", display: "flex", flexDirection: "column" }}>
                                <div className="row" style={{ flex: "1" }}>
                                    <div className="container">
                                        <h6 className="text-center pl-2 pr-2 pt-4" style={{ lineHeight: "1" }}><strong>{this.state.business.businessname} {this.state.fy}</strong></h6>
                                        <h6 className="text-center pl-2 pr-2 mb-4" style={{ lineHeight: "1" }}>{this.state.business.alias ? "(" + this.state.business.alias + ")" : ''}</h6>
                                        <h6 className="text-center mb-4"><u>Attendance Report - {this.props.month} {this.state.year}</u></h6>
                                        <div className="col-12 d-flex justify-content-center">
                                            <div className="col-12">
                                                <div className="text-center text-muted">No Employees having DOJ on or before {this.props.month.toUpperCase()} - {this.state.year}</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <Footer />
                            </div>
                        </div>
                    )
                }
            }
        }
    }
}

export default ViewAttendance;