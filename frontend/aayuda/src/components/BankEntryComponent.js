import React, { Component } from 'react';
import cookie from 'react-cookie';
import { Redirect } from 'react-router-dom';
import { LocalForm, Control } from 'react-redux-form';
import { Header } from './HeaderComponent';
import { Footer } from './Footer';

class BankEntry extends Component {
    constructor(props) {
        super(props);
        this.state = ({
            paymenttype: '',
            paymentmode: 'cash',
            bankid: '',
            remark: '',
            date: new Date().toDateString().split(' ').slice(1).join(' '),
            amount: 0,
            voucherno: this.props.getBankEntry.voucherno ? this.props.getBankEntry.voucherno : 1
        })
        window.onload = () => {
            this.props.refreshBankState();
            this.props.resetBankEntryDetail();
        }
    }

    componentDidMount() {
        if (this.props.addBankEntryMessage.message === 'success') {
            alert("Bank Entry Added Successfully");
            this.props.resetaddBankEntryMessage('success');
        }
        else if (this.props.addBankEntryMessage.message === 'Already Registered') {
            alert("Voucher Number " + (parseInt(this.props.getBankEntry.voucherno) - 1) + " already registered");
            this.props.resetaddBankEntryMessage('success');
        }
        if (this.props.addBankEntryMessage.error !== '') {
            alert(this.props.addBankEntryMessage.error);
            this.setState({
                ...this.props.addBankEntryMessage.state
            })
            this.props.resetaddBankEntryMessage('error');
        }
        if ((this.props.getBankDetail.message === 'initial') && (this.props.getBankDetail.loading === false)) this.props.getBankDetailList(this.props.businessid);
        if ((this.props.getBankEntry.message === 'initial') && (this.props.getBankEntry.loading === false)) this.props.getBankEntryDetail(this.props.businessid, this.props.fyid);
        if (this.props.logoutMessage.error !== '') {
            alert("Error Logging Out. Please Try Again");
            this.props.resetLogoutMessage();
        }
    }

    handleInputChange = (event) => {
        let target = event.target;
        let name = target.name;
        let value = target.value;
        this.setState({
            ...this.state,
            [name]: value
        })

    }

    handleSubmit = () => {
        let business = this.props.businessList.data.filter(b => b.businessid === this.props.businessid)[0];
        let fy = business.financial.filter(f => f.fyid === this.props.fyid)[0].financialyear;
        if (isNaN(parseInt(this.state.voucherno)) || (this.state.voucherno < 0)) alert("Voucher No is mandatory and Must Be In Digits");
        else if (this.state.paymenttype === '') alert("Please Select Deposit/Withdrawl");
        else if (this.state.paymentmode === '') alert("Please Select Payment Mode")
        else if (this.state.bankid === '') alert("Please Select Bank Account");
        else if (!(/^[a-zA-Z]{3} [0-9]{2} [0-9]{4}$/i.test(this.state.date))) alert("Date Must Be In (MON DD YYYY) Format");
        else if ((new Date(this.state.date) < new Date("01 Apr " + fy.split("-")[0])) || (new Date(this.state.date) > new Date("31 Mar " + fy.split("-")[1]))) alert("Invalid Date. Date Must Be Within FY " + fy);
        else if (isNaN(parseInt(this.state.amount)) || (this.state.amount <= 0)) alert("Amount Must Be Greater Than 0");
        else if (this.state.remark.length > 99) alert("Remark Must Be Within 100 Characters");
        else this.props.raiseBankEntry(this.props.businessid, this.props.fyid, this.state);
    }

    render() {
        if (!cookie.load('userservice')) return <Redirect to="/" />;
        else {
            if ((this.props.getBankDetail.loading === true) || (this.props.getBankEntry.loading === true) || (this.props.logoutMessage.loading === true)) {
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div className="mt-3 d-flex justify-content-center">
                            <button className="btn btn-danger">Loading... </button>
                        </div>
                    </div>
                );
            }
            else if ((this.props.getBankDetail.error !== '') || (this.props.getBankEntry.error !== '')) {
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div className="mt-3 d-flex justify-content-center">
                            <button className="btn btn-danger">Error.. Please Refresh and Try Agin </button>
                        </div>
                    </div>
                );
            }
            else {
                let business = this.props.businessList.data.filter(b => b.businessid === this.props.businessid)[0];
                let fy = business.financial.filter(f => f.fyid === this.props.fyid)[0].financialyear;
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div style={{ minHeight: "93vh", display: "flex", flexDirection: "column" }}>
                            <div className="container" style={{ flex: "1" }}>
                                <h6 className="text-center pl-2 pr-2 pt-4" style={{ lineHeight: "1" }}><strong>{business.businessname} {fy}</strong></h6>
                                <h6 className="text-center pl-2 pr-2 mb-4" style={{ lineHeight: "1" }}>{business.alias ? "(" + business.alias + ")" : ''}</h6>
                                <h6 className="text-center pl-2 pr-2"><strong><u>Cash Deposit/Withdrawl From Bank</u></strong></h6>
                                <div className="row" >
                                    <LocalForm className="small" onSubmit={this.handleSubmit}>
                                        <div className="container mt-4 d-flex justify-content-center">
                                            <div class="row col-12 col-md-4">
                                                <div className="form-group mb-1 col-12">
                                                    <label htmlFor="voucherno" className="mb-0 muted-text"><strong>Voucher No</strong><span className="text-danger">*</span></label>
                                                    <Control.text model=".voucherno" className="form-control form-control-sm" id="voucherno" name="voucherno" onChange={this.handleInputChange} value={this.state.voucherno} />
                                                </div>
                                                <div className="form-group mb-1 col-12">
                                                    <label htmlFor="date" className="mb-0 muted-text"><strong>Date<span className="text-danger">*</span></strong></label>
                                                    <Control.text model=".date" className="form-control form-control-sm" id="date" name="date" onChange={this.handleInputChange} value={this.state.date} />
                                                </div>
                                                <div className="form-group mb-1 col-12">
                                                    <label htmlFor="paymenttype" className="mb-0 muted-text"><strong>Select Deposit/Withdrawl:<span className="text-danger">*</span></strong></label>
                                                    <Control.select model=".paymenttype" className="form-control form-control-sm" id="paymenttype" name="paymenttype" onChange={this.handleInputChange} value={this.state.paymenttype}>
                                                        <option value="">Select Entry Type</option>
                                                        <option value="paymentmade">Deposit</option>
                                                        <option value="paymentreceived">Withdrawl</option>
                                                    </Control.select>
                                                </div>
                                                <div className="form-group mb-1 col-12">
                                                    <label htmlFor="bankid" className="mb-0 muted-text"><strong>Bank Name:<span className="text-danger">*</span></strong></label>
                                                    <Control.select model=".bankid" className="form-control form-control-sm" id="bankid" name="bankid" onChange={this.handleInputChange} value={this.state.bankid}>
                                                        <option value="">Select Bank Name</option>
                                                        {this.props.getBankDetail.data.map(b => <option value={b.bankdetailid + "..." + b.bankname}>{b.bankname}</option>)}
                                                    </Control.select>
                                                </div>
                                                <div className="form-group mb-1 col-12">
                                                    <label htmlFor="amount" className="mb-0 muted-text"><strong>Amount<span className="text-danger">*</span></strong></label>
                                                    <Control.text model=".amount" className="form-control form-control-sm" id="amount" name="amount" onChange={this.handleInputChange} value={this.state.amount} />
                                                </div>
                                                <div className="form-group mb-1 col-12">
                                                    <label htmlFor="remark" className="mb-0 muted-text"><strong>Remarks</strong></label>
                                                    <Control.text model=".remark" className="form-control form-control-sm" id="remark" name="remark" onChange={this.handleInputChange} value={this.state.remark} />
                                                </div>
                                                <div className="form-group mb-1 col-12">
                                                    <button type="submit" className="btn btn-success mt-2 btn-sm small" >Bank Entry</button>
                                                </div>
                                            </div>

                                        </div>
                                    </LocalForm>
                                </div>
                            </div>
                            <Footer />
                        </div>
                    </div>
                )
            }
        }
    }
}

export default BankEntry;