import React, { Component } from 'react';
import { Form, Errors, Control } from 'react-redux-form';
import cookie from 'react-cookie';
import { Redirect } from 'react-router-dom'
import { style } from '../sylesheet/stylesheet';

const required = (val) => val && val.length;
const validEmail = (val) => /^[A-Za-z0-9.+-_%$#*]+@[A-Za-z0-9.+-_%$#*]+\.[A-Za-z]{2,4}$/i.test(val);

const SubUserList = function (props) {
        if (props.subUserList.length > 0) {
            return props.subUserList.map ((subUser) => {
                return (
                <div>
                    <span>{subUser.username} - ({subUser.useremail})</span> 
                    <span><button type="submit" className="btn btn-danger btn-sm ml-3" onClick = {() => {return props.deleteSubUser(subUser.useremail);}}>Delete</button></span>
                </div>
                );
            });
        }
        else {
            return (<div>No Sub User Added</div>);
        }
}

const ErrorWrapper = function ({ children }) {
    return (
        <div className="alert alert-danger px-1" style={style.errorWrapper}>{children}</div>
    );
}

const ErrorComponent = function ({ children }) {
    return (
        <div className="pb-1 small">{children}</div>
    );
}

const AddSubUserFormComponent = function ({ handleSubmit }) {
    return (
        <div className="col-md-6">
            <h2 className="mt-3 text-center">
                <span className="text-muted">Add a New SubUser</span>
            </h2>
            <Form model="addSubUser" onSubmit={((values) => handleSubmit(values))}>
                <div className="row justify-content-center mt-5">
                    <div className="form-group col-md-8 mb-1">
                        <label htmlFor="email" className="mb-1"><span className="muted-text">Email Address</span></label>
                        <div className="">
                            <Control.text model=".email" className="form-control form-control-sm" id="email" name="email" placeholder="Email Address of Sub User" validators={{ required, validEmail }} />
                            <Errors className="text-danger small" model=".email" show="touched" messages={{
                                required: "Business Name is Mandatory to Register",
                                validEmail: "Invalid Email Format"
                            }}
                                wrapper={ErrorWrapper}
                                component={ErrorComponent}
                            /> 
                        </div>
                        <div className="mt-3"><button type="submit" className="btn btn-success" >Add</button></div>
                    </div>
                </div>
            </Form>
        </div>
    )
}

class AddSubUser extends Component {

    handleSubmit = (values) => {
        let write = false;
        let update = false;
        let del = false;
        this.props.postAddSubUser(values.email,write,update,del);
        if (this.props.addSubUserMessage.message === "success") this.props.resetAddSubUserForm();
    }
    handleDelete = (subUserEmail) => {
        this.props.deleteSubUser(subUserEmail);
    }

    componentDidMount () {
        if ((this.props.subUserList.message === "") && (this.props.subUserList.loading === false)) this.props.getSubUserList();
        if (this.props.deleteSubUserState.message === "success") window.location.reload();
        if (this.props.addSubUserMessage.message === "Success") window.location.reload();
    }

    render() {
        if (!cookie.load('userservice')) return <Redirect to="/" />
        if (this.props.subUserList.loading === true) return <div><button type="submit" className="btn btn-secondary" >Loading SubUser Data..</button></div>
        if (this.props.addSubUserMessage.loaading === true) return <div><button type="submit" className="btn btn-secondary" >Adding Sub User..</button></div>
        if (this.props.deleteSubUserState.loaading === true) return <div><button type="submit" className="btn btn-secondary" >Deleting..</button></div>
        else return (
            <div>
                <div className="row" style={style.loginHeader}>
                    <div className="container pt-2 pb-3  d-flex justify-content-center">
                        <div className="col-md-10 mt-2 text-white">
                            <div className="row">
                                <div className="col-6 col-md-4 display-4 text-white">Aayuda</div>
                                <div className="col-6 col-md-8 text-right">
                                    WELCOME <strong>Ankit Bagaria</strong>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="container">
                    <div className="col-12 d-flex justify-content-center ">
                        <AddSubUserFormComponent handleSubmit={this.handleSubmit} />
                    </div>
                </div>
                <div className="container">
                    <div className="col-12">
                        <div className="text-center mb-3 mt-5">
                            <h4 className="text-muted">Existing SubUsers</h4>
                        </div>
                        <div className="text-center">
                            <SubUserList subUserList = {this.props.subUserList.data} deleteSubUser = {this.props.deleteSubUser}/>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default AddSubUser;