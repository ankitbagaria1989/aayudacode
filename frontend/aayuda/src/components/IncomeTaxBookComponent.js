import React, { Component } from 'react';
import cookie from 'react-cookie';
import { Redirect } from 'react-router-dom';
import { Header } from './HeaderComponent';
import { Footer } from './Footer';
import { sortData } from '../redux/sortArrayofObjects';
const PreviousYear = function ({ getIncomeTaxOpeningBalance, paymentDetail }) {
    let debit = 0, credit = 0, i = 2;
    let flag = false;
    let returndata = [], returnrow = [];
    let paymentData = paymentDetail.filter(d => d.previouscurrent === 'previous' && (d.customerid === 'tdsreceived' || d.vendorid === 'payit'));
    let openingbalance = getIncomeTaxOpeningBalance.filter(d => d.payreceive === 'receivetds' || d.payreceive === 'payit');
    let t1 =
        <div><h6 className="text-center pl-2 pr-2"><u>Previous Financial Year</u></h6>
            <div className="d-flex small mt-4 p-1 d-md-none d-lg-none">
                <div className="col-5 col-md-5 p-1">
                    <div className="col-12 col-md-6 p-0 "></div>
                </div>
                <div className="col-3 col-md-6 p-0 pt-1 text-right"><strong>Debit</strong></div>
                <div className="col-3 col-md-1 p-0 pt-1 text-right"><strong>Credit</strong></div>
            </div>
            <div className="container small mt-3 d-none d-md-block p-0">
                <div className="d-flex">
                    <div className="col-1 p-0"><strong>Date</strong></div>
                    <div className="col-1 p-0"><strong>Voucher No</strong></div>
                    <div className="col-4 p-0"><strong>Particulars</strong></div>
                    <div className="col-2 text-right"><strong>Debit</strong></div>
                    <div className="col-3 text-right"><strong>Credit</strong></div>
                </div>
            </div></div>

    openingbalance.forEach(d => {
        flag = true;
        if (d.payreceive === 'receivetds') {
            returndata.push(
                <div>
                    <div className="d-flex small p-1 d-md-none d-lg-none" style={i % 2 === 0 ? { backgroundColor: "rgb(181, 253, 233)" } : {}}>
                        <div className="col-5 col-md-5 p-1">
                            <div className="col-12 col-md-6 p-0"><strong>Apr/Opening Bal</strong></div>
                            <div className="col-12 col-md-6 p-0">TDS Payable</div>
                        </div>
                        <div className="col-3 col-md-1 p-0 text-right">{parseFloat(d.amount).toFixed(2)}</div>
                        <div className="col-3 col-md-6 p-1 p-0">
                            <div className="col-12 col-md-2 p-0"></div>
                        </div>
                        <div className="col-1"></div>
                    </div>
                    <div className="container small d-none d-md-block p-1" style={i % 2 === 0 ? { backgroundColor: "rgb(181, 253, 233)" } : {}}>
                        <div className="d-flex">
                            <div className="col-2 p-0">Apr Opening Balance</div>
                            <div className="col-4 p-0"><strong>TDS Receivable</strong></div>
                            <div className="col-2 text-right">{parseFloat(d.amount).toFixed(2)}</div>
                            <div className="col-3"></div>
                        </div>
                    </div>
                </div>
            )
            i++;
            debit = debit + parseFloat(d.amount);
        }
        else if (d.payreceive === 'payit') {
            returndata.push(
                <div>
                    <div className="d-flex small p-1 d-md-none d-lg-none" style={i % 2 === 0 ? { backgroundColor: "rgb(181, 253, 233)" } : {}}>
                        <div className="col-5 col-md-5 p-1">
                            <div className="col-12 col-md-6 p-0"><strong>Apr/Opening Bal</strong></div>
                            <div className="col-12 col-md-6 p-0">Income Tax Payable</div>
                        </div>
                        <div className="col-3 col-md-1 p-0"></div>
                        <div className="col-3 col-md-6 p-1 p-0">
                            <div className="col-12 col-md-2 p-0 text-right">{parseFloat(d.amount).toFixed(2)}</div>
                        </div>
                        <div className="col-1"></div>
                    </div>
                    <div className="container small d-none d-md-block p-1" style={i % 2 === 0 ? { backgroundColor: "rgb(181, 253, 233)" } : {}}>
                        <div className="d-flex">
                            <div className="col-2 p-0">Apr Opening Bal</div>
                            <div className="col-4 p-0"><strong>Income Tax Payable</strong></div>
                            <div className="col-2"></div>
                            <div className="col-3 text-right">{parseFloat(d.amount).toFixed(2)}</div>
                        </div>
                    </div>
                </div>
            )
            i++;
            credit = credit + parseFloat(d.amount);
        }
    })
    paymentData.forEach(d => {
        flag = true;
        if (d.customerid === 'tdsreceived') {
            returndata.push(
                <div>
                    <div className="d-flex small p-1 d-md-none d-lg-none" style={i % 2 === 0 ? { backgroundColor: "rgb(181, 253, 233)" } : {}}>
                        <div className="col-5 col-md-5 p-1">
                            <div className="col-12 col-md-6 p-0"><strong>{d.voucherno / d.date}</strong></div>
                            <div className="col-12 col-md-6 p-0">
                                <div>By {d.bankdetail.split("...")[1]}</div>
                                <div>IT Refund (TDS)</div>
                            </div>
                        </div>
                        <div className="col-3 col-md-1 p-0"></div>
                        <div className="col-3 col-md-6 p-1 p-0">
                            <div className="col-12 col-md-2 p-0 text-right">{parseFloat(d.amount).toFixed(2)}</div>
                        </div>
                        <div className="col-1"></div>
                    </div>
                    <div className="container small d-none d-md-block p-1" style={i % 2 === 0 ? { backgroundColor: "rgb(181, 253, 233)" } : {}}>
                        <div className="d-flex">
                            <div className="col-1 p-0">{d.date}</div>
                            <div className="col-1 p-0">{d.voucherno}</div>
                            <div className="col-4 p-0">
                                <div><strong>By {d.bankdetail.split("...")[1]}</strong></div>
                                <div>IT Refund (TDS)</div>
                            </div>
                            <div className="col-2"></div>
                            <div className="col-3 text-right">{parseFloat(d.amount).toFixed(2)}</div>
                        </div>
                    </div>
                </div>
            )
            i++;
            credit = credit + parseFloat(d.amount);
        }
        else if (d.vendorid === 'payit') {
            returndata.push(
                <div>
                    <div className="d-flex small p-1 d-md-none d-lg-none" style={i % 2 === 0 ? { backgroundColor: "rgb(181, 253, 233)" } : {}}>
                        <div className="col-5 col-md-5 p-1">
                            <div className="col-12 col-md-6 p-0"><strong>{d.voucherno / d.date}</strong></div>
                            <div className="col-12 col-md-6 p-0">
                                <div>To {d.bankdetail.split("...")[1]}</div>
                                <div>Income Tax Paid</div>
                            </div>
                        </div>
                        <div className="col-3 col-md-1 p-0 text-right">{parseFloat(d.amount).toFixed(2)}</div>
                        <div className="col-3 col-md-6 p-1 p-0">
                            <div className="col-12 col-md-2 p-0"></div>
                        </div>
                        <div className="col-1"></div>
                    </div>
                    <div className="container small d-none d-md-block p-1" style={i % 2 === 0 ? { backgroundColor: "rgb(181, 253, 233)" } : {}}>
                        <div className="d-flex">
                            <div className="col-1 p-0">{d.date}</div>
                            <div className="col-1 p-0">{d.voucherno}</div>
                            <div className="col-4 p-0">
                                <div><strong>To {d.bankdetail.split("...")[1]}</strong></div>
                                <div>Income Tax Paid</div>
                            </div>
                            <div className="col-2 text-right">{parseFloat(d.amount).toFixed(2)}</div>
                            <div className="col-3"></div>
                        </div>
                    </div>
                </div>
            )
            i++;
            debit = debit + parseFloat(d.amount);
        }
    })
    if (flag === true) {
        returnrow.push(t1);
        returnrow.push(returndata);
        returnrow.push(
            <div>
                <div className="d-flex small d-md-none">
                    <div className="col-5 p-1"><strong></strong></div>
                    <div className="col-3 p-0 border-top border-dark text-right">{parseFloat(debit).toFixed(2)}</div>
                    <div className="col-3 p-0 border-top border-dark text-right">{parseFloat(credit).toFixed(2)}</div>
                </div>
                <div className="d-flex small d-md-none mt-1">
                    <div className="col-5"><strong>{credit - debit >= 0 ? "Payable" : "Receivable"}</strong></div>
                    <div className="col-3 border-top border-dark p-0 text-right">{credit - debit >= 0 ? parseFloat(credit - debit).toFixed(2) : ''}</div>
                    <div className="col-3 border-top border-dark p-0 text-right">{credit - debit < 0 ? parseFloat(debit - credit).toFixed(2) : ''}</div>
                </div>
                <div className="container small mt-2 d-none d-md-block p-0">
                    <div className="d-flex">
                        <div className="col-2 p-0"><strong></strong></div>
                        <div className="col-4 p-0"><strong></strong></div>
                        <div className="col-2 border-top border-dark text-right">{parseFloat(debit).toFixed(2)}</div>
                        <div className="col-3 border-top border-dark text-right">{parseFloat(credit).toFixed(2)}<strong></strong></div>
                    </div>
                </div>
                <div className="container small mt-2 d-none d-md-block p-0 ">
                    <div className="d-flex">
                        <div className="col-2 p-0"><strong></strong></div>
                        <div className="col-4 p-0"><strong>{credit - debit >= 0 ? "Payable" : "Receivable"}</strong></div>
                        <div className="col-2 border-top border-dark text-right">{credit - debit >= 0 ? parseFloat(credit - debit).toFixed(2) : ''}</div>
                        <div className="col-3 border-top border-dark text-right">{credit - debit < 0 ? parseFloat(debit - credit).toFixed(2) : ''}</div>
                    </div>
                </div>
            </div>
        )
    }
    return returnrow;
}
const DisplayData = function ({ paymentDetail, expenseDetail, businessid, businessList, deleteEntry, fyid, getIncomeOS, getIncomeTaxOpeningBalance }) {
    let returndata = [];
    let data = expenseDetail.filter(e => e.it === "1");
    let paymentData = paymentDetail.filter(p => p.previouscurrent === 'current' && (p.vendorid === 'payit' || p.customerid === 'tdsreceived'));
    let incomeData = getIncomeOS.filter(i => i.tds === '1');
    data = data.concat(paymentData);
    data = data.concat(incomeData);
    data = data.sort(sortData);
    let debit = 0, credit = 0, x = 0, i = 2;
    returndata.push(
        <div><h6 className="text-center pl-2 pr-2 mt-2"><u>Current Financial Year</u></h6>
            <div className="d-flex small mt-4 p-1 d-md-none d-lg-none">
                <div className="col-5 col-md-5 p-1">
                    <div className="col-12 col-md-6 p-0 "></div>
                </div>
                <div className="col-3 col-md-6 p-0 pt-1 text-right"><strong>Debit</strong></div>
                <div className="col-3 col-md-1 p-0 pt-1 text-right"><strong>Credit</strong></div>
            </div>
            <div className="container small mt-3 d-none d-md-block p-0">
                <div className="d-flex">
                    <div className="col-1 p-0"><strong>Date</strong></div>
                    <div className="col-1 p-0"><strong>Voucher No</strong></div>
                    <div className="col-4 p-0"><strong>Particulars</strong></div>
                    <div className="col-2 text-right"><strong>Debit</strong></div>
                    <div className="col-3 text-right"><strong>Credit</strong></div>
                </div>
            </div></div>
    )
    if (x < data.length) {
        if (data[x].type === 'Income') {
            returndata.push(
                <div>
                    <div className=" d-flex small mb-1 p-1 d-md-none d-lg-none" style={i % 2 === 0 ? { backgroundColor: "rgb(181, 253, 233)" } : {}}>
                        <div className="col-6 col-md-5 p-1">
                            <div className="col-12 col-md-6 p-0"><strong>{data[x].voucherno}/{data[x].date}</strong></div>
                            <div className="col-12 col-md-6 p-0 " style={{ lineHeight: "1" }}>To {data[x].category + " " + data[x].type}</div>
                            <div className="col-12 col-md-6 p-0 " style={{ lineHeight: "1" }}>TDS Deducted By {data[x].customername}</div>
                            <div className="col-12 col-md-6 p-0 " style={{ lineHeight: "1" }}>{data[x].description}</div>
                            <div className="col-12 col-md-6 p-0 " style={{ lineHeight: "1" }}>{data[x].transactionno}</div>
                            <div className="col-12 col-md-6 p-0 " style={{ lineHeight: "1" }}>{data[x].remark}</div>
                        </div>
                        <div className="col-3 col-md-6 p-0 pt-1">
                            <div className="col-12 col-md-2 p-0 text-right">{parseFloat(data[x].tdsamount).toFixed(2)}</div>
                        </div>
                        <div className="col-3 col-md-1">
                        </div>
                    </div>
                    <div className="container d-none d-md-block small mb-1 p-1" style={i % 2 === 0 ? { backgroundColor: "rgb(181, 253, 233)" } : {}}>
                        <div className="d-flex">
                            <div className="col-1 p-0">{data[x].date}</div>
                            <div className="col-1 p-0">{data[x].voucherno}</div>
                            <div className="col-4 p-0">
                                <div className="col-12 p-0" style={{ lineHeight: "1" }}><strong>To {data[x].category + " " + data[x].type}</strong></div>
                                <div className="col-12 p-0 " style={{ lineHeight: "1" }}>TDS Deducted By {data[x].customername}</div>
                                <div className="col-12 p-0" style={{ lineHeight: "1" }}>{data[x].description}</div>
                                <div className="col-12 p-0" style={{ lineHeight: "1" }}>{data[x].transactionno}</div>
                                <div className="col-12 p-0" style={{ lineHeight: "1" }}>{data[x].remark}</div>
                            </div>
                            <div className="col-2 text-right">{parseFloat(data[x].tdsamount).toFixed(2)}</div>
                            <div className="col-3"></div>
                            <div className="col-1"></div>
                        </div>
                    </div>
                </div>
            );
            debit = debit + parseFloat(data[x].tdsamount);
        }
        else if (data[x].type === 'Expense') {
            returndata.push(
                <div>
                    <div className=" d-flex small mb-1 p-1 d-md-none d-lg-none" style={i % 2 === 0 ? { backgroundColor: "rgb(181, 253, 233)" } : {}}>
                        <div className="col-5 col-md-5 p-1">
                            <div className="col-12 col-md-6 p-0"><strong>{data[x].voucherno}/{data[x].date}</strong></div>
                            <div className="col-12 col-md-6 p-0 " style={{ lineHeight: "1" }}>By Expense (Income Tax)</div>
                            <div className="col-12 col-md-6 p-0 " style={{ lineHeight: "1" }}>{data[x].description}</div>
                        </div>
                        <div className="col-3 col-md-6 p-0 pt-1">
                        </div>
                        <div className="col-3 col-md-1">
                            <div className="col-12 col-md-2 p-0 text-right">{parseFloat(data[x].amount)}</div>
                        </div>
                    </div>
                    <div className="container d-none d-md-block small mb-1 p-1" style={i % 2 === 0 ? { backgroundColor: "rgb(181, 253, 233)" } : {}}>
                        <div className="d-flex">
                            <div className="col-1 p-0">{data[x].date}</div>
                            <div className="col-1 p-0">{data[x].voucherno}</div>
                            <div className="col-4 p-0">
                                <div className="col-12 p-0"><strong>By Expense (Income Tax)</strong></div>
                                <div className="col-12 p-0">{data[x].description}</div>
                            </div>
                            <div className="col-2"></div>
                            <div className="col-3 text-right">{parseFloat(data[x].amount).toFixed(2)}</div>
                            <div className="col-1"></div>
                        </div>
                    </div>
                </div>
            );
            credit = credit + (data[x].tds === '1' ? (parseFloat(data[x].amount) - parseFloat(data[x].tdsamount)) : parseFloat(data[x].amount));
        }
        else if ((data[x].type === 'payment') && (data[x].vendorid === 'payit')) {
            debit = debit + parseFloat(data[x].amount);
            returndata.push(
                <div>
                    <div className="d-flex small p-1 d-md-none d-lg-none" style={i % 2 === 0 ? { backgroundColor: "rgb(181, 253, 233)" } : {}}>
                        <div className="col-5 col-md-5 p-1">
                            <div className="col-12 col-md-6 p-0"><strong>{data[x].voucherno}/{data[x].date}</strong></div>
                            <div className="col-12 col-md-6 p-0 ">To {data[x].bankdetail.split("...")[1]}</div>
                            <div className="col-12 col-md-6 p-0 ">Payment (Income Tax)</div>
                            <div className="col-12 col-md-6 p-0 ">{data[x].transactionno}</div>
                            <div className="col-12 col-md-6 p-0 ">{data[x].remark}</div>
                        </div>
                        <div className="col-3 col-md-1 text-right">{parseFloat(data[x].amount).toFixed(2)}</div>
                        <div className="col-3 col-md-6 p-1"></div>
                    </div>
                    <div className="container small d-none d-md-block p-1" style={i % 2 === 0 ? { backgroundColor: "rgb(181, 253, 233)" } : {}}>
                        <div className="d-flex">
                            <div className="col-1 p-0">{data[x].date}</div>
                            <div className="col-1 p-0">{data[x].voucherno}</div>
                            <div className="col-4 p-0">
                                <div className="col-12 col-md-6 p-0 "><strong>To {data[x].bankdetail.split("...")[1]}</strong></div>
                                <div className="col-12 col-md-6 p-0 ">Payment (Income Tax)</div>
                                <div className="col-12 col-md-6 p-0 ">{data[x].transactionno}</div>
                                <div className="col-12 col-md-6 p-0 ">{data[x].remark}</div>
                            </div>
                            <div className="col-2 text-right">{parseFloat(data[x].amount).toFixed(2)}</div>
                            <div className="col-3"><strong></strong></div>
                        </div>
                    </div>
                </div >
            )
        }
        else if ((data[x].type === 'payment') && (data[x].customerid === 'tdsreceived')) {
            credit = credit + parseFloat(data[x].amount);
            returndata.push(
                <div>
                    <div className="d-flex small p-1 d-md-none d-lg-none" style={i % 2 === 0 ? { backgroundColor: "rgb(181, 253, 233)" } : {}}>
                        <div className="col-5 col-md-5 p-1">
                            <div className="col-12 col-md-6 p-0"><strong>{data[x].voucherno}/{data[x].date}</strong></div>
                            <div className="col-12 col-md-6 p-0 ">By {data[x].bankdetail.split("...")[1]}</div>
                            <div className="col-12 col-md-6 p-0 ">IT Refund(TDS)</div>
                            <div className="col-12 col-md-6 p-0 ">{data[x].transactionno}</div>
                            <div className="col-12 col-md-6 p-0 ">{data[x].remark}</div>
                        </div>
                        <div className="col-3 col-md-1"></div>
                        <div className="col-3 col-md-6 p-1 text-right">{parseFloat(data[x].amount).toFixed(2)}</div>
                    </div>
                    <div className="container small d-none d-md-block p-1" style={i % 2 === 0 ? { backgroundColor: "rgb(181, 253, 233)" } : {}}>
                        <div className="d-flex">
                            <div className="col-1 p-0">{data[x].date}</div>
                            <div className="col-1 p-0">{data[x].voucherno}</div>
                            <div className="col-4 p-0">
                                <div className="col-12 col-md-6 p-0 "><strong>By {data[x].bankdetail.split("...")[1]}</strong></div>
                                <div className="col-12 col-md-6 p-0 ">IT Refund(TDS)</div>
                                <div className="col-12 col-md-6 p-0 ">{data[x].transactionno}</div>
                                <div className="col-12 col-md-6 p-0 ">{data[x].remark}</div>
                            </div>
                            <div className="col-2"></div>
                            <div className="col-3 text-right">{parseFloat(data[x].amount).toFixed(2)}</div>
                        </div>
                    </div>
                </div>
            )
        }
        x++;
        i++;
    }
    returndata.push(
        <div>
            <div className="d-flex small d-md-none mt-1">
                <div className="col-5 p-1"><strong></strong></div>
                <div className="col-3 p-0 border-top border-dark text-right">{parseFloat(debit).toFixed(2)}</div>
                <div className="col-3 p-0 border-top border-dark text-right">{parseFloat(credit).toFixed(2)}</div>
            </div>
            <div className="d-flex small d-md-none mt-1">
                <div className="col-5"><strong>{credit - debit >= 0 ? "Payable" : "Receivable"}</strong></div>
                <div className="col-3 border-top border-dark p-0 text-right">{credit - debit >= 0 ? parseFloat(credit - debit).toFixed(2) : ''}</div>
                <div className="col-3 border-top border-dark p-0 text-right">{credit - debit < 0 ? parseFloat(debit - credit).toFixed(2) : ''}</div>
            </div>
            <div className="container small mt-2 d-none d-md-block p-0">
                <div className="d-flex">
                    <div className="col-2 p-0"><strong></strong></div>
                    <div className="col-4 p-0"><strong></strong></div>
                    <div className="col-2 border-top border-dark text-right">{parseFloat(debit).toFixed(2)}</div>
                    <div className="col-3 border-top border-dark text-right">{parseFloat(credit).toFixed(2)}</div>
                </div>
            </div>
            <div className="container small mt-2 d-none d-md-block p-0 ">
                <div className="d-flex">
                    <div className="col-2 p-0"><strong></strong></div>
                    <div className="col-4 p-0"><strong>{credit - debit > 0 ? "Payable" : "Receivable"}</strong></div>
                    <div className="col-2 border-top border-dark text-right">{credit - debit > 0 ? parseFloat(credit - debit).toFixed(2) : ''}</div>
                    <div className="col-3 border-top border-dark text-right">{credit - debit <= 0 ? parseFloat(debit - credit).toFixed(2) : ''}</div>
                </div>
            </div>
        </div>
    )

    let returndatatemp = [];
    returndatatemp.push(returndata);
    return returndatatemp;
}
const DisplayExpenseData = function ({ businessid, businessList, getExpense, deleteEntry, fyid, getPayment, getIncomeOS, getIncomeTaxOpeningBalance }) {
    return (
        <div>
            <PreviousYear getIncomeTaxOpeningBalance={getIncomeTaxOpeningBalance.data} paymentDetail={getPayment.data} />
            <DisplayData paymentDetail={getPayment.data} expenseDetail={getExpense.data} businessid={businessid} businessList={businessList} deleteEntry={deleteEntry} fyid={fyid} getIncomeOS={getIncomeOS.data} getIncomeTaxOpeningBalance={getIncomeTaxOpeningBalance.data} />
        </div>
    )
}
class IncomeTaxBook extends Component {
    cancelExpense = (expenseid) => {
        this.props.cancelExpense(expenseid, this.props.businessid, this.props.fyid);
    }
    handleOnChange = (event) => {
        let target = event.target;
        let name = target.name;
        let value = target.value;
        this.setState({
            ...this.state,
            [name]: value
        })
    }
    componentDidMount() {
        window.onload = () => {
            this.props.resetExpenseDetail();
            this.props.resetPaymentDetail();
            this.props.resetIncomeOSDetail();
            this.props.resetIncomeTaxOpeningBalance();
        }
        if ((this.props.getExpense.message === 'initial') && (this.props.getExpense.loading === false)) this.props.getExpenseDetail(this.props.businessid, this.props.fyid);
        if ((this.props.getPayment.message === 'initial') && (this.props.getPayment.loading === false)) this.props.getPaymentDetail(this.props.businessid, this.props.fyid);
        if (this.props.getIncomeOS.message === 'initial') this.props.getIncomeOSDetail(this.props.businessid, this.props.fyid);
        if (this.props.getIncomeTaxOpeningBalance.message === 'initial') this.props.getIncomeTaxOpeningBalanceList(this.props.businessid, this.props.fyid);
        if (this.props.deleteEntryMessage.message === "success") {
            alert("Entry Deleted Successfully");
            this.props.resetDeleteEntry();
        }
        if (this.props.deleteEntryMessage.error !== '') {
            alert("Error Deleting Entry - Please Try Again");
            this.props.resetDeleteEntry();
        }
        if (this.props.logoutMessage.error !== '') {
            alert("Error Logging Out. Please Try Again");
            this.props.resetLogoutMessage();
        }
    }

    render() {
        if (!cookie.load('userservice')) return <Redirect to="/" />;
        else {
            if ((this.props.getExpense.loading === true) || (this.props.getPayment.loading === true) || (this.props.getIncomeOS.loading === true) || (this.props.getIncomeTaxOpeningBalance.loading === true)) {
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div className="mt-3 d-flex justify-content-center">
                            <button className="btn btn-danger">Loading... </button>
                        </div>
                    </div>
                );
            }
            else if ((this.props.getExpense.error !== '') || (this.props.getPayment.error !== '') || (this.props.getIncomeOS.error !== '') || (this.props.getIncomeTaxOpeningBalance.error !== '')) {
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div className="mt-3 d-flex justify-content-center">
                            <button className="btn btn-danger">Error.. Please Refresh and Try Agin </button>
                        </div>
                    </div>
                );
            }
            else {
                let business = this.props.businessList.data.filter(b => b.businessid === this.props.businessid)[0];
                let fy = business.financial.filter(f => f.fyid === this.props.fyid)[0].financialyear;
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div style={{ minHeight: "93vh", display: "flex", flexDirection: "column" }}>
                            <div className="container" style={{ flex: "1" }}>
                                <h6 className="text-center pl-2 pr-2 pt-4" style={{ lineHeight: "1" }}><strong>{business.businessname} {fy}</strong></h6>
                                <h6 className="text-center pl-2 pr-2 mb-4" style={{ lineHeight: "1" }}>{business.alias ? "(" + business.alias + ")" : ''}</h6>
                                <h6 className="text-center pl-2 pr-2"><strong><u>TDS Receivable Ledger (IT Ledger)</u></strong></h6>
                                < DisplayExpenseData businessid={this.props.businessid} businessList={this.props.businessList} getExpense={this.props.getExpense} deleteEntry={this.props.deleteEntry} fyid={this.props.fyid} getPayment={this.props.getPayment} getIncomeOS={this.props.getIncomeOS} getIncomeTaxOpeningBalance={this.props.getIncomeTaxOpeningBalance} />
                            </div>
                            <Footer />
                        </div>
                    </div>
                )
            }
        }
    }
}
export default IncomeTaxBook;
