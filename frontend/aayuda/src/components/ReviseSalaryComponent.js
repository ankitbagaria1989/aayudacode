import React, { Component } from 'react';
import cookie from 'react-cookie';
import { Redirect } from 'react-router-dom';
import { LocalForm, Control } from 'react-redux-form';
import { Header } from './HeaderComponent';
import 'react-calendar/dist/Calendar.css';
import { Footer } from './Footer';

const Option = function () {
    let i = 2015;
    let max = 2100;
    let returnrows = [];
    while (i < max) {
        returnrows.push(<option value={i}>{i}</option>);
        i++;
    }
    return returnrows;
}
const ReviseSalaryForm = function ({ handleSubmit, handleInputChange, state }) {
    return (
        <div className="col-12">
            <LocalForm onSubmit={((values) => handleSubmit(values))}>
                <div className="justify-content-center mt-4">
                    <div className="form-group col-12 mb-0">
                        <label htmlFor="salary" className="mb-0"><span className="muted-text">New Monthly Salary<span className="text-danger">*</span></span></label>
                        <div className="">
                            <Control.text model=".salary" className="form-control form-control-sm pt-3 pb-3" id="salary" name="salary" placeholder="New Monthly Salary" onChange={handleInputChange} value={state.salary} />
                        </div>
                    </div>
                    <div className="d-flex">
                        <div className="form-group col-6">
                            <label htmlFor="month" className="mb-0"><span className="muted-text">Effective Month<span className="text-danger">*</span></span></label>
                            <div className="">
                                <Control.select model=".month" className="form-control form-control-sm" id="month" name="month" onChange={handleInputChange} value={state.month}>
                                    <option value="">Select month</option>
                                    <option value="Jan">Jan</option>
                                    <option value="Feb">Feb</option>
                                    <option value="Mar">Mar</option>
                                    <option value="Apr">Apr</option>
                                    <option value="May">May</option>
                                    <option value="Jun">Jun</option>
                                    <option value="Jul">Jul</option>
                                    <option value="Aug">Aug</option>
                                    <option value="Sep">Sep</option>
                                    <option value="Oct">Oct</option>
                                    <option value="Nov">Nov</option>
                                    <option value="Dec">Dec</option>
                                </Control.select>
                            </div>
                        </div>
                        <div className="form-group col-6">
                            <label htmlFor="year" className="mb-0"><span className="muted-text">Effective Year<span className="text-danger">*</span></span></label>
                            <div className="">
                                <Control.select model=".year" className="form-control form-control-sm" id="year" name="year" onChange={handleInputChange} value={state.year}>
                                    <option value="">Select Year</option>
                                    <Option />
                                </Control.select>
                            </div>
                        </div>
                    </div>
                    <div className="form-group col-12">
                        <div className=""><button type="submit" className="btn btn-success btn-sm pl-4 pr-4" >Add</button></div>
                    </div>
                </div>
            </LocalForm>
        </div>
    );
}
const DisplaySalary = function ({ salary }) {
    let keys = Object.keys(salary);
    let i = keys.length - 1;
    let returnrows = [];
    while (i >= 0) {
        returnrows.push(<div className="card card-body p-2">
            <div className="">{JSON.parse(salary[i]).date} - Rs. {JSON.parse(salary[i]).salary}</div>
        </div>);
        i--;
    }
    return returnrows;
}
const SalaryList = function ({ employee }) {
    let salary = JSON.parse(employee.salary)
    return (<div className="col-12">
        <div className="">
            <DisplaySalary salary={salary} />
        </div>
    </div>)
}

class ReviseSalary extends Component {
    constructor(props) {
        super(props);
        this.state = {
            salary: '',
            month: '',
            year: ''
        }
    }
    componentDidMount() {
        if (this.props.reviseSalaryMessage.message === 'success') {
            alert("Salary Revised SUccessfully");
            this.props.resetReviseSalaryMessage();
        }
        else if (this.props.reviseSalaryMessage.error !== '') {
            alert(this.props.reviseSalaryMessage.error);
            this.setState({
                ...this.props.reviseSalaryMessage.state
            })
            this.props.resetReviseSalaryMessage();
        }
        if (this.props.logoutMessage.error !== '') {
            alert("Error Logging Out. Please Try Again");
            this.props.resetLogoutMessage();
        }
    }
    handleInputChange = (event) => {
        const target = event.target;
        const name = target.name;
        const value = target.value;
        this.setState({
            [name]: value,
        });
    }
    handleSubmit = () => {
        let employee = this.props.getEmployee.data.filter(e => e.employeeid === this.props.employeeid)[0];
        let salary = JSON.parse(employee.salary);
        let keys = Object.keys(salary);
        let i = keys.length;
        let latestdate = JSON.parse(salary[i - 1]).date.split(" ");
        let lastdate = new Date(latestdate[0] + " 01 " + latestdate[1]);
        let newdate = new Date(this.state.month + " 01 " + this.state.year);
        if (this.state.salary === '') alert("Please Enter New Salary");
        else if (isNaN(parseInt(this.state.salary)) || (this.state.totalamountbasic <= 0)) alert("Salary Must Be Greater Than 0");
        else if (this.state.month === '') alert("Please Select Effective Month");
        else if (this.state.month === '') alert("Please Select Effective Year");
        else if (newdate <= lastdate) alert("Date For Salary Revision Must Be After Previous Salary Applicable Salary Date");
        else this.props.reviseSalary(this.state, this.props.businessid, this.props.employeeid, this.props.getEmployee);
    }
    render() {
        if (!cookie.load('userservice')) return <Redirect to="/" />
        else {
            if ((this.props.reviseSalaryMessage.loading === true) || (this.props.logoutMessage.loading === true)) {
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div className="mt-3 d-flex justify-content-center">
                            <button className="btn btn-danger">Loading... </button>
                        </div>
                    </div>
                );
            }
            else {
                let business = this.props.businessList.data.filter(b => b.businessid === this.props.businessid)[0];
                let employee = this.props.getEmployee.data.filter(e => e.employeeid === this.props.employeeid)[0];
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div style={{ minHeight: "93vh", display: "flex", flexDirection: "column" }}>
                            <div className="container d-flex justify-content-center pr-0 pl-0">
                                <div className="col-12 col-md-5">
                                    <div className="small pr-0 pl-0">
                                        <h6 className="text-center pl-2 pr-2 pt-4" style={{ lineHeight: "1" }}><strong>{business.businessname}</strong></h6>
                                        <h6 className="text-center pl-2 pr-2 mb-4" style={{ lineHeight: "1" }}>{business.alias ? "(" + business.alias + ")" : ''}</h6><h6 className="text-center pl-2 pr-2 mb-2"><strong><u>Revise Salary</u></strong></h6>
                                        <h6 className="text-center pl-2 pr-2 mb-2"><strong><u>{employee.employeename}</u></strong></h6>
                                        <ReviseSalaryForm handleSubmit={this.handleSubmit} handleInputChange={this.handleInputChange} state={this.state} />
                                    </div>
                                </div>
                            </div>
                            <div className="container d-flex justify-content-center pr-0 pl-0 mt-4 pb-4" style={{ flex: "1" }}>
                                <div className="col-12 col-md-5">
                                    <div className="pr-0 pl-0">
                                        <SalaryList employee={employee} />
                                    </div>
                                </div>
                            </div>
                            <Footer />
                        </div>
                    </div>
                );
            }
        }
    }
}

export default ReviseSalary;