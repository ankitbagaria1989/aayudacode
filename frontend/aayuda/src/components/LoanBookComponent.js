import React, { Component } from 'react';
import cookie from 'react-cookie';
import { Redirect } from 'react-router-dom';
import { Header } from './HeaderComponent';
import { LocalForm, Control } from 'react-redux-form';
import { Footer } from './Footer';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTrash } from '@fortawesome/free-solid-svg-icons';

const DataLoan = function ({ data, deleteEntry, businessid, fyid, i }) {
    let cancel = function () {
        if (window.confirm("Delete Entry")) deleteEntry(data.id, businessid, fyid, 'loan');
    }
    if ((data.loantype === 'given') && (data.loanrepayment === "repayment")) {
        return (<div><div className="d-flex small mb-1 p-1 d-md-none d-lg-none" style={i % 2 === 0 ? { backgroundColor: "rgb(181, 253, 233)" } : {}}>
            <div className="col-5 col-md-5 p-1">
                <div className="col-12 col-md-6 p-0"><strong>{data.voucherno + "/" + data.date}</strong></div>
                <div className="col-12 col-md-6 p-0" style={{ lineHeight: "1" }}>{data.paymentmode === 'cash' ? "To Cash" : "To " + data.bankdetail.split("...")[1]}</div>
                <div className="col-12 col-md-6 p-0" style={{ lineHeight: "1" }}>Repayment Made</div>
                <div className="col-12 col-md-6 p-0 " style={{ lineHeight: "1" }}>{data.remark}</div>
            </div>
            <div className="col-3 col-md-6 p-0 text-right">{parseFloat(data.amount).toFixed(2)}</div>
            <div className="col-3 col-md-1 p-0"></div>
            <div className="col-1 col-md-1 p-0 d-flex justify-content-center">
                <div className="col-12 mt-2"><FontAwesomeIcon icon={faTrash} onClick={() => cancel()} size="lg" /></div>
            </div>
        </div>
            <div className="container d-none d-md-block small p-1" style={i % 2 === 0 ? { backgroundColor: "rgb(181, 253, 233)" } : {}}>
                <div className="d-flex">
                    <div className="col-1 p-0">{data.date}</div>
                    <div className="col-1 p-0">{data.voucherno}</div>
                    <div className="col-4 p-0">
                        <div className="col-12 col-md-6 p-0" style={{ lineHeight: "1" }}><strong>{data.paymentmode === 'cash' ? "To Cash" : "To " + data.bankdetail.split("...")[1]}</strong></div>
                        <div className="col-12 p-0">Repayment Made</div>
                        <div className="col-12 p-0">{data.remark}</div>
                    </div>
                    <div className="col-2 text-right">{parseFloat(data.amount).toFixed(2)}</div>
                    <div className="col-3"></div>
                    <div className="col-1"><FontAwesomeIcon icon={faTrash} onClick={() => cancel()} size="lg" /></div>
                </div>
            </div></div>)
    }
    else if ((data.loantype === 'received') && (data.loanrepayment === "loan")) {
        return (<div><div className="d-flex small mb-1 p-1 d-md-none d-lg-none" style={i % 2 === 0 ? { backgroundColor: "rgb(181, 253, 233)" } : {}}>
            <div className="col-5 col-md-5 p-1">
                <div className="col-12 col-md-6 p-0"><strong>{data.voucherno + "/" + data.date}</strong></div>
                <div className="col-12 col-md-6 p-0" style={{ lineHeight: "1" }}>{data.paymentmode === 'cash' ? "By Cash" : "By " + data.bankdetail.split("...")[1]}</div>
                <div className="col-12 col-md-6 p-0" style={{ lineHeight: "1" }}>Loan Received</div>
                <div className="col-12 col-md-6 p-0 " style={{ lineHeight: "1" }}>{data.remark}</div>
            </div>
            <div className="col-3 col-md-6 p-0"></div>
            <div className="col-3 col-md-1 p-0 text-right">{parseFloat(data.amount).toFixed(2)}</div>
            <div className="col-1 col-md-1 p-0 d-flex justify-content-center">
                <div className="col-12 mt-2"><FontAwesomeIcon icon={faTrash} onClick={() => cancel()} size="lg" /></div>
            </div>
        </div>
            <div className="container d-none d-md-block small p-1" style={i % 2 === 0 ? { backgroundColor: "rgb(181, 253, 233)" } : {}}>
                <div className="d-flex">
                    <div className="col-1 p-0">{data.date}</div>
                    <div className="col-1 p-0">{data.voucherno}</div>
                    <div className="col-4 p-0">
                        <div className="col-12 col-md-6 p-0" style={{ lineHeight: "1" }}><strong>{data.paymentmode === 'cash' ? "By Cash" : "By " + data.bankdetail.split("...")[1]}</strong></div>
                        <div className="col-12 p-0">Loan Received </div>
                        <div className="col-12 p-0">{data.remark}</div>
                    </div>
                    <div className="col-2"></div>
                    <div className="col-3 text-right">{parseFloat(data.amount).toFixed(2)}</div>
                    <div className="col-1"><FontAwesomeIcon icon={faTrash} onClick={() => cancel()} size="lg" /></div>
                </div>
            </div></div>)
    }
    else if ((data.loantype === 'given') && (data.loanrepayment === "loan")) {
        return (<div><div className="d-flex small mb-1 p-1 d-md-none d-lg-none" style={i % 2 === 0 ? { backgroundColor: "rgb(181, 253, 233)" } : {}}>
            <div className="col-5 col-md-5 p-1">
                <div className="col-12 col-md-6 p-0"><strong>{data.voucherno + "/" + data.date}</strong></div>
                <div className="col-12 col-md-6 p-0" style={{ lineHeight: "1" }}>{data.paymentmode === 'cash' ? "To Cash" : "To " + data.bankdetail.split("...")[1]}</div>
                <div className="col-12 col-md-6 p-0" style={{ lineHeight: "1" }}>Loan Given</div>
                <div className="col-12 col-md-6 p-0 " style={{ lineHeight: "1" }}>{data.remark}</div>
            </div>
            <div className="col-3 col-md-6 p-0 text-right">{parseFloat(data.amount).toFixed(2)}</div>
            <div className="col-3 col-md-1 p-0"></div>
            <div className="col-1 col-md-1 p-0 d-flex justify-content-center">
                <div className="col-12 mt-2"><FontAwesomeIcon icon={faTrash} onClick={() => cancel()} size="lg" /></div>
            </div>
        </div>
            <div className="container d-none d-md-block small p-1" style={i % 2 === 0 ? { backgroundColor: "rgb(181, 253, 233)" } : {}}>
                <div className="d-flex">
                    <div className="col-1 p-0">{data.date}</div>
                    <div className="col-1 p-0">{data.voucherno}</div>
                    <div className="col-4 p-0">
                        <div className="col-12 col-md-6 p-0" style={{ lineHeight: "1" }}><strong>{data.paymentmode === 'cash' ? "To Cash" : "To " + data.bankdetail.split("...")[1]}</strong></div>
                        <div className="col-12 p-0">Loan Given </div>
                        <div className="col-12 p-0">{data.remark}</div>
                    </div>
                    <div className="col-2 text-right">{parseFloat(data.amount).toFixed(2)}</div>
                    <div className="col-3"></div>
                    <div className="col-1"><FontAwesomeIcon icon={faTrash} onClick={() => cancel()} size="lg" /></div>
                </div>
            </div></div>)
    }
    else if ((data.loantype === 'received') && (data.loanrepayment === "repayment")) {
        return (<div><div className="d-flex small mb-1 p-1 d-md-none d-lg-none" style={i % 2 === 0 ? { backgroundColor: "rgb(181, 253, 233)" } : {}}>
            <div className="col-5 col-md-5 p-1">
                <div className="col-12 col-md-6 p-0"><strong>{data.voucherno + "/" + data.date}</strong></div>
                <div className="col-12 col-md-6 p-0" style={{ lineHeight: "1" }}>{data.paymentmode === 'cash' ? "By Cash" : "By " + data.bankdetail.split("...")[1]}</div>
                <div className="col-12 col-md-6 p-0" style={{ lineHeight: "1" }}>Repayment Received</div>
                <div className="col-12 col-md-6 p-0 " style={{ lineHeight: "1" }}>{data.remark}</div>
            </div>
            <div className="col-3 col-md-6 p-0"></div>
            <div className="col-3 col-md-1 p-0 text-right">{parseFloat(data.amount).toFixed(2)}</div>
            <div className="col-1 col-md-1 p-0 d-flex justify-content-center">
                <div className="col-12 mt-2"><FontAwesomeIcon icon={faTrash} onClick={() => cancel()} size="lg" /></div>
            </div>
        </div>
            <div className="container d-none d-md-block small p-1" style={i % 2 === 0 ? { backgroundColor: "rgb(181, 253, 233)" } : {}}>
                <div className="d-flex">
                    <div className="col-1 p-0">{data.date}</div>
                    <div className="col-1 p-0">{data.voucherno}</div>
                    <div className="col-4 p-0">
                        <div className="col-12 col-md-6 p-0" style={{ lineHeight: "1" }}>{data.paymentmode === 'cash' ? "By Cash" : "By " + data.bankdetail.split("...")[1]}</div>
                        <div className="col-12 p-0">Repayment Received </div>
                        <div className="col-12 p-0">{data.remark}</div>
                    </div>
                    <div className="col-2"></div>
                    <div className="col-3 text-right">{parseFloat(data.amount).toFixed(2)}</div>
                    <div className="col-1"><FontAwesomeIcon icon={faTrash} onClick={() => cancel()} size="lg" /></div>
                </div>
            </div></div>)
    }
}

const DataDisplay = function ({ getLoan, state, getLoanOpeningBalance, deleteEntry, businessid, fyid }) {
    let loandata = getLoan.data.filter(d => d.loanaccount.split("...")[0] === state.loanaccount);
    let loanopeningbalance = getLoanOpeningBalance.data.filter(d => d.loanaccount === state.loanaccount)[0];
    let debit = undefined, credit = undefined;
    if (loanopeningbalance) {
        if (loanopeningbalance.payreceive === "pay") { credit = loanopeningbalance.amount; debit = 0 }
        if (loanopeningbalance.payreceive === "receive") { debit = loanopeningbalance.amount; credit = 0 }
    }
    let datarow = [];
    let i = 3;
    datarow.push(<div className="d-flex small mt-4 p-1 d-md-none d-lg-none">
        <div className="col-5 col-md-5 p-1">
            <div className="col-12 col-md-6 p-0 "></div>
        </div>
        <div className="col-3 col-md-6 p-0 pt-1"><strong>Debit</strong></div>
        <div className="col-3 col-md-1 pt-1"><strong>Credit</strong></div>
    </div>);
    datarow.push(
        <div className="container small mt-3 d-none d-md-block p-0">
            <div className="d-flex">
                <div className="col-1 p-0"><strong>Date</strong></div>
                <div className="col-1 p-0"><strong>Voucher No</strong></div>
                <div className="col-4 p-0"><strong>Particulars</strong></div>
                <div className="col-2"><strong>Debit</strong></div>
                <div className="col-3"><strong>Credit</strong></div>
                <div className="col-1"></div>
            </div>
        </div>
    );
    if (debit !== 0 || credit !== 0) {
        datarow.push(<div className="d-flex small mt-4 p-1 d-md-none d-lg-none" style={i % 2 === 0 ? { backgroundColor: "rgb(181, 253, 233)" } : {}}>
            <div className="col-5 col-md-5 p-1">
                <div className="col-12 col-md-6 p-0 ">Apr Opening Bal</div>
            </div>
            <div className="col-3 col-md-6 p-0 pt-1 text-right">{debit ? debit : debit = 0}</div>
            <div className="col-3 col-md-1 pt-1 text-right">{credit ? credit : credit = 0}</div>
        </div>)
        datarow.push(
            <div className="container small mt-3 d-none d-md-block p-0 " style={i % 2 === 0 ? { backgroundColor: "rgb(181, 253, 233)" } : {}}>
                <div className="d-flex">
                    <div className="col-2 p-0">Apr Opening Balance</div>
                    <div className="col-4 p-0"></div>
                    <div className="col-2 text-right">{debit ? debit : debit = 0}</div>
                    <div className="col-3 text-right">{credit ? credit : credit = 0}</div>
                    <div className="col-1"></div>
                </div>
            </div>
        )
        i++;
    }
    loandata.forEach(data => {
        if ((data.loantype === 'given') && (data.loanrepayment === "repayment")) {
            datarow.push(<DataLoan data={data} deleteEntry={deleteEntry} businessid={businessid} fyid={fyid} i={i} />)
            debit = debit + data.amount;
        }
        else if ((data.loantype === 'received') && (data.loanrepayment === "loan")) {
            datarow.push(<DataLoan data={data} deleteEntry={deleteEntry} businessid={businessid} fyid={fyid} i={i} />)
            credit = credit + data.amount;
        }
        else if ((data.loantype === 'given') && (data.loanrepayment === "loan")) {
            datarow.push(<DataLoan data={data} deleteEntry={deleteEntry} businessid={businessid} fyid={fyid} i={i} />)
            debit = debit + data.amount;
        }
        else if ((data.loantype === 'received') && (data.loanrepayment === "repayment")) {
            datarow.push(<DataLoan data={data} deleteEntry={deleteEntry} businessid={businessid} fyid={fyid} i={i} />)
            credit = credit + data.amount;
        }
        i++;
    })
    datarow.push(
        <div>
            <div className="d-flex small d-md-none mt-1">
                <div className="col-5 p-1"><strong></strong></div>
                <div className="col-3 p-0 border-top border-dark text-right">{parseFloat(debit).toFixed(2)}</div>
                <div className="col-3 p-0 border-top border-dark text-right">{parseFloat(credit).toFixed(2)}</div>
            </div>
            <div className="row small d-md-none mt-1">
                <div className="col-5"><strong>{credit >= debit ? "Payable" : "Receivable"}</strong></div>
                <div className="col-3 border-top border-dark p-0 text-right">{credit >= debit ? parseFloat(credit - debit).toFixed(2) : ''}</div>
                <div className="col-3 border-top border-dark p-0 text-right">{credit < debit ? parseFloat(debit - credit).toFixed(2) : ''}</div>
            </div>
            <div className="container small mt-2 d-none d-md-block p-0">
                <div className="d-flex">
                    <div className="col-1 p-0"><strong></strong></div>
                    <div className="col-1 p-0"><strong></strong></div>
                    <div className="col-4 p-0"></div>
                    <div className="col-2 border-top border-dark text-right">{parseFloat(debit).toFixed(2)}</div>
                    <div className="col-3 border-top border-dark text-right">{parseFloat(credit).toFixed(2)}</div>
                    <div className="col-1"></div>
                </div>
            </div>
            <div className="container small mt-2 d-none d-md-block p-0 ">
                <div className="d-flex">
                    <div className="col-1 p-0"><strong></strong></div>
                    <div className="col-1 p-0"><strong></strong></div>
                    <div className="col-4 p-0">{credit >= debit ? "Payable" : "Receivable"}</div>
                    <div className="col-2 border-top border-dark text-right">{credit >= debit ? parseFloat(credit - debit).toFixed(2) : ''}</div>
                    <div className="col-3 border-top border-dark text-right">{credit < debit ? parseFloat(debit - credit).toFixed(2) : ''}</div>
                    <div className="col-1"></div>
                </div>
            </div>
        </div>
    )
    return datarow;
}

class LoanBook extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loanaccount: ''
        }
        window.onload = () => {
            this.props.refreshLoanState();
            this.props.refreshLoanAccountState();
            this.props.resetLoanOpeningBalance();
        }
    }
    componentDidMount() {
        if (this.props.getLoanAccount.message === 'initial') this.props.getLoanAccountList(this.props.businessid, this.props.fyid);
        if (this.props.getLoan.message === 'initial') this.props.getLoanDetail(this.props.businessid, this.props.fyid);
        if (this.props.getLoanOpeningBalance.message === 'initial') this.props.getLoanOpeningBalanceList(this.props.businessid, this.props.fyid);
        if (this.props.logoutMessage.error !== '') {
            alert("Error Logging Out. Please Try Again");
            this.props.resetLogoutMessage();
        }
        if (this.props.deleteEntryMessage.message === "success") {
            alert("Entry Deleted Successfully");
            this.props.resetDeleteEntry();
        }
        if (this.props.deleteEntryMessage.error !== '') {
            alert("Error Deleting Entry - Please Try Again");
            this.props.resetDeleteEntry();
        }
        if (this.props.logoutMessage.error !== '') {
            alert("Error Logging Out. Please Try Again");
            this.props.resetLogoutMessage();
        }
    }
    handleInputChange = (event) => {
        let target = event.target;
        let name = target.name;
        let value = target.value;
        this.setState({
            ...this.state,
            [name]: value
        })
    }
    render() {
        if (!cookie.load('userservice')) return <Redirect to="/" />;
        else {
            if ((this.props.getLoan.loading === true) || (this.props.logoutMessage.loading === true) || (this.props.getLoanAccount.loading === true) || (this.props.deleteEntryMessage.loading === true)) {
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div className="mt-3 d-flex justify-content-center">
                            <button className="btn btn-danger">Loading... </button>
                        </div>
                    </div>
                );
            }
            else if ((this.props.getLoan.error !== '') || (this.props.getLoanAccount.error !== '')) {
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div className="mt-3 d-flex justify-content-center">
                            <button className="btn btn-danger">Error.. Please Refresh and Try Agin </button>
                        </div>
                    </div>
                );
            }
            else {
                let business = this.props.businessList.data.filter(b => b.businessid === this.props.businessid)[0];
                let fy = business.financial.filter(f => f.fyid === this.props.fyid)[0].financialyear;
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div style={{ minHeight: "93vh", display: "flex", flexDirection: "column" }}>
                            <div className="container" style={{ flex: "1" }}>
                                <h6 className="text-center pl-2 pr-2 pt-4" style={{ lineHeight: "1" }}><strong>{business.businessname} {fy}</strong></h6>
                                <h6 className="text-center pl-2 pr-2 mb-4" style={{ lineHeight: "1" }}>{business.alias ? "(" + business.alias + ")" : ''}</h6>
                                <h6 className="text-center pl-2 pr-2 mb-2"><strong><u>Loan Book</u></strong></h6>
                                <div className="row" >
                                    <LocalForm className="container small col-12 col-md-4" onSubmit={this.handleSubmit}>
                                        <div className="form-group mb-1 col-12">
                                            <label htmlFor="loanaccount" className="mb-0 muted-text"><strong>Select Loan Account:<span className="text-danger">*</span></strong></label>
                                            <Control.select model=".loanaccount" className="form-control form-control-sm" id="loanaccount" name="loanaccount" onChange={this.handleInputChange} value={this.state.loanaccount}>
                                                <option value="">Select Loan Account</option>
                                                {this.props.getLoanAccount.data.map(la => <option value={la.id}>{la.loanaccount}</option>)}
                                            </Control.select>
                                        </div>
                                    </LocalForm>
                                </div>
                                {this.state.loanaccount !== '' ? <DataDisplay getLoan={this.props.getLoan} state={this.state} getLoanOpeningBalance={this.props.getLoanOpeningBalance} deleteEntry={this.props.deleteEntry} businessid={this.props.businessid} fyid={this.props.fyid} /> : ''}
                            </div>
                            <Footer />
                        </div>
                    </div>
                )
            }
        }
    }
}

export default LoanBook;