import React, { Component, useState } from 'react';
import cookie from 'react-cookie';
import { Redirect, Link } from 'react-router-dom';
import { Button, Collapse } from 'reactstrap';
import { Form, Errors, Control } from 'react-redux-form';
import { Header } from './HeaderComponent';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faAngleDoubleDown } from '@fortawesome/free-solid-svg-icons';
import { Footer } from './Footer';

const required = (val) => val && val.length;
const maxLength = (len) => val => val ? val.length < len : true;
const isNumber = (val) => !isNaN(parseInt(val));
const length = (len) => val => (val) ? val.length === len : true;

const ErrorComponent = function ({ children }) {
    return (
        <div style={{ lineHeight: "1" }}>{children}</div>
    )
}

const AddItem = function ({ handleSubmit, addItemMessage, handleInputChange, taxtype }) {
    let [isOpen, setIsOpen] = useState(false);
    let toggle = () => setIsOpen((!isOpen));
    if (addItemMessage.error !== '') {
        isOpen = true;
    }
    return (
        <div className="col-12">
            <Button className="btn-sm mt-3 col-12" style={{ backgroundColor: "#fff", border: "1px solid #28a745", color: "#28a745" }} onClick={toggle}><div className="d-flex"><span className="col-11 text-center">Add New Item</span><span className="col-1 p-0 text-right"><FontAwesomeIcon size="sm" icon={faAngleDoubleDown} /></span></div></Button>
            <Collapse isOpen={isOpen}>
                <Form model="addItemForm" onSubmit={((values) => handleSubmit(values))}>
                    <div className="justify-content-center mt-4">
                        <div className="form-group col-12 mb-1">
                            <label htmlFor="itemname" className="mb-0"><span>Item Name</span><span className="text-danger">*</span></label>
                            <div className="">
                                <Control.text model=".itemname" className="form-control form-control-sm" id="itemname" name="itemname" placeholder="Item Name" validators={{ required, maxLength: maxLength(100) }} />
                                <Errors className="text-danger" model=".itemname" show="touched" messages={{
                                    required: "Bank Name is Mandatory to Register",
                                    maxLength: "length must be within 100 characters"
                                }}
                                    component={ErrorComponent}
                                />
                            </div>
                        </div>
                        <div className="form-group col-12 mb-1">
                            <label htmlFor="alias" className="mb-0"><span>Alias</span></label>
                            <div className="">
                                <Control.text model=".alias" className="form-control form-control-sm" id="alias" name="alias" placeholder="Alias" validators={{ maxLength: maxLength(100) }} />
                                <Errors className="text-danger" model=".alias" show="touched" messages={{
                                    maxLength: "length must be within 100 characters"
                                }}
                                    component={ErrorComponent}
                                />
                            </div>
                        </div>
                        <div className="form-group col-12 mb-1">
                            <label className="mb-0" htmlFor="hsncode">HSN Code</label>
                            <div className="">
                                <Control.text model=".hsncode" className="form-control form-control-sm" id="hsncode" name="hsncode" placeholder="HSN Code" validators={{ isNumber, maxLength: length(8) }} />
                                <Errors className="text-danger" model=".hsncode" show="touched" messages={{
                                    isNumber: "Only numbers allowed ",
                                    maxLength: "length must be within 8 characters"
                                }}
                                    component={ErrorComponent}
                                />
                            </div>
                        </div>
                        <div className="form-group col-12 mb-1">
                            <label className="mb-0" htmlFor="gstrate">GST Rate (Eg: 18 for 18%)</label>
                            <div className="">
                                <Control.text model=".gstrate" className="form-control form-control-sm" id="gstrate" name="gstrate" placeholder="GST Rate" validators={{ isNumber, maxLength: maxLength(3) }} />
                                <Errors className="text-danger" model=".gstrate" show="touched" messages={{
                                    isNumber: "Only numbers allowed ",
                                    maxLength: "length must 2 digits or less"
                                }}
                                    component={ErrorComponent}
                                />
                            </div>
                        </div>
                        <div className="form-group col-12 mb-1">
                            <label className="mb-0" htmlFor="billingunit">Billing Unit</label>
                            <div className="">
                                <Control.text model=".billingunit" className="form-control form-control-sm" id="billingunit" name="billingunit" placeholder="eg: 1000 pcs OR kg or MT etc..etc.." validators={{ maxLength: maxLength(20) }} />
                                <Errors className="text-danger" model=".billingunit" show="touched" messages={{
                                    maxLength: "length must be with in 20 characters"
                                }}
                                    component={ErrorComponent}
                                />
                            </div>
                            <div className="mt-3"><button type="submit" className="btn btn-success btn-sm pl-4 pr-4" >Add</button></div>
                        </div>
                    </div>
                </Form>
            </Collapse>
        </div>
    );
}



const Tax = function ({ taxtype, gstrate, vatrate, exciserate }) {
    if (taxtype === "gst") return <div>GST Rate : {gstrate} %</div>
    else if (taxtype === "excise") {
        return (
            <div>
                <div>Vat Rate : {vatrate} %</div>
                <div>Excise Rate : {exciserate} %</div>
            </div>
        );
    }
}

const ItemListCard = function ({ item }) {
    let [isOpen, setIsOpen] = useState(false);
    let toggle = () => setIsOpen((!isOpen));
    return (
        <div className="col-12 justify-content-center">
            <div className="">
                <Button className="btn-sm btn mt-2 col-12 btn-success" onClick={toggle}><div className="d-flex"><span className="col-11">{item.itemname}{item.alias !== '' ? " ( " + item.alias + " ) "  : ''}</span><span className="col-1 p-0 text-right"><FontAwesomeIcon size="sm" icon={faAngleDoubleDown} /></span></div></Button>
                <Collapse isOpen={isOpen}>
                    <div className="card card-body mt-1">
                        <div>HSN Code: {item.hsncode}</div>
                        <Tax taxtype={item.taxtype} gstrate={item.gstrate} vatrate={item.vatrate} exciserate={item.exciserate} />
                        <div>Billing Unit : {item.billingunit}</div>
                        <div><Link to={`/business/businessdetail/item/${item.businessid}/${item.itemid}`}> <button className="btn btn-sm justify-content-right mt-2 pr-4 pl-4" role="button" style={{ backgroundColor: "#fff", border: "1px solid #28a745", color: "#28a745" }}>Edit</button></Link></div>
                    </div>
                </Collapse>
            </div>
        </div>
    );
}

const ItemList = function ({ getItem }) {
    if (getItem.error === '') {
        if (getItem.data.length > 0) return getItem.data.map((it) => <ItemListCard item={it} />);
        else return <h6 className="col-12 text-center text-muted mt-2 pb-4">No Items Added Yet</h6>;
    }
    else return <div></div>
}

class Item extends Component {
    constructor(props) {
        super(props);
        this.state = {
            taxtype: "gst"
        }

        window.onload = () => {
            this.props.refreshItemState();
        }
    }

    componentDidMount() {
        if (this.props.getItem.message === 'initial') this.props.getItemList(this.props.businessid);
        if (this.props.addItemMessage.error !== '') {
            alert("Error " + this.props.addItemMessage.error);
            this.props.resetAddItemMessage();
        }
        if (this.props.addItemMessage.message === 'success') {
            alert("Item Added Successfully");
            this.props.resetAddItemMessageAndForm();
        }
        if (this.props.logoutMessage.error !== '') {
            alert("Error Logging Out. Please Try Again");
            this.props.resetLogoutMessage();
        }
    }

    handleInputChange = (event) => {
        const target = event.target;
        const value = target.value;
        this.setState({
            taxtype: value,
        });
    }

    handleSubmit = (values) => {
        this.props.addItem(values, this.props.businessid);
    }

    render() {
        if (!cookie.load('userservice')) return <Redirect to="/" />
        else {
            if ((this.props.getItem.loading === true) || (this.props.addItemMessage.loading === true) || (this.props.logoutMessage.loading === true)) {
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div className="mt-3 d-flex justify-content-center">
                            <button className="btn btn-danger">Loading... </button>
                        </div>
                    </div>
                );
            }
            else if (this.props.getItem.error !== '') {
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div className="mt-3 d-flex justify-content-center">
                            <button className="btn btn-danger">Error.. Please Refresh and Try Agin </button>
                        </div>
                    </div>
                );
            }
            else {
                let business = this.props.businessList.data.filter(b => b.businessid === this.props.businessid)[0];
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div style={{ minHeight: "93vh", display: "flex", flexDirection: "column" }}>
                            <div className="container d-flex justify-content-center pr-0 pl-0">
                                <div className="col-12 col-md-5">
                                    <div className="small pr-0 pl-0" >
                                        <h6 className="text-center pl-2 pr-2 pt-4" style={{ lineHeight: "1" }}><strong>{business.businessname}</strong></h6>
                                        <h6 className="text-center pl-2 pr-2 mb-4" style={{ lineHeight: "1" }}>{business.alias ? "(" + business.alias + ")" : ''}</h6>
                                        <h6 className="text-center pl-2 pr-2 mb-2"><strong><u>Items</u></strong></h6>
                                        <AddItem handleSubmit={this.handleSubmit} addItemMessage={this.props.addItemMessage} handleInputChange={this.handleInputChange} taxtype={this.state.taxtype} />
                                    </div>
                                </div>
                            </div>
                            <div className="container d-flex justify-content-center pr-0 pl-0 mt-4 pb-4" style={{ flex: "1" }}>
                                <div className="col-12 col-md-5">
                                    <div className="small pr-0 pl-0">
                                        <ItemList getItem={this.props.getItem} addItemMessage={this.props.addItemMessage} />
                                    </div>
                                </div>
                            </div>
                            <Footer />
                        </div>
                    </div>
                );
            }
        }
    }
}

export default Item;