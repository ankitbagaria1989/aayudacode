import React from 'react';

const MyErrorBoundary = function () {
    return (<div className="text-danger text-center">Network Error. Please Try Again</div>)
}

export default MyErrorBoundary;