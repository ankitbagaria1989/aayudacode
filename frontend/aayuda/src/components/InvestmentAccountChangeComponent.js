import React, { Component, useState } from 'react';
import cookie from 'react-cookie';
import { Redirect } from 'react-router-dom';
import { Button, Collapse } from 'reactstrap';
import { Form, Errors, Control } from 'react-redux-form';
import { Header } from './HeaderComponent';
import { Footer } from './Footer';

const AddInvestmentAccount = function ({ handleSubmit, businessid, business }) {
    return (
        <div className="col-12">
            <Form model="updateInvestmentDetail" onSubmit={((values) => handleSubmit(values, businessid))}>
                <div className="justify-content-center mt-4">
                    <div className="form-group col-12 mb-1">
                        {business.businesstype === 'partnership' ? <label htmlFor="investmentaccount" className="mb-0"><span className="muted-text">Partner Account Name<span className="text-danger">*</span></span></label> : ''}
                        {business.businesstype === 'ltd' ? <label htmlFor="investmentaccount" className="mb-0"><span className="muted-text">Share Holder Account Name<span className="text-danger">*</span></span></label> : ''}
                        <div className="">
                            <Control.text model=".investmentaccount" className="form-control form-control-sm pt-3 pb-3" id="investmentaccount" name="investmentaccount" />
                        </div>
                        {business.businesstype === 'partnership' ?
                            <div>
                                <label htmlFor="shareholding" className="mb-0"><span className="muted-text">Partner's Percentage Holding<span className="text-danger">*</span></span></label>
                                <div className="">
                                    <Control.text model=".shareholding" className="form-control form-control-sm pt-3 pb-3" id="shareholding" name="shareholding"/>
                                </div></div> : ''}
                        {business.businesstype === 'ltd' ?
                            <div>
                                <label htmlFor="shareholding" className="mb-0"><span className="muted-text">No of Shares Alloted<span className="text-danger">*</span></span></label>
                                <div className="">
                                    <Control.text model=".shareholding" className="form-control form-control-sm pt-3 pb-3" id="shareholding" name="shareholding"/>
                                </div></div> : ''}
                        <div className="mt-3"><button type="submit" className="btn btn-success btn-sm pl-4 pr-4" >Update</button></div>
                    </div>
                </div>
            </Form>
        </div>
    );
}

class InvestmentAccountChange extends Component {
    constructor(props) {
        super(props);
        this.state = {
            investmentaccount: this.props.getInvestmentAccount.data.filter((i) => {
                if (i.id === this.props.id) return i;
            })[0]
        };
        if ((this.state.investmentaccount !== undefined) && (this.props.updateInvestmentAccountMessage.loading === false)) this.props.setDefaultInvestmentAccountChange(this.state.investmentaccount);
    }
    componentDidMount() {
        if (this.props.updateInvestmentAccountMessage.error !== '') {
            alert("Error " + this.props.updateInvestmentAccountMessage.error);
            this.props.setDefaultInvestmentAccountChange(this.props.updateInvestmentAccountMessage.state);
            this.props.resetUpdateInvestmentAccountMessage();
        }
        if (this.props.updateInvestmentAccountMessage.message === 'success') {
            alert("Account Updated Successfully");
            this.props.resetUpdateInvestmentAccountMessage();
        }
        if (this.props.logoutMessage.error !== '') {
            alert("Error Logging Out. Please Try Again");
            this.props.resetLogoutMessage();
        }
    }
    handleSubmit = (values, businessid) => {
        if (values.investmentaccount === '') alert('Account Name Is Mandatory');
        else if (values.investmentaccount.length > 100) alert("Account Name Must Be Within 100 Characters");
        else if (values.shareholding === '') alert("Holding Data Is Mandatory");
        else if (isNaN(values.shareholding)) alert("Holding Data Must Be A Number");
        else this.props.updateInvestmentAccount(values, businessid, this.props.id);
    }
    render() {
        if (!cookie.load('userservice')) return <Redirect to="/" />
        else {
            if ((this.props.updateInvestmentAccountMessage.loading === true) || (this.props.logoutMessage.loading === true)) {
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div className="mt-3 d-flex justify-content-center">
                            <button className="btn btn-danger">Loading... </button>
                        </div>
                    </div>
                );
            }
            else {
                let business = this.props.businessList.data.filter(b => b.businessid === this.props.businessid)[0];
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div style={{ minHeight: "81vh", display: "flex", flexDirection: "column" }}>
                            <div className="container d-flex justify-content-center pr-0 pl-0">
                                <div className="col-12 col-md-5">
                                    <div className="small pr-0 pl-0">
                                        <h6 className="text-center pl-2 pr-2 pt-4" style={{ lineHeight: "1" }}><strong>{business.businessname}</strong></h6>
                                        <h6 className="text-center pl-2 pr-2 mb-4" style={{ lineHeight: "1" }}>{business.alias ? "(" + business.alias + ")" : ''}</h6>
                                        <h6 className="text-center pl-2 pr-2 mb-2"><strong><u>{business.businesstype === 'ltd' ? "SUpdate hare Holders Account" : ''}</u></strong></h6>
                                        <h6 className="text-center pl-2 pr-2 mb-2"><strong><u>{business.businesstype === 'partnership' ? "Update Partners Account" : ''}</u></strong></h6>
                                        <AddInvestmentAccount handleSubmit={this.handleSubmit} addInvestmentAccountMessage={this.props.addInvestmentAccountMessage} businessid={this.props.businessid} business={business} />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <Footer />
                    </div>
                );
            }
        }
    }
}
export default InvestmentAccountChange;