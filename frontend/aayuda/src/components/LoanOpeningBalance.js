import React, { Component } from 'react';
import cookie from 'react-cookie';
import { Redirect } from 'react-router-dom';
import { LocalForm, Control } from 'react-redux-form';
import { Header } from './HeaderComponent';
import { Footer } from './Footer';

class LoanOpeningBalance extends Component {
    constructor(props) {
        super(props);
        this.state = ({
            loanaccount: '',
            amount: 0,
            payreceiive: ''
        })
        window.onload = () => {
            this.props.refreshLoanAccountState();
            this.props.resetLoanOpeningBalance();
        }
    }
    handleSubmit = () => {
        if (this.state.loanaccount === '') alert("Loan Account Not Selected");
        else {
            if ((this.state.amount === '') || (this.state.amount < 0) || (isNaN(parseInt(this.state.amount)))) alert(" Amount Must Be Greater Than Equal To 0 ");
            else if (this.state.payreceive === '') alert(" Please Select Pay/Receive");
            else this.props.updateLoanOpeningBalance(this.state, this.props.businessid, this.props.fyid);
        }
    }
    componentDidMount() {
        if (this.props.updateLoanOpeningMessage.error !== '') {
            alert(this.props.updateLoanOpeningMessage);
            this.setState({
                ...this.props.updateLoanOpeningMessage.state
            })
            this.props.refreshUpdateLoanOpeningBalance();
        }
        if (this.props.updateLoanOpeningMessage.message === 'success') {
            alert("Data Updated Successfully");
            this.setState({
                ...this.props.updateLoanOpeningMessage.state
            })
            this.props.refreshUpdateLoanOpeningBalance();
        }
        if (this.props.getLoanOpeningBalance.message === 'initial') this.props.getLoanOpeningBalanceList(this.props.businessid, this.props.fyid);
        if (this.props.logoutMessage.error !== '') {
            alert("Error Logging Out. Please Try Again");
            this.props.resetLogoutMessage();
        }
        if (this.props.getLoanAccount.message === 'initial') this.props.getLoanAccountList(this.props.businessid);
        if (this.props.getLoanOpeningBalance.message === 'success') {
            if (this.props.getLoanOpeningBalance.data.length) {
                let loan = this.props.getLoanOpeningBalance.data.filter(d => d.id === this.state.loanaccount)[0];
                this.setState({
                    ...this.state,
                    amount: loan ? loan.amount : 0
                })
            }
        }
    }

    handleInputChange = (event) => {
        let target = event.target;
        let name = target.name;
        let value = target.value;
        if (name === 'loanaccount') {
            this.setState({
                loanaccount: value,
                amount: '',
                payreceive: ''
            })
        }
        else if (name === 'payreceive') {
            let loan = this.props.getLoanOpeningBalance.data.filter(d => d.loanaccount === this.state.loanaccount && d.payreceive === value)[0];
            let amount = loan ? loan.amount : 0
            this.setState({
                ...this.state,
                payreceive : value,
                amount: amount,
            })
        }
        else {
            this.setState({
                [name]: value
            })
        }
    }
    render() {
        if (!cookie.load('userservice')) return <Redirect to="/" />;
        else {
            if ((this.props.updateLoanOpeningMessage.loading === true) || (this.props.getLoanOpeningBalance.loading === true) || (this.props.logoutMessage.loading === true) || (this.props.getLoanAccount.loading === true)) {
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div className="mt-3 d-flex justify-content-center">
                            <button className="btn btn-danger">Loading... </button>
                        </div>
                    </div>
                );
            }
            else if ((this.props.getLoanOpeningBalance.error !== '') || (this.props.getLoanAccount.error !== '')) {
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div className="mt-3 d-flex justify-content-center">
                            <button className="btn btn-danger">Error. Please Refresh and Try Again</button>
                        </div>
                    </div>
                );
            }
            else {
                let business = this.props.businessList.data.filter(b => b.businessid === this.props.businessid)[0];
                let fy = business.financial.filter(f => f.fyid === this.props.fyid)[0].financialyear;
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div style={{ minHeight: "93vh", display: "flex", flexDirection: "column" }}>
                            <div className="container" style={{ flex: "1" }}>
                                <h6 className="text-center pl-2 pr-2 pt-4" style={{ lineHeight: "1" }}><strong>{business.businessname} {fy}</strong></h6>
                                <h6 className="text-center pl-2 pr-2 mb-4" style={{ lineHeight: "1" }}>{business.alias ? "(" + business.alias + ")" : ''}</h6>
                                <h6 className="text-center pl-2 pr-2 col-12"><strong><u>Loan Account Opening Balance</u></strong></h6>
                                <div className="row">
                                    <LocalForm className="small col-12" onSubmit={this.handleSubmit}>
                                        <div className="container mt-4 d-flex justify-content-center">
                                            <div class="row col-12 col-md-4">
                                                <div className="form-group mb-1 col-12">
                                                    <label htmlFor="loanaccount" className="mb-0 muted-text"><strong>Loan Account Name:<span className="text-danger">*</span></strong></label>
                                                    <Control.select model=".loanaccount" className="form-control form-control-sm" id="loanaccount" name="loanaccount" onChange={this.handleInputChange} value={this.state.loanaccount}>
                                                        <option value="">Select Loan Account</option>
                                                        {this.props.getLoanAccount.data.map(b => <option value={b.id}>{b.loanaccount}</option>)}
                                                    </Control.select>
                                                </div>
                                                {this.state.loanaccount !== '' ? <div className="form-group mb-1 col-12">
                                                    <label htmlFor="payreceive" className="mb-0 muted-text"><strong>Pay/receive</strong><span className="text-danger">*</span></label>
                                                    <Control.select model=".payreceive" className="form-control form-control-sm" id="payreceive" name="payreceive" onChange={this.handleInputChange} value={this.state.payreceive}>
                                                        <option value="">Loan Pay/Receive</option>
                                                        <option value="pay">Pay</option>
                                                        <option value="receive">Receive</option>
                                                    </Control.select>
                                                </div> : ''}
                                                {( this.state.loanaccount !== '' && this.state.payreceive !== '' ) ? <div className="form-group mb-1 col-12">
                                                    <label htmlFor="amount" className="mb-0 muted-text"><strong>Opening Balance Amount</strong><span className="text-danger">*</span></label>
                                                    <Control.text model=".amount" className="form-control form-control-sm" id="amount" name="amount" onChange={this.handleInputChange} value={this.state.amount} />
                                                </div> : ''}
                                                {(this.state.loanaccount !== ''  && this.state.payreceive !== '') ? <div className="form-group mb-1 col-12">
                                                    <button type="submit" className="btn btn-success mt-1 btn-sm small" >Update</button>
                                                </div> : ''}
                                            </div>
                                        </div>
                                    </LocalForm>
                                </div>
                            </div>
                            <Footer />
                        </div>
                    </div>
                )
            }
        }
    }
}

export default LoanOpeningBalance;