import React, { Component } from 'react';
import cookie from 'react-cookie';
import { Redirect } from 'react-router-dom';
import { Form, Errors, Control } from 'react-redux-form';
import { Header } from './HeaderComponent';
import { Footer } from './Footer';

const required = (val) => val && val.length;
const maxLength = (len) => val => !(val) || (val.length <= len);
const minLength = (len) => val => (val) && (val.length >= len);
const validEmail = (val) => /^[A-Za-z0-9.+-_%$#*]+@[A-Za-z0-9.+-_%$#*]+\.[A-Za-z]{2,4}$/i.test(val);

class Register extends Component {
    handleSubmit = (values) => {
        this.props.postRegisterUser(values.username, values.email, values.password);

    }
    componentDidMount() {
        if (this.props.registerMessage.error !== '') {
            alert(this.props.registerMessage.error);
            this.props.resetRegisterForm();
        }
        if (this.props.registerMessage.message === 'success') {
            alert("Account Created Successfully");
            this.props.resetRegisterForm();
        }
    }
    render() {
        if (cookie.load('userservice')) return <Redirect to="/" />
        else {
            if (this.props.registerMessage.loading === true) {
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div className="mt-3 d-flex justify-content-center">
                            <button className="btn btn-danger">Loading... </button>
                        </div>
                    </div>
                );
            }
            else {
                return (
                    <div className="jumbotron">
                        <div className="row">
                            <div className="col-12 d-flex justify-content-center">
                                <span className="h1"><strong className="text-uppercase">Create New Account</strong></span>
                            </div>
                        </div>
                        <Form model="register" className="col-md-6" onSubmit={((values) => this.handleSubmit(values))}>
                            <div className="form-group row">
                                <label htmlFor="username" className="col-md-2 text-right">UserName</label>
                                <div className="col-md-10">
                                    <Control.text model=".username" className="form-control" id="username" name="username" placeholder="User Name" validators={{ required }} />
                                    <Errors className="text-danger" model=".username" show="touched" messages={{
                                        required: "Email is mandatory to Register"
                                    }} />
                                </div>
                            </div>
                            <div className="form-group row">
                                <label htmlFor="email" className="col-md-2 text-right">Email</label>
                                <div className="col-md-10">
                                    <Control.text model=".email" className="form-control" id="email" name="email" placeholder="Email" validators={{ required, validEmail }} />
                                    <Errors className="text-danger" model=".email" show="touched" messages={{
                                        required: "Email is mandatory to Register",
                                        validEmail: "Invalid Email Format"
                                    }} />
                                </div>
                            </div>
                            <div className="form-group row">
                                <label htmlFor="password" className="col-md-2 text-right">Password</label>
                                <div className="col-md-10">
                                    <Control type="password" model=".password" className="form-control" id="password" name="password" placeholder="Password" validators={{ required, minLength: minLength(6), maxLength: maxLength(15) }} />
                                    <Errors className="text-danger" model=".password" show="touched" messages={{
                                        required: "Password Mandatory ",
                                        minLength: "Minimum Length must be greater than 6 characters ",
                                        maxLength: "Maximum Lenth must be 15 characters or less "
                                    }} />
                                    <div className="mt-3"><button type="submit" className="btn btn-primary" >Register</button></div>
                                </div>
                            </div>
                        </Form>
                    </div>
                );
            }
        }
    }
}

export default Register; 