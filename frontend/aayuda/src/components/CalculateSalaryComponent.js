import React, { Component } from 'react';
import cookie from 'react-cookie';
import { Redirect } from 'react-router-dom';
import { Header } from './HeaderComponent';
import { style } from '../sylesheet/stylesheet';
import { Footer } from './Footer';

const GetEmployeeList = function ({ getEmployee, year, month }) {
    let returnrows = [];
    let i = 1;
    getEmployee.data.forEach(e => {
        if ((((new Date(e.doj.split(" ")[0] + " 01 " + e.doj.split(" ")[2]) <= new Date(month + " 01 " + year)) && (e.active == 1)) || ((new Date(e.doj.split(" ")[0] + " 01 " + e.doj.split(" ")[2]) <= new Date(month + " 01 " + year)) && (e.active == 0) && (new Date(e.dot.split(" ")[0] + " 01 " + e.dot.split(" ")[2]) > new Date(month + " 01 " + year))))) {
            if (i % 2 !== 0) returnrows.push(<div className="pl-1 pr-0 row" style={style.height1, { paddingTop: "10px", paddingBottom: "10px", backgroundColor: "rgb(181, 253, 233)", whiteSpace: "nowrap", overflow: "hidden" }}>{e.employeename}</div>)
            else returnrows.push(<div className="pl-1 pr-0 row" style={style.height1, { paddingTop: "10px", paddingBottom: "10px", whiteSpace: "nowrap", overflow: "hidden" }}>{e.employeename}</div>)
            i++;
        }
    })
    return returnrows;
}

const Report = function ({ getEmployee, count, month, year, state, handleInputChange }) {
    let returnrows = [];
    let y = 1;
    getEmployee.data.forEach(e => {
        if (state[e.employeeid]) {
            if ((((new Date(e.doj.split(" ")[0] + " 01 " + e.doj.split(" ")[2]) <= new Date(month + " 01 " + year)) && (e.active == 1)) || ((new Date(e.doj.split(" ")[0] + " 01 " + e.doj.split(" ")[2]) <= new Date(month + " 01 " + year)) && (e.active == 0) && (new Date(e.dot.split(" ")[0] + " 01 " + e.dot.split(" ")[2]) > new Date(month + " 01 " + year))))) {
                let salary = 0;
                salary = JSON.parse(e.salary);
                let keys = Object.keys(salary);
                let x = keys.length - 1;
                while (x >= 0) {
                    let salarydate = new Date(JSON.parse(salary[x]).date.split(" ")[0] + " 01 " + JSON.parse(salary[x]).date.split(" ")[1]);
                    let date = new Date(month + " 01 " + year);
                    if (salarydate <= date) {
                        salary = JSON.parse(salary[x]).salary;
                        break;
                    }
                    x--;
                }
                if (y % 2 !== 0) returnrows.push(
                    <div className="pr-0 pl-1" style={style.cardReport, { paddingTop: "7px", paddingBottom: "7px", backgroundColor: "rgb(181, 253, 233)" }}>
                        <span className="small" style={{ width: "100px", display: "inline-block" }}><strong>{count[e.employeeid] ? "P-" + count[e.employeeid].p + " A-" + count[e.employeeid].a + " OT-" + count[e.employeeid].ot : ''}</strong></span>&nbsp;&nbsp;
                        <span className="small" style={{ width: "100px", display: "inline-block" }}><strong>{"Salary : " + salary}</strong></span>&nbsp;&nbsp;
                        <span className="form-group small">
                            <span className="mb-0 muted-text col-8 p-0"><strong>Computable Salary</strong></span>
                            <span className="col-4 text-right">
                                <input name={e.employeeid + "...salary"} type="text" value={state[e.employeeid].salary} size="5" onChange={handleInputChange} />
                            </span>
                        </span>
                        <span className="form-group small">
                            <span className="mb-0 muted-text col-8 p-0"><strong>Adv. Due</strong></span>
                            <span className="col-4 text-right">
                                <input name={e.employeeid + "...advancedue"} type="text" value={state[e.employeeid].advancedue} size="5" onChange={handleInputChange} />
                            </span>
                        </span>
                        <span className="form-group small">
                            <span className="mb-0 muted-text col-8 p-0"><strong>Paid</strong></span>
                            <span className="col-4 text-right">
                                <input name={e.employeeid + "...paid"} type="text" value={state[e.employeeid].paid} size="5" onChange={handleInputChange} />
                            </span>
                        </span>
                        <span className="form-group small">
                            <span className="mb-0 muted-text col-8 p-0"><strong>Adv</strong></span>
                            <span className="col-4 text-right">
                                <input name={e.employeeid + "...advance"} type="text" value={state[e.employeeid].advance} size="5" onChange={handleInputChange} />
                            </span>
                        </span>
                    </div>
                )
                else returnrows.push(
                    <div className="pr-0 pl-1" style={style.cardReport, { paddingTop: "7px", paddingBottom: "7px" }}>
                        <span className="small" style={{ width: "100px", display: "inline-block" }}><strong>{count[e.employeeid] ? "P-" + count[e.employeeid].p + " A-" + count[e.employeeid].a + " OT-" + count[e.employeeid].ot : ''}</strong></span>&nbsp;&nbsp;
                        <span className="small" style={{ width: "100px", display: "inline-block" }}><strong>{"Salary : " + salary}</strong></span>&nbsp;&nbsp;
                        <span className="form-group small">
                            <span className="mb-0 muted-text col-8 p-0"><strong>Computable Salary</strong></span>
                            <span className="col-4 text-right">
                                <input name={e.employeeid + "...salary"} type="text" value={state[e.employeeid].salary} size="5" onChange={handleInputChange} />
                            </span>
                        </span>
                        <span className="form-group small">
                            <span className="mb-0 muted-text col-8 p-0"><strong>Adv. Due</strong></span>
                            <span className="col-4 text-right">
                                <input name={e.employeeid + "...advancedue"} type="text" value={state[e.employeeid].advancedue} size="5" onChange={handleInputChange} />
                            </span>
                        </span>
                        <span className="form-group small">
                            <span className="mb-0 muted-text col-8 p-0"><strong>Paid</strong></span>
                            <span className="col-4 text-right">
                                <input name={e.employeeid + "...paid"} type="text" value={state[e.employeeid].paid} size="5" onChange={handleInputChange} />
                            </span>
                        </span>
                        <span className="form-group small">
                            <span className="mb-0 muted-text col-8 p-0"><strong>Adv</strong></span>
                            <span className="col-4 text-right">
                                <input name={e.employeeid + "...advance"} type="text" value={state[e.employeeid].advance} size="5" onChange={handleInputChange} />
                            </span>
                        </span>
                    </div>)
                y++;
            }
        }
    })
    return returnrows;
}

const IterateOverDate = function ({ getEmployee, year, month, days, getMonthAttendance, count, state, handleInputChange }) {
    let returnrows = [];
    let attendanceobject = {};
    let y = 0;
    const getDateList = (attendanceobject) => {
        getEmployee.data.forEach(e => {
            if (new Date(e.doj.split(" ")[0] + " 01 " + e.doj.split(" ")[2]) <= new Date(month + " 01 " + year)) {
                let x = (count[e.employeeid] && attendanceobject[e.employeeid]) ? (attendanceobject[e.employeeid].present ? count[e.employeeid].p++ : count[e.employeeid].a++) : '';
                x = (count[e.employeeid] && attendanceobject[e.employeeid]) ? (attendanceobject[e.employeeid].ot > 0 ? count[e.employeeid].ot = parseInt(count[e.employeeid].ot) + parseInt(attendanceobject[e.employeeid].ot) : '') : '';
            }
        })
    }
    for (let i = 1; i <= days; i++) {
        attendanceobject = {};
        if (getMonthAttendance.data[y] ? parseInt(getMonthAttendance.data[y].date.split(" ")[1]) == i : false) {
            attendanceobject = JSON.parse(getMonthAttendance.data[y].attendanceobject);
            y++;
            getDateList(attendanceobject)
        }
    }
    returnrows.push(<div style={{ display: "inline-block", width: "820px" }}>
        <Report getEmployee={getEmployee} count={count} month={month} year={year} state={state} handleInputChange={handleInputChange} />
    </div>)
    return returnrows;
}

const AttendanceChart = function ({ getEmployee, year, month, days, getMonthAttendance, count, state, handleInputChange, handleSubmit }) {
    return (
        <div>
            <div className="row">
                <div className="col-4 col-md-2">
                    <GetEmployeeList getEmployee={getEmployee} year={year} month={month} />
                </div>
                <div className="col-8 col-md-10 pl-1 pr-0">
                    <div className="mb-3" style={style.box}>
                        <form>
                            <IterateOverDate getEmployee={getEmployee} year={year} month={month} days={days} getMonthAttendance={getMonthAttendance} count={count} state={state} handleInputChange={handleInputChange} />
                        </form>
                    </div>
                </div>
            </div>
            <div className="row">
                <div className="col-4 col-md-2"></div>
                <div className="col-8 col-md-10">
                    <div className="mb-5"><button className="btn btn-success btn-sm pl-4 pr-4 pt-2 pb-2" onClick={() => this.handleSubmit}>Update Salary</button></div>
                </div>
            </div>
        </div>
    )
}

class CalculateSalary extends Component {
    constructor(props) {
        super(props);
        window.onload = () => {
            this.props.refreshEmployeeState();
        }
        this.state = {};
    }
    handleInputChange = (event) => {
        let target = event.target;
        let name = target.name;
        let value = target.value;
        let employee = name.split("...");
        this.setState({
            ...this.state,
            [employee[0]]: {
                ...this.state[employee[0]],
                [employee[1]]: value
            }
        })
    }
    handleSubmit = () => {
        this.props.updateSalary(this.props.fyid, this.props.month, this.state);
    }
    componentDidMount() {
        window.onload = () => {
            this.props.refreshEmployeeState();
        }
        if ((this.props.getEmployee.message == 'initial') && (this.props.getEmployee.loading == false)) this.props.getEmployeeList(this.props.businessid);
        this.props.getEmployee.data.forEach(e => {
            this.setState({
                ...this.state,
                [e.employeeid]: {
                    salary: 0,
                    paid: 0,
                    advance: 0,
                    advancedue: 0
                }
            })
        })
        if (this.props.logoutMessage.error !== '') {
            alert("Error Logging Out. Please Try Again");
            this.props.resetLogoutMessage();
        }
        if (this.props.getMonthAttendance.message === 'initial') {
            let business = this.props.businessList.data.filter(b => b.businessid === this.props.businessid)[0];
            let fy = business.financial.filter(f => f.fyid === this.props.fyid)[0].financialyear;
            let year = fy.split("-")[0];
            let days = 30;
            switch (this.props.month) {
                case 'apr': days = 30; year = fy.split("-")[0];
                    break;
                case 'may': days = 31; year = fy.split("-")[0];
                    break;
                case 'jun': days = 30; year = fy.split("-")[0];
                    break;
                case 'jul': days = 31; year = fy.split("-")[0];
                    break;
                case 'aug': days = 31; year = fy.split("-")[0];
                    break;
                case 'sep': days = 30; year = fy.split("-")[0];
                    break;
                case 'oct': days = 31; year = fy.split("-")[0];
                    break;
                case 'nov': days = 30; year = fy.split("-")[0];
                    break;
                case 'dec': days = 31; year = fy.split("-")[0];
                    break;
                case 'jan': days = 31; year = fy.split("-")[1];
                    break;
                case 'feb':
                    year = fy.split("-")[1];
                    let result = fy.split("-")[1] % 100 ? fy.split("-")[1] % 400 : fy.split("-")[1] % 4;
                    if (result) days = 29;
                    else days = 28;
                    break;
                case 'mar': days = 31; year = fy.split("-")[1];
                    break;
            }
            this.props.getMonthAttendanceList(this.props.fyid, this.props.month, year);
        }
        if (this.props.updateSalaryMessage.message === 'success') {
            alert("Salary Updated Successfully");
            this.props.resetUpdateSalaryMessage();
        }
        if (this.props.getSalary.message === 'initial') this.props.getSalaryList(this.props.fyid, this.props.month);

        else if (this.props.updateSalaryMessage.error !== '') {
            alert("Error Updating Salary. Please Try Again.");
            this.setState({
                ...this.props.updateSalaryMessage.state
            })
            this.props.resetUpdateSalaryMessage();
        }
        if (this.props.getSalary.data.length > 0) {
            let keys = Object.keys(this.props.getSalary.data[0]);
            keys.forEach(s => {
                this.setState({
                    ...this.state,
                    [s]: {
                        salary: this.props.getSalary.data[0][s].salary,
                        paid: this.props.getSalary.data[0][s].paid,
                        advance: this.props.getSalary.data[0][s].advance,
                        advancedue: this.props.getSalary.data[0][s].advancedue
                    }
                })
            })
        }
    }
    render() {
        if (!cookie.load('userservice')) return <Redirect to="/" />
        else {
            let count = {};
            this.props.getEmployee.data.forEach(e => {
                count = {
                    ...count,
                    [e.employeeid]: {
                        a: 0,
                        p: 0,
                        ot: 0
                    }
                }
            });
            let temp = undefined;
            let business = this.props.businessList.data.filter(b => b.businessid === this.props.businessid)[0];
            let fy = business.financial.filter(f => f.fyid === this.props.fyid)[0].financialyear;
            let year = fy.split("-")[0];
            let days = 30;
            switch (this.props.month) {
                case 'apr': days = 30; year = fy.split("-")[0];
                    break;
                case 'may': days = 31; year = fy.split("-")[0];
                    break;
                case 'jun': days = 30; year = fy.split("-")[0];
                    break;
                case 'jul': days = 31; year = fy.split("-")[0];
                    break;
                case 'aug': days = 31; year = fy.split("-")[0];
                    break;
                case 'sep': days = 30; year = fy.split("-")[0];
                    break;
                case 'oct': days = 31; year = fy.split("-")[0];
                    break;
                case 'nov': days = 30; year = fy.split("-")[0];
                    break;
                case 'dec': days = 31; year = fy.split("-")[0];
                    break;
                case 'jan': days = 31; year = fy.split("-")[1];
                    break;
                case 'feb':
                    year = fy.split("-")[1];
                    let result = fy.split("-")[1] % 100 ? fy.split("-")[1] % 400 : fy.split("-")[1] % 4;
                    if (result) days = 29;
                    else days = 28;
                    break;
                case 'mar': days = 31; year = fy.split("-")[1];
                    break;
            }
            temp = {
                year: year,
                days: days,
                fy: fy,
                business: business
            }
            if ((this.props.logoutMessage.loading === true) || (this.props.getEmployee.loading === true) || (this.props.getMonthAttendance.loading === true) || (this.props.getSalary.loading === true) || (this.props.updateSalaryMessage.loading === true)) {
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div className="mt-3 d-flex justify-content-center">
                            <button className="btn btn-danger">Loading... </button>
                        </div>
                    </div>
                );
            }
            else if ((this.props.getEmployee.error !== "") || (this.props.getMonthAttendance.error !== '') || (this.props.getSalary.error !== '')) {
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div className="mt-3 d-flex justify-content-center">
                            <button className="btn btn-danger">Error. Please Refresh and Try Again</button>
                        </div>
                    </div>
                );
            }
            else {
                let present = false;
                this.props.getEmployee.data.forEach(e => {
                    if (new Date(e.doj.split(" ")[0] + " 01 " + e.doj.split(" ")[2]) <= new Date(this.props.month + " 01 " + temp.year)) present = true;
                })
                if (present) {
                    return (
                        <div>
                            <Header logout={this.props.logout} />
                            <div style={{ minHeight: "93vh", display: "flex", flexDirection: "column" }}>
                                <div className="row" style={{ flex: "1" }}>
                                    <div className="container">
                                        <h6 className="text-center pl-2 pr-2 pt-4" style={{ lineHeight: "1" }}><strong>{temp.business.businessname} - {temp.fy}</strong></h6>
                                        <h6 className="text-center pl-2 pr-2 mb-4" style={{ lineHeight: "1" }}>{temp.business.alias ? "(" + temp.business.alias + ")" : ''}</h6>
                                        <h6 className="text-center mb-4"><u>Salary Sheet - {this.props.month.toUpperCase()} {temp.year}</u></h6>
                                        <div className="col-12 d-flex justify-content-center">
                                            <div className="col-12">
                                                <AttendanceChart getEmployee={this.props.getEmployee} year={temp.year} month={this.props.month} days={temp.days} getMonthAttendance={this.props.getMonthAttendance} count={count} state={this.state} handleInputChange={this.handleInputChange} handleSubmit={this.handleSubmit} />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <Footer />
                            </div>
                        </div>
                    );
                }
                else {
                    return (
                        <div>
                            <Header logout={this.props.logout} />
                            <div style={{ minHeight: "93vh", display: "flex", flexDirection: "column" }}>
                                <div className="row" style={{ flex: "1" }}>
                                    <div className="container">
                                        <h6 className="text-center pl-2 pr-2 pt-4" style={{ lineHeight: "1" }}><strong>{temp.business.businessname} - {temp.fy}</strong></h6>
                                        <h6 className="text-center pl-2 pr-2 mb-4" style={{ lineHeight: "1" }}>{temp.business.alias ? "(" + temp.business.alias + ")" : ''}</h6>
                                        <h6 className="text-center mb-4"><u>Attendance Report - {this.props.month.toUpperCase()} {temp.year}</u></h6>
                                        <div className="col-12 d-flex justify-content-center">
                                            <div className="col-12">
                                                <div className="text-center text-muted">No Employees having DOJ on or before {this.props.month.toUpperCase()} - {temp.year}</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <Footer />
                            </div>
                        </div>
                    )
                }
            }
        }
    }
}

export default CalculateSalary;