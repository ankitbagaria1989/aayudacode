import React, { Component } from 'react';
import { Form, Errors, Control } from 'react-redux-form';
import cookie from 'react-cookie';
import { Redirect } from 'react-router-dom';
import { Header } from './HeaderComponent';
import { Footer } from './Footer';

const required = (val) => val && val.length;
const maxLength = (len) => val => !(val) || (val.length <= len);
const isNumber = (val) => !isNaN(parseInt(val));

const ErrorComponent = function ({ children }) {
    return (
        <div className="pb-1 small">{children}</div>
    );
}

const FormElement = function ({ handleSubmit }) {
    return (
        <Form className="small" model="bankDetailChange" onSubmit={((values) => handleSubmit(values))}>
            <div className="justify-content-center mt-4">
                <div className="form-group col-12 mb-1">
                    <label htmlFor="bankname" className="mb-0"><span className="muted-text">Bank Name<span className="text-danger">*</span></span></label>
                    <div className="">
                        <Control.text model=".bankname" className="form-control form-control-sm pt-3 pb-3" id="bankname" name="bankname" placeholder="Bank Name" validators={{ required, maxLength: maxLength(100) }} />
                        <Errors className="text-danger" model=".bankname" show="touched" messages={{
                            required: "Bank Name is Mandatory to Register",
                            maxLength: "length must be within 100 characters"
                        }}
                            component={ErrorComponent}
                        />
                    </div>
                </div>
                <div className="form-group col-12 mb-1">
                    <label htmlFor="accountnumber" className="mb-0 muted-text">Account Number<span className="text-danger">*</span></label>
                    <div className="">
                        <Control.text model=".accountnumber" className="form-control form-control-sm pt-3 pb-3" id="accountnumber" name="accountnumber" placeholder="Account Number" validators={{ required, isNumber, maxLength: maxLength(20) }} />
                        <Errors className="text-danger" model=".accountnumber" show="touched" messages={{
                            required: "Account Number is mandatory",
                            isnumber: "Only Number Allowed",
                            maxLength: "length must be within 20 digits"
                        }}
                            component={ErrorComponent}
                        />
                    </div>
                    <div className="mt-3"><button type="submit" className="btn btn-success btn-sm pl-4 pr-4" >Update</button></div>
                </div>
            </div>
        </Form>
    );
}

class BankDetailChange extends Component {
    constructor(props) {
        super(props);
        this.state = {
            BankDetail: this.props.getBankDetail.data.filter((bk) => {
                if ((bk.businessid === this.props.businessid) && (bk.bankdetailid === this.props.bankdetailid)) return bk;
            })[0]
        };
        if ((this.state.BankDetail !== undefined) && (this.props.updateBankDetailMessage.error === '') && (this.props.updateBankDetailMessage.loading === false)) this.props.setDefaultBankDetailChange(this.state.BankDetail);
    }

    componentDidMount() {
        if (this.props.updateBankDetailMessage.error !== '') {
            alert(this.props.updateBankDetailMessage.error + 'Please try again');
            this.props.resetBankDetailChange();
        }
        if (this.props.updateBankDetailMessage.message === 'success') {
            alert('Bank Detail Updated Successfully');
            this.props.resetBankDetailChange();
        }
        if (this.props.logoutMessage.error !== '') {
            alert("Error Logging Out. Please Try Again");
            this.props.resetLogoutMessage();
        }
    }


    handleSubmit = (values) => {
        this.props.updateBankDetail(this.state.BankDetail.businessid, this.state.BankDetail.bankdetailid, values);
    }

    render() {
        if (!cookie.load('userservice')) return <Redirect to="/" />;
        else {
            if ((this.props.logoutMessage.loading === true) || (this.props.updateBankDetailMessage.loading === true)) {
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div className="mt-3 d-flex justify-content-center">
                            <button className="btn btn-danger">Loading... </button>
                        </div>
                    </div>
                );
            }
            else {
                let business = this.props.businessList.data.filter(b => b.businessid === this.props.businessid)[0];
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div style={{ minHeight: "93vh", display: "flex", flexDirection: "column" }}>
                            <div className="container d-flex justify-content-center pr-0 pl-0" style={{ flex: "1" }}>
                                <div className="col-12 col-md-5 pb-4">
                                    <div className="pr-0 pl-0">
                                    <h6 className="text-center pl-2 pr-2 pt-4" style={{ lineHeight: "1" }}><strong>{business.businessname}</strong></h6>
                                        <h6 className="text-center pl-2 pr-2 mb-4" style={{ lineHeight: "1" }}>{business.alias ? "(" + business.alias + ")" : ''}</h6>
                                        <h6 className="mt-2 text-center">
                                            <strong><u>Update Bank Detail</u></strong>
                                        </h6>
                                        <FormElement handleSubmit={this.handleSubmit} />
                                    </div>
                                </div>
                            </div>
                            <Footer />
                        </div>
                    </div>
                );
            }
        }
    };
}

export default BankDetailChange;