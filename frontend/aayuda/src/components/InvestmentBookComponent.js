import React, { Component } from 'react';
import cookie from 'react-cookie';
import { Redirect } from 'react-router-dom';
import { Header } from './HeaderComponent';
import { LocalForm, Control } from 'react-redux-form';
import { Footer } from './Footer';
import { sortData } from '../redux/sortArrayofObjects';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faFilePdf } from '@fortawesome/free-solid-svg-icons';
import { Page, Text, View, Document, pdf } from '@react-pdf/renderer';
import { pdfstyles } from '../sylesheet/stylesheet';
import { saveAs } from 'file-saver';

const Option = function ({ data }) {
    let arr = [];
    for (let i = -1; i < data.length; i++) {
        if (i === -1) arr.push(<option value=''>Select Capital Account:</option>);
        else arr.push(<option value={data[i].id + "..." + data[i].investmentaccount}>{data[i].investmentaccount}</option>);
    }
    return arr;
}
const DataDisplayPartner = function ({ getInvestment, deleteEntry, businessid, fyid, state, getInvestmentOpeningBalance, getIncomeOS, getExpense, getPayment, profit, type, getInvestmentAccount }) {
    let investmentdata = getInvestment.data;
    if (type === 'partner') investmentdata = getInvestment.data.filter(d => d.investmentaccount === state.headSelection);
    let incomedata = [], expensedata = [], paymentdata = [], holding = 1;
    if (type === 'partner') {
        incomedata = getIncomeOS.data ? getIncomeOS.data.filter(d => d.customerid === state.headSelection.split("...")[0]) : [];
        expensedata = getExpense.data ? getExpense.data.filter(d => d.vendorid === state.headSelection.split("...")[0]) : [];
        paymentdata = getPayment.data ? getPayment.data.filter(d => d.vendorid === state.headSelection.split("...")[0] || d.customerid === state.headSelection.split("...")[0]) : [];
        holding = (getInvestmentAccount.data.filter(d => d.id === state.headSelection.split("...")[0])[0].shareholding) / 100;
    }
    let data = investmentdata;
    data = data.concat(incomedata);
    data = data.concat(expensedata);
    data = data.concat(paymentdata);
    data = data.sort(sortData);
    let datarow = [];
    let openingBalance = getInvestmentOpeningBalance.data;
    if ((state.headSelection !== '') && (type === 'partner')) {
        openingBalance = openingBalance.filter(d => d.investmentaccount === state.headSelection)
    }
    let i = 2, debit = 0, credit = 0;
    if (parseFloat(profit) >= 0) credit = credit + parseFloat(profit) * holding;
    else debit = debit + (parseFloat(profit) * -1) * holding;
    datarow.push(<div className="d-flex small mt-2 p-1 d-md-none d-lg-none">
        <div className="col-5 col-md-5 p-1">
            <div className="col-12 col-md-6 p-0 "></div>
        </div>
        <div className="col-3 col-md-6 p-0 pt-1 text-right"><strong>Debit</strong></div>
        <div className="col-3 col-md-1 p-0 pt-1 text-right"><strong>Credit</strong></div>
    </div>);
    datarow.push(
        <div className="container small mt-2 d-none d-md-block p-0">
            <div className="d-flex">
                <div className="col-2 p-0"><strong>Date</strong></div>
                <div className="col-4 p-0"><strong>Particulars</strong></div>
                <div className="col-2 text-right p-0"><strong>Debit</strong></div>
                <div className="col-3 text-right p-0"><strong>Credit</strong></div>
                <div className="col-1"></div>
            </div>
        </div>
    );
    openingBalance.forEach(data => {
        datarow.push(<div className="d-flex small p-1 d-md-none d-lg-none" style={i % 2 === 0 ? { backgroundColor: "rgb(181, 253, 233)" } : {}}>
            <div className="col-5 col-md-5 p-1">
                <div className="col-12 col-md-6 p-0 ">
                    <div><strong>{type === 'partner' && data.amounttype === 'payable' ? 'Opening Balance ' : 'Opening Balance'}</strong></div>
                    <div></div>
                </div>
            </div>
            <div className="col-3 col-md-6 p-0 pt-1 text-right">{type === 'partner' && data.amounttype === 'payable' ? parseFloat(data.amount).toFixed(2) : ''}</div>
            <div className="col-3 col-md-1 p-0 pt-1 text-right">{type === 'propritor' ? parseFloat(data.amount).toFixed(2) : ''} {type === 'partner' && data.amounttype === 'deposited' ? parseFloat(data.amount).toFixed(2) : ''}</div>
        </div>);
        datarow.push(
            <div className="container small d-none d-md-block p-1" style={i % 2 === 0 ? { backgroundColor: "rgb(181, 253, 233)" } : {}}>
                <div className="d-flex">
                    <div className="col-2 p-0"></div>
                    <div className="col-4 p-0">
                        <div><strong>{type === 'partner' && data.amounttype === 'payable' ? 'Opening Balance ' : 'Opening Balance'}</strong></div>
                    </div>
                    <div className="col-2 text-right p-0">{type === 'partner' && data.amounttype === 'payable' ? parseFloat(data.amount).toFixed(2) : ''}</div>
                    <div className="col-3 text-right p-0">{type === 'propritor' ? parseFloat(data.amount).toFixed(2) : ''} {type === 'partner' && data.amounttype === 'deposited' ? parseFloat(data.amount).toFixed(2) : ''}</div>
                    <div className="col-1"></div>
                </div>
            </div>
        );
        if ((type === 'partner') && (data.amounttype === 'payable')) debit = debit + parseFloat(data.amount);
        else credit = credit + parseFloat(data.amount);
        i++;
    })
    data.forEach(data => {
        if (data.type === 'investment') {
            if (data.activity === 'drawing') {
                datarow.push(<div className="d-flex small p-1 d-md-none d-lg-none" style={i % 2 === 0 ? { backgroundColor: "rgb(181, 253, 233)" } : {}}>
                    <div className="col-5 col-md-5 p-1">
                        <div className="col-12 col-md-6 p-0 ">
                            <div>{data.date}</div>
                            <div><strong>To Drawings</strong></div>
                        </div>
                    </div>
                    <div className="col-3 col-md-6 p-0 pt-1 text-right">{parseFloat(data.amount + data.extraamount).toFixed(2)}</div>
                    <div className="col-3 col-md-1 p-0 pt-1"></div>
                </div>);
                datarow.push(
                    <div className="container small d-none d-md-block p-1" style={i % 2 === 0 ? { backgroundColor: "rgb(181, 253, 233)" } : {}}>
                        <div className="d-flex">
                            <div className="col-2 p-0">{data.date}</div>
                            <div className="col-4 p-0"><strong>To Drawings</strong><div></div>
                            </div>
                            <div className="col-2 text-right p-0">{parseFloat(data.amount + data.extraamount).toFixed(2)}</div>
                            <div className="col-3"></div>
                            <div className="col-1"></div>
                        </div>
                    </div>
                );
                debit = debit + parseFloat(data.amount + data.extraamount);
            }
            else if (data.activity === 'deposit') {
                datarow.push(<div className="d-flex small p-1 d-md-none d-lg-none" style={i % 2 === 0 ? { backgroundColor: "rgb(181, 253, 233)" } : {}}>
                    <div className="col-5 col-md-5 p-1">
                        <div className="col-12 col-md-6 p-0 ">
                            <div>{data.date}</div>
                            <div><strong>By {data.bankdetail.split("...")[1]} (Deposit)</strong></div>
                        </div>
                    </div>
                    <div className="col-3 col-md-6 p-0 pt-1"></div>
                    <div className="col-3 col-md-1 p-0 pt-1 text-right">{parseFloat(data.amount + data.extraamount).toFixed(2)}</div>
                </div>);
                datarow.push(
                    <div className="container small d-none d-md-block p-1" style={i % 2 === 0 ? { backgroundColor: "rgb(181, 253, 233)" } : {}}>
                        <div className="d-flex">
                            <div className="col-2 p-0">{data.date}</div>
                            <div className="col-4 p-0"><strong>By {data.bankdetail.split("...")[1]} (Deposit)</strong><div></div>
                            </div>
                            <div className="col-2"></div>
                            <div className="col-3 text-right p-0">{parseFloat(data.amount+data.extraamount).toFixed(2)}</div>
                            <div className="col-1"></div>
                        </div>
                    </div>
                );
                credit = credit + parseFloat(data.amount+data.extraamount);
            }
        }
        else if (data.type === 'Income') {
            datarow.push(<div className="d-flex small p-1 d-md-none d-lg-none" style={i % 2 === 0 ? { backgroundColor: "rgb(181, 253, 233)" } : {}}>
                <div className="col-5 col-md-5 p-1">
                    <div className="col-12 col-md-6 p-0 ">
                        <div>{data.date}</div>
                        <div><strong> By {data.category}</strong></div>
                    </div>
                </div>
                <div className="col-3 col-md-6 p-0 pt-1"></div>
                <div className="col-3 col-md-1 p-0 pt-1 text-right">{parseFloat(data.amount).toFixed(2)}</div>
            </div>);
            datarow.push(
                <div className="container small d-none d-md-block p-1" style={i % 2 === 0 ? { backgroundColor: "rgb(181, 253, 233)" } : {}}>
                    <div className="d-flex">
                        <div className="col-2 p-0">{data.date}</div>
                        <div className="col-4 p-0"><strong> By {data.category}</strong><div></div>
                        </div>
                        <div className="col-2"></div>
                        <div className="col-3 text-right p-0">{parseFloat(data.amount).toFixed(2)}</div>
                        <div className="col-1"></div>
                    </div>
                </div>
            );
            credit = credit + parseFloat(data.amount);
        }
        else if (data.type === 'Expense') {
            datarow.push(<div className="d-flex small p-1 d-md-none d-lg-none" style={i % 2 === 0 ? { backgroundColor: "rgb(181, 253, 233)" } : {}}>
                <div className="col-5 col-md-5 p-1">
                    <div className="col-12 col-md-6 p-0 ">
                        <div>{data.date}</div>
                        <div><strong> To {data.category}</strong></div>
                    </div>
                </div>
                <div className="col-3 col-md-6 p-0 pt-1 text-right">{parseFloat(data.amount).toFixed(2)}</div>
                <div className="col-3 col-md-1 p-0 pt-1"></div>
            </div>);
            datarow.push(
                <div className="container small d-none d-md-block p-1" style={i % 2 === 0 ? { backgroundColor: "rgb(181, 253, 233)" } : {}}>
                    <div className="d-flex">
                        <div className="col-2 p-0">{data.date}</div>
                        <div className="col-4 p-0"> <strong>To {data.category}</strong><div></div>
                        </div>
                        <div className="col-2 text-right p-0">{parseFloat(data.amount).toFixed(2)}</div>
                        <div className="col-3"></div>
                        <div className="col-1"></div>
                    </div>
                </div>
            );
            debit = debit + parseFloat(data.amount);
        }
        else if ((data.type === 'payment') && (data.paymenttype === 'paymentmade')) {
            datarow.push(
                <div>
                    <div className="d-flex small p-1 d-md-none d-lg-none" style={i % 2 === 0 ? { backgroundColor: "rgb(181, 253, 233)" } : {}}>
                        <div className="col-5 col-md-5 p-1">
                            <div className="col-12 col-md-6 p-0">{data.voucherno}/{data.date}</div>
                            <div className="col-12 col-md-6 p-0 "><strong>To {data.paymentmode === 'cash' ? 'Cash' : data.bankdetail.split("...")[1]}</strong></div>
                            <div className="col-12 col-md-6 p-0 ">Receipt</div>
                        </div>
                        <div className="col-3 col-md-1 text-right">{parseFloat(data.amount).toFixed(2)}</div>
                        <div className="col-3 col-md-6 p-1"></div>
                    </div>
                    <div className="container small d-none d-md-block p-1" style={i % 2 === 0 ? { backgroundColor: "rgb(181, 253, 233)" } : {}}>
                        <div className="d-flex">
                            <div className="col-1 p-0">{data.date}</div>
                            <div className="col-1 p-0">{data.voucherno}</div>
                            <div className="col-4 p-0">
                                <div className="col-12 col-md-6 p-0 "><strong>To {data.paymentmode === 'cash' ? 'Cash' : data.bankdetail.split("...")[1]}</strong></div>
                                <div className="col-12 col-md-6 p-0 ">Receipt</div>
                            </div>
                            <div className="col-2 text-right">{parseFloat(data.amount).toFixed(2)}</div>
                            <div className="col-3"><strong></strong></div>
                        </div>
                    </div>
                </div >
            )
            debit = debit + parseFloat(data.amount);
        }
        else if ((data.type === 'payment') && (data.paymenttype === 'paymentreceived')) {
            datarow.push(
                <div>
                    <div className="d-flex small p-1 d-md-none d-lg-none" style={i % 2 === 0 ? { backgroundColor: "rgb(181, 253, 233)" } : {}}>
                        <div className="col-5 col-md-5 p-1">
                            <div className="col-12 col-md-6 p-0">{data.voucherno}/{data.date}</div>
                            <div className="col-12 col-md-6 p-0 "><strong>By {data.paymentmode === 'cash' ? 'Cash' : data.bankdetail.split("...")[1]}</strong></div>
                            <div className="col-12 col-md-6 p-0 ">Payment</div>
                        </div>
                        <div className="col-3 col-md-1"></div>
                        <div className="col-3 col-md-6 p-1 text-right">{parseFloat(data.amount).toFixed(2)}</div>
                    </div>
                    <div className="container small d-none d-md-block p-1" style={i % 2 === 0 ? { backgroundColor: "rgb(181, 253, 233)" } : {}}>
                        <div className="d-flex">
                            <div className="col-1 p-0">{data.date}</div>
                            <div className="col-1 p-0">{data.voucherno}</div>
                            <div className="col-4 p-0">
                                <div className="col-12 col-md-6 p-0 "><strong>By {data.paymentmode === 'cash' ? 'Cash' : data.bankdetail.split("...")[1]}</strong></div>
                                <div className="col-12 col-md-6 p-0 ">Payment</div>
                            </div>
                            <div className="col-2"></div>
                            <div className="col-3 text-right">{parseFloat(data.amount).toFixed(2)}</div>
                        </div>
                    </div>
                </div >
            )
            credit = credit + parseFloat(data.amount);
        }
        i++;
    })
    datarow.push(<div>
        <div className="d-flex small p-1 d-md-none d-lg-none" style={i % 2 === 0 ? { backgroundColor: "rgb(181, 253, 233)" } : {}}>
            <div className="col-5 col-md-5 p-1">
                <div className="col-12 col-md-6 p-0"><strong>{profit >= 0 ? "By Profit" : "To Profit"}</strong></div>
            </div>
            <div className="col-3 col-md-1 ">
                <div className="col-12 col-md-2 p-0 pt-1 text-right">{profit < 0 ? parseFloat(-1 * profit * holding).toFixed(2) : ''}</div>
            </div>
            <div className="col-3 col-md-6 p-0 ">
                <div className="col-12 col-md-2 p-0 pt-1 text-right">{profit >= 0 ? parseFloat(profit * holding).toFixed(2) : ''}</div>
            </div>
        </div>
        <div className="container d-none d-md-block small p-1" style={i % 2 === 0 ? { backgroundColor: "rgb(181, 253, 233)" } : {}}>
            <div className="d-flex">
                <div className="col-2 p-0"></div>
                <div className="col-4 p-0"><strong>{profit >= 0 ? "By Profit" : "To Profit"}</strong></div>
                <div className="col-2 text-right p-0">{profit < 0 ? parseFloat(-1 * profit * holding).toFixed(2) : ''}</div>
                <div className="col-3 text-right p-0">{profit >= 0 ? parseFloat(profit * holding).toFixed(2) : ''}</div>
                <div className="col-1"></div>
            </div>
        </div>
    </div>)
    datarow.push(<div>
        <div className="d-flex small p-1 d-md-none d-lg-none border-top">
            <div className="col-5 col-md-5 p-1">
                <div className="col-12 col-md-6 p-0"></div>
                <div className="col-12 col-md-6 p-0"><strong></strong></div>
            </div>
            <div className="col-3 col-md-1 border-top border-dark p-0">
                <div className="col-12 col-md-2 p-0 pt-1 text-right">{parseFloat(debit).toFixed(2)}</div>
            </div>
            <div className="col-3 col-md-6 p-0 border-top border-dark p-0">
                <div className="col-12 col-md-2 p-0 pt-1 text-right">{parseFloat(credit).toFixed(2)}</div>
            </div>
        </div>
        <div className="d-flex small p-1 d-md-none d-lg-none">
            <div className="col-5 col-md-5 p-1 p-0">
                <div className="col-12 col-md-6 p-0"></div>
                <div className="col-12 col-md-6 p-0 "><strong>{((type === 'partner') && (debit > credit)) ? "Closing Bal (Loan)" : "Closing Bal"}</strong></div>
            </div>
            <div className="col-3 col-md-1 border-top border-dark p-0 text-right">{credit >= debit ? parseFloat(credit - debit).toFixed(2) : ''}</div>
            <div className="col-3 col-md-6 p-0 border-top border-dark">
                <div className="col-12 col-md-2 p-0 pt-1 text-right">{debit > credit ? parseFloat(debit - credit).toFixed(2) : ''}</div>
            </div>
        </div>
        <div className="container d-none d-md-block small p-1">
            <div className="d-flex">
                <div className="col-2 p-0"><strong></strong></div>
                <div className="col-4 p-0"></div>
                <div className="col-2 p-0 border-top border-dark text-right">{parseFloat(debit).toFixed(2)}</div>
                <div className="col-3 p-0 border-top border-dark text-right">{parseFloat(credit).toFixed(2)}</div>
                <div className="col-1"></div>
            </div>
        </div>
        <div className="container d-none d-md-block small p-1">
            <div className="d-flex">
                <div className="col-2 p-0"></div>
                <div className="col-4 p-0"><strong>{((type === 'partner') && (debit > credit)) ? "Closing Bal (Loan)" : "Closing Bal"}</strong></div>
                <div className="col-2 border-top border-dark text-right p-0">{credit >= debit ? parseFloat(credit - debit).toFixed(2) : ''}</div>
                <div className="col-3 border-top border-dark text-right p-0">{debit > credit ? parseFloat(debit - credit).toFixed(2) : ''}</div>
                <div className="col-1"></div>
            </div>
        </div>
    </div>)
    return datarow;
}

let debit = 0, credit = 0;
const CapitalAccountPage = function ({ data, profit, holding, business, openingBalance }) {
    let datarow = [];
    let diffdebit = '';
    let diffcredit = '';
    openingBalance.forEach(data => {
        datarow.push(
            <View style={pdfstyles.itemSectionLedger, { marginTop: "5px", textAlign: "center" }}>
                <View style={pdfstyles.itemHeader}><Text style={{ width: '75px', textAlign: "left" }}></Text><Text style={{ width: '300px', textAlign: "left", fontFamily: "Helvetica-Bold" }}>{business.businesstype === 'partnership' && data.amounttype === 'payable' ? 'Opening Balance ' : 'Opening Balance'}</Text><Text style={{ width: '100px', textAlign: "right" }}>{business.businesstype === 'partnership' && data.amounttype === 'payable' ? parseFloat(data.amount).toFixed(2) : ''}</Text><Text style={{ width: '100px', textAlign: "right" }}>{business.businesstype === 'propritorship' ? parseFloat(data.amount).toFixed(2) : ''} {business.businesstype === 'partner' && data.amounttype === 'deposited' ? parseFloat(data.amount).toFixed(2) : ''}</Text></View>
            </View>);
        if ((business.businesstype === 'partnership') && (data.amounttype === 'payable')) debit = debit + parseFloat(data.amount);
        else credit = credit + parseFloat(data.amount);
    })
    data.forEach(data => {
        if (data.type === 'investment') {
            if (data.activity === 'drawing') {
                datarow.push(
                    <View style={pdfstyles.itemSectionLedger, { textAlign: "center" }}>
                        <View style={pdfstyles.itemHeader}><Text style={{ width: '75px', textAlign: "left" }}>{data.date}</Text><Text style={{ width: '300px', textAlign: "left", fontFamily: "Helvetica-Bold" }}>To Drawings</Text><Text style={{ width: '100px', textAlign: "right" }}>{parseFloat(data.amount+data.extraamaount).toFixed(2)}</Text><Text style={{ width: '100px', textAlign: "right" }}></Text></View>
                    </View>);
                debit = debit + parseFloat(data.amount+data.extraamount);
            }
            else if (data.activity === 'deposit') {
                datarow.push(
                    <View style={pdfstyles.itemSectionLedger, { textAlign: "center" }}>
                        <View style={pdfstyles.itemHeader}><Text style={{ width: '75px', textAlign: "left" }}>{data.date}</Text><Text style={{ width: '300px', textAlign: "left", fontFamily: "Helvetica-Bold" }}>By {data.bankdetail.split("...")[1]} (Deposit)</Text><Text style={{ width: '100px', textAlign: "right" }}></Text><Text style={{ width: '100px', textAlign: "right" }}>{parseFloat(data.amount+data.extraamount).toFixed(2)}</Text></View>
                    </View>);
                credit = credit + parseFloat(data.amount+data.extraamount);
            }
        }
        else if (data.type === 'Income') {
            datarow.push(
                <View style={pdfstyles.itemSectionLedger, { textAlign: "center" }}>
                    <View style={pdfstyles.itemHeader}><Text style={{ width: '75px', textAlign: "left" }}>{data.date}</Text><Text style={{ width: '300px', textAlign: "left", fontFamily: "Helvetica-Bold" }}>By {data.category}</Text><Text style={{ width: '100px', textAlign: "right" }}></Text><Text style={{ width: '100px', textAlign: "right" }}>{parseFloat(data.amount).toFixed(2)}</Text></View>
                </View>);
            credit = credit + parseFloat(data.amount);
        }
        else if (data.type === 'Expense') {
            datarow.push(
                <View style={pdfstyles.itemSectionLedger, { textAlign: "center" }}>
                    <View style={pdfstyles.itemHeader}><Text style={{ width: '75px', textAlign: "left" }}>{data.date}</Text><Text style={{ width: '300px', textAlign: "left", fontFamily: "Helvetica-Bold" }}>To {data.category}</Text><Text style={{ width: '100px', textAlign: "right" }}>{parseFloat(data.amount).toFixed(2)}</Text><Text style={{ width: '100px', textAlign: "right" }}></Text></View>
                </View>);
            debit = debit + parseFloat(data.amount);
        }
        else if ((data.type === 'payment') && (data.paymenttype === 'paymentmade')) {
            datarow.push(
                <View style={pdfstyles.itemSectionLedger, { textAlign: "center" }}>
                    <View style={pdfstyles.itemHeader}><Text style={{ width: '75px', textAlign: "left", fontFamily: "Helvetica-Bold" }}>{data.date}</Text><Text style={{ width: '300px', textAlign: "right" }}>To {data.paymentmode === 'cash' ? 'Cash' : data.bankdetail.split("...")[1]} (Receipt)</Text><Text style={{ width: '100px', textAlign: "right" }}>{parseFloat(data.amount).toFixed(2)}</Text><Text style={{ width: '100px', textAlign: "left" }}></Text></View>
                </View>);
            debit = debit + parseFloat(data.amount);
        }
        else if ((data.type === 'payment') && (data.paymenttype === 'paymentreceived')) {
            datarow.push(
                <View style={pdfstyles.itemSectionLedger, { textAlign: "center" }}>
                    <View style={pdfstyles.itemHeader}><Text style={{ width: '75px', textAlign: "left", fontFamily: "Helvetica-Bold" }}>{data.date}</Text><Text style={{ width: '300px', textAlign: "left" }}>By {data.paymentmode === 'cash' ? 'Cash' : data.bankdetail.split("...")[1]} (Payment)</Text><Text style={{ width: '100px', textAlign: "left" }}></Text><Text style={{ width: '100px', textAlign: "left" }}>{parseFloat(data.amount).toFixed(2)}</Text></View>
                </View>);
            credit = credit + parseFloat(data.amount);
        }
    })
    if (profit >= 0) {
        datarow.push(
            <View style={pdfstyles.itemSectionLedger, { textAlign: "center" }}>
                <View style={pdfstyles.itemHeader}><Text style={{ width: '75px', textAlign: "left" }}></Text><Text style={{ width: '300px', textAlign: "left", fontFamily: "Helvetica-Bold" }}>By Profit </Text><Text style={{ width: '100px', textAlign: "right" }}></Text><Text style={{ width: '100px', textAlign: "right" }}>{parseFloat(profit * holding).toFixed(2)}</Text></View>
            </View>);
        credit = credit + parseFloat(profit);
    }
    else if (profit < 0) {
        datarow.push(<View style={pdfstyles.itemSectionLedger, { textAlign: "center" }}>
            <View style={pdfstyles.itemHeader}><Text style={{ width: '75px', textAlign: "left" }}></Text><Text style={{ width: '300px', textAlign: "left", fontFamily: "Helvetica-Bold" }}>To Loss </Text><Text style={{ width: '100px', textAlign: "right" }}>{parseFloat(-1 * profit * holding).toFixed(2)}</Text><Text style={{ width: '100px', textAlign: "right" }}></Text></View>
        </View>);
        debit = debit + parseFloat(profit);
    }
    datarow.push(
        <View style={pdfstyles.itemSectionLedger, { textAlign: "center" }}>
            <View style={pdfstyles.itemHeader}><Text style={{ width: '75px', textAlign: "left" }}></Text><Text style={{ width: '300px', textAlign: "left", fontFamily: "Helvetica-Bold" }}></Text><Text style={{ width: '100px', textAlign: "right", borderTop: "1px sold black" }}>{parseFloat(debit).toFixed(2)}</Text><Text style={{ width: '100px', textAlign: "right", borderTop: "1px sold black" }}>{parseFloat(credit).toFixed(2)}</Text></View>
        </View>);
    if (credit >= debit) diffdebit = credit - debit;
    else if (debit > credit) diffcredit = debit - credit;
    datarow.push(
        <View style={pdfstyles.itemSectionLedger, { textAlign: "center" }}>
            <View style={pdfstyles.itemHeader}><Text style={{ width: '75px', textAlign: "left" }}></Text><Text style={{ width: '300px', textAlign: "left", fontFamily: "Helvetica-Bold" }}></Text><Text style={{ width: '100px', textAlign: "right" }}>{diffdebit ? parseFloat(diffdebit).toFixed(2) : ''}</Text><Text style={{ width: '100px', textAlign: "right" }}>{diffcredit ? parseFloat(diffcredit).toFixed(2) : ''}</Text></View>
        </View>);
    if (isNaN(diffdebit)) diffdebit = 0;
    if (isNaN(diffcredit)) diffcredit = 0;
    datarow.push(
        <View style={pdfstyles.itemSectionLedger, { textAlign: "center", borderTop: "1px solid black", borderBottom: "1px solid black" }}>
            <View style={pdfstyles.itemHeader}><Text style={{ width: '75px', textAlign: "left" }}></Text><Text style={{ width: '300px', textAlign: "left", fontFamily: "Helvetica-Bold" }}>Total</Text><Text style={{ width: '100px', textAlign: "right" }}>{parseFloat(debit + diffdebit).toFixed(2)}</Text><Text style={{ width: '100px', textAlign: "right" }}>{parseFloat(credit + diffcredit).toFixed(2)}</Text></View>
        </View>);
    return datarow;
}

const Report = function ({ data, state, business, fy, type, profit, holding, openingBalance }) {
    let returnrows = [];
    returnrows.push(<Page size="A4" style={pdfstyles.page}>
        <View style={pdfstyles.taxInvoiceHeader, { marginTop: "20px", textAlign: "center" }}>
            <View style={{ fontFamily: "Helvetica-Bold", width: "500px" }}><Text>{business.businessname}</Text></View>
            <View style={{ width: "500px" }}><Text>{business.address}</Text></View>
        </View>
        <View style={pdfstyles.taxInvoiceHeader, { marginTop: "10px", textAlign: "center" }}>
            <View style={{ fontFamily: "Helvetica-Bold", textDecoration: "underline", width: "500px" }}><Text>{business.businesstype === 'partnership' ? state.headSelection.split("...")[1] + " Capital Account" : "Capital Account"}</Text></View>
            <View style={{ width: "500px" }}><Text>{fy}</Text></View>
        </View>
        <View style={pdfstyles.itemSectionLedger, { marginTop: "20px", marginBottom: "5px", fontFamily: "Helvetica-Bold", textAlign: "center", borderBottom: "1px solid black", borderTop: "1px solid black" }}>
            <View style={pdfstyles.itemHeader}><Text style={{ width: '75px', textAlign: "left" }}>Date</Text><Text style={{ width: '300px', textAlign: "left" }}>Particulars</Text><Text style={{ width: '100px', textAlign: "right" }}>Debit</Text><Text style={{ width: '100px', textAlign: "right" }}>Credit</Text></View>
        </View>
        <CapitalAccountPage data={data} profit={profit} holding={holding} business={business} openingBalance={openingBalance} />
    </Page >)
    return returnrows;
}
const CapitalAccountLedger = function ({ data, state, business, fy, type, profit, holding, openingBalance }) {
    return (
        <Document>
            <Report data={data} state={state} business={business} fy={fy} type={type} profit={profit} holding={holding} openingBalance={openingBalance} />
        </Document>
    )
}
const generatePdf = async (getInvestment, deleteEntry, business, fy, state, getInvestmentOpeningBalance, getIncomeOS, getExpense, getLoan, getPayment, profit, type, getInvestmentAccount) => {
    try {
        let investmentdata = getInvestment.data;
        if (business.businesstype === 'partnership') investmentdata = getInvestment.data.filter(d => d.investmentaccount === state.headSelection);
        let incomedata = [], expensedata = [], paymentdata = [], holding = 1;
        if (business.businesstype === 'partnership') {
            incomedata = getIncomeOS.data ? getIncomeOS.data.filter(d => d.customerid === state.headSelection.split("...")[0]) : [];
            expensedata = getExpense.data ? getExpense.data.filter(d => d.vendorid === state.headSelection.split("...")[0]) : [];
            paymentdata = getPayment.data ? getPayment.data.filter(d => d.vendorid === state.headSelection.split("...")[0] || d.customerid === state.headSelection.split("...")[0]) : [];
            holding = (getInvestmentAccount.data.filter(d => d.id === state.headSelection.split("...")[0])[0].shareholding) / 100;
        }
        let data = investmentdata;
        data = data.concat(incomedata);
        data = data.concat(expensedata);
        data = data.concat(paymentdata);
        data = data.sort(sortData);
        let datarow = [];
        let openingBalance = getInvestmentOpeningBalance.data;
        if ((state.headSelection !== '') && (business.businesstype === 'partnership')) {
            openingBalance = openingBalance.filter(d => d.investmentaccount === state.headSelection)
        }
        let i = 2, debit = 0, credit = 0;
        if (parseFloat(profit) >= 0) credit = credit + parseFloat(profit) * holding;
        else debit = debit + (parseFloat(profit) * -1) * holding;

        const doc = <CapitalAccountLedger data={data} state={state} business={business} fy={fy} type={type} profit={profit} holding={holding} openingBalance={openingBalance} />;
        const blobPdf = pdf(doc);
        blobPdf.updateContainer(doc);
        const result = await blobPdf.toBlob();
        let filename = '';
        if (business.businesstype === 'propritorship') filename = 'CapitalAccountLedger.pdf';
        else if (business.businesstype === 'partnership' && state.headSelection !== '') filename = state.headSelection.split("...")[1] + "Ledger.pdf";
        saveAs(result, filename);
    }
    catch (err) {
        alert(err);
    }
}

class InvestmentBook extends Component {
    constructor(props) {
        super(props);
        this.state = ({
            headSelection: ''
        })
        window.onload = () => {
            this.props.refreshInvestmentState();
            this.props.refreshInvestmentAccountState();
            this.props.resetInvestmentOpeningBalance();
            this.props.resetIncomeOSDetail();
            this.props.resetExpenseDetail();
            this.props.refreshLoanState();
            this.props.resetPaymentDetail();
            this.props.getSalesData(this.props.businessid, this.props.fyid);
            this.props.getPurchaseData(this.props.businessid, this.props.fyid);
            this.props.getOpeningClosingDetail(this.props.businessid, this.props.fyid);
            this.props.getDebitCreditList(this.props.businessid, this.props.fyid);
            this.props.getAssetAccountList(this.props.businessid);
            this.props.getAssetOpeningBalanceList(this.props.businessid, this.props.fyid);
            this.props.getDepreciationList(this.props.businessid, this.props.fyid);
        }
    }
    handleOnChange = (event) => {
        let target = event.target;
        let name = target.name;
        let value = target.value;
        this.setState({
            ...this.state,
            [name]: value
        })
    }
    componentDidMount() {
        if (this.props.getInvestmentOpeningBalance.message === 'initial') this.props.getInvestmentOpeningBalanceList(this.props.businessid, this.props.fyid);
        if (this.props.getInvestmentAccount.message === 'initial') this.props.getInvestmentAccountList(this.props.businessid);
        if (this.props.getInvestment.message === 'initial') this.props.getInvestmentDetail(this.props.businessid, this.props.fyid);
        if (this.props.getIncomeOS.message === 'initial') this.props.getIncomeOSDetail(this.props.businessid, this.props.fyid);
        if ((this.props.getExpense.message === 'initial') && (this.props.getExpense.loading === false)) this.props.getExpenseDetail(this.props.businessid, this.props.fyid);
        if (this.props.getLoan.message === 'initial') this.props.getLoanDetail(this.props.businessid, this.props.fyid);
        if ((this.props.getPayment.message === 'initial') && (this.props.getPayment.loading === false)) this.props.getPaymentDetail(this.props.businessid, this.props.fyid);
        if (this.props.logoutMessage.error !== '') {
            alert("Error Logging Out. Please Try Again");
            this.props.resetLogoutMessage();
        }
        if (this.props.deleteEntryMessage.message === "success") {
            alert("Entry Deleted Successfully");
            this.props.resetDeleteEntry();
        }
        if (this.props.deleteEntryMessage.error !== '') {
            alert("Error Deleting Entry - Please Try Again");
            this.props.resetDeleteEntry();
        }

    }
    render() {
        if (!cookie.load('userservice')) return <Redirect to="/" />;
        else {
            if ((this.props.getInvestment.loading === true) || (this.props.logoutMessage.loading === true) || (this.props.deleteEntryMessage.loading === true) || (this.props.getInvestmentAccount.loading === true) || (this.props.getInvestmentOpeningBalance.loading == true) || (this.props.getPayment.loading === true) || (this.props.getSales.loading === true) || (this.props.getPurchase.loading === true) || (this.props.getOpeningClosing.loading === true) || (this.props.getDebitCredit.loading === true) || (this.props.getInvestment.loading === true) || (this.props.getAssetAccount.loading === true) || (this.props.getAssetOpeningBalance.loading === true) || (this.props.getDepreciation.loading == true) || (this.props.getIncomeOS.loading === true) || (this.props.getExpense.loading === true)) {
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div className="mt-3 d-flex justify-content-center">
                            <button className="btn btn-danger">Loading... </button>
                        </div>
                    </div>
                );
            }
            else if ((this.props.getInvestment.error !== '') || (this.props.getInvestmentAccount.error !== '') || (this.props.getInvestmentOpeningBalance.error !== '') || (this.props.getPayment.error !== '') || (this.props.getSales.error !== '') || (this.props.getPurchase.error !== '') || (this.props.getOpeningClosing.error !== '') || (this.props.getDebitCredit.error !== '') || (this.props.getInvestment.error !== '') || (this.props.getAssetAccount.error !== '') || (this.props.getAssetOpeningBalance.error !== '') || (this.props.getDepreciation.error !== '') || (this.props.getIncomeOS.error !== '') || (this.props.getExpense.error !== '')) {
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div className="mt-3 d-flex justify-content-center">
                            <button className="btn btn-danger">Error.. Please Refresh and Try Agin </button>
                        </div>
                    </div>
                );
            }
            else {
                let business = this.props.businessList.data.filter(b => b.businessid === this.props.businessid)[0];
                let fy = business.financial.filter(f => f.fyid === this.props.fyid)[0].financialyear;
                let totalsalesamount = 0;
                let totalpurchaseamount = 0;
                let openingStock = 0;
                let closingStock = 0;
                let totalincome = 0;
                let totalexpense = 0;
                let totalinvestmentprofit = 0;
                let assetpresent = false;
                let totalassetprofit = 0;
                let depreciation = 0;
                let asset = {};
                this.props.getSales.data.forEach(d => d.type === 'sales' ? totalsalesamount = totalsalesamount + parseFloat(d.totalbasic) : '');
                this.props.getPurchase.data.forEach(d => d.asset == 0 ? totalpurchaseamount = totalpurchaseamount + parseFloat(d.totalbasic) : totalpurchaseamount = totalpurchaseamount);
                this.props.getDebitCredit.data.forEach(d => d.vendorcustomer === 'customer' ? totalsalesamount = totalsalesamount - parseFloat(d.basic) : totalpurchaseamount = totalpurchaseamount - parseFloat(d.basic));
                this.props.getExpense.data.forEach(d => totalexpense = totalexpense + parseFloat(d.amount));
                this.props.getIncomeOS.data.forEach(d => totalincome = totalincome + parseFloat(d.amount));
                let flag = 0;
                this.props.getAssetAccount.data.forEach(d => {
                    if (d.soldfyid === this.props.fyid) {
                        asset = {
                            ...asset,
                            [d.id]: { name: d.assetaccount, amount: d.soldbasic }
                        }
                        totalassetprofit = totalassetprofit + parseFloat(d.soldbasic);
                        assetpresent = true;
                    }
                    if ((d.depreciating === '1') && (d.purchasefyid === this.props.fyid)) {
                        if (Object.keys(asset).includes(d.asset)) {
                            flag = 1;
                            asset = {
                                ...asset,
                                [d.id]: {
                                    name: asset[d.id].name,
                                    amount: asset[d.id].amount - parseFloat(d.purchasebasic)
                                }
                            }
                            totalassetprofit = totalassetprofit - parseFloat(d.purchasebasic);
                        }
                    }
                    else if (d.depreciating === '0') {
                        if (Object.keys(asset).includes(d.asset)) {
                            asset = {
                                ...asset,
                                [d.id]: {
                                    name: asset[d.id].name,
                                    amount: asset[d.id].amount - parseFloat(d.purchasebasic)
                                }
                            }
                            totalassetprofit = totalassetprofit - parseFloat(d.purchasebasic);
                        }
                    }
                })
                this.props.getAssetOpeningBalance.data.forEach(d => {
                    if (Object.keys(asset).includes(d.asset)) {
                        if (flag === 1) {
                            asset = {
                                ...asset,
                                [d.assetaccount]: {
                                    name: asset[d.assetaccount].name,
                                    amount: asset[d.assetaccount].amount - parseFloat(d.amount)
                                }
                            }
                            totalassetprofit = totalassetprofit - parseFloat(d.amount);
                        }
                    }
                })
                this.props.getDepreciation.data.forEach(d => {
                    depreciation = depreciation + parseFloat(d.amount)
                })
                openingStock = parseFloat(this.props.getOpeningClosing.data.aprilopening);
                closingStock = parseFloat(this.props.getOpeningClosing.data.marchclosing);
                let profit = parseFloat(totalsalesamount + closingStock + totalincome + totalinvestmentprofit + totalassetprofit) - parseFloat(totalexpense + totalpurchaseamount + openingStock + depreciation)
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div style={{ minHeight: "93vh", display: "flex", flexDirection: "column" }}>
                            <div className="container" style={{ flex: "1" }}>
                                <h6 className="text-center pl-2 pr-2 pt-4" style={{ lineHeight: "1" }}><strong>{business.businessname} {fy}</strong></h6>
                                <h6 className="text-center pl-2 pr-2 mb-3" style={{ lineHeight: "1" }}>{business.alias ? "(" + business.alias + ")" : ''}</h6>
                                <h6 className="text-center pl-2 pr-2 mb-2"><strong><u>{business.businesstype === 'propritorship' ? 'Capital Account Ledger' : "Partner's Capital Account Ledger"}</u></strong>&nbsp;&nbsp;&nbsp;{business.businesstype === 'propritorship' || (business.businesstype === 'partnership' && this.state.headSelection !== '') ? <FontAwesomeIcon icon={faFilePdf} size="lg" onClick={() => generatePdf(this.props.getInvestment, this.props.deleteEntry, business, fy, this.state, this.props.getInvestmentOpeningBalance, this.props.getIncomeOS, this.props.getExpense, this.props.getLoan, this.props.getPayment, profit, 'propritor', this.props.getInvestmentAccount)} /> : ''}</h6>
                                {business.businesstype === 'partnership' ? <LocalForm className="small">
                                    <div className="d-flex justify-content-center">
                                        <div className="form-group col-12 col-md-3">
                                            <label htmlFor="headSelection" className="mb-0 muted-text"><strong>Select Partner</strong></label>
                                            <Control.select model=".headSelection" className="form-control form-control-sm" id="headSelection" name="headSelection" value={this.state.headSelection} onChange={this.handleOnChange}>
                                                <Option data={this.props.getInvestmentAccount.data} />
                                            </Control.select>
                                        </div>
                                    </div>
                                </LocalForm> : ''}
                                {business.businesstype === 'partnership' ? this.state.headSelection !== '' ? <DataDisplayPartner getInvestment={this.props.getInvestment} deleteEntry={this.props.deleteEntry} businessid={this.props.businessid} fyid={this.props.fyid} state={this.state} getInvestmentOpeningBalance={this.props.getInvestmentOpeningBalance} getIncomeOS={this.props.getIncomeOS} getExpense={this.props.getExpense} getLoan={this.props.getLoan} getPayment={this.props.getPayment} profit={profit} type={'partner'} getInvestmentAccount={this.props.getInvestmentAccount} /> : '' : ''}
                                {business.businesstype === 'propritorship' ? <DataDisplayPartner getInvestment={this.props.getInvestment} deleteEntry={this.props.deleteEntry} businessid={this.props.businessid} fyid={this.props.fyid} state={this.state} getInvestmentOpeningBalance={this.props.getInvestmentOpeningBalance} getIncomeOS={this.props.getIncomeOS} getExpense={this.props.getExpense} getLoan={this.props.getLoan} getPayment={this.props.getPayment} profit={profit} type={'propritor'} getInvestmentAccount={this.props.getInvestmentAccount} /> : ''}
                            </div>
                            <Footer />
                        </div>
                    </div>
                )
            }
        }
    }
}

export default InvestmentBook;