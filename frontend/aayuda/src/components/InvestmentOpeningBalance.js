import React, { Component } from 'react';
import cookie from 'react-cookie';
import { Redirect } from 'react-router-dom';
import { LocalForm, Control } from 'react-redux-form';
import { Header } from './HeaderComponent';
import { Footer } from './Footer';

class InvestmentOpeningBalance extends Component {
    constructor(props) {
        super(props);
        this.state = ({
            Investmentaccount: '',
            amount: 0,
            madereceived: 'inward',
            businesstype: '',
            amounttype: ''
        })
        window.onload = () => {
            this.props.refreshInvestmentAccountState();
            this.props.resetInvestmentOpeningBalance();
        }
    }
    handleSubmit = () => {
        let business = this.props.businessList.data.filter(b => b.businessid === this.props.businessid)[0];
        if ((this.state.Investmentaccount === '') && (business.businesstype !== 'propritorship')) alert("Investment Account Not Selected");
        else if (this.state.madereceived === '') alert("Please Select Investment Made/Received");
        else {
            if ((this.state.amount === '') || (this.state.amount < 0) || (isNaN(parseInt(this.state.amount)))) alert(" Amount Must Be Greater Than Equal To 0 ");
            else this.props.updateInvestmentOpeningBalance(this.state, this.props.businessid, this.props.fyid);
        }
    }
    componentDidMount() {
        if (this.props.updateInvestmentOpeningMessage.error !== '') {
            alert(this.props.updateInvestmentOpeningMessage.error);
            this.setState({
                ...this.props.updateInvestmentOpeningMessage.state
            })
            this.props.refreshUpdateInvestmentOpeningBalance();
        }
        if (this.props.updateInvestmentOpeningMessage.message === 'success') {
            alert("Data Updated Successfully");
            this.setState({
                ...this.props.updateInvestmentOpeningMessage.state
            })
            this.props.refreshUpdateInvestmentOpeningBalance();
        }
        if (this.props.getInvestmentOpeningBalance.message === 'initial') this.props.getInvestmentOpeningBalanceList(this.props.businessid, this.props.fyid);
        if (this.props.logoutMessage.error !== '') {
            alert("Error Logging Out. Please Try Again");
            this.props.resetLogoutMessage();
        }
        if (this.props.getInvestmentAccount.message === 'initial') this.props.getInvestmentAccountList(this.props.businessid);
        if (this.props.getInvestmentOpeningBalance.message === 'success') {
            if (this.props.getInvestmentOpeningBalance.data.length) {
                let Investment = this.props.getInvestmentOpeningBalance.data.filter(d => d.investmentaccount === this.state.Investmentaccount && d.madereceive === this.state.madereceive)[0];
                this.setState({
                    ...this.state,
                    amount: Investment ? Investment.amount : 0
                })
            }
        }
    }
    handleInputChange = (event) => {
        let target = event.target;
        let name = target.name;
        let value = target.value;
        if (name === 'Investmentaccount') {
            let temp = this.props.getInvestmentOpeningBalance.data.filter(d => d.investmentaccount === value)[0];
            this.setState({
                Investmentaccount: value,
                amount: temp ? temp.amount : 0,
                madereceived: 'inward',
                amounttype: temp ? temp.amounttype : this.state.amounttype
            })
        }
        else {
            this.setState({
                [name]: value
            })
        }
    }
    render() {
        if (!cookie.load('userservice')) return <Redirect to="/" />;
        else {
            if ((this.props.updateInvestmentOpeningMessage.loading === true) || (this.props.getInvestmentOpeningBalance.loading === true) || (this.props.logoutMessage.loading === true) || (this.props.getInvestmentAccount.loading === true)) {
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div className="mt-3 d-flex justify-content-center">
                            <button className="btn btn-danger">Loading... </button>
                        </div>
                    </div>
                );
            }
            else if ((this.props.getInvestmentOpeningBalance.error !== '') || (this.props.getInvestmentAccount.error !== '')) {
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div className="mt-3 d-flex justify-content-center">
                            <button className="btn btn-danger">Error. Please Refresh and Try Again</button>
                        </div>
                    </div>
                );
            }
            else {
                let business = this.props.businessList.data.filter(b => b.businessid === this.props.businessid)[0];
                let fy = business.financial.filter(f => f.fyid === this.props.fyid)[0].financialyear;
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div style={{ minHeight: "93vh", display: "flex", flexDirection: "column" }}>
                            <div className="container" style={{ flex: "1" }}>
                                <h6 className="text-center pl-2 pr-2 pt-4" style={{ lineHeight: "1" }}><strong>{business.businessname} {fy}</strong></h6>
                                <h6 className="text-center pl-2 pr-2 mb-4" style={{ lineHeight: "1" }}>{business.alias ? "(" + business.alias + ")" : ''}</h6>
                                <h6 className="text-center pl-2 pr-2 col-12"><strong><u>{business.businesstype === 'partnership' ? "Partners Account Opening Balance" : ""}{business.businesstype === 'ltd' ? "Share Holders Account Opening Balance" : ""}{business.businesstype === 'propritorship' ? "Opening Balance - Capital Account (Propritor)" : ""}</u></strong></h6>
                                <div className="row">
                                    <LocalForm className="small col-12" onSubmit={this.handleSubmit}>
                                        <div className="container mt-4 d-flex justify-content-center">
                                            <div class="row col-12 col-md-4">
                                                {business.businesstype === 'partnership' ? <div><div className="form-group mb-1 col-12">
                                                    <div className="form-group mb-1 col-12">
                                                        <label className="col-6 p-0" ><input type="radio" name="amounttype" id="amounttype" value="payable" checked={this.state.amounttype === "payable" ? true : false} onChange={this.handleInputChange} /> Payable To Partnership Co.</label>&nbsp;&nbsp;&nbsp;&nbsp;
                                                <label className="col-5 p-0"><input type="radio" name="amounttype" id="amounttype" value="deposited" checked={this.state.amounttype === "deposited" ? true : false} onChange={this.handleInputChange} /> Deposited WIth Partnership Co.</label>
                                                    </div>
                                                </div><div className="form-group mb-1 col-12">
                                                    <label htmlFor="Investmentaccount" className="mb-0 muted-text"><strong>{business.businesstype === 'partnership' ? "Partner Account" : ''}<span className="text-danger">*</span></strong></label>
                                                    <Control.select model=".Investmentaccount" className="form-control form-control-sm" id="Investmentaccount" name="Investmentaccount" onChange={this.handleInputChange} value={this.state.Investmentaccount}>
                                                        <option value="">Partner Account</option>
                                                        {this.props.getInvestmentAccount.data.map(d => <option value={d.id + "..." + d.investmentaccount}>{d.investmentaccount}</option>
                                                        )}
                                                    </Control.select>
                                                </div></div> : ''}
                                                {business.businesstype === 'ltd' ? <div className="form-group mb-1 col-12">
                                                    <label htmlFor="Investmentaccount" className="mb-0 muted-text"><strong>Share Holders Account<span className="text-danger">*</span></strong></label>
                                                    <Control.select model=".Investmentaccount" className="form-control form-control-sm" id="Investmentaccount" name="Investmentaccount" onChange={this.handleInputChange} value={this.state.Investmentaccount}>
                                                        <option value="">Share Holders Account</option>
                                                        {this.props.getInvestmentAccount.data.map(d => <option value={d.id + "..." + d.investmentaccount}>{d.investmentaccount}</option>
                                                        )}
                                                    </Control.select>
                                                </div> : ''}
                                                {this.state.Investmentaccount !== '' || business.businesstype === 'propritorship' ? <div className="form-group mb-1 col-12">
                                                    <label htmlFor="amount" className="mb-0 muted-text"><strong>Opening Value Amount</strong><span className="text-danger">*</span></label>
                                                    <Control.text model=".amount" className="form-control form-control-sm" id="amount" name="amount" onChange={this.handleInputChange} value={this.state.amount} />
                                                </div> : ''}
                                                {this.state.Investmentaccount !== ''  || business.businesstype === 'propritorship' ? <div className="form-group mb-1 col-12">
                                                    <button type="submit" className="btn btn-success mt-1 btn-sm small" >Update</button>
                                                </div> : ''}
                                            </div>
                                        </div>
                                    </LocalForm>
                                </div>
                            </div>
                            <Footer />
                        </div>
                    </div>
                )
            }
        }
    }
}

export default InvestmentOpeningBalance;