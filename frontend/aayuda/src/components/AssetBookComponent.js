import React, { Component } from 'react';
import cookie from 'react-cookie';
import { Redirect } from 'react-router-dom';
import { Header } from './HeaderComponent';
import { LocalForm, Control } from 'react-redux-form';
import { Footer } from './Footer';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTrash } from '@fortawesome/free-solid-svg-icons';

const DataDepreciation = function ({ data, deleteEntry, businessid, fyid, i, state }) {
    let dataObj = {
        fyid: data.fyid,
        asset: data.asset
    }
    let cancel = function () {
        if (window.confirm("Delete Entry")) deleteEntry(dataObj, businessid, fyid, 'depreciation');
    }
    return (
        <div><div className="d-flex small mb-1 p-1 d-md-none d-lg-none" style={i % 2 === 0 ? { backgroundColor: "rgb(181, 253, 233)" } : {}}>
            <div className="col-5 col-md-5 p-1">
                <div className="col-12 col-md-6 p-0"><strong>{data.depreciateappreciate === 'appreciate' ? 'Appreciation' : ''}{data.depreciateappreciate === 'depreciate' ? 'Depreciation' : ''}</strong></div>
            </div>
            <div className="col-3 col-md-6 p-0 text-right">{data.depreciateappreciate === 'appreciate' ? parseFloat(data.amount).toFixed(2) : ''}</div>
            <div className="col-3 col-md-1 p-0 text-right">{data.depreciateappreciate === 'depreciate' ? parseFloat(data.amount).toFixed(2) : ''}</div>
            <div className="col-1 col-md-1 p-0 d-flex justify-content-center">
                <div className="col-12 mt-2"><FontAwesomeIcon icon={faTrash} onClick={() => cancel()} size="lg" /></div>
            </div>
        </div>
            <div className="container d-none d-md-block small p-1" style={i % 2 === 0 ? { backgroundColor: "rgb(181, 253, 233)" } : {}}>
                <div className="d-flex">
                    <div className="col-1 p-0"><strong></strong></div>
                    <div className="col-1 p-0"><strong></strong></div>
                    <div className="col-4 p-0"><strong>{data.depreciateappreciate === 'appreciate' ? 'Appreciation' : ''}{data.depreciateappreciate === 'depreciate' ? 'Depreciation' : ''}</strong></div>
                    <div className="col-2 text-right">{data.depreciateappreciate === 'appreciate' ? parseFloat(data.amount).toFixed(2) : ''}</div>
                    <div className="col-3 text-right">{data.depreciateappreciate === 'depreciate' ? parseFloat(data.amount).toFixed(2) : ''}</div>
                    <div className="col-1"><FontAwesomeIcon icon={faTrash} onClick={() => cancel()} size="lg" /></div>
                </div>
            </div></div>
    );
}
const DataAsset = function ({ data, deleteEntry, businessid, fyid, i, type }) {
    let dataObj = {
        id: data.id,
        type: type
    }
    let cancel = function () {
        if (window.confirm("Delete Entry")) deleteEntry(dataObj, businessid, fyid, 'asset');
    }
    if (data.type === 'asset') {
        if (data.purchasefyid === fyid) {
            return (<div><div className="d-flex small mb-1 p-1 d-md-none d-lg-none" style={i % 2 === 0 ? { backgroundColor: "rgb(181, 253, 233)" } : {}}>
                <div className="col-5 col-md-5 p-1">
                    <div className="col-12 col-md-6 p-0"><strong>{data.purchasevoucherno}</strong></div>
                    <div className="col-12 col-md-6 p-0"><strong>{data.purchasedate}</strong></div>
                    <div className="col-12 col-md-6 p-0">To {data.vendorid.split("...")[1]}</div>
                    <div className="col-12 col-md-6 p-0">Asset Purchase</div>
                </div>
                <div className="col-3 col-md-6 p-0 text-right">{parseFloat(data.purchasetotal).toFixed(2)}</div>
                <div className="col-3 col-md-1 p-0"></div>
                <div className="col-1 col-md-1 p-0 d-flex justify-content-center">
                    <div className="col-12 mt-2"><FontAwesomeIcon icon={faTrash} onClick={() => cancel()} size="lg" /></div>
                </div>
            </div>
                <div className="container d-none d-md-block small p-1" style={i % 2 === 0 ? { backgroundColor: "rgb(181, 253, 233)" } : {}}>
                    <div className="d-flex">
                        <div className="col-1 p-0">{data.purchasedate}</div>
                        <div className="col-1 p-0">{data.purchasevoucherno}</div>
                        <div className="col-4 p-0"><div><strong>To {data.vendorid.split("...")[1]}(Asset Purchase)</strong></div><div>(Asset Purchase)</div></div>
                        <div className="col-2 text-right">{parseFloat(data.purchasetotal).toFixed(2)}</div>
                        <div className="col-3"></div>
                        <div className="col-1"><FontAwesomeIcon icon={faTrash} onClick={() => cancel()} size="lg" /></div>
                    </div>
                </div></div>)
        }
        if (data.soldfyid === fyid) {
            return (<div><div className="d-flex small mb-1 p-1 d-md-none d-lg-none" style={i % 2 === 0 ? { backgroundColor: "rgb(181, 253, 233)" } : {}}>
                <div className="col-5 col-md-5 p-1">
                    <div className="col-12 col-md-6 p-0"><strong>{data.soldvoucherno}</strong></div>
                    <div className="col-12 col-md-6 p-0"><strong>{data.solddate}</strong></div>
                    <div className="col-12 col-md-6 p-0">By {data.customerid === 'cash' ? "Cash" : data.customerid.split("...")[1]}</div>
                    <div className="col-12 col-md-6 p-0">Asset Sold</div>
                </div>
                <div className="col-3 col-md-6 p-0"></div>
                <div className="col-3 col-md-1 p-0 text-right">{parseFloat(data.soldtotal).toFixed(2)}</div>
                <div className="col-1 col-md-1 p-0 d-flex justify-content-center">
                    <div className="col-12 mt-2"><FontAwesomeIcon icon={faTrash} onClick={() => cancel()} size="lg" /></div>
                </div>
            </div>
                <div className="container d-none d-md-block small p-1" style={i % 2 === 0 ? { backgroundColor: "rgb(181, 253, 233)" } : {}}>
                    <div className="d-flex">
                        <div className="col-1 p-0"><strong>{data.solddate}</strong></div>
                        <div className="col-1 p-0"><strong>{data.soldvoucherno}</strong></div>
                        <div className="col-4 p-0"><strong>By {data.customerid === 'cash' ? "Cash" : data.customerid.split("...")[1]}</strong></div>
                        <div className="col-4 p-0"><strong>Asset Sold</strong></div>
                        <div className="col-2"><strong></strong></div>
                        <div className="col-3 text-right">{parseFloat(data.soldtotal).toFixed(2)}</div>
                        <div className="col-1"><FontAwesomeIcon icon={faTrash} onClick={() => cancel()} size="lg" /></div>
                    </div>
                </div></div>)
        }
    }
}
const DataDisplay = function ({ getAssetAccount, state, getAssetOpeningBalance, fyid, getDepreciation, deleteEntry, businessid, }) {
    let assetdata = getAssetAccount.data.filter(d => d.id === state.assetaccount);
    let depreciationdata = getDepreciation.data.filter(d => d.asset === state.assetaccount);
    let assetopeningbalance = getAssetOpeningBalance.data.filter(d => d.assetaccount === state.assetaccount)[0];
    let debit = assetopeningbalance ? assetopeningbalance.amount : 0, credit = 0;
    let datarow = [], data = [];
    if (assetdata[0].sold === "1") {
        data = data.concat(depreciationdata);
        data = data.concat(assetdata);
    }
    else {
        data = data.concat(assetdata);
        data = data.concat(depreciationdata);
    }
    datarow.push(<div className="d-flex small mt-4 p-1 d-md-none d-lg-none">
        <div className="col-5 col-md-5 p-1">
            <div className="col-12 col-md-6 p-0 "></div>
        </div>
        <div className="col-3 col-md-6 p-0 pt-1 text-right"><strong>Debit</strong></div>
        <div className="col-3 col-md-1 pt-1 text-right"><strong>Credit</strong></div>
    </div>);
    datarow.push(
        <div className="container small mt-3 d-none d-md-block p-0">
            <div className="d-flex">
                <div className="col-1 p-0"><strong>Date</strong></div>
                <div className="col-1 p-0"><strong>Voucher No</strong></div>
                <div className="col-4 p-0"><strong>Particulars</strong></div>
                <div className="col-2 text-right"><strong>Debit</strong></div>
                <div className="col-3 text-right"><strong>Credit</strong></div>
                <div className="col-1"></div>
            </div>
        </div>
    );
    let i = 2;
    if (assetopeningbalance) {
        datarow.push(<div className="d-flex small mt-4 p-1 d-md-none d-lg-none" style={i % 2 === 0 ? { backgroundColor: "rgb(181, 253, 233)" } : {}}>
            <div className="col-5 col-md-5 p-1">
                <div className="col-12 col-md-6 p-0 "><strong>Apr Opening Bal</strong></div>
            </div>
            <div className="col-3 col-md-6 p-0 pt-1 text-right">{parseFloat(debit).toFixed(2)}</div>
            <div className="col-3 col-md-1 pt-1"></div>
        </div>)
        datarow.push(
            <div className="container small mt-3 d-none d-md-block p-1 " style={i % 2 === 0 ? { backgroundColor: "rgb(181, 253, 233)" } : {}}>
                <div className="d-flex">
                    <div className="col-2 p-0">Apr Opening Bal</div>
                    <div className="col-4 p-0"><strong></strong></div>
                    <div className="col-2 text-right">{parseFloat(debit).toFixed(2)}</div>
                    <div className="col-3"></div>
                    <div className="col-1"></div>
                </div>
            </div>
        )
        i++;
    }
    data.forEach(data => {
        if (data.type === 'asset') {
            if (data.purchasefyid === fyid) {
                datarow.push(<DataAsset data={data} deleteEntry={deleteEntry} businessid={businessid} fyid={fyid} i={i} type="purchase" />)
                debit = debit + parseFloat(data.amount);
                i = i + 1;
            }
            if (data.soldfyid === fyid) {
                datarow.push(<DataAsset data={data} deleteEntry={deleteEntry} businessid={businessid} fyid={fyid} i={i} type="sold" />)
                credit = credit + parseFloat(data.soldtotal);
                i = i + 1;
            }
        }
        else if (data.type === 'depreciation') {
            datarow.push(<DataDepreciation data={data} deleteEntry={deleteEntry} businessid={businessid} fyid={fyid} i={i} state={state} />)
            if (data.depreciateappreciate === 'depreciate') credit = credit + parseFloat(data.amount);
            else debit = debit + parseFloat(data.amount);
            i = i + 1;
        }
    })
    datarow.push(
        <div>
            <div className="d-flex small d-md-none mt-1">
                <div className="col-5 p-1"><strong></strong></div>
                <div className="col-3 p-0 border-top border-dark text-right">{parseFloat(debit).toFixed(2)}</div>
                <div className="col-3 p-0 border-top border-dark text-right">{parseFloat(credit).toFixed(2)}</div>
            </div>
            <div className="container small mt-2 d-none d-md-block p-0">
                <div className="d-flex">
                    <div className="col-1 p-0"><strong></strong></div>
                    <div className="col-1 p-0"><strong></strong></div>
                    <div className="col-4 p-0"></div>
                    <div className="col-2 border-top border-dark text-right">{parseFloat(debit).toFixed(2)}</div>
                    <div className="col-3 border-top border-dark text-right">{parseFloat(credit).toFixed(2)}</div>
                    <div className="col-1"></div>
                </div>
            </div>
        </div>
    )
    {
        assetdata[0].sold === "1" ? credit - debit > 0 ? datarow.push(
            <div>
                <div className="d-flex small d-md-none mt-1">
                    <div className="col-5 p-1"><strong>Profit</strong></div>
                    <div className="col-3 p-0 border-top border-dark text-right">{parseFloat(credit - debit).toFixed(2)}</div>
                    <div className="col-3 p-0"><strong></strong></div>
                </div>
                <div className="container small mt-2 d-none d-md-block p-0">
                    <div className="d-flex">
                        <div className="col-1 p-0"><strong></strong></div>
                        <div className="col-1 p-0"><strong></strong></div>
                        <div className="col-4 p-0"><strong>Profit</strong></div>
                        <div className="col-2 border-top border-dark text-right">{parseFloat(credit - debit).toFixed(2)}</div>
                        <div className="col-3"></div>
                        <div className="col-1"></div>
                    </div>
                </div>
            </div>
        ) : datarow.push(
            <div>
                <div className="d-flex small d-md-none mt-1">
                    <div className="col-5 p-1"><strong>Loss</strong></div>
                    <div className="col-3 p-0"><strong></strong></div>
                    <div className="col-3 p-0 border-top border-dark text-right">{parseFloat(debit - credit).toFixed(2)}</div>
                </div>
                <div className="container small mt-2 d-none d-md-block p-0">
                    <div className="d-flex">
                        <div className="col-1 p-0"><strong></strong></div>
                        <div className="col-1 p-0"><strong></strong></div>
                        <div className="col-4 p-0"><strong>Loss</strong></div>
                        <div className="col-2"></div>
                        <div className="col-3 border-top border-dark text-right">{parseFloat(debit - credit).toFixed(2)}</div>
                        <div className="col-1"></div>
                    </div>
                </div>
            </div>
        ) : datarow.push(
            <div>
                <div className="d-flex small d-md-none mt-1">
                    <div className="col-5 p-1"><strong>Closing value</strong></div>
                    <div className="col-3 p-0"><strong></strong></div>
                    <div className="col-3 p-0 border-top border-dark text-right">{parseFloat(debit - credit).toFixed(2)}</div>
                </div>
                <div className="container small mt-2 d-none d-md-block p-0">
                    <div className="d-flex">
                        <div className="col-1 p-0"><strong></strong></div>
                        <div className="col-1 p-0"><strong></strong></div>
                        <div className="col-4 p-0"><strong>Closing Value</strong></div>
                        <div className="col-2 border-top border-dark text-right">{credit - debit > 0 ? parseFloat(credit - debit).toFixed(2) : ''}</div>
                        <div className="col-3 border-top border-dark text-right">{debit - credit > 0 ? parseFloat(debit - credit).toFixed(2) : ''}</div>
                        <div className="col-1"></div>
                    </div>
                </div>
            </div>
        )
    }
    return datarow;
}
class AssetBook extends Component {
    constructor(props) {
        super(props);
        this.state = {
            assetaccount: '',
            assetname: '',
            assetdetail: ''
        }
        window.onload = () => {
            this.props.resetAssetOpeningBalance();
            this.props.refreshAssetAccountState();
            this.props.refreshDepreciationState();
        }
    }
    componentDidMount() {
        if (this.props.getAssetAccount.message === 'initial') this.props.getAssetAccountList(this.props.businessid);
        if (this.props.getAssetOpeningBalance.message === 'initial') this.props.getAssetOpeningBalanceList(this.props.businessid, this.props.fyid);
        if (this.props.getDepreciation.message === 'initial') this.props.getDepreciationList(this.props.businessid, this.props.fyid);
        if (this.props.deleteEntryMessage.message === "success") {
            alert("Entry Deleted Successfully");
            this.props.resetDeleteEntry();
        }
        if (this.props.deleteEntryMessage.error !== '') {
            alert("Error Deleting Entry - Please Try Again");
            this.props.resetDeleteEntry();
        }
    }
    handleInputChange = (event) => {
        let target = event.target;
        let name = target.name;
        let value = target.value;
        this.setState({
            ...this.state,
            assetaccount: value.split("...")[0],
            assetname: value.split("...")[1],
            assetdetail: value
        })
    }
    render() {
        if (!cookie.load('userservice')) return <Redirect to="/" />;
        else {
            if ((this.props.getAssetAccount.loading === true) || (this.props.logoutMessage.loading === true) || (this.props.getAssetOpeningBalance.loading === true) || (this.props.getDepreciation.loading == true) || (this.props.deleteEntryMessage.loading === true)) {
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div className="mt-3 d-flex justify-content-center">
                            <button className="btn btn-danger">Loading... </button>
                        </div>
                    </div>
                );
            }
            else if ((this.props.getAssetOpeningBalance.error !== '') || (this.props.getAssetAccount.error !== '') || (this.props.getDepreciation.error !== '')) {
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div className="mt-3 d-flex justify-content-center">
                            <button className="btn btn-danger">Error.. Please Refresh and Try Agin </button>
                        </div>
                    </div>
                );
            }
            else {
                let business = this.props.businessList.data.filter(b => b.businessid === this.props.businessid)[0];
                let fy = business.financial.filter(f => f.fyid === this.props.fyid)[0].financialyear;
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div style={{ minHeight: "93vh", display: "flex", flexDirection: "column" }}>
                            <div className="container" style={{ flex: "1" }}>
                                <h6 className="text-center pl-2 pr-2 pt-4" style={{ lineHeight: "1" }}><strong>{business.businessname} {fy}</strong></h6>
                                <h6 className="text-center pl-2 pr-2 mb-4" style={{ lineHeight: "1" }}>{business.alias ? "(" + business.alias + ")" : ''}</h6>
                                <h6 className="text-center pl-2 pr-2 mb-2"><strong><u>Asset Book</u></strong></h6>
                                <div className="row">
                                    <LocalForm className="small col-12" onSubmit={this.handleSubmit}>
                                        <div className="container mt-4 d-flex justify-content-center">
                                            <div class="row col-12 col-md-4">
                                                <div className="form-group mb-1 col-12">
                                                    <label htmlFor="assetdetail" className="mb-0 muted-text"><strong>Asset Account Name:<span className="text-danger">*</span></strong></label>
                                                    <Control.select model=".assetdetail" className="form-control form-control-sm" id="assetdetail" name="assetdetail" onChange={this.handleInputChange} value={this.state.assetdetail}>
                                                        <option value="">Select Asset Account</option>
                                                        {this.props.getAssetAccount.data.map(b => {
                                                            if ((!b.sold) || (b.sold === "1") && (new Date(b.solddate) >= new Date("01 Apr " + fy.split("-")[0]))) return <option value={b.id + "..." + b.assetaccount}>{b.assetaccount}</option>
                                                        })}
                                                    </Control.select>
                                                </div>
                                            </div>
                                        </div>
                                    </LocalForm>
                                </div>
                                {this.state.assetaccount !== '' ? <DataDisplay getAssetAccount={this.props.getAssetAccount} state={this.state} getAssetOpeningBalance={this.props.getAssetOpeningBalance} fyid={this.props.fyid} getDepreciation={this.props.getDepreciation} deleteEntry={this.props.deleteEntry} businessid={this.props.businessid} /> : ''}
                            </div>
                            <Footer />
                        </div>
                    </div>
                )
            }
        }
    }
}

export default AssetBook;