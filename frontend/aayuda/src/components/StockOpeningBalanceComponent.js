import React, { Component } from 'react';
import cookie from 'react-cookie';
import { Redirect } from 'react-router-dom';
import { LocalForm, Control } from 'react-redux-form';
import { Header } from './HeaderComponent';
import { Footer } from './Footer';

class StockOpeningBalance extends Component {
    constructor(props) {
        super(props);
        this.state = ({
            name: '',
            unit: '',
            itemid: '',
            stock: 0
        })
        window.onload = () => {
            this.props.refreshItemState();
            this.props.resetStockOpeningBalance();
        }
    }
    handleSubmit = () => {
        if (this.state.name === '') alert("Item Not Selected");
        else {
            if ((this.state.stock === '') || (this.state.stock < 0) || (isNaN(parseInt(this.state.stock)))) alert(" Stock Opening Balance Muste Be Greater Than Equal To 0 ");
            else this.props.updateStockOpeningBalance(this.state, this.props.businessid, this.props.fyid);
        }
    }
    componentDidMount() {
        if (this.props.updateStockOpeningMessage.error !== '') {
            alert("Error Updating Data");
            this.setState({
                ...this.props.updateStockOpeningMessage.state
            })
            this.props.refreshUpdateStockOpeningBalance();
        }
        if (this.props.updateStockOpeningMessage.message === 'success') {
            alert("Data Updated Successfully");
            this.setState({
                ...this.props.updateStockOpeningMessage.state
            })
            this.props.refreshUpdateStockOpeningBalance();
        }
        if (this.props.getStockOpeningBalance.message === 'initial') this.props.getStockOpeningBalanceList(this.props.businessid, this.props.fyid);
        if (this.props.logoutMessage.error !== '') {
            alert("Error Logging Out. Please Try Again");
            this.props.resetLogoutMessage();
        }
        if (this.props.getItem.message === 'initial') this.props.getItemList(this.props.businessid);
    }
    handleInputChange = (event) => {
        let target = event.target;
        let name = target.name;
        let value = target.value;
        if (name === 'stock') {
            this.setState({
                ...this.state,
                stock: value
            })
        }
        else {
            let item = this.props.getStockOpeningBalance.data.filter(d => d.itemid === value.split("...")[0])[0];
            this.setState({
                name: value,
                itemid: value.split("...")[0],
                unit: value.split("...")[1],
                stock: item ? item.stock : 0
            })
        }
    }
    render() {
        if (!cookie.load('userservice')) return <Redirect to="/" />;
        else {
            if ((this.props.updateStockOpeningMessage.loading === true) || (this.props.getStockOpeningBalance.loading === true) || (this.props.logoutMessage.loading === true)) {
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div className="mt-3 d-flex justify-content-center">
                            <button className="btn btn-danger">Loading... </button>
                        </div>
                    </div>
                );
            }
            else if ((this.props.getStockOpeningBalance.error !== '')) {
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div className="mt-3 d-flex justify-content-center">
                            <button className="btn btn-danger">Error. Please Refresh and Try Again</button>
                        </div>
                    </div>
                );
            }
            else {
                let business = this.props.businessList.data.filter(b => b.businessid === this.props.businessid)[0];
                let fy = business.financial.filter(f => f.fyid === this.props.fyid)[0].financialyear;
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div style={{ minHeight: "93vh", display: "flex", flexDirection: "column" }}>
                            <div className="container" style={{ flex: "1" }}>
                                <h6 className="text-center pl-2 pr-2 pt-4" style={{ lineHeight: "1" }}><strong>{business.businessname} {fy}</strong></h6>
                                <h6 className="text-center pl-2 pr-2 mb-4" style={{ lineHeight: "1" }}>{business.alias ? "(" + business.alias + ")" : ''}</h6>
                                <h6 className="text-center pl-2 pr-2 col-12"><strong><u>Item - Stock Opening Balance</u></strong></h6>
                                <div className="row mt-4 small justify-content-center mb-3" >
                                    <LocalForm className="container col-12 col-md-5" onSubmit={this.handleSubmit}>
                                        <div className="form-group mb-1 col-12">
                                            <label htmlFor="itemid" className="mb-0 muted-text"><strong>Select Item:<span className="text-danger">*</span></strong></label>
                                            <Control.select model=".itemid" className="form-control form-control-sm" id="itemid" name="itemid" onChange={this.handleInputChange} value={this.state.name}>
                                                <option value="">Select Item</option>
                                                {this.props.getItem.data.map(b => <option value={b.itemid + "..." + b.billingunit}>{b.alias ? b.alias : b.itemname}</option>)}
                                            </Control.select>
                                        </div>
                                        {this.state.name !== '' ? <div><div className="form-group mb-1 col-12">
                                            <label htmlFor="stock" className="mb-0 muted-text"><strong>Opening Stock in Units ({this.state.unit})</strong><span className="text-danger">*</span></label>
                                            <Control.text model=".stock" className="form-control form-control-sm" id="stock" name="stock" onChange={this.handleInputChange} value={this.state.stock} />
                                        </div><div className="form-group mb-1 col-12">
                                                <button type="submit" className="btn btn-success mt-1 btn-sm small" >Update</button>
                                            </div></div> : ''}
                                    </LocalForm>
                                </div>
                            </div>
                            <Footer />
                        </div>
                    </div>
                )
            }
        }
    }
}

export default StockOpeningBalance;