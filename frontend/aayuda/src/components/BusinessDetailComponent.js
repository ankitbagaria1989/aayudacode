import React, { Component, useState } from 'react';
import cookie from 'react-cookie';
import { Redirect, Link } from 'react-router-dom';
import { style } from '../sylesheet/stylesheet';
import { Button, Collapse } from 'reactstrap';
import { Form, Errors, Control } from 'react-redux-form';
import { resetaddFinancialYearMessage } from '../redux/actionCreators';
import { Header } from './HeaderComponent';
import { Footer } from './Footer';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEdit, faAngleDoubleDown } from '@fortawesome/free-solid-svg-icons';


const required = (val) => val && val.length;
const validFinancialYear = (val) => /^[0-9]{4}-[0-9]{4}$/i.test(val);
const ErrorComponent = function ({ children }) {
    return (
        <div style={{ lineHeight: "1" }}>{children}</div>
    )
}
const FinancialYear = function (props) {
    let financial = props.financial;
    if ((financial.length === 1) && ((financial[0].financialyear === null) || (financial[0].financialyear === undefined))) {
        return (
            <div><button className="btn btn-sm btn-light p-2" >No Financial Year Configured yet</button></div>
        )
    }
    else {
        let arr = [];
        for (let i = 0; i < financial.length; i++) {
            arr.push(<div className="mt-1 col-4 p-0">
                <Link to={`/business/businessdetail/businesschoice/${props.businessid}/${financial[i].fyid}`}><button className="btn btn-sm p-2" style={{ backgroundColor: "#416ff9", color: "white" }}>{financial[i].financialyear}</button></Link>
            </div>);
        }
        return arr;
    }
}
const BusinessCard = function ({ businessList, handleSubmit, addFinancialYearMessage, businessid, resetaddFinancialYearMessage }) {
    let [isOpen, setIsOpen] = useState(false);
    let toggle = () => setIsOpen((!isOpen));
    if (addFinancialYearMessage.error !== '') {
        isOpen = true;
    }
    let business = businessList.data.filter(buss => buss.businessid === businessid)[0];
    if (business) {
        return (
            <div className="col-12 col-md-5">
                <div className="pr-0 pl-0">
                    <div className="">
                        <div className="card-title mb-0" style={style.lineHeight1, { fontSize: "1rem", fontWeight: "600" }}>{business.businessname.toUpperCase()}</div>
                        <div className="card-title mb-0" style={style.lineHeight1}>{business.alias ? "(" + business.alias + ")" : ''}</div>
                        <div className="card-sub-title small" style={style.lineHeight1}><span className="text-muted"><strong>GSTIN:</strong></span><strong>{business.gstin.toUpperCase()}</strong></div>
                        <div className="card-sub-title small" style={style.lineHeight1}><span className="text-muted"><strong>PAN:</strong></span><strong>{business.pan ? business.pan.toUpperCase() : ''}</strong></div>
                        <div className="card-text text-wrap small" style={style.lineHeight1}><span className="text-muted"><strong>ADDRESS:</strong></span><strong>{business.address.toUpperCase()}</strong></div>
                        <div className="card-text text-wrap small" style={style.lineHeight1}><span className="text-muted"><strong>PINCODE:</strong></span><strong>{business.pincode}</strong></div>
                        <div className="card-text text-wrap small" style={style.lineHeight1}><span className="text-muted"><strong>STATE CODE :</strong></span><strong> {business.gstin.substring(1, 3)} </strong><span className="text-muted"><strong>STATE :</strong></span> <strong>{business.state.toUpperCase()} </strong></div>
                        <div className="card-text text-wrap small" style={style.lineHeight1}><span className="text-muted"><strong>MOBILE :</strong></span> <strong>{business.mobile.toUpperCase()}</strong></div>
                        <div className="card-text text-wrap small" style={style.lineHeight1}><span className="text-muted"><strong>EMAIL : </strong></span><strong>{business.email.toLowerCase()}</strong></div>
                        <div className="card-text text-wrap row mb-2 small" style={style.lineHeight1}><div className="col-10"><span className="text-muted"><strong>LANDLINE : </strong></span><span><strong>{business.landline}</strong></span></div><span className="col-2 text-right"><Link to={`/business/businessdetail/businesschange/${business.businessid}`} style={{ color: "#3e3737" }}>
                            <FontAwesomeIcon icon={faEdit} size="lg" /></Link></span></div>
                        <div className="row mt-0 ml-1"><FinancialYear financial={business.financial} businessid={business.businessid} /></div>
                        <div className="mt-3 d-flex justify-content-center">
                            <div className="p-0 col-md-12">
                                <Button className="mb-1 mt-1 col-md-12 btn-sm" style={{ backgroundColor: "#fff", color: "#28a745", border: "1px solid #28a745" }} onClick={toggle}><div className="d-flex"><span className="col-11">Add a New FY</span><span className="col-1 pr-0 text-align-right"><FontAwesomeIcon size="sm" icon={faAngleDoubleDown} /></span></div></Button>
                                <Collapse isOpen={isOpen}>
                                    <Form model="addFinancialYear" className="mt-1 card shadow p-2 mb-2" onSubmit={((values) => handleSubmit(values, business.businessid))}>
                                        <div className="form-group col-md-8 mb-1">
                                            <div className="row">
                                                <Control.text model=".fy" className="form-control form-control-sm" id="fy" name="fy" placeholder="2017-2018 etc.." validators={{ required, validFinancialYear }} />
                                                <Errors className="text-danger small" model=".fy" show="touched" messages={{
                                                    required: "Field is mandatory",
                                                    validFinancialYear: "Must be in YYYY-YYYY format"
                                                }}
                                                    component={ErrorComponent}
                                                />
                                            </div>
                                            <div className="row mt-2"><button type="submit" className="btn btn-sm" style={{ backgroundColor: "#fff", border: "1px solid #28a745", color: "#28a745" }} ><strong>Add</strong></button></div>
                                        </div>
                                    </Form>
                                </Collapse>
                            </div>
                        </div>
                        <div className="d-flex justify-content-center">
                            <div className="p-0 col-md-12">
                                <Link to={`/business/businessdetail/customer/${business.businessid}`}><Button className="mb-1 col-md-12 btn-sm" style={{ backgroundColor: "#fff", color: "#28a745", border: "1px solid #28a745" }}>Add/Edit Customer Account</Button></Link>
                            </div>
                        </div>
                        <div className="d-flex justify-content-center">
                            <div className="p-0 col-md-12">
                                <Link to={`/business/businessdetail/vendor/${business.businessid}`}><Button className="mb-1 col-md-12 btn-sm" style={{ backgroundColor: "#fff", color: "#28a745", border: "1px solid #28a745" }}>Add/Edit Vendor Account</Button></Link>
                            </div>
                        </div>
                        <div className="d-flex justify-content-center">
                            <div className="p-0 col-md-12">
                                <Link to={`/business/businessdetail/bankdetail/${business.businessid}`}><Button className="mb-1 col-md-12 btn-sm" style={{ backgroundColor: "#fff", color: "#28a745", border: "1px solid #28a745" }}>Add/Edit Bank Account</Button></Link>
                            </div>
                        </div>
                        <div className="d-flex justify-content-center">
                            <div className="p-0 col-md-12">
                                <Link to={`/business/businessdetail/item/${business.businessid}`}><Button className="col-md-12 btn-sm mb-1" style={{ backgroundColor: "#fff", color: "#28a745", border: "1px solid #28a745" }} >Add/Edit Item</Button></Link>
                            </div>
                        </div>
                        <div className="d-flex justify-content-center">
                            <div className="p-0 col-md-12">
                                <Link to={`/business/businessdetail/expensehead/${business.businessid}`}><Button className="col-md-12 btn-sm" style={{ backgroundColor: "#fff", color: "#28a745", border: "1px solid #28a745" }} >Add/Edit Expense Account</Button></Link>
                            </div>
                        </div>
                        <div className="d-flex justify-content-center">
                            <div className="p-0 col-md-12 mt-1">
                                <Link to={`/business/businessdetail/incomehead/${business.businessid}`}><Button className="col-md-12 btn-sm" style={{ backgroundColor: "#fff", color: "#28a745", border: "1px solid #28a745" }} >Add/Edit Income (Other Sources) Account</Button></Link>
                            </div>
                        </div>
                        <div className="d-flex justify-content-center">
                            <div className="p-0 col-md-12 mt-1">
                                <Link to={`/business/businessdetail/loanaccount/${business.businessid}`}><Button className="col-md-12 btn-sm" style={{ backgroundColor: "#fff", color: "#28a745", border: "1px solid #28a745" }} >Add/Edit Loan Account</Button></Link>
                            </div>
                        </div>
                        <div className="d-flex justify-content-center">
                            <div className="p-0 col-md-12 mt-1">
                                <Link to={`/business/businessdetail/assetaccount/${business.businessid}`}><Button className="col-md-12 btn-sm" style={{ backgroundColor: "#fff", color: "#28a745", border: "1px solid #28a745" }} >Add/Edit Asset Account</Button></Link>
                            </div>
                        </div>
                        {(business.businesstype === 'partnership') ? <div className="d-flex justify-content-center">
                            <div className="p-0 col-md-12 mt-1">
                                <Link to={`/business/businessdetail/investmentaccount/${business.businessid}`}><Button className="col-md-12 btn-sm" style={{ backgroundColor: "#fff", color: "#28a745", border: "1px solid #28a745" }} >Add/Edit Partner Account</Button></Link>
                            </div>
                        </div> : ''}
                        {(business.businesstype === 'ltd') ? <div className="d-flex justify-content-center">
                            <div className="p-0 col-md-12 mt-1">
                                <Link to={`/business/businessdetail/investmentaccount/${business.businessid}`}><Button className="col-md-12 btn-sm" style={{ backgroundColor: "#fff", color: "#28a745", border: "1px solid #28a745" }} >Add/Edit ShareHolder Account</Button></Link>
                            </div>
                        </div> : ''}                        
                        <div className="d-flex justify-content-center">
                            <div className="p-0 col-md-12 mt-1 pb-4">
                                <Link to={`/business/businessdetail/employees/${business.businessid}`}><Button className="col-md-12 btn-sm" style={{ backgroundColor: "#fff", color: "#28a745", border: "1px solid #28a745" }} >Add/Edit Employees</Button></Link>
                            </div>
                        </div>

                    </div>
                </div>
                <div className="col-md-1"></div>
            </div>
        );
    }
    else return <div></div>
}
class BusinessDetail extends Component {
    handleSubmit = (values, businessid) => {
        let financialYear = values.fy;
        this.props.addFinancialYear(financialYear, businessid);
    }

    componentDidMount() {
        window.onload = () => {
            this.props.resetIndividualBusinessMessage();
        }
        if ((this.props.individualBusinessMessage.message === 'initial') && (this.props.individualBusinessMessage.loading === false)) this.props.getIndividualBusiness(this.props.businessid);
        if ((this.props.getCustomer.message === 'initial') && (this.props.getCustomer.loading === false)) this.props.getCustomerList(this.props.businessid);
        if ((this.props.getVendor.message === 'initial') && (this.props.getVendor.loading === false)) this.props.getVendorList(this.props.businessid);
        if ((this.props.getItem.message === 'initial') && (this.props.getItem.loading === false)) this.props.getItemList(this.props.businessid);
        if ((this.props.getExpenseHead.message === 'initial') && (this.props.getExpenseHead.loading === false)) this.props.getExpenseHeadList(this.props.businessid);
        if ((this.props.getIncomeHead.message === 'initial') && (this.props.getIncomeHead.loading === false)) this.props.getIncomeHeadList(this.props.businessid);
        if ((this.props.getBankDetail.message === 'initial') && (this.props.getBankDetail.loading === false)) this.props.getBankDetailList(this.props.businessid);
        if ((this.props.getEmployee.message === 'initial') && (this.props.getEmployee.loading === false)) this.props.getEmployeeList(this.props.businessid);
        if (this.props.getLoanAccount.message === 'initial') this.props.getLoanAccountList(this.props.businessid);
        if (this.props.getAssetAccount.message === 'initial') this.props.getAssetAccountList(this.props.businessid);
        if (this.props.getInvestmentAccount.message === 'initial') this.props.getInvestmentAccountList(this.props.businessid);
        
        if (this.props.salesDetail.message !== 'initial') this.props.resetSalesDetail();
        if (this.props.purchaseDetail.message !== 'initial') this.props.resetPurchaseDetail();
        if (this.props.getExpense.message !== 'initial') this.props.resetExpenseDetail();
        if (this.props.getPayment.message !== 'initial') this.props.resetPaymentDetail();
        if (this.props.getOpeningClosing.message !== 'initial') this.props.resetOpeningClosingDetail();
        if (this.props.getOpeningBalance.message !== 'initial') this.props.resetOpeningBalance();
        if (this.props.getBankEntry.message !== 'initial') this.props.resetBankEntryDetail();
        if (this.props.getCashInHand.message !== 'initial') this.props.resetCashInHand();
        if (this.props.getDebitCredit.message !== 'initial') this.props.refreshDebitCredit();
        if (this.props.getIncomeOS.message !== 'initial') this.props.resetIncomeOSDetail();
        if (this.props.getBankOpeningBalance.message !== 'initial') this.props.resetBankOpeningBalance();
        if (this.props.getInvestment.message !== 'initial') this.props.refreshInvestmentState();
        if (this.props.getLoanOpeningBalance.message !== 'initial') this.props.resetLoanOpeningBalance();
        if (this.props.getDepreciation.message !== 'initial') this.props.refreshDepreciationState();
        if (this.props.getAssetOpeningBalance.message !== 'initial') this.props.resetAssetOpeningBalance();
        if (this.props.getInvestmentOpeningBalance.message !== 'initial') this.props.resetInvestmentOpeningBalance();
        if (this.props.getTDSOpeningBalance.message !== 'initial') this.props.resetTDSOpeningBalance();
        if (this.props.getIncomeTaxOpeningBalance.message !== 'initial') this.props.resetIncomeTaxOpeningBalance();
        if (this.props.getEmployeeOpeningBalance.message !== 'initial') this.props.resetEmployeeOpeningBalance();
        if (this.props.addFinancialYearMessage.error !== '') {
            alert("Error : " + this.props.addFinancialYearMessage.error);
            this.props.resetaddFinancialYear();
        }
        else if (this.props.addFinancialYearMessage.message === "success") {
            alert("FinancialYear Added Successfully");
            this.props.resetaddFinancialYearMessage();
        }
        if (this.props.addFinancialYearMessage.message === "Already Registered") {
            alert("FinancialYear " + this.props.businessList.fy + " Already Registered");
            this.props.resetaddFinancialYearMessage();
        }
        if (this.props.logoutMessage.error !== '') {
            alert("Error Logging Out. Please Try Again");
            this.props.resetLogoutMessage();
        }
    }

    render() {
        if (!cookie.load('userservice')) return <Redirect to="/" />
        else {
            if ((this.props.individualBusinessMessage.loading === true) || (this.props.logoutMessage.loading === true) || (this.props.addFinancialYearMessage.loading === true)) {
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div className="mt-3 d-flex justify-content-center">
                            <button className="btn btn-danger">Loading... </button>
                        </div>
                    </div>
                );
            }
            else if (this.props.individualBusinessMessage.error !== "") {
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div className="mt-3 d-flex justify-content-center">
                            <button className="btn btn-danger">Error. Please Refresh and Try Again</button>
                        </div>
                    </div>
                );
            }
            else {
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div style={{ minHeight: "92vh", display: "flex", flexDirection: "column" }}>
                            <div className="container mt-3 d-flex justify-content-center pr-0 pl-0" style={{ flex: "1" }}>
                                <BusinessCard businessList={this.props.businessList} handleSubmit={this.handleSubmit} addFinancialYearMessage={this.props.addFinancialYearMessage} businessid={this.props.businessid} resetaddFinancialYearMessage={resetaddFinancialYearMessage} />
                            </div>
                            <Footer />
                        </div>
                    </div>
                );
            }
        }
    }
}
export default BusinessDetail;
