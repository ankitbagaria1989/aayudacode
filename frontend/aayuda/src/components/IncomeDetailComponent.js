import React, { Component } from 'react';
import cookie from 'react-cookie';
import { Redirect } from 'react-router-dom';
import { LocalForm, Control } from 'react-redux-form';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Header } from './HeaderComponent';
import { Footer } from './Footer';
import { faTrash, faFilePdf } from '@fortawesome/free-solid-svg-icons';
import { Page, Text, View, Document, pdf } from '@react-pdf/renderer';
import { pdfstyles } from '../sylesheet/stylesheet';
import { saveAs } from 'file-saver';

const Option = function ({ data }) {
    let arr = [];
    for (let i = -1; i < data.length; i++) {
        if (i === -1) arr.push(<option value=''>Select Head :</option>);
        else arr.push(<option value={data[i].incomeheadname}>{data[i].incomeheadname}</option>);
    }
    return arr;
}

let totalcredit = 0, totaldebit = 0;
const IncomeDataPage = function ({ data, y }) {
    let debit = 0, credit = 0;
    let returnrows = [];
    if (y > 1) {
        returnrows.push(
            <View style={pdfstyles.itemSectionLedger, { textAlign: "center" }}>
                <View style={pdfstyles.itemHeader}><Text style={{ width: '75px', textAlign: "left" }}></Text><Text style={{ width: '75px', textAlign: "left" }}></Text><Text style={{ width: '225px', textAlign: "left", fontFamily: "Helvetica-Bold" }}>...Carry Forward</Text><Text style={{ width: '100px', textAlign: "right" }}>{parseFloat(totaldebit).toFixed(2)}</Text><Text style={{ width: '100px', textAlign: "right" }}>{parseFloat(totalcredit).toFixed(2)}</Text></View>
            </View>
        )
        debit = totaldebit;
        credit = totalcredit;
    }
    for (let i = (y - 1) * 50; i < y * 50 && i < data.length; i++) {
        let d = data[i];
        credit = credit + parseFloat(d.amount);
        totalcredit = totalcredit + parseFloat(d.amount);
        returnrows.push(
            <View style={pdfstyles.itemSectionLedger, { textAlign: "center" }}>
                <View style={pdfstyles.itemHeader}><Text style={{ width: '75px', textAlign: "left" }}>{d.date}</Text><Text style={{ width: '75px', textAlign: "left" }}>{d.voucherno}</Text><View style={{ width: "225px" }}><Text style={{ width: '225px', textAlign: "left", fontFamily: "Helvetica-Bold" }}>{d.cash === "1" ? "By Cash" : "By " + d.businessname}</Text><Text style={{ width: '225px', textAlign: "left", paddingLeft: "15px" }}>{d.category + " " + d.type}</Text></View><Text style={{ width: '100px', textAlign: "right" }}></Text><Text style={{ width: '100px', textAlign: "right" }}>{parseFloat(d.amount).toFixed(2)}</Text></View>
            </View>
        )

    }
    if (y === Math.ceil(data.length / 50)) {
        returnrows.push(<View style={pdfstyles.itemSectionLedger, { fontFamily: "Helvetica-Bold", marginTop: "5px", paddingBottom: "2px", borderTop: "1px solid black", borderBottom: "1px solid black" }}>
            <View style={pdfstyles.itemHeader}><Text style={{ width: '75px', textAlign: "left" }}></Text><Text style={{ width: '75px', textAlign: "left" }}></Text><Text style={{ width: '225px', textAlign: "left" }}>Total Income Other Sources</Text><Text style={{ width: '100px', textAlign: "right" }}></Text><Text style={{ width: '100px', textAlign: "right" }}>{parseFloat(totalcredit - totaldebit).toFixed(2)}</Text></View>
        </View>
        )
    }
    else {
        returnrows.push(<View><View style={pdfstyles.itemSectionLedger, { fontFamily: "Helvetica-Bold", marginTop: "2px" }}>
            <View style={pdfstyles.itemHeader}><Text style={{ width: '75px', textAlign: "left" }}></Text><Text style={{ width: '75px', textAlign: "left" }}></Text><Text style={{ width: '225px', textAlign: "left" }}></Text><Text style={{ width: '100px', borderTop: "1px solid black", textAlign: "right" }}>{parseFloat(debit).toFixed(2)}</Text><Text style={{ width: '100px', borderTop: "1px solid black", textAlign: "right" }}>{parseFloat(credit).toFixed(2)}</Text></View>
        </View>
            <View style={pdfstyles.itemSectionLedger, { fontFamily: "Helvetica", marginTop: "5px", textAlign: "right" }}><Text></Text>...Continued</View></View>)
    }
    return returnrows;
}
const Report = function ({ business, data, l, n, period }) {
    let returnrows = [];
    for (let y = 1; y <= n; y++) {
        returnrows.push(<Page size="A4" style={pdfstyles.page}>
            {y === 1 ? <View><View style={pdfstyles.taxInvoiceHeader, { marginTop: "20px", textAlign: "center" }}>
                <View style={{ fontFamily: "Helvetica-Bold", width: "500px" }}><Text>{business.businessname}</Text></View>
                <View style={{ width: "500px" }}><Text>{business.address}</Text></View>
            </View>
                <View style={pdfstyles.taxInvoiceHeader, { marginTop: "10px", textAlign: "center" }}>
                    <View style={{ fontFamily: "Helvetica-Bold", textDecoration: "underline", width: "500px" }}><Text>Income (Other Sources) Ledger</Text></View>
                    <View style={{ width: "500px" }}><Text>{period}</Text></View>
                </View></View> : ''}
            <View style={pdfstyles.itemSectionLedger, { marginTop: "20px", marginBottom: "5px", fontFamily: "Helvetica-Bold", textAlign: "center", borderTop: "1px solid black", borderBottom: "1px solid black" }}>
                <View style={pdfstyles.itemHeader}><Text style={{ width: '75px', textAlign: "left" }}>Date</Text><Text style={{ width: '75px', textAlign: "left" }}>Voucher No</Text><Text style={{ width: '225px', textAlign: "left" }}>Particulars</Text><Text style={{ width: '100px', textAlign: "right" }}>Debit</Text><Text style={{ width: '100px', textAlign: "right" }}>Credit</Text></View>
            </View>
            <IncomeDataPage data={data} y={y} />
        </Page >)
    }
    return returnrows;
}
const IncomeLedger = function ({ data, business, fy, head }) {
    let period = fy;
    totalcredit = 0;
    totaldebit = 0;
    let l = data.length, i = 0, n = Math.ceil(l / 50), y = 0;
    return (
        <Document>
            <Report business={business} data={data} l={l} n={n} period={period} />
        </Document>
    )
}

const DataRow = function ({ data, getCustomer, businessid, businessList, deleteEntry, fyid, i }) {
    let cancel = function (segment) {
        if (window.confirm("Delete Entry")) deleteEntry(data.incomeid, businessid, fyid, segment);
    }
    return (
        <div>
            <div className=" d-flex small border-bottom mb-2 p-1 d-md-none d-lg-none" style={i % 2 === 0 ? { backgroundColor: "rgb(181, 253, 233)" } : {}}>
                <div className="col-5 col-md-5 p-1">
                    <div className="col-12 col-md-6 p-0"><strong>{data.voucherno}/{data.date}</strong></div>
                    <div className="col-12 col-md-6 p-0 " style={{ lineHeight: "1" }}>{data.cash === "1" ? "By Cash/" : "By " + data.customername}</div>
                    <div className="col-12 col-md-6 p-0 " style={{ lineHeight: "1" }}>{data.category + " " + data.type}</div>
                    <div className="col-12 col-md-2 p-0 " style={{ lineHeight: "1" }}>{data.description}</div>
                    {data.tds === '1' ? <div className="col-12 p-0 " style={{ lineHeight: "1" }}>{"TDS Deducted : " + data.tdsamount}</div> : ''}
                </div>
                <div className="col-3 p-0 pt-1 text-right">
                </div>
                <div className="col-3 p-0 pt-1 text-right">
                    <div className="col-12 col-md-2 p-0">{parseFloat(data.amount).toFixed(2)}</div>
                </div>
                <div className="col-1 col-md-1 p-0 d-flex justify-content-center">
                    <div className="col-12 mt-2"><FontAwesomeIcon icon={faTrash} onClick={() => cancel("income")} size="lg" /></div>
                </div>
            </div>
            <div className="container d-none d-md-block small mb-1 p-1" style={i % 2 === 0 ? { backgroundColor: "rgb(181, 253, 233)" } : {}}>
                <div className="d-flex">
                    <div className="col-1 p-0">{data.date}</div>
                    <div className="col-1 p-0">{data.voucherno}</div>
                    <div className="col-4 p-0">
                        <div className="col-12 p-0"><strong>{data.cash === "1" ? "By Cash" : "By " + data.customername}</strong></div>
                        <div className="col-12 p-0">{data.category + " " + data.type}</div>
                        <div className="col-12 p-0">{data.description}</div>
                        {data.tds === '1' ? <div className="col-12 p-0 " style={{ lineHeight: "1" }}>{"TDS Deducted : " + data.tdsamount}</div> : ''}
                    </div>
                    <div className="col-2 text-right"></div>
                    <div className="col-3 text-right">{parseFloat(data.amount).toFixed(2)}</div>
                    <div className="col-1 pt-1"><FontAwesomeIcon icon={faTrash} onClick={() => cancel("income")} size="lg" /></div>
                </div>
            </div>
        </div>
    )
}
const DisplayData = function ({ state, incomeDetail, getCustomer, businessid, businessList, deleteEntry, fyid }) {
    let returndata = [];
    let data = incomeDetail;
    if (state.headSelection !== '') data = data.filter(d => d.category === state.headSelection);
    let totalincome = 0;
    let temp = '';
    if (data.length > 0) {
        let i = 1;
        temp =
            <div>
                <div className="">
                    {data.map(d => {
                        i++;
                        totalincome = totalincome + parseFloat(d.amount);
                        return <DataRow data={d} getCustomer={getCustomer} businessid={businessid} businessList={businessList} deleteEntry={deleteEntry} fyid={fyid} i={i} />;
                    })}
                </div>
            </div>
        returndata.push(temp);
    }
    else if (data.length === 0) {
        temp =
            <div className="container mt-2">
                <div className="card border">
                    <div className="text-center">No Income Data to Display</div>
                </div>
            </div>
        returndata.push(temp)
    }
    let t1 = <div><div className="d-flex small mt-4 p-1 d-md-none d-lg-none">
        <div className="col-5 col-md-5 p-1">
            <div className="col-12 col-md-6 p-0 "></div>
        </div>
        <div className="col-3 col-md-6 p-0 pt-1 text-right"><strong>Debit</strong></div>
        <div className="col-3 col-md-1 p-0 pt-1 text-right"><strong>Credit</strong></div>
    </div>
        <div className="container small mt-3 d-none d-md-block p-0">
            <div className="d-flex">
                <div className="col-1 p-0"><strong>Date</strong></div>
                <div className="col-1 p-0"><strong>Voucher No</strong></div>
                <div className="col-4 p-0"><strong>Particulars</strong></div>
                <div className="col-2 text-right"><strong>Debit</strong></div>
                <div className="col-3 text-right"><strong>Credit</strong></div>
            </div>
        </div></div>;

    let t2 = <div>
        <div className=" d-flex small p-1 d-md-none d-lg-none">
            <div className="col-5 p-1"></div>
            <div className="col-3 text-right"></div>
            <div className="col-3 border-top border-dark text-right">{parseFloat(totalincome).toFixed(2)}</div>
        </div>
        <div className="container small d-none d-md-block p-1">
            <div className="d-flex">
                <div className="col-1 p-0"><strong></strong></div>
                <div className="col-1 p-0"><strong></strong></div>
                <div className="col-4 p-0"><strong>Total Income Other Source</strong></div>
                <div className="col-2 text-right"><strong></strong></div>
                <div className="col-3 border-top border-dark text-right">{parseFloat(totalincome).toFixed(2)}</div>
            </div>
        </div>
    </div>;
    let returndatatemp = [];
    returndatatemp.push(t1);
    returndatatemp.push(returndata);
    returndatatemp.push(t2);
    return returndatatemp;
}
const DisplayIncomeData = function ({ businessid, businessList, getCustomer, getIncomeOS, state, handleOnChange, deleteEntry, fyid, getIncomeHead }) {
    return (
        <div>
            <LocalForm className="small">
                <div className="d-flex justify-content-center mt-4">
                    <div className="form-group mb-1 col-12 col-md-3">
                        <label htmlFor="headSelection" className="mb-0 muted-text"><strong>Filter Data by Head:</strong></label>
                        <Control.select model=".headSelection" className="form-control form-control-sm" id="headSelection" name="headSelection" value={state.headSelection} onChange={handleOnChange}>
                            <Option data={getIncomeHead.data} />
                        </Control.select>
                    </div>
                </div>
            </LocalForm>
            <DisplayData state={state} incomeDetail={getIncomeOS.data} getCustomer={getCustomer} businessid={businessid} businessList={businessList} deleteEntry={deleteEntry} fyid={fyid} />
        </div>
    )
}
class IncomeDetail extends Component {
    constructor(props) {
        super(props);
        this.state = ({
            headSelection: ''
        })
        window.onload = () => {
            this.props.refreshCustomerState();
            this.props.resetIncomeOSDetail();
            this.props.refreshIncomeHead();
        }
    }

    handleOnChange = (event) => {
        let target = event.target;
        let name = target.name;
        let value = target.value;
        this.setState({
            ...this.state,
            [name]: value
        })
    }
    componentDidMount() {
        if (this.props.getCustomer.message === 'initial') this.props.getCustomerList(this.props.businessid);
        if (this.props.getIncomeOS.message === 'initial') this.props.getIncomeOSDetail(this.props.businessid, this.props.fyid);
        if (this.props.getIncomeHead.message === 'initial') this.props.getIncomeHeadList(this.props.businessid);
        if (this.props.deleteEntryMessage.message === "success") {
            alert("Entry Deleted Successfully");
            this.props.resetDeleteEntry();
        }
        if (this.props.deleteEntryMessage.error !== '') {
            alert("Error Deleting Entry - Please Try Again");
            this.props.resetDeleteEntry();
        }
        if (this.props.logoutMessage.error !== '') {
            alert("Error Logging Out. Please Try Again");
            this.props.resetLogoutMessage();
        }
    }
    genereateSalesPdf = async (business, fy) => {
        try {
            let data = this.props.getIncomeOS.data;
            if (this.state.headSelection !== '') data = data.filter(d => d.category === this.state.headSelection);
            const doc = <IncomeLedger data={data} business={business} fy={fy} head={this.state.headSelection} />;
            const blobPdf = pdf(doc);
            blobPdf.updateContainer(doc);
            const result = await blobPdf.toBlob();
            let filename = "Income(OS)Ledger.pdf";
            saveAs(result, filename);
        }
        catch (err) {
            alert(err);
        }
    }
    render() {
        if (!cookie.load('userservice')) return <Redirect to="/" />;
        else {
            if ((this.props.getIncomeOS.loading === true) || (this.props.getCustomer.loading === true) || (this.props.logoutMessage.loading === true) || (this.props.deleteEntryMessage.loading === true) || (this.props.getIncomeHead.loading === true)) {
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div className="mt-3 d-flex justify-content-center">
                            <button className="btn btn-danger">Loading... </button>
                        </div>
                    </div>
                );
            }
            else if ((this.props.getIncomeOS.error !== '') || (this.props.getCustomer.error !== '') || (this.props.getIncomeHead.error !== '')) {
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div className="mt-3 d-flex justify-content-center">
                            <button className="btn btn-danger">Error.. Please Refresh and Try Agin </button>
                        </div>
                    </div>
                );
            }
            else {
                let business = this.props.businessList.data.filter(b => b.businessid === this.props.businessid)[0];
                let fy = business.financial.filter(f => f.fyid === this.props.fyid)[0].financialyear;
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div style={{ minHeight: "93vh", display: "flex", flexDirection: "column" }}>
                            <div className="" style={{ flex: "1" }}>
                                <h6 className="text-center pl-2 pr-2 pt-4" style={{ lineHeight: "1" }}><strong>{business.businessname} {fy}</strong></h6>
                                <h6 className="text-center pl-2 pr-2 mb-4" style={{ lineHeight: "1" }}>{business.alias ? "(" + business.alias + ")" : ''}</h6>
                                <h6 className="text-center pl-2 pr-2"><strong><u>Income (Other Sources) Ledger</u></strong>&nbsp;&nbsp;&nbsp;<FontAwesomeIcon icon={faFilePdf} size="lg" onClick={() => this.genereateSalesPdf(business, fy)} />&nbsp;&nbsp;&nbsp;</h6>
                                < DisplayIncomeData businessid={this.props.businessid} businessList={this.props.businessList} getCustomer={this.props.getCustomer} getIncomeOS={this.props.getIncomeOS} state={this.state} handleOnChange={this.handleOnChange} deleteEntry={this.props.deleteEntry} fyid={this.props.fyid} getIncomeHead={this.props.getIncomeHead} />
                            </div>
                            <Footer />
                        </div>
                    </div>
                )
            }
        }
    }
}
export default IncomeDetail;
