import React, { Component } from 'react';
import cookie from 'react-cookie';
import { Redirect } from 'react-router-dom';
import { LocalForm, Errors, Control } from 'react-redux-form';
import { Header } from './HeaderComponent';
import { Footer } from './Footer';

class TDSOpeningBalance extends Component {
    constructor(props) {
        super(props);
        this.state = ({
            payreceive: 'pay',
            amount: this.props.getTDSOpeningBalance.data.length > 0 ? this.props.getTDSOpeningBalance.data[0].amount : 0  
        })
        window.onload = () => {
            this.props.resetTDSOpeningBalance();
        }
    }
    handleSubmit = () => {
        if (this.state.payreceive === '') alert("please Select TDS Payable or Receivable");
        else {
            if ((this.state.amount === '') || (this.state.amount < 0) || (isNaN(parseInt(this.state.amount)))) alert(" Amount Must Be Greater Than Equal To 0 ");
            else this.props.updateTDSOpeningBalance(this.state, this.props.businessid, this.props.fyid);
        }
    }
    componentDidMount() {
        if (this.props.updateTDSOpeningMessage.error !== '') {
            alert(this.props.updateTDSOpeningMessage.error);
            this.setState({
                ...this.props.updateTDSOpeningMessage.state
            })
            this.props.refreshUpdateTDSOpeningBalance();
        }
        if (this.props.updateTDSOpeningMessage.message === 'success') {
            alert("Data Updated Successfully");
            this.setState({
                ...this.props.getTDSOpeningBalance.state
            })
            this.props.refreshUpdateTDSOpeningBalance();
        }
        if (this.props.getTDSOpeningBalance.message === 'initial') this.props.getTDSOpeningBalanceList(this.props.businessid, this.props.fyid);
        if (this.props.logoutMessage.error !== '') {
            alert("Error Logging Out. Please Try Again");
            this.props.resetLogoutMessage();
        }
    }

    handleInputChange = (event) => {
        let target = event.target;
        let name = target.name;
        let value = target.value;
        this.setState({
            [name]: value
        })
    }
    render() {
        if (!cookie.load('userservice')) return <Redirect to="/" />;
        else {
            if ((this.props.updateTDSOpeningMessage.loading === true) || (this.props.getTDSOpeningBalance.loading === true) || (this.props.logoutMessage.loading === true)) {
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div className="mt-3 d-flex justify-content-center">
                            <button className="btn btn-danger">Loading... </button>
                        </div>
                    </div>
                );
            }
            else if (this.props.getTDSOpeningBalance.error !== '') {
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div className="mt-3 d-flex justify-content-center">
                            <button className="btn btn-danger">Error. Please Refresh and Try Again</button>
                        </div>
                    </div>
                );
            }
            else {
                let business = this.props.businessList.data.filter(b => b.businessid === this.props.businessid)[0];
                let fy = business.financial.filter(f => f.fyid === this.props.fyid)[0].financialyear;
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div style={{ minHeight: "93vh", display: "flex", flexDirection: "column" }}>
                            <div className="container" style={{ flex: "1" }}>
                                <h6 className="text-center pl-2 pr-2 pt-4" style={{ lineHeight: "1" }}><strong>{business.businessname} {fy}</strong></h6>
                                <h6 className="text-center pl-2 pr-2 mb-4" style={{ lineHeight: "1" }}>{business.alias ? "(" + business.alias + ")" : ''}</h6>
                                <h6 className="text-center pl-2 pr-2 col-12"><strong><u>TDS Payable - Opening Value</u></strong></h6>
                                <div className="row">
                                    <LocalForm className="small col-12" onSubmit={this.handleSubmit}>
                                        <div className="container mt-4 d-flex justify-content-center">
                                            <div class="row col-12 col-md-4">
                                                {this.state.payreceive !== '' ? <div className="form-group mb-1 col-12">
                                                    <label htmlFor="amount" className="mb-0 muted-text"><strong>Opening Value Amount</strong><span className="text-danger">*</span></label>
                                                    <Control.text model=".amount" className="form-control form-control-sm" id="amount" name="amount" onChange={this.handleInputChange} value={this.state.amount} />
                                                </div> : ''}
                                                {this.state.payreceive !== '' ? <div className="form-group mb-1 col-12">
                                                    <button type="submit" className="btn btn-success mt-1 btn-sm small" >Update</button>
                                                </div> : ''}
                                            </div>
                                        </div>
                                    </LocalForm>
                                </div>
                            </div>
                            <Footer />
                        </div>
                    </div>
                )
            }
        }
    }
}
export default TDSOpeningBalance;