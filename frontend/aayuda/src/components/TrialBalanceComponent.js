import React, { Component } from 'react';
import cookie from 'react-cookie';
import { Redirect } from 'react-router-dom';
import { Header } from './HeaderComponent';
import { Footer } from './Footer';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faFilePdf } from '@fortawesome/free-solid-svg-icons';
import { Page, Text, View, Document, pdf } from '@react-pdf/renderer';
import { pdfstyles } from '../sylesheet/stylesheet';
import { saveAs } from 'file-saver';

const CustomerPDF = function ({ customer }) {
    let returnrows = [];
    Object.keys(customer).forEach(k => {
        returnrows.push(
            <View style={{ display: "flex", flexDirection: "row", width: "500px" }}><Text style={{ width: "300px", textAlign: "left" }}>{customer[k].amount !== 0 ? customer[k].name : ''}</Text><Text style={{ width: "100px", textAlign: "right" }}>{customer[k].amount > 0 ? parseFloat(customer[k].amount).toFixed(2) : ''}</Text><Text style={{ width: "100px", textAlign: "right" }}>{customer[k].amount < 0 ? parseFloat(-1 * customer[k].amount).toFixed(2) : ''}</Text></View>
        )
    })
    return returnrows;
}
const VendorPDF = function ({ vendor }) {
    let returnrows = [];
    Object.keys(vendor).forEach(k => {
        returnrows.push(
            <View style={{ display: "flex", flexDirection: "row", width: "500px" }}><Text style={{ width: "300px", textAlign: "left" }}>{vendor[k].amount !== 0 ? vendor[k].name : ''}</Text><Text style={{ width: "100px", textAlign: "right" }}>{vendor[k].amount > 0 ? parseFloat(vendor[k].amount).toFixed(2) : ''}</Text><Text style={{ width: "100px", textAlign: "right" }}>{vendor[k].amount < 0 ? parseFloat(-1 * vendor[k].amount).toFixed(2) : ''}</Text></View>
        )
    })
    return returnrows;
}
const BankPDF = function ({ bank }) {
    let returnrows = [];
    Object.keys(bank).forEach(k => {
        returnrows.push(
            <View style={{ display: "flex", flexDirection: "row", width: "500px" }}><Text style={{ width: "300px", textAlign: "left" }}>{bank[k].amount !== 0 ? bank[k].name : ''}</Text><Text style={{ width: "100px", textAlign: "right" }}>{bank[k].amount > 0 ? parseFloat(bank[k].amount).toFixed(2) : ''}</Text><Text style={{ width: "100px", textAlign: "right" }}>{bank[k].amount < 0 ? parseFloat(-1 * bank[k].amount).toFixed(2) : ''}</Text></View>
        )
    })
    return returnrows;
}
const LoanPDF = function ({ loan }) {
    let returnrows = [];
    Object.keys(loan).forEach(k => {
        returnrows.push(
            <View style={{ display: "flex", flexDirection: "row", width: "500px" }}><Text style={{ width: "300px", textAlign: "left" }}>{loan[k].amount !== 0 ? loan[k].name : ''}</Text><Text style={{ width: "100px", textAlign: "right" }}>{loan[k].amount > 0 ? parseFloat(loan[k].amount).toFixed(2) : ''}</Text><Text style={{ width: "100px", textAlign: "right" }}>{loan[k].amount < 0 ? parseFloat(-1 * loan[k].amount).toFixed(2) : ''}</Text></View>
        )
    })
    return returnrows;
}
const InvestmentPDF = function ({ investment, getInvestmentAccount, profit }) {
    let returnrows = [];
    Object.keys(investment).forEach(k => {
        let holding = (getInvestmentAccount.data.filter(d => d.id === k)[0].shareholding) / 100;
        let amount = investment[k].amount - (profit * holding);
        returnrows.push(
            <View style={{ display: "flex", flexDirection: "row", width: "500px" }}><Text style={{ width: "300px", textAlign: "left" }}>{investment[k].amount !== 0 ? investment[k].name : ''}</Text><Text style={{ width: "100px", textAlign: "right" }}>{amount > 0 ? parseFloat(amount).toFixed(2) : ''}</Text><Text style={{ width: "100px", textAlign: "right" }}>{amount < 0 ? parseFloat(-1 * amount).toFixed(2) : ''}</Text></View>
        )
    })
    return returnrows;
}
const AssetPDF = function ({ asset }) {
    let returnrows = [];
    Object.keys(asset).forEach(k => {
        returnrows.push(
            <View style={{ display: "flex", flexDirection: "row", width: "500px" }}><Text style={{ width: "300px", textAlign: "left" }}>{asset[k].amount !== 0 ? asset[k].name : ''}</Text><Text style={{ width: "100px", textAlign: "right" }}>{asset[k].amount > 0 ? parseFloat(asset[k].amount).toFixed(2) : ''}</Text><Text style={{ width: "100px", textAlign: "right" }}>{asset[k].amount < 0 ? parseFloat(-1 * asset[k].amount).toFixed(2) : ''}</Text></View>
        )
    })
    return returnrows;
}

const Customer = function ({ customer }) {
    let returnrows = [];
    Object.keys(customer).forEach(k => {
        returnrows.push(
            <div className="d-flex col-12 p-0">
                <div className="col-6 text-left"><strong>{customer[k].amount !== 0 ? customer[k].name : ''}</strong></div>
                <div className="col-3 text-right">{customer[k].amount > 0 ? parseFloat(customer[k].amount).toFixed(2) : ''}</div>
                <div className="col-3 text-right">{customer[k].amount < 0 ? parseFloat(-1 * customer[k].amount).toFixed(2) : ''}</div>
            </div>
        )
    })
    return returnrows;
}
const Vendor = function ({ vendor }) {
    let returnrows = [];
    Object.keys(vendor).forEach(k => {
        returnrows.push(
            <div className="d-flex col-12 p-0">
                <div className="col-6 text-left"><strong>{vendor[k].amount !== 0 ? vendor[k].name : ''}</strong></div>
                <div className="col-3 text-right">{vendor[k].amount > 0 ? parseFloat(vendor[k].amount).toFixed(2) : ''}</div>
                <div className="col-3 text-right">{vendor[k].amount < 0 ? parseFloat(-1 * vendor[k].amount).toFixed(2) : ''}</div>
            </div>
        )
    })
    return returnrows;
}
const Bank = function ({ bank }) {
    let returnrows = [];
    Object.keys(bank).forEach(k => {
        returnrows.push(
            <div className="d-flex col-12 p-0">
                <div className="col-6 text-left"><strong>{bank[k].amount !== 0 ? bank[k].name : ''}</strong></div>
                <div className="col-3 text-right">{bank[k].amount > 0 ? parseFloat(bank[k].amount).toFixed(2) : ''}</div>
                <div className="col-3 text-right">{bank[k].amount < 0 ? parseFloat(-1 * bank[k].amount).toFixed(2) : ''}</div>
            </div>
        )
    })
    return returnrows;
}
const Loan = function ({ loan }) {
    let returnrows = [];
    Object.keys(loan).forEach(k => {
        returnrows.push(
            <div className="d-flex col-12 p-0">
                <div className="col-6 text-left"><strong>{loan[k].amount !== 0 ? loan[k].name : ''}</strong></div>
                <div className="col-3 text-right">{loan[k].amount > 0 ? parseFloat(loan[k].amount).toFixed(2) : ''}</div>
                <div className="col-3 text-right">{loan[k].amount < 0 ? parseFloat(-1 * loan[k].amount).toFixed(2) : ''}</div>
            </div>
        )
    })
    return returnrows;
}
const Investment = function ({ investment, getInvestmentAccount, profit }) {
    let returnrows = [];
    Object.keys(investment).forEach(k => {
        let holding = (getInvestmentAccount.data.filter(d => d.id === k)[0].shareholding) / 100;
        let amount = investment[k].amount - (profit * holding);
        returnrows.push(
            <div className="d-flex col-12 p-0">
                <div className="col-6 text-left"><strong>{investment[k].amount !== 0 ? investment[k].name : ''}</strong></div>
                <div className="col-3 text-right">{amount > 0 ? parseFloat(amount).toFixed(2) : ''}</div>
                <div className="col-3 text-right">{amount < 0 ? parseFloat(-1 * amount).toFixed(2) : ''}</div>
            </div>
        )
    })
    return returnrows;
}
const Asset = function ({ asset }) {
    let returnrows = [];
    Object.keys(asset).forEach(k => {
        returnrows.push(
            <div className="d-flex col-12 p-0">
                <div className="col-6 text-left"><strong>{asset[k].amount !== 0 ? asset[k].name : ''}</strong></div>
                <div className="col-3 text-right">{asset[k].amount > 0 ? parseFloat(asset[k].amount).toFixed(2) : ''}</div>
                <div className="col-3 text-right">{asset[k].amount < 0 ? parseFloat(-1 * asset[k].amount).toFixed(2) : ''}</div>
            </div>
        )
    })
    return returnrows;
}
const Report = function ({ sales, gst, tdspayable, tdsreceivable, customer, purchase, expense, vendor, cash, bank, incomeos, reversecharge, loanaccount, investmentaccount, assetaccount, depreciation, capitalaccount, openingbaldr, openingbalcr, totalassetprofit, assetpresent, asset, flag, equitycapital, shareholderreserves, profit, it, debit, credit, business, fy, getInvestmentAccount }) {
    let returnrows = [];
    returnrows.push(<Page size="A4" style={pdfstyles.page}>
        <View style={pdfstyles.taxInvoiceHeader, { marginTop: "20px", textAlign: "center" }}>
            <View style={{ fontFamily: "Helvetica-Bold", width: "500px" }}><Text>{business.businessname}</Text></View>
            <View style={{ width: "500px" }}><Text>{business.address}</Text></View>
        </View>
        <View style={pdfstyles.taxInvoiceHeader, { marginTop: "10px", textAlign: "center" }}>
            <View style={{ fontFamily: "Helvetica-Bold", textDecoration: "underline", width: "500px" }}><Text>Trial Balance</Text></View>
            <View style={{ width: "500px" }}><Text>{fy}</Text></View>
        </View>
        <View style={{ width: "500px", marginTop: "20px", display: "flex", flexDirection: "row", borderBottom: "1px solid black", borderTop: "1px solid black", fontFamily: "Helvetica-Bold" }}>
            <View style={{ display: "flex", flexDirection: "row" }}><Text style={{ width: "300px", textAlign: "left" }}>Particulars</Text><Text style={{ width: "100px", textAlign: "right" }}>Debit</Text><Text style={{ width: "100px", textAlign: "right" }}>Credit</Text></View>
        </View>
        <View style={{ display: "flex", flexDirection: "row", width: "500px" }}><Text style={{ width: "300px", textAlign: "left" }}>{sales !== 0 ? "Sales" : ''}</Text><Text style={{ width: "100px", textAlign: "right" }}>{sales > 0 ? parseFloat(sales).toFixed(2) : ''}</Text><Text style={{ width: "100px", textAlign: "right" }}>{sales < 0 ? parseFloat(-1 * sales).toFixed(2) : ''}</Text></View>
        <View style={{ display: "flex", flexDirection: "row", width: "500px" }}><Text style={{ width: "300px", textAlign: "left" }}>{incomeos !== 0 ? "Income OS" : ''}</Text><Text style={{ width: "100px", textAlign: "right" }}>{incomeos > 0 ? parseFloat(incomeos).toFixed(2) : ''}</Text><Text style={{ width: "100px", textAlign: "right" }}>{incomeos < 0 ? parseFloat(-1 * incomeos).toFixed(2) : ''}</Text></View>
        <View style={{ display: "flex", flexDirection: "row", width: "500px" }}><Text style={{ width: "300px", textAlign: "left" }}>{purchase !== 0 ? "Purchase" : ''}</Text><Text style={{ width: "100px", textAlign: "right" }}>{purchase > 0 ? parseFloat(purchase).toFixed(2) : ''}</Text><Text style={{ width: "100px", textAlign: "right" }}>{purchase < 0 ? parseFloat(-1 * purchase).toFixed(2) : ''}</Text></View>
        <View style={{ display: "flex", flexDirection: "row", width: "500px" }}><Text style={{ width: "300px", textAlign: "left" }}>{expense !== 0 ? "Expense" : ''}</Text><Text style={{ width: "100px", textAlign: "right" }}>{expense > 0 ? parseFloat(expense).toFixed(2) : ''}</Text><Text style={{ width: "100px", textAlign: "right" }}>{expense < 0 ? parseFloat(-1 * expense).toFixed(2) : ''}</Text></View>
        {cash !== 0 ? <View style={{ display: "flex", flexDirection: "row", width: "500px" }}><Text style={{ width: "300px", textAlign: "left" }}>Cash</Text><Text style={{ width: "100px", textAlign: "right" }}>{cash > 0 ? parseFloat(cash).toFixed(2) : ''}</Text><Text style={{ width: "100px", textAlign: "right" }}>{cash < 0 ? parseFloat(-1 * cash).toFixed(2) : ''}</Text></View> : <View></View>}
        {tdspayable !== 0 ? <View style={{ display: "flex", flexDirection: "row", width: "500px" }}><Text style={{ width: "300px", textAlign: "left" }}>TDS Payable</Text><Text style={{ width: "100px", textAlign: "right" }}>{tdspayable > 0 ? parseFloat(tdspayable).toFixed(2) : ''}</Text><Text style={{ width: "100px", textAlign: "right" }}>{tdspayable < 0 ? parseFloat(-1 * tdspayable).toFixed(2) : ''}</Text></View> : <View></View>}
        {tdsreceivable !== 0 ? <View style={{ display: "flex", flexDirection: "row", width: "500px" }}><Text style={{ width: "300px", textAlign: "left" }}>TDS Receivable</Text><Text style={{ width: "100px", textAlign: "right" }}>{tdsreceivable > 0 ? parseFloat(tdsreceivable).toFixed(2) : ''}</Text><Text style={{ width: "100px", textAlign: "right" }}>{tdsreceivable < 0 ? parseFloat(-1 * tdsreceivable).toFixed(2) : ''}</Text></View> : <View></View>}
        {it !== 0 ? <View style={{ display: "flex", flexDirection: "row", width: "500px" }}><Text style={{ width: "300px", textAlign: "left" }}>Income Tax</Text><Text style={{ width: "100px", textAlign: "right" }}>{it > 0 ? parseFloat(it).toFixed(2) : ''}</Text><Text style={{ width: "100px", textAlign: "right" }}>{it < 0 ? parseFloat(-1 * it).toFixed(2) : ''}</Text></View> : <View></View>}
        {gst !== 0 ? <View style={{ display: "flex", flexDirection: "row", width: "500px" }}><Text style={{ width: "300px", textAlign: "left" }}>GST</Text><Text style={{ width: "100px", textAlign: "right" }}>{gst > 0 ? parseFloat(gst).toFixed(2) : ''}</Text><Text style={{ width: "100px", textAlign: "right" }}>{gst < 0 ? parseFloat(-1 * gst).toFixed(2) : ''}</Text></View> : <View></View>}
        {depreciation !== 0 ? <View style={{ display: "flex", flexDirection: "row", width: "500px" }}><Text style={{ width: "300px", textAlign: "left" }}>Depreciation</Text><Text style={{ width: "100px", textAlign: "right" }}>{depreciation > 0 ? parseFloat(depreciation).toFixed(2) : ''}</Text><Text style={{ width: "100px", textAlign: "right" }}>{depreciation < 0 ? parseFloat(-1 * depreciation).toFixed(2) : ''}</Text></View> : <View></View>}
        <View style={{ display: "flex", flexDirection: "row", width: "500px" }}><Text style={{ width: "300px", textAlign: "left" }}>{openingbaldr !== openingbalcr ? "Difference In Opening Balance" : ''}</Text><Text style={{ width: "100px", textAlign: "right" }}>{openingbaldr < openingbalcr ? parseFloat(openingbalcr - openingbaldr).toFixed(2) : ''}</Text><Text style={{ width: "100px", textAlign: "right" }}>{openingbaldr > openingbalcr ? parseFloat(openingbaldr - openingbalcr).toFixed(2) : ''}</Text></View>
        <View style={{ display: "flex", flexDirection: "row", width: "500px" }}><Text style={{ width: "300px", textAlign: "left" }}>{((profit > 0) && (business.businesstype === 'partnership') || (business.businesstype === 'propritorship')) ? 'Profit' : ''}{((profit < 0) && (business.businesstype === 'partnership') || (business.businesstype === 'propritorship')) ? 'Loss' : ''}</Text><Text style={{ width: "100px", textAlign: "right" }}>{((profit > 0) && (business.businesstype === 'partnership') || (business.businesstype === 'propritorship')) ? parseFloat(profit).toFixed(2) : ''}</Text><Text style={{ width: "100px", textAlign: "right" }}>{((profit < 0) && (business.businesstype === 'partnership') || (business.businesstype === 'propritorship')) ? parseFloat(-1 * profit).toFixed(2) : ''}</Text></View>
        <CustomerPDF customer={customer} />
        <VendorPDF vendor={vendor} />
        <BankPDF bank={bank} />
        <LoanPDF loan={loanaccount} />
        {business.businesstype === 'partnership' ? <div className="row col-12">
            <InvestmentPDF investment={investmentaccount} getInvestmentAccount={getInvestmentAccount} profit={profit} />
        </div> : <View></View>}
        {business.businesstype === 'propritorship' ?
            <View style={{ display: "flex", flexDirection: "row", width: "500px" }}><Text style={{ width: "300px", textAlign: "left" }}>Capital A/C</Text><Text style={{ width: "100px", textAlign: "right" }}>{capitalaccount > 0 ? parseFloat(capitalaccount).toFixed(2) : ''}</Text><Text style={{ width: "100px", textAlign: "right" }}>{capitalaccount < 0 ? parseFloat(-1 * capitalaccount).toFixed(2) : ''}</Text></View>
            : <View></View>}
        {business.businesstype === 'ltd' && equitycapital !== 0 ?
            <View style={{ display: "flex", flexDirection: "row", width: "500px" }}><Text style={{ width: "300px", textAlign: "left" }}>Equity Capital Fund</Text><Text style={{ width: "100px", textAlign: "right" }}>{equitycapital > 0 ? parseFloat(equitycapital).toFixed(2) : ''}</Text><Text style={{ width: "100px", textAlign: "right" }}>{equitycapital < 0 ? parseFloat(-1 * equitycapital).toFixed(2) : ''}</Text></View>
            : <View></View>}
        {business.businesstype === 'ltd' && shareholderreserves !== 0 ?
            <View style={{ display: "flex", flexDirection: "row", width: "500px" }}><Text style={{ width: "300px", textAlign: "left" }}>ShareHolders Reserves Fund</Text><Text style={{ width: "100px", textAlign: "right" }}>{shareholderreserves > 0 ? parseFloat(shareholderreserves).toFixed(2) : ''}</Text><Text style={{ width: "100px", textAlign: "right" }}>{shareholderreserves < 0 ? parseFloat(-1 * shareholderreserves).toFixed(2) : ''}</Text></View> : <View></View>}
        <AssetPDF asset={assetaccount} />
        <View style={{ display: "flex", flexDirection: "row", width: "500px" }}><Text style={{ width: "300px", textAlign: "left" }}></Text><Text style={{ width: "100px", textAlign: "right", borderTop: "1px solid black" }}>{parseFloat(debit).toFixed(2)}</Text><Text style={{ width: "100px", textAlign: "right", borderTop: "1px solid black" }}>{parseFloat(credit * -1).toFixed(2)}</Text></View>
    </Page >)
    return returnrows;
}
const TrialBalancePDF = function ({ sales, gst, tdspayable, tdsreceivable, customer, purchase, expense, vendor, cash, bank, incomeos, reversecharge, loanaccount, investmentaccount, assetaccount, depreciation, capitalaccount, openingbaldr, openingbalcr, totalassetprofit, assetpresent, asset, flag, equitycapital, shareholderreserves, profit, it, debit, credit, business, fy, getInvestmentAccount }) {
    return (
        <Document>
            <Report sales={sales} gst={gst} tdspayable={tdspayable} tdsreceivable={tdsreceivable} customer={customer} purchase={purchase} expense={expense} vendor={vendor} cash={cash} bank={bank} incomeos={incomeos} reversecharge={reversecharge} loanaccount={loanaccount} investmentaccount={investmentaccount} assetaccount={assetaccount} depreciation={depreciation} capitalaccount={capitalaccount} openingbaldr={openingbaldr} openingbalcr={openingbalcr} totalassetprofit={totalassetprofit} assetpresent={assetpresent} asset={asset} flag={flag} equitycapital={equitycapital} shareholderreserves={shareholderreserves} profit={profit} it={it} debit={debit} credit={credit} business={business} fy={fy} getInvestmentAccount={getInvestmentAccount} />
        </Document>
    )
}
class TrialBalance extends Component {
    componentDidMount() {
        if (this.props.logoutMessage.error !== '') {
            alert("Error Logging Out. Please Try Again");
            this.props.resetLogoutMessage();
        }
    }
    generatePdf = async (sales, gst, tdspayable, tdsreceivable, customer, purchase, expense, vendor, cash, bank, incomeos, reversecharge, loanaccount, investmentaccount, assetaccount, depreciation, capitalaccount, openingbaldr, openingbalcr, totalassetprofit, assetpresent, asset, flag, equitycapital, shareholderreserves, profit, it, debit, credit, business, fy, getInvestmentAccount) => {
        try {
            const doc = <TrialBalancePDF sales={sales} gst={gst} tdspayable={tdspayable} tdsreceivable={tdsreceivable} customer={customer} purchase={purchase} expense={expense} vendor={vendor} cash={cash} bank={bank} incomeos={incomeos} reversecharge={reversecharge} loanaccount={loanaccount} investmentaccount={investmentaccount} assetaccount={assetaccount} depreciation={depreciation} capitalaccount={capitalaccount} openingbaldr={openingbaldr} openingbalcr={openingbalcr} totalassetprofit={totalassetprofit} assetpresent={assetpresent} asset={asset} flag={flag} equitycapital={equitycapital} shareholderreserves={shareholderreserves} profit={profit} it={it} debit={debit} credit={credit} business={business} fy={fy} getInvestmentAccount={getInvestmentAccount} />;
            const blobPdf = pdf(doc);
            blobPdf.updateContainer(doc);
            const result = await blobPdf.toBlob();
            let filename = "TrialBalance.pdf";
            saveAs(result, filename);
        }
        catch (err) {
            alert(err);
        }
    }
    render() {
        if (!cookie.load('userservice')) return <Redirect to="/" />
        else {
            if ((this.props.getExpense.loading === true) || (this.props.salesDetail.loading === true) || (this.props.purchaseDetail.loading === true) || (this.props.getPayment.loading === true) || (this.props.getBankEntry.loading === true) || (this.props.logoutMessage.loading === true) || (this.props.getExpenseHead.loading === true) || (this.props.getLoan.loading === true) || (this.props.getIncomeOS.loading === true) || (this.props.getDebitCredit.loading === true) || (this.props.getInvestment.loading === true) || (this.props.getAssetAccount.loading === true) || (this.props.getAssetOpeningBalance.loading === true) || (this.props.getCustomer.loading === true) || (this.props.getVendor.loading === true) || (this.props.getExpenseHead.loading === true) || (this.props.getBankDetail.loading === true) || (this.props.getLoanAccount.loading === true) || (this.props.getInvestmentAccount.loading === true) || (this.props.getOpeningBalance.loading === true) || (this.props.getRCOpeningBalance.loading === true)) {
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div className="mt-3 d-flex justify-content-center">
                            <button className="btn btn-danger">Loading... </button>
                        </div>
                    </div>
                );
            }
            else if ((this.props.getExpense.error !== '') || (this.props.salesDetail.error !== '') || (this.props.purchaseDetail.error !== '') || (this.props.getPayment.error !== '') || (this.props.getBankEntry.error !== '') || (this.props.logoutMessage.error !== '') || (this.props.getExpenseHead.error !== '') || (this.props.getLoan.error !== '') || (this.props.getIncomeOS.error !== '') || (this.props.getDebitCredit.error !== '') || (this.props.getInvestment.error !== '') || (this.props.getAssetAccount.error !== '') || (this.props.getAssetOpeningBalance.error !== '') || (this.props.getCustomer.error !== '') || (this.props.getVendor.error !== '') || (this.props.getExpenseHead.error !== '') || (this.props.getBankDetail.error !== '') || (this.props.getLoanAccount.error !== '') || (this.props.getInvestmentAccount.error !== '') || (this.props.getOpeningBalance.error !== '') || (this.props.getRCOpeningBalance.error !== '')) {
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div className="mt-3 d-flex justify-content-center">
                            <button className="btn btn-danger">Error.. Please Refresh and Try Again </button>
                        </div>
                    </div>
                );
            }
            else {
                let sales = 0;
                let gst = 0;
                let tdspayable = 0;
                let tdsreceivable = 0;
                let customer = {};
                let purchase = 0;
                let expense = 0;
                let vendor = {};
                let cash = 0;
                let bank = {};
                let incomeos = 0;
                let reversecharge = 0;
                let loanaccount = {};
                let investmentaccount = {};
                let assetaccount = {};
                let depreciation = 0;
                let capitalaccount = 0;
                let openingbaldr = 0;
                let openingbalcr = 0;
                let totalassetprofit = 0;
                let assetpresent = false;
                let asset = {}
                let flag = 0;
                let equitycapital = 0;
                let shareholderreserves = 0;
                let profit = 0;
                let it = 0;
                let debit = 0;
                let credit = 0;
                let business = this.props.businessList.data.filter(b => b.businessid === this.props.businessid)[0];
                let fy = business.financial.filter(f => f.fyid === this.props.fyid)[0].financialyear;
                this.props.getAssetAccount.data.forEach(d => {
                    if (d.soldfyid === this.props.fyid) {
                        asset = {
                            ...asset,
                            [d.id]: { name: d.assetaccount, amount: d.soldbasic }
                        }
                        totalassetprofit = totalassetprofit + parseFloat(d.soldbasic);
                        assetpresent = true;
                    }
                    if ((d.depreciating === '1') && (d.purchasefyid === this.props.fyid)) {
                        if (Object.keys(asset).includes(d.id)) {
                            flag = 1;
                            totalassetprofit = totalassetprofit - parseFloat(d.purchasebasic);
                        }
                    }
                    else if (d.depreciating === '0') {
                        if (Object.keys(asset).includes(d.id)) {
                            totalassetprofit = totalassetprofit - parseFloat(d.purchasebasic);
                        }
                    }
                })
                this.props.getAssetOpeningBalance.data.forEach(d => {
                    if (Object.keys(asset).includes(d.assetaccount)) {
                        if (flag === 1) {
                            totalassetprofit = totalassetprofit - parseFloat(d.amount);
                        }
                    }
                })
                let openingStock = parseFloat(this.props.getOpeningClosing.data.aprilopening);
                let closingStock = parseFloat(this.props.getOpeningClosing.data.marchclosing);
                this.props.getCustomer.data.forEach(d => {
                    customer = {
                        ...customer,
                        [d.customerid]: {
                            name: d.businessname,
                            amount: 0
                        }
                    }
                })
                this.props.getLoanAccount.data.forEach(d => {
                    loanaccount = {
                        ...loanaccount,
                        [d.id]: {
                            name: d.loanaccount,
                            amount: 0
                        }
                    }
                })
                this.props.getInvestmentAccount.data.forEach(d => {
                    investmentaccount = {
                        ...investmentaccount,
                        [d.id]: {
                            name: d.investmentaccount,
                            amount: 0
                        }
                    }
                })
                this.props.getAssetAccount.data.forEach(d => {
                    assetaccount = {
                        ...assetaccount,
                        [d.id]: {
                            name: d.assetaccount,
                            amount: 0
                        }
                    }
                })
                this.props.getVendor.data.forEach(d => {
                    vendor = {
                        ...vendor,
                        [d.vendorid]: {
                            name: d.businessname,
                            amount: 0
                        }
                    }
                })
                this.props.getBankDetail.data.forEach(d => {
                    bank = {
                        ...bank,
                        [d.bankdetailid]: {
                            name: d.bankname,
                            amount: 0
                        }
                    }
                })
                this.props.salesDetail.data.forEach(d => {
                    if ((d.type === "sales") && (d.active == 0)) {
                        if (d.cash == 1) {
                            sales = sales - parseFloat(d.totalbasic);
                            sales = sales - parseFloat(d.freight);
                            gst = gst - parseFloat(d.totaligst);
                            gst = gst - parseFloat(d.totalcgst);
                            gst = gst - parseFloat(d.totalsgst);
                            credit = credit - parseFloat(d.totalbasic) - parseFloat(d.freight);
                            cash = cash + parseFloat(d.grandtotal);
                            //debit = debit + parseFloat(d.grandtotal);
                        }
                        else {
                            if (d.customerid !== '') {
                                sales = sales - parseFloat(d.totalbasic);
                                sales = sales - parseFloat(d.freight);
                                gst = gst - parseFloat(d.totaligst);
                                gst = gst - parseFloat(d.totalcgst);
                                gst = gst - parseFloat(d.totalsgst);
                                credit = credit - parseFloat(d.totalbasic) - parseFloat(d.freight);
                                customer = {
                                    ...customer,
                                    [d.customerid]: {
                                        name: customer[d.customerid].name,
                                        amount: customer[d.customerid].amount + parseFloat(d.grandtotal)
                                    }
                                }
                                //    debit = debit + parseFloat(d.grandtotal);
                            }
                        }
                    }
                });
                this.props.purchaseDetail.data.forEach(d => {
                    purchase = purchase + parseFloat(d.totalbasic);
                    gst = gst + parseFloat(d.totaligst);
                    gst = gst + parseFloat(d.totalcgst);
                    gst = gst + parseFloat(d.totalsgst);
                    debit = debit + parseFloat(d.totalbasic);
                    vendor = {
                        ...vendor,
                        [d.vendorid]: {
                            name: vendor[d.vendorid].name,
                            amount: vendor[d.vendorid].amount - parseFloat(d.grandtotal)
                        }
                    }
                    //credit = credit - parseFloat(d.grandtotal);
                });
                this.props.getExpense.data.forEach(d => {
                    expense = expense + parseFloat(d.amount);
                    debit = debit + parseFloat(d.amount);
                    if (d.tds === "1") {
                        tdspayable = tdspayable - parseFloat(d.tdsamount);
                        vendor = {
                            ...vendor,
                            [d.vendorid]: {
                                name: vendor[d.vendorid].name,
                                amount: vendor[d.vendorid].amount - ((parseFloat(d.amount) - parseFloat(d.tdsamount)))
                            }
                        }
                        credit = credit - parseFloat(d.amount);
                    }
                    else if (d.it === "1") {
                        it = it - parseFloat(d.amount);
                        //credit = credit - parseFloat(d.amount);
                        expense = expense + parseFloat(d.amount);
                        debit = debit + parseFloat(d.amount);
                    }
                    else if (d.partner === '1') {
                        if (d.vendorid) investmentaccount = {
                            ...investmentaccount,
                            [d.vendorid]: {
                                name: investmentaccount[d.vendorid].name,
                                amount: investmentaccount[d.vendorid].amount - parseFloat(d.amount)
                            }
                        }
                    }
                    else {
                        if (d.cash === "1") {
                            cash = cash - parseFloat(d.amount);
                            //credit = credit - parseFloat(d.amount);
                        }
                        else {
                            if (d.vendorid) vendor = {
                                ...vendor,
                                [d.vendorid]: {
                                    name: vendor[d.vendorid].name,
                                    amount: vendor[d.vendorid].amount - parseFloat(d.amount)
                                }
                            }
                            //credit = credit - parseFloat(d.amount);
                        }
                    }
                });
                this.props.getDebitCredit.data.forEach(d => {
                    if (d.vendorcustomer === 'customer') {
                        sales = sales + parseFloat(d.basic);
                        gst = gst + parseFloat(d.igst);
                        gst = gst + parseFloat(d.cgst);
                        gst = gst + parseFloat(d.sgst);
                        debit = debit + parseFloat(d.basic) + parseFloat(d.igst) + parseFloat(d.cgst) + parseFloat(d.sgst);
                        customer = {
                            ...customer,
                            [d.partyid]: {
                                name: customer[d.partyid].name,
                                amount: customer[d.partyid].amount - parseFloat(d.total)
                            }
                        }
                        credit = credit - parseFloat(d.total);
                    }
                    else if (d.vendorcustomer === 'vendor') {
                        purchase = purchase - parseFloat(d.totalbasic);
                        gst = gst - parseFloat(d.totaligst);
                        gst = gst - parseFloat(d.totalcgst);
                        gst = gst - parseFloat(d.totalsgst);
                        credit = credit - parseFloat(d.totalbasic) - parseFloat(d.totaligst) - parseFloat(d.totalcgst) - parseFloat(d.totalsgst);
                        vendor = {
                            ...vendor,
                            [d.partyid]: {
                                name: vendor[d.partyid].name,
                                amount: vendor[d.partyid].amount + parseFloat(d.total)
                            }
                        }
                        debit = debit + parseFloat(d.total);
                    }
                })
                this.props.getOpeningBalance.data.forEach(d => {
                    if (d.party === 'customer') {
                        if (d.amounttype === 'receivable') {
                            customer = {
                                ...customer,
                                [d.partyid]: {
                                    name: customer[d.partyid].name,
                                    amount: customer[d.partyid].amount + parseFloat(d.amount)
                                }
                            }
                            //debit = debit + parseFloat(d.amount);
                            openingbaldr = openingbaldr + parseFloat(d.amount);
                        }
                        else if (d.amounttype === 'receivedadvance') {
                            customer = {
                                ...customer,
                                [d.partyid]: {
                                    name: customer[d.partyid].name,
                                    amount: customer[d.partyid].amount - parseFloat(d.amount)
                                }
                            }
                            //credit = credit - parseFloat(d.amount);
                            openingbalcr = openingbalcr - parseFloat(d.amount);
                        }
                    }
                    else if (d.party === 'vendor') {
                        if (d.amounttype === 'payable') {
                            vendor = {
                                ...vendor,
                                [d.partyid]: {
                                    name: vendor[d.partyid].name,
                                    amount: vendor[d.partyid].amount - parseFloat(d.amount)
                                }
                            }
                            //credit = credit + parseFloat(d.amount);
                            openingbalcr = openingbalcr - parseFloat(d.amount);
                        }
                        else if (d.amounttype === 'paidadvance') {
                            vendor = {
                                ...vendor,
                                [d.partyid]: {
                                    name: vendor[d.partyid].name,
                                    amount: vendor[d.partyid].amount + parseFloat(d.amount)
                                }
                            }
                            //debit = debit + parseFloat(d.amount);
                            openingbaldr = openingbaldr + parseFloat(d.amount);
                        }
                    }
                })
                this.props.getIncomeTaxOpeningBalance.data.forEach(d => {
                    if (d.payreceive === 'paytds') {
                        tdspayable = tdspayable - parseFloat(d.amount);
                        openingbalcr = openingbalcr - parseFloat(d.amount);
                    }
                    else if (d.payreceive === 'receivetds') {
                        tdsreceivable = tdsreceivable + parseFloat(d.amount);
                        openingbaldr = openingbaldr + parseFloat(d.amount);
                    }
                    else if (d.payreceive === 'payit') {
                        tdsreceivable = tdsreceivable - parseFloat(d.amount);
                        openingbalcr = openingbalcr - parseFloat(d.amount);
                    }
                })
                this.props.getBankOpeningBalance.data.forEach(d => {
                    bank = {
                        ...bank,
                        [d.bankid]: {
                            name: bank[d.bankid].name,
                            amount: bank[d.bankid].amount + parseFloat(d.amount)
                        }
                    }
                    //debit = debit + parseFloat(d.amount);
                    openingbaldr = openingbaldr + parseFloat(d.amount);
                })
                cash = cash + parseFloat(this.props.getCashInHand.amount);
                //debit = debit + parseFloat(this.props.getCashInHand.amount);
                openingbaldr = openingbaldr + parseFloat(this.props.getCashInHand.amount);
                if (this.props.getGSTOpeningBalance.inputpayable === 'input') {
                    gst = gst + parseFloat(this.props.getGSTOpeningBalance.gst);
                    openingbaldr = openingbaldr + parseFloat(this.props.getGSTOpeningBalance.gst);
                }
                else if (this.props.getGSTOpeningBalance.inputpayable === 'payable') {
                    gst = gst - parseFloat(this.props.getGSTOpeningBalance.gst);
                    openingbalcr = openingbalcr + parseFloat(this.props.getGSTOpeningBalance.gst);
                }
                this.props.getLoanOpeningBalance.data.forEach(d => {
                    if (d.payreceive === 'pay') {
                        loanaccount = {
                            ...loanaccount,
                            [d.loanaccount]: {
                                name: loanaccount[d.loanaccount].name,
                                amount: loanaccount[d.loanaccount].amount + d.amount
                            }
                        }
                        //debit = debit + parseFloat(d.amount);
                        openingbaldr = openingbaldr + parseFloat(d.amount);
                    }
                    else {
                        loanaccount = {
                            ...loanaccount,
                            [d.loanaccount]: {
                                name: loanaccount[d.loanaccount].name,
                                amount: loanaccount[d.loanaccount] - d.amount
                            }
                        }
                        //credit = credit - parseFloat(d.amount);
                        openingbalcr = openingbalcr + parseFloat(d.amount);
                    }
                })
                this.props.getAssetOpeningBalance.data.forEach(d => {
                    assetaccount = {
                        ...assetaccount,
                        [d.assetaccount]: {
                            name: assetaccount[d.assetaccount].name,
                            amount: assetaccount[d.assetaccount].amount + parseFloat(d.amount)
                        }
                    }
                    //debit = debit + parseFloat(d.amount);
                    openingbaldr = openingbaldr + parseFloat(d.amount);
                })
                if (business.businesstype === 'partnership') this.props.getInvestmentOpeningBalance.data.forEach(d => {
                    if (d.amounttype === 'payable') {
                        investmentaccount = {
                            ...investmentaccount,
                            [d.investmentaccount.split("...")[0]]: {
                                name: investmentaccount[d.investmentaccount.split("...")[0]].name,
                                amount: investmentaccount[d.investmentaccount.split("...")[0]].amount + parseFloat(d.amount)
                            }
                        }
                        //debit = debit + parseFloat(d.amount);
                        openingbaldr = openingbaldr + parseFloat(d.amount);
                    }
                    else if (d.amounttype === 'deposited') {
                        investmentaccount = {
                            ...investmentaccount,
                            [d.investmentaccount.split("...")[0]]: {
                                name: investmentaccount[d.investmentaccount.split("...")[0]].name,
                                amount: investmentaccount[d.investmentaccount.split("...")[0]].amount - parseFloat(d.amount)
                            }
                        }
                        //credit = credit - parseFloat(d.amount);
                        openingbalcr = openingbalcr + parseFloat(d.amount);
                    }
                })
                else if (business.businesstype === 'propritorship') {
                    capitalaccount = capitalaccount - parseFloat(this.props.getInvestmentOpeningBalance.data[0].amount);
                    credit = credit - parseFloat(this.props.getInvestmentOpeningBalance.data[0].amount);
                    openingbalcr = openingbalcr - parseFloat(this.props.getInvestmentOpeningBalance.data[0].amount);
                }
                this.props.getTDSOpeningBalance.data.forEach(d => {
                    if (d.payreceive === 'pay') {
                        tdspayable = tdspayable - parseFloat(d.amount);
                        //credit = credit - parseFloat(d.amount);
                        openingbalcr = openingbalcr + parseFloat(d.amount);
                    }
                    else if (d.payreceive === 'receive') {
                        tdsreceivable = tdsreceivable + parseFloat(d.amount);
                        //debit = debit + parseFloat(d.amount);
                        openingbaldr = openingbaldr + parseFloat(d.amount);
                    }
                })
                this.props.getPayment.data.forEach(d => {
                    if (d.specialpayment === 'partner') {
                        if (d.paymenttype === 'paymentmade') {
                            investmentaccount = {
                                ...investmentaccount,
                                [d.vendorid]: {
                                    name: investmentaccount[d.vendorid].name,
                                    amount: investmentaccount[d.vendorid].amount + parseFloat(d.amount)
                                }
                            }
                            bank = {
                                ...bank,
                                [d.bankdetail.split("...")[0]]: {
                                    name: bank[d.bankdetail.split("...")[0]].name,
                                    amount: bank[d.bankdetail.split("...")[0]].amount - parseFloat(d.amount)
                                }
                            }
                        }
                        else if (d.paymenttype === 'paymentreceived') {
                            investmentaccount = {
                                ...investmentaccount,
                                [d.customerid]: {
                                    name: investmentaccount[d.customerid].name,
                                    amount: investmentaccount[d.customerid].amount - parseFloat(d.amount)
                                }
                            }
                            bank = {
                                ...bank,
                                [d.bankdetail.split("...")[0]]: {
                                    name: bank[d.bankdetail.split("...")[0]].name,
                                    amount: bank[d.bankdetail.split("...")[0]].amount + parseFloat(d.amount)
                                }
                            }
                        }
                    }
                    else if (d.customerid === 'tdsreceived') {
                        tdsreceivable = tdsreceivable - parseFloat(d.amount);
                        //credit = credit - parseFloat(d.amount);
                        bank = {
                            ...bank,
                            [d.bankdetail.split("...")[0]]: {
                                name: bank[d.bankdetail.split("...")[0]].name,
                                amount: bank[d.bankdetail.split("...")[0]].amount + parseFloat(d.amount)
                            }
                        }
                        //debit = debit + parseFloat(d.amount);
                    }
                    else if (d.vendorid === 'paytds') {
                        tdspayable = tdspayable + parseFloat(d.amount);
                        //debit = debit + parseFloat(d.amount);
                        bank = {
                            ...bank,
                            [d.bankdetail.split("...")[0]]: {
                                name: bank[d.bankdetail.split("...")[0]].name,
                                amount: bank[d.bankdetail.split("...")[0]].amount - parseFloat(d.amount)
                            }
                        }
                        //credit = credit - parseFloat(d.amount);
                    }
                    else if (d.vendorid === 'payit') {
                        it = it + parseFloat(d.amount);
                        //debit = debit + parseFloat(d.amount);
                        bank = {
                            ...bank,
                            [d.bankdetail.split("...")[0]]: {
                                name: bank[d.bankdetail.split("...")[0]].name,
                                amount: bank[d.bankdetail.split("...")[0]].amount - parseFloat(d.amount)
                            }
                        }
                        //credit = credit - parseFloat(d.amount);
                    }
                    else if (d.vendorid === 'gst') {
                        gst = gst + parseFloat(d.amount);
                        //debit = debit + parseFloat(d.amount);
                        bank = {
                            ...bank,
                            [d.bankdetail.split("...")[0]]: {
                                name: bank[d.bankdetail.split("...")[0]].name,
                                amount: bank[d.bankdetail.split("...")[0]].amount - parseFloat(d.amount)
                            }
                        }
                        //credit = credit - parseFloat(d.amount);
                    }
                    else if (d.vendorid === 'reversecharge') {
                        gst = gst + parseFloat(d.amount);
                        //debit = debit + parseFloat(d.amount);
                        bank = {
                            ...bank,
                            [d.bankdetail.split("...")[0]]: {
                                name: bank[d.bankdetail.split("...")[0]].name,
                                amount: bank[d.bankdetail.split("...")[0]].amount - parseFloat(d.amount)
                            }
                        }
                        //credit = credit - parseFloat(d.amount);
                    }
                    // include payment or receipt from partners
                    else if (d.paymenttype === 'paymentmade') {
                        if (d.paymentmode === 'cash') {
                            vendor = {
                                ...vendor,
                                [d.vendorid]: {
                                    name: vendor[d.vendorid].name,
                                    amount: vendor[d.vendorid].amount + parseFloat(d.amount)
                                }
                            }
                            //    debit = debit + parseFloat(d.amount);
                            cash = cash - parseFloat(d.total);
                            //    credit = credit - parseFloat(d.amount);
                        }
                        else {
                            vendor = {
                                ...vendor,
                                [d.vendorid]: {
                                    name: vendor[d.vendorid].name,
                                    amount: vendor[d.vendorid].amount + parseFloat(d.amount)
                                }
                            }
                            //    debit = debit + parseFloat(d.amount);
                            bank = {
                                ...bank,
                                [d.bankdetail.split("...")[0]]: {
                                    name: bank[d.bankdetail.split("...")[0]].name,
                                    amount: bank[d.bankdetail.split("...")[0]].amount - parseFloat(d.amount)
                                }
                            }
                            //    credit = credit - parseFloat(d.amount);
                        }
                    }
                    else if (d.paymenttype === 'paymentreceived') {
                        if (d.paymentmode === 'cash') {
                            customer = {
                                ...customer,
                                [d.customerid]: {
                                    name: vendor[d.customerid].name,
                                    amount: vendor[d.customerid].amount - parseFloat(d.amount)
                                }
                            }
                            //    credit = credit - parseFloat(d.amount);
                            cash = cash + parseFloat(d.amount);
                            //    debit = debit + parseFloat(d.amount);
                        }
                        else {
                            customer = {
                                ...customer,
                                [d.customerid]: {
                                    name: customer[d.customerid].name,
                                    amount: customer[d.customerid].amount - parseFloat(d.amount)
                                }
                            }
                            //    credit = credit - parseFloat(d.amount);
                            bank = {
                                ...bank,
                                [d.bankdetail.split("...")[0]]: {
                                    name: bank[d.bankdetail.split("...")[0]].name,
                                    amount: bank[d.bankdetail.split("...")[0]].amount + parseFloat(d.amount)
                                }
                            }
                            //    debit = debit + parseFloat(d.amount);
                        }
                    }
                })
                this.props.getIncomeOS.data.forEach(d => {
                    incomeos = incomeos - d.amount;
                    //credit = credit - parseFloat(d.amount);
                    if (d.tds === "1") {
                        tdsreceivable = tdsreceivable + parseFloat(d.tdsamount);
                        customer = {
                            ...customer,
                            [d.customerid]: {
                                name: customer[d.customerid].name,
                                amount: customer[d.customerid].amount + (parseFloat(d.amount) - parseFloat(d.tdsamount))
                            }
                        }
                        //debit = debit + parseFloat(d.amount);
                    }
                    else {
                        if (d.cash === "1") {
                            cash = cash + parseFloat(d.amount);
                            //        debit = debit + parseFloat(d.amount);
                        }
                        else {
                            customer = {
                                ...customer,
                                [d.customerid]: {
                                    name: customer[d.customerid].name,
                                    amount: customer[d.customerid].amount + (parseFloat(d.amount))
                                }
                            }
                            //        debit = debit + parseFloat(d.amount);
                        }
                    }
                })
                this.props.getBankEntry.data.forEach(d => {
                    if (d.paymenttype === 'paymentmade') {
                        cash = cash - parseFloat(d.amount);
                        //    debit = debit + parseFloat(d.amount);
                        bank = {
                            ...bank,
                            [d.bankid.split("...")[0]]: {
                                name: bank[d.bankid.split("...")[0]].name,
                                amount: bank[d.bankid.split("...")[0]].amount + parseFloat(d.amount)
                            }
                        }
                        //    credit = credit - parseFloat(d.amount);
                    }
                    else if (d.paymenttype === 'paymentreceived') {
                        cash = cash + parseFloat(d.amount);
                        //    debit = debit + parseFloat(d.amount);
                        bank = {
                            ...bank,
                            [d.bankid.split("...")[0]]: {
                                name: bank[d.bankid.split("...")[0]].name,
                                amount: bank[d.bankid.split("...")[0]].amount - parseFloat(d.amount)
                            }
                        }
                        //    credit = credit - parseFloat(d.amount);
                    }
                })
                this.props.getLoan.data.forEach(d => {
                    if (d.loantype === "received") {
                        if (d.paymentmode !== 'cash') {
                            loanaccount = {
                                ...loanaccount,
                                [d.loanaccount.split("...")[0]]: {
                                    name: loanaccount[d.loanaccount.split("...")[0]].name,
                                    amount: loanaccount[d.loanaccount.split("...")[0]] - d.amount
                                }
                            }
                            //        credit = credit - parseFloat(d.amount);
                            bank = {
                                ...bank,
                                [d.bankdetail.split("...")[0]]: {
                                    name: bank[d.bankdetail.split("...")[0]].name,
                                    amount: bank[d.bankdetail.split("...")[0]].amount + parseFloat(d.total)
                                }
                            }
                            //        debit = debit + parseFloat(d.amount);
                        }
                        else {
                            loanaccount = {
                                ...loanaccount,
                                [d.loanaccount.split("...")[0]]: {
                                    name: loanaccount[d.loanaccount.split("...")[0]].name,
                                    amount: loanaccount[d.loanaccount.split("...")[0]] - parseFloat(d.amount)
                                }
                            }
                            //        credit = credit - parseFloat(d.amount);
                            cash = cash + parseFloat(d.amount);
                            //        debit = debit + parseFloat(d.amount);
                        }
                    }
                    if (d.loantype === "given") {
                        if (d.paymentmode !== 'cash') {
                            loanaccount = {
                                ...loanaccount,
                                [d.loanaccount.split("...")[0]]: {
                                    name: loanaccount[d.loanaccount.split("...")[0]].name,
                                    amount: loanaccount[d.loanaccount.split("...")[0]] + parseFloat(d.amount)
                                }
                            }
                            //        debit = debit + parseFloat(d.amount);
                            bank = {
                                ...bank,
                                [d.bankdetail.split("...")[0]]: {
                                    name: bank[d.bankdetail.split("...")[0]].name,
                                    amount: bank[d.bankdetail.split("...")[0]].amount - parseFloat(d.total)
                                }
                            }
                            //        credit = credit - parseFloat(d.amount);
                        }
                        else {
                            loanaccount = {
                                ...loanaccount,
                                [d.loanaccount.split("...")[0]]: {
                                    name: loanaccount[d.loanaccount.split("...")[0]].name,
                                    amount: loanaccount[d.loanaccount.split("...")[0]] + parseFloat(d.amount)
                                }
                            }
                            //        debit = debit + parseFloat(d.amount);
                            cash = cash - parseFloat(d.amount);
                            //        credit = credit - parseFloat(d.amount);
                        }
                    }
                })
                this.props.getInvestment.data.forEach(d => {
                    if (d.activity === 'deposit') {
                        if (business.businesstype === 'partnership') {
                            investmentaccount = {
                                ...investmentaccount,
                                [d.investmentaccount.split("...")[0]]: {
                                    name: investmentaccount[d.investmentaccount.split("...")[0]].name,
                                    amount: investmentaccount[d.investmentaccount.split("...")[0]].amount - parseFloat(d.amount) - parseFloat(d.extraamount)
                                }
                            }
                            //        credit = credit - parseFloat(d.amount) - parseFloat(d.extraamount);
                            bank = {
                                ...bank,
                                [d.bankdetail.split("...")[0]]: {
                                    name: bank[d.bankdetail.split("...")[0]].name,
                                    amount: bank[d.bankdetail.split("...")[0]].amount + parseFloat(d.amount) + parseFloat(d.extraamount)
                                }
                            }
                            //        debit = debit + parseFloat(d.amount) + parseFloat(d.extraamount);
                        }
                        else if (business.businesstype === 'propritorship') {
                            capitalaccount = capitalaccount - parseFloat(d.amount) - parseFloat(d.extraamount);
                            credit = credit - parseFloat(d.amount) - parseFloat(d.extraamount);
                            bank = {
                                ...bank,
                                [d.bankdetail.split("...")[0]]: {
                                    name: bank[d.bankdetail.split("...")[0]].name,
                                    amount: bank[d.bankdetail.split("...")[0]].amount + parseFloat(d.amount) + parseFloat(d.extraamount)
                                }
                            }
                            //        debit = debit + parseFloat(d.amount) + parseFloat(d.extraamount);
                        }
                        else if (business.businesstype === 'ltd') {
                            equitycapital = equitycapital - parseFloat(d.amount);
                            shareholderreserves = shareholderreserves - parseFloat(d.extraamount);
                            credit = credit - parseFloat(d.amount) - parseFloat(d.extraamount);
                            bank = {
                                ...bank,
                                [d.bankdetail.split("...")[0]]: {
                                    name: bank[d.bankdetail.split("...")[0]].name,
                                    amount: bank[d.bankdetail.split("...")[0]].amount + parseFloat(d.amount) + parseFloat(d.extraamount)
                                }
                            }
                            //        debit = debit + parseFloat(d.amount) + parseFloat(d.extraamount);
                        }
                    }
                    else if (d.activity === 'drawing') {
                        investmentaccount = {
                            ...investmentaccount,
                            [d.investmentaccount.split("...")[0]]: {
                                name: investmentaccount[d.investmentaccount.split("...")[0]].name,
                                amount: investmentaccount[d.investmentaccount.split("...")[0]].amount + parseFloat(d.amount) + parseFloat(d.extraamount)
                            }
                        }
                        //    debit = debit + parseFloat(d.amount) + parseFloat(d.extraamount);
                        bank = {
                            ...bank,
                            [d.bankdetail.split("...")[0]]: {
                                name: bank[d.bankdetail.split("...")[0]].name,
                                amount: bank[d.bankdetail.split("...")[0]].amount - parseFloat(d.amount) - parseFloat(d.extraamount)
                            }
                        }
                        //    credit = credit - parseFloat(d.amount) + parseFloat(d.extraamount);
                    }
                })
                this.props.getAssetAccount.data.forEach(d => {
                    if ((d.sold === '1') && (d.soldfyid === this.props.fyid)) {
                        if (d.customerid === 'cash') {
                            assetaccount = {
                                ...assetaccount,
                                [d.id]: {
                                    name: assetaccount[d.id].name,
                                    amount: assetaccount[d.id].amount - parseFloat(d.soldbasic)
                                }
                            }
                            gst = gst - parseFloat(d.soldigst);
                            gst = gst - parseFloat(d.soldcgst);
                            gst = gst - parseFloat(d.soldsgst);
                            //        credit = credit - parseFloat(d.soldbasic) - parseFloat(d.soldigst) - parseFloat(d.soldsgst) - parseFloat(d.soldcgst);
                            cash = cash + parseFloat(d.soldtotal);
                            //        debit = debit + parseFloat(d.soldtotal);
                        }
                        else {
                            assetaccount = {
                                ...assetaccount,
                                [d.id]: {
                                    name: assetaccount[d.id].name,
                                    amount: assetaccount[d.id].amount - parseFloat(d.soldbasic)
                                }
                            }
                            gst = gst - parseFloat(d.soldigst);
                            gst = gst - parseFloat(d.soldcgst);
                            gst = gst - parseFloat(d.soldsgst);
                            //        credit = credit - parseFloat(d.soldigst) - parseFloat(d.soldcgst) - parseFloat(d.soldsgst) - parseFloat(d.soldbasic);
                            customer = {
                                ...customer,
                                [d.customerid.split("...")[0]]: {
                                    name: customer[d.customerid.split("...")[0]].name,
                                    amount: customer[d.customerid.split("...")[0]].amount + (parseFloat(d.soldtotal))
                                }
                            }
                            //        debit = debit + parseFloat(d.soldtotal);
                        }
                    }
                    else if ((d.purchase === '1') && (d.purchasefyid === this.props.fyid)) {
                        assetaccount = {
                            ...assetaccount,
                            [d.id]: {
                                name: assetaccount[d.id].name,
                                amount: assetaccount[d.id] + parseFloat(d.purchasebasic)
                            }
                        }
                        gst = gst + parseFloat(d.purchaseigst);
                        gst = gst + parseFloat(d.purchasecgst);
                        gst = gst + parseFloat(d.purchasesgst);
                        //    debit = debit + parseFloat(d.purchasebasic) + parseFloat(d.purchaseigst) + parseFloat(d.purchasecgst) + parseFloat(d.purchasesgst);
                        vendor = {
                            ...vendor,
                            [d.vendorid.split("...")[0]]: {
                                name: vendor[d.vendorid.split("...")[0]].name,
                                amount: vendor[d.vendorid.split("...")[0]].amount - (parseFloat(d.purchasetotal))
                            }
                        }
                        //    credit = credit - parseFloat(d.purchasetotal);
                    }
                })
                this.props.getDepreciation.data.forEach(d => {
                    assetaccount = {
                        ...assetaccount,
                        [d.asset]: {
                            name: assetaccount[d.asset].name,
                            amount: assetaccount[d.asset].amount - parseFloat(d.amount)
                        }
                    }
                    // credit = credit - parseFloat(d.amount);
                    depreciation = depreciation + parseFloat(d.amount);
                    debit = debit + parseFloat(d.amount);
                })

                profit = -1 * (parseFloat(sales) + parseFloat(closingStock) + parseFloat(incomeos) + parseFloat(totalassetprofit) + parseFloat(expense) + parseFloat(purchase) + parseFloat(openingStock) + parseFloat(depreciation));
                if (profit > 0) debit = debit + parseFloat(profit);
                else if (profit < 0) credit = credit + parseFloat(profit);

                Object.keys(vendor).forEach(v => {
                    if (vendor[v].amount > 0) debit = debit + parseFloat(vendor[v].amount);
                    else if (vendor[v].amount < 0) credit = credit + parseFloat(vendor[v].amount);
                });

                Object.keys(customer).forEach(c => {
                    if (customer[c].amount > 0) debit = debit + parseFloat(customer[c].amount);
                    else if (customer[c].amount < 0) credit = credit + parseFloat(customer[c].amount);
                });

                Object.keys(bank).forEach(b => {
                    if (bank[b].amount > 0) debit = debit + parseFloat(bank[b].amount);
                    else if (bank[b].amount < 0) credit = credit + parseFloat(bank[b].amount);
                });
                Object.keys(loanaccount).forEach(l => {
                    if (loanaccount[l].amount > 0) debit = debit + parseFloat(loanaccount[l].amount);
                    else if (loanaccount[l].amount < 0) credit = credit + parseFloat(loanaccount[l].amount);
                });
                Object.keys(assetaccount).forEach(a => {
                    if (assetaccount[a].amount > 0) debit = debit + parseFloat(assetaccount[a].amount);
                    else if (assetaccount[a].amount < 0) credit = credit + parseFloat(assetaccount[a].amount);
                });
                Object.keys(investmentaccount).forEach(k => {
                    let holding = (this.props.getInvestmentAccount.data.filter(d => d.id === k)[0].shareholding) / 100;
                    let amount = investmentaccount[k].amount - (profit * holding);
                    if (amount < 0) credit = credit + parseFloat(amount);
                    else if (amount > 0) debit = debit + parseFloat(amount);
                })
                if (cash > 0) debit = debit + parseFloat(cash);
                else if (cash < 0) credit = credit + parseFloat(cash);

                if (tdspayable > 0) debit = debit + parseFloat(tdspayable);
                else if (tdspayable < 0) credit = credit + parseFloat(tdspayable)

                if (tdsreceivable > 0) debit = debit + parseFloat(tdsreceivable);
                else if (tdsreceivable < 0) credit = credit + parseFloat(tdsreceivable);

                if (equitycapital > 0) debit = debit + parseFloat(equitycapital);
                if (equitycapital < 0) credit = credit + parseFloat(equitycapital);

                if (shareholderreserves > 0) debit = debit + parseFloat(shareholderreserves);
                if (shareholderreserves < 0) credit = credit + parseFloat(shareholderreserves);

                if (it > 0) debit = debit + parseFloat(it);
                else if (it < 0) credit = credit + parseFloat(it);

                if (openingbaldr > openingbalcr) credit = credit - (parseFloat(openingbaldr) - parseFloat(openingbalcr));
                else if (openingbalcr > openingbaldr) debit = debit + parseFloat(openingbalcr) - parseFloat(openingbaldr);
                if (gst > 0) debit = debit + parseFloat(gst);
                else if (gst < 0) credit = credit + parseFloat(gst);

                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div style={{ minHeight: "93vh", display: "flex", flexDirection: "column" }}>
                            <div className="container mt-4" style={{ flex: "1" }}>
                                <div className="d-flex justify-content-center">
                                    <div className="col-12 col-md-6 p-0">
                                        <h6 className="text-center pl-2 pr-2 pt-4" style={{ lineHeight: "1" }}><strong>{business.businessname} {fy}</strong></h6>
                                        <h6 className="text-center pl-2 pr-2 mb-4" style={{ lineHeight: "1" }}>{business.alias ? "(" + business.alias + ")" : ''}</h6>
                                        <h6 className="text-center"><u>Trial Balance</u>&nbsp;&nbsp;&nbsp;<FontAwesomeIcon icon={faFilePdf} size="lg" onClick={() => this.generatePdf(sales, gst, tdspayable, tdsreceivable, customer, purchase, expense, vendor, cash, bank, incomeos, reversecharge, loanaccount, investmentaccount, assetaccount, depreciation, capitalaccount, openingbaldr, openingbalcr, totalassetprofit, assetpresent, asset, flag, equitycapital, shareholderreserves, profit, it, debit, credit, business, fy, this.props.getInvestmentAccount)} />&nbsp;&nbsp;&nbsp;</h6>
                                        <div>
                                            <div className="row col-12 small border-bottom">
                                                <div className="d-flex d-md-none mt-1">
                                                    <div className="col-6 col-md-5 p-1 text-left">Particulars</div>
                                                    <div className="col-3 col-md-1 text-right"><strong>Debit</strong></div>
                                                    <div className="col-3 col-md-6 p-1 text-right"><strong>Credit</strong></div>
                                                </div>
                                                <div className="col-12 mt-2 d-none d-md-block p-0">
                                                    <div className="d-flex">
                                                        <div className="col-6 p-1 text-left"><strong>Particulars</strong></div>
                                                        <div className="col-3 text-right"><strong>Debit</strong></div>
                                                        <div className="col-3 text-right"><strong>Credit</strong></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="row small mb-1 p-0 d-md-none d-lg-none">
                                                <div className="row col-12">
                                                    <div className="col-6 text-left"><strong>{sales !== 0 ? "Sales" : ''}</strong></div>
                                                    <div className="col-3 text-right">{sales > 0 ? parseFloat(sales).toFixed(2) : ''}</div>
                                                    <div className="col-3 text-right">{sales < 0 ? parseFloat(-1 * sales).toFixed(2) : ''}</div>
                                                </div>
                                                <div className="row col-12">
                                                    <div className="col-6 text-left"><strong>{purchase !== 0 ? "Purchase" : ''}</strong></div>
                                                    <div className="col-3 text-right">{purchase > 0 ? parseFloat(purchase).toFixed(2) : ''}</div>
                                                    <div className="col-3 text-right">{purchase < 0 ? parseFloat(-1 * purchase).toFixed(2) : ''}</div>
                                                </div>
                                                <div className="row col-12">
                                                    <div className="col-6 text-left"><strong>{expense !== 0 ? "Expense" : ''}</strong></div>
                                                    <div className="col-3 text-right">{expense > 0 ? parseFloat(expense).toFixed(2) : ''}</div>
                                                    <div className="col-3 text-right">{expense < 0 ? parseFloat(-1 * expense).toFixed(2) : ''}</div>
                                                </div>
                                                <div className="row col-12">
                                                    <div className="col-6 text-left"><strong>{incomeos !== 0 ? "Income - OS" : ''}</strong></div>
                                                    <div className="col-3 text-right">{incomeos > 0 ? parseFloat(incomeos).toFixed(2) : ''}</div>
                                                    <div className="col-3 text-right">{incomeos < 0 ? parseFloat(-1 * incomeos).toFixed(2) : ''}</div>
                                                </div>
                                                <div className="row col-12">
                                                    <div className="col-6 text-left"><strong>{cash !== 0 ? "Cash" : ''}</strong></div>
                                                    <div className="col-3 text-right">{cash > 0 ? parseFloat(cash).toFixed(2) : ''}</div>
                                                    <div className="col-3 text-right">{cash < 0 ? parseFloat(-1 * cash).toFixed(2) : ''}</div>
                                                </div>
                                                <div className="row col-12">
                                                    <div className="col-6 text-left"><strong>{gst !== 0 ? "GST" : ''}</strong></div>
                                                    <div className="col-3 text-right">{gst > 0 ? parseFloat(gst).toFixed(2) : ''}</div>
                                                    <div className="col-3 text-right">{gst < 0 ? parseFloat(-1 * gst).toFixed(2) : ''}</div>
                                                </div>
                                                <div className="row col-12">
                                                    <div className="col-6 text-left"><strong>{tdspayable !== 0 ? "TDS Payable" : ''}</strong></div>
                                                    <div className="col-3 text-right">{tdspayable > 0 ? parseFloat(tdspayable).toFixed(2) : ''}</div>
                                                    <div className="col-3 text-right">{tdspayable < 0 ? parseFloat(-1 * tdspayable).toFixed(2) : ''}</div>
                                                </div>
                                                <div className="row col-12">
                                                    <div className="col-6 text-left"><strong>{tdsreceivable !== 0 ? "TDS Receivable" : ''}</strong></div>
                                                    <div className="col-3 text-right">{tdsreceivable > 0 ? parseFloat(tdsreceivable).toFixed(2) : ''}</div>
                                                    <div className="col-3 text-right">{tdsreceivable < 0 ? parseFloat(-1 * tdsreceivable).toFixed(2) : ''}</div>
                                                </div>
                                                <div className="row col-12">
                                                    <div className="col-6 text-left"><strong>{it !== 0 ? "IncomeTax" : ''}</strong></div>
                                                    <div className="col-3 text-right">{it > 0 ? parseFloat(it).toFixed(2) : ''}</div>
                                                    <div className="col-3 text-right">{it < 0 ? parseFloat(-1 * it).toFixed(2) : ''}</div>
                                                </div>
                                                <div className="row col-12">
                                                    <div className="col-6 text-left"><strong>{depreciation !== 0 ? "Depreciation" : ''}</strong></div>
                                                    <div className="col-3 text-right">{depreciation > 0 ? parseFloat(depreciation).toFixed(2) : ''}</div>
                                                    <div className="col-3 text-right"><strong>{depreciation < 0 ? parseFloat(-1 * depreciation).toFixed(2) : ''}</strong></div>
                                                </div>
                                                <div className="row col-12">
                                                    <div className="col-6 text-left"><strong>{openingbaldr !== openingbalcr ? "Diff In Opening Bal" : ''}</strong></div>
                                                    <div className="col-3 text-right">{openingbaldr < openingbalcr ? parseFloat(openingbalcr - openingbaldr).toFixed(2) : ''}</div>
                                                    <div className="col-3 text-right">{openingbaldr > openingbalcr ? parseFloat(openingbaldr - openingbalcr).toFixed(2) : ''}</div>
                                                </div>
                                                {business.businesstype === 'partnership' || business.businesstype === 'propritorship' ? <div className="row col-12">
                                                    <div className="col-6 text-left"><strong>{profit !== 0 && profit > 0 ? "Profit" : profit !== 0 && profit < 0 ? "Loss" : ''}</strong></div>
                                                    <div className="col-3 text-right">{profit !== 0 && profit > 0 ? parseFloat(profit).toFixed(2) : ''}</div>
                                                    <div className="col-3 text-right">{profit !== 0 && profit < 0 ? parseFloat(-1 * profit).toFixed(2) : ''}</div>
                                                </div> : ''}
                                                <div className="row col-12">
                                                    <Customer customer={customer} />
                                                </div>
                                                <div className="row col-12">
                                                    <Vendor vendor={vendor} />
                                                </div>
                                                <div className="row col-12">
                                                    <Bank bank={bank} />
                                                </div>
                                                <div className="row col-12 ">
                                                    <Loan loan={loanaccount} />
                                                </div>
                                                {business.businesstype === 'partnership' ? <div className="row col-12">
                                                    <Investment investment={investmentaccount} getInvestmentAccount={this.props.getInvestmentAccount} profit={profit} />
                                                </div> : ''}
                                                {business.businesstype === 'propritorship' && capitalaccount !== 0 ? <div className="row col-12">
                                                    <div className="col-6 text-left"><strong>Capital A/C</strong></div>
                                                    <div className="col-6 text-left">{capitalaccount > 0 ? parseFloat(capitalaccount).toFixed(2) : ''}</div>
                                                    <div className="col-6 text-left">{capitalaccount < 0 ? parseFloat(-1 * capitalaccount).toFixed(2) : ''}</div>
                                                </div> : ''}
                                                {business.businesstype === 'ltd' && equitycapital !== 0 ? <div className="row col-12">
                                                    <div className="col-6 text-left"><strong>Equity Capital Fund</strong></div>
                                                    <div className="col-6 text-left">{equitycapital > 0 ? parseFloat(equitycapital).toFixed(2) : ''}</div>
                                                    <div className="col-6 text-left">{equitycapital < 0 ? parseFloat(-1 * equitycapital).toFixed(2) : ''}</div>
                                                </div> : ''}
                                                {business.businesstype === 'ltd' && shareholderreserves !== 0 ? <div className="row col-12">
                                                    <div className="col-6 text-left"><strong>ShareHolders Reserves Fund</strong></div>
                                                    <div className="col-6 text-left">{shareholderreserves > 0 ? parseFloat(shareholderreserves).toFixed(2) : ''}</div>
                                                    <div className="col-6 text-left">{shareholderreserves < 0 ? parseFloat(-1 * shareholderreserves).toFixed(2) : ''}</div>
                                                </div> : ''}
                                                <div className="row col-12">
                                                    <Asset asset={assetaccount} />
                                                </div>
                                            </div>
                                            <div className="container d-none d-md-block small p-1" >
                                                <div className="row ">
                                                    <div className="row col-12">
                                                        <div className="col-6 text-left"><strong>{sales !== 0 ? "Sales" : ''}</strong></div>
                                                        <div className="col-3 text-right">{sales > 0 ? parseFloat(sales).toFixed(2) : ''}</div>
                                                        <div className="col-3 text-right">{sales < 0 ? parseFloat(-1 * sales).toFixed(2) : ''}</div>
                                                    </div>
                                                    <div className="row col-12">
                                                        <div className="col-6 text-left"><strong>{purchase !== 0 ? "Purchase" : ''}</strong></div>
                                                        <div className="col-3 text-right">{purchase > 0 ? parseFloat(purchase).toFixed(2) : ''}</div>
                                                        <div className="col-3 text-right">{purchase < 0 ? parseFloat(-1 * purchase).toFixed(2) : ''}</div>
                                                    </div>
                                                    <div className="row col-12">
                                                        <div className="col-6 text-left"><strong>{expense !== 0 ? "Expense" : ''}</strong></div>
                                                        <div className="col-3 text-right">{expense > 0 ? parseFloat(expense).toFixed(2) : ''}</div>
                                                        <div className="col-3 text-right">{expense < 0 ? parseFloat(-1 * expense).toFixed(2) : ''}</div>
                                                    </div>
                                                    <div className="row col-12">
                                                        <div className="col-6 text-left"><strong>{incomeos !== 0 ? "Income - OS" : ''}</strong></div>
                                                        <div className="col-3 text-right">{incomeos > 0 ? parseFloat(incomeos).toFixed(2) : ''}</div>
                                                        <div className="col-3 text-right">{incomeos < 0 ? parseFloat(-1 * incomeos).toFixed(2) : ''}</div>
                                                    </div>
                                                    <div className="row col-12">
                                                        <div className="col-6 text-left"><strong>{cash !== 0 ? "Cash" : ''}</strong></div>
                                                        <div className="col-3 text-right">{cash > 0 ? parseFloat(cash).toFixed(2) : ''}</div>
                                                        <div className="col-3 text-right">{cash < 0 ? parseFloat(-1 * cash).toFixed(2) : ''}</div>
                                                    </div>
                                                    <div className="row col-12">
                                                        <div className="col-6 text-left"><strong>{tdspayable !== 0 ? "TDS Payable" : ''}</strong></div>
                                                        <div className="col-3 text-right">{tdspayable > 0 ? parseFloat(tdspayable).toFixed(2) : ''}</div>
                                                        <div className="col-3 text-right">{tdspayable < 0 ? parseFloat(-1 * tdspayable).toFixed(2) : ''}</div>
                                                    </div>
                                                    <div className="row col-12">
                                                        <div className="col-6 text-left"><strong>{tdsreceivable !== 0 ? "TDS Receivable" : ''}</strong></div>
                                                        <div className="col-3 text-right">{tdsreceivable > 0 ? parseFloat(tdsreceivable).toFixed(2) : ''}</div>
                                                        <div className="col-3 text-right">{tdsreceivable < 0 ? parseFloat(-1 * tdsreceivable).toFixed(2) : ''}</div>
                                                    </div>
                                                    <div className="row col-12">
                                                        <div className="col-6 text-left"><strong>{it !== 0 ? "IncomeTax" : ''}</strong></div>
                                                        <div className="col-3 text-right">{it > 0 ? parseFloat(it).toFixed(2) : ''}</div>
                                                        <div className="col-3 text-right">{it < 0 ? parseFloat(-1 * it).toFixed(2) : ''}</div>
                                                    </div>
                                                    <div className="row col-12">
                                                        <div className="col-6 text-left"><strong>{gst !== 0 ? "GST" : ''}</strong></div>
                                                        <div className="col-3 text-right">{gst > 0 ? parseFloat(gst).toFixed(2) : ''}</div>
                                                        <div className="col-3 text-right">{gst < 0 ? parseFloat(-1 * gst).toFixed(2) : ''}</div>
                                                    </div>
                                                    <div className="row col-12">
                                                        <div className="col-6 text-left"><strong>{depreciation !== 0 ? "Depreciation" : ''}</strong></div>
                                                        <div className="col-3 text-right">{depreciation > 0 ? parseFloat(depreciation).toFixed(2) : ''}</div>
                                                        <div className="col-3 text-right">{depreciation < 0 ? parseFloat(-1 * depreciation).toFixed(2) : ''}</div>
                                                    </div>
                                                    <div className="row col-12">
                                                        <div className="col-6 text-left"><strong>{openingbaldr !== openingbalcr ? "Difference In Opening Balance" : ''}</strong></div>
                                                        <div className="col-3 text-right">{openingbaldr < openingbalcr ? parseFloat(openingbalcr - openingbaldr).toFixed(2) : ''}</div>
                                                        <div className="col-3 text-right">{openingbaldr > openingbalcr ? parseFloat(openingbaldr - openingbalcr).toFixed(2) : ''}</div>
                                                    </div>
                                                    {business.businesstype === 'partnership' || business.businesstype === 'propritorship' ? <div className="row col-12">
                                                        <div className="col-6 text-left"><strong>{profit > 0 ? "Profit" : profit < 0 ? "Loss" : ''}</strong></div>
                                                        <div className="col-3 text-right">{profit > 0 ? parseFloat(profit).toFixed(2) : ''}</div>
                                                        <div className="col-3 text-right">{profit < 0 ? parseFloat(-1 * profit).toFixed(2) : ''}</div>
                                                    </div> : ''}
                                                    <div className="row col-12">
                                                        <Customer customer={customer} />
                                                    </div>
                                                    <div className="row col-12">
                                                        <Vendor vendor={vendor} />
                                                    </div>
                                                    <div className="row col-12">
                                                        <Bank bank={bank} />
                                                    </div>
                                                    <div className="row col-12">
                                                        <Loan loan={loanaccount} />
                                                    </div>
                                                    {business.businesstype === 'partnership' ? <div className="row col-12">
                                                        <Investment investment={investmentaccount} getInvestmentAccount={this.props.getInvestmentAccount} profit={profit} />
                                                    </div> : ''}
                                                    {business.businesstype === 'propritorship' ? <div className="row col-12">
                                                        <div className="col-6 text-left"><strong>Capital A/C</strong></div>
                                                        <div className="col-6 text-left">{capitalaccount > 0 ? parseFloat(capitalaccount).toFixed(2) : ''}</div>
                                                        <div className="col-6 text-left">{capitalaccount < 0 ? parseFloat(-1 * capitalaccount).toFixed(2) : ''}</div>
                                                    </div> : ''}
                                                    {business.businesstype === 'ltd' && equitycapital !== 0 ? <div className="row col-12">
                                                        <div className="col-6 text-left"><strong>Equity Capital Fund</strong></div>
                                                        <div className="col-3 text-left">{equitycapital > 0 ? parseFloat(equitycapital).toFixed(2) : ''}</div>
                                                        <div className="col-3 text-left">{equitycapital < 0 ? parseFloat(-1 * equitycapital).toFixed(2) : ''}</div>
                                                    </div> : ''}
                                                    {business.businesstype === 'ltd' && shareholderreserves !== 0 ? <div className="row col-12">
                                                        <div className="col-6 text-left"><strong>ShareHolders Reserves Fund</strong></div>
                                                        <div className="col-3 text-left">{shareholderreserves > 0 ? parseFloat(shareholderreserves).toFixed(2) : ''}</div>
                                                        <div className="col-3 text-left">{shareholderreserves < 0 ? parseFloat(-1 * shareholderreserves).toFixed(2) : ''}</div>
                                                    </div> : ''}
                                                    <div className="row col-12">
                                                        <Asset asset={assetaccount} />
                                                    </div>
                                                    <div className="row col-12">
                                                        <div className="d-flex d-md-none mt-1">
                                                            <div className="col-6 p-1"><strong></strong></div>
                                                            <div className="col-3 p-0 border-top border-dark text-right"><strong>{parseFloat(debit).toFixed(2)}</strong></div>
                                                            <div className="col-3 p-0 border-top border-dark text-right"><strong>{parseFloat(credit * -1).toFixed(2)}</strong></div>
                                                        </div>
                                                        <div className="col-12 mt-1 d-none d-md-block p-0">
                                                            <div className="d-flex">
                                                                <div className="col-6 p-0"><strong></strong></div>
                                                                <div className="col-3 border-top border-dark text-right"><strong>{parseFloat(debit).toFixed(2)}</strong></div>
                                                                <div className="col-3 border-top border-dark text-right"><strong>{parseFloat(credit * -1).toFixed(2)}</strong><strong></strong></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div></div>
                                    </div>
                                </div>
                            </div>
                            <Footer />
                        </div>
                    </div>
                );
            }
        }
    }
}

export default TrialBalance;