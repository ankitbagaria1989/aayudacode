import React, { Component } from 'react';
import cookie from 'react-cookie';
import { Redirect } from 'react-router-dom';
import { Header } from './HeaderComponent';
import { Footer } from './Footer';

const GetEmployeeList = function ({ getEmployee, completeDate, state, handleInputChange }) {
    let returnrows = [];
    let i = 1;
    getEmployee.data.forEach(e => {
        if ((new Date(e.doj) <= new Date(completeDate) && (e.active === 1)) || (new Date(e.doj) <= new Date(completeDate) && (e.active === 0) && (new Date(e.dot) > new Date(completeDate)))) {
            if (i % 2 !== 0) {
                returnrows.push(
                    <div className="form-group col-12 mb-1 d-flex p-2" style={{ backgroundColor: "rgb(181, 253, 233)" }}>
                        <div htmlFor={"present" + e.employeeid} className="mb-0 muted-text col-8 p-0">{e.employeename}</div>
                        <div className="col-4 text-right p-0">
                            <input name={"present" + e.employeeid} type="checkbox" checked={state["present" + e.employeeid]} onChange={handleInputChange} />&nbsp;&nbsp;
                            <input name={"ot" + e.employeeid} type="text" value={state["ot" + e.employeeid]} onChange={handleInputChange} size="2" />
                        </div>
                    </div>
                )
            }
            else returnrows.push(
                <div className="form-group col-12 mb-1 d-flex p-2">
                    <div className="mb-0 muted-text col-8 p-0">{e.employeename}</div>
                    <div className="col-4 text-right p-0">
                        <input name={"present" + e.employeeid} type="checkbox" checked={state["present" + e.employeeid]} onChange={handleInputChange} />&nbsp;&nbsp;
                        <input name={"ot" + e.employeeid} type="text" value={state["ot" + e.employeeid]} onChange={handleInputChange} size="2" />
                    </div>
                </div>
            )
            i++;
        }
    })
    if (returnrows.length === 0) returnrows.push(<div className="text-center text-muted">No Employees having DOJ on or before {completeDate}</div>)
    else returnrows.push(<div className="mt-2"><button type="submit" className="btn btn-success btn-sm pl-4 pr-4 pt-2 pb-2 mb-5" >Mark Attendance</button></div>)
    return returnrows;
}
const Attendance = function ({ getEmployee, state, handleInputChange, completeDate, handleSubmit }) {
    return (
        <form onSubmit={handleSubmit}>
            <div className>
                <div className="col-12 p-0">
                    <GetEmployeeList getEmployee={getEmployee} completeDate={completeDate} state={state} handleInputChange={handleInputChange} />
                </div>
            </div>
        </form>
    )
}
class MarkAttendance extends Component {
    constructor(props) {
        super(props);
        this.state = {};

    }
    handleInputChange = (event) => {
        let target = event.target;
        let name = target.name;
        let value = target.value;
        if (/^present/i.test(name)) {
            this.setState({
                ...this.state,
                [name]: !this.state[name]
            })
        }
        else this.setState({
            ...this.state,
            [name]: value
        })
    }
    handleSubmit = () => {
        this.props.updateAttendance(this.props.fyid, this.props.completeDate, this.state);
    }
    componentDidMount() {
        window.onload = () => {
            this.props.refreshEmployeeState();
        }
        if ((this.props.getEmployee.message === 'initial') && (this.props.getEmployee.loading === false)) this.props.getEmployeeList(this.props.businessid);
        if (this.props.getAttendance.message === 'initial') this.props.getAttendanceList(this.props.fyid, this.props.completeDate);
        if (this.props.logoutMessage.error !== '') {
            alert("Error Logging Out. Please Try Again");
            this.props.resetLogoutMessage();
        }
        this.props.getEmployee.data.forEach(e => {
            this.setState({
                ["present" + e.employeeid]: false,
                ["ot" + e.employeeid]: 0
            })
        });
        if (this.props.updateAttendanceMessage.message === 'success') {
            alert("Attendance Update Successfully");
            this.props.resetUpdateAttendanceMessage();
        }
        else if (this.props.updateAttendanceMessage.error !== '') {
            alert("Error Updating Attendance. Please Try Again.");
            this.setState({
                ...this.props.updateAttendanceMessage.state
            })
            this.props.resetUpdateAttendanceMessage();
        }
        if (this.props.getAttendance.data.length > 0) {
            let attendanceobject = JSON.parse(this.props.getAttendance.data[0].attendanceobject);
            let keys = Object.keys(attendanceobject);
            for (let i = 0; i < keys.length; i++) {
                this.setState({
                    ...this.state,
                    ["present" + keys[i]]: attendanceobject[keys[i]].present,
                    ["ot" + keys[i]]: attendanceobject[keys[i]].ot
                })
            }
        }
    }
    render() {
        if (!cookie.load('userservice')) return <Redirect to="/" />
        else {
            if ((this.props.logoutMessage.loading === true) || (this.props.getEmployee.loading === true) || (this.props.updateAttendanceMessage.loading === true) || (this.props.getAttendance.loading === true)) {
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div className="mt-3 d-flex justify-content-center">
                            <button className="btn btn-danger">Loading... </button>
                        </div>
                    </div>
                );
            }
            else if ((this.props.getEmployee.error !== "") || (this.props.getAttendance.error != '')) {
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div className="mt-3 d-flex justify-content-center">
                            <button className="btn btn-danger">Error. Please Refresh and Try Again</button>
                        </div>
                    </div>
                );
            }
            else {
                let business = this.props.businessList.data.filter(b => b.businessid === this.props.businessid)[0];
                let fy = business.financial.filter(f => f.fyid === this.props.fyid)[0].financialyear;
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div style={{ minHeight: "93vh", display: "flex", flexDirection: "column" }}>
                            <div className="row" style={{ flex: "1" }}>
                                <div className="container">
                                    <h6 className="text-center pl-2 pr-2 pt-4" style={{ lineHeight: "1" }}><strong>{business.businessname} {fy}</strong></h6>
                                    <h6 className="text-center pl-2 pr-2 mb-4" style={{ lineHeight: "1" }}>{business.alias ? "(" + business.alias + ")" : ''}</h6>
                                    <h6 className="text-center mb-4"><u>Attendance - {this.props.completeDate.toUpperCase()}</u></h6>
                                    <div className="col-12 d-flex justify-content-center">
                                        <div className="col-12 col-md-6">
                                            <Attendance getEmployee={this.props.getEmployee} state={this.state} handleInputChange={this.handleInputChange} completeDate={this.props.completeDate} handleSubmit={this.handleSubmit} />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <Footer />
                        </div>
                    </div >
                )
            }
        }
    }
}

export default MarkAttendance;