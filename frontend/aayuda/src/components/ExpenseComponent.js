import React, { Component } from 'react';
import cookie from 'react-cookie';
import { Redirect } from 'react-router-dom';
import { LocalForm, Control } from 'react-redux-form';
import { Header } from './HeaderComponent';
import { Footer } from './Footer';

class Expense extends Component {
    constructor(props) {
        super(props);
        this.state = ({
            vendorid: '',
            cash: false,
            expensecategory: '',
            expensedescription: '',
            voucherno: this.props.getExpense.voucherno ? this.props.getExpense.voucherno : 1,
            date: new Date().toDateString().split(' ').slice(1).join(' '),
            amount: 0,
            tds: false,
            tdsamount: 0,
            it: false,
            partner: false,
            employee: false
        })
        window.onload = () => {
            this.props.resetExpenseDetail();
            this.props.refreshVendorState();
            this.props.refreshExpenseHead();
            this.props.refreshBankState();
            this.props.refreshInvestmentAccountState();
            this.props.refreshEmployeeState();
        }
    }
    componentDidMount() {
        if (this.props.addExpenseMessage.message === 'success') {
            alert("Expense Added Successfully");
            this.props.resetaddExpenseMessage('success');
        }
        else if (this.props.addExpenseMessage.message === 'Already Registered') {
            alert("Voucher Number " + parseInt(this.props.getExpense.voucherno) - 1 + " already registered");
            this.props.resetaddExpenseMessage('success');
        }
        else if (this.props.addExpenseMessage.error !== '') {
            alert(this.props.addExpenseMessage.error);
            this.setState({
                ...this.props.addExpenseMessage.state,
            })
            this.props.resetaddExpenseMessage('error');
        }
        if (this.props.logoutMessage.error !== '') {
            alert("Error Logging Out. Please Try Again");
            this.props.resetLogoutMessage();
        }
        if (this.props.getVendor.message === 'initial') this.props.getVendorList(this.props.businessid);
        if (this.props.getBankDetail.message === 'initial') this.props.getBankDetailList(this.props.businessid);
        if (this.props.getExpense.message === 'initial') this.props.getExpenseDetail(this.props.businessid, this.props.fyid);
        if (this.props.getExpenseHead.message === 'initial') this.props.getExpenseHeadList(this.props.businessid);
        if (this.props.getInvestmentAccount.message === 'initial') this.props.getInvestmentAccountList(this.props.businessid);
        if (this.props.getEmployee.message === 'initial') this.props.getEmployeeList(this.props.businessid);
    }
    handleInputChange = (event) => {
        let target = event.target;
        let name = target.name;
        let value = target.value;
        if (name === 'tds') {
            this.setState({
                ...this.state,
                tds: !this.state.tds,
                cash: false,
                it: false,
                partner: false,
                employee: this.state.employee
            })
        }
        else if (name === 'cash') {
            this.setState({
                ...this.state,
                cash: !this.state.cash,
                tds: false,
                it: false,
                partner: false,
                employee: false
            })
        }
        else if (name === 'it') {
            this.setState({
                ...this.state,
                it: !this.state.it,
                tds: false,
                cash: false,
                partner: false,
                employee: false
            })
        }
        else if (name === 'partner') {
            this.setState({
                ...this.state,
                it: false,
                tds: false,
                cash: false,
                partner: !this.state.partner,
                employee: false
            })
        }
        else if (name === 'employee') {
            this.setState({
                ...this.state,
                it: false,
                tds: this.state.tds,
                cash: false,
                partner: false,
                employee: !this.state.employee
            })
        }
        else {
            this.setState({
                ...this.state,
                [name]: value
            })
        }
    }
    handleSubmit = () => {
        let business = this.props.businessList.data.filter(b => b.businessid === this.props.businessid)[0];
        let fy = business.financial.filter(f => f.fyid === this.props.fyid)[0].financialyear;
        if (this.state.expensecategory === '') alert("Please Select Expense Head");
        else if (isNaN(parseInt(this.state.voucherno)) || (this.state.voucherno < 0)) alert("Voucher No is mandatory and must be in digits");
        else if (!(/^[a-zA-Z]{3} [0-9]{2} [0-9]{4}$/i.test(this.state.date))) alert("Date Must Be In (MON DD YYYY) Format");
        else if ((new Date(this.state.date) < new Date("01 Apr " + fy.split("-")[0])) || (new Date(this.state.date) > new Date("31 Mar " + fy.split("-")[1]))) alert("Invalid Date. Date Must Be Within FY " + fy);
        else if ((this.state.cash === false) && (this.state.vendorid === '')) alert("Please Select Vebdor Account");
        else if (this.state.expensedescription.length > 100) alert("Remark Must Be Within 100 Characters");
        else if (isNaN(parseInt(this.state.amount)) || (this.state.amount <= 0)) alert("Amount Must Be Greater Than 0");
        else if ((this.state.tds) && ((isNaN(parseInt(this.state.tdsamount)) || (this.state.tdsamount <= 0)))) alert("TDS Amount Must Be Greater Than 0");
        else if ((this.state.tds) && (this.state.cash)) alert("TDS Cannot Be Deducted When Raising Expense By cash");
        else this.props.raiseExpense(this.props.businessid, this.props.fyid, this.state);
    }
    render() {
        if (!cookie.load('userservice')) return <Redirect to="/" />;
        else {
            if ((this.props.addExpenseMessage.loading === true) || (this.props.getExpense.loading === true) || (this.props.logoutMessage.loading === true) || (this.props.getVendor.loading === true) || (this.props.getExpenseHead.loading === true) || (this.props.getBankDetail.loading === true) || (this.props.getInvestmentAccount.loading === true)) {
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div className="mt-3 d-flex justify-content-center">
                            <button className="btn btn-danger">Loading... </button>
                        </div>
                    </div>
                );
            }
            else if ((this.props.getExpense.error !== '') || (this.props.getVendor.error !== '') || (this.props.getBankDetail.error !== '') || (this.props.getExpenseHead.error !== '') || (this.props.getInvestmentAccount.error !== '')) {
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div className="mt-3 d-flex justify-content-center">
                            <button className="btn btn-danger">Error.. Please Refresh and Try Agin </button>
                        </div>
                    </div>
                );
            }
            else {
                let business = this.props.businessList.data.filter(b => b.businessid === this.props.businessid)[0];
                let fy = business.financial.filter(f => f.fyid === this.props.fyid)[0].financialyear;
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div style={{ minHeight: "93vh", display: "flex", flexDirection: "column" }}>
                            <div className="container" style={{ flex: "1" }}>
                                <h6 className="text-center pl-2 pr-2 pt-4" style={{ lineHeight: "1" }}><strong>{business.businessname} {fy}</strong></h6>
                                <h6 className="text-center pl-2 pr-2 mb-4" style={{ lineHeight: "1" }}>{business.alias ? "(" + business.alias + ")" : ''}</h6>
                                <h6 className="text-center pl-2 pr-2"><strong><u>Expense Entry</u></strong></h6>
                                <div className="row" >
                                    <LocalForm className="small" onSubmit={this.handleSubmit}>
                                        <div className="container mt-4 d-flex justify-content-center">
                                            <div class="row col-12 col-md-4">
                                                <div className="form-group mb-0 col-6">
                                                    <label><input type="checkbox" name="cash" id="cash" checked={this.state.cash} onChange={this.handleInputChange} /> <strong>Expense By Cash</strong></label>
                                                </div>
                                                <div className="form-group mb-0 col-6">
                                                    <label><input type="checkbox" name="tds" id="tds" checked={this.state.tds} onChange={this.handleInputChange} /> <strong>Deduct Tds</strong></label>
                                                </div>
                                                <div className="form-group mb-1 col-6 pr-0">
                                                    <label><input type="checkbox" name="it" id="it" checked={this.state.it} onChange={this.handleInputChange} /> <strong>IncomeTax Expense</strong></label>
                                                </div>
                                                {business.businesstype === 'partnership' ? <div className="form-group mb-1 col-6">
                                                    <label><input type="checkbox" name="partner" id="partner" checked={this.state.partner} onChange={this.handleInputChange} /> <strong>Expense To Partner</strong></label>
                                                </div> : ''}
                                                <div className="form-group mb-1 col-6 pr-0">
                                                    <label><input type="checkbox" name="employee" id="employee" checked={this.state.employee} onChange={this.handleInputChange} /> <strong>Expense To Employee</strong></label>
                                                </div>
                                                {this.state.it === false ? <div className="form-group mb-1 col-12 mt-1">
                                                    <label htmlFor="expensecategory" className="mb-0 muted-text"><strong>Select Expense Head:</strong><span className="text-danger">*</span></label>
                                                    <Control.select model=".expensecategory" className="form-control form-control-sm" id="expensecategory" name="expensecategory" onChange={this.handleInputChange} value={this.state.expensecategory}>
                                                        <option value="">Select Expense Head</option>
                                                        {this.props.getExpenseHead.data.map(d => {
                                                            return <option value={d.expenseheadname}>{d.expenseheadname}</option>
                                                        })}
                                                    </Control.select>
                                                </div> : ''}
                                                {this.state.cash === false && this.state.it === false && this.state.partner === false && this.state.employee === false ? <div className="form-group mb-1 col-12">
                                                    <label htmlFor="vendorid" className="mb-0 muted-text"><strong>Select Vendor Account:</strong><span className="text-danger">*</span></label>
                                                    <Control.select model=".vendorid" className="form-control form-control-sm" id="vedorid" name="vendorid" onChange={this.handleInputChange} value={this.state.vendorid}>
                                                        <option value="">Select Vendor Account</option>
                                                        {this.props.getVendor.data.map(d => {
                                                            return <option value={d.vendorid + "..." + d.businessname}>{d.alias ? d.alias : d.businessname}</option>
                                                        })}
                                                    </Control.select>
                                                </div> : ''}
                                                {this.state.partner === true ? <div className="form-group mb-1 col-12">
                                                    <label htmlFor="vendorid" className="mb-0 muted-text"><strong>{business.businesstype === 'partnership' ? 'Select Partner Account' : ''}{business.businesstype === 'ltd' ? 'Select Share Holders Account' : ''}</strong><span className="text-danger">*</span></label>
                                                    <Control.select model=".vendorid" className="form-control form-control-sm" id="vendorid" name="vendorid" onChange={this.handleInputChange} value={this.state.vendorid}>
                                                        <option value="">Select Partner Account</option>
                                                        {this.props.getInvestmentAccount.data.map(b => <option value={b.id + "..." + b.investmentaccount}>{b.investmentaccount}</option>)}
                                                    </Control.select>
                                                </div> : ''}
                                                {this.state.employee === true ? <div className="form-group mb-1 col-12">
                                                    <label htmlFor="vendorid" className="mb-0 muted-text"><strong>Select Employee Account</strong><span className="text-danger">*</span></label>
                                                    <Control.select model=".vendorid" className="form-control form-control-sm" id="vendorid" name="vendorid" onChange={this.handleInputChange} value={this.state.vendorid}>
                                                        <option value="">Select Employee Account</option>
                                                        {this.props.getEmployee.data.map(b => <option value={b.employeeid + "..." + b.employeename}>{b.employeename}</option>)}
                                                    </Control.select>
                                                </div> : ''}
                                                <div className="form-group mb-1 col-12">
                                                    <label htmlFor="voucherno" className="mb-0 muted-text"><strong>Voucher No</strong><span className="text-danger">*</span></label>
                                                    <Control.text model=".voucherno" className="form-control form-control-sm" id="voucherno" name="voucherno" onChange={this.handleInputChange} value={this.state.voucherno} />
                                                </div>
                                                <div className="form-group mb-1 col-12">
                                                    <label htmlFor="date" className="mb-0 muted-text"><strong>Expense Date</strong><span className="text-danger">*</span></label>
                                                    <Control.text model=".date" className="form-control form-control-sm" id="date" name="date" onChange={this.handleInputChange} value={this.state.date} />
                                                </div>
                                                <div className="form-group mb-1 col-12">
                                                    <label htmlFor="expensedescription" className="mb-0 muted-text"><strong>Remark</strong></label>
                                                    <Control.text model=".expensedescription" className="form-control form-control-sm" id="expensedescription" name="expensedescription" onChange={this.handleInputChange} value={this.state.expensedescription} />
                                                </div>
                                                <div className="form-group mb-1 col-12">
                                                    <label htmlFor="amount" className="mb-0 muted-text"><strong>Total Expense Amount</strong><span className="text-danger">*</span></label>
                                                    <Control.text model=".amount" className="form-control form-control-sm" id="amount" name="amount" onChange={this.handleInputChange} value={this.state.amount} />
                                                </div>
                                                {this.state.tds ? <div className="form-group mb-1 col-12 pt-2 pb-2" style={{ backgroundColor: "antiquewhite" }}>
                                                    <label htmlFor="tdsamount" className="mb-0 muted-text"><strong>TDS Deduction Amount</strong><span className="text-danger">*</span></label>
                                                    <Control.text model=".tdsamount" className="form-control form-control-sm" id="tdsamount" name="tdsamount" onChange={this.handleInputChange} value={this.state.tdsamount} />
                                                </div> : ''}
                                                <div className="form-group mb-1 col-12">
                                                    <button type="submit" className="btn btn-success mt-1 btn-sm small" >Enter Expense</button>
                                                </div>
                                            </div>
                                        </div>
                                    </LocalForm>
                                </div>
                            </div>
                            <Footer />
                        </div>
                    </div >
                )
            }
        }
    }
}

export default Expense;