import React, { Component } from 'react';
import cookie from 'react-cookie';
import { Redirect } from 'react-router-dom';
import { LocalForm, Control } from 'react-redux-form';
import { Header } from './HeaderComponent';
import { Footer } from './Footer';

class CashInHand extends Component {
    constructor(props) {
        super(props);
        this.state = ({
            amount: this.props.getCashInHand.amount ? this.props.getCashInHand.amount : 0
        })
        window.onload = () => {
            this.props.resetCashInHand();
        }
    }

    componentDidMount() {
        if (this.props.updateCashInHandMessage.message === 'success') {
            alert("Cash IN Hand Updated Successfully");
            this.props.resetCashInHandDetail();
        }
        if (this.props.updateCashInHandMessage.error !== '') {
            this.props.resetCashInHandDetail();
        }
        if (this.props.getCashInHand.message === 'initial') this.props.getCashInHandDetail(this.props.businessid, this.props.fyid);
        if (this.props.logoutMessage.error !== '') {
            alert("Error Logging Out. Please Try Again");
            this.props.resetLogoutMessage();
        }
    }
    handleInputChange = (event) => {
        let target = event.target;
        let name = target.name;
        let value = target.value;
        this.setState({
            ...this.state,
            [name]: value
        })
    }
    handleSubmit = () => {
        if ((this.state.amount === '') || (this.state.amount < 0) || (isNaN(parseInt(this.state.amount)))) alert(" Cash In Hand Opening Balance Muste Be Greater Than Equal To 0 ");
        else this.props.updateCashInHandDetail(this.props.businessid, this.props.fyid, this.state.amount);
    }
    render() {
        if (!cookie.load('userservice')) return <Redirect to="/" />;
        else {
            if ((this.props.getCashInHand.loading === true) || (this.props.updateCashInHandMessage.loading === true) || (this.props.logoutMessage.loading === true)) {
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div className="mt-3 d-flex justify-content-center">
                            <button className="btn btn-danger">Loading... </button>
                        </div>
                    </div>
                );
            }
            else if (this.props.getCashInHand.error !== '') {
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div className="mt-3 d-flex justify-content-center">
                            <button className="btn btn-danger">Error.. Please Refresh and Try Agin </button>
                        </div>
                    </div>
                );
            }
            else {
                let business = this.props.businessList.data.filter(b => b.businessid === this.props.businessid)[0];
                let fy = business.financial.filter(f => f.fyid === this.props.fyid)[0].financialyear;
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div style={{ minHeight: "93vh", display: "flex", flexDirection: "column" }}>
                            <div className="container" style={{ flex: "1" }}>
                                <h6 className="text-center pl-2 pr-2 pt-4" style={{ lineHeight: "1" }}><strong>{business.businessname} {fy}</strong></h6>
                                <h6 className="text-center pl-2 pr-2 mb-4" style={{ lineHeight: "1" }}>{business.alias ? "(" + business.alias + ")" : ''}</h6>
                                <h6 className="text-center pl-2 pr-2"><strong><u>Cash In Hand Opening Balance</u></strong></h6>
                                <div className="">
                                    <LocalForm className="container col-12 col-md-4 small mt-4" onSubmit={this.handleSubmit}>
                                        <div className="form-group mb-1 col-12">
                                            <label htmlFor="amount" className="mb-0 muted-text"><strong>Cash In Hand as of 1st April  <span className="text-danger">*</span></strong></label>
                                            <Control.text model=".amount" className="form-control form-control-sm" id="amount" name="amount" onChange={this.handleInputChange} value={this.state.amount} />
                                        </div>
                                        <div className="col-12 mb-5 mt-2 pb-5"><button type="Update" className="btn btn-success mt-1 btn-sm small" >Update</button></div>
                                    </LocalForm>
                                </div>
                            </div>
                            <Footer />
                        </div>
                    </div>
                )
            }
        }
    }
}

export default CashInHand;