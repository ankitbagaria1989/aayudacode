import React, { Component } from 'react';
import cookie from 'react-cookie';
import { Redirect } from 'react-router-dom';
import { Header } from './HeaderComponent';
import { Footer } from './Footer';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faFilePdf } from '@fortawesome/free-solid-svg-icons';
import { Page, Text, View, Document, pdf } from '@react-pdf/renderer';
import { pdfstyles } from '../sylesheet/stylesheet';
import { saveAs } from 'file-saver';

const DataDisplay = function ({ purchaseDetail, getPayment, fyid, businessList, businessid, fy, getDebitCredit, getExpense, getAssetAccount, getOpeningBalance, creditors }) {
    let returnrows = [];
    let debit = 0;
    let credit = 0;
    let i = 2;
    returnrows.push(<div><div className="d-flex small mt-4 mb-2 p-1 d-md-none d-lg-none mb-1">
        <div className="col-6 col-md-5 p-1"></div>
        <div className="col-3 col-md-6 p-0 pt-1 text-right"><strong>Debit</strong></div>
        <div className="col-3 col-md-1 p-0 pt-1 text-right"><strong>Credit</strong></div>
    </div>
        <div className="container small mt-3 d-none d-md-block p-0 mb-1">
            <div className="d-flex">
                <div className="col-6 p-0"><strong>Vendors</strong></div>
                <div className="col-3 text-right"><strong>Debit</strong></div>
                <div className="col-3 text-right"><strong>Credit</strong></div>
            </div>
        </div></div>)
    purchaseDetail.data.forEach(d => {
        if (d.type === 'purchase') creditors[d.vendorid].purchase = creditors[d.vendorid].purchase + d.grandtotal;
    })
    getPayment.data.forEach(d => {
        if (Object.keys(creditors).includes(d.vendorid)) creditors[d.vendorid].paid = creditors[d.vendorid].purchase + d.amount;
    })
    getDebitCredit.data.forEach(d => {
        if ((d.issue === "received") && (d.debitcredit === 'credit')) creditors[d.partyid].purchase = creditors[d.partyid].purchase - d.amount;
        else if ((d.issue === "issue") && (d.debitcredit === 'debit')) creditors[d.partyid].purchase = creditors[d.partyid].purchase - d.amount;
    })
    getAssetAccount.data.forEach(d => {
        if (d.purchasefyid === fyid) {
            if (d.vendorid !== 'cash') creditors[d.vendorid.split("...")[0]].purchase = creditors[d.vendorid.split("...")[0]].purchase + d.purchasetotal;
        }
    })
    getExpense.data.forEach(d => {
        if ((d.cash === "0") && (d.tds === '0')) creditors[d.vendorid].purchase = creditors[d.vendorid].purchase + d.amount;
    })
    getOpeningBalance.data.forEach(d => {
        if (d.party === 'vendor') creditors[d.partyid].purchase = creditors[d.partyid].purchase + d.amount;
    })
    Object.keys(creditors).forEach(k => {
        returnrows.push(<div><div className="d-flex small p-1 d-md-none d-lg-none" style={i % 2 === 0 ? { backgroundColor: "rgb(181, 253, 233)" } : {}}>
            <div className="col-6 col-md-5 p-1"><strong>{creditors[k].vendorname}</strong></div>
            <div className="col-3 col-md-6 p-0 pt-1"><strong></strong></div>
            <div className="col-3 col-md-1 p-0 pt-1 text-right">{parseFloat(creditors[k].purchase - creditors[k].paid).toFixed(2)}</div>
        </div>
            <div className="container small d-none d-md-block p-1" style={i % 2 === 0 ? { backgroundColor: "rgb(181, 253, 233)" } : {}}>
                <div className="d-flex">
                    <div className="col-6 p-0"><strong>{creditors[k].vendorname}</strong></div>
                    <div className="col-3 p-0">{parseFloat(creditors[k].purchase - creditors[k].paid) < 0 ? parseFloat(creditors[k].paid - creditors[k].purchase).toFixed(2) : ''}</div>
                    <div className="col-3 text-right p-0">{parseFloat(creditors[k].purchase - creditors[k].paid) >= 0 ? parseFloat(creditors[k].purchase - creditors[k].paid).toFixed(2) : ''}</div>
                </div>
            </div></div>)
        if (parseFloat(creditors[k].purchase - creditors[k].paid) >= 0) credit = credit + parseFloat(creditors[k].purchase - creditors[k].paid);
        else if (parseFloat(creditors[k].purchase - creditors[k].paid) < 0) debit = debit + parseFloat(creditors[k].paid - creditors[k].purchase);
        i++;
    })
    returnrows.push(<div><div className="d-flex small p-1 d-md-none d-lg-none" >
        <div className="col-6 col-md-5 p-1 text-left"><strong>Total</strong></div>
        <div className="col-3 col-md-6 p-0 pt-1 text-right" style={{ borderTop: "1px solid black" }}>{debit > 0 ? parseFloat(debit).toFixed(2) : ''}</div>
        <div className="col-3 col-md-1 p-0 pt-1 text-right" style={{ borderTop: "1px solid black" }}>{credit > 0 ? parseFloat(credit).toFixed(2) : ''}</div>
    </div>
        <div className="container small d-none d-md-block p-1">
            <div className="d-flex">
                <div className="col-6 p-0 text-left"><strong>Total</strong></div>
                <div className="col-3 p-0 text-right" style={{ borderTop: "1px solid black" }}>{debit > 0 ? parseFloat(debit).toFixed(2) : ''}</div>
                <div className="col-3 text-right p-0" style={{ borderTop: "1px solid black" }}>{credit > 0 ? parseFloat(credit).toFixed(2) : ''}</div>
            </div>
        </div></div>)
    return returnrows;
}
let debit = 0, credit = 0;
const CreditorsLedgerPage = function ({ y, creditors }) {
    let returnrows = [];
    let keys = Object.keys(creditors);
    if (y > 1) {
        returnrows.push(
            <View style={pdfstyles.itemSectionLedger, { textAlign: "center" }}>
                <View style={pdfstyles.itemHeader}><Text style={{ width: '375px', textAlign: "left" }}>...Carry Forward</Text><Text style={{ width: '100px', textAlign: "right" }}>{parseFloat(debit).toFixed(2)}</Text><Text style={{ width: '100px', textAlign: "right" }}>{parseFloat(credit).toFixed(2)}</Text></View>
            </View>
        )
    }
    for (let x = (y - 1) * 50; x < y * 50 && x < keys.length; x++) {
        if (creditors[keys[x]].purchase >= creditors[keys[x]].paid) {
            credit = credit + parseFloat(creditors[keys[x]].purchase - creditors[keys[x]].paid);
            returnrows.push(<View style={pdfstyles.itemSectionLedger, { fontFamily: "Helvetica" }}>
                <View style={pdfstyles.itemHeader}><Text style={{ width: '375px', textAlign: "left"}}>{creditors[keys[x]].vendorname}</Text><Text style={{ width: '100px', textAlign: "right" }}></Text><Text style={{ width: '100px', textAlign: "right" }}>{parseFloat(creditors[keys[x]].purchase - creditors[keys[x]].paid).toFixed(2)}</Text></View>
            </View>)
        }
        else if (creditors[keys[x]].paid > creditors[keys[x]].purchase) {
            debit = debit + parseFloat(creditors[keys[x]].paid - creditors[keys[x]].purchase);
            returnrows.push(<View style={pdfstyles.itemSectionLedger, { fontFamily: "Helvetica" }}>
                <View style={pdfstyles.itemHeader}><Text style={{ width: '375px', textAlign: "left" }}>{creditors[keys[x]].vendorname}</Text><Text style={{ width: '100px', textAlign: "right" }}>{parseFloat(creditors[keys[x]].paid - creditors[keys[x]].purchase).toFixed(2)}</Text><Text style={{ width: '100px', textAlign: "right" }}></Text></View>
            </View>)
        }
    }
    if (y === Math.ceil(keys.length / 50)) {
        returnrows.push(<View style={pdfstyles.itemSectionLedger, { fontFamily: "Helvetica-Bold", borderTop: "1px solid black", borderBottom: "1px solid black" }}>
            <View style={pdfstyles.itemHeader}><Text style={{ width: '375px', textAlign: "left" }}>Total</Text><Text style={{ width: '100px', textAlign: "right" }}>{debit > 0 ? parseFloat(debit).toFixed(2) : ''}</Text><Text style={{ width: '100px', textAlign: "right" }}>{credit > 0 ? parseFloat(credit).toFixed(2) : ''}</Text></View>
        </View>
        )
    }
    else {
        returnrows.push(<View><View style={pdfstyles.itemSectionLedger, { fontFamily: "Helvetica-Bold" }}>
            <View style={pdfstyles.itemHeader}><Text style={{ width: '375px', textAlign: "left" }}></Text><Text style={{ width: '100px', borderTop: "1px solid black", textAlign: "right" }}>{parseFloat(debit).toFloat(2)}</Text><Text style={{ width: '100px', borderTop: "1px solid black", textAlign: "right" }}>{parseFloat(credit).toFixed(2)}</Text></View>
        </View>
            <View style={pdfstyles.itemSectionLedger, { fontFamily: "Helvetica", marginTop: "5px", textAlign: "right" }}><Text></Text>...Continued</View></View>)
    }
    return returnrows;
}
const Report = function ({ business, l, n, period, creditors }) {
    let returnrows = [];
    for (let y = 1; y <= n; y++) {
        returnrows.push(<Page size="A4" style={pdfstyles.page}>
            {y === 1 ? <View><View style={pdfstyles.taxInvoiceHeader, { marginTop: "20px", textAlign: "center" }}>
                <View style={{ fontFamily: "Helvetica-Bold", width: "500px" }}><Text>{business.businessname}</Text></View>
                <View style={{ width: "500px" }}><Text>{business.address}</Text></View>
            </View>
                <View style={pdfstyles.taxInvoiceHeader, { marginTop: "10px", textAlign: "center" }}>
                    <View style={{ fontFamily: "Helvetica-Bold", textDecoration: "underline", width: "500px" }}><Text>Sundry Creditors</Text></View>
                    <View style={{ width: "500px" }}><Text>{period}</Text></View>
                </View></View> : ''}
            <View style={pdfstyles.itemSectionLedger, { marginTop: "20px", marginBottom:"5px", fontFamily: "Helvetica-Bold", textAlign: "center", borderBottom: "1px solid black", borderTop: "1px solid black" }}>
                <View style={pdfstyles.itemHeader}><Text style={{ width: '375px', textAlign: "left" }}>Particulars</Text><Text style={{ width: '100px', textAlign: "right" }}>Debit</Text><Text style={{ width: '100px', textAlign: "right" }}>Credit</Text></View>
            </View>
            <CreditorsLedgerPage y={y} creditors={creditors} />
        </Page >)
    }
    return returnrows;
}
const CreditorsLedger = function ({ business, fy, creditors }) {
    let period = fy;
    let l = Object.keys(creditors).length, i = 0, n = Math.ceil(l / 50), y = 0;
    return (
        <Document>
            <Report business={business} l={l} n={n} period={period} creditors={creditors} />
        </Document>
    )
}
class AccountPayables extends Component {
    constructor(props) {
        super(props);
        window.onload = () => {
            this.props.resetPaymentDetail();
            this.props.resetOpeningBalance();
            this.props.refreshVendorState();
            this.props.resetPurchaseDetail();
            this.props.refreshDebitCredit();
            this.props.resetExpenseDetail();
            this.props.refreshAssetAccountState();
        }
    }
    componentDidMount() {
        if ((this.props.getVendor.message === 'initial') && (this.props.getVendor.loading === false)) this.props.getVendorList(this.props.businessid);
        if ((this.props.purchaseDetail.message === 'initial') && (this.props.purchaseDetail.loading === false)) this.props.getPurchaseData(this.props.businessid, this.props.fyid);
        if ((this.props.getPayment.message === 'initial') && (this.props.getPayment.loading === false)) this.props.getPaymentDetail(this.props.businessid, this.props.fyid);
        if ((this.props.getOpeningBalance.message === 'initial') && (this.props.getOpeningBalance.loading === false)) this.props.getPartyOpeningBalance(this.props.businessid, this.props.fyid);
        if (this.props.getDebitCredit.message === 'initial') this.props.getDebitCreditList(this.props.businessid, this.props.fyid);
        if (this.props.getExpense.message === 'initial') this.props.getExpenseDetail(this.props.businessid, this.props.fyid);
        if (this.props.getAssetAccount.message === 'initial') this.props.getAssetAccountList(this.props.businessid);
        if (this.props.logoutMessage.error !== '') {
            alert("Error Logging Out. Please Try Again");
            this.props.resetLogoutMessage();
        }
    }
    generatePdf = async (business, fy, creditors) => {
        try {
            debit = 0;
            credit = 0;
            const doc = <CreditorsLedger business={business} fy={fy} creditors={creditors} />;
            const blobPdf = pdf(doc);
            blobPdf.updateContainer(doc);
            const result = await blobPdf.toBlob();
            let filename = "SundryCreditors.pdf";
            saveAs(result, filename);
        }
        catch (err) {
            alert(err);
        }
    }
    render() {
        if (!cookie.load('userservice')) return <Redirect to="/" />;
        else {
            if ((this.props.getPayment.loading === true) || (this.props.getOpeningBalance.loading === true) || (this.props.getVendor.loading === true) || (this.props.purchaseDetail.loading === true) || (this.props.logoutMessage.loading === true) || (this.props.getDebitCredit.loading === true) || (this.props.getExpense.loading === true) || (this.props.getAssetAccount.loading === true)) {
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div className="mt-3 d-flex justify-content-center">
                            <button className="btn btn-danger">Loading... </button>
                        </div>
                    </div>
                );
            }
            else if ((this.props.getPayment.error !== '') || (this.props.getOpeningBalance.error !== '') || (this.props.getVendor.error !== '') || (this.props.purchaseDetail.error !== '') || (this.props.getDebitCredit.error !== '') || (this.props.getExpense.error !== '') || (this.props.getAssetAccount.error !== '')) {
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div className="mt-3 d-flex justify-content-center">
                            <button className="btn btn-danger">Error.. Please Refresh and Try Again </button>
                        </div>
                    </div>
                );
            }

            else {
                let business = this.props.businessList.data.filter(b => b.businessid === this.props.businessid)[0];
                let fy = business.financial.filter(f => f.fyid === this.props.fyid)[0].financialyear;
                let creditors = {};
                this.props.getVendor.data.forEach(d => creditors[d.vendorid] = {
                    vendorname: d.businessname,
                    purchase: 0,
                    paid: 0
                })
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div style={{ minHeight: "93vh", display: "flex", flexDirection: "column" }}>
                            <div className="container" style={{ flex: "1" }}>
                                <h6 className="text-center pl-2 pr-2 pt-4" style={{ lineHeight: "1" }}><strong>{business.businessname} {fy}</strong></h6>
                                <h6 className="text-center pl-2 pr-2 mb-4" style={{ lineHeight: "1" }}>{business.alias ? "(" + business.alias + ")" : ''}</h6>
                                <h6 className="text-center pl-2 pr-2"><strong><u>Account Payables (Sundry Creditors)</u></strong>&nbsp;&nbsp;&nbsp;<FontAwesomeIcon icon={faFilePdf} size="lg" onClick={() => this.generatePdf(business, fy, creditors)} />&nbsp;&nbsp;&nbsp;</h6>
                                <div className="col-12 p-0">
                                    {this.props.getVendor.message === 'success' ? <DataDisplay purchaseDetail={this.props.purchaseDetail} getPayment={this.props.getPayment} fyid={this.props.fyid} businessList={this.props.businessList} businessid={this.props.businessid} fy={fy} getDebitCredit={this.props.getDebitCredit} getExpense={this.props.getExpense} getAssetAccount={this.props.getAssetAccount} getOpeningBalance={this.props.getOpeningBalance} creditors={creditors} /> : ''}
                                </div>
                            </div>
                            <Footer />
                        </div>
                    </div>
                )
            }
        }
    }
}
export default AccountPayables;