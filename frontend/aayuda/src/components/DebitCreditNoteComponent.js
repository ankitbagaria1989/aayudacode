import React, { Component } from 'react';
import cookie from 'react-cookie';
import { Redirect } from 'react-router-dom';
import { LocalForm, Control } from 'react-redux-form';
import { Header } from './HeaderComponent';
import { Footer } from './Footer';

const Option = function ({ list }) {
    let length = list.length;
    let arr = [];
    for (let i = -1; i < length; i++) {
        if (i === -1) arr.push(<option value=''>Select :</option>);
        else arr.push(<option value={list[i].vendorid ? list[i].vendorid + "..." + list[i].businessname : list[i].customerid + "..." + list[i].businessname}>{list[i].alias ? list[i].alias : list[i].businessname}</option>);
    }
    return arr;
}

class DebitCredit extends Component {
    constructor(props) {
        super(props);
        this.state = ({
            debitcredit: '',
            issuereceived: '',
            vendorcustomer: '',
            partyid: '',
            businessname: '',
            date: new Date().toDateString().split(' ').slice(1).join(' '),
            basic: 0,
            igst: 0,
            cgst: 0,
            sgst: 0,
            total: 0,
            voucherno: '',
            remarks: ''
        })
        window.onload = () => {
            this.props.refreshCustomerState();
            this.props.refreshVendorState();
            this.props.refreshDebitCredit();
        }
    }

    componentDidMount() {
        if (this.props.addDebitCreditMessage.message === 'success') {
            alert("Note Added Successfully");
            this.props.resetaddDebitCreditMessage();
        }
        else if (this.props.addDebitCreditMessage.message === 'Already Registered') {
            alert("Note Already Registered");
            this.props.resetaddDebitCreditMessage();
        }
        if (this.props.addDebitCreditMessage.error !== '') {
            alert("Error Adding Note.");
            this.setState({
                ...this.props.addDebitCreditMessage.state
            })
            this.props.resetaddDebitCreditMessage('error');
        }
        if (this.props.getCustomer.message === 'initial') this.props.getCustomerList(this.props.businessid);
        if (this.props.getVendor.message === 'initial') this.props.getVendorList(this.props.businessid);
        if (this.props.getDebitCredit.message === 'initial') this.props.getDebitCreditList(this.props.businessid, this.props.fyid);
        if (this.props.logoutMessage.error !== '') {
            alert("Error Logging Out. Please Try Again");
            this.props.resetLogoutMessage();
        }
    }
    handleInputChange = (event) => {
        let target = event.target;
        let name = target.name;
        let value = target.value;
        if (name === "partyid") {
            this.setState({
                ...this.state,
                partyid: value.split("...")[0],
                businessname: value.split("...")[1]
            })
        }
        if (name === 'issuereceived' && value === 'issue') {
            this.setState({
                ...this.state,
                issuereceived: value,
                voucherno: this.props.getDebitCredit.voucherno
            })
        }
        else {
            this.setState({
                ...this.state,
                [name]: value
            })
        }
    }
    handleSubmit = () => {
        let business = this.props.businessList.data.filter(b => b.businessid === this.props.businessid)[0];
        let fy = business.financial.filter(f => f.fyid === this.props.fyid)[0].financialyear;
        if (this.state.debitcredit === '') alert("Please Select Debit/Credit Note");
        else if (this.state.issuereceived === '') alert("Please Select Issue/Received");
        else if (this.state.vendorcustomer === '') alert("Please Select Vendor/Customer");
        else if ((this.state.vendorcustomer === 'vendor') && (this.state.partyid === '')) alert("Please Select Vendor Account");
        else if ((this.state.vendorcustomer === 'customer') && (this.state.customerid === '')) alert("Please Select Customer Account");
        else if ((this.state.vendorcustomer === 'customer') && (this.state.issuereceived === 'issue') && (this.debitcredit === 'debit')) alert("Only Credit Note Can be Issued To Customers");
        else if ((this.state.vendorcustomer === 'customer') && (this.state.issuereceived === 'received') && (this.debitcredit === 'credit')) alert("Only Debit Note Can be Received From Customers");
        else if ((this.state.vendorcustomer === 'vendor') && (this.state.issuereceived === 'issue') && (this.debitcredit === 'credit')) alert("Only Debit Note Can be Issued To Vendors");
        else if ((this.state.vendorcustomer === 'vendor') && (this.state.issuereceived === 'received') && (this.debitcredit === 'debit')) alert("Only Credit Note Can be Received From Vendors");
        else if (isNaN(parseInt(this.state.voucherno)) || (this.state.voucherno < 0)) alert("Voucher No is mandatory and Must Be In Digits");
        else if (!(/^[a-zA-Z]{3} [0-9]{2} [0-9]{4}$/i.test(this.state.date))) alert("Date Must Be In (MON DD YYYY) Format");
        else if ((new Date(this.state.date) < new Date("01 Apr " + fy.split("-")[0])) || (new Date(this.state.date) > new Date("31 Mar " + fy.split("-")[1]))) alert("Invalid Date. Date Must Be Within FY " + fy);
        else if (isNaN(parseInt(this.state.basic)) || (this.state.basic < 0)) alert("Basic Must Be Greater Than or Equal To 0");
        else if (isNaN(parseInt(this.state.igst)) || (this.state.igst < 0)) alert("IGST Must Be Greater Than or Equal To 0");
        else if (isNaN(parseInt(this.state.cgst)) || (this.state.cgst < 0)) alert("CGST Must Be Greater Than or Equal To 0");
        else if (isNaN(parseInt(this.state.sgst)) || (this.state.sgst < 0)) alert("SGST Must Be Greater Than or Equal To 0");
        else if (isNaN(parseInt(this.state.total)) || (this.state.total <= 0)) alert("Toal Must Be Greater Than 0");
        else if (this.state.remarks.length > 499) alert("Remark Must Be Within 500 Characters");
        else this.props.raiseDebitCreditNote(this.props.businessid, this.props.fyid, this.state);
    }
    render() {
        if (!cookie.load('userservice')) return <Redirect to="/" />;
        else {
            if ((this.props.getCustomer.loading === true) || (this.props.getVendor.loading === true) || (this.props.getDebitCredit.loading === true) || (this.props.logoutMessage.loading === true)) {
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div className="mt-3 d-flex justify-content-center">
                            <button className="btn btn-danger">Loading... </button>
                        </div>
                    </div>
                );
            }
            else if ((this.props.getCustomer.error !== '') || (this.props.getVendor.error !== '') || (this.props.getDebitCredit.error !== '')) {
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div className="mt-3 d-flex justify-content-center">
                            <button className="btn btn-danger">Error.. Please Refresh and Try Again </button>
                        </div>
                    </div>
                );
            }
            else {
                let business = this.props.businessList.data.filter(b => b.businessid === this.props.businessid)[0];
                let fy = business.financial.filter(f => f.fyid === this.props.fyid)[0].financialyear;
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div style={{ minHeight: "93vh", display: "flex", flexDirection: "column" }}>
                            <div className="container" style={{ flex: "1" }}>
                                <h6 className="text-center pl-2 pr-2 pt-4" style={{ lineHeight: "1" }}><strong>{business.businessname} {fy}</strong></h6>
                                <h6 className="text-center pl-2 pr-2 mb-4" style={{ lineHeight: "1" }}>{business.alias ? "(" + business.alias + ")" : ''}</h6>
                                <h6 className="text-center pl-2 pr-2"><strong><u>Debit/Credit Entry</u></strong></h6>
                                <div className="row" >
                                    <LocalForm className="small" onSubmit={this.handleSubmit}>
                                        <div className="container mt-4 d-flex justify-content-center">
                                            <div class="row col-12 col-md-4">
                                                <div className="form-group mb-1 col-12">
                                                    <label htmlFor="debitcredit" className="mb-0 muted-text"><strong>Debit or Credit Note<span className="text-danger">*</span></strong></label>
                                                    <Control.select model="debitcredit" className="form-control form-control-sm" id="debitcredit" name="debitcredit" onChange={this.handleInputChange} value={this.state.debitcredit}>
                                                        <option value="">Debit or Credit Note</option>
                                                        <option value="debit">Debit Note</option>
                                                        <option value="credit">Credit Note</option>
                                                    </Control.select>
                                                </div>
                                                <div className="form-group mb-1 col-12">
                                                    <label htmlFor="issuereceived" className="mb-0 muted-text"><strong>Issue Or Received<span className="text-danger">*</span></strong></label>
                                                    <Control.select model=".issuereceived" className="form-control form-control-sm" id="issuereceived" name="issuereceived" onChange={this.handleInputChange} value={this.state.issuereceived}>
                                                        <option value="">Issue or Received</option>
                                                        <option value="issue">Issue</option>
                                                        <option value="received">Received</option>
                                                    </Control.select>
                                                </div>
                                                <div className="form-group mb-1 col-12">
                                                    <label htmlFor="vendorcustomer" className="mb-0 muted-text"><strong>Vendor or Customer<span className="text-danger">*</span></strong></label>
                                                    <Control.select model=".vendorcustomer" className="form-control form-control-sm" id="vendorcustomer" name="vendorcustomer" onChange={this.handleInputChange} value={this.state.vendorcustomer}>
                                                        <option value="">Vendor or Customer</option>
                                                        <option value="vendor">Vendor</option>
                                                        <option value="customer">Customer</option>
                                                    </Control.select>
                                                </div>
                                                {(this.state.vendorcustomer == 'vendor') ? <div className="form-group mb-1 col-12">
                                                    <label htmlFor="partyid" className="mb-0 muted-text"><strong>Select Vendor:</strong><span className="text-danger">*</span></label>
                                                    <Control.select model=".partyid" className="form-control form-control-sm" id="partyid" name="partyid" onChange={this.handleInputChange} value={this.state.partyid}>
                                                        <Option list={this.props.getVendor.data} />
                                                    </Control.select>
                                                </div> : <div className="form-group mb-1 col-12">
                                                        <label htmlFor="partyid" className="mb-0 muted-text"><strong>Select Customer:</strong><span className="text-danger">*</span></label>
                                                        <Control.select model=".partyid" className="form-control form-control-sm" id="partyid" name="partyid" onChange={this.handleInputChange} value={this.state.partyid}>
                                                            <Option list={this.props.getCustomer.data} />
                                                        </Control.select>
                                                    </div>}
                                                <div className="form-group mb-1 col-12">
                                                    <label htmlFor="voucherno" className="mb-0 muted-text"><strong>Voucher No<span className="text-danger">*</span></strong></label>
                                                    <Control.text model=".voucherno" className="form-control form-control-sm" id="voucherno" name="voucherno" onChange={this.handleInputChange} value={this.state.voucherno} />
                                                </div>
                                                <div className="form-group mb-1 col-12">
                                                    <label htmlFor="date" className="mb-0 muted-text"><strong>Date<span className="text-danger">*</span></strong></label>
                                                    <Control.text model=".date" className="form-control form-control-sm" id="date" name="date" onChange={this.handleInputChange} value={this.state.date} />
                                                </div>
                                                <div className="form-group mb-1 col-12">
                                                    <label htmlFor="basic" className="mb-0 muted-text"><strong>Basic Amount<span className="text-danger">*</span></strong></label>
                                                    <Control.text model=".basic" className="form-control form-control-sm" id="basic" name="basic" onChange={this.handleInputChange} value={this.state.basic} />
                                                </div>
                                                <div className="form-group mb-1 col-12">
                                                    <label htmlFor="igst" className="mb-0 muted-text"><strong>IGST<span className="text-danger">*</span></strong></label>
                                                    <Control.text model=".igst" className="form-control form-control-sm" id="igst" name="igst" onChange={this.handleInputChange} value={this.state.igst} />
                                                </div>
                                                <div className="form-group mb-1 col-12">
                                                    <label htmlFor="cgst" className="mb-0 muted-text"><strong>CGST<span className="text-danger">*</span></strong></label>
                                                    <Control.text model=".cgst" className="form-control form-control-sm" id="cgst" name="cgst" onChange={this.handleInputChange} value={this.state.cgst} />
                                                </div>
                                                <div className="form-group mb-1 col-12">
                                                    <label htmlFor="sgst" className="mb-0 muted-text"><strong>SGST<span className="text-danger">*</span></strong></label>
                                                    <Control.text model=".sgst" className="form-control form-control-sm" id="sgst" name="sgst" onChange={this.handleInputChange} value={this.state.sgst} />
                                                </div>
                                                <div className="form-group mb-1 col-12">
                                                    <label htmlFor="total" className="mb-0 muted-text"><strong>Total<span className="text-danger">*</span></strong></label>
                                                    <Control.text model=".total" className="form-control form-control-sm" id="total" name="total" onChange={this.handleInputChange} value={this.state.total} />
                                                </div>
                                                <div className="form-group mb-1 col-12">
                                                    <label htmlFor="remarks" className="mb-0 muted-text"><strong>Remarks</strong></label>
                                                    <Control.textarea rows="3" model=".remarks" className="form-control form-control-sm" id="remarks" name="remarks" onChange={this.handleInputChange} value={this.state.remarks} />
                                                </div>
                                                <div className="form-group mb-1 col-12">
                                                    <button type="submit" className="btn btn-success mt-2 btn-sm small" >Enter</button>
                                                </div>
                                            </div>
                                        </div>
                                    </LocalForm>
                                </div>
                            </div>
                            <Footer />
                        </div>
                    </div >
                )
            }
        }
    }
}
export default DebitCredit;