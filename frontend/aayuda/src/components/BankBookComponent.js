import React, { Component } from 'react';
import cookie from 'react-cookie';
import { Redirect } from 'react-router-dom';
import { Header } from './HeaderComponent';
import { LocalForm, Control } from 'react-redux-form';
import { sortData } from '../redux/sortArrayofObjects';
import { Footer } from './Footer';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faFilePdf, faTrash } from '@fortawesome/free-solid-svg-icons';
import { Page, Text, View, Document, pdf } from '@react-pdf/renderer';
import { pdfstyles } from '../sylesheet/stylesheet';
import { saveAs } from 'file-saver';
const DataInvestment = function ({ data, deleteEntry, businessid, fyid, i, business }) {
    let cancel = function () {
        if (window.confirm("Delete Entry")) deleteEntry(data.id, businessid, fyid, 'investment');
    }
    if (data.activity === 'deposit') {
        return (<div><div className="d-flex small mb-1 p-1 d-md-none d-lg-none" style={i % 2 === 0 ? { backgroundColor: "rgb(181, 253, 233)" } : {}}>
            <div className="col-5 col-md-5 p-1">
                <div className="col-12 col-md-6 p-0"><strong>{data.voucherno + "/" + data.date}</strong></div>
                <div className="col-12 col-md-6 p-0" style={{ lineHeight: "1" }}>{business.businesstype === 'propritorship' ? 'To Capital Account (Propritor Deposit)' : ''}{business.businesstype === 'partnership' ? 'To ' + data.investmentaccount.split("...")[1] + "(Partner Deposit)" : ''}{business.businesstype === 'ltd' ? 'To ' + data.investmentaccount.split("...")[1] + "(Equity)" : ''}</div>
                <div className="col-12 col-md-6 p-0 " style={{ lineHeight: "1" }}>{data.remark}</div>
            </div>
            <div className="col-3 col-md-6 p-0 text-right">{parseFloat(parseFloat(data.amount) + parseFloat(data.extraamount)).toFixed(2)}</div>
            <div className="col-3 col-md-1 p-0"></div>
            <div className="col-1 col-md-1 p-0 d-flex justify-content-center">
                <div className="mt-2"><FontAwesomeIcon icon={faTrash} onClick={() => cancel()} size="lg" /></div>
            </div>
        </div>
            <div className="container d-none d-md-block small p-1" style={i % 2 === 0 ? { backgroundColor: "rgb(181, 253, 233)" } : {}}>
                <div className="d-flex">
                    <div className="col-1 p-0">{data.date}</div>
                    <div className="col-1 p-0">{data.voucherno}</div>
                    <div className="col-4 p-0">
                        <div className="col-12 p-0"><strong>{business.businesstype === 'propritorship' ? 'To Capital Account (Propritor Deposit)' : ''}{business.businesstype === 'partnership' ? 'To ' + data.investmentaccount.split("...")[1] + "(Partner Deposit)" : ''}{business.businesstype === 'ltd' ? 'To ' + data.investmentaccount.split("...")[1] + "(Equity)" : ''}</strong></div>
                        <div className="col-12 p-0">{data.remark}</div>
                    </div>
                    <div className="col-2 text-right">{parseFloat(parseFloat(data.amount) + parseFloat(data.extraamount)).toFixed(2)}</div>
                    <div className="col-3"></div>
                    <div className="col-1"><FontAwesomeIcon icon={faTrash} onClick={() => cancel()} size="lg" /></div>
                </div>
            </div></div>)
    }
    else if (data.activity === 'drawing') {
        return (<div><div className="d-flex small mb-1 p-1 d-md-none d-lg-none" style={i % 2 === 0 ? { backgroundColor: "rgb(181, 253, 233)" } : {}}>
            <div className="col-5 col-md-5 p-1">
                <div className="col-12 col-md-6 p-0"><strong>{data.voucherno + "/" + data.date}</strong></div>
                <div className="col-12 col-md-6 p-0" style={{ lineHeight: "1" }}>{business.businesstype === 'propritorship' ? 'By Capital Account (Propritor Drawings)' : ''}{business.businesstype === 'partnership' ? 'By ' + data.investmentaccount.split("...")[1] + "(Partner Drawings)" : ''}</div>
                <div className="col-12 col-md-6 p-0 " style={{ lineHeight: "1" }}>{data.remark}</div>
            </div>
            <div className="col-3 col-md-6 p-0"></div>
            <div className="col-3 col-md-1 p-0 text-right">{parseFloat(data.amount).toFixed(2)}</div>
            <div className="col-1 col-md-1 p-0 d-flex justify-content-center">
                <div className="mt-2"><FontAwesomeIcon icon={faTrash} onClick={() => cancel()} size="lg" /></div>
            </div>
        </div>
            <div className="container d-none d-md-block small p-1" style={i % 2 === 0 ? { backgroundColor: "rgb(181, 253, 233)" } : {}}>
                <div className="d-flex">
                    <div className="col-1 p-0">{data.date}</div>
                    <div className="col-1 p-0">{data.voucherno}</div>
                    <div className="col-4 p-0">
                        <div className="col-12 p-0"><strong>{business.businesstype === 'propritorship' ? 'By Capital Account (Propritor Drawings)' : ''}{business.businesstype === 'partnership' ? 'By ' + data.investmentaccount.split("...")[1] + "(Partner Drawings)" : ''}</strong></div>
                        <div className="col-12 p-0">{data.remark}</div>
                    </div>
                    <div className="col-2"></div>
                    <div className="col-3 text-right">{parseFloat(data.amount).toFixed(2)}</div>
                    <div className="col-1"><FontAwesomeIcon icon={faTrash} onClick={() => cancel()} size="lg" /></div>
                </div>
            </div></div>)
    }
}

const DataLoan = function ({ data, deleteEntry, businessid, fyid, i }) {
    let cancel = function () {
        if (window.confirm("Delete Entry")) deleteEntry(data.id, businessid, fyid, 'loan');
    }
    if ((data.loantype === 'given') && (data.loanrepayment === "repayment")) {
        return (<div><div className="d-flex small mb-1 p-1 d-md-none d-lg-none" style={i % 2 === 0 ? { backgroundColor: "rgb(181, 253, 233)" } : {}}>
            <div className="col-5 col-md-5 p-1">
                <div className="col-12 col-md-6 p-0"><strong>{data.voucherno + "/" + data.date}</strong></div>
                <div className="col-12 col-md-6 p-0" style={{ lineHeight: "1" }}>By {data.loanaccount.split("...")[1]}</div>
                <div className="col-12 col-md-6 p-0" style={{ lineHeight: "1" }}>Repayment Made</div>
                <div className="col-12 col-md-6 p-0" style={{ lineHeight: "1" }}>{data.remark}</div>
            </div>
            <div className="col-3 col-md-6 p-0"></div>
            <div className="col-3 col-md-1 p-0">{parseFloat(data.amount).toFixed(2)}</div>
            <div className="col-1 col-md-1 p-0 d-flex justify-content-center">
                <div className="mt-2"><FontAwesomeIcon icon={faTrash} onClick={() => cancel()} size="lg" /></div>
            </div>
        </div>
            <div className="container d-none d-md-block small p-1" style={i % 2 === 0 ? { backgroundColor: "rgb(181, 253, 233)" } : {}}>
                <div className="d-flex">
                    <div className="col-1 p-0">{data.date}</div>
                    <div className="col-1 p-0">{data.voucherno}</div>
                    <div className="col-4 p-0">
                        <div className="col-12 col-md-6 p-0" style={{ lineHeight: "1" }}><strong>By {data.loanaccount.split("...")[1]}</strong></div>
                        <div className="col-12 p-0" style={{ lineHeight: "1" }}>Repayment Made</div>
                        <div className="col-12 col-md-6 p-0" style={{ lineHeight: "1" }}>{data.remark}</div>
                    </div>
                    <div className="col-2"></div>
                    <div className="col-3 text-right">{parseFloat(data.amount).toFixed(2)}</div>
                    <div className="col-1"><FontAwesomeIcon icon={faTrash} onClick={() => cancel()} size="lg" /></div>
                </div>
            </div></div>)
    }
    else if ((data.loantype === 'received') && (data.loanrepayment === "loan")) {
        return (<div><div className="d-flex small mb-1 p-1 d-md-none d-lg-none" style={i % 2 === 0 ? { backgroundColor: "rgb(181, 253, 233)" } : {}}>
            <div className="col-5 col-md-5 p-1">
                <div className="col-12 col-md-6 p-0"><strong>{data.voucherno + "/" + data.date}</strong></div>
                <div className="col-12 col-md-6 p-0" style={{ lineHeight: "1" }}>To {data.loanaccount.split("...")[1]}</div>
                <div className="col-12 col-md-6 p-0" style={{ lineHeight: "1" }}>Loan Received</div>
                <div className="col-12 col-md-6 p-0" style={{ lineHeight: "1" }}>{data.remark}</div>
            </div>
            <div className="col-3 col-md-6 p-0 text-right">{parseFloat(data.amount)}</div>
            <div className="col-3 col-md-1 p-0"></div>
            <div className="col-1 col-md-1 p-0 d-flex justify-content-center">
                <div className="mt-2"><FontAwesomeIcon icon={faTrash} onClick={() => cancel()} size="lg" /></div>
            </div>
        </div>
            <div className="container d-none d-md-block small p-1" style={i % 2 === 0 ? { backgroundColor: "rgb(181, 253, 233)" } : {}}>
                <div className="d-flex">
                    <div className="col-1 p-0">{data.date}</div>
                    <div className="col-1 p-0">{data.voucherno}</div>
                    <div className="col-4 p-0">
                        <div className="col-12 col-md-6 p-0" style={{ lineHeight: "1" }}><strong>To {data.loanaccount.split("...")[1]}</strong></div>
                        <div className="col-12 p-0" style={{ lineHeight: "1" }}>Loan Received</div>
                        <div className="col-12 col-md-6 p-0" style={{ lineHeight: "1" }}>{data.remark}</div>
                    </div>
                    <div className="col-2">{parseFloat(data.amount).toFixed(2)}</div>
                    <div className="col-3"></div>
                    <div className="col-1"><FontAwesomeIcon icon={faTrash} onClick={() => cancel()} size="lg" /></div>
                </div>
            </div></div>)
    }
    else if ((data.loantype === 'given') && (data.loanrepayment === "loan")) {
        return (<div><div className="d-flex small mb-1 p-1 d-md-none d-lg-none" style={i % 2 === 0 ? { backgroundColor: "rgb(181, 253, 233)" } : {}}>
            <div className="col-5 col-md-5 p-1">
                <div className="col-12 col-md-6 p-0"><strong>{data.voucherno + "/" + data.date}</strong></div>
                <div className="col-12 col-md-6 p-0" style={{ lineHeight: "1" }}>By {data.loanaccount.split("...")[1]}</div>
                <div className="col-12 col-md-6 p-0" style={{ lineHeight: "1" }}>Loan Given</div>
                <div className="col-12 col-md-6 p-0" style={{ lineHeight: "1" }}>{data.remark}</div>
            </div>
            <div className="col-3 col-md-6 p-0"></div>
            <div className="col-3 col-md-1 p-0 text-right">{parseFloat(data.amount).toFixed(2)}</div>
            <div className="col-1 col-md-1 p-0 d-flex justify-content-center">
                <div className="mt-2"><FontAwesomeIcon icon={faTrash} onClick={() => cancel()} size="lg" /></div>
            </div>
        </div>
            <div className="container d-none d-md-block small p-1" style={i % 2 === 0 ? { backgroundColor: "rgb(181, 253, 233)" } : {}}>
                <div className="d-flex">
                    <div className="col-1 p-0">{data.date}</div>
                    <div className="col-1 p-0">{data.voucherno}</div>
                    <div className="col-4 p-0">
                        <div className="col-12 col-md-6 p-0" style={{ lineHeight: "1" }}><strong>By {data.loanaccount.split("...")[1]}</strong></div>
                        <div className="col-12 p-0">Loan Given</div>
                        <div className="col-12 p-0">{data.remark}</div>
                    </div>
                    <div className="col-2"></div>
                    <div className="col-3 text-right">{parseFloat(data.amount).toFixed(2)}</div>
                    <div className="col-1"><FontAwesomeIcon icon={faTrash} onClick={() => cancel()} size="lg" /></div>
                </div>
            </div></div>)
    }
    else if ((data.loantype === 'received') && (data.loanrepayment === "repayment")) {
        return (<div><div className="d-flex small mb-1 p-1 d-md-none d-lg-none" style={i % 2 === 0 ? { backgroundColor: "rgb(181, 253, 233)" } : {}}>
            <div className="col-5 col-md-5 p-1">
                <div className="col-12 col-md-6 p-0"><strong>{data.voucherno + "/" + data.date}</strong></div>
                <div className="col-12 col-md-6 p-0" style={{ lineHeight: "1" }}>To {data.loanaccount.split("...")[1]}</div>
                <div className="col-12 col-md-6 p-0" style={{ lineHeight: "1" }}>Repayment Received</div>
                <div className="col-12 col-md-6 p-0 " style={{ lineHeight: "1" }}>{data.remark}</div>
            </div>
            <div className="col-3 col-md-6 p-0 text-right">{parseFloat(data.amount).toFixed(2)}</div>
            <div className="col-3 col-md-1 p-0"></div>
            <div className="col-1 col-md-1 p-0 d-flex justify-content-center">
                <div className="mt-2"><FontAwesomeIcon icon={faTrash} onClick={() => cancel()} size="lg" /></div>
            </div>
        </div>
            <div className="container d-none d-md-block small p-1" style={i % 2 === 0 ? { backgroundColor: "rgb(181, 253, 233)" } : {}}>
                <div className="d-flex">
                    <div className="col-1 p-0">{data.date}</div>
                    <div className="col-1 p-0">{data.voucherno}</div>
                    <div className="col-4 p-0">
                        <div className="col-12 col-md-6 p-0" style={{ lineHeight: "1" }}><strong>To {data.loanaccount.split("...")[1]}</strong></div>
                        <div className="col-12 p-0">Repayment Received</div>
                        <div className="col-12 p-0">{data.remark}</div>
                    </div>
                    <div className="col-2 text-right">{parseFloat(data.amount).toFixed(2)}</div>
                    <div className="col-3"></div>
                    <div className="col-1"><FontAwesomeIcon icon={faTrash} onClick={() => cancel()} size="lg" /></div>
                </div>
            </div></div>)
    }
}

const DataBankEntry = function ({ data, deleteEntry, businessid, fyid, i }) {
    let cancel = function () {
        if (window.confirm("Delete Entry")) deleteEntry(data.bankentryid, businessid, fyid, 'bankentry');
    }
    if (data.paymenttype === 'paymentmade') {
        return (<div><div className="d-flex small mb-1 p-1 d-md-none d-lg-none" style={i % 2 === 0 ? { backgroundColor: "rgb(181, 253, 233)" } : {}}>
            <div className="col-5 col-md-5 p-1">
                <div className="col-12 col-md-6 p-0"><strong>{data.voucherno + "/" + data.date}</strong></div>
                <div className="col-12 col-md-6 p-0 " style={{ lineHeight: "1" }}>To {data.bankid.split("...")[1]}</div>
                <div className="col-12 col-md-6 p-0 " style={{ lineHeight: "1" }}>Deposit</div>
                <div className="col-12 col-md-6 p-0 " style={{ lineHeight: "1" }}>{data.remark}</div>
            </div>
            <div className="col-3 col-md-6 p-0 text-right">{parseFloat(data.amount).toFixed(2)}</div>
            <div className="col-3 col-md-1">
                <div className="col-12 col-md-2 p-0"></div>
            </div>
            <div className="col-1 col-md-1 p-0 d-flex justify-content-center">
                <div className="mt-2"><FontAwesomeIcon icon={faTrash} onClick={() => cancel()} size="lg" /></div>
            </div>
        </div>
            <div className="container d-none d-md-block small p-1" style={i % 2 === 0 ? { backgroundColor: "rgb(181, 253, 233)" } : {}}>
                <div className="d-flex">
                    <div className="col-1 p-0">{data.date}</div>
                    <div className="col-1 p-0">{data.voucherno}</div>
                    <div className="col-4 p-0">
                        <div className="col-12 p-0"><strong>To {data.bankid.split("...")[1]}</strong></div>
                        <div className="col-12 p-0">Deposit</div>
                        <div className="col-12 p-0">{data.remark}</div>
                    </div>
                    <div className="col-2 text-right">{parseFloat(data.amount).toFixed(2)}</div>
                    <div className="col-3"></div>
                    <div className="col-1"><FontAwesomeIcon icon={faTrash} onClick={() => cancel()} size="lg" /></div>
                </div>
            </div></div>)
    }
    else if (data.paymenttype === 'paymentreceived') {
        return (<div><div className="d-flex small p-1 d-md-none d-lg-none" style={i % 2 === 0 ? { backgroundColor: "rgb(181, 253, 233)" } : {}}>
            <div className="col-5 col-md-5 p-1">
                <div className="col-12 col-md-6 p-0"><strong>{data.voucherno + "/" + data.date}</strong></div>
                <div className="col-12 col-md-6 p-0 " style={{ lineHeight: "1" }}>By {data.bankid.split("...")[1]}</div>
                <div className="col-12 col-md-6 p-0 " style={{ lineHeight: "1" }}>Withdrawl</div>
                <div className="col-12 col-md-6 p-0 " style={{ lineHeight: "1" }}>{data.remark}</div>
            </div>
            <div className="col-3 col-md-6 p-0">
                <div className="col-12 col-md-2 p-0"></div>
            </div>
            <div className="col-3 col-md-1 text-right">{parseFloat(data.amount).toFixed(2)}</div>
            <div className="col-1 col-md-1 p-0 d-flex justify-content-center">
                <div className="mt-2"><FontAwesomeIcon icon={faTrash} onClick={() => cancel()} size="lg" /></div>
            </div>
        </div>
            <div className="container d-none d-md-block small p-1" style={i % 2 == 0 ? { backgroundColor: "rgb(181, 253, 233)" } : {}}>
                <div className="d-flex">
                    <div className="col-1 p-0">{data.date}</div>
                    <div className="col-1 p-0">{data.voucherno}</div>
                    <div className="col-4 p-0">
                        <div className="col-12 p-0"><strong>By {data.bankid.split("...")[1]}</strong></div>
                        <div className="col-12 p-0">Withdrawl</div>
                        <div className="col-12 p-0">{data.remark}</div>
                    </div>
                    <div className="col-2"></div>
                    <div className="col-3 text-right">{parseFloat(data.amount).toFixed(2)}</div>
                    <div className="col-1"><FontAwesomeIcon icon={faTrash} onClick={() => cancel()} size="lg" /></div>
                </div>
            </div></div>)
    }
}

const DataPayment = function ({ data, deleteEntry, businessid, fyid, i }) {
    let cancel = function () {
        if (window.confirm("Delete Entry")) deleteEntry(data.paymentid, businessid, fyid, 'payment');
    }
    return (data.customerid ? <div><div className="d-flex small mb-1 p-1 d-md-none d-lg-none" style={i % 2 === 0 ? { backgroundColor: "rgb(181, 253, 233)" } : {}}>
        <div className="col-5 col-md-5 p-1">
            <div className="col-12 col-md-6 p-0"><strong>{data.voucherno + "/" + data.date}</strong></div>
            <div className="col-12 col-md-6 p-0" style={{ lineHeight: "1" }}>To {data.businessname}</div>
            <div className="col-12 col-md-6 p-0" style={{ lineHeight: "1" }}>{"Receipt"}</div>
            <div className="col-12 col-md-6 p-0 " style={{ lineHeight: "1" }}>{data.remark}</div>
        </div>
        <div className="col-3 col-md-6 p-0">
            <div className="col-12 col-md-2 p-0 text-right">{parseFloat(data.amount).toFixed(2)}</div>
        </div>
        <div className="col-3 col-md-6 p-0"></div>
        <div className="col-1 col-md-1 p-0 d-flex justify-content-center">
            <div className="mt-2"><FontAwesomeIcon icon={faTrash} onClick={() => cancel()} size="lg" /></div>
        </div>
    </div>
        <div className="container d-none d-md-block small p-1" style={i % 2 === 0 ? { backgroundColor: "rgb(181, 253, 233)" } : {}}>
            <div className="d-flex">
                <div className="col-1 p-0">{data.date}</div>
                <div className="col-1 p-0">{data.voucherno}</div>
                <div className="col-4 p-0">
                    <div className="col-12 p-0"><strong>To {data.businessname}</strong></div>
                    <div className="col-12 p-0">Receipt {data.remark}</div>
                </div>
                <div className="col-2 text-right">{parseFloat(data.amount).toFixed(2)}</div>
                <div className="col-3"></div>
                <div className="col-1"><FontAwesomeIcon icon={faTrash} onClick={() => cancel()} size="lg" /></div>
            </div>
        </div></div> : <div><div className="d-flex small mb-1 p-1 d-md-none d-lg-none" style={i % 2 === 0 ? { backgroundColor: "rgb(181, 253, 233)" } : {}}>
            <div className="col-5 col-md-5 p-1">
                <div className="col-12 col-md-6 p-0"><strong>{data.voucherno + "/" + data.date}</strong></div>
                <div className="col-12 col-md-6 p-0" style={{ lineHeight: "1" }}>By {data.businessname}</div>
                <div className="col-12 col-md-6 p-0" style={{ lineHeight: "1" }}>{"Payment"}</div>
                <div className="col-12 col-md-6 p-0 " style={{ lineHeight: "1" }}>{data.remark}</div>
            </div>
            <div className="col-3 col-md-1"></div>
            <div className="col-3 col-md-6 p-0">
                <div className="col-12 col-md-2 p-0 text-right">{parseFloat(data.amount).toFixed(2)}</div>
            </div>
            <div className="col-1 col-md-1 p-0 d-flex justify-content-center">
                <div className="mt-2"><FontAwesomeIcon icon={faTrash} onClick={() => cancel()} size="lg" /></div>
            </div>
        </div>
            <div className="container d-none d-md-block small p-1" style={i % 2 === 0 ? { backgroundColor: "rgb(181, 253, 233)" } : {}}>
                <div className="d-flex">
                    <div className="col-1 p-0">{data.date}</div>
                    <div className="col-1 p-0">{data.voucherno}</div>
                    <div className="col-4 p-0">
                        <div className="col-12 p-0"><strong>By {data.businessname}</strong></div>
                        <div className="col-12 p-0">Payment {data.remark}</div>
                    </div>
                    <div className="col-2"></div>
                    <div className="col-3 text-right">{parseFloat(data.amount).toFixed(2)}</div>
                    <div className="col-1"><FontAwesomeIcon icon={faTrash} onClick={() => cancel()} size="lg" /></div>
                </div>
            </div></div>)
}
const DataDisplay = function ({ monthWisePayment, monthWiseBankEntry, monthWiseOpeningClosing, state, getBankOpeningBalance, monthWiseLoanEntry, monthWiseInvestmentEntry, deleteEntry, businessid, fyid, business }) {
    let bank = getBankOpeningBalance.data.filter(d => d.bankid === state.bankid)[0];
    let datarow = [];
    let i = 3, totaldebit = bank ? parseFloat(bank.amount) : 0, totalcredit = 0;
    let mon = ['apr', 'may', 'jun', 'jul', 'aug', 'sep', 'oct', 'nov', 'dec', 'jan', 'feb', 'mar'];
    datarow.push(<div className="d-flex small mt-4 p-1 d-md-none d-lg-none">
        <div className="col-5 col-md-5 p-1">
            <div className="col-12 col-md-6 p-0 "></div>
        </div>
        <div className="col-3 col-md-6 p-0 pt-1 text-right"><strong>Debit</strong></div>
        <div className="col-3 col-md-1 p-0 pt-1 text-right"><strong>Credit</strong></div>
    </div>);
    datarow.push(
        <div className="container small mt-3 d-none d-md-block p-0">
            <div className="d-flex">
                <div className="col-1 p-0"><strong>Date</strong></div>
                <div className="col-1 p-0"><strong>Voucher No</strong></div>
                <div className="col-4 p-0"><strong>Particulars</strong></div>
                <div className="col-2 text-right"><strong>Debit</strong></div>
                <div className="col-3 text-right"><strong>Credit</strong></div>
            </div>
        </div>
    );
    if (bank) {
        datarow.push(<div className="d-flex small p-1 d-md-none d-lg-none" style={{ backgroundColor: "rgb(181, 253, 233)" }}>
            <div className="col-5 col-md-5 p-1">
                <div className="col-12 col-md-6 p-0 "><strong>Apr-Opening Bal</strong></div>
            </div>
            <div className="col-3 col-md-6 p-0 pt-1">
                <div className="col-12 col-md-2 p-0 text-right">{parseFloat(bank.amount).toFixed(2)}</div>
            </div>
            <div className="col-3 col-md-1"></div>
        </div>)
        datarow.push(
            <div className="container d-none d-md-block small mb-1 p-1 mt-1" style={{ backgroundColor: "rgb(181, 253, 233)" }}>
                <div className="d-flex">
                    <div className="col-2 p-0">Apr-Opening Balance</div>
                    <div className="col-4 p-0"></div>
                    <div className="col-2 text-right">{parseFloat(bank.amount)}</div>
                    <div className="col-3"></div>
                    <div className="col-1"></div>
                </div>
            </div>
        )
    }
    mon.forEach(month => {
        let x = 0;
        let data = [];
        data = data.concat(monthWisePayment[month]);
        data = data.concat(monthWiseBankEntry[month]);
        data = data.concat(monthWiseLoanEntry[month]);
        data = data.concat(monthWiseInvestmentEntry[month]);
        data = data.sort(sortData);
        let debit = 0;
        let credit = 0;
        while (x < data.length) {
            if (data[x].type === 'payment') {
                datarow.push(<DataPayment data={data[x]} deleteEntry={deleteEntry} businessid={businessid} fyid={fyid} i={i} />);
                data[x].customerid ? debit = debit + parseFloat(data[x].amount) : credit = credit + parseFloat(data[x].amount)
                data[x].customerid ? totaldebit = totaldebit + parseFloat(data[x].amount) : totalcredit = totalcredit + parseFloat(data[x].amount)
            }
            else if (data[x].type === 'bankentry') {
                if ((data[x].paymenttype === 'paymentmade')) {
                    datarow.push(<DataBankEntry data={data[x]} deleteEntry={deleteEntry} businessid={businessid} fyid={fyid} i={i} />);
                    debit = debit + parseFloat(data[x].amount);
                    totaldebit = totaldebit + parseFloat(data[x].amount);
                }
                else if (data[x].paymenttype === 'paymentreceived') {
                    datarow.push(<DataBankEntry data={data[x]} deleteEntry={deleteEntry} businessid={businessid} fyid={fyid} i={i} />);
                    credit = credit + parseFloat(data[x].amount);
                    totalcredit = totalcredit + parseFloat(data[x].amount);
                }
            }
            else if (data[x].type === 'loan') {
                if ((data[x].loantype === 'given') && (data[x].loanrepayment === "repayment")) {
                    datarow.push(<DataLoan data={data[x]} deleteEntry={deleteEntry} businessid={businessid} fyid={fyid} i={i} />)
                    credit = credit + parseFloat(data[x].amount);
                    totalcredit = totalcredit + parseFloat(data[x].amount);
                }
                else if ((data[x].loantype === 'received') && (data[x].loanrepayment === "loan")) {
                    datarow.push(<DataLoan data={data[x]} deleteEntry={deleteEntry} businessid={businessid} fyid={fyid} i={i} />)
                    debit = debit + parseFloat(data[x].amount);
                    totaldebit = totaldebit + parseFloat(data[x].amount);
                }
                else if ((data[x].loantype === 'given') && (data[x].loanrepayment === "loan")) {
                    datarow.push(<DataLoan data={data[x]} deleteEntry={deleteEntry} businessid={businessid} fyid={fyid} i={i} />)
                    credit = credit + parseFloat(data[x].amount);
                    totalcredit = totalcredit + parseFloat(data[x].amount);
                }
                else if ((data[x].loantype === 'received') && (data[x].loanrepayment === "repayment")) {
                    datarow.push(<DataLoan data={data[x]} deleteEntry={deleteEntry} businessid={businessid} fyid={fyid} i={i} />)
                    debit = debit + parseFloat(data[x].amount);
                    totaldebit = totaldebit + parseFloat(data[x].amount);
                }
            }
            else if (data[x].type === 'investment') {
                if (data[x].activity === 'deposit') {
                    datarow.push(<DataInvestment data={data[x]} deleteEntry={deleteEntry} businessid={businessid} fyid={fyid} i={i} business={business} />)
                    debit = debit + parseFloat(data[x].amount) + parseFloat(data[x].extraamount);
                    totaldebit = totaldebit + parseFloat(data[x].amount) + parseFloat(data[x].extraamount);
                }
                else if (data[x].activity === 'drawing') {
                    datarow.push(<DataInvestment data={data[x]} deleteEntry={deleteEntry} businessid={businessid} fyid={fyid} i={i} business={business} />)
                    credit = credit + parseFloat(data[x].amount);
                    totalcredit = totalcredit + parseFloat(data[x].amount);
                }
            }
            x++;
            i++;
        }
        switch (month) {
            case 'apr':
                monthWiseOpeningClosing.apr.closing = (debit + parseFloat(monthWiseOpeningClosing.apr.opening)) - credit;
                monthWiseOpeningClosing.may.opening = monthWiseOpeningClosing.apr.closing;
                break;
            case 'may':
                monthWiseOpeningClosing.may.closing = (debit + monthWiseOpeningClosing.may.opening) - credit;
                monthWiseOpeningClosing.jun.opening = monthWiseOpeningClosing.may.closing;
                break;
            case 'jun':
                monthWiseOpeningClosing.jun.closing = (debit + monthWiseOpeningClosing.jun.opening) - credit;
                monthWiseOpeningClosing.jul.opening = monthWiseOpeningClosing.jun.closing;
                break;
            case 'jul':
                monthWiseOpeningClosing.jul.closing = (debit + monthWiseOpeningClosing.jul.opening) - credit;
                monthWiseOpeningClosing.aug.opening = monthWiseOpeningClosing.jul.closing;
                break;
            case 'aug':
                monthWiseOpeningClosing.aug.closing = (debit + monthWiseOpeningClosing.aug.opening) - credit;
                monthWiseOpeningClosing.sep.opening = monthWiseOpeningClosing.aug.closing;
                break;
            case 'sep':
                monthWiseOpeningClosing.sep.closing = (debit + monthWiseOpeningClosing.sep.opening) - credit;
                monthWiseOpeningClosing.oct.opening = monthWiseOpeningClosing.sep.closing;
                break;
            case 'oct':
                monthWiseOpeningClosing.oct.closing = (debit + monthWiseOpeningClosing.oct.opening) - credit;
                monthWiseOpeningClosing.nov.opening = monthWiseOpeningClosing.oct.closing;
                break;
            case 'nov':
                monthWiseOpeningClosing.nov.closing = (debit + monthWiseOpeningClosing.nov.opening) - credit;
                monthWiseOpeningClosing.dec.opening = monthWiseOpeningClosing.nov.closing;
                break;
            case 'dec':
                monthWiseOpeningClosing.dec.closing = (debit + monthWiseOpeningClosing.dec.opening) - credit;
                monthWiseOpeningClosing.jan.opening = monthWiseOpeningClosing.dec.closing;
                break;
            case 'jan':
                monthWiseOpeningClosing.jan.closing = (debit + monthWiseOpeningClosing.jan.opening) - credit;
                monthWiseOpeningClosing.feb.opening = monthWiseOpeningClosing.jan.closing;
                break;
            case 'feb':
                monthWiseOpeningClosing.feb.closing = (debit + monthWiseOpeningClosing.feb.opening) - credit;
                monthWiseOpeningClosing.mar.opening = monthWiseOpeningClosing.feb.closing;
                break;
            case 'mar':
                monthWiseOpeningClosing.mar.closing = (debit + monthWiseOpeningClosing.mar.opening) - credit;
                break;
        }
    })
    datarow.push(<div>
        <div className="d-flex small p-1 d-md-none d-lg-none border-top">
            <div className="col-5 col-md-5 p-1">
                <div className="col-12 col-md-6 p-0"></div>
                <div className="col-12 col-md-6 p-0"><strong></strong></div>
            </div>
            <div className="col-3 col-md-1 border-top border-dark">
                <div className="col-12 col-md-2 p-0 pt-1 text-right">{parseFloat(totaldebit).toFixed(2)}</div>
            </div>
            <div className="col-3 col-md-6 p-0 border-top border-dark">
                <div className="col-12 col-md-2 p-0 pt-1 text-right">{parseFloat(totalcredit).toFixed(2)}</div>
            </div>
        </div>
        <div className="d-flex small p-1 d-md-none d-lg-none">
            <div className="col-5 col-md-5 p-1">
                <div className="col-12 col-md-6 p-0"></div>
                <div className="col-12 col-md-6 p-0 "><strong>Closing Bal</strong></div>
            </div>
            <div className="col-3 col-md-1"></div>
            <div className="col-3 col-md-6 p-0">
                <div className="col-12 col-md-2 p-0 pt-1 border-top border-dark text-right">{parseFloat(monthWiseOpeningClosing['dec'].closing).toFixed(2)}</div>
            </div>
        </div>
        <div className="container d-none d-md-block small mb-1 p-1">
            <div className="d-flex">
                <div className="col-2 p-0"><strong></strong></div>
                <div className="col-4 p-0"></div>
                <div className="col-2 border-top border-dark text-right">{parseFloat(totaldebit).toFixed(2)}</div>
                <div className="col-3 border-top border-dark text-right">{parseFloat(totalcredit).toFixed(2)}</div>
                <div className="col-1"></div>
            </div>
        </div>
        <div className="container d-none d-md-block small mb-1 p-1">
            <div className="d-flex">
                <div className="col-2 p-0"></div>
                <div className="col-4 p-0"><strong>Closing Balance</strong></div>
                <div className="col-2"></div>
                <div className="col-3 border-top border-dark text-right">{parseFloat(monthWiseOpeningClosing['mar'].closing).toFixed(2)}</div>
                <div className="col-1"></div>
            </div>
        </div>
    </div>)
    return datarow;
}
let debit = 0, credit = 0, balance = 0;
const BankDataPage = function ({ data, y, monthWiseOpeningClosing }) {
    let returnrows = [];
    if (y > 1) {
        returnrows.push(
            <View style={pdfstyles.itemSectionLedger, { textAlign: "center", marginTop: "3px" }}>
                <View style={pdfstyles.itemHeader}><Text style={{ width: '75px', textAlign: "left" }}></Text><Text style={{ width: '75px', textAlign: "left" }}></Text><Text style={{ width: '200px', textAlign: "left", fontFamily: "Helvetica-Bold" }}>...Carry Forward</Text><Text style={{ width: '75px', textAlign: "right" }}>{parseFloat(debit).toFixed(2)}</Text><Text style={{ width: '75px', textAlign: "right" }}>{parseFloat(credit).toFixed(2)}</Text><Text style={{ width: '75px', textAlign: "right" }}>{parseFloat(balance).toFixed(2)}</Text></View>
            </View>
        )
    }
    else if (y === 1 && monthWiseOpeningClosing.apr.opening > 0) {
        returnrows.push(
            <View style={pdfstyles.itemSectionLedger, { textAlign: "center", marginTop: "3px" }}>
                <View style={pdfstyles.itemHeader}><Text style={{ width: '75px', textAlign: "left" }}></Text><Text style={{ width: '75px', textAlign: "left" }}></Text><Text style={{ width: '200px', textAlign: "left", fontFamily: "Helvetica-Bold" }}>Opening Balance</Text><Text style={{ width: '75px', textAlign: "right" }}>{parseFloat(monthWiseOpeningClosing.apr.opening).toFixed(2)}</Text><Text style={{ width: '75px', textAlign: "right" }}></Text><Text style={{ width: '75px', textAlign: "right" }}>{parseFloat(balance).toFixed(2)}</Text></View>
            </View>
        )
    }
    for (let x = (y - 1) * 50; x < y * 50 && x < data.length; x++) {
        if (data[x].type === 'payment') {
            data[x].customerid ? debit = debit + parseFloat(data[x].amount) : credit = credit + parseFloat(data[x].amount);
            data[x].customerid ? balance = balance + parseFloat(data[x].amount) : balance = balance - parseFloat(data[x].amount);
            data[x].customerid ? returnrows.push(<View style={pdfstyles.itemSectionLedger, { textAlign: "center", marginTop: "3px" }}>
                <View style={pdfstyles.itemHeader}><Text style={{ width: '75px', textAlign: "left" }}>{data[x].date}</Text><Text style={{ width: '75px', textAlign: "left" }}>{data[x].voucherno}</Text><Text style={{ width: '200px', textAlign: "left", fontFamily: "Helvetica-Bold" }}>To {data[x].businessname}</Text><Text style={{ width: '75px', textAlign: "right" }}>{parseFloat(data[x].amount).toFixed(2)}</Text><Text style={{ width: '75px', textAlign: "right" }}></Text><Text style={{ width: '75px', textAlign: "right" }}>{parseFloat(balance).toFixed(2)}</Text></View>
            </View>) : returnrows.push(<View style={pdfstyles.itemSectionLedger, { textAlign: "center", marginTop: "3px" }}>
                <View style={pdfstyles.itemHeader}><Text style={{ width: '75px', textAlign: "left" }}>{data[x].date}</Text><Text style={{ width: '75px', textAlign: "left" }}>{data[x].voucherno}</Text><Text style={{ width: '200px', textAlign: "left", fontFamily: "Helvetica-Bold" }}>By Cash</Text><Text style={{ width: '75px', textAlign: "right" }}></Text><Text style={{ width: '75px', textAlign: "right" }}>{parseFloat(data[x].amount).toFixed(2)}</Text><Text style={{ width: '75px', textAlign: "right" }}>{parseFloat(balance).toFixed(2)}</Text></View>
            </View>)
        }
        else if (data[x].type === 'bankentry') {
            if (data[x].paymenttype === 'paymentmade') {
                debit = debit + parseFloat(data[x].amount);
                balance = balance + parseFloat(data[x].amount);
                returnrows.push(<View style={pdfstyles.itemSectionLedger, { textAlign: "center", marginTop: "3px" }}>
                    <View style={pdfstyles.itemHeader}><Text style={{ width: '75px', textAlign: "left" }}>{data[x].date}</Text><Text style={{ width: '75px', textAlign: "left" }}>{data[x].voucherno}</Text><Text style={{ width: '200px', textAlign: "left", fontFamily: "Helvetica-Bold" }}>To Cash</Text><Text style={{ width: '75px', textAlign: "right" }}>{parseFloat(data[x].amount).toFixed(2)}</Text><Text style={{ width: '75px', textAlign: "right" }}></Text><Text style={{ width: '75px', textAlign: "right" }}>{parseFloat(balance).toFixed(2)}</Text></View>
                </View>)
            }
            else if (data[x].paymenttype === 'paymentreceived') {
                debit = debit + parseFloat(data[x].amount);
                balance = balance + parseFloat(data[x].amount);
                returnrows.push(<View style={pdfstyles.itemSectionLedger, { textAlign: "center", marginTop: "3px" }}>
                    <View style={pdfstyles.itemHeader}><Text style={{ width: '75px', textAlign: "left" }}>{data[x].date}</Text><Text style={{ width: '75px', textAlign: "left" }}>{data[x].voucherno}</Text><Text style={{ width: '200px', textAlign: "left", fontFamily: "Helvetica-Bold" }}>By Cash</Text><Text style={{ width: '75px', textAlign: "right" }}></Text><Text style={{ width: '75px', textAlign: "right" }}>{parseFloat(data[x].amount).toFixed(2)}</Text><Text style={{ width: '75px', textAlign: "right" }}>{parseFloat(balance).toFixed(2)}</Text></View>
                </View>)
            }
        }
        else if (data[x].type === 'loan') {
            if ((data[x].loantype === 'given') && (data[x].loanrepayment === "repayment")) {
                credit = credit + parseFloat(data[x].amount);
                balance = balance - parseFloat(data[x].amount);
                returnrows.push(<View style={pdfstyles.itemSectionLedger, { textAlign: "center", marginTop: "3px" }}>
                    <View style={pdfstyles.itemHeader}><Text style={{ width: '75px', textAlign: "left" }}>{data[x].date}</Text><Text style={{ width: '75px', textAlign: "left" }}>{data[x].voucherno}</Text><Text style={{ width: '200px', textAlign: "left", fontFamily: "Helvetica-Bold" }}>By {data[x].loanaccount.split("...")[1]}</Text><Text style={{ width: '75px', textAlign: "right" }}></Text><Text style={{ width: '75px', textAlign: "right" }}>{parseFloat(data[x].amount).toFixed(2)}</Text><Text style={{ width: '75px', textAlign: "right" }}>{parseFloat(balance).toFixed(2)}</Text></View>
                </View>)
            }
            else if ((data[x].loantype === 'received') && (data[x].loanrepayment === "loan")) {
                debit = debit + parseFloat(data[x].amount);
                balance = balance + parseFloat(data[x].amount);
                returnrows.push(<View style={pdfstyles.itemSectionLedger, { textAlign: "center", marginTop: "3px" }}>
                    <View style={pdfstyles.itemHeader}><Text style={{ width: '75px', textAlign: "left" }}>{data[x].date}</Text><Text style={{ width: '75px', textAlign: "left" }}>{data[x].voucherno}</Text><Text style={{ width: '200px', textAlign: "left", fontFamily: "Helvetica-Bold" }}>To {data[x].loanaccount.split("...")[1]}</Text><Text style={{ width: '75px', textAlign: "right" }}>{parseFloat(data[x].amount).toFixed(2)}</Text><Text style={{ width: '75px', textAlign: "right" }}></Text><Text style={{ width: '75px', textAlign: "right" }}>{parseFloat(balance).toFixed(2)}</Text></View>
                </View>)
            }
            else if ((data[x].loantype === 'given') && (data[x].loanrepayment === "loan")) {
                credit = credit + parseFloat(data[x].amount);
                balance = balance - parseFloat(data[x].amount);
                returnrows.push(<View style={pdfstyles.itemSectionLedger, { textAlign: "center", marginTop: "3px" }}>
                    <View style={pdfstyles.itemHeader}><Text style={{ width: '75px', textAlign: "left" }}>{data[x].date}</Text><Text style={{ width: '75px', textAlign: "left" }}>{data[x].voucherno}</Text><Text style={{ width: '200px', textAlign: "left", fontFamily: "Helvetica-Bold" }}>By {data[x].loanaccount.split("...")[1]}</Text><Text style={{ width: '75px', textAlign: "right" }}></Text><Text style={{ width: '75px', textAlign: "right" }}>{parseFloat(data[x].amount).toFixed(2)}</Text><Text style={{ width: '75px', textAlign: "right" }}>{parseFloat(balance).toFixed(2)}</Text></View>
                </View>)
            }
            else if ((data[x].loantype === 'received') && (data[x].loanrepayment === "repayment")) {
                debit = debit + parseFloat(data[x].amount);
                balance = balance + parseFloat(data[x].amount);
                returnrows.push(<View style={pdfstyles.itemSectionLedger, { textAlign: "center", marginTop: "3px" }}>
                    <View style={pdfstyles.itemHeader}><Text style={{ width: '75px', textAlign: "left" }}>{data[x].date}</Text><Text style={{ width: '75px', textAlign: "left" }}>{data[x].voucherno}</Text><Text style={{ width: '200px', textAlign: "left", fontFamily: "Helvetica-Bold" }}>To {data[x].loanaccount.split("...")[1]}</Text><Text style={{ width: '75px', textAlign: "right" }}>{parseFloat(data[x].amount).toFixed(2)}</Text><Text style={{ width: '75px', textAlign: "left" }}></Text><Text style={{ width: '75px', textAlign: "right" }}>{parseFloat(balance).toFixed(2)}</Text></View>
                </View>)
            }
        }
        else if (data[x].type === 'investment') {
            if (data[x].activity === 'deposit') {
                debit = debit + parseFloat(data[x].amount) + parseFloat(data[x].extraamount);
                balance = balance + parseFloat(data[x].amount) + parseFloat(data[x].extraamount);
                returnrows.push(<View style={pdfstyles.itemSectionLedger, { textAlign: "center", marginTop: "3px" }}>
                    <View style={pdfstyles.itemHeader}><Text style={{ width: '75px', textAlign: "left" }}>{data[x].date}</Text><Text style={{ width: '75px', textAlign: "left" }}>{data[x].voucherno}</Text><Text style={{ width: '200px', textAlign: "left", fontFamily: "Helvetica-Bold" }}>To {data[x].investmentaccount.split("...")[1]}</Text><Text style={{ width: '75px', textAlign: "right" }}>{parseFloat(data[x].amount).toFixed(2)}</Text><Text style={{ width: '75px', textAlign: "right" }}></Text><Text style={{ width: '75px', textAlign: "right" }}>{parseFloat(balance).toFixed(2)}</Text></View>
                </View>)
            }
            else if (data[x].activity === 'drawing') {
                credit = credit + parseFloat(data[x].amount);
                balance = balance - parseFloat(data[x].amount);
                returnrows.push(<View style={pdfstyles.itemSectionLedger, { textAlign: "center", marginTop: "3px" }}>
                    <View style={pdfstyles.itemHeader}><Text style={{ width: '75px', textAlign: "left" }}>{data[x].date}</Text><Text style={{ width: '75px', textAlign: "left" }}>{data[x].voucherno}</Text><Text style={{ width: '200px', textAlign: "left", fontFamily: "Helvetica-Bold" }}>By {data[x].investmentaccount.split("...")[1]}</Text><Text style={{ width: '75px', textAlign: "right" }}></Text><Text style={{ width: '75px', textAlign: "right" }}>{parseFloat(data[x].amount).toFixed(2)}</Text><Text style={{ width: '75px', textAlign: "right" }}>{parseFloat(balance).toFixed(2)}</Text></View>
                </View>)
            }
        }
    }
    if (y === Math.ceil(data.length / 50)) {
        returnrows.push(<View><View style={pdfstyles.itemSectionLedger, { marginTop: "5px", paddingBottom: "2px" }}>
            <View style={pdfstyles.itemHeader}><Text style={{ width: '75px', textAlign: "left" }}></Text><Text style={{ width: '75px', textAlign: "left" }}></Text><Text style={{ width: '200px', textAlign: "left" }}></Text><Text style={{ width: '75px', borderTop: "1px solid black", textAlign: "right" }}>{parseFloat(debit).toFixed(2)}</Text><Text style={{ width: '75px', borderTop: "1px solid black", textAlign: "right" }}>{parseFloat(credit).toFixed(2)}</Text></View>
        </View>
            <View style={pdfstyles.itemSectionLedger, { fontFamily: "Helvetica-Bold", marginTop: "5px", paddingBottom: "2px", borderTop: "1px solid black", borderBottom: "1px solid black" }}>
                <View style={pdfstyles.itemHeader}><Text style={{ width: '75px', textAlign: "left" }}></Text><Text style={{ width: '75px', textAlign: "left" }}></Text><Text style={{ width: '200px', textAlign: "left" }}>Closing Balance</Text><Text style={{ width: '75px', textAlign: "right" }}></Text><Text style={{ width: '75px', textAlign: "right" }}>{parseFloat(balance).toFixed(2)}</Text><Text style={{ width: '75px', textAlign: "left" }}></Text></View>
            </View></View>)
    }
    else {
        returnrows.push(<View><View style={pdfstyles.itemSectionLedger, { fontFamily: "Helvetica-Bold", marginTop: "2px" }}>
            <View style={pdfstyles.itemHeader}><Text style={{ width: '75px', textAlign: "left" }}></Text><Text style={{ width: '75px', textAlign: "left" }}></Text><Text style={{ width: '200px', textAlign: "left" }}></Text><Text style={{ width: '75px', borderTop: "1px solid black", textAlign: "left" }}>{parseFloat(debit).toFixed(2)}</Text><Text style={{ width: '75px', borderTop: "1px solid black", textAlign: "left" }}>{parseFloat(credit).toFixed(2)}</Text><Text style={{ width: '75px', borderTop: "1px solid black", textAlign: "left" }}>{parseFloat(balance).toFixed(2)}</Text><Text style={{ width: '75px', textAlign: "left" }}></Text></View>
        </View>
            <View style={pdfstyles.itemSectionLedger, { fontFamily: "Helvetica", marginTop: "5px", textAlign: "right" }}><Text></Text>...Continued</View></View>)
    }
    return returnrows;
}
const Report = function ({ business, data, l, n, period, monthWiseOpeningClosing }) {
    let returnrows = [];
    debit = 0;
    credit = 0;
    balance = 0;
    debit = debit + parseFloat(monthWiseOpeningClosing.apr.opening);
    balance = balance + parseFloat(monthWiseOpeningClosing.apr.opening);
    for (let y = 1; y <= n; y++) {
        returnrows.push(<Page size="A4" style={pdfstyles.page}>
            {y === 1 ? <View><View style={pdfstyles.taxInvoiceHeader, { marginTop: "20px", textAlign: "center" }}>
                <View style={{ fontFamily: "Helvetica-Bold", width: "500px" }}><Text>{business.businessname}</Text></View>
                <View style={{ width: "500px" }}><Text>{business.address}</Text></View>
            </View>
                <View style={pdfstyles.taxInvoiceHeader, { marginTop: "10px", textAlign: "center" }}>
                    <View style={{ fontFamily: "Helvetica-Bold", textDecoration: "underline", width: "500px" }}><Text>BANK LEDGER</Text></View>
                    <View style={{ width: "500px" }}><Text>{period}</Text></View>
                </View></View> : ''}
            <View style={pdfstyles.itemSectionLedger, { marginTop: "20px", marginBottom: "5px", fontFamily: "Helvetica-Bold", textAlign: "center", borderTop: "1px solid black", borderBottom: "1px solid black" }}>
                <View style={pdfstyles.itemHeader}><Text style={{ width: '75px', textAlign: "left" }}>Date</Text><Text style={{ width: '75px', textAlign: "left" }}>Voucher No</Text><Text style={{ width: '200px', textAlign: "left" }}>Particulars</Text><Text style={{ width: '75px', textAlign: "right" }}>Debit</Text><Text style={{ width: '75px', textAlign: "right" }}>Credit</Text><Text style={{ width: '75px', textAlign: "right" }}>Balance</Text></View>
            </View>
            <BankDataPage data={data} y={y} monthWiseOpeningClosing={monthWiseOpeningClosing} />
        </Page >)
    }
    return returnrows;
}
const BankLedger = function ({ data, business, fy, monthWiseOpeningClosing }) {
    let l = data.length, i = 0, n = Math.ceil(l / 50), y = 0;
    return (
        <Document>
            <Report business={business} data={data} l={l} n={n} period={fy} monthWiseOpeningClosing={monthWiseOpeningClosing} />
        </Document>
    )
}
class BankBookStatement extends Component {
    constructor(props) {
        super(props);
        this.state = {
            bankid: ''
        }
        window.onload = () => {
            this.props.resetPaymentDetail();
            this.props.resetBankEntryDetail();
            this.props.resetBankOpeningBalance();
            this.props.resetExpenseDetail();
            this.props.resetIncomeOSDetail();
            this.props.refreshLoanState();
            this.props.refreshInvestmentState();
        }
    }
    componentDidMount() {
        if (this.props.getPayment.message === 'initial') this.props.getPaymentDetail(this.props.businessid, this.props.fyid);
        if (this.props.getBankEntry.message === 'initial') this.props.getBankEntryDetail(this.props.businessid, this.props.fyid);
        if (this.props.getBankDetail.message === 'initial') this.props.getBankDetailList(this.props.businessid);
        if (this.props.getBankOpeningBalance.message === 'initial') this.props.getBankOpeningBalanceList(this.props.businessid, this.props.fyid);
        if (this.props.getExpense.message === 'initial') this.props.getExpenseDetail(this.props.businessid, this.props.fyid);
        if (this.props.getIncomeOS.message === 'initial') this.props.getIncomeOSDetail(this.props.businessid, this.props.fyid);
        if (this.props.getLoan.message === 'initial') this.props.getLoanDetail(this.props.businessid, this.props.fyid);
        if (this.props.getInvestment.message === 'initial') this.props.getInvestmentDetail(this.props.businessid, this.props.fyid);
        if (this.props.logoutMessage.error !== '') {
            alert("Error Logging Out. Please Try Again");
            this.props.resetLogoutMessage();
        }
        if (this.props.deleteEntryMessage.message === "success") {
            alert("Entry Deleted Successfully");
            this.props.resetDeleteEntry();
        }
        if (this.props.deleteEntryMessage.error !== '') {
            alert("Error Deleting Entry - Please Try Again");
            this.props.resetDeleteEntry();
        }
    }
    handleInputChange = (event) => {
        let target = event.target;
        let name = target.name;
        let value = target.value;
        this.setState({
            ...this.state,
            [name]: value
        })
    }
    genereateSalesPdf = async (business, fy, monthWisePayment, monthWiseBankEntry, monthWiseOpeningClosing, state, getBankOpeningBalance, monthWiseLoanEntry, monthWiseInvestmentEntry) => {
        try {
            let month = ['apr', 'may', 'jun', 'jul', 'aug', 'sep', 'oct', 'nov', 'dec', 'jan', 'feb', 'mar'];
            let x = 0;
            let data = [];
            month.forEach(month => {
                data = data.concat(monthWisePayment[month]);
                data = data.concat(monthWiseBankEntry[month]);
                data = data.concat(monthWiseLoanEntry[month]);
                data = data.concat(monthWiseInvestmentEntry[month]);
            })
            data = data.sort(sortData);
            const doc = <BankLedger data={data} business={business} fy={fy} monthWiseOpeningClosing={monthWiseOpeningClosing} />;
            const blobPdf = pdf(doc);
            blobPdf.updateContainer(doc);
            const result = await blobPdf.toBlob();
            let filename = "BANK LEDGER.pdf";
            saveAs(result, filename);
        }
        catch (err) {
            alert(err);
        }
    }
    render() {
        if (!cookie.load('userservice')) return <Redirect to="/" />;
        else {
            if ((this.props.logoutMessage.loading === true) || (this.props.getBankEntry.loading === true) || (this.props.getPayment.loading === true) || (this.props.getBankDetail.loading === true) || (this.props.getBankOpeningBalanceList.loading === true) || (this.props.getExpense.loading === true) || (this.props.getIncomeOS.loading === true) || (this.props.deleteEntryMessage.loading === true)) {
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div className="mt-3 d-flex justify-content-center">
                            <button className="btn btn-danger">Loading... </button>
                        </div>
                    </div>
                );
            }
            else if ((this.props.getPayment.error !== '') || (this.props.getBankDetail.error !== '') || (this.props.getBankEntry.error !== '') || (this.props.getBankOpeningBalance.error !== '') || (this.props.getExpense.error !== '') || (this.props.getIncomeOS.error !== '')) {
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div className="mt-3 d-flex justify-content-center">
                            <button className="btn btn-danger">Error.. Please Refresh and Try Again </button>
                        </div>
                    </div>
                );
            }
            else {
                let business = this.props.businessList.data.filter(b => b.businessid === this.props.businessid)[0];
                let fy = business.financial.filter(f => f.fyid === this.props.fyid)[0].financialyear;
                let monthWisePayment = {
                    apr: [],
                    may: [],
                    jun: [],
                    jul: [],
                    aug: [],
                    sep: [],
                    oct: [],
                    nov: [],
                    dec: [],
                    jan: [],
                    feb: [],
                    mar: []
                }
                let monthWiseBankEntry = {
                    apr: [],
                    may: [],
                    jun: [],
                    jul: [],
                    aug: [],
                    sep: [],
                    oct: [],
                    nov: [],
                    dec: [],
                    jan: [],
                    feb: [],
                    mar: []
                }
                let payment = this.props.getPayment.data.filter(d => (d.paymentmode !== 'cash' && d.bankdetail.split("...")[0] === this.state.bankid));
                let bankentryrecord = this.props.getBankEntry.data.filter(d => d.bankid.split("...")[0] === this.state.bankid);
                let bank = this.props.getBankOpeningBalance.data.filter(d => d.bankid === this.state.bankid)[0];
                let loanEntry = this.props.getLoan.data.filter(d => d.paymentmode !== 'cash' && d.fy === fy && this.state.bankid === d.bankdetail.split("...")[0]);
                let investmentEntry = this.props.getInvestment.data.filter(d => d.paymentmode !== 'cash' && d.fy === fy && this.state.bankid === d.bankdetail.split("...")[0]);
                let monthWiseInvestmentEntry = {
                    apr: [],
                    may: [],
                    jun: [],
                    jul: [],
                    aug: [],
                    sep: [],
                    oct: [],
                    nov: [],
                    dec: [],
                    jan: [],
                    feb: [],
                    mar: []
                }
                let monthWiseLoanEntry = {
                    apr: [],
                    may: [],
                    jun: [],
                    jul: [],
                    aug: [],
                    sep: [],
                    oct: [],
                    nov: [],
                    dec: [],
                    jan: [],
                    feb: [],
                    mar: []
                }
                let monthWiseOpeningClosing = {
                    apr: { opening: bank ? parseFloat(bank.amount) : 0, closing: '', td: 0, tc: 0 },
                    may: { opening: 0, closing: 0, td: 0, tc: 0 },
                    jun: { opening: 0, closing: 0, td: 0, tc: 0 },
                    jul: { opening: 0, closing: 0, td: 0, tc: 0 },
                    aug: { opening: 0, closing: 0, td: 0, tc: 0 },
                    sep: { opening: 0, closing: 0, td: 0, tc: 0 },
                    oct: { opening: 0, closing: 0, td: 0, tc: 0 },
                    nov: { opening: 0, closing: 0, td: 0, tc: 0 },
                    dec: { opening: 0, closing: 0, td: 0, tc: 0 },
                    jan: { opening: 0, closing: 0, td: 0, tc: 0 },
                    feb: { opening: 0, closing: 0, td: 0, tc: 0 },
                    mar: { opening: 0, closing: 0, td: 0, tc: 0 },
                }
                payment.map(d => {
                    monthWisePayment[d.date.split(" ")[0].toLowerCase()].push(d)
                })
                bankentryrecord.map(d => {
                    monthWiseBankEntry[d.date.split(" ")[0].toLowerCase()].push(d);
                })
                loanEntry.map(d => {
                    monthWiseLoanEntry[d.date.split(" ")[0].toLowerCase()].push(d);
                })
                investmentEntry.map(d => {
                    monthWiseInvestmentEntry[d.date.split(" ")[0].toLowerCase()].push(d);
                })
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div style={{ minHeight: "93vh", display: "flex", flexDirection: "column" }}>
                            <div className="container" style={{ flex: "1" }}>
                                <h6 className="text-center pl-2 pr-2 pt-4" style={{ lineHeight: "1" }}><strong>{business.businessname} {fy}</strong></h6>
                                <h6 className="text-center pl-2 pr-2 mb-2" style={{ lineHeight: "1" }}>{business.alias ? "(" + business.alias + ")" : ''}</h6>
                                <h6 className="text-center pl-2 pr-2"><strong><u>Bank Book</u></strong> &nbsp;&nbsp;&nbsp;&nbsp;{this.state.bankid !== '' ? <FontAwesomeIcon icon={faFilePdf} size="lg" onClick={() => this.genereateSalesPdf(business, fy, monthWisePayment, monthWiseBankEntry, monthWiseOpeningClosing, this.state, this.props.getBankOpeningBalance, monthWiseLoanEntry, monthWiseInvestmentEntry)} /> : ''}</h6>
                                <div className="row" >
                                    <LocalForm className="container small col-12 col-md-4" onSubmit={this.handleSubmit}>
                                        <div className="form-group mb-1 col-12">
                                            <label htmlFor="bankid" className="mb-0 muted-text"><strong>Bank Name:<span className="text-danger">*</span></strong></label>
                                            <Control.select model=".bankid" className="form-control form-control-sm" id="bankid" name="bankid" onChange={this.handleInputChange} value={this.state.bankid}>
                                                <option value="">Select Bank Name</option>
                                                {this.props.getBankDetail.data.map(b => <option value={b.bankdetailid}>{b.bankname}</option>)}
                                            </Control.select>
                                        </div>
                                    </LocalForm>
                                </div>
                                {this.state.bankid !== '' ? <DataDisplay monthWisePayment={monthWisePayment} monthWiseBankEntry={monthWiseBankEntry} monthWiseOpeningClosing={monthWiseOpeningClosing} state={this.state} getBankOpeningBalance={this.props.getBankOpeningBalance} monthWiseLoanEntry={monthWiseLoanEntry} monthWiseInvestmentEntry={monthWiseInvestmentEntry} deleteEntry={this.props.deleteEntry} businessid={this.props.businessid} fyid={this.props.fyid} business={business} /> : ''}
                            </div>
                            <Footer />
                        </div>
                    </div>
                )
            }
        }
    }
}
export default BankBookStatement;