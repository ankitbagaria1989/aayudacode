import React, { Component } from 'react';
import cookie from 'react-cookie';
import { Redirect } from 'react-router-dom';
import { Header } from './HeaderComponent';
import { Footer } from './Footer';
import { LocalForm, Control } from 'react-redux-form';
import { sortData } from '../redux/sortArrayofObjects';

const StockList = function ({ getItem, itemStock }) {
    let returnrows = [];
    returnrows.push(
        <div>
            <div className="d-flex mb-1 p-1 mt-3">
                <div className="col-6"><strong>Item Name</strong></div>
                <div className="col-6"><strong>Net Stock</strong></div>
            </div>
        </div>
    );
    let x = 0;
    getItem.data.forEach(i => {
        let stock = 0;
        itemStock[i.itemid].forEach(d => {
            if (d.type === 'sales') stock = stock - parseFloat(d.qty);
            else if (d.type === 'purchase') stock = stock + parseFloat(d.qty);
            else if (d.type === 'consumption') stock = stock - parseFloat(d.qty);
            else if (d.type === 'production') stock = stock + parseFloat(d.qty);
            else if (d.type === 'openingbalance') stock = stock + parseFloat(d.qty);
        });
        returnrows.push(
            <div style={x % 2 === 0 ? { backgroundColor: "rgb(181, 253, 233)" } : {}}>
                <div className="d-flex mb-1 p-1 mt-1">
                    <div className="col-6"><strong>{i.alias ? i.alias : i.itemname}</strong></div>
                    <div className="col-6">{stock} units ({i.billingunit})</div>
                </div>
            </div>
        );
        x++;
    });
    return returnrows;
}

const StockDetail = function ({ state, getStockOpeningBalance, itemData }) {
    let data = itemData[state.itemid];
    data = data.sort(sortData);
    let i = 0, y = 0, l = data.length, stock = 0, x = 2, flag = 0;
    let returnrows = [];
    returnrows.push(
        <div>
            <div className="d-flex mb-1 p-1 d-md-none d-lg-none mt-3">
                <div className="col-5 col-md-5 p-1">
                </div>
                <div className="col-3 col-md-1"><strong>Inward</strong></div>
                <div className="col-3 col-md-6"><strong>Outward</strong></div>
                <div className="col-1 p-0"></div>
            </div>
            <div className="container mt-3 d-none d-md-block p-0">
                <div className="d-flex">
                    <div className="col-1 p-0"><strong>Date</strong></div>
                    <div className="col-1 p-0"><strong>Voucher No</strong></div>
                    <div className="col-4 p-0"><strong>Particulars</strong></div>
                    <div className="col-2"><strong>Inward</strong></div>
                    <div className="col-3"><strong>Outward</strong></div>
                </div>
            </div>
        </div>)

    let item = getStockOpeningBalance.data.filter(d => d.itemid === state.itemid)[0];
    if (item) {
        stock = stock + item.stock;
        flag = 1;
        returnrows.push(<div className="d-flex p-1 d-md-none d-lg-none" style={i % 2 === 0 ? { backgroundColor: "rgb(181, 253, 233)" } : {}}>
            <div className="col-6 col-md-5 p-1">
                <div className="col-12 col-md-6 p-0 "><strong>Apr-Opening Bal</strong></div>
            </div>
            <div className="col-3 col-md-6 p-0 pt-1">
                <div className="col-12 col-md-2 p-0">{item ? item.stock + " Units" : "0"}</div>
            </div>
            <div className="col-3 col-md-1"></div>
        </div>)
        returnrows.push(
            <div className="container d-none d-md-block mb-1 p-1" style={i % 2 === 0 ? { backgroundColor: "rgb(181, 253, 233)" } : {}}>
                <div className="d-flex">
                    <div className="col-2 p-0"><strong>Apr-Opening Bal</strong></div>
                    <div className="col-4 p-0"></div>
                    <div className="col-2">{item ? item.stock + " Units" : "0"}</div>
                    <div className="col-3"></div>
                    <div className="col-1"></div>
                </div>
            </div>
        )
        x++;
        i++;
    }

    while (i < l) {
        if (data[i].type === 'sales') {
            stock = stock - parseFloat(data[i].qty);
            returnrows.push(
                <div className="">
                    <div className="d-flex p-1 d-md-none d-lg-none" style={i % 2 === 0 ? { backgroundColor: "rgb(181, 253, 233)" } : {}}>
                        <div className="col-5 col-md-5 p-1">
                            <div className="col-12 col-md-6 p-0"><strong>{data[i].date}</strong></div>
                            <div className="col-12 col-md-6 p-0"><strong>{data[i].invoicenumber}</strong></div>
                            <div className="col-12 col-md-6 p-0">Sold To - {data[i].customername}</div>
                        </div>
                        <div className="col-3 col-md-1">
                        </div>
                        <div className="col-3 col-md-6 p-1">
                            <div className="col-12 col-md-2 p-0">{data[i].qty} units</div>
                        </div>
                    </div>
                    <div className="container d-none d-md-block p-1" style={i % 2 === 0 ? { backgroundColor: "rgb(181, 253, 233)" } : {}}>
                        <div className="d-flex">
                            <div className="col-1 p-0">{data[i].date}</div>
                            <div className="col-1 p-0">{data[i].invoicenumber}</div>
                            <div className="col-4 p-0"><strong>Sold To - {data[i].customername}</strong></div>
                            <div className="col-2"></div>
                            <div className="col-3">{data[i].qty} units</div>
                        </div>
                    </div>
                </div>
            );
            i++;
        }
        else if (data[i].type === 'purchase') {
            stock = stock + parseFloat(data[i].qty);
            returnrows.push(
                <div className="">
                    <div className="d-flex p-1 d-md-none d-lg-none" style={i % 2 === 0 ? { backgroundColor: "rgb(181, 253, 233)" } : {}}>
                        <div className="col-5 col-md-5 p-1">
                            <div className="col-12 col-md-6 p-0"><strong>{data[i].date}</strong></div>
                            <div className="col-12 col-md-6 p-0"><strong>{data[i].invoicenumber}</strong></div>
                            <div className="col-12 col-md-6 p-0">Purchased From - {data[i].vendorname}</div>
                        </div>
                        <div className="col-3 col-md-1">
                            <div className="col-12 col-md-2 p-0">{data[i].qty} units</div>
                        </div>
                        <div className="col-3 col-md-6 p-1">
                        </div>
                    </div>
                    <div className="container d-none d-md-block p-1" style={i % 2 === 0 ? { backgroundColor: "rgb(181, 253, 233)" } : {}}>
                        <div className="d-flex">
                            <div className="col-1 p-0">{data[i].date}</div>
                            <div className="col-1 p-0">{data[i].invoicenumber}</div>
                            <div className="col-4 p-0"><strong>Purchased From - {data[i].vendorname}</strong></div>
                            <div className="col-2">{data[i].qty} units</div>
                            <div className="col-3"><strong></strong></div>
                        </div>
                    </div>
                </div>
            );
            i++;
        }
        else if (data[i].type === 'consumption') {
            stock = stock - parseFloat(data[i].qty);
            returnrows.push(
                <div className="">
                    <div className="d-flex p-1 d-md-none d-lg-none" style={i % 2 === 0 ? { backgroundColor: "rgb(181, 253, 233)" } : {}}>
                        <div className="col-5 col-md-5 p-1">
                            <div className="col-12 col-md-6 p-0"><strong>{data[i].date}</strong></div>
                            <div className="col-12 col-md-6 p-0">Consumption</div>
                        </div>
                        <div className="col-3 col-md-1">
                        </div>
                        <div className="col-3 col-md-6 p-1">
                            <div className="col-12 col-md-2 p-0">{data[i].qty} units</div>
                        </div>
                    </div>
                    <div className="container d-none d-md-block p-1" style={i % 2 === 0 ? { backgroundColor: "rgb(181, 253, 233)" } : {}}>
                        <div className="d-flex">
                            <div className="col-1 p-0">{data[i].date}</div>
                            <div className="col-1 p-0"></div>
                            <div className="col-4 p-0"><strong>Consumption</strong></div>
                            <div className="col-2"></div>
                            <div className="col-3">{data[i].qty} units</div>
                        </div>
                    </div>
                </div>
            );
            i++;
        }
        else if (data[i].type === 'production') {
            stock = stock + parseFloat(data[i].qty);
            returnrows.push(
                <div className="">
                    <div className="d-flex p-1 d-md-none d-lg-none" style={i % 2 === 0 ? { backgroundColor: "rgb(181, 253, 233)" } : {}}>
                        <div className="col-5 col-md-5 p-1">
                            <div className="col-12 col-md-6 p-0"><strong>{data[i].date}</strong></div>
                            <div className="col-12 col-md-6 p-0">Production</div>
                        </div>
                        <div className="col-3 col-md-1">
                            <div className="col-12 col-md-2 p-0">{data[i].qty} units</div>
                        </div>
                        <div className="col-3 col-md-6 p-1">
                        </div>
                    </div>
                    <div className="container d-none d-md-block p-1" style={i % 2 === 0 ? { backgroundColor: "rgb(181, 253, 233)" } : {}}>
                        <div className="d-flex">
                            <div className="col-1 p-0">{data[i].date}</div>
                            <div className="col-1 p-0"></div>
                            <div className="col-4 p-0"><strong>Production</strong></div>
                            <div className="col-2">{data[i].qty} units</div>
                            <div className="col-3"><strong></strong></div>
                        </div>
                    </div>
                </div>
            );
            i++;
        }
        else i++;
    }
    let temp = '';
    if ((l === 0) && (flag === 0)) {
        temp =
            <div className="container mt-2">
                <div className="card border">
                    <div className="text-center">No Data Data to Display</div>
                </div>
            </div>
        returnrows.push(temp)
    }
    let t = <div className="col-12 mt-2 text-white pt-1 pb-1 " style={{ backgroundColor: "#ff8f00" }}><strong>Net Stock :&nbsp;&nbsp;&nbsp; {stock + " Units (" + state.unit + ")"}</strong></div>;
    let returndatatemp = [];
    returndatatemp.push(t);
    returndatatemp.push(returnrows);
    return returndatatemp;
}

class Stock extends Component {
    constructor(props) {
        super(props);
        this.state = {
            itemid: '',
            unit: '',
            name: ''
        }
    }
    handleInputChange = (event) => {
        let target = event.target;
        let value = target.value;
        this.setState({
            ...this.state,
            name: value,
            itemid: value.split("...")[0],
            unit: value.split("...")[1],
        })
    }
    componentDidMount() {
        window.onload = () => {
            this.props.resetSalesDetail();
            this.props.resetPurchaseDetail();
            this.props.refreshItemState();
            this.props.resetStockOpeningBalance();
            this.props.refreshGetStockJournal();
            this.props.refreshVendorState();
        }
        if (this.props.logoutMessage.error !== '') {
            alert("Error Logging Out. Please Try Again");
            this.props.resetLogoutMessage();
        }
        if (this.props.purchaseDetail.message === 'initial') this.props.getPurchaseData(this.props.businessid, this.props.fyid);
        if (this.props.salesDetail.message === 'initial') this.props.getSalesData(this.props.businessid, this.props.fyid);
        if (this.props.getItem.message === 'initial') this.props.getItemList(this.props.businessid, this.props.fyid);
        if (this.props.getStockOpeningBalance.message === 'initial') this.props.getStockOpeningBalanceList(this.props.businessid, this.props.fyid);
        if (this.props.getStockJournal.message === 'initial') this.props.getStockJournalList(this.props.businessid, this.props.fyid);
        if (this.props.getVendor.message === 'initial') this.props.getVendorList(this.props.businessid);
    }
    render() {
        if (!cookie.load('userservice')) return <Redirect to="/" />
        else {
            if ((this.props.logoutMessage.loading === true) || (this.props.purchaseDetail.loading === true) || (this.props.salesDetail.loading === true) || (this.props.getStockOpeningBalance.loading === true) || (this.props.getItem.loading === true) || (this.props.getStockJournal.loading === true) || (this.props.getVendor.loading === true)) {
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div className="mt-3 d-flex justify-content-center">
                            <button className="btn btn-danger">Loading... </button>
                        </div>
                    </div>
                );
            }
            else if ((this.props.purchaseDetail.error !== '') || (this.props.salesDetail.error !== '') || (this.props.getStockOpeningBalance.error !== '') || (this.props.getItem.error !== '') || (this.props.getStockJournal.error !== '') || (this.props.getVendor.error !== '')) {
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div className="mt-3 d-flex justify-content-center">
                            <button className="btn btn-danger">Error fetching Data. Please Try Again. </button>
                        </div>
                    </div>
                );
            }
            else {
                let business = this.props.businessList.data.filter(b => b.businessid === this.props.businessid)[0];
                let fy = business.financial.filter(f => f.fyid === this.props.fyid)[0].financialyear;
                let item = {};
                this.props.getItem.data.forEach(i => {
                    item = {
                        ...item,
                        [i.itemid]: []
                    }
                });
                this.props.salesDetail.data.forEach(d => {
                    if (d.active === 0) {
                        let items = JSON.parse(d.itemobject);
                        let keys = Object.keys(items);
                        for (let i = 0; i < keys.length; i++) {
                            if (items[i].name !== '') {
                                item[JSON.parse(items[i].name).itemid].push({
                                    type: "sales",
                                    qty: items[i].qty,
                                    date: d.invoicedate,
                                    customername: d.customername,
                                    invoicenumber: d.invoicenumber
                                })
                            }
                        }
                    }
                })
                this.props.purchaseDetail.data.forEach(d => {
                    let items = JSON.parse(d.itemobject);
                    let keys = Object.keys(items);
                    let vendor = this.props.getVendor.data.filter(cust => cust.vendorid === d.vendorid)[0];
                    for (let i = 0; i < keys.length; i++) {
                        if (items[i].name !== '') {
                            item[JSON.parse(items[i].name).itemid].push({
                                type: "purchase",
                                qty: items[i].qty,
                                date: d.invoicedate,
                                vendorname: vendor.businessname,
                                invoicenumber: d.invoicenumber
                            })
                        }
                    }
                })
                this.props.getStockJournal.data.forEach(d => {
                    let consumptiondetail = JSON.parse(d.consumptiondetail);
                    let productiondetail = JSON.parse(d.productiondetail);
                    let keysConsumption = Object.keys(consumptiondetail);
                    let keysProduction = Object.keys(productiondetail);
                    for (let x = 1; x <= keysConsumption.length; x++) {
                        item[consumptiondetail[x].itemid.split("...")[0]].push({
                            type: "consumption",
                            qty: consumptiondetail[x].qty,
                            date: d.date
                        })
                    }
                    for (let y = 1; y <= keysProduction.length; y++) {
                        item[productiondetail[y].itemid.split("...")[0]].push({
                            type: "production",
                            qty: productiondetail[y].qty,
                            date: d.date
                        })
                    }
                })
                this.props.getStockOpeningBalance.data.forEach(d => {
                    item[d.itemid].push({
                        type: "openingbalance",
                        qty: d.stock
                    })
                })
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div style={{ minHeight: "93vh", display: "flex", flexDirection: "column" }}>
                            <div className="container d-flex justify-content-center pr-0 pl-0" style={{ flex: "1" }}>
                                <div className="col-12">
                                    <div className="small pr-0 pl-0">
                                        <h6 className="text-center pl-2 pr-2 pt-4" style={{ lineHeight: "1" }}><strong>{business.businessname} {fy}</strong></h6>
                                        <h6 className="text-center pl-2 pr-2 mb-4" style={{ lineHeight: "1" }}>{business.alias ? "(" + business.alias + ")" : ''}</h6>
                                        <h6 className="text-center pl-2 pr-2"><strong><u>Stock Data</u></strong></h6>
                                        <div className="row mt-2 justify-content-center mb-3" >
                                            <LocalForm className="container col-12 col-md-5" onSubmit={this.handleSubmit}>
                                                <div className="form-group mb-1 col-12">
                                                    <label htmlFor="itemid" className="mb-0 muted-text"><strong>Select Item for Detail Data : <span className="text-danger">*</span></strong></label>
                                                    <Control.select model=".itemid" className="form-control form-control-sm" id="itemid" name="itemid" onChange={this.handleInputChange} value={this.state.name}>
                                                        <option value="">Select Item</option>
                                                        {this.props.getItem.data.map(b => <option value={b.itemid + "..." + b.billingunit}>{b.alias ? b.alias : b.itemname}</option>)}
                                                    </Control.select>
                                                </div>
                                            </LocalForm>
                                        </div>
                                        {this.state.itemid === '' ? <StockList getItem={this.props.getItem} itemStock={item} /> : ''}
                                        {this.state.itemid !== '' ? <StockDetail businessid={this.props.businessid} fyid={this.props.fyid} salesDetail={this.props.salesDetail} purchaseDetail={this.props.purchaseDetail} state={this.state} getStockOpeningBalance={this.props.getStockOpeningBalance} itemData={item} /> : ''}
                                    </div>
                                </div>
                            </div>
                            <Footer />
                        </div>
                    </div>
                );
            }
        }
    }
}
export default Stock;