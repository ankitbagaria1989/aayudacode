import React, { Component } from 'react';
import cookie from 'react-cookie';
import { Redirect } from 'react-router-dom';
import { Control } from 'react-redux-form';
import { Header } from './HeaderComponent';
import { Footer } from './Footer';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faFilePdf } from '@fortawesome/free-solid-svg-icons';
import { Page, Text, View, Document, pdf } from '@react-pdf/renderer';
import { pdfstyles } from '../sylesheet/stylesheet';
import { saveAs } from 'file-saver';

const Period = function ({ state }) {
    if (state.filter === "fy") return state.fy;
    else if (state.filter === "1qtr") return " April June Qtr - " + state.fy;
    else if (state.filter === "2qtr") return " July September Qtr - " + state.fy;
    else if (state.filter === "3qtr") return " October December Qtr - " + state.fy;
    else if (state.filter === "4qtr") return " January March Qtr - " + state.fy;
}
const Expense = function ({ getExpenseHead, expense }) {
    let returnrows = [];
    getExpenseHead.data.map(d => expense[d.expenseheadname] ? returnrows.push(<div><div className="col-6 float-left pl-1">{d.expenseheadname}</div><div className="col-6 float-right text-right ">{expense[d.expenseheadname] ? parseFloat(expense[d.expenseheadname]).toFixed(2) : 0}</div></div>) : '');
    return returnrows;
}
const Income = function ({ getIncomeHead, income }) {
    let returnrows = [];
    getIncomeHead.data.map(d => income[d.incomeheadname] ? returnrows.push(<div><div className="col-6 float-left pl-1">{d.incomeheadname}</div><div className="col-6 float-right text-right ">{income[d.incomeheadname] ? parseFloat(income[d.incomeheadname]).toFixed(2) : 0}</div></div>) : '');
    return returnrows;
}
const Asset = function ({ asset }) {
    let returnrows = [];
    Object.keys(asset).map(k => returnrows.push(<div><div className="col-6 float-left pl-1">{asset[k].name}</div><div className="col-6 float-right text-right ">{parseFloat(asset[k].amount).toFixed(2)}</div></div>));
    return returnrows;
}
const Incomekeys = function (incomekeys, income) {
    let returnrows = [];
    if (incomekeys.length > 0) {
        incomekeys.forEach(k => returnrows.push(<View style={{ width: "250px", paddingLeft: "5px", display: "flex", flexDirection: "row" }}><Text style={{ width: "170px", textAlign: "left" }}>{k}</Text><Text style={{ width: "80px", textAlign: "right" }}>{parseFloat(income[k]).toFixed(2)}</Text></View>)
        )
    }
    return returnrows;
}
const Report = function ({ state, totalsalesamount, totalpurchaseamount, openingStock, closingStock, totalincome, totalexpense, totalinvestmentprofit, assetpresent, totalassetprofit, depreciation, expense, income }) {
    let returnrows = [];
    let incomekeys = Object.keys(income);
    let expensekeys = Object.keys(expense);
    let profit = parseFloat(totalsalesamount + closingStock + totalincome + totalinvestmentprofit + totalassetprofit) - parseFloat(totalexpense + totalpurchaseamount + openingStock + depreciation);
    let assetprofit = totalassetprofit > 0 ? totalassetprofit : 0;
    let assetloss = totalassetprofit < 0 ? totalassetprofit : 0;
    let netprofit = profit > 0 ? profit : 0;
    let netloss = profit < 0 ? profit : 0;
    returnrows.push(<Page size="A4" style={pdfstyles.page}>
        <View style={pdfstyles.taxInvoiceHeader, { marginTop: "20px", textAlign: "center" }}>
            <View style={{ fontFamily: "Helvetica-Bold", width: "500px" }}><Text>{state.businessname}</Text></View>
            <View style={{ width: "500px" }}><Text>{state.address}</Text></View>
        </View>
        <View style={pdfstyles.taxInvoiceHeader, { marginTop: "10px", textAlign: "center" }}>
            <View style={{ fontFamily: "Helvetica-Bold", textDecoration: "underline", width: "500px" }}><Text>Profit Loss Statement</Text></View>
            <View style={{ width: "500px" }}><Text>{state.fy}</Text></View>
        </View>
        <View style={{ width: "550px", marginTop: "20px", display: "flex", flexDirection: "row", borderBottom: "1px solid black", borderTop: "1px solid black", fontFamily: "Helvetica-Bold" }}>
            <View style={{ width: "250px", textAlign: "left", display: "flex", flexDirection: "row" }}><Text style={{width:"170px", textAlign:"left"}}>Particulars</Text><Text style={{width:"80px", textAlign:"right"}}>Debit</Text></View>
            <View style={{ width: "50px" }}></View>
            <View style={{ width: "250px", display: "flex", flexDirection: "row" }}><Text style={{width:"170px", textAlign:"left"}}>Particulars</Text><Text style={{width:"80px", textAlign:"right"}}>Credit</Text></View>
        </View>
        <View style={{ width: "550px", marginTop: "5px", display: "flex", flexDirection: "row" }}>
        <View style={{ width: "250px" }}>
                <View style={{ width: "250px", display: "flex", flexDirection: "row" }}><Text style={{ width: "170px", textAlign: "left" }}>Purchase</Text><Text style={{ width: "80px", textAlign: "right" }}>{parseFloat(totalpurchaseamount).toFixed(2)}</Text></View>
                <View style={{ width: "250px", display: "flex", flexDirection: "row" }}><Text style={{ width: "170px", textAlign: "left" }}>Opening Stock</Text><Text style={{ width: "80px", textAlign: "right" }}>{parseFloat(openingStock).toFixed(2)}</Text></View>
                {expensekeys.length > 0 ? <View style={{ width: "250px" }}><Text style={{ width: "170px", textAlign: "left", fontFamily: "Helvetica-Bold", marginTop: "5px" }}>Expense</Text></View> : <View></View>}
                <Incomekeys incomekeys={expensekeys} income={expense} />
                {depreciation > 0 ? <View style={{ width: "250px", textAlign: "left", display: "flex", flexDirection: "row" }}><Text style={{ width: "170px", textAlign: "left" }}>Depreciation</Text><Text style={{ width: "80px", textAlign: "right" }}>{parseFloat(depreciation).toFixed(2)}</Text></View> : <View></View>}
                {assetloss !== 0 ? <View style={{ width: "250px", textAlign: "left", display: "flex", flexDirection: "row" }}><Text style={{ width: "170px", textAlign: "left" }}>Loss From Asset</Text><Text style={{ width: "80px", textAlign: "right" }}>{parseFloat(assetloss).toFixed(2)}</Text></View> : <View></View>}
                <View style={{ width: "250px", textAlign: "left", display: "flex", flexDirection: "row" }}><Text style={{ width: "170px", textAlign: "left" }}></Text><Text style={{ width: "80px", textAlign: "right", borderTop: "1px solid black" }}>{parseFloat(totalpurchaseamount + openingStock + totalexpense + assetloss + depreciation).toFixed(2)}</Text></View>
                {netprofit !== 0 ? <View style={{ width: "250px", textAlign: "left", display: "flex", flexDirection: "row" }}><Text style={{ width: "170px", textAlign: "left" }}>Profit</Text><Text style={{ width: "80px", textAlign: "right" }}>{parseFloat(netprofit).toFixed(2)}</Text></View> : <View></View>}
            </View>
            <View style={{ width: "50px" }}></View>
            <View style={{ width: "250px" }}>
                <View style={{ width: "250px", display: "flex", flexDirection: "row" }}><Text style={{ width: "170px", textAlign: "left" }}>Sales</Text><Text style={{ width: "80px", textAlign: "right" }}>{parseFloat(totalsalesamount).toFixed(2)}</Text></View>
                <View style={{ width: "250px", display: "flex", flexDirection: "row" }}><Text style={{ width: "170px", textAlign: "left" }}>Closing Stock</Text><Text style={{ width: "80px", textAlign: "right" }}>{parseFloat(closingStock).toFixed(2)}</Text></View>
                {incomekeys.length > 0 ? <View style={{ width: "250px" }}><Text style={{ width: "170px", textAlign: "left", fontFamily: "Helvetica-Bold", marginTop: "5px" }}>Income - Other Sources</Text></View> : <View></View>}
                <Incomekeys incomekeys={incomekeys} income={income} />
                {assetprofit !== 0 ? <View style={{ width: "250px", textAlign: "left", display: "flex", flexDirection: "row" }}><Text style={{ width: "170px", textAlign: "left" }}>Profit From Asset</Text><Text style={{ width: "80px", textAlign: "right" }}>{parseFloat(totalassetprofit).toFixed(2)}</Text></View> : <View></View>}
                <View style={{ width: "250px", textAlign: "left", display: "flex", flexDirection: "row" }}><Text style={{ width: "170px", textAlign: "left" }}></Text><Text style={{ width: "80px", textAlign: "right", borderTop: "1px solid black" }}>{parseFloat(totalsalesamount + closingStock + totalincome + assetprofit).toFixed(2)}</Text></View>
                {netloss !== 0 ? <View style={{ width: "250px", textAlign: "left", display: "flex", flexDirection: "row" }}><Text style={{ width: "170px", textAlign: "left" }}>Loss</Text><Text style={{ width: "80px", textAlign: "right" }}>{parseFloat(netloss).toFixed(2)}</Text></View> : <View></View>}
            </View>
        </View>
        <View style={{ width: "550px", display: "flex", flexDirection: "row", marginTop: "5px", borderTop:"1px solid black", borderBottom:"1px solid black" }}>
            <View style={{ width: "250px", textAlign: "left", display: "flex", flexDirection: "row", fontFamily: "Helvetica-Bold" }}><Text style={{ width: "170px", textAlign: "left" }}>Total</Text><Text style={{ width: "80px", textAlign: "right" }}>{parseFloat(totalpurchaseamount + openingStock + totalexpense + assetloss + netprofit + depreciation).toFixed(2)}</Text></View>
            <View style={{ width: "50px" }}></View>
            <View style={{ width: "250px", textAlign: "left", display: "flex", flexDirection: "row", fontFamily: "Helvetica-Bold", }}><Text style={{ width: "170px", textAlign: "left" }}>Total</Text><Text style={{ width: "80px", textAlign: "right" }}>{parseFloat(totalsalesamount + closingStock + totalincome + assetprofit + netloss).toFixed(2)}</Text></View>
            
        </View>
    </Page >)
    return returnrows;
}
const PnLLedger = function ({ state, totalsalesamount, totalpurchaseamount, openingStock, closingStock, totalincome, totalexpense, totalinvestmentprofit, assetpresent, totalassetprofit, depreciation, expense, income, profit, asset }) {
    return (
        <Document>
            <Report state={state} totalsalesamount={totalsalesamount} totalpurchaseamount={totalpurchaseamount} openingStock={openingStock} closingStock={closingStock} totalincome={totalincome} totalexpense={totalexpense} totalinvestmentprofit={totalinvestmentprofit} assetpresent={assetpresent} totalassetprofit={totalassetprofit} depreciation={depreciation} expense={expense} income={income} profit={profit} asset={asset} />
        </Document>
    )
}
class ProfitLoss extends Component {
    constructor(props) {
        super(props);
        let business = this.props.businessList.data.filter(b => b.businessid === this.props.businessid)[0];
        let financial = business.financial.filter(f => f.fyid === this.props.fyid)[0];
        this.state = {
            filter: "fy",
            fy: financial.financialyear,
            businessname: business.businessname,
            alias: business.alias,
            address: business.address
        }
        window.onload = () => {
            this.props.getExpenseDetail(this.props.businessid, this.props.fyid);
            this.props.getSalesData(this.props.businessid, this.props.fyid);
            this.props.getPurchaseData(this.props.businessid, this.props.fyid);
            this.props.getOpeningClosingDetail(this.props.businessid, this.props.fyid);
            this.props.getIncomeOSDetail(this.props.businessid, this.props.fyid);
            this.props.getDebitCreditList(this.props.businessid, this.props.fyid);
            this.props.getIncomeHeadList(this.props.businessid);
            this.props.getInvestmentDetail(this.props.businessid, this.props.fyid);
            this.props.getAssetAccountList(this.props.businessid);
            this.props.getAssetOpeningBalanceList(this.props.businessid, this.props.fyid);
            this.props.getDepreciationList(this.props.businessid, this.props.fyid);
        }
        if (this.props.logoutMessage.error !== '') {
            alert("Error Logging Out. Please Try Again");
            this.props.resetLogoutMessage();
        }
    }
    handleInputChange = (event) => {
        let target = event.target;
        let name = target.name;
        let value = target.value;
        this.setState({
            ...this.state,
            [name]: value
        })
    }
    generatePdf = async (state, totalsalesamount, totalpurchaseamount, openingStock, closingStock, totalincome, totalexpense, totalinvestmentprofit, assetpresent, totalassetprofit, depreciation, expense, income, profit, asset) => {
        try {
            const doc = <PnLLedger state={state} totalsalesamount={totalsalesamount} totalpurchaseamount={totalpurchaseamount} openingStock={openingStock} closingStock={closingStock} totalincome={totalincome} totalexpense={totalexpense} totalinvestmentprofit={totalinvestmentprofit} assetpresent={assetpresent} totalassetprofit={totalassetprofit} depreciation={depreciation} expense={expense} income={income} profit={profit} asset={asset} />;
            const blobPdf = pdf(doc);
            blobPdf.updateContainer(doc);
            const result = await blobPdf.toBlob();
            let filename = "ProfitLossStatement.pdf";
            saveAs(result, filename);
        }
        catch (err) {
            alert(err);
        }
    }
    render() {
        if (!cookie.load('userservice')) return <Redirect to="/" />
        else {
            if ((this.props.getExpense.loading === true) || (this.props.getSales.loading === true) || (this.props.getPurchase.loading === true) || (this.props.getOpeningClosing.loading === true) || (this.props.logoutMessage.loading === true) || (this.props.getExpenseHead.loading === true) || (this.props.getIncomeOS.loading === true) || (this.props.getDebitCredit.loading === true) || (this.props.getIncomeHead.loading === true) || (this.props.getInvestment.loading === true) || (this.props.getAssetAccount.loading === true) || (this.props.getAssetOpeningBalance.loading === true) || (this.props.getDepreciation.loading == true)) {
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div className="mt-3 d-flex justify-content-center">
                            <button className="btn btn-danger">Loading... </button>
                        </div>
                    </div>
                );
            }
            else if ((this.props.getExpense.error !== '') || (this.props.getSales.error !== '') || (this.props.getPurchase.error !== '') || (this.props.getOpeningClosing.error !== '') || (this.props.getExpenseHead.error !== '') || (this.props.getIncomeOS.error !== '') || (this.props.getDebitCredit.error !== '') || (this.props.getIncomeHead.error !== '') || (this.props.getInvestment.error !== '') || (this.props.getAssetOpeningBalance.error !== '') || (this.props.getAssetAccount.error !== '') || (this.props.getDepreciation.error !== '')) {
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div className="mt-3 d-flex justify-content-center">
                            <button className="btn btn-danger">Error.. Please Refresh and Try Again </button>
                        </div>
                    </div>
                );
            }
            else {
                let totalsalesamount = 0;
                let totalpurchaseamount = 0;
                let openingStock = 0;
                let closingStock = 0;
                let totalincome = 0;
                let totalexpense = 0;
                let investmentpresent = false;
                let totalinvestmentprofit = 0;
                let assetpresent = false;
                let totalassetprofit = 0;
                let depreciation = 0;
                let expense = {};
                let income = {};
                let profit = {};
                let asset = {};
                if (this.state.filter === 'fy') {
                    this.props.getSales.data.forEach(d => d.type === 'sales' ? totalsalesamount = totalsalesamount + parseFloat(d.totalbasic) : '');
                    this.props.getPurchase.data.forEach(d => d.asset == 0 ? totalpurchaseamount = totalpurchaseamount + parseFloat(d.totalbasic) : totalpurchaseamount = totalpurchaseamount);
                    this.props.getDebitCredit.data.forEach(d => d.vendorcustomer === 'customer' ? totalsalesamount = totalsalesamount - parseFloat(d.basic) : totalpurchaseamount = totalpurchaseamount - parseFloat(d.basic));
                    this.props.getExpense.data.forEach(d => {
                        expense = {
                            ...expense,
                            [d.category]: parseFloat(d.amount) + (expense[d.category] ? expense[d.category] : 0)
                        }
                        totalexpense = totalexpense + parseFloat(d.amount);
                    })
                    this.props.getIncomeOS.data.forEach(d => {
                        income = {
                            ...income,
                            [d.category]: parseFloat(d.amount) + (income[d.category] ? income[d.category] : 0)
                        }
                        totalincome = totalincome + parseFloat(d.amount);
                    })
                    this.props.getInvestment.data.forEach(d => {
                    })
                    let flag = 0;
                    this.props.getAssetAccount.data.forEach(d => {
                        if (d.soldfyid === this.props.fyid) {
                            asset = {
                                ...asset,
                                [d.id]: { name: d.assetaccount, amount: d.soldbasic }
                            }
                            totalassetprofit = totalassetprofit + parseFloat(d.soldbasic);
                            assetpresent = true;
                        }
                        if ((d.depreciating === '1') && (d.purchasefyid === this.props.fyid)) {
                            if (Object.keys(asset).includes(d.id)) {
                                flag = 1;
                                asset = {
                                    ...asset,
                                    [d.id]: {
                                        name: asset[d.id].name,
                                        amount: asset[d.id].amount - parseFloat(d.purchasebasic)
                                    }
                                }
                                totalassetprofit = totalassetprofit - parseFloat(d.purchasebasic);
                            }
                        }
                        else if (d.depreciating === '0') {
                            if (Object.keys(asset).includes(d.id)) {
                                asset = {
                                    ...asset,
                                    [d.id]: {
                                        name: asset[d.id].name,
                                        amount: asset[d.id].amount - parseFloat(d.purchasebasic)
                                    }
                                }
                                totalassetprofit = totalassetprofit - parseFloat(d.purchasebasic);
                            }
                        }
                    })
                    this.props.getAssetOpeningBalance.data.forEach(d => {
                        if (Object.keys(asset).includes(d.assetaccount)) {
                            if (flag === 1) {
                                asset = {
                                    ...asset,
                                    [d.assetaccount]: {
                                        name: asset[d.assetaccount].name,
                                        amount: asset[d.assetaccount].amount - parseFloat(d.amount)
                                    }
                                }
                                totalassetprofit = totalassetprofit - parseFloat(d.amount);
                            }
                        }
                    })
                    this.props.getDepreciation.data.forEach(d => {
                        depreciation = depreciation + parseFloat(d.amount)
                    })
                    openingStock = parseFloat(this.props.getOpeningClosing.data.aprilopening);
                    closingStock = parseFloat(this.props.getOpeningClosing.data.marchclosing);
                }
                else if (this.state.filter === '1qtr') {
                    this.props.getSales.data.forEach(d => (d.type === 'sales' && ['apr', 'may', 'jun'].includes(d.invoicedate.split(' ')[0].toLowerCase())) ? totalsalesamount = totalsalesamount + parseFloat(d.totalbasic) : totalsalesamount = totalsalesamount)
                    this.props.getPurchase.data.forEach(d => (['apr', 'may', 'jun'].includes(d.invoicedate.split(' ')[0].toLowerCase()) && d.asset == 0) ? totalpurchaseamount = totalpurchaseamount + parseFloat(d.totalbasic) : totalpurchaseamount = totalpurchaseamount)
                    this.props.getDebitCredit.data.forEach(d => ['apr', 'may', 'jun'].includes(d.date.split(' ')[0].toLowerCase()) ? d.vendorcustomer === 'customer' ? totalsalesamount = totalsalesamount - parseFloat(d.basic) : totalpurchaseamount = totalpurchaseamount - parseFloat(d.basic) : '');
                    this.props.getExpense.data.forEach(d => {
                        if (['apr', 'may', 'jun'].includes(d.date.split(' ')[0].toLowerCase())) {
                            expense = {
                                ...expense,
                                [d.category]: parseFloat(d.amount) + (expense[d.category] ? expense[d.category] : 0)
                            }
                            totalexpense = totalexpense + parseFloat(d.amount);
                        }
                    })
                    this.props.getIncomeOS.data.forEach(d => {
                        if (['apr', 'may', 'jun'].includes(d.date.split(' ')[0].toLowerCase())) {
                            income = {
                                ...income,
                                [d.category]: parseFloat(d.amount) + (income[d.category] ? income[d.category] : 0)
                            }
                            totalincome = totalincome + parseFloat(d.amount);
                        }
                    })
                    openingStock = parseFloat(this.props.getOpeningClosing.data.aprilopening);
                    closingStock = parseFloat(this.props.getOpeningClosing.data.juneclosing);
                }
                else if (this.state.filter === '2qtr') {
                    this.props.getSales.data.forEach(d => (d.type === 'sales' && ['jul', 'aug', 'sep'].includes(d.invoicedate.split(' ')[0].toLowerCase())) ? totalsalesamount = totalsalesamount + parseFloat(d.totalbasic) : totalsalesamount = totalsalesamount)
                    this.props.getPurchase.data.forEach(d => (['jul', 'aug', 'sep'].includes(d.invoicedate.split(' ')[0].toLowerCase()) && d.asset == 0) ? totalpurchaseamount = totalpurchaseamount + parseFloat(d.totalbasic) : totalpurchaseamount = totalpurchaseamount)
                    this.props.getDebitCredit.data.forEach(d => ['apr', 'may', 'jun'].includes(d.date.split(' ')[0].toLowerCase()) ? d.vendorcustomer === 'customer' ? totalsalesamount = totalsalesamount - parseFloat(d.basic) : totalpurchaseamount = totalpurchaseamount - parseFloat(d.basic) : '');
                    this.props.getExpense.data.forEach(d => {
                        if (['jul', 'aug', 'sep'].includes(d.date.split(' ')[0].toLowerCase())) {
                            expense = {
                                ...expense,
                                [d.category]: parseFloat(d.amount) + (expense[d.category] ? expense[d.category] : 0)
                            }
                            totalexpense = totalexpense + parseFloat(d.amount);
                        }
                    })
                    this.props.getIncomeOS.data.forEach(d => {
                        if (['jul', 'aug', 'sep'].includes(d.date.split(' ')[0].toLowerCase())) {
                            income = {
                                ...income,
                                [d.category]: parseFloat(d.amount) + (income[d.category] ? income[d.category] : 0)
                            }
                            totalincome = totalincome + parseFloat(d.amount);
                        }
                    })
                    openingStock = parseFloat(this.props.getOpeningClosing.data.julyopening);
                    closingStock = parseFloat(this.props.getOpeningClosing.data.septemberclosing);
                }
                else if (this.state.filter === '3qtr') {
                    this.props.getSales.data.forEach(d => (d.type === 'sales' && ['oct', 'nov', 'dec'].includes(d.invoicedate.split(' ')[0].toLowerCase())) ? totalsalesamount = totalsalesamount + parseFloat(d.totalbasic) : totalsalesamount = totalsalesamount)
                    this.props.getPurchase.data.forEach(d => (['oct', 'nov', 'dec'].includes(d.invoicedate.split(' ')[0].toLowerCase()) && d.asset == 0) ? totalpurchaseamount = totalpurchaseamount + parseFloat(d.totalbasic) : totalpurchaseamount = totalpurchaseamount)
                    this.props.getDebitCredit.data.forEach(d => ['oct', 'nov', 'dec'].includes(d.date.split(' ')[0].toLowerCase()) ? d.vendorcustomer === 'customer' ? totalsalesamount = totalsalesamount - parseFloat(d.basic) : totalpurchaseamount = totalpurchaseamount - parseFloat(d.basic) : '');
                    this.props.getExpense.data.forEach(d => {
                        if (['oct', 'nov', 'dec'].includes(d.date.split(' ')[0].toLowerCase())) {
                            expense = {
                                ...expense,
                                [d.category]: parseFloat(d.amount) + (expense[d.category] ? expense[d.category] : 0)
                            }
                            totalexpense = totalexpense + parseFloat(d.amount);
                        }
                    })
                    this.props.getIncomeOS.data.forEach(d => {
                        if (['oct', 'nov', 'dec'].includes(d.date.split(' ')[0].toLowerCase())) {
                            income = {
                                ...income,
                                [d.category]: parseFloat(d.amount) + (income[d.category] ? income[d.category] : 0)
                            }
                            totalincome = totalincome + parseFloat(d.amount);
                        }
                    })
                    openingStock = parseFloat(this.props.getOpeningClosing.data.octoberopening);
                    closingStock = parseFloat(this.props.getOpeningClosing.data.decemberclosing);
                }
                else if (this.state.filter === '4qtr') {
                    this.props.getSales.data.forEach(d => (d.type === 'sales' && ['jan', 'feb', 'mar'].includes(d.invoicedate.split(' ')[0].toLowerCase())) ? totalsalesamount = totalsalesamount + parseFloat(d.totalbasic) : totalsalesamount = totalsalesamount)
                    this.props.getPurchase.data.forEach(d => (['jan', 'feb', 'mar'].includes(d.invoicedate.split(' ')[0].toLowerCase()) && d.asset == 0) ? totalpurchaseamount = totalpurchaseamount + parseFloat(d.totalbasic) : totalpurchaseamount = totalpurchaseamount)
                    this.props.getDebitCredit.data.forEach(d => ['jan', 'feb', 'mar'].includes(d.date.split(' ')[0].toLowerCase()) ? d.vendorcustomer === 'customer' ? totalsalesamount = totalsalesamount - parseFloat(d.basic) : totalpurchaseamount = totalpurchaseamount - parseFloat(d.basic) : '');
                    this.props.getExpense.data.forEach(d => {
                        if (['jan', 'feb', 'mar'].includes(d.date.split(' ')[0].toLowerCase())) {
                            expense = {
                                ...expense,
                                [d.category]: parseFloat(d.amount) + (expense[d.category] ? expense[d.category] : 0)
                            }
                            totalexpense = totalexpense + parseFloat(d.amount);
                        }
                    })
                    this.props.getIncomeOS.data.forEach(d => {
                        if (['jan', 'feb', 'mar'].includes(d.date.split(' ')[0].toLowerCase())) {
                            income = {
                                ...income,
                                [d.category]: parseFloat(d.amount) + (income[d.category] ? income[d.category] : 0)
                            }
                            totalincome = totalincome + parseFloat(d.amount);
                        }
                    })
                    openingStock = parseFloat(this.props.getOpeningClosing.data.januaryopening);
                    closingStock = parseFloat(this.props.getOpeningClosing.data.marchclosing);
                }
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div style={{ minHeight: "93vh", display: "flex", flexDirection: "column" }}>
                            <div className="container mt-4" style={{ flex: "1" }}>
                                <div className="d-flex small justify-content-center">
                                    <div className="col-12 col-md-4">
                                        <h6 className="text-center pl-2 pr-2 pt-4" style={{ lineHeight: "1" }}><strong>{this.state.businessname} {this.state.fy}</strong></h6>
                                        <h6 className="text-center pl-2 pr-2 mb-4" style={{ lineHeight: "1" }}>{this.state.alias ? "(" + this.state.alias + ")" : ''}</h6>
                                        <h6 className="text-center"><strong>Profit and Loss Statement</strong>&nbsp;&nbsp;&nbsp;<FontAwesomeIcon icon={faFilePdf} size="lg" onClick={() => this.generatePdf(this.state, totalsalesamount, totalpurchaseamount, openingStock, closingStock, totalincome, totalexpense, totalinvestmentprofit, assetpresent, totalassetprofit, depreciation, expense, income, profit, asset)} />&nbsp;&nbsp;&nbsp;</h6>
                                        <h6 className="text-center"><Period state={this.state} /></h6>
                                        <div className="form-group mb-2 p-1">
                                            <label htmlFor="configvalue" className="mb-0 muted-text"><strong>Select Period<span className="text-danger"> </span></strong></label>
                                            <Control.select model=".configvalue" className="form-control form-control-sm" id="configvalue" name="filter" onChange={this.handleInputChange} value={this.state.filter}>
                                                <option value="fy">{"for FY" + this.state.fy}</option>
                                                <option value="1qtr">{'for 1st Qtr of ' + this.state.fy}</option>
                                                <option value="2qtr">{'for 2nd Qtr of ' + this.state.fy}</option>
                                                <option value="3qtr">{'for 3rd Qtr of ' + this.state.fy}</option>
                                                <option value="4qtr">{'for 4th Qtr of ' + this.state.fy}</option>
                                            </Control.select>
                                        </div>
                                        <div>
                                            <div><strong>Revenue</strong></div>
                                            <div className="col-6 float-left pl-1">Total Sales</div><div className="col-6 float-right text-right ">{parseFloat(totalsalesamount).toFixed(2)}</div>
                                            <div className="col-6 float-left pl-1">Closing Stock</div><div className="col-6 float-right text-right ">{parseFloat(closingStock).toFixed(2)}</div>
                                        </div>
                                        {totalincome > 0 ? <div>
                                            <div><strong>Income - OS</strong></div>
                                            <Income getIncomeHead={this.props.getIncomeHead} income={income} />
                                        </div> : ''}
                                        <div>
                                            <div className="col-6 float-left p-0 mt-1"><strong>Total Revenue</strong></div><div className="col-6 border-top border-dark float-right text-right mt-1"><strong>{(parseFloat(totalsalesamount) + parseFloat(closingStock) + parseFloat(totalincome)).toFixed(2)}</strong></div>
                                        </div>
                                        <div>
                                            <div className="col-6 float-left p-0 mt-3"><strong>Purchase</strong></div><div className="col-6 float-right text-right mt-3 mb-2"><strong>{parseFloat(totalpurchaseamount).toFixed(2)}</strong></div>
                                        </div>
                                        <div>
                                            <div><strong>Expense</strong></div>
                                            <div className="col-6 float-left pl-1">Opening Stock</div><div className="col-6 float-right text-right ">{parseFloat(openingStock).toFixed(2)}</div>
                                            <Expense getExpenseHead={this.props.getExpenseHead} expense={expense} />
                                        </div>
                                        <div>
                                            <div className="col-6 float-left p-0 mt-1"><strong>Depreciation</strong></div><div className="col-6 float-right text-right mt-1"><strong>{parseFloat(depreciation).toFixed(2)}</strong></div>
                                        </div>
                                        <div>
                                            <div className="col-6 float-left p-0 mt-1"><strong>Total Expenditure</strong></div><div className="col-6 float-right text-right border-top border-dark mt-1"><strong>{(parseFloat(totalexpense + totalpurchaseamount + openingStock + depreciation)).toFixed(2)}</strong></div>
                                        </div>
                                        {assetpresent === true ? <div className="col-12 float-left p-0 mt-3"><div>
                                            <div><strong>Asset Sold</strong></div>
                                            <Asset asset={asset} />
                                        </div>
                                            <div>
                                                <div className="col-6 float-left p-0 mt-1"><strong>{totalassetprofit > 0 ? 'Profit From Asset' : 'Loss From Asset'}</strong></div><div className="col-6 float-right text-right border-top border-dark mt-1"><strong>{totalassetprofit > 0 ? parseFloat(totalassetprofit).toFixed(2) : (-1 * parseFloat(totalassetprofit)).toFixed(2)}</strong></div>
                                            </div></div> : ''}
                                        <div>
                                            <div className="col-6 float-left p-0 mt-2 mb-2"><strong>Profit</strong></div><div className="col-6 float-right text-right mt-2 mb-2"><strong>{(parseFloat(totalsalesamount + closingStock + totalincome + totalinvestmentprofit + totalassetprofit) - parseFloat(totalexpense + totalpurchaseamount + openingStock + depreciation)).toFixed(2)}</strong></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <Footer />
                        </div>
                    </div>
                );
            }
        }
    }
}

export default ProfitLoss;