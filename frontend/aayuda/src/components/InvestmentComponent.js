import React, { Component } from 'react';
import cookie from 'react-cookie';
import { Redirect } from 'react-router-dom';
import { LocalForm, Control } from 'react-redux-form';
import { Header } from './HeaderComponent';
import { Footer } from './Footer';

class Investment extends Component {
    constructor(props) {
        super(props);
        this.state = ({
            activity: 'deposit',
            madereceived: 'inward',
            amount: 0,
            extraamount: 0,
            paymentmode: '',
            remark: '',
            date: new Date().toDateString().split(' ').slice(1).join(' '),
            bankdetail: '',
            voucherno: this.props.getInvestment.voucherno ? this.props.getInvestment.voucherno : 1,
            investmentaccount: '',
            businesstype: '',
            drawingtype: ''
        })
        window.onload = () => {
            this.props.refreshBankState();
            this.props.refreshInvestmentState();
            this.props.refreshInvestmentAccountState();
        }
    }
    componentDidMount() {
        if (this.props.addInvestmentMessage.message === 'success') {
            alert("Entry Added Successfully");
            this.props.resetaddInvestmentMessage();
        }
        else if (this.props.addInvestmentMessage.message === 'Already Registered') {
            alert("Voucher Number " + (parseInt(this.props.getPayment.voucherno) - 1) + "already registered");
            this.props.resetaddInvestmentMessage();
        }
        if (this.props.addInvestmentMessage.error !== '') {
            alert(this.props.addInvestmentMessage.error);
            this.setState({
                ...this.props.addInvestmentMessage.state
            })
            this.props.resetaddInvestmentMessage();
        }
        if (this.props.getInvestment.message === 'initial') this.props.getInvestmentDetail(this.props.businessid, this.props.fyid);
        if (this.props.getBankDetail.message === 'initial') this.props.getBankDetailList(this.props.businessid);
        if (this.props.getInvestmentAccount.message === 'initial') this.props.getInvestmentAccountList(this.props.businessid);
        if (this.props.logoutMessage.error !== '') {
            alert("Error Logging Out. Please Try Again");
            this.props.resetLogoutMessage();
        }
    }
    handleInputChange = (event) => {
        let target = event.target;
        let name = target.name;
        let value = target.value;
        this.setState({
            ...this.state,
            [name]: value
        })

    }
    handleSubmit = () => {
        let business = this.props.businessList.data.filter(b => b.businessid === this.props.businessid)[0];
        let fy = business.financial.filter(f => f.fyid === this.props.fyid)[0].financialyear;
        if ((this.state.activity === '') && ((business.businesstype === 'partnership') || (business.businesstype === 'propritorship'))) alert("Please Select Deposit/Drawings");
        else if ((this.state.activity === '') && (business.businesstype === 'ltd')) alert("Please Select Deposit/Buy Back");
        else if (isNaN(parseInt(this.state.voucherno)) || (this.state.voucherno < 0)) alert("Voucher No is mandatory and Must Be In Digits");
        else if (this.state.madereceived === '') alert("Please Select Investment (Made/Received)");
        else if ((this.state.investmentaccount === '') && (business.businesstype !== 'propritorship')) alert("Please Select Account");
        else if (this.state.paymentmode === '') alert("Please Select Payment Mode");
        else if (!(/^[a-zA-Z]{3} [0-9]{2} [0-9]{4}$/i.test(this.state.date))) alert("Date Must Be In (MON DD YYYY) Format");
        else if ((new Date(this.state.date) < new Date("01 Apr " + fy.split("-")[0])) || (new Date(this.state.date) > new Date("31 Mar " + fy.split("-")[1]))) alert("Invalid Date. Date Must Be Within FY " + fy);
        else if (isNaN(parseFloat(this.state.amount)) || (this.state.amount < 0)) alert("Amount is mandatory and Must Be Positive Number");
        else if (this.state.remark.length > 199) alert("Remark Must Be Within 200 Characters");
        else if (this.state.bankdetail === '') alert("Please Select Bank Account");
        else if ((business.businesstype === 'ltd') && ((isNaN(parseFloat(this.state.amount)) || (this.state.amount < 0)))) alert("Amount Towards Reserves Is Mandatory And Must Be Positive Number");
        else this.props.raiseInvestment(this.props.businessid, this.props.fyid, this.state, fy);

    }
    render() {
        if (!cookie.load('userservice')) return <Redirect to="/" />;
        else {
            if ((this.props.addInvestmentMessage.loading === true) || (this.props.getInvestment.loading === true) || (this.props.logoutMessage.loading === true) || (this.props.getBankDetail.loading === true) || (this.props.getInvestmentAccount.loading === true)) {
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div className="mt-3 d-flex justify-content-center">
                            <button className="btn btn-danger">Loading... </button>
                        </div>
                    </div>
                );
            }
            else if ((this.props.getInvestment.error !== '') || (this.props.getBankDetail.error !== '') || (this.props.getInvestmentAccount.error !== '')) {
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div className="mt-3 d-flex justify-content-center">
                            <button className="btn btn-danger">Error.. Please Refresh and Try Agin </button>
                        </div>
                    </div>
                );
            }
            else {
                let business = this.props.businessList.data.filter(b => b.businessid === this.props.businessid)[0];
                let fy = business.financial.filter(f => f.fyid === this.props.fyid)[0].financialyear;
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div style={{ minHeight: "93vh", display: "flex", flexDirection: "column" }}>
                            <div className="container" style={{ flex: "1" }}>
                                <h6 className="text-center pl-2 pr-2 pt-4" style={{ lineHeight: "1" }}><strong>{business.businessname} {fy}</strong></h6>
                                <h6 className="text-center pl-2 pr-2 mb-4" style={{ lineHeight: "1" }}>{business.alias ? "(" + business.alias + ")" : ''}</h6>
                                <h6 className="text-center pl-2 pr-2"><strong><u>{business.businesstype === 'partnership' ? "Partners Capital Account Entry" : ''}</u></strong></h6>
                                <h6 className="text-center pl-2 pr-2"><strong><u>{business.businesstype === 'ltd' ? "Share Holders Capital Entry" : ''}</u></strong></h6>
                                <h6 className="text-center pl-2 pr-2"><strong><u>{business.businesstype === 'propritorship' ? "Capital Account Entry" : ''}</u></strong></h6>
                                <div className="row" >
                                    <LocalForm className="small" onSubmit={this.handleSubmit}>
                                        <div className="container mt-4 d-flex justify-content-center">
                                            <div class="row col-12 col-md-4">
                                                {business.businesstype === 'partnership' || business.businesstype === 'propritorship' ? <div className="form-group mb-1 col-12">
                                                    <div className="form-group mb-1 col-12">
                                                        <label className="col-6 p-0" ><input type="radio" name="activity" id="activity" value="deposit" checked={this.state.activity === "deposit" ? true : false} onChange={this.handleInputChange} />Deposit Capital</label>&nbsp;&nbsp;&nbsp;&nbsp;
                                                <label className="col-5 p-0"><input type="radio" name="activity" id="activity" value="drawing" checked={this.state.activity === "drawing" ? true : false} onChange={this.handleInputChange} />Drawings</label>
                                                    </div>
                                                </div> : ''}
                                                {business.businesstype !== 'propritorship' ? <div className="form-group mb-1 col-12">
                                                    <label htmlFor="investmentaccount" className="mb-0 muted-text"><strong>{business.businesstype === 'partnership' ? 'Select Partner Account' : ''}{business.businesstype === 'ltd' ? 'Select Share Holders Account' : ''}</strong><span className="text-danger">*</span></label>
                                                    <Control.select model=".investmentaccount" className="form-control form-control-sm" id="investmentaccount" name="investmentaccount" onChange={this.handleInputChange} value={this.state.investmentaccount}>
                                                        <option value="">{business.businesstype === 'partnership' ? 'Select Partner Account' : ''}{business.businesstype === 'ltd' ? 'Select Share Holders Account' : ''}</option>
                                                        {this.props.getInvestmentAccount.data.map(b => <option value={b.id + "..." + b.investmentaccount}>{b.investmentaccount}</option>)}
                                                    </Control.select>
                                                </div> : ''}
                                                <div className="form-group mb-1 col-12">
                                                    <label htmlFor="voucherno" className="mb-0 muted-text"><strong>Voucher No</strong><span className="text-danger">*</span></label>
                                                    <Control.text model=".voucherno" className="form-control form-control-sm" id="voucherno" name="voucherno" onChange={this.handleInputChange} value={this.state.voucherno} />
                                                </div>
                                                <div className="form-group mb-1 col-12">
                                                    <label htmlFor="date" className="mb-0 muted-text"><strong>Date<span className="text-danger">*</span></strong></label>
                                                    <Control.text model=".date" className="form-control form-control-sm" id="date" name="date" onChange={this.handleInputChange} value={this.state.date} />
                                                </div>
                                                {business.businesstype === 'partnership' || business.businesstype === 'propritorship' ? <div className="form-group mb-1 col-12">
                                                    <label htmlFor="amount" className="mb-0 muted-text"><strong>Amount<span className="text-danger">*</span></strong></label>
                                                    <Control.text model=".amount" className="form-control form-control-sm" id="amount" name="amount" onChange={this.handleInputChange} value={this.state.amount} />
                                                </div> : ''}
                                                {business.businesstype === 'ltd' ? <div><div className="form-group mb-1 col-12">
                                                    <label htmlFor="amount" className="mb-0 muted-text"><strong>Amount Towards Equity Capital<span className="text-danger">*</span></strong></label>
                                                    <Control.text model=".amount" className="form-control form-control-sm" id="amount" name="amount" onChange={this.handleInputChange} value={this.state.amount} />
                                                </div><div className="form-group mb-1 col-12">
                                                        <label htmlFor="extraamount" className="mb-0 muted-text"><strong>Amount Towards Reserves<span className="text-danger">*</span></strong></label>
                                                        <Control.text model=".extraamount" className="form-control form-control-sm" id="extraamount" name="extraamount" onChange={this.handleInputChange} value={this.state.extraamount} />
                                                    </div></div> : ''}
                                                <div className="form-group mb-1 col-12">
                                                    <label htmlFor="paymentmode" className="mb-0 muted-text"><strong>Payment Mode:</strong><span className="text-danger">*</span></label>
                                                    <Control.select model=".paymentmode" className="form-control form-control-sm" id="paymentmode" name="paymentmode" onChange={this.handleInputChange} value={this.state.paymentmode}>
                                                        <option value="">Payment Mode</option>
                                                        <option value="cheque">Cheque</option>
                                                        <option value="rtgs">RTGS/NEFT</option>
                                                        <option value="other">Other digital payment mode</option>
                                                    </Control.select>
                                                </div>
                                                {(this.state.paymentmode !== 'cash') ? <div className="form-group mb-1 col-12">
                                                    <label htmlFor="bankdetail" className="mb-0 muted-text"><strong>Select Bank Account:</strong><span className="text-danger">*</span></label>
                                                    <Control.select model=".bankdetail" className="form-control form-control-sm" id="bankdetail" name="bankdetail" onChange={this.handleInputChange} value={this.state.bankdetail}>
                                                        <option value="">Select Bank Name</option>
                                                        {this.props.getBankDetail.data.map(b => <option value={b.bankdetailid + "..." + b.bankname}>{b.bankname}</option>)}
                                                    </Control.select>
                                                </div> : ''}
                                                {this.state.sellinvestment === true ? <div className="form-group mb-1 col-12">
                                                    <label htmlFor="amountreserves" className="mb-0 muted-text"><strong>Amount From Reserves<span className="text-danger">*</span></strong></label>
                                                    <Control.text model=".amountreserves" className="form-control form-control-sm" id="amountreserves" name="amountreserves" onChange={this.handleInputChange} value={this.state.amountreserves} />
                                                </div> : ''}
                                                <div className="form-group mb-1 col-12">
                                                    <label htmlFor="remark" className="mb-0 muted-text"><strong>Remarks</strong></label>
                                                    <Control.text model=".remark" className="form-control form-control-sm" id="remark" name="remark" onChange={this.handleInputChange} value={this.state.remark} />
                                                </div>
                                                <div className="form-group mb-1 col-12">
                                                    <button type="submit" className="btn btn-success mt-1 btn-sm small" >Enter</button>
                                                </div>
                                            </div>
                                        </div>
                                    </LocalForm>
                                </div>
                            </div>
                            <Footer />
                        </div>
                    </div>
                )
            }
        }
    }
}

export default Investment;