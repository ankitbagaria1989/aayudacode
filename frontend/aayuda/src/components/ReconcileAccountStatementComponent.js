import React, { Component } from 'react';
import cookie from 'react-cookie';
import { Redirect } from 'react-router-dom';
import { LocalForm, Errors, Control, Field } from 'react-redux-form';
import { Header } from './HeaderComponent';

const DataDisplay = function ({ state, salesDetail, purchaseDetail, getPayment, fyid, getCustomer, getVendor, getOpeningBalance, businessList, businessid, handleInputChange, handleSubmit }) {
    let returnrows = [];
    if (state.saleorpurchase !== '') {
        let business = businessList.data.filter(b => b.businessid === businessid)[0];
        let financial = business.financial.filter(f => f.fyid === fyid)[0];
        if (state.saleorpurchase === 'sale') {
            let paymentData = getPayment.data.filter(p => p.customerid !== '')
            returnrows.push(
                <div>
                    <h6>Reconciled Account Statement - Sale - {financial.financialyear}</h6>
                    <div className="row small mb-3">
                        <div className="col-2 p-0"><strong>Date</strong></div>
                        <div className="col-2 p-0"><strong>Voucher No</strong></div>
                        <div className="col-4 p-0"><strong>Customer</strong></div>
                        <div className="col-2 p-0"><strong>Debit</strong></div>
                        <div className="col-2 p-0"><strong>Credit</strong></div>
                    </div>
                </div>)
            getOpeningBalance.data.forEach(ob => {
                if (ob.party === 'customer') {
                    let customer = undefined;
                    getCustomer.data.forEach(c => {
                        if (c.customerid === ob.partyid) customer = c.businessname;
                    })
                    returnrows.push(
                        <div>
                            <div className="row small">
                                <div className="col-8 p-0"><strong>Opening Balance - {customer}</strong></div>
                                <div className="col-2 p-0"><strong>{ob.amount}</strong></div>
                                <div className="col-2 p-0"><strong></strong></div>
                            </div>
                        </div>)
                }
            })
            let x = salesDetail.data.length;
            let y = paymentData.length;
            let i = 0, j = 0;
            while (i < x && j < y) {
                if (salesDetail.data[i].invoicedata <= paymentData[j].date) {
                    let customer = undefined;
                    getCustomer.data.forEach(c => {
                        if (c.customerid === salesDetail.data[i].customerid) customer = c.businessname;
                    })
                    returnrows.push(
                        <div>
                            <div className="row small mb-3">
                                <div className="col-2 p-0"><strong>{salesDetail.data[i].invoicedate}</strong></div>
                                <div className="col-2 p-0"><strong>{salesDetail.data[i].invoicenumber}</strong></div>
                                <div className="col-4 p-0"><strong>{customer}</strong></div>
                                <div className="col-2 p-0"><strong>{salesDetail.data[i].grandtotal}</strong></div>
                                <div className="col-2 p-0"><strong></strong></div>
                            </div>
                        </div>)
                    i++;
                }
                else {
                    let customer = undefined;
                    getCustomer.data.forEach(c => {
                        if (c.customerid === paymentData[j].customerid) customer = c.businessname;
                    })
                    returnrows.push(
                        <div>
                            <div className="row small mb-3">
                                <div className="col-2 p-0"><strong>{paymentData[i].date}</strong></div>
                                <div className="col-2 p-0"><strong>Payment</strong></div>
                                <div className="col-4 p-0"><strong>{customer}</strong></div>
                                <div className="col-2 p-0"><strong></strong></div>
                                <div className="col-2 p-0"><strong>{paymentData[i].amount}</strong></div>
                            </div>
                        </div>)
                    j++;
                }
            }
            while (i < x) {
                let customer = undefined;
                getCustomer.data.forEach(c => {
                    if (c.customerid === salesDetail.data[i].customerid) customer = c.businessname;
                })
                returnrows.push(
                    <div>
                        <div className="row small mb-3">
                            <div className="col-2 p-0"><strong>{salesDetail.data[i].invoicedate}</strong></div>
                            <div className="col-2 p-0"><strong>{salesDetail.data[i].invoicenumber}</strong></div>
                            <div className="col-4 p-0"><strong>{customer}</strong></div>
                            <div className="col-2 p-0"><strong>{salesDetail.data[i].grandtotal}</strong></div>
                            <div className="col-2 p-0"><strong></strong></div>
                        </div>
                    </div>)
                i++;
            }
            while (j < y) {
                let customer = undefined;
                getCustomer.data.forEach(c => {
                    if (c.customerid === paymentData[j].customerid) customer = c.businessname;
                })
                returnrows.push(
                    <div>
                        <div className="row small mb-3">
                            <div className="col-2 p-0"><strong>{paymentData[i].date}</strong></div>
                            <div className="col-2 p-0"><strong>Payment</strong></div>
                            <div className="col-4 p-0"><strong>{customer}</strong></div>
                            <div className="col-2 p-0"><strong></strong></div>
                            <div className="col-2 p-0"><strong>{paymentData[i].amount}</strong></div>
                        </div>
                    </div>)
                j++;
            }
        }
        else if (state.saleorpurchase === 'purchase') {
            let paymentData = getPayment.data.filter(p => p.vendorid !== '')
            returnrows.push(
                <div>
                    <h6>Reconciled Account Statement - Purchase - {financial.financialyear}</h6>
                    <div className="row small mb-3">
                        <div className="col-2 p-0"><strong>Date</strong></div>
                        <div className="col-2 p-0"><strong>Voucher No</strong></div>
                        <div className="col-4 p-0"><strong>Customer</strong></div>
                        <div className="col-2 p-0"><strong>Debit</strong></div>
                        <div className="col-2 p-0"><strong>Credit</strong></div>
                    </div>
                </div>)
            getOpeningBalance.data.forEach(ob => {
                if (ob.party === 'vendor') {
                    let customer = undefined;
                    getVendor.data.forEach(c => {
                        if (c.vendorid === ob.partyid) customer = c.businessname;
                    })
                    returnrows.push(
                        <div>
                            <div className="row small">
                                <div className="col-8 p-0"><strong>Opening Balance - {customer}</strong></div>
                                <div className="col-2 p-0"><strong></strong></div>
                                <div className="col-2 p-0"><strong>{ob.amount}</strong></div>
                            </div>
                        </div>)
                }
            })
            let x = purchaseDetail.data.length;
            let y = paymentData.length;
            let i = 0, j = 0;
            while (i < x && j < y) {
                if (purchaseDetail.data[i].invoicedata <= paymentData[j].date) {
                    let customer = undefined;
                    getVendor.data.forEach(c => {
                        if (c.vendorid === purchaseDetail.data[i].vendorid) customer = c.businessname;
                    })
                    returnrows.push(
                        <div>
                            <div className="row small">
                                <div className="col-2 p-0"><strong>{purchaseDetail.data[i].invoicedate}</strong></div>
                                <div className="col-2 p-0"><strong>{purchaseDetail.data[i].invoicenumber}</strong></div>
                                <div className="col-4 p-0"><strong>{customer}</strong></div>
                                <div className="col-2 p-0"><strong></strong></div>
                                <div className="col-2 p-0"><strong>{purchaseDetail.data[i].grandtotal}</strong></div>
                            </div>
                        </div>)
                    i++;
                }
                else {
                    let customer = undefined;
                    getVendor.data.forEach(c => {
                        if (c.vendorid === paymentData[j].vendorid) customer = c.businessname;
                    })
                    returnrows.push(
                        <div>
                            <div className="row small">
                                <div className="col-2 p-0"><strong>{paymentData[i].date}</strong></div>
                                <div className="col-2 p-0"><strong>Payment</strong></div>
                                <div className="col-4 p-0"><strong>{customer}</strong></div>
                                <div className="col-2 p-0"><strong>{paymentData[i].amount}</strong></div>
                                <div className="col-2 p-0"><strong></strong></div>
                            </div>
                        </div>)
                    j++;
                }
            }
            while (i < x) {
                let customer = undefined;
                getVendor.data.forEach(c => {
                    if (c.vendorid === purchaseDetail.data[i].vendorid) customer = c.businessname;
                })
                returnrows.push(
                    <div>
                        <div className="row small">
                            <div className="col-2 p-0"><strong>{purchaseDetail.data[i].invoicedate}</strong></div>
                            <div className="col-2 p-0"><strong>{purchaseDetail.data[i].invoicenumber}</strong></div>
                            <div className="col-4 p-0"><strong>{customer}</strong></div>
                            <div className="col-2 p-0"><strong></strong></div>
                            <div className="col-2 p-0"><strong>{purchaseDetail.data[i].grandtotal}</strong></div>
                        </div>
                    </div>)
                i++;
            }
            while (j < y) {
                let customer = undefined;
                getVendor.data.forEach(c => {
                    if (c.vendorid === paymentData[j].customerid) customer = c.businessname;
                })
                returnrows.push(
                    <div>
                        <div className="row small mb-3">
                            <div className="col-2 p-0"><strong>{paymentData[i].date}</strong></div>
                            <div className="col-2 p-0"><strong>Payment</strong></div>
                            <div className="col-4 p-0"><strong>{customer}</strong></div>
                            <div className="col-2 p-0"><strong></strong></div>
                            <div className="col-2 p-0"><strong>{paymentData[i].amount}</strong></div>
                        </div>
                    </div>)
                j++;
            }
        }

    }
    return returnrows;
}

class ReconcileAccountStatement extends Component {
    constructor(props) {
        super(props);
        this.state = ({
            saleorpurchase: ''
        })
        window.onload = () => {
            this.props.resetPaymentDetail();
            this.props.resetOpeningBalance();
            this.props.refreshVendorState();
            this.props.refreshCustomerState();
            this.props.resetSalesDetail();
            this.props.resetPurchaseDetail();
            this.props.resetPaymentDetail();
        }
    }
    componentDidMount() {
        if ((this.props.getVendor.message === 'initial') && (this.props.getVendor.loading === false)) this.props.getVendorList(this.props.businessid);
        if ((this.props.getCustomer.message === 'initial') && (this.props.getCustomer.loading === false)) this.props.getCustomerList(this.props.businessid);
        if ((this.props.salesDetail.message === 'initial') && (this.props.salesDetail.loading === false)) this.props.getSalesData(this.props.businessid, this.props.fyid);
        if ((this.props.purchaseDetail.message === 'initial') && (this.props.purchaseDetail.loading === false)) this.props.getPurchaseData(this.props.businessid, this.props.fyid);
        if ((this.props.getPayment.message === 'initial') && (this.props.getPayment.loading === false)) this.props.getPaymentDetail(this.props.businessid);
        if ((this.props.getOpeningBalance.message === 'initial') && (this.props.getOpeningBalance.loading === false)) this.props.getPartyOpeningBalance(this.props.businessid, this.props.fyid);
        if (this.props.logoutMessage.error !== '') {
            alert("Error Logging Out. Please Try Again");
            this.props.resetLogoutMessage();
        }
    }

    handleInputChange = (event) => {
        let target = event.target;
        let name = target.name;
        let value = target.value;
        this.setState({
            [name]: value
        })
    }

    render() {
        if (!cookie.load('userservice')) return <Redirect to="/" />;
        else {
            if ((this.props.getPayment.loading === true) || (this.props.getOpeningBalance.loading === true) || (this.props.getCustomer.loading === true) || (this.props.getVendor.loading === true) || (this.props.salesDetail.loading === true) || (this.props.purchaseDetail.loading === true) || (this.props.logoutMessage.loading === true)) {
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div className="mt-3 d-flex justify-content-center">
                            <button className="btn btn-danger">Loading... </button>
                        </div>
                    </div>
                );
            }
            else if ((this.props.getPayment.error !== '') || (this.props.getOpeningBalance.error !== '') || (this.props.getCustomer.error !== '') || (this.props.getVendor.error !== '') || (this.props.salesDetail.error !== '') || (this.props.purchaseDetail.error !== '')) {
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div className="mt-3 d-flex justify-content-center">
                            <button className="btn btn-danger">Error.. Please Refresh and Try Again </button>
                        </div>
                    </div>
                );
            }
            else {
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div className="container">
                            <div className="row" >
                                <LocalForm className="small col-12">
                                    <div className="col-container row mt-4">
                                        <div class="col-12 row">
                                            <div className="form-group mb-1 col-6 col-md-3 p-1">
                                                <label htmlFor="saleorpurchase" className="mb-0 muted-text"><strong>Purchase/Sale<span className="text-danger">*</span></strong></label>
                                                <Control.select model=".saleorpurchase" className="form-control form-control-sm" id="saleorpurchase" name="saleorpurchase" onChange={this.handleInputChange} value={this.state.saleorpurchase}>
                                                    <option value="">Purchase/Sale</option>
                                                    <option value="purchase">Purchase Account</option>
                                                    <option value="sale">Sale Account</option>
                                                </Control.select>
                                            </div>
                                        </div>
                                    </div>
                                </LocalForm>
                            </div>
                            <div className="row">
                                <div className="col-md-6">
                                    <DataDisplay state={this.state} salesDetail={this.props.salesDetail} purchaseDetail={this.props.purchaseDetail} getPayment={this.props.getPayment} fyid={this.props.fyid} getCustomer={this.props.getCustomer} getVendor={this.props.getVendor} getOpeningBalance={this.props.getOpeningBalance} businessList={this.props.businessList} businessid={this.props.businessid} handleInputChange={this.handleInputChange} handleSubmit={this.handleSubmit} />
                                </div>
                            </div>
                        </div>
                    </div>
                )
            }
        }
    }
}

export default ReconcileAccountStatement;