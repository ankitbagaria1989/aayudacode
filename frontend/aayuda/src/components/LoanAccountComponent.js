import React, { Component, useState } from 'react';
import cookie from 'react-cookie';
import { Redirect } from 'react-router-dom';
import { Button, Collapse } from 'reactstrap';
import { Form, Errors, Control } from 'react-redux-form';
import { Header } from './HeaderComponent';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faAngleDoubleDown } from '@fortawesome/free-solid-svg-icons';
import { Footer } from './Footer';

const required = (val) => val && val.length;
const maxLength = (len) => val => val ? val.length < len : true;

const ErrorComponent = function ({ children }) {
    return (
        <div style={{ lineHeight: "1" }}>{children}</div>
    )
}
const AddLoanAccount = function ({ handleSubmit, addLoanAccountMessage, businessid }) {
    let [isOpen, setIsOpen] = useState(false);
    let toggle = () => setIsOpen((!isOpen));
    if (addLoanAccountMessage.error !== '') {
        isOpen = true;
    }
    return (
        <div className="col-12">
            <Button className="btn-sm mt-3 col-12" style={{ backgroundColor: "#fff", border: "1px solid #28a745", color: "#28a745" }} onClick={toggle}><div className="d-flex"><span className="col-11 text-center">Add a New Loan Account</span><span className="col-1 p-0 text-right"><FontAwesomeIcon size="sm" icon={faAngleDoubleDown} /></span></div></Button>
            <Collapse isOpen={isOpen}>
                <Form model="addLoanDetail" onSubmit={((values) => handleSubmit(values, businessid))}>
                    <div className="justify-content-center mt-4">
                        <div className="form-group col-12 mb-1">
                            <label htmlFor="loanaccount" className="mb-0"><span className="muted-text">Loan Account Name<span className="text-danger">*</span></span></label>
                            <div className="">
                                <Control.text model=".loanaccount" className="form-control form-control-sm pt-3 pb-3" id="loanaccount" name="loanaccount" placeholder="Loan Account Name" validators={{ required, maxLength: maxLength(100) }} />
                                <Errors className="text-danger" model=".loanaccount" show="touched" messages={{
                                    required: "Loan Account Name is Mandatory",
                                    maxLength: "length must be within 100 characters"
                                }}
                                    component={ErrorComponent}
                                />
                            </div>
                            <div className="mt-3"><button type="submit" className="btn btn-success btn-sm pl-4 pr-4" >Add</button></div>
                        </div>
                    </div>
                </Form>
            </Collapse>
        </div>
    );
}

const LoanAccountDetailCard = function ({ loanAccountDetail }) {
    return (
        <div className="col-12 justify-content-center">
            <div className="">
                <Button className="btn-sm btn mt-2 col-12 btn-success"><div className="d-flex"><span className="col-11">{loanAccountDetail.loanaccount}</span><span className="col-1 p-0 text-right"></span></div></Button>
            </div>
        </div>
    );
}

const LoanAccountDetailList = function ({ getLoanAccount }) {
    if ((getLoanAccount.error === '')) {
        if (getLoanAccount.data.length > 0) return getLoanAccount.data.map((d) => <LoanAccountDetailCard loanAccountDetail={d} />);
        else return <h6 className="col-12 text-center text-muted mt-2 pb-4">No Loan Account Added Yet</h6>;
    }
    else return <div classname="row col-12 card text-center"></div>;
}

class LoanAccount extends Component {
    componentDidMount() {
        window.onload = () => {
            this.props.refreshLoanAccountState();
        }
        if (this.props.getLoanAccount.message === 'initial') this.props.getLoanAccountList(this.props.businessid);
        if (this.props.addLoanAccountMessage.error !== '') {
            alert("Error " + this.props.addLoanAccountMessage.error);
            this.props.resetAddLoanAccountMessage();
        }
        if (this.props.addLoanAccountMessage.message === 'success') {
            alert("Loan Account Added Successfully");
            this.props.resetAddLoanAccountMessageAndForm();
        }
        if (this.props.logoutMessage.error !== '') {
            alert("Error Logging Out. Please Try Again");
            this.props.resetLogoutMessage();
        }
    }
    handleSubmit = (values, businessid) => {
        this.props.addLoanAccount(values, businessid);
        //if (this.props.addFinancialYear.message === "success") window.location.reload();
    }
    render() {
        if (!cookie.load('userservice')) return <Redirect to="/" />
        else {
            if ((this.props.getLoanAccount.loading === true) || (this.props.addLoanAccountMessage.loading === true) || (this.props.logoutMessage.loading === true)) {
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div className="mt-3 d-flex justify-content-center">
                            <button className="btn btn-danger">Loading... </button>
                        </div>
                    </div>
                );
            }
            else if (this.props.getLoanAccount.error !== '') {
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div className="mt-3 d-flex justify-content-center">
                            <button className="btn btn-danger">Error.. Please Refresh and Try Agin </button>
                        </div>
                    </div>
                );
            }
            else {
                let business = this.props.businessList.data.filter(b => b.businessid === this.props.businessid)[0];
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div style={{ minHeight: "93vh", display: "flex", flexDirection: "column" }}>
                            <div className="container d-flex justify-content-center pr-0 pl-0">
                                <div className="col-12 col-md-5">
                                    <div className="small pr-0 pl-0">
                                        <h6 className="text-center pl-2 pr-2 pt-4" style={{ lineHeight: "1" }}><strong>{business.businessname}</strong></h6>
                                        <h6 className="text-center pl-2 pr-2 mb-4" style={{ lineHeight: "1" }}>{business.alias ? "(" + business.alias + ")" : ''}</h6>
                                        <h6 className="text-center pl-2 pr-2 mb-2"><strong><u>Loan Account</u></strong></h6>
                                        <AddLoanAccount handleSubmit={this.handleSubmit} addLoanAccountMessage={this.props.addLoanAccountMessage} businessid={this.props.businessid} />
                                    </div>
                                </div>
                            </div>
                            <div className="container d-flex justify-content-center pr-0 pl-0 mt-4 pb-4" style={{ flex: "1" }}>
                                <div className="col-12 col-md-5">
                                    <div className="small pr-0 pl-0">
                                        <LoanAccountDetailList getLoanAccount={this.props.getLoanAccount} />
                                    </div>
                                </div>
                            </div>
                            <Footer />
                        </div>
                    </div>
                );
            }
        }
    }
}

export default LoanAccount;