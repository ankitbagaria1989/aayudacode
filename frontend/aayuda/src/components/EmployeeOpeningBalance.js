import React, { Component } from 'react';
import cookie from 'react-cookie';
import { Redirect } from 'react-router-dom';
import { LocalForm, Control } from 'react-redux-form';
import { Header } from './HeaderComponent';
import { Footer } from './Footer';

class EmployeeOpeningBalance extends Component {
    constructor(props) {
        super(props);
        this.state = ({
            employeeaccount: '',
            amount: 0,
            payablepaidadvance: '',

        })
        window.onload = () => {
            this.props.refreshEmployeeState();
            this.props.resetEmployeeOpeningBalance();
        }
    }
    handleSubmit = () => {
        let business = this.props.businessList.data.filter(b => b.businessid === this.props.businessid)[0];
        if (this.state.employeeaccount === '') alert("Employee Account Not Selected");
        else if (this.state.payablepaidadvance === '') alert("Please Select Payable/Paid Advance");
        else this.props.updateEmployeeOpeningBalance(this.state, this.props.businessid, this.props.fyid);
    }
    componentDidMount() {
        if (this.props.updateEmployeeOpeningMessage.error !== '') {
            alert(this.props.updateEmployeeOpeningMessage.error);
            this.setState({
                ...this.props.updateEmployeeOpeningMessage.state
            })
            this.props.refreshUpdateEmployeeOpeningBalance();
        }
        if (this.props.updateEmployeeOpeningMessage.message === 'success') {
            alert("Data Updated Successfully");
            this.setState({
                ...this.props.updateEmployeeOpeningMessage.state
            })
            this.props.refreshUpdateEmployeeOpeningBalance();
        }
        if (this.props.getEmployeeOpeningBalance.message === 'initial') this.props.getEmployeeOpeningBalanceList(this.props.businessid, this.props.fyid);
        if (this.props.logoutMessage.error !== '') {
            alert("Error Logging Out. Please Try Again");
            this.props.resetLogoutMessage();
        }
        if (this.props.getEmployee.message === 'initial') this.props.getEmployeeList(this.props.businessid);
        if (this.props.getEmployeeOpeningBalance.message === 'success') {
            if (this.props.getEmployeeOpeningBalance.data.length) {
                let employee = this.props.getEmployeeOpeningBalance.data.filter(e => e.employeeaccount === this.state.employeeaccount && e.payablepaidadvance === this.state.payablepaidadvance)[0];
                this.setState({
                    ...this.state,
                    amount: employee ? employee.amount : 0
                })
            }
        }
    }
    handleInputChange = (event) => {
        let target = event.target;
        let name = target.name;
        let value = target.value;
        if (name === 'employeeaccount') {
            let temp = this.props.getEmployeeOpeningBalance.data.filter(e => e.employeeaccount === value)[0];
            this.setState({
                employeeaccount: value,
                amount: temp ? temp.amount : 0,
                payablepaidadvance: temp ? temp.payablepaidadvance : this.state.payablepaidadvance
            })
        }
        else {
            this.setState({
                [name]: value
            })
        }
    }
    render() {
        if (!cookie.load('userservice')) return <Redirect to="/" />;
        else {
            if ((this.props.updateEmployeeOpeningMessage.loading === true) || (this.props.getEmployeeOpeningBalance.loading === true) || (this.props.logoutMessage.loading === true) || (this.props.getEmployee.loading === true)) {
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div className="mt-3 d-flex justify-content-center">
                            <button className="btn btn-danger">Loading... </button>
                        </div>
                    </div>
                );
            }
            else if ((this.props.getEmployeeOpeningBalance.error !== '') || (this.props.getEmployee.error !== '')) {
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div className="mt-3 d-flex justify-content-center">
                            <button className="btn btn-danger">Error. Please Refresh and Try Again</button>
                        </div>
                    </div>
                );
            }
            else {
                let business = this.props.businessList.data.filter(b => b.businessid === this.props.businessid)[0];
                let fy = business.financial.filter(f => f.fyid === this.props.fyid)[0].financialyear;
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div style={{ minHeight: "93vh", display: "flex", flexDirection: "column" }}>
                            <div className="container" style={{ flex: "1" }}>
                                <h6 className="text-center pl-2 pr-2 pt-4" style={{ lineHeight: "1" }}><strong>{business.businessname} {fy}</strong></h6>
                                <h6 className="text-center pl-2 pr-2 mb-4" style={{ lineHeight: "1" }}>{business.alias ? "(" + business.alias + ")" : ''}</h6>
                                <h6 className="text-center pl-2 pr-2 col-12"><strong><u>Employee Account Opening Balance</u></strong></h6>
                                <div className="row">
                                    <LocalForm className="small col-12" onSubmit={this.handleSubmit}>
                                        <div className="container mt-4 d-flex justify-content-center">
                                            <div class="row col-12 col-md-4">
                                                <div className="col-12">
                                                    <div className="form-group mb-1 col-12">
                                                        <div className="form-group mb-1 col-12">
                                                            <label className="col-6 p-0" ><input type="radio" name="payablepaidadvance" id="payablepaidadvance" value="payable" checked={this.state.payablepaidadvance === "payable" ? true : false} onChange={this.handleInputChange} /> Payable</label>&nbsp;&nbsp;&nbsp;&nbsp;
                                                <label className="col-5 p-0"><input type="radio" name="payablepaidadvance" id="payablepaidadvance" value="paidadvance" checked={this.state.payablepaidadvance === "paidadvance" ? true : false} onChange={this.handleInputChange} /> Paid Advance</label>
                                                        </div>
                                                    </div><div className="form-group mb-1 col-12">
                                                        <label htmlFor="employeeaccount" className="mb-0 muted-text"><strong>Employee Account<span className="text-danger">*</span></strong></label>
                                                        <Control.select model=".employeeaccount" className="form-control form-control-sm" id="employeeaccount" name="employeeaccount" onChange={this.handleInputChange} value={this.state.employeeaccount}>
                                                            <option value="">Employee Account</option>
                                                            {this.props.getEmployee.data.map(e => <option value={e.employeeid + "..." + e.employeename}>{e.employeename}</option>
                                                            )}
                                                        </Control.select>
                                                    </div>
                                                    {this.state.employeeaccount !== '' ? <div className="form-group mb-1 col-12">
                                                        <label htmlFor="amount" className="mb-0 muted-text"><strong>Opening Value Amount</strong><span className="text-danger">*</span></label>
                                                        <Control.text model=".amount" className="form-control form-control-sm" id="amount" name="amount" onChange={this.handleInputChange} value={this.state.amount} />
                                                    </div> : ''}
                                                    {this.state.employeeaccount !== '' ? <div className="form-group mb-1 col-12">
                                                        <button type="submit" className="btn btn-success mt-1 btn-sm small" >Update</button>
                                                    </div> : ''}
                                                </div>
                                            </div>
                                        </div>
                                    </LocalForm>
                                </div>
                            </div>
                            <Footer />
                        </div>
                    </div>
                )
            }
        }
    }
}

export default EmployeeOpeningBalance;