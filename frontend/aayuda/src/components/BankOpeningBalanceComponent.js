import React, { Component } from 'react';
import cookie from 'react-cookie';
import { Redirect } from 'react-router-dom';
import { LocalForm, Control } from 'react-redux-form';
import { Header } from './HeaderComponent';
import { Footer } from './Footer';

class BankOpeningBalance extends Component {
    constructor(props) {
        super(props);
        this.state = ({
            bankid: '',
            amount: 0
        })
        window.onload = () => {
            this.props.refreshBankState();
            this.props.resetBankOpeningBalance();
        }
    }
    handleSubmit = () => {
        if (this.props.bankid === '') alert("Bank Not Selected");
        else {
            if ((this.state.amount === '') || (this.state.amount < 0) || (isNaN(parseInt(this.state.amount)))) alert(" Bank Opening Balance Muste Be Greater Than Equal To 0 ");
            else this.props.updateBankOpeningBalance(this.state, this.props.businessid, this.props.fyid);
        }
    }
    componentDidMount() {
        if (this.props.updateBankOpeningMessage.error !== '') {
            alert("Error Updating Data");
            this.setState({
                ...this.props.updateBankOpeningMessage.state
            })
            this.props.refreshUpdateBankOpeningBalance();
        }
        if (this.props.updateBankOpeningMessage.message === 'success') {
            alert("Data Updated Successfully");
            this.setState({
                ...this.props.updateBankOpeningMessage.state
            })
            this.props.refreshUpdateBankOpeningBalance();
        }
        if (this.props.getBankOpeningBalance.message === 'initial') this.props.getBankOpeningBalanceList(this.props.businessid, this.props.fyid);
        if (this.props.logoutMessage.error !== '') {
            alert("Error Logging Out. Please Try Again");
            this.props.resetLogoutMessage();
        }
        if (this.props.getBankDetail.message === 'initial') this.props.getBankDetailList(this.props.businessid);
    }

    handleInputChange = (event) => {
        let target = event.target;
        let name = target.name;
        let value = target.value;
        if (name === 'bankid') {
            let bank = this.props.getBankOpeningBalance.data.filter(d => d.bankid === value)[0];
            let amount = bank ? bank.amount : 0
            this.setState({
                bankid: value,
                amount: amount
            })

        }
        else {
            this.setState({
                [name]: value
            })
        }
    }
    render() {
        if (!cookie.load('userservice')) return <Redirect to="/" />;
        else {
            if ((this.props.updateBankOpeningMessage.loading === true) || (this.props.getBankOpeningBalance.loading === true) || (this.props.logoutMessage.loading === true)) {
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div className="mt-3 d-flex justify-content-center">
                            <button className="btn btn-danger">Loading... </button>
                        </div>
                    </div>
                );
            }
            else if (this.props.getBankOpeningBalance.loading === true) {
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div className="mt-3 d-flex justify-content-center">
                            <button className="btn btn-danger">Error. Please Refresh and Try Again</button>
                        </div>
                    </div>
                );
            }
            else {
                let business = this.props.businessList.data.filter(b => b.businessid === this.props.businessid)[0];
                let fy = business.financial.filter(f => f.fyid === this.props.fyid)[0].financialyear;
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div style={{ minHeight: "93vh", display: "flex", flexDirection: "column" }}>
                            <div className="container" style={{ flex: "1" }}>
                                <h6 className="text-center pl-2 pr-2 pt-4" style={{ lineHeight: "1" }}><strong>{business.businessname} {fy}</strong></h6>
                                <h6 className="text-center pl-2 pr-2 mb-4" style={{ lineHeight: "1" }}>{business.alias ? "(" + business.alias + ")" : ''}</h6>
                                <h6 className="text-center pl-2 pr-2 col-12"><strong><u>Bank Account Opening Balance</u></strong></h6>
                                <div className="row">
                                    <LocalForm className="small col-12" onSubmit={this.handleSubmit}>
                                        <div className="container mt-4 d-flex justify-content-center">
                                            <div class="row col-12 col-md-4">
                                                <div className="form-group mb-1 col-12">
                                                    <label htmlFor="bankid" className="mb-0 muted-text"><strong>Bank Name:<span className="text-danger">*</span></strong></label>
                                                    <Control.select model=".bankid" className="form-control form-control-sm" id="bankid" name="bankid" onChange={this.handleInputChange} value={this.state.bankid}>
                                                        <option value="">Select Bank Name</option>
                                                        {this.props.getBankDetail.data.map(b => <option value={b.bankdetailid}>{b.bankname}</option>)}
                                                    </Control.select>
                                                </div>
                                                {this.state.bankid !== '' ? <div className="form-group mb-1 col-12">
                                                    <label htmlFor="amount" className="mb-0 muted-text"><strong>Opening Balance Amount</strong><span className="text-danger">*</span></label>
                                                    <Control.text model=".amount" className="form-control form-control-sm" id="amount" name="amount" onChange={this.handleInputChange} value={this.state.amount} />
                                                </div> : ''}
                                                {this.state.bankid !== '' ? <div className="form-group mb-1 col-12">
                                                    <button type="submit" className="btn btn-success mt-1 btn-sm small" >Update</button>
                                                </div> : ''}
                                            </div>
                                        </div>
                                    </LocalForm>
                                </div>
                            </div>
                            <Footer />
                        </div>
                    </div>
                )
            }
        }
    }
}

export default BankOpeningBalance;