import React, { Component } from 'react';
import cookie from 'react-cookie';
import { Redirect } from 'react-router-dom';
import { LocalForm, Control } from 'react-redux-form';
import { Header } from './HeaderComponent';
import { Footer } from './Footer';

class AssetOpeningBalance extends Component {
    constructor(props) {
        super(props);
        this.state = ({
            assetaccount: '',
            amount: 0
        })
        window.onload = () => {
            this.props.refreshAssetAccountState();
            this.props.resetAssetOpeningBalance();
        }
    }
    handleSubmit = () => {
        if (this.state.assetaccount === '') alert("Asset Account Not Selected");
        else {
            if ((this.state.amount === '') || (this.state.amount < 0) || (isNaN(parseInt(this.state.amount)))) alert(" Amount Must Be Greater Than Equal To 0 ");
            else this.props.updateAssetOpeningBalance(this.state, this.props.businessid, this.props.fyid);
        }
    }
    componentDidMount() {
        if (this.props.updateAssetOpeningMessage.error !== '') {
            alert(this.props.updateAssetOpeningMessage.error);
            this.setState({
                ...this.props.updateAssetOpeningMessage.state
            })
            this.props.refreshUpdateAssetOpeningBalance();
        }
        if (this.props.updateAssetOpeningMessage.message === 'success') {
            alert("Data Updated Successfully");
            this.setState({
                ...this.props.updateAssetOpeningMessage.state
            })
            this.props.refreshUpdateAssetOpeningBalance();
        }
        if (this.props.getAssetOpeningBalance.message === 'initial') this.props.getAssetOpeningBalanceList(this.props.businessid, this.props.fyid);
        if (this.props.logoutMessage.error !== '') {
            alert("Error Logging Out. Please Try Again");
            this.props.resetLogoutMessage();
        }
        if (this.props.getAssetAccount.message === 'initial') this.props.getAssetAccountList(this.props.businessid);
        if (this.props.getAssetOpeningBalance.message === 'success') {
            if (this.props.getAssetOpeningBalance.data.length) {
                let asset = this.props.getAssetOpeningBalance.data.filter(d => d.id === this.state.assetaccount)[0];
                this.setState({
                    ...this.state,
                    amount: asset ? asset.amount : 0
                })
            }
        }
    }

    handleInputChange = (event) => {
        let target = event.target;
        let name = target.name;
        let value = target.value;
        if (name === 'assetaccount') {
            let asset = this.props.getAssetOpeningBalance.data.filter(d => d.assetaccount === value)[0];
            let amount = asset ? asset.amount : 0
            this.setState({
                assetaccount: value,
                amount: amount
            })

        }
        else {
            this.setState({
                [name]: value
            })
        }
    }
    render() {
        if (!cookie.load('userservice')) return <Redirect to="/" />;
        else {
            if ((this.props.updateAssetOpeningMessage.loading === true) || (this.props.getAssetOpeningBalance.loading === true) || (this.props.logoutMessage.loading === true) || (this.props.getAssetAccount.loading === true)) {
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div className="mt-3 d-flex justify-content-center">
                            <button className="btn btn-danger">Loading... </button>
                        </div>
                    </div>
                );
            }
            else if ((this.props.getAssetOpeningBalance.error !== '') || (this.props.getAssetAccount.error !== '')) {
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div className="mt-3 d-flex justify-content-center">
                            <button className="btn btn-danger">Error. Please Refresh and Try Again</button>
                        </div>
                    </div>
                );
            }
            else {
                let business = this.props.businessList.data.filter(b => b.businessid === this.props.businessid)[0];
                let fy = business.financial.filter(f => f.fyid === this.props.fyid)[0].financialyear;
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div style={{ minHeight: "93vh", display: "flex", flexDirection: "column" }}>
                            <div className="container" style={{ flex: "1" }}>
                                <h6 className="text-center pl-2 pr-2 pt-4" style={{ lineHeight: "1" }}><strong>{business.businessname} {fy}</strong></h6>
                                <h6 className="text-center pl-2 pr-2 mb-4" style={{ lineHeight: "1" }}>{business.alias ? "(" + business.alias + ")" : ''}</h6>
                                <h6 className="text-center pl-2 pr-2 col-12"><strong><u>Asset Account Opening Value</u></strong></h6>
                                <div className="row">
                                    <LocalForm className="small col-12" onSubmit={this.handleSubmit}>
                                        <div className="container mt-4 d-flex justify-content-center">
                                            <div class="row col-12 col-md-4">
                                                <div className="form-group mb-1 col-12">
                                                    <label htmlFor="assetaccount" className="mb-0 muted-text"><strong>Asset Account Name:<span className="text-danger">*</span></strong></label>
                                                    <Control.select model=".assetaccount" className="form-control form-control-sm" id="assetaccount" name="assetaccount" onChange={this.handleInputChange} value={this.state.assetaccount}>
                                                        <option value="">Select Asset Account</option>
                                                        {this.props.getAssetAccount.data.map(b => {
                                                            if ((!b.sold) || (b.sold === "1") && (new Date(b.solddate) >= new Date ("01 Apr " + fy.split("-")[0]))) return <option value={b.id}>{b.assetaccount}</option>
                                                        })}
                                                    </Control.select>
                                                </div>
                                                {this.state.assetaccount !== '' ? <div className="form-group mb-1 col-12">
                                                    <label htmlFor="amount" className="mb-0 muted-text"><strong>Opening Value Amount</strong><span className="text-danger">*</span></label>
                                                    <Control.text model=".amount" className="form-control form-control-sm" id="amount" name="amount" onChange={this.handleInputChange} value={this.state.amount} />
                                                </div> : ''}
                                                {this.state.assetaccount !== '' ? <div className="form-group mb-1 col-12">
                                                    <button type="submit" className="btn btn-success mt-1 btn-sm small" >Update</button>
                                                </div> : ''}
                                            </div>
                                        </div>
                                    </LocalForm>
                                </div>
                            </div>
                            <Footer />
                        </div>
                    </div>
                )
            }
        }
    }
}

export default AssetOpeningBalance;