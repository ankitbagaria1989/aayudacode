import React, { Component, useState } from 'react';
import cookie from 'react-cookie';
import { Redirect } from 'react-router-dom';
import { Button, Collapse } from 'reactstrap';
import { Form, Errors, Control } from 'react-redux-form';
import { Header } from './HeaderComponent';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTrash, faAngleDoubleDown } from '@fortawesome/free-solid-svg-icons';
import { Footer } from './Footer';

const required = (val) => val && val.length;
const maxLength = (len) => val => val ? val.length < len : true;

const ErrorComponent = function ({ children }) {
    return (
        <div style={{ lineHeight: "1" }}>{children}</div>
    )
}

const AddExpenseHead = function ({ handleSubmit, addExpenseHeadMessage, businessid }) {
    let [isOpen, setIsOpen] = useState(false);
    let toggle = () => setIsOpen((!isOpen));
    if (addExpenseHeadMessage.error !== '') {
        isOpen = true;
    }
    return (
        <div className="col-12">
            <Button className="btn-sm mt-3 col-12" style={{ backgroundColor: "#fff", border: "1px solid #28a745", color: "#28a745" }} onClick={toggle}><div className="d-flex"><span className="col-11 text-center">Add a New Expense/Income Head</span><span className="col-1 p-0 text-right"><FontAwesomeIcon size="sm" icon={faAngleDoubleDown} /></span></div></Button>
            <Collapse isOpen={isOpen}>
                <Form model="addExpenseHead" onSubmit={((values) => handleSubmit(values, businessid))}>
                    <div className="justify-content-center mt-4">
                        <div className="form-group col-12 mb-1">
                            <label htmlFor="expenseheadname" className="mb-0"><span className="muted-text"><strong>Expense/Income Head Name</strong><span className="text-danger">*</span></span></label>
                            <div className="">
                                <Control.text model=".expenseheadname" className="form-control form-control-sm pt-3 pb-3" id="expenseheadname" name="expenseheadname" placeholder="Expense Head Name" validators={{ required, maxLength: maxLength(100) }} />
                                <Errors className="text-danger" model=".expenseheadname" show="touched" messages={{
                                    required: "ExpenseHead name is  mandatory",
                                    maxLength: "length must be within 100 characters"
                                }}
                                    component={ErrorComponent}
                                />
                            </div>
                            <div className="text-danger small">Eg: Interest, Travel, Recreational etc..</div>
                        </div>
                        <div className="mt-3 col-12"><button type="submit" className="btn-success btn-sm pl-4 pr-4" >Add</button></div>
                    </div>
                </Form>
            </Collapse>
        </div>
    );
}
const ExpenseHeadCard = function ({ data, deleteEntry, businessid }) {
    let cancel = function (segment) {
        if (window.confirm("Delete Expense Head")) deleteEntry(data.expenseheadid, businessid, 'expensehead', segment);
    }
    return (
        <div className="col-12 justify-content-center">
            <div className="">
                <Button className="btn-sm btn mt-2 col-12 btn-success"><div className="d-flex"><span className="col-11">{data.expenseheadname}</span><span className="col-1"><FontAwesomeIcon icon={faTrash} onClick={() => cancel("expensehead")} size="lg" /></span></div></Button>
            </div>
        </div>
    );
}
const ExpenseHeadList = function ({ getExpenseHead, deleteEntry, businessid }) {
    if ((getExpenseHead.error === '')) {
        if (getExpenseHead.data.length > 0) return getExpenseHead.data.map(d => <ExpenseHeadCard data={d} deleteEntry={deleteEntry} businessid={businessid} />);
        else return <h6 className="col-12 text-center text-muted mt-2 pb-4">No Expense/Income Head Added Yet</h6>;
    }
    else return <div classname="row col-12 card text-center"></div>;
}
class ExpenseHead extends Component {
    componentDidMount() {
        window.onload = () => {
            this.props.refreshExpenseHead();
        }
        if (this.props.getExpenseHead.message === 'initial') this.props.getExpenseHeadList(this.props.businessid);
        if (this.props.addExpenseHeadMessage.error !== '') {
            alert("Error " + this.props.addExpenseHeadMessage.error);
            this.props.resetAddExpenseHeadMessage();
        }
        if (this.props.addExpenseHeadMessage.message === 'success') {
            alert("Expense Head Added Successfully");
            this.props.resetAddExpenseHeadMessageAndForm();
        }
        if (this.props.logoutMessage.error !== '') {
            alert("Error Logging Out. Please Try Again");
            this.props.resetLogoutMessage();
        }
        if (this.props.deleteEntryMessage.message === "success") {
            alert("Entry Deleted Successfully");
            this.props.resetDeleteEntry();
        }
        if (this.props.deleteEntryMessage.error !== '') {
            alert("Error Deleting Entry - Please Try Again");
            this.props.resetDeleteEntry();
        }
    }
    handleSubmit = (values, businessid) => {
        this.props.addExpenseHead(values.expenseheadname, businessid);
    }
    render() {
        if (!cookie.load('userservice')) return <Redirect to="/" />
        else {
            if ((this.props.getExpenseHead.loading === true) || (this.props.addExpenseHeadMessage.loading === true) || (this.props.logoutMessage.loading === true) || (this.props.deleteEntryMessage.loading === true)) {
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div className="mt-3 d-flex justify-content-center">
                            <button className="btn btn-danger">Loading... </button>
                        </div>
                    </div>
                );
            }
            else if (this.props.getExpenseHead.error !== '') {
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div className="mt-3 d-flex justify-content-center">
                            <button className="btn btn-danger">Error.. Please Refresh and Try Agin </button>
                        </div>
                    </div>
                );
            }
            else {
                let business = this.props.businessList.data.filter(b => b.businessid === this.props.businessid)[0];
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div style={{ minHeight: "93vh", display: "flex", flexDirection: "column" }}>
                            <div className="container d-flex justify-content-center pr-0 pl-0">
                                <div className="col-12 col-md-5">
                                    <div className="small pr-0 pl-0">
                                        <h6 className="text-center pl-2 pr-2 pt-4" style={{ lineHeight: "1" }}><strong>{business.businessname}</strong></h6>
                                        <h6 className="text-center pl-2 pr-2 mb-4" style={{ lineHeight: "1" }}>{business.alias ? "(" + business.alias + ")" : ''}</h6>
                                        <h6 className="text-center pl-2 pr-2 mb-2"><strong><u>Expense Head</u></strong></h6>
                                        <AddExpenseHead handleSubmit={this.handleSubmit} addExpenseHeadMessage={this.props.addExpenseHeadMessage} businessid={this.props.businessid} />
                                    </div>
                                </div>
                            </div>
                            <div className="container d-flex justify-content-center pr-0 pl-0 mt-4 pb-4" style={{ flex: "1" }}>
                                <div className="col-12 col-md-5">
                                    <div className="small pr-0 pl-0">
                                        <ExpenseHeadList getExpenseHead={this.props.getExpenseHead} deleteEntry={this.props.deleteEntry} businessid={this.props.businessid} />
                                    </div>
                                </div>
                            </div>
                            <Footer />
                        </div>
                    </div>
                );
            }
        }
    }
}

export default ExpenseHead;