import React, { Component } from 'react';
import cookie from 'react-cookie';
import { Redirect } from 'react-router-dom';
import { Header } from './HeaderComponent';
import { sortData } from '../redux/sortArrayofObjects';
import { Footer } from './Footer';
const PreviousYear = function ({ getIncomeTaxOpeningBalance, paymentDetail }) {
    let debit = 0, credit = 0, i = 2;
    let flag = false;
    let returndata = [], returnrow = [];
    let paymentData = paymentDetail.filter(d => d.previouscurrent === 'previous' && d.vendorid === 'paytds');
    let openingbalance = getIncomeTaxOpeningBalance.filter(d => d.payreceive === 'paytds');
    let t1 =
        <div><h6 className="text-center pl-2 pr-2"><u>Previous Financial Year</u></h6>
            <div className="d-flex small mt-4 p-1 d-md-none d-lg-none">
                <div className="col-5 col-md-5 p-1">
                    <div className="col-12 col-md-6 p-0 "></div>
                </div>
                <div className="col-3 col-md-6 p-0 pt-1 text-right"><strong>Debit</strong></div>
                <div className="col-3 col-md-1 p-0 pt-1 text-right"><strong>Credit</strong></div>
            </div>
            <div className="container small mt-3 d-none d-md-block p-0">
                <div className="d-flex">
                    <div className="col-1 p-0"><strong>Date</strong></div>
                    <div className="col-1 p-0"><strong>Voucher No</strong></div>
                    <div className="col-4 p-0"><strong>Particulars</strong></div>
                    <div className="col-2 text-right"><strong>Debit</strong></div>
                    <div className="col-3 text-right"><strong>Credit</strong></div>
                </div>
            </div></div>

    openingbalance.forEach(d => {
        flag = true;
        returndata.push(
            <div>
                <div className="d-flex small p-1 d-md-none d-lg-none" style={i % 2 === 0 ? { backgroundColor: "rgb(181, 253, 233)" } : {}}>
                    <div className="col-5 col-md-5 p-1">
                        <div className="col-12 col-md-6 p-0"><strong>Apr/Opening Bal</strong></div>
                        <div className="col-12 col-md-6 p-0">TDS Payable</div>
                    </div>
                    <div className="col-3 col-md-1 p-0"></div>
                    <div className="col-3 col-md-6 p-1 p-0">
                        <div className="col-12 col-md-2 p-0 text-right">{parseFloat(d.amount).toFixed(2)}</div>
                    </div>
                    <div className="col-1"></div>
                </div>
                <div className="container small d-none d-md-block p-1" style={i % 2 === 0 ? { backgroundColor: "rgb(181, 253, 233)" } : {}}>
                    <div className="d-flex">
                        <div className="col-2 p-0">Apr Opening Bal</div>
                        <div className="col-4 p-0"><strong>TDS Payable</strong></div>
                        <div className="col-2"></div>
                        <div className="col-3 text-right">{parseFloat(d.amount).toFixed(2)}</div>
                    </div>
                </div>
            </div>
        )
        i++;
        credit = credit + parseFloat(d.amount);
    })
    paymentData.forEach(d => {
        flag = true;
        returndata.push(
            <div>
                <div className="d-flex small p-1 d-md-none d-lg-none" style={i % 2 === 0 ? { backgroundColor: "rgb(181, 253, 233)" } : {}}>
                    <div className="col-5 col-md-5 p-1">
                        <div className="col-12 col-md-6 p-0"><strong>{d.voucherno / d.date}</strong></div>
                        <div className="col-12 col-md-6 p-0">
                            <div>To {d.bankdetail.split("...")[1]}</div>
                            <div>TDS Paid</div>
                        </div>
                    </div>
                    <div className="col-3 col-md-1 p-0 text-right">{parseFloat(d.amount).toFixed(2)}</div>
                    <div className="col-3 col-md-6 p-1 p-0">
                        <div className="col-12 col-md-2 p-0"></div>
                    </div>
                    <div className="col-1"></div>
                </div>
                <div className="container small d-none d-md-block p-1" style={i % 2 === 0 ? { backgroundColor: "rgb(181, 253, 233)" } : {}}>
                    <div className="d-flex">
                        <div className="col-1 p-0"><strong>{d.date}</strong></div>
                        <div className="col-1 p-0"><strong>{d.voucherno}</strong></div>
                        <div className="col-4 p-0">
                            <div><strong>To {d.bankdetail.split("...")[1]}</strong></div>
                            <div>TDS Paid</div>
                        </div>
                        <div className="col-2 text-right">{parseFloat(d.amount).toFixed(2)}</div>
                        <div className="col-3"></div>
                    </div>
                </div>
            </div>
        )
        i++;
        debit = debit + parseFloat(d.amount);
    })
    if (flag === true) {
        returnrow.push(t1);
        returnrow.push(returndata);
        returnrow.push(
            <div>
                <div className="d-flex small p-1 d-md-none d-lg-none border-top">
                    <div className="col-6 col-md-5 p-1">
                        <div className="col-12 col-md-6 p-0"></div>
                        <div className="col-12 col-md-6 p-0"><strong></strong></div>
                    </div>
                    <div className="col-3 col-md-1 border-top border-dark">
                        <div className="col-12 col-md-2 p-0 pt-1 text-right">{parseFloat(debit).toFixed(2)}</div>
                    </div>
                    <div className="col-3 col-md-6 p-0 border-top border-dark">
                        <div className="col-12 col-md-2 p-0 pt-1 text-right">{parseFloat(credit).toFixed(2)}</div>
                    </div>
                </div>
                <div className="d-flex small mb-3 p-1 d-md-none d-lg-none">
                    <div className="col-6 col-md-5 p-1">
                        <div className="col-12 col-md-6 p-0"></div>
                        <div className="col-12 col-md-6 p-0 "><strong>Payable</strong></div>
                    </div>
                    <div className="col-3 col-md-1 border-top border-dark text-right">{parseFloat(credit - debit).toFixed(2)}</div>
                    <div className="col-3 col-md-6 p-0">
                        <div className="col-12 col-md-2 p-0 pt-1 "></div>
                    </div>
                </div>
                <div className="container d-none d-md-block small mb-1 p-1">
                    <div className="d-flex">
                        <div className="col-2 p-0"><strong></strong></div>
                        <div className="col-4 p-0"></div>
                        <div className="col-2 border-top border-dark text-right">{parseFloat(debit).toFixed(2)}</div>
                        <div className="col-3 border-top border-dark text-right">{parseFloat(credit).toFixed(2)}</div>
                        <div className="col-1"></div>
                    </div>
                </div>
                <div className="container d-none d-md-block small mb-1 p-1">
                    <div className="d-flex">
                        <div className="col-2 p-0"></div>
                        <div className="col-4 p-0"><strong>Payable</strong></div>
                        <div className="col-2 border-top border-dark text-right">{parseFloat(credit - debit).toFixed(2)}</div>
                        <div className="col-3 "><strong></strong></div>
                        <div className="col-1"></div>
                    </div>
                </div>
            </div>
        )
    }
    return returnrow;
}

const DataDisplayPayable = function ({ expense, payment, getIncomeTaxOpeningBalance }) {
    let data = expense;
    data = data.concat(payment.filter(d => d.vendorid === 'paytds'));
    data = data.sort(sortData);
    let datarow = [];
    let openingbalance = getIncomeTaxOpeningBalance.data.filter(d => d.previouscurrent === 'current' && d.payreceive === 'paytds')[0];
    let i = 2, totaldebit = 0, totalcredit = 0;
    datarow.push(<h6 className="text-center pl-2 pr-2 mt-2"><u>Current Financial Year</u></h6>)
    datarow.push(<div className="d-flex small mt-4 p-1 d-md-none d-lg-none">
        <div className="col-6 col-md-5 p-1">
            <div className="col-12 col-md-6 p-0 "></div>
        </div>
        <div className="col-3 col-md-6 p-0 pt-1 text-right"><strong>Debit</strong></div>
        <div className="col-3 col-md-1 p-0 pt-1 text-right"><strong>Credit</strong></div>
    </div>);
    datarow.push(
        <div className="container small mt-3 d-none d-md-block p-0">
            <div className="d-flex">
                <div className="col-1 p-0"><strong>Date</strong></div>
                <div className="col-1 p-0"><strong>Voucher No</strong></div>
                <div className="col-4 p-0"><strong>Particulars</strong></div>
                <div className="col-2 text-right"><strong>Debit</strong></div>
                <div className="col-3 text-right"><strong>Credit</strong></div>
            </div>
        </div>
    );

    let x = 0;
    while (x < data.length) {
        if (data[x].type === 'payment') {
            datarow.push(<div><div className="d-flex small mb-1 p-1 d-md-none d-lg-none" style={i % 2 === 0 ? { backgroundColor: "rgb(181, 253, 233)" } : {}}>
                <div className="col-6 col-md-5 p-1">
                    <div className="col-12 col-md-6 p-0"><strong>{data[x].voucherno + "/" + data[x].date}</strong></div>
                    <div className="col-12 col-md-6 p-0" style={{ lineHeight: "1" }}>To {data[x].bankdetail.split("...")[1]} {data[x].transactionno}</div>
                    <div className="col-12 col-md-6 p-0" style={{ lineHeight: "1" }}>TDS PAID</div>
                    <div className="col-12 col-md-6 p-0 " style={{ lineHeight: "1" }}>{data[x].remark}</div>
                </div>
                <div className="col-3 col-md-6 p-0 text-right">{parseFloat(data[x].amount).toFixed(2)}</div>
                <div className="col-3 col-md-1"></div>
            </div>
                <div className="container d-none d-md-block small p-1" style={i % 2 === 0 ? { backgroundColor: "rgb(181, 253, 233)" } : {}}>
                    <div className="d-flex">
                        <div className="col-1 p-0">{data[x].date}</div>
                        <div className="col-1 p-0">{data[x].voucherno}</div>
                        <div className="col-4 p-0">
                            <div className="col-12 p-0" style={{ lineHeight: "1" }}><strong>To {data[x].bankdetail.split("...")[1]}</strong></div>
                            <div className="col-12 p-0" style={{ lineHeight: "1" }}>{data[x].transactionno}</div>
                            <div className="col-12 p-0" style={{ lineHeight: "1" }}>TDS PAID</div>
                            <div className="col-12 p-0" style={{ lineHeight: "1" }}>{data[x].remark}</div>
                        </div>
                        <div className="col-2 text-right">{parseFloat(data[x].amount).toFixed(2)}</div>
                        <div className="col-3"></div>
                        <div className="col-1"></div>
                    </div>
                </div></div>);
            totaldebit = totaldebit + parseFloat(data[x].amount);
        }
        else if (data[x].type === 'Expense') {
            datarow.push(
                <div>
                    <div className=" d-flex small mb-1 p-1 d-md-none d-lg-none" style={i % 2 === 0 ? { backgroundColor: "rgb(181, 253, 233)" } : {}}>
                        <div className="col-6 col-md-5 p-1">
                            <div className="col-12 col-md-6 p-0"><strong>{data[x].voucherno}/{data[x].date}</strong></div>
                            <div className="col-12 col-md-6 p-0 " style={{ lineHeight: "1" }}>By {data[x].category + " " + data[x].type}</div>
                            <div className="col-12 col-md-6 p-0 " style={{ lineHeight: "1" }}>({data[x].businessname})</div>
                            <div className="col-12 col-md-6 p-0 " style={{ lineHeight: "1" }}>{data[x].description}</div>
                            <div className="col-12 col-md-6 p-0 " style={{ lineHeight: "1" }}>{data[x].transactionno}</div>
                            <div className="col-12 col-md-6 p-0 " style={{ lineHeight: "1" }}>{data[x].remark}</div>
                        </div>
                        <div className="col-3 col-md-6 p-0 pt-1">
                            <div className="col-12 col-md-2 p-0"></div>
                        </div>
                        <div className="col-3 col-md-1 text-right">
                            {parseFloat(data[x].tdsamount).toFixed(2)}
                        </div>
                    </div>
                    <div className="container d-none d-md-block small mb-1 p-1" style={i % 2 === 0 ? { backgroundColor: "rgb(181, 253, 233)" } : {}}>
                        <div className="d-flex">
                            <div className="col-1 p-0">{data[x].date}</div>
                            <div className="col-1 p-0">{data[x].voucherno}</div>
                            <div className="col-4 p-0">
                                <div className="col-12 p-0" style={{ lineHeight: "1" }}><strong>By {data[x].category + " " + data[x].type}</strong></div>
                                <div className="col-12 p-0" style={{ lineHeight: "1" }}>({data[x].businessname})</div>
                                <div className="col-12 p-0" style={{ lineHeight: "1" }}>{data[x].description}</div>
                                <div className="col-12 p-0" style={{ lineHeight: "1" }}>{data[x].transactionno}</div>
                                <div className="col-12 p-0" style={{ lineHeight: "1" }}>{data[x].remark}</div>
                            </div>
                            <div className="col-2"></div>
                            <div className="col-3 text-right">{parseFloat(data[x].tdsamount).toFixed(2)}</div>
                            <div className="col-1"></div>
                        </div>
                    </div>
                </div>
            );
            totalcredit = totalcredit + parseFloat(data[x].tdsamount);
        }
        x++;
        i++;
    }

    datarow.push(<div>
        <div className="d-flex small p-1 d-md-none d-lg-none border-top">
            <div className="col-6 col-md-5 p-1">
                <div className="col-12 col-md-6 p-0"></div>
                <div className="col-12 col-md-6 p-0"><strong></strong></div>
            </div>
            <div className="col-3 col-md-1 border-top border-dark">
                <div className="col-12 col-md-2 p-0 pt-1 text-right">{parseFloat(totaldebit).toFixed(2)}</div>
            </div>
            <div className="col-3 col-md-6 p-0 border-top border-dark">
                <div className="col-12 col-md-2 p-0 pt-1 text-right">{parseFloat(totalcredit).toFixed(2)}</div>
            </div>
        </div>
        <div className="d-flex small mb-3 p-1 d-md-none d-lg-none">
            <div className="col-6 col-md-5 p-1">
                <div className="col-12 col-md-6 p-0"></div>
                <div className="col-12 col-md-6 p-0 "><strong>Payable</strong></div>
            </div>
            <div className="col-3 col-md-1 border-top border-dark text-right">{parseFloat(totalcredit - totaldebit).toFixed(2)}</div>
            <div className="col-3 col-md-6 p-0">
                <div className="col-12 col-md-2 p-0 pt-1 "></div>
            </div>
        </div>
        <div className="container d-none d-md-block small mb-1 p-1">
            <div className="d-flex">
                <div className="col-2 p-0"><strong></strong></div>
                <div className="col-4 p-0"></div>
                <div className="col-2 border-top border-dark text-right">{parseFloat(totaldebit).toFixed(2)}</div>
                <div className="col-3 border-top border-dark text-right">{parseFloat(totalcredit).toFixed(2)}</div>
                <div className="col-1"></div>
            </div>
        </div>
        <div className="container d-none d-md-block small mb-1 p-1">
            <div className="d-flex">
                <div className="col-2 p-0"></div>
                <div className="col-4 p-0"><strong>Payable</strong></div>
                <div className="col-2 border-top border-dark text-right">{parseFloat(totalcredit - totaldebit).toFixed(2)}</div>
                <div className="col-3 "><strong></strong></div>
                <div className="col-1"></div>
            </div>
        </div>
    </div>)
    return datarow;
}

class TDSStatement extends Component {
    constructor(props) {
        super(props);
        window.onload = () => {
            this.props.resetPaymentDetail();
            this.props.resetExpenseDetail();
            this.props.resetIncomeTaxOpeningBalance();
        }
    }
    componentDidMount() {
        if (this.props.getPayment.message === 'initial') this.props.getPaymentDetail(this.props.businessid, this.props.fyid);
        if (this.props.getExpense.message === 'initial') this.props.getExpenseDetail(this.props.businessid, this.props.fyid);
        if (this.props.getIncomeTaxOpeningBalance.message === 'initial') this.props.getIncomeTaxOpeningBalanceList(this.props.businessid, this.props.fyid);
        if (this.props.logoutMessage.error !== '') {
            alert("Error Logging Out. Please Try Again");
            this.props.resetLogoutMessage();
        }
    }
    render() {
        if (!cookie.load('userservice')) return <Redirect to="/" />;
        else {
            if ((this.props.logoutMessage.loading === true) || (this.props.getPayment.loading === true) || (this.props.getExpense.loading === true) || (this.props.getIncomeTaxOpeningBalance.loading === true)) {
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div className="mt-3 d-flex justify-content-center">
                            <button className="btn btn-danger">Loading... </button>
                        </div>
                    </div>
                );
            }
            else if ((this.props.getPayment.error !== '') || (this.props.getExpense.error !== '') || (this.props.getIncomeTaxOpeningBalance.error !== '')) {
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div className="mt-3 d-flex justify-content-center">
                            <button className="btn btn-danger">Error.. Please Refresh and Try Again </button>
                        </div>
                    </div>
                );
            }
            else {
                let business = this.props.businessList.data.filter(b => b.businessid === this.props.businessid)[0];
                let fy = business.financial.filter(f => f.fyid === this.props.fyid)[0].financialyear;
                let payment = this.props.getPayment.data.filter(d => ((d.vendorid === 'paytds')));
                let expense = this.props.getExpense.data.filter(d => d.tds === "1");
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div style={{ minHeight: "93vh", display: "flex", flexDirection: "column" }}>
                            <div className="container" style={{ flex: "1" }}>
                                <h6 className="text-center pl-2 pr-2 pt-4" style={{ lineHeight: "1" }}><strong>{business.businessname} {fy}</strong></h6>
                                <h6 className="text-center pl-2 pr-2 mb-4" style={{ lineHeight: "1" }}>{business.alias ? "(" + business.alias + ")" : ''}</h6>
                                <h6 className="text-center pl-2 pr-2"><strong><u>TDS Payable Book</u></strong></h6>
                                <PreviousYear getIncomeTaxOpeningBalance={this.props.getIncomeTaxOpeningBalance.data} paymentDetail={payment} />
                                <DataDisplayPayable expense={expense} payment={payment} getIncomeTaxOpeningBalance={this.props.getIncomeTaxOpeningBalance} />
                            </div>
                            <Footer />
                        </div>
                    </div>
                )
            }
        }
    }
}
export default TDSStatement;