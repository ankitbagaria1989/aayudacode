import React, { Component, useState } from 'react';
import cookie from 'react-cookie';
import { Redirect, Link } from 'react-router-dom';
import { Button, Collapse } from 'reactstrap';
import { Form, Errors, Control } from 'react-redux-form';
import { Header } from './HeaderComponent';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faAngleDoubleRight, faAngleDoubleDown } from '@fortawesome/free-solid-svg-icons';
import { Footer } from './Footer';

const required = (val) => val && val.length;
const maxLength = (len) => val => val ? val.length < len : true;
const isNumber = (val) => !isNaN(parseInt(val));

const ErrorComponent = function ({ children }) {
    return (
        <div style={{ lineHeight: "1" }}>{children}</div>
    )
}
const AddInvestmentAccount = function ({ handleSubmit, addInvestmentAccountMessage, businessid, business }) {
    let [isOpen, setIsOpen] = useState(false);
    let toggle = () => setIsOpen((!isOpen));
    if (addInvestmentAccountMessage.error !== '') {
        isOpen = true;
    }
    return (
        <div className="col-12">
            {business.businesstype === 'partnership' ? <Button className="btn-sm mt-3 col-12" style={{ backgroundColor: "#fff", border: "1px solid #28a745", color: "#28a745" }} onClick={toggle}><div className="d-flex"><span className="col-11 text-center">Add a New Partner Account</span><span className="col-1 p-0 text-right"><FontAwesomeIcon size="sm" icon={faAngleDoubleDown} /></span></div></Button> : ''}
            {business.businesstype === 'ltd' ? <Button className="btn-sm mt-3 col-12" style={{ backgroundColor: "#fff", border: "1px solid #28a745", color: "#28a745" }} onClick={toggle}><div className="d-flex"><span className="col-11 text-center">Add a New Share Holder Account</span><span className="col-1 p-0 text-right"><FontAwesomeIcon size="sm" icon={faAngleDoubleDown} /></span></div></Button> : ''}
            <Collapse isOpen={isOpen}>
                <Form model="addInvestmentDetail" onSubmit={((values) => handleSubmit(values, businessid))}>
                    <div className="justify-content-center mt-4">
                        <div className="form-group col-12 mb-1">
                            {business.businesstype === 'partnership' ? <label htmlFor="investmentaccount" className="mb-0"><span className="muted-text">Partner Account Name<span className="text-danger">*</span></span></label> : ''}
                            {business.businesstype === 'ltd' ? <label htmlFor="investmentaccount" className="mb-0"><span className="muted-text">Share Holder Account Name<span className="text-danger">*</span></span></label> : ''}
                            <div className="">
                                <Control.text model=".investmentaccount" className="form-control form-control-sm pt-3 pb-3" id="investmentaccount" name="investmentaccount" validators={{ required, maxLength: maxLength(100) }} />
                                <Errors className="text-danger" model=".investmentaccount" show="touched" messages={{
                                    required: "Account Name is Mandatory",
                                    maxLength: "Length Must Be within 100 characters"
                                }}
                                    component={ErrorComponent}
                                />
                            </div>
                            {business.businesstype === 'partnership' ?
                                <div>
                                    <label htmlFor="shareholding" className="mb-0"><span className="muted-text">Partner's Percentage Holding<span className="text-danger">*</span></span></label>
                                    <div className="">
                                        <Control.text model=".shareholding" className="form-control form-control-sm pt-3 pb-3" id="shareholding" name="shareholding" validators={{ required, isNumber }} />
                                        <Errors className="text-danger" model=".shareholding" show="touched" messages={{
                                            required: "Holding Data is Mandatory",
                                            isNumber: "Must Be In Numbers"
                                        }}
                                            component={ErrorComponent}
                                        />
                                    </div></div> : ''}
                            {business.businesstype === 'ltd' ?
                                <div>
                                    <label htmlFor="shareholding" className="mb-0"><span className="muted-text">No of Shares Alloted<span className="text-danger">*</span></span></label>
                                    <div className="">
                                        <Control.text model=".shareholding" className="form-control form-control-sm pt-3 pb-3" id="shareholding" name="shareholding" validators={{ required, isNumber }} />
                                        <Errors className="text-danger" model=".shareholding" show="touched" messages={{
                                            required: "No Of Shares Data is Mandatory",
                                            isNumber: "Must Be In Numbers"
                                        }}
                                            component={ErrorComponent}
                                        />
                                    </div></div> : ''}
                            <div className="mt-3"><button type="submit" className="btn btn-success btn-sm pl-4 pr-4" >Add</button></div>
                        </div>
                    </div>
                </Form>
            </Collapse>
        </div>
    );
}
const InvestmentAccountDetailCard = function ({ investmentAccountDetail, business }) {
    return (
        <div className="col-12 justify-content-center">
            <div className="">
                <Link to={`/business/businessdetail/investmentaccountchange/${business.businessid}/${investmentAccountDetail.id}`}><Button className="btn-sm btn mt-2 col-12 btn-success"><div className="d-flex"><span className="col-11">{investmentAccountDetail.investmentaccount} - {investmentAccountDetail.shareholding} {business.businesstype === 'ltd' ? "Shares" : ''} {business.businesstype === 'partnership' ? "%" : ''} </span><span className="col-1 pr-0 text-align-right"><FontAwesomeIcon size="sm" icon={faAngleDoubleRight} /></span></div></Button></Link>
            </div>
            </div>
    );
}
const InvestmentAccountDetailList = function ({ getInvestmentAccount, business}) {
    if ((getInvestmentAccount.error === '')) {
        if (getInvestmentAccount.data.length > 0) return getInvestmentAccount.data.map((d) => <InvestmentAccountDetailCard investmentAccountDetail={d} business={business} />);
        else return <h6 className="col-12 text-center text-muted mt-2 pb-4">{business.businesstype === 'partnership' ? "No Partners Account Created Yet" : ''}{business.businesstype === 'ltd' ? "No Share Holder Account Created Yet" : ''}</h6>;
    }
    else return <div classname="row col-12 card text-center"></div>;
}
class InvestmentAccount extends Component {
                componentDidMount() {
                window.onload = () => {
                    this.props.refreshInvestmentAccountState();
                }
        if (this.props.getInvestmentAccount.message === 'initial') this.props.getInvestmentAccountList(this.props.businessid);
        if (this.props.addInvestmentAccountMessage.error !== '') {
                alert("Error " + this.props.addInvestmentAccountMessage.error);
            this.props.resetAddInvestmentAccountMessage();
        }
        if (this.props.addInvestmentAccountMessage.message === 'success') {
                alert("Account Added Successfully");
            this.props.resetAddInvestmentAccountMessageAndForm();
        }
        if (this.props.logoutMessage.error !== '') {
                alert("Error Logging Out. Please Try Again");
            this.props.resetLogoutMessage();
        }
    }
    handleSubmit = (values, businessid) => {
                this.props.addInvestmentAccount(values, businessid);
    }
    render() {
        if (!cookie.load('userservice')) return <Redirect to="/" />
        else {
            if ((this.props.getInvestmentAccount.loading === true) || (this.props.addInvestmentAccountMessage.loading === true) || (this.props.logoutMessage.loading === true)) {
                return (
            <div>
                <Header logout={this.props.logout} />
                <div className="mt-3 d-flex justify-content-center">
                    <button className="btn btn-danger">Loading... </button>
                </div>
            </div>
                );
            }
            else if (this.props.getInvestmentAccount.error !== '') {
                return (
            <div>
                <Header logout={this.props.logout} />
                <div className="mt-3 d-flex justify-content-center">
                    <button className="btn btn-danger">Error.. Please Refresh and Try Agin </button>
                </div>
            </div>
                );
            }
            else {
                let business = this.props.businessList.data.filter(b => b.businessid === this.props.businessid)[0];
                return (
            <div>
                <Header logout={this.props.logout} />
                <div style={{ minHeight: "93vh", display: "flex", flexDirection: "column" }}>
                    <div className="container d-flex justify-content-center pr-0 pl-0">
                        <div className="col-12 col-md-5">
                            <div className="small pr-0 pl-0">
                                <h6 className="text-center pl-2 pr-2 pt-4" style={{ lineHeight: "1" }}><strong>{business.businessname}</strong></h6>
                                <h6 className="text-center pl-2 pr-2 mb-4" style={{ lineHeight: "1" }}>{business.alias ? "(" + business.alias + ")" : ''}</h6>
                                <h6 className="text-center pl-2 pr-2 mb-2"><strong><u>{business.businesstype === 'ltd' ? "Share Holders Account" : ''}</u></strong></h6>
                                <h6 className="text-center pl-2 pr-2 mb-2"><strong><u>{business.businesstype === 'partnership' ? "Partners Account" : ''}</u></strong></h6>
                                <AddInvestmentAccount handleSubmit={this.handleSubmit} addInvestmentAccountMessage={this.props.addInvestmentAccountMessage} businessid={this.props.businessid} business={business} />
                            </div>
                        </div>
                    </div>
                    <div className="container d-flex justify-content-center pr-0 pl-0 mt-4 pb-4" style={{ flex: "1" }}>
                        <div className="col-12 col-md-5">
                            <div className="small pr-0 pl-0">
                                <InvestmentAccountDetailList getInvestmentAccount={this.props.getInvestmentAccount} business={business} />
                            </div>
                        </div>
                    </div>
                    <Footer />
                </div>
            </div>
                );
            }
        }
    }
}
export default InvestmentAccount;