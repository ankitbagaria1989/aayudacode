import React, { Component } from 'react';
import cookie from 'react-cookie';
import { Redirect } from 'react-router-dom';
import { Header } from './HeaderComponent';

const DataDisplay = function ({ getCashInHand, getBankEntry, getBankDetail }) {
    let returnrows = [];
    let balance = getCashInHand.amount ? getCashInHand.amount : 0;
    returnrows.push(
        <div>
            <h4 className="mt-4 mb-4">Cash In Hand Statement</h4>
            <div className="row small mb-3">
                <div className="col-2 p-0"><strong>Date</strong></div>
                <div className="col-3 p-0"><strong>Bank</strong></div>
                <div className="col-2 p-0"><strong>Withdrawn</strong></div>
                <div className="col-2 p-0"><strong>Deposit</strong></div>
                <div className="col-2 p-0"><strong>Total</strong></div>
            </div>
        </div>)
    returnrows.push(<div>
        <div className="row small">
            <div className="col-2 p-0"><strong>1st April</strong></div>
            <div className="col-7 p-0"><strong>Opening</strong></div>
            <div className="col-2 p-0"><strong>{balance}</strong></div>
        </div>
    </div>)
    getBankEntry.data.forEach(b => {
        if (b.paymentmode === 'cash') {
            let bankname = undefined;
            getBankDetail.data.forEach(ba => {
                if (ba.bankid === b.bankid) bankname = ba.bankname;
            })
            if (b.paymenttype === 'deposit') {
                balance = balance - b.amount;
                returnrows.push(<div>
                    <div className="row small">
                        <div className="col-2 p-0"><strong>{b.date}</strong></div>
                        <div className="col-3 p-0"><strong>{bankname}</strong></div>
                        <div className="col-2 p-0"><strong></strong></div>
                        <div className="col-2 p-0"><strong>{b.amount}</strong></div>
                        <div className="col-2 p-0"><strong>{balance}</strong></div>
                    </div>
                </div>)
            }
            else if (b.paymenttype === 'withdrawl') {
                balance = balance + b.amount;
                returnrows.push(<div>
                    <div className="row small">
                        <div className="col-2 p-0"><strong>{b.date}</strong></div>
                        <div className="col-3 p-0"><strong>{bankname}</strong></div>
                        <div className="col-2 p-0"><strong>{b.amount}</strong></div>
                        <div className="col-2 p-0"><strong></strong></div>
                        <div className="col-2 p-0"><strong>{balance}</strong></div>
                    </div>
                </div>)
            }
        }
    })

    return returnrows;
}

class CashInHandStatement extends Component {
    constructor(props) {
        super(props);
        window.onload = () => {
            this.props.resetCashInHandDetail();
            this.props.refreshBankState();
        }
    }
    componentDidMount() {
        if ((this.props.getCashInHand.message === 'initial') && (this.props.getCashInHand.loading === false)) this.props.getCashInHandDetail(this.props.businessid, this.props.fyid);
        if ((this.props.getBankDetail.message === 'initial') && (this.props.getBankDetail.loading === false)) this.props.getBankDetailList(this.props.businessid);
        if (this.props.logoutMessage.error !== '') {
            alert("Error Logging Out. Please Try Again");
            this.props.resetLogoutMessage();
        }
    }

    render() {
        if (!cookie.load('userservice')) return <Redirect to="/" />;
        else {
            if ((this.props.getCashInHand.loading === true) || (this.props.getBankDetail.loading === true) || (this.props.logoutMessage.loading === true)) {
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div className="mt-3 d-flex justify-content-center">
                            <button className="btn btn-danger">Loading... </button>
                        </div>
                    </div>
                );
            }
            else if ((this.props.getCashInHand.error !== '') || (this.props.getBankDetail.error !== '')) {
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div className="mt-3 d-flex justify-content-center">
                            <button className="btn btn-danger">Error.. Please Refresh and Try Again </button>
                        </div>
                    </div>
                );
            }
            else {
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div className="container">
                            <div className="row">
                                <div className="col-md-6">
                                    <DataDisplay getCashInHand={this.props.getCashInHand} getBankEntry={this.props.getBankEntry} getBankDetail={this.props.getBankDetail} />
                                </div>
                            </div>
                        </div>
                    </div>
                )
            }
        }
    }
}

export default CashInHandStatement;