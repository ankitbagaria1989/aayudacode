import React, { Component, useState } from 'react';
import cookie from 'react-cookie';
import { Redirect } from 'react-router-dom';
import { Button, Collapse } from 'reactstrap';
import { LocalForm, Errors, Control } from 'react-redux-form';
import { Header } from './HeaderComponent';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faAngleDoubleDown } from '@fortawesome/free-solid-svg-icons';
import { Footer } from './Footer';
const required = (val) => val && val.length;
const maxLength = (len) => val => val ? val.length < len : true;
const ErrorComponent = function ({ children }) {
    return (
        <div style={{ lineHeight: "1" }}>{children}</div>
    )
}
const AddAssetAccount = function ({ handleSubmit, addAssetAccountMessage, businessid, state, handleInputChange }) {
    let [isOpen, setIsOpen] = useState(false);
    let toggle = () => setIsOpen((!isOpen));
    if (addAssetAccountMessage.error !== '') {
        isOpen = true;
    }
    return (
        <div className="col-12">
            <Button className="btn-sm mt-3 col-12" style={{ backgroundColor: "#fff", border: "1px solid #28a745", color: "#28a745" }} onClick={toggle}><div className="d-flex"><span className="col-11 text-center">Add a New Asset Account</span><span className="col-1 p-0 text-right"><FontAwesomeIcon size="sm" icon={faAngleDoubleDown} /></span></div></Button>
            <Collapse isOpen={isOpen}>
                <LocalForm onSubmit={((values) => handleSubmit(values, businessid))}>
                    <div className="justify-content-center mt-4">
                        <div className="form-group col-12 mb-1">
                            <label htmlFor="assetaccount" className="mb-0"><span className="muted-text">Asset Account Name<span className="text-danger">*</span></span></label>
                            <div className="">
                                <Control.text model=".assetaccount" className="form-control form-control-sm pt-3 pb-3" id="assetaccount" name="assetaccount" placeholder="Asset Account Name" validators={{ required, maxLength: maxLength(100) }} value={state.assetaccount} onChange={handleInputChange} />
                                <Errors className="text-danger" model=".assetaccount" show="touched" messages={{
                                    required: "Asset Account Name is Mandatory",
                                    maxLength: "length must be within 100 characters"
                                }}
                                    component={ErrorComponent}
                                />
                            </div>
                            <div className="col-12 mb-1 mt-2">
                                <input name="depreciating" type="checkbox" checked={state.depreciating} onChange={handleInputChange} />&nbsp;&nbsp;
                                <span htmlFor="depreciating" className="mb-0 muted-text col-8 p-0"><strong>Depreciating Asset ? </strong></span>
                            </div>
                            <div className="col-12 mb-1">
                                <input name="purchasedbefore" type="checkbox" checked={state.purchasedbefore} onChange={handleInputChange} />&nbsp;&nbsp;
                                <span htmlFor="purchasedbefore" className="mb-0 muted-text col-8 p-0"><strong>Purchased Before Current FY ? </strong></span>
                            </div>
                            {state.depreciating === true && state.purchasedbefore === true ? <div><label htmlFor="assetaccount" className="mb-0"><span className="muted-text">Purchase Date<span className="text-danger">*</span></span></label>
                                <div className="col-12 mb-1 p-0">
                                    <Control.text model=".date" className="form-control form-control-sm pt-3 pb-3" id="date" name="date" validators={{ required }} value={state.date} onChange={handleInputChange} />
                                </div><label htmlFor="amount" className="mb-0"><span className="muted-text">Purchase Amount<span className="text-danger">*</span></span></label>
                                <div className="col-12 mb-1 p-0">
                                    <Control.text model=".amount" className="form-control form-control-sm pt-3 pb-3" id="amount" name="amount" validators={{ required }} value={state.amount} onChange={handleInputChange} />
                                </div></div> : ''}
                            <div className="mt-3"><button type="submit" className="btn btn-success btn-sm pl-4 pr-4" >Add</button></div>
                        </div>
                    </div>
                </LocalForm>
            </Collapse>
        </div>
    );
}
const AssetAccountDetailCard = function ({ assetAccountDetail }) {
    return (
        <div className="col-12 justify-content-center">
            <div className="">
                <Button className="btn-sm btn mt-2 col-12 btn-success"><div className="d-flex"><span className="col-11">{assetAccountDetail.assetaccount}</span><span className="col-1 p-0 text-right"></span></div></Button>
            </div>
        </div>
    );
}
const AssetAccountDetailList = function ({ getAssetAccount }) {
    if ((getAssetAccount.error === '')) {
        if (getAssetAccount.data.length > 0) return getAssetAccount.data.map((d) => <AssetAccountDetailCard assetAccountDetail={d} />);
        else return <h6 className="col-12 text-center text-muted mt-2 pb-4">No Asset Account Added Yet</h6>;
    }
    else return <div classname="row col-12 card text-center"></div>;
}
class AssetAccount extends Component {
    constructor(props) {
        super(props);
        this.state = ({
            assetaccount: '',
            depreciating: true,
            purchasedbefore: false,
            date: new Date().toDateString().split(' ').slice(1).join(' '),
            amount: ''
        })
    }
    componentDidMount() {
        window.onload = () => {
            this.props.refreshAssetAccountState();
        }
        if (this.props.getAssetAccount.message === 'initial') this.props.getAssetAccountList(this.props.businessid);
        if (this.props.addAssetAccountMessage.error !== '') {
            alert(this.props.addAssetAccountMessage.error);
            this.setState({
                ...this.props.addAssetAccountMessage.state
            })
            this.props.resetAddAssetAccountMessage();
        }
        if (this.props.addAssetAccountMessage.message === 'success') {
            alert("Asset Account Added Successfully");
            this.props.resetAddAssetAccountMessageAndForm();
        }
        if (this.props.logoutMessage.error !== '') {
            alert("Error Logging Out. Please Try Again");
            this.props.resetLogoutMessage();
        }
    }
    handleInputChange = (event) => {
        let target = event.target;
        let name = target.name;
        let value = target.value;
        if (name === 'depreciating') {
            this.setState({
                ...this.state,
                depreciating: !this.state.depreciating
            })
        }
        else if (name === 'purchasedbefore') {
            this.setState({
                ...this.state,
                purchasedbefore: !this.state.purchasedbefore
            })
        }
        else {
            this.setState({
                ...this.state,
                [name]: value
            })
        }
    }
    handleSubmit = (values, businessid) => {
        let present = false;
        this.props.getAssetAccount.data.forEach(d => d.assetaccount === values.assetaccount ? present = true : '');
        if (present === true) alert("Asset " + values.assetaccount + " Already Registered");
        else if (this.state.assetaccount.length > 100) alert ("Asset Account Name Must Be Within 100 characters");
        else if ((this.state.depreciating === true) && (this.state.purchasedbefore === true) && (this.state.date === '')) alert ("Date Is Mandatory");
        else if ((this.state.depreciating === true) && (this.state.purchasedbefore === true) && (this.state.amount === '')) alert ("Amount Is Mandatory");
        else this.props.addAssetAccount(this.state, businessid);
        //if (this.props.addFinancialYear.message === "success") window.location.reload();
    }
    render() {
        if (!cookie.load('userservice')) return <Redirect to="/" />
        else {
            if ((this.props.getAssetAccount.loading === true) || (this.props.addAssetAccountMessage.loading === true) || (this.props.logoutMessage.loading === true)) {
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div className="mt-3 d-flex justify-content-center">
                            <button className="btn btn-danger">Loading... </button>
                        </div>
                    </div>
                );
            }
            else if (this.props.getAssetAccount.error !== '') {
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div className="mt-3 d-flex justify-content-center">
                            <button className="btn btn-danger">Error.. Please Refresh and Try Agin </button>
                        </div>
                    </div>
                );
            }
            else {
                let business = this.props.businessList.data.filter(b => b.businessid === this.props.businessid)[0];
                return (
                    <div>
                        <Header logout={this.props.logout} />
                        <div style={{ minHeight: "93vh", display: "flex", flexDirection: "column" }}>
                            <div className="container d-flex justify-content-center pr-0 pl-0">
                                <div className="col-12 col-md-5">
                                    <div className="small pr-0 pl-0">
                                        <h6 className="text-center pl-2 pr-2 pt-4" style={{ lineHeight: "1" }}><strong>{business.businessname}</strong></h6>
                                        <h6 className="text-center pl-2 pr-2 mb-4" style={{ lineHeight: "1" }}>{business.alias ? "(" + business.alias + ")" : ''}</h6>
                                        <h6 className="text-center pl-2 pr-2 mb-2"><strong><u>Asset Account</u></strong></h6>
                                        <AddAssetAccount handleSubmit={this.handleSubmit} addAssetAccountMessage={this.props.addAssetAccountMessage} businessid={this.props.businessid} state={this.state} handleInputChange={this.handleInputChange} />
                                    </div>
                                </div>
                            </div>
                            <div className="container d-flex justify-content-center pr-0 pl-0 mt-4 pb-4" style={{ flex: "1" }}>
                                <div className="col-12 col-md-5">
                                    <div className="small pr-0 pl-0">
                                        <AssetAccountDetailList getAssetAccount={this.props.getAssetAccount} />
                                    </div>
                                </div>
                            </div>
                            <Footer />
                        </div>
                    </div>
                );
            }
        }
    }
}

export default AssetAccount;