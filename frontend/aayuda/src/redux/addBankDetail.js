import * as ACTION_TYPES from './actionTypes';

const initialState = {
    loading: false,
    disabled: false,
    message: 'initial',
    error: ''
}

export const addBankDetailMessage = function (state = initialState, action) {
    switch (action.type) {
        case ACTION_TYPES.ADD_BANK_DETAIL_LOADING:
            return action.payload;
        case ACTION_TYPES.ADD_BANK_DETAIL:
            return action.payload;
        case ACTION_TYPES.RESET_BANK_DETAIL_MESSAGE:
            return ({
                loading: false,
                disabled: false,
                message: 'initial',
                error: ''
            })
        default: return state;
    }
}