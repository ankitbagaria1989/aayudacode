import * as ACTION_TYPES from './actionTypes';

const initialState = {
    loading : false,
    message : "initial",
    disabled : false,
    error : '',
    state: {}
}

export const updateAssetOpeningMessage = function (state = initialState, action) {
    switch(action.type) {
        case ACTION_TYPES.UPDATE_ASSET_OPENING_LOADING :
            return action.payload;
        case ACTION_TYPES.UPDATE_ASSET_OPENING :
            return action.payload;      
        default : return state;        
    }
}