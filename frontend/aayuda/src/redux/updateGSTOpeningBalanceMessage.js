import * as ACTION_TYPES from './actionTypes';

const initialState = {
    loading : false,
    error : '',
    message : 'initial',
    state: {}
}

export const updateGSTOpeningBalanceMessage = function (state = initialState, action) {
    switch(action.type) {
        case ACTION_TYPES.UPDATE_GST_OPENING_BALANCE_LOADING:
            return action.payload;
        case ACTION_TYPES.UPDATE_GST_OPENING_BALANCE:
            return action.payload;
        case ACTION_TYPES.REFRESH_GST_OPENING_BALANCE:
            return action.payload;        
        default: return state;    
    }
}