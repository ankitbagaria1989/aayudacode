import * as ACTION_TYPES from './actionTypes';

const initialState = {
    loading : false,
    message : '',
    error : '',
    state : {}
}

export const addLoanMessage = function (state = initialState, action) {
    switch (action.type) {
        case ACTION_TYPES.ADD_LOAN_LOADING:
           return action.payload;
        case ACTION_TYPES.ADD_LOAN:
            return action.payload;
        default: return state;
    }
}