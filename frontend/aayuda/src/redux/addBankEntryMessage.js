import * as ACTION_TYPES from './actionTypes';

const initialState = {
    loading : false,
    message : '',
    error : '',
    state : []
}

export const addBankEntryMessage = function (state = initialState, action) {
    let tempState = {};
    switch (action.type) {
        case ACTION_TYPES.ADD_BANK_ENTRY:
           tempState = {
               loading : false,
               message : action.payload.message,
               error : action.payload.error,
               state: action.payload.state 
           }
           return tempState;
        case ACTION_TYPES.LOADING_ADD_BANK_ENTRY:
            return action.payload;
        case ACTION_TYPES.RESET_ADD_BANK_ENTRY_MESSAGE:
            return ({
                loading : false,
                message : '',
                error : '',
                state : state.state !== [] ? state.state : null
            })
        default: return state;
    }
}