import * as ACTION_TYPES from './actionTypes';

const initialState = {
    loading : false,
    error : '',
    message : 'initial'
}

export const updateCashInHandMessage = function (state = initialState, action) {
    switch(action.type) {
        case ACTION_TYPES.UPDATE_CASH_IN_HAND_LOADING:
            return action.payload;
        case ACTION_TYPES.UPDATE_CASH_IN_HAND:
            return action.payload;
        case ACTION_TYPES.REFRESH_CASH_IN_HAND_MESSAGE:
            return action.payload;        
        default: return state;    
    }
}