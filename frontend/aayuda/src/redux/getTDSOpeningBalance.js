import * as ACTION_TYPES from './actionTypes';

const initialstate = {
    loading: false,
    message: 'initial',
    data:[],
    error: ''
};

export const getTDSOpeningBalance = function (state = initialstate, action) {
    switch (action.type) {
        case ACTION_TYPES.GET_TDS_OPENING_BALANCE_LOADING:
            return action.payload;
        case ACTION_TYPES.GET_TDS_OPENING_BALANCE:
            return action.payload;
        case ACTION_TYPES.PUSH_TDS_OPENING_BALANCE:
            let amount = action.payload.state.amount;
            let payreceive = action.payload.state.payreceive;
            let businessid = action.payload.businessid;
            let fyid = action.payload.fyid;
            let da = [{
                payreceive: payreceive,
                businessid: businessid,
                fyid: fyid,
                amount: amount,
            }];
            return ({
                loading: false,
                message: '',
                data: da,
                error: '',
                state: action.payload.state
            })
        default: return state;
    }
}