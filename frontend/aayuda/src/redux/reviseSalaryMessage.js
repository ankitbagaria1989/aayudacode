import * as ACTION_TYPES from './actionTypes';

const initialState = {
    loading : false,
    message : "initial",
    disabled : true,
    error : '',
    state: {}
}

export const reviseSalaryMessage = function (state = initialState, action) {
    switch(action.type) {
        case ACTION_TYPES.REVISE_SALARY_LOADING :
            return action.payload;
        case ACTION_TYPES.REVISE_SALARY :
            return action.payload;    
        case ACTION_TYPES.RESET_REVISE_SALARY:
            return action.payload;    
        default : return state;        
    }

}