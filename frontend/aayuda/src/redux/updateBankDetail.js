import * as ACTION_TYPES from './actionTypes';

const initialState = {
    loading : false,
    message : "initial",
    disabled : true,
    error : ''
}

export const updateBankDetailMessage = function (state = initialState, action) {
    switch(action.type) {
        case ACTION_TYPES.UPDATE_BANK_DETAIL_LOADING :
            return action.payload;
        case ACTION_TYPES.UPDATE_BANK_DETAIL :
            return action.payload;
        case ACTION_TYPES.UPDATE_BANK_DETAIL_ERROR:
            return action.payload;    
        case ACTION_TYPES.RESETTING_BANK_DETAIL_CHANGE:
            return action.payload;    
        default : return state;        
    }

}