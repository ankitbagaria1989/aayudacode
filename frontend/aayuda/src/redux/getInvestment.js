import * as ACTION_TYPES from './actionTypes';
import { sortVoucherno } from './sortArrayofObjects';

const initialstate = {
    loading: false,
    message: 'initial',
    data: [],
    error: ''
};

export const getInvestment = function (state = initialstate, action) {
    switch (action.type) {
        case ACTION_TYPES.GET_INVESTMENT_LOADING:
            return action.payload;
        case ACTION_TYPES.GET_INVESTMENT:
            return action.payload;
        case ACTION_TYPES.PUSH_INVESTMENT:
            let da = action.payload.data;
            let existingdata = state.data;
            existingdata.push(da);
            existingdata.sort(sortVoucherno);
            return ({
                loading: false,
                error: '',
                message: '',
                data: existingdata,
                voucherno: action.payload.voucherno
            })
        case ACTION_TYPES.PUSH_CHECK_INVESTMENT:
            let da1 = action.payload.data;
            let existingdata1 = state.data;
            let present = false;
            existingdata1.forEach(d => {
                if (d.id === da1.id) present = true;
            })
            if (present === false) {
                existingdata1.push(da);
                existingdata1.sort(sortVoucherno);
                return ({
                    loading: false,
                    error: '',
                    message: '',
                    data: existingdata1,
                    voucherno: action.payload.voucherno
                })
            }
            else return {
                ...state,
                voucherno: action.payload.voucherno
            }
        case ACTION_TYPES.DELETE_INVESTMENT:
            let newData = state.data.filter(d => d.id !== action.payload.id)
            return ({
                ...state,
                data: newData,
            })
        default: return state;
    }
}