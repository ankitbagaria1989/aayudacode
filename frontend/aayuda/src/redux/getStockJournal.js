import * as ACTION_TYPES from './actionTypes';

const initialstate = {
    loading: false,
    message: 'initial',
    data: [],
    error: ''
};

export const getStockJournal = function (state = initialstate, action) {
    switch (action.type) {
        case ACTION_TYPES.GET_STOCK_JOURNAL_LOADING:
            return action.payload;
        case ACTION_TYPES.GET_STOCK_JOURNAL:
            return action.payload;
        case ACTION_TYPES.PUSH_STOCK_JOURNAL_ENTRY:
            let id = action.payload.id;
            let consumptiondetail = JSON.stringify(action.payload.state.consumptiondetail);
            let productiondetail = JSON.stringify(action.payload.state.productiondetail);
            let date = action.payload.state.date;
            let fyid = action.payload.fyid;
            let businessid = action.payload.businessid;
            let existingdata = state.data;
            existingdata.push({
                id: id,
                businessid: businessid,
                fyid: fyid,
                consumptiondetail: consumptiondetail,
                productiondetail: productiondetail,
                date: date
            })
            return ({
                loading: false,
                message: '',
                data: existingdata,
                error: ''
            })
        default: return state;
    }
}