import * as ACTION_TYPES from './actionTypes';
import { sortItem } from './sortArrayofObjects';

const initialstate = {
    loading: false,
    message: 'initial',
    data: [],
    error: ''
};


export const getItem = function (state = initialstate, action) {
    switch (action.type) {
        case ACTION_TYPES.GET_ITEM_LOADING:
            return action.payload;
        case ACTION_TYPES.GET_ITEM:
            return action.payload;
        case ACTION_TYPES.GET_ITEM_ERROR:
            return action.payload;
        case ACTION_TYPES.REFRESH_ITEM:
            return ({
                loading: false,
                message: 'initial',
                data: [],
                error: ''
            })
        case ACTION_TYPES.REFRESH_ITEM_SECOND:
            return ({
                loading: false,
                message: '',
                data: [],
                error: ''
            })
        case ACTION_TYPES.PUSH_ITEM:
            let item = action.payload.itemDetail;
            let existingItem = state.data;
            let present = false;
            existingItem.forEach(i => {
                if (i.itemid === item.itemid) present = true;
            })
            if (present === false) {
                existingItem.push(item);
                existingItem.sort(sortItem);
                return ({
                    loading: false,
                    data: existingItem,
                    error: '',
                    message: ''
                })
            }
            else return state;
        case ACTION_TYPES.DELETE_AND_PUSH_ITEM:
            let itemUpdate = action.payload.item;
            let existingItemUpdate = state.data;
            existingItemUpdate = existingItemUpdate.filter(it => it.itemid !== itemUpdate.itemid);
            existingItemUpdate.push(itemUpdate);
            existingItemUpdate.sort(sortItem);
            return ({
                loading: false,
                message: '',
                data: existingItemUpdate,
                error: ''
            })
        default: return state;
    }
}