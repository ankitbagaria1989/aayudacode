import * as ACTION_TYPES from './actionTypes';
import { sortBusiness } from './sortArrayofObjects';

const initialstate = {
    loading: false,
    message: 'initial',
    data: [],
    error: ''
};


export const getVendor = function (state = initialstate, action) {
    switch (action.type) {
        case ACTION_TYPES.GET_VENDOR_LOADING:
            return action.payload;
        case ACTION_TYPES.GET_VENDOR:
            return action.payload;
        case ACTION_TYPES.GET_VENDOR_ERROR:
            return action.payload;
        case ACTION_TYPES.REFRESH_VENDOR_SECOND :
            return ({
                message : '',
                error : '',
                loading : false,
                data : []
            })    
        case ACTION_TYPES.REFRESH_VENDOR:
            return ({
                message: 'initial',
                loading: false,
                data: [],
                error: ''
            })
        case ACTION_TYPES.PUSH_NEW_VENDOR:
            let ven = state.data;
            let present = false;
            ven.forEach(v => {
                if (v.vendorid === action.payload.data.vendorid) present = true;
            })
            if (present === false) {
                ven.push(action.payload.data);
                ven.sort(sortBusiness);
                return ({
                    loading: false,
                    message: '',
                    data: ven,
                    error: ''
                })
            }
            else return state;
        case ACTION_TYPES.DELETE_AND_ADD_VENDOR:
            let existingVendor = state.data;
            existingVendor = existingVendor.filter(c => c.vendorid !== action.payload.vendordetail.vendorid);
            existingVendor.push (action.payload.vendordetail); 
            existingVendor.sort(sortBusiness);    
            return ({
                message : '',
                error : '',
                loading : false,
                data : existingVendor
            })  
        default: return state;
    }
}