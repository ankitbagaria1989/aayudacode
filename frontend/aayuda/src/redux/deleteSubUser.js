import * as ACTION_TYPES from './actionTypes';

const initialState = {
    message : '',
    loading : false
}

export const deleteSubUser = function (state = initialState,action) {
    switch(action.type) {
        case ACTION_TYPES.DELETE_SUB_USER_LOADING :
            return ({
                message : '',
                loading : true
            });
        case ACTION_TYPES.DELETE_SUB_USER :
            return ({
                message : action.payload.result,
                loading:false
            });
        default : return state;        
    }
}