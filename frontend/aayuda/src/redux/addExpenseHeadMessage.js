import * as ACTION_TYPES from './actionTypes';

const initialState = {
    loading: false,
    disabled: false,
    message: 'initial',
    error: ''
}

export const addExpenseHeadMessage = function (state = initialState, action) {
    switch (action.type) {
        case ACTION_TYPES.ADD_EXPENSE_HEAD_LOADING:
            return action.payload;
        case ACTION_TYPES.ADD_EXPENSE_HEAD:
            return action.payload;
        case ACTION_TYPES.RESET_ADD_EXPENSE_HEAD_MESSAGE:
            return ({
                loading: false,
                disabled: false,
                message: 'initial',
                error: ''
            })
        default: return state;
    }
}