import * as ACTION_TYPES from './actionTypes';

const initialState = {
    message: 'initial',
    loading: false,
    error: ''
}

export const restoreInvoiceMessage = function (state = initialState, action) {
    switch (action.type) {
        case ACTION_TYPES.RESTORE_INVOICE_MESSAGE_LOADING:
            return ({
                message: '',
                loading: true,
                error: ''
            })
        case ACTION_TYPES.RESTORE_INVOICE_MESSAGE:
            return ({
                message: action.payload.message,
                error: action.payload.error,
                loading: false
            })
        case ACTION_TYPES.RESET_RESTORE_INVOICE_MESSAGE:
            return ({
                message: 'initial',
                error: '',
                loading: false
            })
        default: return state;
    }
}