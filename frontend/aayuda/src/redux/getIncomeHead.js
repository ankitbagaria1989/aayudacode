import * as ACTION_TYPES from './actionTypes';

const initialstate = {
    loading: false,
    message: 'initial',
    data: [],
    error: ''
};

export const getIncomeHead = function (state = initialstate, action) {
    switch (action.type) {
        case ACTION_TYPES.GET_INCOME_HEAD_LOADING:
            return action.payload;
        case ACTION_TYPES.GET_INCOME_HEAD:
            return action.payload;
        case ACTION_TYPES.REFRESH_INCOME_HEAD:
            return ({
                loading: false,
                message: 'initial',
                data: [],
                error: ''
            })
        case ACTION_TYPES.PUSH_INCOME_HEAD:
            let existingincome = state.data;
            existingincome.push(action.payload.data);
            return ({
                loading: false,
                message: '',
                data: existingincome,
                error: ''
            })
        case ACTION_TYPES.DELETE_INCOME_HEAD:
            let newData = state.data.filter(d => d.incomeheadid !== action.payload.id)
            return ({
                ...state,
                data: newData,
            })
        default: return state;
    }
}