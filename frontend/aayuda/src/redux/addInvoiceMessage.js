import * as ACTION_TYPES from './actionTypes';

const initialState = {
    loading : false,
    message : '',
    error : ''
}

export const addInvoiceMessage = function (state = initialState, action) {
    let tempState = {};
    switch (action.type) {
        case ACTION_TYPES.ADD_INVOICE:
           tempState = {
               loading : false,
               message : action.payload.message,
               error : action.payload.error 
           }
           return tempState;
        case ACTION_TYPES.LOADING_ADD_INVOICE:
            tempState = {
                loading : true,
                message : '',
                error : ''
            }
            return tempState;
        case ACTION_TYPES.RESET_ADD_INVOICE_MESSAGE:
            return ({
                loading : false,
                message : '',
                error : '',
                state : state.state !== undefined ? state.state : null
            })    
        case ACTION_TYPES.ADD_INVOICE_ERROR:
            return action.payload;    
        default: return state;
    }
}