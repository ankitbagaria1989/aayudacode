import * as ACTION_TYPES from './actionTypes';

const initialState = {
    loading: false,
    disabled: false,
    message : 'initial',
    error : ''
}

export const addItemMessage = function (state = initialState, action) {
    switch (action.type) {
        case ACTION_TYPES.ADD_ITEM_LOADING:
            return action.payload;
        case ACTION_TYPES.ADD_ITEM :
            return action.payload;
        case ACTION_TYPES.RESET_ITEM_MESSAGE:
            return ({
                loading : false,
                disabled : false,
                message : 'initial',
                error : ''
            })    
        default : return state;         
    }
}