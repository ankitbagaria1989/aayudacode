import * as ACTION_TYPES from './actionTypes';

const initialstate = {
    loading: false,
    message: 'initial',
    data: [],
    error: ''
};

export const getAssetOpeningBalance = function (state = initialstate, action) {
    switch (action.type) {
        case ACTION_TYPES.GET_ASSET_OPENING_BALANCE_LOADING:
            return action.payload;
        case ACTION_TYPES.GET_ASSET_OPENING_BALANCE:
            return action.payload;
        case ACTION_TYPES.PUSH_ASSET_OPENING_BALANCE:
            let amount = action.payload.state.amount;
            let assetaccount = action.payload.state.assetaccount;
            let businessid = action.payload.businessid;
            let fyid = action.payload.fyid;
            let da = state.data;
            let existingdata = da.filter(d => d.assetaccount === assetaccount);
            if (existingdata.length === 0) {
                da.push({
                    assetaccount: assetaccount,
                    businessid: businessid,
                    fyid: fyid,
                    amount: amount
                })
            }
            else da.map(d => d.assetaccount === assetaccount ? d.amount = amount : d.amount = d.amount);
            return ({
                loading: false,
                message: '',
                data: da,
                error: '',
                state: action.payload.state
            })
        default: return state;
    }
}