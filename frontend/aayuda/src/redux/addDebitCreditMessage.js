import * as ACTION_TYPES from './actionTypes';

const initialState = {
    loading : false,
    message : '',
    error : '',
    state : []
}

export const addDebitCreditMessage = function (state = initialState, action) {
    switch (action.type) {
        case ACTION_TYPES.ADD_DEBIT_CREDIT_LOADING:
           return action.payload;
        case ACTION_TYPES.ADD_DEBIT_CREDIT:
            return action.payload;
        case ACTION_TYPES.RESET_ADD_DEBIT_CREDIT:
            return ({
                loading : false,
                message : '',
                error : '',
                state : []
            })
        default: return state;
    }
}