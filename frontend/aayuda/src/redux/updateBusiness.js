import * as ACTION_TYPES from './actionTypes';

const initialState = {
    loading : false,
    message : 'initial',
    disabled : true,
    error : '',
    state: {}
}

export const updateBusinessMessage = function (state = initialState, action) {
    switch (action.type) {
        case ACTION_TYPES.UPDATE_BUSINESS_LOADING:
            return action.payload;
        case ACTION_TYPES.UPDATE_BUSINESS:
            return action.payload;
        case ACTION_TYPES.RESETTING_BUSINESS_CHANGE:
            return action.payload;
        default: return state;    
    }
}