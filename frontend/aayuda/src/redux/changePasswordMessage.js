import * as ACTION_TYPES from './actionTypes';

const initialState = {
    message: 'initial',
    error: '',
    loading: false,
    state: {}
}

export const changePasswordMessage = function (state = initialState, action) {
    switch (action.type) {
        case ACTION_TYPES.CHANGE_PASSWORD_LOADING:
            return action.payload;
        case ACTION_TYPES.CHANGE_PASSWORD:
            return action.payload;
        default: return state;
    }
}