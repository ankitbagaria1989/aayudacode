import * as ACTION_TYPES from './actionTypes';

const initialstate = {
    loading: false,
    message: 'initial',
    data: [],
    error: ''
};

export const getDepreciation = function (state = initialstate, action) {
    switch (action.type) {
        case ACTION_TYPES.GET_DEPRECIATION_LOADING:
            return action.payload;
        case ACTION_TYPES.GET_DEPRECIATION:
            return action.payload;
        case ACTION_TYPES.UPDATE_DEPRECIATION:
            let depreciation = state.data;
            let present = 0;
            depreciation.forEach(d => {
                if (d.asset === action.payload.data.asset) {
                    present = 1;
                    d.remark = action.payload.data.remark;
                    d.amount = action.payload.data.amount;
                    d.depreciateappreciate = action.payload.data.depreciateappreciate;
                }
            })
            if (present === 0) depreciation.push(action.payload.data);
            return ({
                ...state,
                data: depreciation
            })
        case ACTION_TYPES.DELETE_DEPRECIATION:
            let fyid = action.payload.dataObj.fyid;
            let asset = action.payload.dataObj.asset;
            let newData = state.data.filter(d => d.fyid !== fyid && d.asset !== asset);
            return ({
                ...state,
                data: newData,
            })    
        default: return state;
    }
}