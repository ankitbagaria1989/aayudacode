import * as ACTION_TYPES from './actionTypes';

const initialState = {
    loading : false,
    message : '',
    error : '',
    state : {}
}

export const addDepreciationMessage = function (state = initialState, action) {
    let tempState = {};
    switch (action.type) {
        case ACTION_TYPES.ADD_DEPRECIATION_LOADING:
           return action.payload;
        case ACTION_TYPES.ADD_DEPRECIATION:
            return action.payload;
        default: return state;
    }
}