import * as ACTION_TYPES from './actionTypes';

const initialState = {
    loading : false,
    message : '',
    error : ''
}

export const addOpeningClosingMessage = function (state = initialState, action) {
    let tempState = {};
    switch (action.type) {
        case ACTION_TYPES.ADD_OPENINGCLOSING:
           tempState = {
               loading : false,
               message : action.payload.message,
               error : action.payload.error,
               state: action.payload.state 
           }
           return tempState;
        case ACTION_TYPES.LOADING_ADD_OPENINGCLOSING:
            return action.payload;
        case ACTION_TYPES.RESET_ADD_OPENINGCLOSING_MESSAGE:
            return ({
                loading : false,
                message : '',
                error : '',
                state : state.state !== undefined ? state.state : null
            })
        default: return state;
    }
}