export const sortBusiness = function (a, b) {
  let businessA = a.businessname.toUpperCase();
  let businessB = b.businessname.toUpperCase();
  let comparison = 0;
  if (businessA > businessB) {
    comparison = 1;
  } else if (businessA < businessB) {
    comparison = -1;
  }
  return comparison;
}

export const sortFinancialYear = function (a, b) {
  let fyA = a.financialyear;
  let fyB = b.financialyear;
  let comparison = 0;
  if (fyA > fyB) {
    comparison = 1;
  } else if (fyA < fyB) {
    comparison = -1;
  }
  return comparison * -1;
}

export const sortItem = function (a, b) {
  let itemA = a.itemname.toUpperCase();
  let itemB = b.itemname.toUpperCase();
  let comparison = 0;
  if (itemA > itemB) {
    comparison = 1;
  } else if (itemA < itemB) {
    comparison = -1;
  }
  return comparison;
}

export const sortInvoice = function (a, b) {
  let itemA = a.invoicenumber;
  let itemB = b.invoicenumber;
  let comparison = 0;
  if (itemA > itemB) {
    comparison = 1;
  } else if (itemA < itemB) {
    comparison = -1;
  }
  return comparison * -1;
}

export const sortSalesData = function (a, b) {
  let itemA = a.invoicenumber;
  let itemB = b.invoicenumber;
  let comparison = 0;
  if (itemA > itemB) {
    comparison = 1;
  } else if (itemA < itemB) {
    comparison = -1;
  }
  return comparison * -1;
}

export const sortPurchaseData = function (a, b) {
  let itemA = a.invoicedate;
  let itemB = b.invoicedate;
  let comparison = 0;
  if (itemA > itemB) {
    comparison = 1;
  } else if (itemA < itemB) {
    comparison = -1;
  }
  return comparison * -1;
}

export const sortExpense = function (a, b) {
  let itemA = a.date;
  let itemB = b.date;
  let comparison = 0;
  if (itemA > itemB) {
    comparison = 1;
  } else if (itemA < itemB) {
    comparison = -1;
  }
  return comparison * -1;
}

export const sortDataPurchase = function (a, b) {
  let itemA = a.date ? a.date : a.invoicedate ? a.invoicedate : a.purchasedate;
  let itemB = b.date ? b.date : b.invoicedate ? b.invoicedate : b.purchasedate;
  let comparison = 0;
  if (new Date(itemA) >= new Date(itemB)) {
    comparison = 1;
  } else if (new Date(itemA) < new Date(itemB)) {
    comparison = -1;
  }
  return comparison;
}

export const sortData = function (a, b) {
  let itemA = a.date ? a.date : a.invoicedate ? a.invoicedate : a.solddate;
  let itemB = b.date ? b.date : b.invoicedate ? b.invoicedate : b.solddate;
  let comparison = 0;
  if (new Date(itemA) >= new Date(itemB)) {
    comparison = 1;
  } else if (new Date(itemA) < new Date(itemB)) {
    comparison = -1;
  }
  return comparison;
}

export const sortDataStock = function (a, b) {
  let itemA = a.date ;
  let itemB = b.date ;
  let comparison = 0;
  if (new Date(itemA) >= new Date(itemB)) {
    comparison = 1;
  } else if (new Date(itemA) < new Date(itemB)) {
    comparison = -1;
  }
  return -1 * comparison;
}

export const sortVoucherno = function (a, b) {
  let itemA = a.voucherno;
  let itemB = b.voucherno;
  let comparison = 0;
  if (itemA >= itemB) {
    comparison = 1;
  } else if (itemA < itemB) {
    comparison = -1;
  }
  return comparison * -1;
}