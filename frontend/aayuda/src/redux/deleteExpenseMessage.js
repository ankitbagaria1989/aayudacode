import * as ACTION_TYPES from './actionTypes';

const initialState = {
    message: 'initial',
    error: '',
    loading: false
}

export const deleteExpenseMessage = function (state = initialState, action) {
    switch (action.type) {
        case ACTION_TYPES.LOADING_DELETE_EXPENSE_MESSAGE:
            return ({
                message: '',
                error: '',
                loading: true
            })
        case ACTION_TYPES.DELETE_EXPENSE_MESSAGE:
            return ({
                message: action.payload.message,
                error: action.payload.error,
                loading: false
            })
        case ACTION_TYPES.RESET_DELETE_EXPENSE_MESSAGE:
            return ({
                message: 'initial',
                error: '',
                loading: false
            })
        default: return state;
    }
}