import * as ACTION_TYPES from './actionTypes';

const initialState = {
    loading : false,
    message : "initial",
    disabled : false,
    error : '',
    state: {}
}

export const updateStockOpeningMessage = function (state = initialState, action) {
    switch(action.type) {
        case ACTION_TYPES.UPDATE_STOCK_OPENING_LOADING :
            return action.payload;
        case ACTION_TYPES.UPDATE_STOCK_OPENING :
            return action.payload;  
        case ACTION_TYPES.RESETTING_STOCK_OPENING_CHANGE:
            return action.payload;    
        default : return state;        
    }
}