import * as ACTION_TYPES from './actionTypes';

const initialstate = {
    email : '',
    password:''
}

export const login = (state = initialstate, action) => {
    switch (action.type) {
        case ACTION_TYPES.LOGIN_USER :
            let newValuePair = action.payload;
            let newState = {
                ...state,
                [Object.keys(newValuePair)[0]] : newValuePair[Object.keys(newValuePair)[0]]
            }
            return newState;
        case ACTION_TYPES.RESET_LOGIN_FORM_DETAIL :
            return newState = {
                email : '',
                password : ''
            }

        default: 
            return state;
        }
}