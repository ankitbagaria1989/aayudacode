import * as ACTION_TYPES from './actionTypes';

const initialState = {
    loading: false,
    disabled: false,
    message: 'initial',
    error: ''
}

export const addIncomeHeadMessage = function (state = initialState, action) {
    switch (action.type) {
        case ACTION_TYPES.ADD_INCOME_HEAD_LOADING:
            return action.payload;
        case ACTION_TYPES.ADD_INCOME_HEAD:
            return action.payload;
        case ACTION_TYPES.RESET_ADD_INCOME_HEAD_MESSAGE:
            return ({
                loading: false,
                disabled: false,
                message: 'initial',
                error: ''
            })
        default: return state;
    }
}