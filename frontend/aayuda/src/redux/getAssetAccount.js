import * as ACTION_TYPES from './actionTypes';

const initialstate = {
    loading: false,
    message: 'initial',
    data: [],
    error: ''
};


export const getAssetAccount = function (state = initialstate, action) {
    switch (action.type) {
        case ACTION_TYPES.GET_ASSET_ACCOUNT_LOADING:
            return action.payload;
        case ACTION_TYPES.GET_ASSET_ACCOUNT:
            return action.payload;
        case ACTION_TYPES.PUSH_NEW_ASSET_ACCOUNT:
            let assetaccount = action.payload.assetaccount;
            assetaccount.purchase = "";
            assetaccount.purchasevoucherno = "";
            assetaccount.purchasedate = "";
            assetaccount.purchasebasic = "";
            assetaccount.purchaseigst = "";
            assetaccount.purchasecgst = "";
            assetaccount.purchasesgst = "";
            assetaccount.purchasetotal = "";
            assetaccount.purchasefyid = "";
            assetaccount.sold = "";
            assetaccount.solddate = "";
            assetaccount.soldbasic = "";
            assetaccount.soldigst = "";
            assetaccount.soldcgst = "";
            assetaccount.soldsgst = "";
            assetaccount.soldtotal = "";
            assetaccount.soldfyid = "";
            assetaccount.soldvoucherno = "";
            let existingdata = state.data;
            existingdata.push(assetaccount);
            return ({
                message: '',
                error: '',
                loading: false,
                data: existingdata
            })
        case ACTION_TYPES.UPDATE_ASSET_DATA_PURCHASE:
            let data = state.data;
            let assetdata = data.filter(d => d.id === action.payload.assetInformation.assetid)[0];
            assetdata = {
                ...assetdata,
                purchase: "1",
                purchasedate: action.payload.assetInformation.date,
                purchasebasic: action.payload.assetInformation.totalbasic,
                purchaseigst: action.payload.assetInformation.totaligst,
                purchasecgst: action.payload.assetInformation.totalcgst,
                purchasesgst: action.payload.assetInformation.totalsgst,
                purchasetotal: action.payload.assetInformation.grandtotal,
                purchasevoucherno: action.payload.assetInformation.invoicenumber,
                purchasefyid: action.payload.assetInformation.purchasefyid,
                vendorid: action.payload.assetInformation.vendorid,
                type: "asset"
            }
            let otherAssetdata = data.filter(d => d.id !== action.payload.assetInformation.assetid);
            if (otherAssetdata.length > 0) otherAssetdata.push(assetdata);
            else otherAssetdata = [assetdata];
            return ({
                ...state,
                data: otherAssetdata
            })
        case ACTION_TYPES.UPDATE_ASSET_DATA_SELL:
            let data1 = state.data;
            let assetdata1 = data1.filter(d => d.id === action.payload.assetInformation.assetid)[0];
            assetdata1 = {
                ...assetdata1,
                sold: "1",
                solddate: action.payload.assetInformation.date,
                soldbasic: action.payload.assetInformation.totalbasic,
                soldigst: action.payload.assetInformation.totaligst,
                soldcgst: action.payload.assetInformation.totalcgst,
                soldsgst: action.payload.assetInformation.totalsgst,
                grandtotal: action.payload.assetInformation.grandtotal,
                soldvoucherno: action.payload.assetInformation.invoicenumber,
                soldfyid: action.payload.assetInformation.soldfyid,
                customerid: action.payload.assetInformation.customerid,
                type: "asset"
            }
            let otherAssetdata1 = data1.filter(d => d.id !== action.payload.assetInformation.assetid);
            if (otherAssetdata1.length > 0) otherAssetdata1.push(assetdata1);
            else otherAssetdata1 = [assetdata1];
            return ({
                ...state,
                data: otherAssetdata1
            })
        case ACTION_TYPES.DELETE_ASSET:
            let idDelete = action.payload.dataObj.id;
            let typeDelete = action.payload.dataObj.type;
            let dataModify = state.data.filter(d => d.id === idDelete)[0];
            let okData = state.data.filter(d => d.id !== idDelete);
            if (typeDelete === "sold") {
                dataModify.sold = "";
                dataModify.solddate = "";
                dataModify.soldbasic = "";
                dataModify.soldigst = "";
                dataModify.soldcgst = "";
                dataModify.soldsgst = "";
                dataModify.grandtotal = "";
                dataModify.soldvoucherno = "";
                dataModify.soldfyid = "";
                dataModify.customerid = "";
            }
            else if (typeDelete === "purchase") {
                dataModify.purchase = "";
                dataModify.purchasevoucherno = "";
                dataModify.purchasedate = "";
                dataModify.purchasebasic = "";
                dataModify.purchaseigst = "";
                dataModify.purchasecgst = "";
                dataModify.purchasesgst = "";
                dataModify.purchasetotal = "";
                dataModify.purchasefyid = "";
                dataModify.vendorid = "";
            }
            okData.push(dataModify);
            return ({
                ...state,
                data: okData
            })
        default: return state;
    }
}