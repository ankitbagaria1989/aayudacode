import * as ACTION_TYPES from './actionTypes';

const initialstate = {
    loading: false,
    message: 'initial',
    data: [],
    error: ''
};

export const getBankOpeningBalance = function (state = initialstate, action) {
    switch (action.type) {
        case ACTION_TYPES.GET_BANK_OPENING_BALANCE_LOADING:
            return action.payload;
        case ACTION_TYPES.GET_BANK_OPENING_BALANCE:
            return action.payload;
        case ACTION_TYPES.REFRESH_BANK_OPENING_BALANCE:
            return ({
                loading: false,
                message: 'initial',
                data: [],
                error: ''
            })
        case ACTION_TYPES.PUSH_BANK_OPENING_BALANCE:
            let amount = action.payload.state.amount;
            let bankid = action.payload.state.bankid;
            let businessid = action.payload.businessid;
            let fyid = action.payload.fyid;
            let id = action.payload.id;
            let da = state.data;
            let existingdata = da.filter(d => d.bankid === bankid && d.businessid === businessid && d.fyid === fyid);
            if (existingdata.length === 0) {
                da.push({
                    id: id,
                    businessid: businessid,
                    bankid: bankid,
                    fyid: fyid,
                    amount: amount
                })
            }
            else da.map(d => d.bankid === bankid && d.businessid === businessid && d.fyid === fyid ? d.amount = amount : d.amount = d.amount);
            return ({
                loading: false,
                message: '',
                data: da,
                error: '',
                state: action.payload.state
            })
        default: return state;
    }
}