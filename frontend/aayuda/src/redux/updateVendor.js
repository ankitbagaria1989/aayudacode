import * as ACTION_TYPES from './actionTypes';

const initialState = {
    loading : false,
    message : "initial",
    disabled : true,
    error : ''
}

export const updateVendorMessage = function (state = initialState, action) {
    switch(action.type) {
        case ACTION_TYPES.UPDATE_VENDOR_LOADING :
            return action.payload;
        case ACTION_TYPES.UPDATE_VENDOR :
            return action.payload;
        case ACTION_TYPES.UPDATE_VENDOR_ERROR:
            return action.payload;    
        case ACTION_TYPES.RESETTING_VENDOR_CHANGE:
            return action.payload;    
        default : return state;        
    }

}