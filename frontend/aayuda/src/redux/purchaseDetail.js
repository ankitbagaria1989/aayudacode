import * as ACTION_TYPES from './actionTypes';
import { sortPurchaseData } from './sortArrayofObjects';

const initialState = {
    loading: false,
    message: 'initial',
    data: [],
    error: ''
};

export const purchaseDetail = function (state = initialState, action) {
    switch (action.type) {
        case ACTION_TYPES.GET_PURCHASE_DATA:
            return action.payload;
        case ACTION_TYPES.GET_PURCHASE_DATA_LOADING:
            return action.payload;
        case ACTION_TYPES.PUSH_CHECK_PURCHASE_DATA:
            let existingPurchase = state.data;
            let newPurchase = action.payload.data;
            let present = false;
            existingPurchase.forEach(p => {
                if (p.purchaseid === newPurchase.purchaseid) {
                    present = true;
                }
            })
            if (present === false) {
                existingPurchase.push(newPurchase);
                existingPurchase.sort(sortPurchaseData);
                return ({
                    loading: false,
                    message: '',
                    data: existingPurchase,
                    error: '',
                    vendorname: action.payload.vendorname,
                    invoicenumber: action.payload.invoicenumber
                })
            }
            else return {
                ...state,
                vendorname: action.payload.vendorname,
                invoicenumber: action.payload.invoicenumber
            }
        case ACTION_TYPES.PUSH_PURCHASE_DATA:
            let existingPurchase1 = state.data;
            let newPurchase1 = action.payload.purchase;
            existingPurchase1.push(newPurchase1);
            existingPurchase1.sort(sortPurchaseData);
            return ({
                loading: false,
                message: '',
                data: existingPurchase1,
                error: ''
            })
        case ACTION_TYPES.UPDATE_PURCHASE:
            let newd = action.payload.data;
            let purchaseid = action.payload.purchaseid;
            let d = state.data;
            d = d.filter(x => x.purchaseid !== purchaseid);
            d.push(newd);
            d.sort(sortPurchaseData);
            return ({
                ...state,
                error: '',
                message: '',
                loading: false,
                data: d
            })
        case ACTION_TYPES.DELETE_PURCHASE:
            let newData = state.data.filter(d => d.purchaseid !== action.payload.id)
            return ({
                ...state,
                data: newData,
            })         
        default: return state;
    }
}

