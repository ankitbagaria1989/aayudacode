import * as ACTION_TYPES from './actionTypes';

const initialState = {
    loading: false,
    error: '',
    message: 'initial',
    gst:0,
    inputpayable : ''
}

export const getGSTOpeningBalance = function (state = initialState, action) {
    switch (action.type) {
        case ACTION_TYPES.GET_GST_OPENING_BALANCE_LOADING:
            return action.payload
        case ACTION_TYPES.GET_GST_OPENING_BALANCE:
            return action.payload
        case ACTION_TYPES.REFRESH_GST_OPENING_BALANCE:
            return initialState;
        default: return state;
    }
}