import * as ACTION_TYPES from './actionTypes';

const initialstate = {
    loading: false,
    message: 'initial',
    data: [],
    error: ''
};


export const getExpenseHead = function (state = initialstate, action) {
    switch (action.type) {
        case ACTION_TYPES.GET_EXPENSE_HEAD_LOADING:
            return action.payload;
        case ACTION_TYPES.GET_EXPENSE_HEAD:
            return action.payload;
        case ACTION_TYPES.REFRESH_EXPENSE_HEAD:
            return ({
                loading: false,
                message: 'initial',
                data: [],
                error: ''
            })
        case ACTION_TYPES.PUSH_EXPENSE_HEAD:
            let existingexpense = state.data;
            existingexpense.push(action.payload.data);
            return ({
                loading: false,
                message: '',
                data: existingexpense,
                error: ''
            })
        case ACTION_TYPES.DELETE_EXPENSE_HEAD:
            let newData = state.data.filter(d => d.expenseheadid !== action.payload.id)
            return ({
                ...state,
                data: newData,
            })
        default: return state;
    }
}