import * as ACTION_TYPES from './actionTypes';
import { sortVoucherno } from './sortArrayofObjects';

const initialstate = {
    loading: false,
    message: 'initial',
    data: [],
    error: ''
};


export const getIncomeOS = function (state = initialstate, action) {
    switch (action.type) {
        case ACTION_TYPES.GET_INCOMEOS_LOADING:
            return action.payload;
        case ACTION_TYPES.GET_INCOMEOS:
            return action.payload;
        case ACTION_TYPES.REFRESH_INCOMEOS:
            return ({
                loading: false,
                message: 'initial',
                data: [],
                error: ''
            })
        case ACTION_TYPES.PUSH_CHECK_INCOMEOS:
            let da1 = action.payload.data;
            let existingdata1 = state.data;
            let present = false;
            existingdata1.forEach(d => {
                if (d.incomeid === da1.incomeid) present = true;
            })
            if (present === false) {
                existingdata1.push(da1);
                existingdata1.sort(sortVoucherno);
                return ({
                    loading: false,
                    error: '',
                    message: '',
                    data: existingdata1,
                    voucherno: action.payload.voucherno
                })
            }
            else return ({
                ...state,
                voucherno: action.payload.voucherno
            })

        case ACTION_TYPES.PUSH_INCOMEOS:
            let da = action.payload.data;
            let existingdata = state.data;
            existingdata.push(da);
            existingdata.sort(sortVoucherno);
            return ({
                loading: false,
                error: '',
                message: '',
                data: existingdata,
                voucherno: action.payload.voucherno
            })
        case ACTION_TYPES.DELETE_INCOMEOS:
            let newData = state.data.filter(d => d.incomeid !== action.payload.id)
            return ({
                ...state,
                data: newData,
            })
        default: return state;
    }
}