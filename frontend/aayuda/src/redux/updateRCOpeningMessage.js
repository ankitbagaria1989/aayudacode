import * as ACTION_TYPES from './actionTypes';

const initialState = {
    loading : false,
    message : "initial",
    disabled : false,
    error : '',
    state: {}
}

export const updateRCOpeningMessage = function (state = initialState, action) {
    switch(action.type) {
        case ACTION_TYPES.UPDATE_RC_OPENING_LOADING :
            return action.payload;
        case ACTION_TYPES.UPDATE_RC_OPENING :
            return action.payload;      
        default : return state;        
    }
}