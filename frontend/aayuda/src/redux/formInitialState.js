export const InitialLogin = {
    email: '',
    password: ''
}

export const InitialRegister = {
    fullname: '',
    email: '',
    password: ''
}

export const InitailAddBusiness = {
    businessname: '',
    alias:'',
    gstin: '',
    stateut: 'state',
    state:'',
    address: '',
    pincode: '',
    mobilenumber: '',
    landline: '',
    email: '',
    pan:''
}

export const InitialSubUser = {
    email: ''
}

export const InitialFinancialYear = {
    fy: ''
}
export const InitialBusinessChange = {
    businessname: '',
    gstin: '',
    state: '',
    address: '',
    pincode: '',
    mobile: '',
    landline: '',
    email: '',
    alias:'',
    stateut:'state',
    pan: ''
}
export const InitialAddCustomer = {
    businessname: '',
    gstin: '',
    state: '',
    address: '',
    pincode: '',
    mobile: '',
    landline: '',
    email: '',
    alias: ''
}

export const InitialBankDetail = {
    bankname: '',
    accountnumber: ''
}

export const InitialItem = {
    itemname: '',
    alias : '',
    hsncode: '',
    taxtype: "gst",
    gstrate: '',
    vatrate: '',
    exciserate: '',
    billingunit: ''
}

export const MarkAttendancee = {

}

export const InitialAddExpenseHead = {
    expenseheadname:''
}
export const InitialAddIncomeHead = {
    incomeheadname:''
}
export const InitailAddLoanDetail = {
    loanaccount: ''
}
export const InitialAddAssetDetail = {
    assetaccount: '',
    depreciating : false,
    purchasedate:  new Date().toDateString().split(' ').slice(1).join(' '),
    purchasevalue: ''
}
export const InitialAddInvestmentDetail = {
    investmentaccount: '',
    shareholding: ''
}