import * as ACTION_TYPES from './actionTypes';

const initialState = {
    message: 'initial',
    error: '',
    loading: false
}

export const cancelInvoiceMessage = function (state = initialState, action) {
    switch (action.type) {
        case ACTION_TYPES.LOADING_CANCEL_INVOICE_MESSAGE:
            return ({
                message: '',
                error: '',
                loading: true
            })
        case ACTION_TYPES.CANCEL_INVOICE_MESSAGE:
            return ({
                message: action.payload.message,
                error: action.payload.error,
                loading: false
            })
        case ACTION_TYPES.RESET_CANCEL_INVOICE_MESSAGE:
            return ({
                message: 'initial',
                error: '',
                loading: false
            })
        default: return state;
    }
}