import * as ACTION_TYPES from './actionTypes';

const initialState = {
    loading: false,
    error: '',
    message: 'initial',
    data: []
}

export const getAttendance = function (state = initialState, action) {
    switch (action.type) {
        case ACTION_TYPES.GET_ATTENDANCE_LOADING:
            return action.payload
        case ACTION_TYPES.GET_ATTENDANCE:
            return action.payload
        case ACTION_TYPES.REFRESH_GET_ATTENDANCE:
            return ({
                loading: false,
                error: '',
                message: 'initial',
                data: []
            })
        default: return state;
    }
}