import * as ACTION_TYPES from './actionTypes';

const initialState = {
    message: 'initial',
    error: '',
    loading: false
}

export const deleteEntryMessage = function (state = initialState, action) {
    switch (action.type) {
        case ACTION_TYPES.DELETE_ENTRY_LOADING:
            return action.payload;
        case ACTION_TYPES.DELETE_ENTRY:
            return action.payload;
        default: return state;
    }
}