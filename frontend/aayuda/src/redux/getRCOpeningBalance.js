import * as ACTION_TYPES from './actionTypes';

const initialstate = {
    loading: false,
    message: 'initial',
    data: 0,
    error: ''
};

export const getRCOpeningBalance = function (state = initialstate, action) {
    switch (action.type) {
        case ACTION_TYPES.GET_RC_OPENING_BALANCE_LOADING:
            return action.payload;
        case ACTION_TYPES.GET_RC_OPENING_BALANCE:
            return action.payload;
        case ACTION_TYPES.PUSH_RC_OPENING_BALANCE:
            let amount = action.payload.state.amount;
            return ({
                loading: false,
                message: '',
                data: amount,
                error: '',
                state: action.payload.state
            })
        default: return state;
    }
}