import * as ACTION_TYPES from './actionTypes';

const initialstate = {
    loading: false,
    message: 'initial',
    data: [],
    error: ''
};

export const getEmployeeOpeningBalance = function (state = initialstate, action) {
    switch (action.type) {
        case ACTION_TYPES.GET_EMPLOYEE_OPENING_BALANCE_LOADING:
            return action.payload;
        case ACTION_TYPES.GET_EMPLOYEE_OPENING_BALANCE:
            return action.payload;
        case ACTION_TYPES.PUSH_EMPLOYEE_OPENING_BALANCE:
            let amount = action.payload.state.amount;
            let employeeaccount = action.payload.state.employeeaccount;
            let payablepaidadvance = action.payload.state.payablepaidadvance;
            let businessid = action.payload.businessid;
            let fyid = action.payload.fyid;
            let da = state.data;
            let existingdata = da.filter(d => d.employeeaccount === employeeaccount);
            if (existingdata.length === 0) {
                da.push({
                    employeeaccount: employeeaccount,
                    businessid: businessid,
                    fyid: fyid,
                    amount: amount,
                    payablepaidadvance: payablepaidadvance
                })
            }
            else da.map(d => {
                if (d.employeeaccount === employeeaccount) {
                    d.amount = amount;
                    d.payablepaidadvance = payablepaidadvance;
                }
            });
            return ({
                loading: false,
                message: '',
                data: da,
                error: '',
                state: action.payload.state
            })
        default: return state;
    }
}