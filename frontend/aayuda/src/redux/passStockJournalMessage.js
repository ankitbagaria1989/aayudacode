import * as ACTION_TYPES from './actionTypes';

const initialState = {
    message: 'initial',
    error: '',
    loading: false
}

export const passStockJournalMessage = function (state = initialState, action) {
    switch (action.type) {
        case ACTION_TYPES.PASS_STOCK_JOURNAL_LOADING:
            return action.payload;
        case ACTION_TYPES.PASS_STOCK_JOURNAL:
            return action.payload;
        default: return state;
    }
}