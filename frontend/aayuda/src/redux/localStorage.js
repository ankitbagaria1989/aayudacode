
export const loadState = () => {
    try {
        const serializedState = localStorage.getItem('state');
        if (serializedState === null) return undefined;
        return JSON.parse(serializedState);
    }
    catch (err) {
        return undefined;
    }
}
export const saveState = ({ businessList, getItem, getVendor, getBankDetail, getCustomer, individualBusinessMessage, salesDetail, purchaseDetail, getExpense, getPayment, getBankEntry, getOpeningClosing, getOpeningBalance, getCashInHand, getEmployee, getGSTOpeningBalance, getDebitCredit, getExpenseHead, getIncomeOS, getBankOpeningBalance, getLoan, getLoanAccount, getInvestment, getIncomeHead, getAssetAccount, getDepreciation, getAssetOpeningBalance, getLoanOpeningBalance, getInvestmentAccount, getInvestmentOpeningBalance, getTDSOpeningBalance, getRCOpeningBalance, getStockOpeningBalance, getIncomeTaxOpeningBalance, getStockJournal, getEmployeeOpeningBalance }) => {
    const state = {
        businessList: businessList,
        getItem: getItem,
        getVendor: getVendor,
        getBankDetail: getBankDetail,
        getCustomer: getCustomer,
        individualBusinessMessage: individualBusinessMessage,
        salesDetail: salesDetail,
        purchaseDetail: purchaseDetail,
        getExpense: getExpense,
        getPayment: getPayment,
        getBankEntry: getBankEntry,
        getOpeningClosing: getOpeningClosing,
        getOpeningBalance: getOpeningBalance,
        getCashInHand: getCashInHand,
        getEmployee: getEmployee,
        getGSTOpeningBalance: getGSTOpeningBalance,
        getDebitCredit: getDebitCredit,
        getExpenseHead: getExpenseHead,
        getIncomeOS: getIncomeOS,
        getBankOpeningBalance: getBankOpeningBalance,
        getLoan: getLoan,
        getLoanAccount: getLoanAccount,
        getInvestment: getInvestment,
        getIncomeHead: getIncomeHead,
        getAssetAccount: getAssetAccount,
        getDepreciation: getDepreciation,
        getLoanOpeningBalance: getLoanOpeningBalance,
        getAssetOpeningBalance: getAssetOpeningBalance,
        getInvestmentAccount: getInvestmentAccount,
        getInvestmentOpeningBalance: getInvestmentOpeningBalance,
        getTDSOpeningBalance: getTDSOpeningBalance,
        getRCOpeningBalance: getRCOpeningBalance,
        getStockOpeningBalance: getStockOpeningBalance,
        getIncomeTaxOpeningBalance: getIncomeTaxOpeningBalance,
        getStockJournal: getStockJournal,
        getEmployeeOpeningBalance: getEmployeeOpeningBalance
    }
    try {
        const serializedState = JSON.stringify(state);
        localStorage.setItem('state', serializedState);
    }
    catch (err) {
        console.log(err);
    }
}

