import * as ACTION_TYPES from './actionTypes';

const initialState = {
    loading : false,
    message : "initial",
    disabled : true,
    error : '',
    state: {}
}

export const terminateEmployeeMessage = function (state = initialState, action) {
    switch(action.type) {
        case ACTION_TYPES.TERMINATE_EMPLOYEE_LOADING :
            return action.payload;
        case ACTION_TYPES.TERMINATE_EMPLOYEE :
            return action.payload;    
        case ACTION_TYPES.RESET_TERMINATE_EMPLOYEE:
            return action.payload;    
        default : return state;        
    }

}