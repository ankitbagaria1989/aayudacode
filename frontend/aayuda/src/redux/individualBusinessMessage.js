import * as ACTION_TYPES from './actionTypes';

const initialState = {
    message : 'initial',
    error : '',
    loading : false
};

export const individualBusinessMessage = function (state = initialState, action) {
    switch(action.type){
        case ACTION_TYPES.CONSUME_INDIVIDUAL_BUSINESS:
            return ({
                message : '',
                error : '',
                loading : false
            })
        case ACTION_TYPES.LOADING_INDIVIDUAL_BUSINESS:
            return({
                message: '',
                error : '',
                loading : true
            })
        case ACTION_TYPES.INDIVIDUAL_BUSINESS:
            return action.payload;  
        case ACTION_TYPES.RESET_INDIVIDUAL_BUSINESS:
            return action.payload;    
        default : return state;          
    }
}