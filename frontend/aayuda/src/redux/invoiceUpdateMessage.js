import * as ACTION_TYPES from './actionTypes';

const initialState = {
    loading : false,
    message : 'initial',
    disabled : true,
    error : '',
    state : []
}

export const invoiceUpdateMessage = function (state = initialState, action) {
    switch (action.type) {
        case ACTION_TYPES.INVOICE_UPDATE_LOADING:
            return action.payload;
        case ACTION_TYPES.INVOICE_UPDATE:
            return action.payload;
        case ACTION_TYPES.RESET_INVOICE_UPDATE:
            return action.payload;
        case ACTION_TYPES.RESET_INVOICE_UPDATE_MESSAGE:
            return ({
                loading : false,
                message : 'initial',
                disabled : true,
                error : '',
                state:[]
            })    
        default: return state;    
    }
}