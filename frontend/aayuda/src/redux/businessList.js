import * as ACTION_TYPES from './actionTypes';
import { sortBusiness, sortFinancialYear } from './sortArrayofObjects';

const initialstate = {
    loading: false,
    message: 'initial',
    error: '',
    data: []
};

export const businessList = (state = initialstate, action) => {
    console.log("Action" + action);
    switch (action.type) {
        case ACTION_TYPES.BUSINESS_LIST:
            if (action.payload.result === "success") {
                let business = action.payload.data;
                let newState = { data: [] };
                business.forEach((buss) => {
                    if (buss) {
                        if (newState.data.length == 0) newState.data.push(createState(buss));
                        else {
                            if (newState.data[newState.data.length - 1].businessid != buss.businessid) newState.data.push(createState(buss));
                            else {
                                newState.data[newState.data.length - 1].financial.push({ "financialyear": buss.financialyear, "fyid": buss.fyid });
                            }
                        }
                    }
                })
                newState.data.map((buss) => {
                    buss.financial.sort(sortFinancialYear);
                })
                return { loading: false, message: action.payload.result, error: '', data: newState.data };
            }
            else {
                return { loading: false, message: '', error: action.payload.result, data: [] }
            }
        case ACTION_TYPES.BUSINESS_LIST_LOADING:
            return { loading: true, message: '', error: '', data: [] }
        case ACTION_TYPES.PUSH_CHECK_BUSINESS:
            let tempdata = state.data;
            let present = false;
            tempdata.forEach((d) => {
                if (d.businessid === action.payload.data.businessid) present = true;
            })
            if (present === false) {
                tempdata.push(createState(action.payload.data));
                tempdata.sort(sortBusiness);
                return ({
                    loading: false,
                    message: '',
                    error: '',
                    data: tempdata,
                    gstin: action.payload.gstin
                })
            }
            else return {
                ...state,
                gstin: action.payload.gstin
            };
        case ACTION_TYPES.PUSH_NEW_BUSINESS:
            let tempdata1 = state.data;
            tempdata1.push(createState(action.payload.data));
            tempdata1.sort(sortBusiness);
            return ({
                loading: false,
                message: '',
                error: '',
                data: tempdata1
            })
        case ACTION_TYPES.BUSINESS_LIST_RESET:
            return action.payload;
        case ACTION_TYPES.DELETE_AND_INSERT_BUSINESS:
            let buss = state.data;
            let businessid = action.payload.business.businessid;
            let leftBusiness = buss.filter(b => b.businessid !== businessid);
            leftBusiness.push(action.payload.business);
            leftBusiness.sort(sortBusiness);
            return ({
                loading: false,
                message: '',
                data: leftBusiness,
                error: ''
            })
        case ACTION_TYPES.PUSH_CHECK_FY:
            let data = action.payload.data;
            let fy1 = {
                fyid: data.fyid,
                financialyear: data.financialyear
            }
            let existingdata = state.data;
            let present1 = false;
            existingdata.map(b => {
                if (b.businessid === data.businessid) {
                    b.financial.forEach(x => {
                        if (x.fyid === data.fyid) present1 = true;
                    })
                    if (present1 === false) {
                        b.financial.push(fy1);
                        b.financial.sort(sortFinancialYear);
                    }
                }
                return b;
            })
            return ({
                error: '',
                message: '',
                data: existingdata,
                loading: false,
                fy: action.payload.fy
            })
                
        case ACTION_TYPES.PUSH_NEW_FY:
            let fy = {
                fyid: action.payload.fyid,
                financialyear: action.payload.fy
            }
            let business = state.data;
            business.map(b => {
                if (b.businessid === action.payload.businessid) {
                    b.financial = b.financial.filter(f => (f.fyid !== null && f.fyid !== undefined))
                    let present = false;
                    b.financial.forEach(x => {
                        if (x.fyid === fy.fyid) present = true;
                    })
                    if (present === false) {
                        b.financial.push(fy);
                        b.financial.sort(sortFinancialYear);
                    }
                }
                return b;
            })
            return ({
                error: '',
                message: '',
                data: business,
                loading: false
            })
        case ACTION_TYPES.ADD_INDIVIDUAL_BUSINESS:
            let individualbusiness = state.data;
            individualbusiness = individualbusiness.filter(b => b.businessid !== action.payload.businessid);
            let newBusiness = action.payload.data;
            let newBusinessState;
            newBusiness.forEach(b => {
                if (newBusinessState === undefined) newBusinessState = createState(b);
                else newBusinessState.financial.push({ "financialyear": b.financialyear, "fyid": b.fyid });
            })
            newBusinessState.financial.sort(sortFinancialYear);
            individualbusiness.push(newBusinessState);
            individualbusiness.sort(sortBusiness);
            return ({
                loading: false,
                data: individualbusiness,
                error: '',
                message: ''
            })
        default:
            return state;
    }
}

const createState = function (buss) {
    let tempState = {
        "businessid": buss.businessid,
        "businessname": buss.businessname,
        "gstin": buss.gstin,
        "address": buss.address,
        "state": buss.state,
        "landline": buss.landline,
        "mobile": buss.mobile,
        "email": buss.email,
        "stateut": buss.stateut,
        "alias": buss.alias,
        "pincode": buss.pincode,
        "pan" : buss.pan,
        "businesstype": buss.businesstype,
        "noofshares": buss.noofshares,
        "fv": buss.fv,
        "financial": [{
            "financialyear": buss.financialyear,
            "fyid": buss.fyid
        }]
    };
    return tempState;

}