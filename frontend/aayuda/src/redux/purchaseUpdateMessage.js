import * as ACTION_TYPES from './actionTypes';

const initialState = {
    loading : false,
    message : 'initial',
    disabled : true,
    error : '',
    state : []
}

export const purchaseUpdateMessage = function (state = initialState, action) {
    switch (action.type) {
        case ACTION_TYPES.PURCHASE_UPDATE_LOADING:
            return action.payload;
        case ACTION_TYPES.PURCHASE_UPDATE:
            return action.payload;
        case ACTION_TYPES.RESET_PURCHASE_UPDATE_MESSAGE:
            return ({
                loading : false,
                message : 'initial',
                disabled : true,
                error : '',
                state:[]
            })    
        default: return state;    
    }
}