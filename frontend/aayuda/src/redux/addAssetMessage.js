import * as ACTION_TYPES from './actionTypes';

const initialState = {
    loading: false,
    message: '',
    error: ''
}

export const addAssetMessage = function (state = initialState, action) {
    let tempState = {};
    switch (action.type) {
        case ACTION_TYPES.ADD_ASSET_LOADING:
            return action.payload;
        case ACTION_TYPES.ADD_ASSET:
            return action.payload;
        default: return state;
    }
}