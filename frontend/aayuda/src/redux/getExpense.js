import * as ACTION_TYPES from './actionTypes';
import { sortVoucherno } from './sortArrayofObjects';

const initialstate = {
    loading: false,
    message: 'initial',
    data: [],
    error: ''
};


export const getExpense = function (state = initialstate, action) {
    switch (action.type) {
        case ACTION_TYPES.GET_EXPENSE_LOADING:
            return action.payload;
        case ACTION_TYPES.GET_EXPENSE:
            return action.payload;
        case ACTION_TYPES.REFRESH_EXPENSE:
            return ({
                loading: false,
                message: 'initial',
                data: [],
                error: ''
            })
        case ACTION_TYPES.REFRESH_EXPENSE_SECOND:
            return ({
                loading: false,
                message: '',
                data: [],
                error: ''
            })
        case ACTION_TYPES.PUSH_CHECK_EXPENSE:
            let da1 = action.payload.data;
            let existingdata1 = state.data;
            let present = false;
            existingdata1.forEach(d => {
                if (d.expenseid === da1.expenseid) present = true;
            })
            if (present === false) {
                existingdata1.push(da1);
                existingdata1.sort(sortVoucherno);
                return ({
                    loading: false,
                    error: '',
                    message: '',
                    data: existingdata1,
                    voucherno: action.payload.voucherno
                })
            }
            else return ({
                ...state,
                voucherno: action.payload.voucherno
            })
        case ACTION_TYPES.PUSH_EXPENSE:
            let da = action.payload.data;
            let existingdata = state.data;
            existingdata.push(da);
            existingdata.sort(sortVoucherno);
            return ({
                loading: false,
                error: '',
                message: '',
                data: existingdata,
                voucherno: action.payload.voucherno
            })
        case ACTION_TYPES.DELETE_EXPENSE:
            let newData = state.data.filter(d => d.expenseid !== action.payload.id)
            return ({
                ...state,
                data: newData,
            })
        default: return state;
    }
}