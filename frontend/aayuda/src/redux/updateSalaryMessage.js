import * as ACTION_TYPES from './actionTypes';
const initialState = {
    loading: false,
    error: '',
    message: 'initial'
}

export const updateSalaryMessage = function (state = initialState, action) {
    switch (action.type) {
        case ACTION_TYPES.UPDATE_SALARY_LOADING:
            return action.payload;
        case ACTION_TYPES.UPDATE_SALARY:
            return action.payload;
        case ACTION_TYPES.REFRESH_SALARY_MESSAGE:
            return ({
                loading: false,
                error: '',
                message: 'initial'
            })
        default: return state;
    }
}