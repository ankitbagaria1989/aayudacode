import * as ACTION_TYPES from './actionTypes';

const initialstate = {
    loading: false,
    message: 'initial',
    data: {},
    error: ''
};


export const getOpeningClosing = function (state = initialstate, action) {
    switch (action.type) {
        case ACTION_TYPES.GET_OPENINGCLOSING_LOADING:
            return action.payload;
        case ACTION_TYPES.GET_OPENINGCLOSING:
            return action.payload;
        case ACTION_TYPES.REFRESH_OPENINGCLOSING:
            return ({
                loading: false,
                message: 'initial',
                data: {},
                error: ''
            })
        case ACTION_TYPES.REFRESH_OPENINGCLOSING_SECOND:
            return ({
                loading: false,
                message: '',
                data: {},
                error: ''
            })
        default: return state;
    }
}