import * as ACTION_TYPES from './actionTypes';

const initialstate = {
    loading: false,
    message: 'initial',
    data: [],
    error: ''
};


export const getLoanAccount = function (state = initialstate, action) {
    switch (action.type) {
        case ACTION_TYPES.GET_LOAN_ACCOUNT_LOADING:
            return action.payload;
        case ACTION_TYPES.GET_LOAN_ACCOUNT:
            return action.payload;
        case ACTION_TYPES.PUSH_NEW_LOAN_ACCOUNT:
            let loanaccount = state.data;
            loanaccount.push(action.payload.loanaccount);
            return ({
                message: '',
                error: '',
                loading: false,
                data: loanaccount
            })    
        default: return state;
    }
}