import * as ACTION_TYPES from './actionTypes';

const initialState = {
    loading: false,
    disabled: false,
    message : '',
    error : ''
}

export const addCustomerMessage = function (state = initialState, action) {
    switch (action.type) {
        case ACTION_TYPES.ADD_CUSTOMER_LOADING:
            return action.payload;
        case ACTION_TYPES.ADD_CUSTOMER :
            return action.payload;
        case ACTION_TYPES.RESET_CUSTOMER_MESSAGE:
            return ({
                loading : false,
                disabled : false,
                message : '',
                error : ''
            });    
        default : return state;         
    }
}