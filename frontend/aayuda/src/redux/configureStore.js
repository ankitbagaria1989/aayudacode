import { createStore, combineReducers, applyMiddleware } from 'redux';
import { createForms } from 'react-redux-form';
import thunk from 'redux-thunk';
import logger from 'redux-logger';
import { businessList } from './businessList';
import { subUserList } from './subuserList';
import { deleteSubUser } from './deleteSubUser';
import { updateBusinessMessage } from './updateBusiness';
import { addCustomerMessage } from './addCustomer';
import { getCustomer } from './getCustomer';
import { updateCustomerMessage } from './updateCustomer';
import { addVendorMessage } from './addVendor';
import { getVendor } from './getVendor';
import { updateVendorMessage } from './updateVendor';
import { addBankDetailMessage } from './addBankDetail';
import { getBankDetail } from './getBankDetail';
import { updateBankDetailMessage } from './updateBankDetail';
import { addItemMessage } from './addItem';
import { getItem } from './getItem';
import { updateItemMessage } from './updateItem';
import { addInvoiceMessage } from './addInvoiceMessage';
import { salesDetail } from './salesDetail';
import { addPurchaseMessage } from './addPurchaseMessage';
import { purchaseDetail } from './purchaseDetail';
import { individualBusinessMessage } from './individualBusinessMessage';
import { cancelInvoiceMessage } from './cancelInvoiceMessage';
import { restoreInvoiceMessage } from './restoreInvoiceMessage';
import { invoiceUpdateMessage } from './invoiceUpdateMessage';
import { deletePurchaseMessage } from './deletePurchaseMessage';
import { addExpenseMessage } from './addExpenseMessage';
import { getExpense } from './getExpense';
import { addPaymentMessage } from './addPaymentMessage';
import { getPayment } from './getPayment';
import { getBankEntry } from './getBankEntry';
import { addBankEntryMessage } from './addBankEntryMessage';
import { addOpeningClosingMessage } from './addOpeningClosingMessage';
import { getOpeningClosing } from './getOpeningClosing';
import { updatePartyOpeningMessage } from './updatePartyOpening';
import { getOpeningBalance } from './getOpeningBalance';
import { logoutMessage } from './logoutMessage';
import { getCashInHand } from './getCashInHand';
import { updateCashInHandMessage } from './updateCashInHandMessage';
import { getEmployee } from './getEmployee';
import { addEmployeeMessage } from './addEmployeeMessage';
import { getAttendance } from './getAttendance';
import { updateAttendanceMessage } from './updateAttendanceMessage';
import { getMonthAttendance } from './getMonthAttendance';
import { getSalary } from './getSalary';
import { updateSalaryMessage } from './updateSalaryMessage';
import { purchaseUpdateMessage } from './purchaseUpdateMessage';
import { deleteExpenseMessage } from './deleteExpenseMessage';
import { updateBankOpeningMessage } from './updateBankOpening';
import { getBankOpeningBalance } from './getBankOpeningBalance';
import { getGSTOpeningBalance } from './getGSTOpeningBalance';
import { updateGSTOpeningBalanceMessage } from './updateGSTOpeningBalanceMessage';
import { addDebitCreditMessage } from './addDebitCreditMessage';
import { getDebitCredit } from './getDebitCredit';
import { deleteEntryMessage } from './deleteEntryMessage';
import { getExpenseHead } from './getExpenseHead';
import { addExpenseHeadMessage } from './addExpenseHeadMessage';
import { addIncomeOSMessage } from './addIncomeOSMessage';
import { getIncomeOS } from './getIncomeOS';
import { reviseSalaryMessage } from './reviseSalaryMessage';
import { terminateEmployeeMessage } from './terminateEmployeeMessage';
import { changePasswordMessage } from './changePasswordMessage';
import { addLoanMessage } from './addLoanMessage';
import { getLoan } from './getLoan';
import { getLoanAccount } from './getLoanAccount';
import { addLoanAccountMessage } from './addLoanAccountMessage';
import { getInvestment } from './getInvestment';
import { addInvestmentMessage } from './addInvestmentMessage';
import { addIncomeHeadMessage } from './addIncomeHeadMessage';
import { getIncomeHead } from './getIncomeHead';
import { getAssetAccount } from './getAssetAccount';
import { addAssetAccountMessage } from './addAssetAccountMessage';
import { addAssetMessage } from './addAssetMessage';
import { addDepreciationMessage } from './addDepreciationMessage';
import { getDepreciation } from './getDepreciation';
import { updateLoanOpeningMessage } from './updateLoanOpening';
import { getLoanOpeningBalance } from './getLoanOpeningBalance';
import { updateAssetOpeningMessage } from './updateAssetOpening';
import { getAssetOpeningBalance } from './getAssetOpeningBalance';
import { getInvestmentAccount } from './getInvestmentAccount';
import { addInvestmentAccountMessage } from './addInvestmentAccountMessage';
import { updateInvestmentOpeningMessage } from './updateInvestmentOpening';
import { getInvestmentOpeningBalance } from './getInvestmentOpeningBalance';
import { updateTDSOpeningMessage } from './updateTDSOpeningMessage';
import { getTDSOpeningBalance } from './getTDSOpeningBalance';
import { getRCOpeningBalance } from './getRCOpeningBalance';
import { updateRCOpeningMessage } from './updateRCOpeningMessage';
import { updateStockOpeningMessage } from './updateStockOpeningMessage';
import { getStockOpeningBalance } from './getStockOpeningBalance';
import { getIncomeTaxOpeningBalance } from './getIncomeTaxOpeningBalance';
import { updateIncomeTaxOpeningMessage } from './updateIncomeTaxOpeningMessage';
import { updateInvestmentAccountMessage } from './updateInvestmentAccountMessage';
import { passStockJournalMessage } from './passStockJournalMessage';
import { getStockJournal } from './getStockJournal';
import { updateEmployeeOpeningMessage } from './updateEmployeeOpeningMessage';
import { getEmployeeOpeningBalance } from './getEmployeeOpeningBalance';
import { registerMessage, loginMessage, addBusinessMessage, addSubUserMessage, addFinancialYearMessage } from './formMessage';
import { InitialLogin, InitialRegister, InitailAddBusiness, InitialSubUser, InitialFinancialYear, InitialBusinessChange, InitialAddCustomer, InitialBankDetail, InitialItem, MarkAttendancee, InitialAddExpenseHead, InitailAddLoanDetail, InitialAddIncomeHead, InitialAddAssetDetail, InitialAddInvestmentDetail } from './formInitialState'
import { loadState, saveState } from './localStorage';
import throttle from 'lodash/throttle';

export const configureStore = () => {
    let persistedState = loadState();
    const store = createStore(combineReducers({
        individualBusinessMessage: individualBusinessMessage,
        addInvoiceMessage: addInvoiceMessage,
        businessList: businessList,
        registerMessage: registerMessage,
        loginMessage: loginMessage,
        addBusinessMessage: addBusinessMessage,
        addSubUserMessage: addSubUserMessage,
        subUserList: subUserList,
        deleteSubUser: deleteSubUser,
        addFinancialYearMessage: addFinancialYearMessage,
        updateBusinessMessage: updateBusinessMessage,
        addCustomerMessage: addCustomerMessage,
        getCustomer: getCustomer,
        updateCustomerMessage: updateCustomerMessage,
        addVendorMessage: addVendorMessage,
        getVendor: getVendor,
        updateVendorMessage: updateVendorMessage,
        addBankDetailMessage: addBankDetailMessage,
        getBankDetail: getBankDetail,
        updateBankDetailMessage: updateBankDetailMessage,
        addItemMessage: addItemMessage,
        getItem: getItem,
        updateItemMessage: updateItemMessage,
        salesDetail: salesDetail,
        addPurchaseMessage: addPurchaseMessage,
        purchaseDetail: purchaseDetail,
        cancelInvoiceMessage: cancelInvoiceMessage,
        restoreInvoiceMessage: restoreInvoiceMessage,
        invoiceUpdateMessage: invoiceUpdateMessage,
        deletePurchaseMessage: deletePurchaseMessage,
        addExpenseMessage: addExpenseMessage,
        getExpense: getExpense,
        addPaymentMessage: addPaymentMessage,
        getPayment: getPayment,
        addBankEntryMessage: addBankEntryMessage,
        getBankEntry: getBankEntry,
        addOpeningClosingMessage: addOpeningClosingMessage,
        getOpeningClosing: getOpeningClosing,
        updatePartyOpeningMessage: updatePartyOpeningMessage,
        getOpeningBalance: getOpeningBalance,
        logoutMessage: logoutMessage,
        getCashInHand: getCashInHand,
        updateCashInHandMessage: updateCashInHandMessage,
        getEmployee: getEmployee,
        addEmployeeMessage: addEmployeeMessage,
        getAttendance: getAttendance,
        updateAttendanceMessage: updateAttendanceMessage,
        getMonthAttendance: getMonthAttendance,
        getSalary: getSalary,
        updateSalaryMessage: updateSalaryMessage,
        purchaseUpdateMessage: purchaseUpdateMessage,
        deleteExpenseMessage: deleteExpenseMessage,
        updateBankOpeningMessage: updateBankOpeningMessage,
        getBankOpeningBalance: getBankOpeningBalance,
        getGSTOpeningBalance: getGSTOpeningBalance,
        updateGSTOpeningBalanceMessage: updateGSTOpeningBalanceMessage,
        addDebitCreditMessage: addDebitCreditMessage,
        getDebitCredit: getDebitCredit,
        deleteEntryMessage: deleteEntryMessage,
        getExpenseHead: getExpenseHead,
        addExpenseHeadMessage: addExpenseHeadMessage,
        addIncomeOSMessage: addIncomeOSMessage,
        getIncomeOS: getIncomeOS,
        reviseSalaryMessage: reviseSalaryMessage,
        terminateEmployeeMessage: terminateEmployeeMessage,
        changePasswordMessage: changePasswordMessage,
        addLoanMessage: addLoanMessage,
        getLoan: getLoan,
        getLoanAccount: getLoanAccount,
        addLoanAccountMessage: addLoanAccountMessage,
        getInvestment: getInvestment,
        addInvestmentMessage: addInvestmentMessage,
        addIncomeHeadMessage: addIncomeHeadMessage,
        getIncomeHead: getIncomeHead,
        getAssetAccount: getAssetAccount,
        addAssetAccountMessage: addAssetAccountMessage,
        addAssetMessage: addAssetMessage,
        addDepreciationMessage: addDepreciationMessage,
        getDepreciation: getDepreciation,
        updateLoanOpeningMessage: updateLoanOpeningMessage,
        getLoanOpeningBalance: getLoanOpeningBalance,
        updateAssetOpeningMessage: updateAssetOpeningMessage,
        getAssetOpeningBalance: getAssetOpeningBalance,
        getInvestmentAccount: getInvestmentAccount,
        addInvestmentAccountMessage: addInvestmentAccountMessage,
        updateInvestmentOpeningMessage: updateInvestmentOpeningMessage,
        getInvestmentOpeningBalance: getInvestmentOpeningBalance,
        updateTDSOpeningMessage: updateTDSOpeningMessage,
        getTDSOpeningBalance: getTDSOpeningBalance,
        getRCOpeningBalance: getRCOpeningBalance,
        updateRCOpeningMessage: updateRCOpeningMessage,
        updateStockOpeningMessage: updateStockOpeningMessage,
        getStockOpeningBalance: getStockOpeningBalance,
        getIncomeTaxOpeningBalance: getIncomeTaxOpeningBalance,
        updateIncomeTaxOpeningMessage: updateIncomeTaxOpeningMessage,
        updateInvestmentAccountMessage: updateInvestmentAccountMessage,
        passStockJournalMessage: passStockJournalMessage,
        getStockJournal: getStockJournal,
        updateEmployeeOpeningMessage: updateEmployeeOpeningMessage,
        getEmployeeOpeningBalance: getEmployeeOpeningBalance,
        ...createForms({
            login: InitialLogin,
            register: InitialRegister,
            addBusiness: InitailAddBusiness,
            businessChange: InitialBusinessChange,
            addSubUser: InitialSubUser,
            addFinancialYear: InitialFinancialYear,
            addCustomer: InitialAddCustomer,
            customerChange: InitialAddCustomer,
            addVendor: InitialAddCustomer,
            vendorChange: InitialAddCustomer,
            addBankDetail: InitialBankDetail,
            bankDetailChange: InitialBankDetail,
            addItemForm: InitialItem,
            itemChange: InitialItem,
            markattendancee: MarkAttendancee,
            addExpenseHead: InitialAddExpenseHead,
            addIncomeHead: InitialAddIncomeHead,
            addLoanDetail: InitailAddLoanDetail,
            addAssetDetail: InitialAddAssetDetail,
            addInvestmentDetail: InitialAddInvestmentDetail,
            updateInvestmentDetail: InitialAddInvestmentDetail
        })
    }), persistedState, applyMiddleware(thunk, logger));

    store.subscribe(throttle(() => {
        saveState({
            businessList: store.getState().businessList,
            getItem: store.getState().getItem,
            getVendor: store.getState().getVendor,
            getBankDetail: store.getState().getBankDetail,
            getCustomer: store.getState().getCustomer,
            individualBusinessMessage: store.getState().individualBusinessMessage,
            salesDetail: store.getState().salesDetail,
            purchaseDetail: store.getState().purchaseDetail,
            getExpense: store.getState().getExpense,
            getPayment: store.getState().getPayment,
            getBankEntry: store.getState().getBankEntry,
            getOpeningClosing: store.getState().getOpeningClosing,
            getOpeningBalance: store.getState().getOpeningBalance,
            getCashInHand: store.getState().getCashInHand,
            getEmployee: store.getState().getEmployee,
            getGSTOpeningBalance: store.getState().getGSTOpeningBalance,
            getDebitCredit: store.getState().getDebitCredit,
            getExpenseHead: store.getState().getExpenseHead,
            getIncomeOS: store.getState().getIncomeOS,
            getBankOpeningBalance: store.getState().getBankOpeningBalance,
            getLoan: store.getState().getLoan,
            getLoanAccount: store.getState().getLoanAccount,
            getInvestment: store.getState().getInvestment,
            getIncomeHead: store.getState().getIncomeHead,
            getAssetAccount: store.getState().getAssetAccount,
            getDepreciation: store.getState().getDepreciation,
            getLoanOpeningBalance: store.getState().getLoanOpeningBalance,
            getAssetOpeningBalance: store.getState().getAssetOpeningBalance,
            getInvestmentAccount: store.getState().getInvestmentAccount,
            getInvestmentOpeningBalance: store.getState().getInvestmentOpeningBalance,
            getTDSOpeningBalance: store.getState().getTDSOpeningBalance,
            getRCOpeningBalance: store.getState().getRCOpeningBalance,
            getStockOpeningBalance: store.getState().getStockOpeningBalance,
            getIncomeTaxOpeningBalance: store.getState().getIncomeTaxOpeningBalance,
            getStockJournal: store.getState().getStockJournal,
            getEmployeeOpeningBalance: store.getState().getEmployeeOpeningBalance
        });
    }, 1000));
    return store;
}
