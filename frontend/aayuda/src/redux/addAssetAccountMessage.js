import * as ACTION_TYPES from './actionTypes';

const initialState = {
    loading : false,
    message : '',
    error : '',
    state : {}
}

export const addAssetAccountMessage = function (state = initialState, action) {
    switch (action.type) {
        case ACTION_TYPES.ADD_ASSET_ACCOUNT_LOADING:
           return action.payload;
        case ACTION_TYPES.ADD_ASSET_ACCOUNT:
            return action.payload;
        default: return state;
    }
}