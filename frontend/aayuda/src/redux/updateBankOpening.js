import * as ACTION_TYPES from './actionTypes';

const initialState = {
    loading : false,
    message : "initial",
    disabled : false,
    error : '',
    state: {}
}

export const updateBankOpeningMessage = function (state = initialState, action) {
    switch(action.type) {
        case ACTION_TYPES.UPDATE_BANK_OPENING_LOADING :
            return action.payload;
        case ACTION_TYPES.UPDATE_BANK_OPENING :
            return action.payload;
        case ACTION_TYPES.UPDATE_BANK_OPENING_ERROR:
            return action.payload;    
        case ACTION_TYPES.RESETTING_BANK_OPENING_CHANGE:
            return action.payload;    
        default : return state;        
    }
}