import * as ACTION_TYPES from './actionTypes';

const initialstate = {
    loading: false,
    message: 'initial',
    data: [],
    error: ''
};

export const getStockOpeningBalance = function (state = initialstate, action) {
    switch (action.type) {
        case ACTION_TYPES.GET_STOCK_OPENING_BALANCE_LOADING:
            return action.payload;
        case ACTION_TYPES.GET_STOCK_OPENING_BALANCE:
            return action.payload;
        case ACTION_TYPES.REFRESH_STOCK_OPENING_BALANCE:
            return ({
                loading: false,
                message: 'initial',
                data: [],
                error: ''
            })
        case ACTION_TYPES.PUSH_STOCK_OPENING_BALANCE:
            let stock = action.payload.state.stock;
            let itemid = action.payload.state.itemid;
            let businessid = action.payload.businessid;
            let fyid = action.payload.fyid;
            let da = state.data;
            let existingdata = da.filter(d => d.itemid === itemid);
            if (existingdata.length === 0) {
                da.push({
                    businessid: businessid,
                    itemid: itemid,
                    fyid: fyid,
                    stock: stock
                })
            }
            else da.map(d => d.itemid === itemid ? d.stock = stock : d.stock = d.stock);
            return ({
                loading: false,
                message: '',
                data: da,
                error: '',
                state: action.payload.state
            })
        default: return state;
    }
}