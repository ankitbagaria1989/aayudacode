import * as ACTION_TYPES from './actionTypes';

const initialState = {
    loading : false,
    message : "initial",
    disabled : true,
    error : ''
}

export const updateItemMessage = function (state = initialState, action) {
    switch(action.type) {
        case ACTION_TYPES.UPDATE_ITEM_LOADING :
            return action.payload;
        case ACTION_TYPES.UPDATE_ITEM :
            return action.payload;
        case ACTION_TYPES.UPDATE_ITEM_ERROR:
            return action.payload;    
        case ACTION_TYPES.RESETTING_ITEM_CHANGE:
            return action.payload;    
        default : return state;        
    }

}