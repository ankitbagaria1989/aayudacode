import * as ACTION_TYPES from './actionTypes';
import { sortSalesData } from './sortArrayofObjects';

const initialState = {
    loading: false,
    message: 'initial',
    data: [],
    error: '',
    invoicenumber: '',
    invoicedate: ''
};

export const salesDetail = function (state = initialState, action) {
    switch (action.type) {
        case ACTION_TYPES.GET_SALES_DATA:
            return action.payload;
        case ACTION_TYPES.GET_SALES_DATA_LOADING:
            return action.payload;
        case ACTION_TYPES.PUSH_CHECK_SALES_DATA:
            let existingSales = state.data;
            let newSales = action.payload.data;
            let present = false;
            existingSales.forEach(s => {
                if (s.invoiceid === newSales.invoiceid) {
                    present = true;
                }
            })
            if (present === false) {
                existingSales.push(newSales);
                existingSales.sort(sortSalesData);
                return ({
                    loading: false,
                    message: '',
                    data: existingSales,
                    error: '',
                    invoicenumber: action.payload.invoicenumber,
                    invoicedate: action.payload.invoicedate,
                    previousinvnum: action.payload.previousinvnum
                })
            }
            else return {
                ...state,
                previousinvnum: action.payload.previousinvnum,
            }

        case ACTION_TYPES.PUSH_SALES_DATA:
            let existingSales1 = state.data;
            let newSales1 = action.payload.sales;
            existingSales1.push(newSales1);
            existingSales1.sort(sortSalesData);
            return ({
                loading: false,
                message: '',
                data: existingSales1,
                error: '',
                invoicenumber: action.payload.invoicenumber,
                invoicedate: action.payload.invoicedate
            })
        case ACTION_TYPES.CANCEL_INVOICE:
            let data = state.data;
            data = data.map(d => {
                if ((d.businessid === action.payload.businessid) && (d.financialyearid === action.payload.fyid) && (d.invoiceid === action.payload.invoiceid)) {
                    d.active = 1;
                }
                return d;
            })
            return ({
                ...state,
                data: data
            })
        case ACTION_TYPES.RESTORE_INVOICE:
            let da = state.data;
            da = da.map(d => {
                if ((d.businessid === action.payload.businessid) && (d.financialyearid === action.payload.fyid) && (d.invoiceid === action.payload.invoiceid)) {
                    d.active = 0;
                }
                return d;
            })
            return ({
                ...state,
                data: da
            })
        case ACTION_TYPES.UPDATE_INVOICE:
            let newd = action.payload.data;
            let invoiceid = action.payload.invoiceid;
            let d = state.data;
            d = d.filter(x => x.invoiceid !== invoiceid);
            d.push(newd);
            d.sort(sortSalesData);
            return ({
                ...state,
                error: '',
                message: '',
                loading: false,
                data: d
            })
        case ACTION_TYPES.ADJUST_ASSET_SALES_DATA:
            let existingSales2 = state.data;
            let newSales2 = action.payload.sales;
            existingSales2.push(newSales2);
            existingSales2.sort(sortSalesData);
            return ({
                loading: false,
                message: '',
                data: existingSales2,
                error: '',
                invoicenumber: action.payload.invoicenumber,
                invoicedate: action.payload.invoicedate
            })
        default: return state;
    }
}

