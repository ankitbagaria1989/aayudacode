import * as ACTION_TYPES from './actionTypes';
import { baseUrl } from './baseUrl';
import { actions } from 'react-redux-form';
import { sortInvoice, sortVoucherno } from './sortArrayofObjects';

const retry = require('cross-fetch');
const fetchRetry = require('fetch-retry')(retry, {
    retries: 3,
    retryDelay: 1000,
    retryOn: function (attempt, error, response) {
        if (error !== null || response.status >= 500) {
            if (attempt < 2) return true
            else return false
        }
    }
});

export const postLoginUser = function (email, password, dispatch) {
    const user = {
        email: email,
        password: password
    };
    dispatch({
        type: ACTION_TYPES.LOGIN_LOADING,
        payload: {
            message: '',
            disabled: true,
            loading: true,
            error: ''
        }
    });
    fetchRetry(baseUrl + 'login', {
        method: "post",
        body: JSON.stringify(user),
        headers: {
            "Content-Type": "application/json",
        },
        credentials: "include"
    })
        .then(response => response.json())
        .then(dataObject => dispatch(loginMessage(dataObject)))
        .catch(err => dispatch(loginError(err.message)))
}

const loginMessage = function (dataObject) {
    if (dataObject.result === 'success') {
        return ({
            type: ACTION_TYPES.LOGIN_MESSAGE,
            payload: {
                message: dataObject.result,
                error: '',
                disabled: false,
                loading: false
            }
        });
    }
    else {
        return ({
            type: ACTION_TYPES.LOGIN_MESSAGE,
            payload: {
                message: '',
                error: dataObject.result,
                disabled: false,
                loading: false
            }
        });
    }
}

const loginError = function (message) {
    return ({
        type: ACTION_TYPES.LOGIN_MESSAGE,
        payload: {
            error: "Error" + message,
            disabled: false,
            loading: false,
            message: ''
        }
    });
}

export const postRegisterUser = function (username, email, password, dispatch) {
    const user = {
        username: username,
        email: email,
        password: password
    }
    dispatch({
        type: ACTION_TYPES.REGISTER_LOADING,
        payload: {
            message: '',
            disabled: true,
            loading: true,
            error: ''
        }
    });
    return fetch(baseUrl + 'register', {
        method: "post",
        body: JSON.stringify(user),
        headers: {
            "Content-Type": "application/json"
        },
        credentials: "include"
    })
        .then((response) => response.json())
        .then((dataObject) => dispatch(registerMessage(dataObject)))
        .catch(err => dispatch({
            type: ACTION_TYPES.REGISTER_MESSAGE,
            payload: {
                error: "Error" + err.message,
                disabled: false,
                loading: false,
                message: ''
            }
        }))
}

const registerMessage = function (dataObject) {
    if (dataObject.result === 'success') {
        return ({
            type: ACTION_TYPES.REGISTER_MESSAGE,
            payload: {
                message: dataObject.result,
                disabled: false,
                loading: false,
                error: ''
            }
        });
    }
    else {
        return ({
            type: ACTION_TYPES.REGISTER_MESSAGE,
            payload: {
                error: dataObject.result,
                disabled: false,
                loading: false,
                message: ''
            }
        });
    }
}

export const getBusinessList = function (dispatch) {
    dispatch({
        type: ACTION_TYPES.BUSINESS_LIST_LOADING,
        payload: {
            loading: true,
            result: '',
            data: [],
            error: ''
        }
    });
    return fetchRetry(baseUrl + 'business', {
        credentials: "include"
    })
        .then((response) => response.json())
        .then((dataObject) => {
            dispatch(businessList(dataObject.result, dataObject.data));
        })
        .catch(err => {
            dispatch({
                type: ACTION_TYPES.BUSINESS_LIST,
                payload: {
                    result: err.message
                }
            });
        })
}

const businessList = function (result, data) {
    return (
        {
            type: ACTION_TYPES.BUSINESS_LIST,
            payload: {
                result: result,
                data: data
            }
        }
    );
}

export const refreshBusinessList = function (dispatch) {
    dispatch({
        type: ACTION_TYPES.BUSINESS_LIST_RESET,
        payload: {
            error: '',
            message: 'initial',
            loading: false,
            data: []
        }
    })
}

export const postAddBusiness = function (businessname, alias, gstin, state, address, pincode, mobile, landline, email, stateut, pan, businesstype, noofshares, fv, dispatch) {
    dispatch({
        type: ACTION_TYPES.ADD_BUSINESS_LOADING,
        payload: {
            message: '',
            disabled: true,
            loading: true,
            error: ''
        }
    });
    let business = {
        businessname: businessname,
        alias: alias,
        gstin: gstin,
        state: state,
        address: address,
        pincode: pincode,
        landline: landline,
        mobile: mobile,
        email: email,
        stateut: stateut,
        pan: pan,
        businesstype: businesstype,
        noofshares: noofshares,
        fv: fv
    };
    return fetchRetry(baseUrl + 'business', {
        method: "post",
        body: JSON.stringify(business),
        headers: {
            "Content-Type": "application/json",
        },
        credentials: "include"
    })
        .then((response) => response.json())
        .then((dataObject) => {
            if (dataObject.result === 'success') {
                business.businessid = dataObject.id.businessid;
                business.fyid = null;
                business.financialyear = null;
                dispatch({
                    type: ACTION_TYPES.ADD_BUSINESS,
                    payload: {
                        message: dataObject.result,
                        error: '',
                        loading: false
                    }
                });
                dispatch({
                    type: ACTION_TYPES.PUSH_NEW_BUSINESS,
                    payload: {
                        data: business
                    }
                })
            }
            else if (dataObject.result === 'Already Registered') {
                dispatch({
                    type: ACTION_TYPES.PUSH_CHECK_BUSINESS,
                    payload: {
                        data: dataObject.data,
                        gstin: gstin
                    }
                })
                dispatch({
                    type: ACTION_TYPES.ADD_BUSINESS,
                    payload: {
                        message: dataObject.result,
                        error: '',
                        state: business,
                        loading: false
                    }
                });
            }
            else {
                dispatch({
                    type: ACTION_TYPES.ADD_BUSINESS,
                    payload: {
                        error: dataObject.result,
                        message: '',
                        state: business,
                        loading: false
                    }
                });
            }
        })
        .catch(err => dispatch({
            type: ACTION_TYPES.ADD_BUSINESS,
            payload: {
                message: '',
                error: err.message,
                state: business,
                loading: false
            }
        }))
}

export const resetAddBusinessMessage = function (dispatch) {
    dispatch({
        type: ACTION_TYPES.ADD_BUSINESS,
        payload: {
            message: 'initial',
            disabled: false,
            loading: false,
            error: '',
            state: {}
        }
    })
}

export const resetBusinessFormAndMessage = function (dispatch) {
    dispatch({
        type: ACTION_TYPES.ADD_BUSINESS_LOADING,
        payload: {
            message: '',
            disabled: false,
            loading: false,
            error: ''
        }
    })
    dispatch(actions.reset('addBusiness'));
}

export const postAddSubUser = function (subuseremail, write, update, del, dispatch) {
    dispatch({
        type: ACTION_TYPES.ADD_SUB_USER_LOADING,
        payload: {
            message: '',
            loading: true,
            disabled: true
        }
    })
    return fetchRetry(baseUrl + 'subuser', {
        method: "post",
        body: JSON.stringify({ email: subuseremail }),
        headers: {
            "Content-Type": "application/json",
        },
        credentials: "include"
    })
        .then((response) => response.json())
        .then((dataObject) => dispatch(addSubUserMessage(dataObject.result)))
        .catch(err => {
            dispatch(addSubUserMessage("Error" + err.message));
        })
}

const addSubUserMessage = function (result) {
    return ({
        type: ACTION_TYPES.ADD_SUB_USER_MESSAGE,
        payload: {
            message: result,
            loading: false,
            disabled: false
        }
    });

}

export const resetAddSubUserMessage = function (dispatch) {
    dispatch({
        type: ACTION_TYPES.ADD_SUB_USER_LOADING,
        payload: {
            message: '',
            loading: false,
            disabled: false
        }
    })
}

export const getSubUserList = function (dispatch) {
    dispatch({
        type: ACTION_TYPES.SUB_USER_LIST_LOADING,
        payload: {
            loading: true,
            message: '',
            data: []
        }
    });
    return fetchRetry(baseUrl + 'subuser', {
        credentials: "include"
    })
        .then((response) => response.json())
        .then((dataObject) => { dispatch(subUserList(dataObject)); })
        .catch((err) => dispatch({
            type: ACTION_TYPES.SUB_USER_LIST,
            payload: {
                loading: false,
                result: "Error : " + err.message,
                data: []
            }
        }))
}

const subUserList = function (dataObject) {
    return (
        {
            type: ACTION_TYPES.SUB_USER_LIST,
            payload: {
                loading: false,
                result: dataObject.result,
                data: dataObject.data
            }
        }
    )
}

export const deleteSubUser = function (email, dispatch) {
    dispatch({
        type: ACTION_TYPES.DELETE_SUB_USER_LOADING,
        payload: {
            loading: true,
            message: ''
        }
    });
    return fetchRetry(baseUrl + 'subuser', {
        method: "DELETE",
        body: JSON.stringify({ email: email }),
        headers: {
            "Content-Type": "application/json"
        },
        credentials: "include"
    })
        .then((response) => response.json())
        .then((dataObject) => {
            dispatch(subUser(dataObject));
        })
        .catch(err => dispatch({
            type: ACTION_TYPES.DELETE_SUB_USER,
            payload: {
                result: "Error : " + err.message
            }
        }));
}

const subUser = function (dataObject) {
    return ({
        type: ACTION_TYPES.DELETE_SUB_USER,
        payload: {
            result: dataObject.result
        }
    });
}

export const addFinancialYear = function (financialyear, businessid, dispatch) {
    dispatch({
        type: ACTION_TYPES.ADD_FINANCIAL_YEAR_LOADING,
        payload: {
            loading: true,
            message: '',
            diabled: true,
            error: ''
        }
    });
    let obj = {
        aprilopening: 0,
        aprilclosing: 0,
        mayopening: 0,
        mayclosing: 0,
        juneopening: 0,
        juneclosing: 0,
        julyopening: 0,
        julyclosing: 0,
        augustopening: 0,
        augustclosing: 0,
        septemberopening: 0,
        septemberclosing: 0,
        octoberopening: 0,
        octoberclosing: 0,
        novemberopening: 0,
        novemberclosing: 0,
        decemberopening: 0,
        decemberclosing: 0,
        januaryopening: 0,
        januaryclosing: 0,
        feburaryopening: 0,
        feburaryclosing: 0,
        marchopening: 0,
        marchclosing: 0
    }
    return fetchRetry(baseUrl + 'fy', {
        method: "POST",
        body: JSON.stringify({ businessid: businessid, financialyear: financialyear, obj: JSON.stringify(obj) }),
        headers: {
            "Content-Type": "application/json"
        },
        credentials: "include"
    })
        .then((response) => response.json())
        .then((dataObject) => {
            if (dataObject.result === "success") {
                dispatch({
                    type: ACTION_TYPES.PUSH_NEW_FY,
                    payload: {
                        fyid: dataObject.id.fyid,
                        businessid: businessid,
                        fy: financialyear,
                        error: '',
                        message: ''
                    }
                })
                dispatch(financialYear(dataObject));
            }
            else if (dataObject.result === 'Already Registered') {
                dispatch({
                    type: ACTION_TYPES.PUSH_CHECK_FY,
                    payload: {
                        data: dataObject.data,
                        fy: financialyear,
                        error: '',
                        message: ''
                    }
                })
                dispatch(financialYear(dataObject));
            }
            else {
                dispatch({
                    type: ACTION_TYPES.ADD_FINANCIAL_YEAR,
                    payload: {
                        message: '',
                        loading: false,
                        disable: false,
                        error: "Error : " + dataObject.result
                    }
                })
            }
        })
        .catch((err) => dispatch({
            type: ACTION_TYPES.ADD_FINANCIAL_YEAR,
            payload: {
                message: '',
                loading: false,
                disable: false,
                error: "Error : " + err.message
            }
        }))
}

const financialYear = function (dataObject) {
    return ({
        type: ACTION_TYPES.ADD_FINANCIAL_YEAR,
        payload: {
            message: dataObject.result,
            loading: false,
            disable: false,
            error: ''
        }
    });
}

export const resetaddFinancialYear = function (dispatch) {
    dispatch({
        type: ACTION_TYPES.RESET_FINANCIAL_YEAR_MESSAGE,
        payload: {}
    })
}
export const resetaddFinancialYearMessage = function (dispatch) {
    dispatch({
        type: ACTION_TYPES.RESET_FINANCIAL_YEAR_MESSAGE,
        payload: {}
    })
    dispatch(actions.reset('addFinancialYear'));
}
export const updateBusiness = function (businessid, financial, businessname, alias, gstin, state, address, pincode, mobile, landline, email, pan, businesstype, noofshares, fv, dispatch) {
    dispatch({
        type: ACTION_TYPES.UPDATE_BUSINESS_LOADING,
        payload: {
            loading: true,
            disabled: true,
            message: '',
            error: ''
        }
    });
    let business = {
        businessid: businessid,
        businessname: businessname,
        alias: alias,
        gstin: gstin,
        state: state,
        address: address,
        pincode: pincode,
        mobile: mobile,
        landline: landline,
        email: email,
        pan: pan,
        stateut: "state",
        businesstype: businesstype,
        noofshares: noofshares,
        fv: fv
    };
    return fetchRetry(baseUrl + 'business', {
        method: "PUT",
        body: JSON.stringify(business),
        headers: {
            "Content-Type": "application/json"
        },
        credentials: "include"
    })
        .then(response => response.json())
        .then(dataObject => {
            if (dataObject.result === 'success') {
                business.financial = financial;
                dispatch({
                    type: ACTION_TYPES.DELETE_AND_INSERT_BUSINESS,
                    payload: {
                        business: business
                    }
                })
                dispatch({
                    type: ACTION_TYPES.UPDATE_BUSINESS,
                    payload: {
                        loading: false,
                        message: dataObject.result,
                        disabled: false,
                        error: '',
                        state: business
                    }
                })
            }
            else {
                dispatch({
                    type: ACTION_TYPES.UPDATE_BUSINESS,
                    payload: {
                        loading: false,
                        error: dataObject.result,
                        disabled: false,
                        message: '',
                        state: business
                    }
                })
            }
        })
        .catch(err => dispatch({
            type: ACTION_TYPES.UPDATE_BUSINESS,
            payload: {
                loading: false,
                error: "Error : " + err.message,
                disabled: false,
                message: '',
                state: business
            }
        }))
}

export const resetBusinessChangeMessage = function (dispatch) {
    dispatch({
        type: ACTION_TYPES.RESETTING_BUSINESS_CHANGE,
        payload: {
            message: 'initial',
            loading: false,
            disabled: false,
            error: '',
            state: {}
        }
    });
}

export const addCustomer = function (customerDetail, businessid, dispatch) {
    dispatch({
        type: ACTION_TYPES.ADD_CUSTOMER_LOADING,
        payload: {
            loading: true,
            disabled: true,
            message: '',
            error: ''
        }
    });
    const data = {
        customerdetail: customerDetail
    }
    return fetchRetry(baseUrl + 'customer/' + businessid, {
        method: "POST",
        body: JSON.stringify(data),
        headers: {
            "Content-Type": "application/json"
        },
        credentials: "include"
    })
        .then(response => response.json())
        .then(dataObject => {
            if (dataObject.result === 'success') {
                let c = { ...customerDetail };
                c.customerid = dataObject.id.customerid;
                c.businessid = businessid;
                dispatch({
                    type: ACTION_TYPES.PUSH_NEW_CUSTOMER,
                    payload: {
                        customerdetail: c,
                        loading: false,
                        error: '',
                        message: '',
                    }
                })
                dispatch({
                    type: ACTION_TYPES.ADD_CUSTOMER,
                    payload: {
                        loading: false,
                        disabled: false,
                        message: dataObject.result,
                        error: ''
                    }
                })
            }
            else {
                dispatch({
                    type: ACTION_TYPES.ADD_CUSTOMER,
                    payload: {
                        loading: false,
                        disabled: false,
                        message: '',
                        error: dataObject.result
                    }
                })
            }
        })
        .catch((err) => dispatch({
            type: ACTION_TYPES.ADD_CUSTOMER,
            payload: {
                loading: false,
                disabled: false,
                error: "Error : " + err.message,
                message: ''
            }

        }))
}

export const getCustomerList = function (businessid, dispatch) {
    dispatch({
        type: ACTION_TYPES.GET_CUSTOMER_LOADING,
        payload: {
            loading: true,
            message: '',
            data: [],
            error: ''
        }
    });
    return fetchRetry(baseUrl + 'customer/' + businessid, {
        credentials: "include"
    })
        .then(response => response.json())
        .then(dataObject => {
            if (dataObject.result === 'success') {
                dispatch({
                    type: ACTION_TYPES.GET_CUSTOMER,
                    payload: {
                        loading: false,
                        message: dataObject.result,
                        data: dataObject.data,
                        error: ''
                    }
                })
            }
            else {
                dispatch({
                    type: ACTION_TYPES.GET_CUSTOMER,
                    payload: {
                        loading: false,
                        error: "Error : " + dataObject.result,
                        data: [],
                        message: ''
                    }
                })
            }

        })
        .catch(err => {
            dispatch({
                type: ACTION_TYPES.GET_CUSTOMER_ERROR,
                payload: {
                    data: [],
                    error: "Error" + err.message,
                    loading: false,
                    message: ''
                }
            });
        })
}

export const refreshCustomerState = function (dispatch) {
    dispatch({
        type: ACTION_TYPES.REFRESH_CUSTOMER,
        payload: {}
    })
}

export const refreshCustomerStateSecond = function (dispatch) {
    dispatch({
        type: ACTION_TYPES.REFRESH_CUSTOMER_SECOND,
        payload: {}
    })
}

export const refreshVendorState = function (dispatch) {
    dispatch({
        type: ACTION_TYPES.REFRESH_VENDOR,
        payload: {}
    })
}

export const refreshVendorStateSecond = function (dispatch) {
    dispatch({
        type: ACTION_TYPES.REFRESH_VENDOR_SECOND,
        payload: {}
    })
}

export const refreshBankDetailStateSecond = function (dispatch) {
    dispatch({
        type: ACTION_TYPES.REFRESH_BANK_DETAIL_SECOND,
        payload: {}
    })
}

export const refreshItemState = function (dispatch) {
    dispatch({
        type: ACTION_TYPES.REFRESH_ITEM,
        payload: {}
    })
}
export const refreshItemStateSecond = function (dispatch) {
    dispatch({
        type: ACTION_TYPES.REFRESH_ITEM_SECOND,
        payload: {}
    })
}

export const refreshBankState = function (dispatch) {
    dispatch({
        type: ACTION_TYPES.REFRESH_BANK,
        payload: {}
    })
}

export const resetCVIB = function (dispatch) {
    refreshItemState(dispatch);
    refreshVendorState(dispatch);
    refreshCustomerState(dispatch);
    refreshBankState(dispatch);
}

export const consumeIndividualBusinessMessage = function (dispatch) {
    dispatch({
        type: ACTION_TYPES.CONSUME_INDIVIDUAL_BUSINESS,
        payload: {}
    })
}

export const getIndividualBusiness = function (businessid, dispatch) {
    dispatch({
        type: ACTION_TYPES.LOADING_INDIVIDUAL_BUSINESS,
        payload: {}
    })
    return fetchRetry(baseUrl + 'business/' + businessid, {
        credentials: "include"
    })
        .then(response => response.json())
        .then(dataObject => {
            if (dataObject.result === 'success') {
                dispatch({
                    type: ACTION_TYPES.ADD_INDIVIDUAL_BUSINESS,
                    payload: {
                        data: dataObject.data,
                        businessid: businessid
                    }
                })
                dispatch({
                    type: ACTION_TYPES.INDIVIDUAL_BUSINESS,
                    payload: {
                        loading: false,
                        error: '',
                        message: dataObject.result
                    }
                })
            }
            else {
                dispatch({
                    type: ACTION_TYPES.INDIVIDUAL_BUSINESS,
                    payload: {
                        loading: false,
                        error: dataObject.result,
                        message: ''
                    }
                })
            }
        })
        .catch(err => dispatch({
            type: ACTION_TYPES.INDIVIDUAL_BUSINESS,
            payload: {
                loading: false,
                error: err.message,
                message: ''
            }
        }))
}

export const resetIndividualBusinessMessage = function (dispatch) {
    dispatch({
        type: ACTION_TYPES.RESET_INDIVIDUAL_BUSINESS,
        payload: {
            loading: false,
            error: '',
            message: 'initial'
        }
    })
}

export const updateCustomer = function (businessid, customerid, values, dispatch) {
    dispatch({
        type: ACTION_TYPES.UPDATE_CUSTOMER_LOADING,
        payload: {
            loading: true,
            message: '',
            disabled: true,
            error: ''
        }
    });
    let data = {
        customerid: customerid,
        customerdetail: values
    }
    return fetchRetry(baseUrl + 'customer/' + businessid, {
        method: "PUT",
        body: JSON.stringify(data),
        headers: {
            "Content-Type": "application/json"
        },
        credentials: "include"
    })
        .then(response => response.json())
        .then(dataObject => {
            if (dataObject.result === 'success') {
                let custdetail = { ...values };
                custdetail.customerid = customerid;
                custdetail.businessid = businessid
                dispatch({
                    type: ACTION_TYPES.DELETE_AND_PUSH_CUSTOMER,
                    payload: {
                        customerdetail: custdetail
                    }
                })
                dispatch({
                    type: ACTION_TYPES.UPDATE_CUSTOMER,
                    payload: {
                        loading: false,
                        disabled: false,
                        message: dataObject.result,
                        error: ''
                    }
                })
            }
            else {
                dispatch({
                    type: ACTION_TYPES.UPDATE_CUSTOMER,
                    payload: {
                        loading: false,
                        disabled: false,
                        error: dataObject.result,
                        message: ''
                    }
                })
            }
        })
        .catch(err => dispatch({
            type: ACTION_TYPES.UPDATE_CUSTOMER_ERROR,
            payload: {
                loading: false,
                disabled: false,
                error: "Error : " + err.message,
                message: ''
            }
        }))

}

export const resetCustomerChange = function (dispatch) {
    dispatch({
        type: ACTION_TYPES.RESETTING_CUSTOMER_CHANGE,
        payload: {
            message: 'initial',
            loading: false,
            disabled: false,
            error: ''
        }
    });
}

export const resetAddCustomerMessage = function (dispatch) {
    dispatch({
        type: ACTION_TYPES.RESET_CUSTOMER_MESSAGE,
        payload: {}
    })
}

export const resetAddCustomerMessageAndForm = function (dispatch) {
    dispatch({
        type: ACTION_TYPES.RESET_CUSTOMER_MESSAGE,
        payload: {}
    });
    dispatch(actions.reset('addCustomer'));
}

export const resetAddItemMessage = function (dispatch) {
    dispatch({
        type: ACTION_TYPES.RESET_ITEM_MESSAGE,
        payload: {}
    })
}

export const resetAddItemMessageAndForm = function (dispatch) {
    dispatch({
        type: ACTION_TYPES.RESET_ITEM_MESSAGE,
        payload: {}
    });
    dispatch(actions.reset('addItemForm'));
}

export const resetAddBankMessageAndForm = function (dispatch) {
    dispatch({
        type: ACTION_TYPES.RESET_BANK_DETAIL_MESSAGE,
        payload: {}
    });
    dispatch(actions.reset('addBankDetail'));
}

export const resetAddVendorMessage = function (dispatch) {
    dispatch({
        type: ACTION_TYPES.RESET_VENDOR_MESSAGE,
        payload: {}
    })
}

export const resetAddVendorMessageAndForm = function (dispatch) {
    dispatch({
        type: ACTION_TYPES.RESET_VENDOR_MESSAGE,
        payload: {}
    });
    dispatch(actions.reset('addVendor'));
}

export const resetAddBankDetailMessage = function (dispatch) {
    dispatch({
        type: ACTION_TYPES.RESET_BANK_DETAIL_MESSAGE,
        payload: {}
    })
}

export const addVendor = function (vendorDetail, businessid, dispatch) {
    dispatch({
        type: ACTION_TYPES.ADD_VENDOR_LOADING,
        payload: {
            loading: true,
            disabled: true,
            message: '',
            error: ''

        }
    });
    const data = {
        vendordetail: vendorDetail
    }
    return fetchRetry(baseUrl + 'vendor/' + businessid, {
        method: "POST",
        body: JSON.stringify(data),
        headers: {
            "Content-Type": "application/json"
        },
        credentials: "include"
    })
        .then(response => response.json())
        .then(dataObject => {
            if (dataObject.result === 'success') {
                let ven = { ...vendorDetail };
                ven.businessid = businessid;
                ven.vendorid = dataObject.id.vendorid;
                dispatch({
                    type: ACTION_TYPES.PUSH_NEW_VENDOR,
                    payload: {
                        loading: false,
                        diabled: false,
                        message: '',
                        data: ven
                    }
                })
                dispatch({
                    type: ACTION_TYPES.ADD_VENDOR,
                    payload: {
                        loading: false,
                        disabled: false,
                        message: dataObject.result,
                        error: ''
                    }
                })
            }
            else {
                dispatch({
                    type: ACTION_TYPES.ADD_VENDOR,
                    payload: {
                        loading: false,
                        disabled: false,
                        error: dataObject.result,
                        message: ''
                    }
                })
            }
        })
        .catch((err) => dispatch({
            type: ACTION_TYPES.ADD_VENDOR,
            payload: {
                loading: false,
                disabled: false,
                error: err.message,
                message: ''
            }
        }))
}

export const getVendorList = function (businessid, dispatch) {
    dispatch({
        type: ACTION_TYPES.GET_VENDOR_LOADING,
        payload: {
            loading: true,
            message: '',
            data: [],
            error: ''
        }
    });
    return fetchRetry(baseUrl + 'vendor/' + businessid, {
        credentials: "include"
    })
        .then(response => response.json())
        .then(dataObject => {
            if (dataObject.result === "success") {
                dispatch({
                    type: ACTION_TYPES.GET_VENDOR,
                    payload: {
                        loading: false,
                        message: dataObject.result,
                        data: dataObject.data,
                        error: ''
                    }
                })
            }
            else {
                dispatch({
                    type: ACTION_TYPES.GET_VENDOR,
                    payload: {
                        loading: false,
                        data: [],
                        error: "Error : " + dataObject.result,
                        message: ''
                    }
                })
            }
        })
        .catch(err => {
            dispatch({
                type: ACTION_TYPES.GET_VENDOR_ERROR,
                payload: {
                    data: [],
                    error: "Error : " + err.message,
                    loading: false,
                    message: ''
                }
            });
        })
}

export const updateVendor = function (businessid, vendorid, values, dispatch) {
    dispatch({
        type: ACTION_TYPES.UPDATE_VENDOR_LOADING,
        payload: {
            loading: true,
            message: '',
            disabled: true,
            error: ''
        }
    });
    let data = {
        vendorid: vendorid,
        vendordetail: values
    }
    return fetchRetry(baseUrl + 'vendor/' + businessid, {
        method: "PUT",
        body: JSON.stringify(data),
        headers: {
            "Content-Type": "application/json"
        },
        credentials: "include"
    })
        .then(response => response.json())
        .then(dataObject => {
            if (dataObject.result === 'success') {
                let ven = { ...values };
                ven.vendorid = vendorid;
                ven.businessid = businessid;
                dispatch({
                    type: ACTION_TYPES.DELETE_AND_ADD_VENDOR,
                    payload: {
                        vendordetail: ven
                    }
                })
                dispatch({
                    type: ACTION_TYPES.UPDATE_VENDOR,
                    payload: {
                        loading: false,
                        disabled: false,
                        message: dataObject.result,
                        error: ''
                    }
                })
            }
            else {
                dispatch({
                    type: ACTION_TYPES.UPDATE_VENDOR,
                    payload: {
                        loading: false,
                        disabled: false,
                        error: dataObject.result,
                        message: ''
                    }
                })
            }
        })
        .catch(err => dispatch({
            type: ACTION_TYPES.UPDATE_VENDOR_ERROR,
            payload: {
                loading: false,
                disabled: false,
                error: err.message,
                message: ''
            }
        }))
}

export const resetVendorChange = function (msg, dispatch) {
    dispatch({
        type: ACTION_TYPES.RESETTING_VENDOR_CHANGE,
        payload: {
            message: 'initial',
            loading: false,
            disabled: false,
            error: ''
        }
    });
}

export const addBankDetail = function (bankDetail, businessid, dispatch) {
    dispatch({
        type: ACTION_TYPES.ADD_BANK_DETAIL_LOADING,
        payload: {
            loading: true,
            disabled: true,
            message: '',
            error: ''

        }
    });
    const data = {
        bankdetail: bankDetail
    }
    return fetchRetry(baseUrl + 'bankdetail/' + businessid, {
        method: "POST",
        body: JSON.stringify(data),
        headers: {
            "Content-Type": "application/json"
        },
        credentials: "include"
    })
        .then(response => response.json())
        .then(dataObject => {
            let bd = { ...bankDetail };
            bd.bankdetailid = dataObject.id.bankdetailid;
            bd.businessid = businessid;
            if (dataObject.result === 'success') {
                dispatch({
                    type: ACTION_TYPES.PUSH_NEW_BANK,
                    payload: {
                        bankdetail: bd
                    }
                })
                dispatch({
                    type: ACTION_TYPES.ADD_BANK_DETAIL,
                    payload: {
                        loading: false,
                        disabled: false,
                        message: dataObject.result,
                        error: ''
                    }
                })
            }
            else {
                dispatch({
                    type: ACTION_TYPES.ADD_BANK_DETAIL,
                    payload: {
                        loading: false,
                        disabled: false,
                        error: dataObject.result,
                        message: ''
                    }
                })
            }
        })
        .catch((err) => dispatch({
            type: ACTION_TYPES.ADD_BANK_DETAIL,
            payload: {
                loading: false,
                disabled: false,
                error: "Error : " + err.message,
                message: ''
            }
        }))
}

export const getBankDetailList = function (businessid, dispatch) {
    dispatch({
        type: ACTION_TYPES.GET_BANK_DETAIL_LOADING,
        payload: {
            loading: true,
            message: '',
            data: [],
            error: ''
        }
    });
    return fetchRetry(baseUrl + 'bankdetail/' + businessid, {
        credentials: "include"
    })
        .then(response => response.json())
        .then(dataObject => {
            if (dataObject.result === 'success') {
                dispatch({
                    type: ACTION_TYPES.GET_BANK_DETAIL,
                    payload: {
                        loading: false,
                        message: dataObject.result,
                        data: dataObject.data,
                        error: ''
                    }
                })
            }
            else {
                dispatch({
                    type: ACTION_TYPES.GET_BANK_DETAIL,
                    payload: {
                        loading: false,
                        message: '',
                        data: [],
                        error: "Error : " + dataObject.result
                    }
                })
            }
        })
        .catch(err => {
            dispatch({
                type: ACTION_TYPES.GET_BANK_DETAIL_ERROR,
                payload: {
                    data: [],
                    error: "Error : " + err.message,
                    loading: false,
                    message: ''
                }
            });
        })
}

export const updateBankDetail = function (businessid, bankdetailid, values, dispatch) {
    dispatch({
        type: ACTION_TYPES.UPDATE_BANK_DETAIL_LOADING,
        payload: {
            loading: true,
            message: '',
            disabled: true,
            error: ''
        }
    });
    let data = {
        bankdetailid: bankdetailid,
        bankdetail: values
    }
    return fetchRetry(baseUrl + 'bankdetail/' + businessid, {
        method: "PUT",
        body: JSON.stringify(data),
        headers: {
            "Content-Type": "application/json"
        },
        credentials: "include"
    })
        .then(response => response.json())
        .then(dataObject => {
            if (dataObject.result === 'success') {
                let bankD = { ...values };
                bankD.businessid = businessid;
                bankD.bankdetailid = bankdetailid;
                dispatch({
                    type: ACTION_TYPES.DELETE_AND_PUSH_BANK_DETAIL,
                    payload: {
                        bankDetail: bankD
                    }
                })
                dispatch({
                    type: ACTION_TYPES.UPDATE_BANK_DETAIL,
                    payload: {
                        loading: false,
                        disabled: false,
                        message: dataObject.result,
                        error: ''
                    }
                })
            }
            else {
                dispatch({
                    type: ACTION_TYPES.UPDATE_BANK_DETAIL,
                    payload: {
                        loading: false,
                        disabled: false,
                        error: dataObject.result,
                        message: ''
                    }
                })
            }

        })
        .catch(err => dispatch({
            type: ACTION_TYPES.UPDATE_BANK_DETAIL_ERROR,
            payload: {
                loading: false,
                disabled: false,
                error: "Error " + err.message,
                message: ''
            }
        }))
}

export const resetBankDetailChange = function (dispatch) {
    dispatch({
        type: ACTION_TYPES.RESETTING_BANK_DETAIL_CHANGE,
        payload: {
            message: 'initiil',
            loading: false,
            disabled: false,
            error: ''
        }
    });
}

export const addItem = function (itemDetail, businessid, dispatch) {
    dispatch({
        type: ACTION_TYPES.ADD_ITEM_LOADING,
        payload: {
            loading: true,
            disabled: true,
            message: '',
            error: ''
        }
    });
    const data = {
        itemdetail: itemDetail
    }
    return fetchRetry(baseUrl + 'item/' + businessid, {
        method: "POST",
        body: JSON.stringify(data),
        headers: {
            "Content-Type": "application/json"
        },
        credentials: "include"
    })
        .then(response => response.json())
        .then(dataObject => {
            if (dataObject.result === 'success') {
                let item = { ...itemDetail };
                let itemid = dataObject.id.itemid;
                item = {
                    ...item,
                    businessid: businessid,
                    itemid: itemid,
                }
                dispatch({
                    type: ACTION_TYPES.PUSH_ITEM,
                    payload: {
                        itemDetail: item
                    }
                })
                dispatch({
                    type: ACTION_TYPES.ADD_ITEM,
                    payload: {
                        loading: false,
                        disabled: false,
                        message: dataObject.result,
                        error: ''
                    }
                })
            }
            else {
                dispatch({
                    type: ACTION_TYPES.ADD_ITEM,
                    payload: {
                        loading: false,
                        disabled: false,
                        error: dataObject.result,
                        message: ''
                    }
                })
            }
        })
        .catch((err) => dispatch({
            type: ACTION_TYPES.ADD_ITEM,
            payload: {
                loading: false,
                disabled: false,
                error: err.message,
                message: ''
            }
        }))
}

export const getItemList = function (businessid, dispatch) {
    dispatch({
        type: ACTION_TYPES.GET_ITEM_LOADING,
        payload: {
            loading: true,
            message: '',
            data: [],
            error: ''
        }
    });
    return fetchRetry(baseUrl + 'item/' + businessid, {
        credentials: "include"
    })
        .then(response => response.json())
        .then(dataObject => {
            if (dataObject.result === 'success') {
                dispatch({
                    type: ACTION_TYPES.GET_ITEM,
                    payload: {
                        loading: false,
                        message: dataObject.result,
                        data: dataObject.data,
                        error: ''
                    }
                })
            }
            else {
                dispatch({
                    type: ACTION_TYPES.GET_ITEM,
                    payload: {
                        loading: false,
                        message: '',
                        data: [],
                        error: dataObject.result
                    }
                })
            }
        })
        .catch(err => {
            dispatch({
                type: ACTION_TYPES.GET_ITEM_ERROR,
                payload: {
                    data: [],
                    error: err.message,
                    loading: false,
                    message: ''
                }
            });
        })
}

export const updateItem = function (businessid, itemid, values, dispatch) {
    dispatch({
        type: ACTION_TYPES.UPDATE_ITEM_LOADING,
        payload: {
            loading: true,
            message: '',
            disabled: true,
            error: ''
        }
    });
    let data = {
        itemid: itemid,
        itemdetail: values
    }
    return fetchRetry(baseUrl + 'item/' + businessid, {
        method: "PUT",
        body: JSON.stringify(data),
        headers: {
            "Content-Type": "application/json"
        },
        credentials: "include"
    })
        .then(response => response.json())
        .then(dataObject => {
            if (dataObject.result === 'success') {
                let item = { ...values };
                item.businessid = businessid;
                item.itemid = itemid;
                dispatch({
                    type: ACTION_TYPES.DELETE_AND_PUSH_ITEM,
                    payload: {
                        item: item
                    }
                })
                dispatch({
                    type: ACTION_TYPES.UPDATE_ITEM,
                    payload: {
                        loading: false,
                        disabled: false,
                        message: dataObject.result,
                        error: ''
                    }
                })
            }
        })
        .catch(err => dispatch({
            type: ACTION_TYPES.UPDATE_ITEM_ERROR,
            payload: {
                loading: false,
                disabled: false,
                error: "Error " + err.message,
                message: ''
            }
        }))

}

export const resetItemChange = function (dispatch) {
    dispatch({
        type: ACTION_TYPES.RESETTING_ITEM_CHANGE,
        payload: {
            message: 'initial',
            loading: false,
            disabled: false,
            error: ''
        }
    });
}

export const raiseInvoice = function (businessid, financialyearid, state, dispatch) {
    const invoiceInformation = {
        customerid: state.customerid.split("...")[0],
        shippedtoid: state.shippedtoid.split("...")[0],
        customername: state.customername,
        shippedtoname: state.shippedtoname,
        invoicedate: state.invoicedate,
        invoicenumber: parseInt(state.invoicenumber),
        ponumber: state.ponumber,
        podate: state.podate,
        transporter: state.transporter,
        vehiclenumber: state.vehiclenumber,
        grno: state.grno,
        destination: state.destination,
        ewaybillnumber: state.ewaybillnumber,
        freight: state.freight,
        terms: state.terms,
        itemno: state.itemno,
        totalbasic: state.totalamountbasic,
        totaligst: state.totaligst,
        totalcgst: state.totalcgst,
        totalsgst: state.totalsgst,
        totalvat: state.totalvat,
        totalexcise: state.totalexcise,
        grandtotal: state.grandtotal,
        invoicetype: state.invoicetype,
        active: 0,
        cash: state.cash,
        blamount: state.blamount
    }

    let item = {};
    for (let i = 0; i <= state.itemno; i++) {
        let itemobj = {
            ['name']: state['itemname' + i],
            ['desc']: state['itemdescription' + i],
            ['qty']: state['qty' + i],
            ['price']: state['price' + i],
            ['basic']: state['basic' + i],
            ['igst']: state['igst' + i],
            ['cgst']: state['cgst' + i],
            ['sgst']: state['sgst' + i],
            ['total']: state['total' + i]
        }
        item[i] = itemobj;
    }
    invoiceInformation.itemobject = JSON.stringify(item);
    dispatch({
        type: ACTION_TYPES.LOADING_ADD_INVOICE,
        payload: {}
    });
    fetchRetry(baseUrl + 'raiseinvoice/' + businessid + "/" + financialyearid, {
        method: "POST",
        body: JSON.stringify(invoiceInformation),
        headers: {
            "Content-Type": "application/json"
        },
        credentials: "include"
    })
        .then(response => response.json())
        .then(dataObject => {
            if ((dataObject.result === 'success')) {
                invoiceInformation.invoiceid = dataObject.id.invoiceid;
                invoiceInformation.businessid = businessid;
                invoiceInformation.financialyearid = financialyearid;
                invoiceInformation.type = "sales";
                if (invoiceInformation.cash === true) invoiceInformation.cash = "1";
                let invnum = parseInt(state.invoicenumber);
                invnum = invnum + 1;
                invnum = invnum.toString();
                if (invnum.length < 3) {
                    let l = 3 - invnum.length;
                    for (let i = 0; i < l; i++) {
                        invnum = '0' + invnum;
                    }
                }
                dispatch({
                    type: ACTION_TYPES.PUSH_SALES_DATA,
                    payload: {
                        sales: invoiceInformation,
                        invoicenumber: invnum,
                        invoicedate: new Date().toDateString().split(' ').slice(1).join(' ')
                    }
                })
                dispatch({
                    type: ACTION_TYPES.ADD_INVOICE,
                    payload: {
                        message: dataObject.result,
                        error: ''
                    }
                })
            }
            else if (dataObject.result === 'Already Registered') {
                let innum = parseInt(dataObject.data.invoicenumber);
                innum = innum + 1;
                innum = innum.toString();
                if (innum.length < 3) {
                    let l = 3 - innum.length;
                    for (let i = 0; i < l; i++) {
                        innum = '0' + innum;
                    }
                }
                dispatch({
                    type: ACTION_TYPES.PUSH_CHECK_SALES_DATA,
                    payload: {
                        data: dataObject.data,
                        previousinvnum: parseInt(state.invoicenumber),
                        invoicenumber: innum,
                        invoicedate: new Date().toDateString().split(' ').slice(1).join(' ')
                    }
                })
                dispatch({
                    type: ACTION_TYPES.ADD_INVOICE,
                    payload: {
                        message: dataObject.result,
                        error: ''
                    }
                })
            }
            else {
                dispatch({
                    type: ACTION_TYPES.ADD_INVOICE_ERROR,
                    payload: {
                        message: '',
                        error: "Error : " + dataObject.result,
                        state: state,
                        loading: false
                    }
                })
            }
        })
        .catch(err => dispatch({
            type: ACTION_TYPES.ADD_INVOICE_ERROR,
            payload: {
                error: "Error " + err.message,
                message: '',
                state: state,
                loading: false
            }
        }))
}
export const resetaddInvoiceMessage = function (message, dispatch) {
    dispatch({
        type: ACTION_TYPES.RESET_ADD_INVOICE_MESSAGE,
        payload: {}
    })
}

export const getSalesData = function (businessid, fyid, dispatch) {
    dispatch({
        type: ACTION_TYPES.GET_SALES_DATA_LOADING,
        payload: {
            loading: true,
            mesaage: '',
            error: '',
            data: [],
            invoicenumber: '',
            invoicedate: ''
        }
    })
    fetchRetry(baseUrl + 'raiseinvoice/' + businessid + "/" + fyid, {
        credentials: "include"
    })
        .then(response => response.json())
        .then(dataObject => {
            if (dataObject.result === 'success') {
                let invoiceData = dataObject.data;
                let invnum = "001";
                let date = new Date().toDateString().split(' ').slice(1).join(' ');
                if (dataObject.data.length > 0) {
                    invoiceData = invoiceData.sort(sortInvoice);
                    let invno = invoiceData[0].invoicenumber + 1;
                    invnum = invno.toString();
                    if (invnum.length < 3) {
                        let l = 3 - invnum.length;
                        for (let i = 0; i < l; i++) {
                            invnum = '0' + invnum;
                        }
                    }
                    invoiceData.forEach(d => {
                        let no = d.invoicenumber.toString();
                        if (no.length < 3) {
                            let l = 3 - no.length;
                            for (let i = 0; i < l; i++) {
                                no = '0' + no;
                            }
                            d.invoicenumber = no;
                        }
                    })
                }
                dispatch({
                    type: ACTION_TYPES.GET_SALES_DATA,
                    payload: {
                        message: dataObject.result,
                        data: invoiceData,
                        error: '',
                        loading: false,
                        invoicenumber: invnum,
                        invoicedate: date
                    }
                })
            }
            else {
                dispatch({
                    type: ACTION_TYPES.GET_SALES_DATA,
                    payload: {
                        message: '',
                        loading: false,
                        error: dataObject.result,
                        data: [],
                        invoicenumber: '',
                        invoicedate: ''
                    }
                })
            }
        })
        .catch(err => {
            dispatch({
                type: ACTION_TYPES.GET_SALES_DATA,
                payload: {
                    message: '',
                    loading: false,
                    error: err.message,
                    data: [],
                    invoicenumber: '',
                    invoicedate: ''
                }
            })
        })
}

export const resetSalesDetailSecond = function (dispatch) {
    dispatch({
        type: ACTION_TYPES.GET_SALES_DATA,
        payload: {
            message: '',
            loading: false,
            error: '',
            data: [],
            invoicenumber: '',
            invoicedate: ''
        }
    })
}

export const resetSalesDetail = function (dispatch) {
    dispatch({
        type: ACTION_TYPES.GET_SALES_DATA,
        payload: {
            message: 'initial',
            loading: false,
            error: '',
            data: [],
            invoicenumber: '',
            invoicedate: ''
        }
    })
}

export const raisePurchase = function (businessid, financialyearid, state, dispatch, getVendor) {
    const purchaseInformation = {
        vendorid: state.vendorid.split("...")[0],
        invoicedate: state.invoicedate,
        invoicenumber: state.invoicenumber,
        paymentduedate: state.paymentduedate,
        freight: state.freight,
        itemno: state.itemno,
        totalbasic: state.totalamountbasic,
        totaligst: state.totaligst,
        totalcgst: state.totalcgst,
        totalsgst: state.totalsgst,
        importduty: state.importduty,
        grandtotal: state.grandtotal,
        invoicetype: state.invoicetype,
        asset: state.asset
    }
    let item = {};
    for (let i = 0; i <= state.itemno; i++) {
        let itemobj = {
            ['name']: state['itemname' + i],
            ['qty']: state['qty' + i],
            ['price']: state['price' + i],
            ['basic']: state['basic' + i],
            ['igst']: state['igst' + i],
            ['cgst']: state['cgst' + i],
            ['sgst']: state['sgst' + i],
            ['vat']: state['vat' + i],
            ['excise']: state['excise' + i],
            ['total']: state['total' + i]
        }
        item[i] = itemobj;
    }
    purchaseInformation.itemobject = JSON.stringify(item);
    dispatch({
        type: ACTION_TYPES.LOADING_ADD_PURCHASE,
        payload: {}
    });
    fetchRetry(baseUrl + 'raisepurchase/' + businessid + "/" + financialyearid, {
        method: "POST",
        body: JSON.stringify(purchaseInformation),
        headers: {
            "Content-Type": "application/json"
        },
        credentials: "include"
    })
        .then(response => response.json())
        .then(dataObject => {
            if (dataObject.result === 'success') {
                purchaseInformation.purchaseid = dataObject.id.purchaseid;
                purchaseInformation.businessid = businessid;
                purchaseInformation.financilayearid = financialyearid;
                purchaseInformation.type = "purchase";
                dispatch({
                    type: ACTION_TYPES.PUSH_PURCHASE_DATA,
                    payload: {
                        purchase: purchaseInformation
                    }
                })
                dispatch({
                    type: ACTION_TYPES.ADD_PURCHASE,
                    payload: {
                        message: dataObject.result,
                        error: ''
                    }
                })
            }
            else if (dataObject.result === "Already Registered") {
                let vendorid = dataObject.data.vendorid;
                let vendorname;
                getVendor.data.forEach(v => {
                    if (v.vendorid === vendorid) vendorname = v.businessname;
                })
                dispatch({
                    type: ACTION_TYPES.PUSH_CHECK_PURCHASE_DATA,
                    payload: {
                        data: dataObject.data,
                        vendorname: vendorname,
                        invoicenumber: state.invoicenumber
                    }
                })
                dispatch({
                    type: ACTION_TYPES.ADD_PURCHASE,
                    payload: {
                        message: dataObject.result,
                        error: ''
                    }
                })
            }
            else {
                dispatch({
                    type: ACTION_TYPES.ADD_PURCHASE_ERROR,
                    payload: {
                        message: '',
                        error: "Error : " + dataObject.result,
                        state: state
                    }
                })
            }
        })
        .catch(err => dispatch({
            type: ACTION_TYPES.ADD_PURCHASE_ERROR,
            payload: {
                error: "Error " + err.message,
                message: '',
                state: state
            }
        }))
}
export const resetaddPurchaseMessage = function (message, dispatch) {
    dispatch({
        type: ACTION_TYPES.RESET_ADD_PURCHASE_MESSAGE,
        payload: {}
    })
}

export const getPurchaseData = function (businessid, fyid, dispatch) {
    dispatch({
        type: ACTION_TYPES.GET_PURCHASE_DATA_LOADING,
        payload: {
            loading: true,
            mesaage: '',
            error: '',
            data: []
        }
    })
    fetchRetry(baseUrl + 'raisePurchase/' + businessid + "/" + fyid, {
        credentials: "include"
    })
        .then(response => response.json())
        .then(dataObject => {
            if (dataObject.result === 'success') {
                let purchaseData = dataObject.data;
                //purchaseData = purchaseData.sort(sortPurchaseData);
                dispatch({
                    type: ACTION_TYPES.GET_PURCHASE_DATA,
                    payload: {
                        message: dataObject.result,
                        data: purchaseData,
                        error: '',
                        loading: false
                    }
                })
            }
            else {
                dispatch({
                    type: ACTION_TYPES.GET_PURCHASE_DATA,
                    payload: {
                        message: '',
                        loading: false,
                        error: dataObject.result,
                        data: []
                    }
                })
            }
        })
        .catch(err => {
            dispatch({
                type: ACTION_TYPES.GET_PURCHASE_DATA,
                payload: {
                    message: '',
                    loading: false,
                    error: err.message,
                    data: []
                }
            })
        })
}

export const resetPurchaseDetailSecond = function (dispatch) {

    dispatch({
        type: ACTION_TYPES.GET_PURCHASE_DATA,
        payload: {
            message: '',
            loading: false,
            error: '',
            data: []
        }
    })
}

export const resetPurchaseDetail = function (dispatch) {
    dispatch({
        type: ACTION_TYPES.GET_PURCHASE_DATA,
        payload: {
            message: 'initial',
            loading: false,
            error: '',
            data: []
        }
    })
}

export const pushNewBusiness = function (value, id, dispatch) {
    value.businessid = id.businessid;
    value.userid = id.userid;
    value.fyid = null;
    value.financialyear = null;
    dispatch({
        type: ACTION_TYPES.PUSH_NEW_BUSINESS,
        payload: {
            data: value
        }
    })
}

export const addPurchaseItem = function (dispatch) {
    dispatch({
        type: ACTION_TYPES.ADD_ITEM_PURCHASE,
        payload: {}
    })
}

export const cancelInvoice = function (invoiceid, businessid, fyid, dispatch) {
    dispatch({
        type: ACTION_TYPES.LOADING_CANCEL_INVOICE_MESSAGE,
        payload: {}
    })
    fetchRetry(baseUrl + 'raiseInvoice/' + businessid + "/" + fyid, {
        method: "PUT",
        body: JSON.stringify({ invoiceid: invoiceid, operation: 'cancelinvoice' }),
        headers: {
            'Content-Type': "application/json"
        },
        credentials: "include"
    })
        .then(response => response.json())
        .then(dataObject => {
            if (dataObject.result === 'success') {
                dispatch({
                    type: ACTION_TYPES.CANCEL_INVOICE,
                    payload: {
                        invoiceid: invoiceid,
                        businessid: businessid,
                        fyid: fyid
                    }
                })
                dispatch({
                    type: ACTION_TYPES.CANCEL_INVOICE_MESSAGE,
                    payload: {
                        message: dataObject.result,
                        error: ''
                    }
                })
            }
            else {
                dispatch({
                    type: ACTION_TYPES.CANCEL_INVOICE_MESSAGE,
                    payload: {
                        error: dataObject.result,
                        message: ''
                    }
                })
            }
        })
        .catch(err => {
            dispatch({
                type: ACTION_TYPES.CANCEL_INVOICE_MESSAGE,
                payload: {
                    error: err.message,
                    message: ''
                }
            })
        })
}

export const resetCancelInvoiceMessage = function (dispatch) {
    dispatch({
        type: ACTION_TYPES.RESET_CANCEL_INVOICE_MESSAGE,
        payload: {}
    })
}

export const restoreInvoice = function (invoiceid, businessid, fyid, dispatch) {
    dispatch({
        type: ACTION_TYPES.RESTORE_INVOICE_MESSAGE_LOADING,
        payload: {}
    })
    fetchRetry(baseUrl + 'raiseInvoice/' + businessid + "/" + fyid, {
        method: "PUT",
        body: JSON.stringify({ invoiceid: invoiceid, operation: 'restoreinvoice' }),
        headers: {
            'Content-Type': "application/json"
        },
        credentials: "include"
    })
        .then(response => response.json())
        .then(dataObject => {
            if (dataObject.result === 'success') {
                dispatch({
                    type: ACTION_TYPES.RESTORE_INVOICE,
                    payload: {
                        invoiceid: invoiceid,
                        businessid: businessid,
                        fyid: fyid
                    }
                })
                dispatch({
                    type: ACTION_TYPES.RESTORE_INVOICE_MESSAGE,
                    payload: {
                        message: dataObject.result,
                        error: ''
                    }
                })
            }
            else {
                dispatch({
                    type: ACTION_TYPES.RESTORE_INVOICE_MESSAGE,
                    payload: {
                        error: dataObject.result,
                        message: ''
                    }
                })
            }
        })
        .catch(err => {
            dispatch({
                type: ACTION_TYPES.RESTORE_INVOICE_MESSAGE,
                payload: {
                    error: err.message,
                    message: ''
                }
            })
        })
}

export const resetRestoreInvoiceMessage = function (dispatch) {
    dispatch({
        type: ACTION_TYPES.RESET_RESTORE_INVOICE_MESSAGE,
        payload: {}
    })
}


export const invoiceUpdate = function (businessid, fyid, state, dispatch) {
    dispatch({
        type: ACTION_TYPES.INVOICE_UPDATE_LOADING,
        payload: {
            loading: true,
            message: '',
            error: ''
        }
    })
    state = {
        ...state,
        customerid: state.customerid.split("...")[0],
        shippedtoid: state.shippedtoid.split("...")[0]
    }
    const invoiceInformation = {
        customerid: state.customerid,
        shippedtoid: state.shippedtoid,
        invoicedate: state.invoicedate,
        invoicenumber: parseInt(state.invoicenumber),
        ponumber: state.ponumber,
        podate: state.podate,
        transporter: state.transporter,
        vehiclenumber: state.vehiclenumber,
        grno: state.grno,
        destination: state.destination,
        ewaybillnumber: state.ewaybillnumber,
        freight: state.freight,
        terms: state.terms,
        itemno: state.itemno,
        totalbasic: state.totalamountbasic,
        totaligst: state.totaligst,
        totalcgst: state.totalcgst,
        totalsgst: state.totalsgst,
        totalvat: state.totalvat,
        totalexcise: state.totalexcise,
        grandtotal: state.grandtotal,
        invoicetype: state.invoicetype,
        active: 0,
        invoiceid: state.invoiceid,
        operation: "updateinvoice",
        cash: state.cash,
        customername: state.customername,
        shippedtoname: state.shippedtoname,
        blamount: state.blamount,
        type : "sales"
    }

    let item = {};
    for (let i = 0; i <= state.itemno; i++) {
        let itemobj = {
            ['name']: state['itemname' + i],
            ['desc']: state['itemdescription' + i],
            ['qty']: state['qty' + i],
            ['price']: state['price' + i],
            ['basic']: state['basic' + i],
            ['igst']: state['igst' + i],
            ['cgst']: state['cgst' + i],
            ['sgst']: state['sgst' + i],
            ['vat']: state['vat' + i],
            ['excise']: state['excise' + i],
            ['total']: state['total' + i]
        }
        item[i] = itemobj;
    }
    invoiceInformation.itemobject = JSON.stringify(item);
    fetchRetry(baseUrl + 'raiseinvoice/' + businessid + "/" + fyid, {
        method: "PUT",
        body: JSON.stringify(invoiceInformation),
        headers: {
            "Content-Type": "application/json"
        },
        credentials: "include"
    })
        .then(response => response.json())
        .then(dataObject => {
            if (dataObject.result === 'success') {
                invoiceInformation.businessid = businessid;
                invoiceInformation.financialyearid = fyid
                dispatch({
                    type: ACTION_TYPES.UPDATE_INVOICE,
                    payload: {
                        invoiceid: state.invoiceid,
                        data: invoiceInformation
                    }
                })
                dispatch({
                    type: ACTION_TYPES.INVOICE_UPDATE,
                    payload: {
                        loading: false,
                        message: dataObject.result,
                        error: '',
                        state: invoiceInformation
                    }
                })
            }
            else {
                dispatch({
                    type: ACTION_TYPES.INVOICE_UPDATE,
                    payload: {
                        loading: false,
                        message: '',
                        error: dataObject.result,
                        state: invoiceInformation
                    }
                })
            }
        })
        .catch(err => {
            dispatch({
                type: ACTION_TYPES.INVOICE_UPDATE,
                payload: {
                    loading: false,
                    message: '',
                    error: err.message,
                    state: invoiceInformation
                }
            })
        })
}

export const resetInvoiceUpdateMessage = function (dispatch) {
    dispatch({
        type: ACTION_TYPES.RESET_INVOICE_UPDATE_MESSAGE,
        payload: {}
    })
}

export const resetDeletePurchaseMessage = function (dispatch) {
    dispatch({
        type: ACTION_TYPES.RESET_DELETE_PURCHASE_MESSAGE,
        payload: {}
    })
}

export const getExpenseDetail = function (businessid, fyid, dispatch) {
    dispatch({
        type: ACTION_TYPES.GET_EXPENSE_LOADING,
        payload: {
            loading: true,
            message: '',
            data: [],
            error: ''
        }
    })
    fetchRetry(baseUrl + 'raiseexpense/' + businessid + "/" + fyid, {
        credentials: "include"
    })
        .then(response => response.json())
        .then(dataObject => {
            if (dataObject.result === 'success') {
                let expenseData = dataObject.data;
                let voucherno = 1;
                if (expenseData.length > 0) {
                    expenseData = expenseData.sort(sortVoucherno);
                    voucherno = parseInt(expenseData[0].voucherno) + 1
                }
                dispatch({
                    type: ACTION_TYPES.GET_EXPENSE,
                    payload: {
                        message: dataObject.result,
                        error: '',
                        loading: false,
                        data: dataObject.data,
                        voucherno: voucherno
                    }
                })
            }
            else {
                dispatch(
                    {
                        type: ACTION_TYPES.GET_EXPENSE,
                        payload: {
                            message: '',
                            error: dataObject.result,
                            loading: false,
                            data: []
                        }
                    }
                )
            }
        })
        .catch(err => dispatch(
            {
                type: ACTION_TYPES.GET_EXPENSE,
                payload: {
                    message: '',
                    error: err.message,
                    loading: false,
                    data: []
                }
            }
        )

        )
}

export const raiseExpense = function (businessid, fyid, state, dispatch) {
    dispatch({
        type: ACTION_TYPES.LOADING_ADD_EXPENSE,
        payload: {
            loading: true,
            error: '',
            message: '',
            state: []
        }
    })
    let dataobj = {
        date: state.date,
        expensedescription: state.expensedescription,
        expensecategory: state.expensecategory,
        amount: state.amount,
        voucherno: state.voucherno,
        tds: state.tds,
        tdsamount: state.tdsamount,
        cash: state.cash,
        vendorid: state.vendorid.split('...')[0],
        businessname: state.vendorid.split('...')[1],
        it: state.it,
        partner: state.partner,
        employee: state.employee
    };
    fetchRetry(baseUrl + 'raiseexpense/' + businessid + "/" + fyid, {
        method: "POST",
        body: JSON.stringify(dataobj),
        headers: {
            "Content-Type": "application/json"
        },
        credentials: "include"
    })
        .then(response => response.json())
        .then(dataObject => {
            if (dataObject.result === "success") {
                let data = {
                    date: state.date,
                    description: state.expensedescription,
                    category: state.expensecategory,
                    amount: state.amount,
                    voucherno: state.voucherno,
                    tds: state.tds === true ? "1" : '',
                    tdsamount: state.tdsamount,
                    vendorid: state.vendorid.split('...')[0],
                    businessname: state.vendorid.split('...')[1],
                    expenseid: dataObject.id.expenseid,
                    businessid: businessid,
                    fyid: fyid,
                    type: "Expense",
                    cash: state.cash === true ? "1" : '',
                    it: state.it === true ? "1" : '',
                    partner: state.partner === true ? "1" : '',
                    employee: state.employee === true ? "1" : ''
                }
                dispatch({
                    type: ACTION_TYPES.PUSH_EXPENSE,
                    payload: {
                        data: data,
                        voucherno: parseInt(state.voucherno) + 1
                    }
                })
                dispatch({
                    type: ACTION_TYPES.ADD_EXPENSE,
                    payload: {
                        loading: false,
                        message: dataObject.result,
                        error: '',
                        state: []
                    }
                })
            }
            else if (dataObject.result === "Already Registered") {
                let vonum = parseInt(dataObject.data.voucherno);
                vonum = vonum + 1;
                dispatch({
                    type: ACTION_TYPES.PUSH_CHECK_EXPENSE,
                    payload: {
                        data: dataObject.data,
                        voucherno: vonum
                    }
                })
                dispatch({
                    type: ACTION_TYPES.ADD_EXPENSE,
                    payload: {
                        loading: false,
                        message: dataObject.result,
                        error: '',
                        state: []
                    }
                })
            }
            else {
                dispatch({
                    type: ACTION_TYPES.ADD_EXPENSE,
                    payload: {
                        loading: false,
                        message: '',
                        error: dataObject.result,
                        state: dataobj
                    }
                })
            }
        })
        .catch(err => {
            dispatch({
                type: ACTION_TYPES.ADD_EXPENSE,
                payload: {
                    loading: false,
                    message: '',
                    error: err.message,
                    state: dataobj
                }
            })
        })
}
export const resetaddExpenseMessage = function (message, dispatch) {
    dispatch({
        type: ACTION_TYPES.RESET_ADD_EXPENSE_MESSAGE,
        payload: {}
    })
}

export const resetExpenseDetailSecond = function (dispatch) {

    dispatch({
        type: ACTION_TYPES.REFRESH_EXPENSE_SECOND,
        payload: {
            message: '',
            loading: false,
            error: '',
            data: []
        }
    })
}
export const resetExpenseDetail = function (dispatch) {
    dispatch({
        type: ACTION_TYPES.REFRESH_EXPENSE,
        payload: {}
    })
}
export const getPaymentDetail = function (businessid, fyid, dispatch) {
    dispatch({
        type: ACTION_TYPES.GET_PAYMENT_LOADING,
        payload: {
            loading: true,
            message: '',
            data: [],
            error: ''
        }
    })
    fetchRetry(baseUrl + 'raisepayment/' + businessid + "/" + fyid, {
        credentials: "include"
    })
        .then(response => response.json())
        .then(dataObject => {
            if (dataObject.result === 'success') {
                let paymentData = dataObject.data;
                let voucherno = 1;
                if (paymentData.length > 0) {
                    paymentData = paymentData.sort(sortVoucherno);
                    voucherno = parseInt(paymentData[0].voucherno) + 1;
                }
                dispatch({
                    type: ACTION_TYPES.GET_PAYMENT,
                    payload: {
                        message: dataObject.result,
                        error: '',
                        loading: false,
                        data: dataObject.data,
                        voucherno: voucherno
                    }
                })
            }
            else {
                dispatch(
                    {
                        type: ACTION_TYPES.GET_PAYMENT,
                        payload: {
                            message: '',
                            error: dataObject.result,
                            loading: false,
                            data: []
                        }
                    }
                )
            }
        })
        .catch(err => dispatch(
            {
                type: ACTION_TYPES.GET_PAYMENT,
                payload: {
                    message: '',
                    error: err.message,
                    loading: false,
                    data: []
                }
            }
        ))
}
export const raisePayment = function (businessid, fyid, state, dispatch) {
    dispatch({
        type: ACTION_TYPES.LOADING_ADD_PAYMENT,
        payload: {
            loading: true,
            error: '',
            message: '',
            state: []
        }
    })
    state = {
        ...state,
        businessname: state.vendorid ? state.vendorid.split("...")[1] : state.customerid.split("...")[1]
    }
    fetchRetry(baseUrl + 'raisepayment/' + businessid + "/" + fyid, {
        method: "POST",
        body: JSON.stringify(state),
        headers: {
            "Content-Type": "application/json"
        },
        credentials: "include"
    })
        .then(response => response.json())
        .then(dataObject => {
            if (dataObject.result === "success") {
                let data = {
                    ...state,
                    paymentid: dataObject.id.paymentid,
                    businessid: businessid,
                    fyid: fyid,
                    type: "payment",
                    customerid : state.customerid ? state.customerid.split("...")[0] : '',
                    vendorid : state.vendorid ? state.vendorid.split("...")[0] : ''
                }
                dispatch({
                    type: ACTION_TYPES.PUSH_PAYMENT,
                    payload: {
                        data: data,
                        voucherno: parseInt(state.voucherno) + 1
                    }
                })
                dispatch({
                    type: ACTION_TYPES.ADD_PAYMENT,
                    payload: {
                        loading: false,
                        message: dataObject.result,
                        error: '',
                        state: []
                    }
                })
            }
            else if (dataObject.result === "Already Registered") {
                let vonum = parseInt(dataObject.data.voucherno);
                vonum = vonum + 1;
                dispatch({
                    type: ACTION_TYPES.PUSH_CHECK_PAYMENT,
                    payload: {
                        data: dataObject.data,
                        voucherno: vonum
                    }
                })
                dispatch({
                    type: ACTION_TYPES.ADD_PAYMENT,
                    payload: {
                        loading: false,
                        message: dataObject.result,
                        error: '',
                        state: []
                    }
                })
            }
            else {
                dispatch({
                    type: ACTION_TYPES.ADD_PAYMENT,
                    payload: {
                        loading: false,
                        message: '',
                        error: dataObject.result,
                        state: state
                    }
                })
            }
        })
        .catch(err => {
            dispatch({
                type: ACTION_TYPES.ADD_PAYMENT,
                payload: {
                    loading: false,
                    message: '',
                    error: err.message,
                    state: state
                }
            })
        })
}
export const resetaddPaymentMessage = function (message, dispatch) {
    dispatch({
        type: ACTION_TYPES.RESET_ADD_PAYMENT_MESSAGE,
        payload: {}
    })
}
export const resetPaymentDetailSecond = function (dispatch) {
    dispatch({
        type: ACTION_TYPES.REFRESH_PAYMENT_SECOND,
        payload: {
            message: '',
            loading: false,
            error: '',
            data: []
        }
    })
}
export const resetPaymentDetail = function (dispatch) {
    dispatch({
        type: ACTION_TYPES.REFRESH_PAYMENT,
        payload: {}
    })
}
export const raiseBankEntry = function (businessid, fyid, state, dispatch) {
    dispatch({
        type: ACTION_TYPES.LOADING_ADD_BANK_ENTRY,
        payload: {
            loading: true,
            error: '',
            message: '',
            state: []
        }
    })
    fetchRetry(baseUrl + 'raisebankentry/' + businessid + "/" + fyid, {
        method: "POST",
        body: JSON.stringify(state),
        headers: {
            "Content-Type": "application/json"
        },
        credentials: "include"
    })
        .then(response => response.json())
        .then(dataObject => {
            if (dataObject.result === "success") {
                let data = {
                    ...state,
                    bankentryid: dataObject.id.bankentryid,
                    businessid: businessid,
                    fyid: fyid,
                    type: "bankentry"
                }
                dispatch({
                    type: ACTION_TYPES.PUSH_BANK_ENTRY,
                    payload: {
                        data: data,
                        voucherno: state.voucherno + 1
                    }
                })
                dispatch({
                    type: ACTION_TYPES.ADD_BANK_ENTRY,
                    payload: {
                        loading: false,
                        message: dataObject.result,
                        error: '',
                        state: []
                    }
                })
            }
            else if (dataObject.result === 'Already Registered') {
                let voucherno = parseInt(dataObject.data.voucherno) + 1;
                dispatch({
                    type: ACTION_TYPES.PUSH_CHECK_BANK_ENTRY_DATA,
                    payload: {
                        data: dataObject.data,
                        voucherno: voucherno
                    }
                })
                dispatch({
                    type: ACTION_TYPES.ADD_BANK_ENTRY,
                    payload: {
                        loading: false,
                        message: dataObject.result,
                        error: '',
                        state: []
                    }
                })
            }
            else {
                dispatch({
                    type: ACTION_TYPES.ADD_BANK_ENTRY,
                    payload: {
                        loading: false,
                        message: '',
                        error: dataObject.result,
                        state: state
                    }
                })
            }
        })
        .catch(err => {
            dispatch({
                type: ACTION_TYPES.ADD_BANK_ENTRY,
                payload: {
                    loading: false,
                    message: '',
                    error: err.message,
                    state: state
                }
            })
        })
}

export const getBankEntryDetail = function (businessid, fyid, dispatch) {
    dispatch({
        type: ACTION_TYPES.GET_BANK_ENTRY_LOADING,
        payload: {
            loading: true,
            message: '',
            data: [],
            error: ''
        }
    })
    fetchRetry(baseUrl + 'raisebankentry/' + businessid + "/" + fyid, {
        credentials: "include"
    })
        .then(response => response.json())
        .then(dataObject => {
            if (dataObject.result === 'success') {
                let voucherno = 1;
                if (dataObject.data.length > 0) {
                    let bankEntryData = dataObject.data.sort(sortVoucherno);
                    voucherno = bankEntryData[0].voucherno + 1;
                }
                dispatch({
                    type: ACTION_TYPES.GET_BANK_ENTRY,
                    payload: {
                        message: dataObject.result,
                        error: '',
                        loading: false,
                        data: dataObject.data,
                        voucherno: voucherno
                    }
                })
            }
            else {
                dispatch(
                    {
                        type: ACTION_TYPES.GET_BANK_ENTRY,
                        payload: {
                            message: '',
                            error: dataObject.result,
                            loading: false,
                            data: []
                        }
                    }
                )
            }
        })
        .catch(err => dispatch(
            {
                type: ACTION_TYPES.GET_BANK_ENTRY,
                payload: {
                    message: '',
                    error: err.message,
                    loading: false,
                    data: []
                }
            }
        )
        )
}

export const resetaddBankEntryMessage = function (message, dispatch) {
    dispatch({
        type: ACTION_TYPES.RESET_ADD_BANK_ENTRY_MESSAGE,
        payload: {}
    })
}

export const resetBankEntryDetailSecond = function (dispatch) {
    dispatch({
        type: ACTION_TYPES.REFRESH_BANK_ENTRY_SECOND,
        payload: {
            message: '',
            loading: false,
            error: '',
            data: []
        }
    })
}

export const resetBankEntryDetail = function (dispatch) {
    dispatch({
        type: ACTION_TYPES.REFRESH_BANK_ENTRY,
        payload: {
            message: 'initial',
            loading: false,
            error: '',
            data: []
        }
    })
}

export const getOpeningClosingDetail = function (businessid, fyid, dispatch) {
    dispatch({
        type: ACTION_TYPES.GET_OPENINGCLOSING_LOADING,
        payload: {
            loading: true,
            message: '',
            data: [],
            error: ''
        }
    })
    fetchRetry(baseUrl + 'raiseopeningclosing/' + businessid + "/" + fyid, {
        credentials: "include"
    })
        .then(response => response.json())
        .then(dataObject => {
            if (dataObject.result === 'success') {
                dispatch({
                    type: ACTION_TYPES.GET_OPENINGCLOSING,
                    payload: {
                        message: dataObject.result,
                        error: '',
                        loading: false,
                        data: JSON.parse(dataObject.data[0].obj)
                    }
                })
            }
            else {
                dispatch(
                    {
                        type: ACTION_TYPES.GET_OPENINGCLOSING,
                        payload: {
                            message: '',
                            error: dataObject.result,
                            loading: false,
                            data: {}
                        }
                    }
                )
            }
        })
        .catch(err => dispatch(
            {
                type: ACTION_TYPES.GET_OPENINGCLOSING,
                payload: {
                    message: '',
                    error: err.message,
                    loading: false,
                    data: {}
                }
            }
        )

        )
}

export const raiseOpeningClosing = function (businessid, fyid, state, dispatch) {
    dispatch({
        type: ACTION_TYPES.LOADING_ADD_OPENINGCLOSING,
        payload: {
            loading: true,
            error: '',
            message: ''
        }
    })
    let data = {
        aprilopening: state.aprilopening,
        aprilclosing: state.aprilclosing,
        mayopening: state.mayopening,
        mayclosing: state.mayclosing,
        juneopening: state.juneopening,
        juneclosing: state.juneclosing,
        julyopening: state.julyopening,
        julyclosing: state.julyclosing,
        augustopening: state.augustopening,
        augustclosing: state.augustclosing,
        septemberopening: state.septemberopening,
        septemberclosing: state.septemberclosing,
        octoberopening: state.octoberopening,
        octoberclosing: state.octoberclosing,
        novemberopening: state.novemberopening,
        novemberclosing: state.novemberclosing,
        decemberopening: state.decemberopening,
        decemberclosing: state.decemberclosing,
        januaryopening: state.januaryopening,
        januaryclosing: state.januaryclosing,
        feburaryopening: state.feburaryopening,
        feburaryclosing: state.feburaryclosing,
        marchopening: state.marchopening,
        marchclosing: state.marchclosing
    }
    fetchRetry(baseUrl + 'raiseopeningclosing/' + businessid + "/" + fyid, {
        method: "PUT",
        body: JSON.stringify({ state: JSON.stringify(data) }),
        headers: {
            "Content-Type": "application/json"
        },
        credentials: "include"
    })
        .then(response => response.json())
        .then(dataObject => {
            if (dataObject.result === "success") {
                dispatch({
                    type: ACTION_TYPES.GET_OPENINGCLOSING,
                    payload: {
                        data: state,
                        loading: false,
                        error: '',
                        message: ''
                    }
                })
                dispatch({
                    type: ACTION_TYPES.ADD_OPENINGCLOSING,
                    payload: {
                        loading: false,
                        message: dataObject.result,
                        error: '',
                        state: state
                    }
                })
            }
            else {
                dispatch({
                    type: ACTION_TYPES.ADD_OPENINGCLOSING,
                    payload: {
                        loading: false,
                        message: '',
                        error: dataObject.result,
                        state: state
                    }
                })
            }
        })
        .catch(err => {
            dispatch({
                type: ACTION_TYPES.ADD_OPENINGCLOSING,
                payload: {
                    loading: false,
                    message: '',
                    error: err.message,
                    state: state
                }
            })
        })
}

export const resetaddOpeningClosingMessage = function (message, dispatch) {
    dispatch({
        type: ACTION_TYPES.RESET_ADD_OPENINGCLOSING_MESSAGE,
        payload: {}
    })
}

export const resetOpeningClosingDetailSecond = function (dispatch) {
    dispatch({
        type: ACTION_TYPES.REFRESH_OPENINGCLOSING_SECOND,
        payload: {
            message: '',
            loading: false,
            error: '',
            data: {}
        }
    })
}

export const resetOpeningClosingDetail = function (dispatch) {
    dispatch({
        type: ACTION_TYPES.REFRESH_OPENINGCLOSING,
        payload: {}
    })
}

export const updatePartyOpeningBalance = function (state, businessid, fyid, dispatch) {
    dispatch({
        type: ACTION_TYPES.UPDATE_PARTY_OPENING_LOADING,
        payload: {
            loading: true,
            message: '',
            disabled: '',
            error: ''
        }
    })
    let obj = {
        party: state.customerorvendor,
        partyid: state.name.split("....")[0],
        amount: state.openingbalance,
        amounttype: state.amounttype
    }
    fetchRetry(baseUrl + 'partyOpening/' + businessid + "/" + fyid, {
        method: "POST",
        body: JSON.stringify(obj),
        headers: {
            "Content-Type": "application/json"
        },
        credentials: "include"
    })
        .then(response => response.json())
        .then(dataObject => {
            if (dataObject.result === "success") {
                dispatch({
                    type: ACTION_TYPES.PUSH_OPENING_BALANCE,
                    payload: {
                        id: dataObject.id.id,
                        businessid: businessid,
                        fyid: fyid,
                        state: state
                    }
                })
                dispatch({
                    type: ACTION_TYPES.UPDATE_PARTY_OPENING,
                    payload: {
                        loading: false,
                        message: dataObject.result,
                        error: '',
                        disabled: false,
                        state: state
                    }
                })
            }
            else {
                dispatch({
                    type: ACTION_TYPES.UPDATE_PARTY_OPENING,
                    payload: {
                        loading: false,
                        error: dataObject.result,
                        message: '',
                        disabled: false,
                        state: state
                    }
                })
            }
        })
        .catch(err => dispatch({
            type: ACTION_TYPES.UPDATE_PARTY_OPENING,
            payload: {
                loading: false,
                error: err.message,
                message: '',
                disabled: false,
                state: state
            }
        }))
}

export const getPartyOpeningBalance = function (businessid, fyid, dispatch) {
    dispatch({
        type: ACTION_TYPES.GET_OPENING_BALANCE_LOADING,
        payload: {
            loading: true,
            result: '',
            data: [],
            error: ''
        }
    });
    return fetchRetry(baseUrl + 'partyOpening/' + businessid + "/" + fyid, {
        credentials: "include"
    })
        .then((response) => response.json())
        .then((dataObject) => {
            if (dataObject.result === 'success') {
                dispatch({
                    type: ACTION_TYPES.GET_OPENING_BALANCE,
                    payload: {
                        message: dataObject.result,
                        error: '',
                        loading: false,
                        data: dataObject.data
                    }
                })
            }
            else {
                dispatch(
                    {
                        type: ACTION_TYPES.GET_OPENING_BALANCE,
                        payload: {
                            message: '',
                            error: dataObject.result,
                            loading: false,
                            data: {}
                        }
                    }
                )
            }
        })
        .catch(err => {
            dispatch({
                type: ACTION_TYPES.GET_OPENING_BALANCE,
                payload: {
                    message: '',
                    error: err.message,
                    loading: false,
                    data: {}
                }
            });
        })
}

export const resetOpeningBalanceSecond = function (dispatch) {
    dispatch({
        type: ACTION_TYPES.REFRESH_OPENING_BALANCE_SECOND,
        payload: {
            message: '',
            loading: false,
            error: '',
            data: []
        }
    })
}

export const resetOpeningBalance = function (dispatch) {
    dispatch({
        type: ACTION_TYPES.REFRESH_OPENING_BALANCE,
        payload: {}
    })
}

export const logout = function (dispatch) {
    dispatch({
        type: ACTION_TYPES.LOGOUTLOADING,
        payload: {}
    })
    return fetchRetry(baseUrl + 'logout', {
        credentials: "include"
    })
        .then(response => response.json())
        .then(dataObject => {
            if (dataObject.result === 'success') {
                dispatch({
                    type: ACTION_TYPES.LOGOUT,
                    payload: {
                        loading: false,
                        error: '',
                        message: dataObject.result
                    }
                })
            }
            else {
                dispatch({
                    type: ACTION_TYPES.LOGOUT,
                    payload: {
                        loading: false,
                        error: dataObject.result,
                        message: ''
                    }
                })
            }
        })
        .catch(err => {
            dispatch({
                type: ACTION_TYPES.LOGOUT,
                payload: {
                    loading: false,
                    error: err.message,
                    message: ''
                }
            })
        })
}

export const resetLogoutMessage = function (dispatch) {
    dispatch({
        type: ACTION_TYPES.REFRESH_LOGOUTMESSAGE,
        payload: {}
    })
}

export const refreshUpdatepartyOpeningBalance = function (dispatch) {
    dispatch({
        type: ACTION_TYPES.RESETTING_PARTY_OPENING_CHANGE,
        payload: {
            loading: false,
            message: "initial",
            disabled: false,
            error: ''
        }
    })
}

export const getCashInHandDetail = function (businessid, fyid, dispatch) {
    dispatch({
        type: ACTION_TYPES.GET_CASH_IN_HAND_LOADING,
        payload: {
            loading: true,
            message: '',
            error: ''
        }
    })
    fetchRetry(baseUrl + 'cashinhand/' + businessid + "/" + fyid, {
        credentials: "include"
    })
        .then(response => response.json())
        .then(dataObject => {
            if (dataObject.result === 'success') {
                dispatch({
                    type: ACTION_TYPES.GET_CASH_IN_HAND,
                    payload: {
                        loading: false,
                        error: '',
                        message: dataObject.result,
                        amount: dataObject.amount ? dataObject.amount.amount : 0
                    }
                })
            }
            else {
                dispatch({
                    type: ACTION_TYPES.GET_CASH_IN_HAND,
                    payload: {
                        loading: false,
                        message: '',
                        error: dataObject.result
                    }
                })
            }
        })
        .catch(err => {
            dispatch({
                type: ACTION_TYPES.GET_CASH_IN_HAND,
                payload: {
                    loading: false,
                    message: '',
                    error: err.message
                }
            })
        })
}

export const updateCashInHandDetail = function (businessid, fyid, amount, dispatch) {
    dispatch({
        type: ACTION_TYPES.UPDATE_CASH_IN_HAND_LOADING,
        payload: {
            loading: true,
            error: '',
            message: '',
            amount: amount
        }
    })
    fetchRetry(baseUrl + 'cashinhand/' + businessid + "/" + fyid, {
        method: "PUT",
        body: JSON.stringify({ amount: amount }),
        headers: {
            "Content-Type": "application/json"
        },
        credentials: "include"
    })
        .then(response => response.json())
        .then(dataObject => {
            if (dataObject.result === 'success') {
                dispatch({
                    type: ACTION_TYPES.GET_CASH_IN_HAND,
                    payload: {
                        loading: false,
                        error: '',
                        message: '',
                        amount: amount
                    }
                })
                dispatch({
                    type: ACTION_TYPES.UPDATE_CASH_IN_HAND,
                    payload: {
                        loading: false,
                        error: '',
                        message: dataObject.result,
                    }
                })
            }
            else {
                dispatch({
                    type: ACTION_TYPES.UPDATE_CASH_IN_HAND,
                    payload: {
                        loading: false,
                        message: '',
                        error: dataObject.result,
                    }
                })
            }
        })
        .catch(err => dispatch({
            type: ACTION_TYPES.UPDATE_CASH_IN_HAND,
            payload: {
                loading: false,
                message: '',
                error: err.message,
            }
        }))
}

export const resetCashInHandDetail = function (dispatch) {
    dispatch({
        type: ACTION_TYPES.REFRESH_CASH_IN_HAND_MESSAGE,
        payload: {
            loading: false,
            message: 'initial',
            error: ''
        }
    })
}

export const resetCashInHand = function (dispatch) {
    dispatch({
        type: ACTION_TYPES.REFRESH_CASH_IN_HAND,
        payload: {
            loading: false,
            message: 'initial',
            error: ''
        }
    })
}

export const resetAddEmployeeMessage = function (dispatch) {
    dispatch({
        type: ACTION_TYPES.RESET_EMPLOYEE_MESSAGE,
        payload: {}
    })
}

export const getEmployeeList = function (businessid, dispatch) {
    dispatch({
        type: ACTION_TYPES.GET_EMPLOYEE_LOADING,
        payload: {
            loading: true,
            message: '',
            data: [],
            error: ''
        }
    });
    return fetchRetry(baseUrl + 'employee/' + businessid, {
        credentials: "include"
    })
        .then(response => response.json())
        .then(dataObject => {
            if (dataObject.result === 'success') {
                let employeeData = dataObject.data;
                let employeeno = 1;
                if (employeeData.length > 0) employeeno = employeeData[0].employeeno + 1;
                dispatch({
                    type: ACTION_TYPES.GET_EMPLOYEE,
                    payload: {
                        loading: false,
                        message: dataObject.result,
                        data: dataObject.data,
                        error: '',
                        employeeno: employeeno
                    }
                })
            }
            else {
                dispatch({
                    type: ACTION_TYPES.GET_EMPLOYEE,
                    payload: {
                        loading: false,
                        message: '',
                        data: [],
                        error: dataObject.result
                    }
                })
            }
        })
        .catch(err => {
            dispatch({
                type: ACTION_TYPES.GET_EMPLOYEE,
                payload: {
                    data: [],
                    error: err.message,
                    loading: false,
                    message: ''
                }
            });
        })
}

export const addEmployee = function (state, businessid, dispatch) {
    let date = new Date().toDateString().split(' ').slice(1).join(' ');
    dispatch({
        type: ACTION_TYPES.ADD_EMPLOYEE_LOADING,
        payload: {
            loading: true,
            disabled: true,
            message: '',
            error: ''
        }
    });
    let salary = {
        0: JSON.stringify({
            date: date.split(' ')[0] + ' ' + date.split(' ')[2],
            salary: state.salary
        })
    }
    const data = {
        employeeno: state.employeeno,
        employeename: state.employeename,
        aadharnumber: state.aadharnumber,
        salary: JSON.stringify(salary),
        active: state.active,
        doj: state.doj
    }
    return fetchRetry(baseUrl + 'employee/' + businessid, {
        method: "POST",
        body: JSON.stringify(data),
        headers: {
            "Content-Type": "application/json"
        },
        credentials: "include"
    })
        .then(response => response.json())
        .then(dataObject => {
            if (dataObject.result === 'success') {
                data.employeeid = dataObject.id.employeeid;
                data.businessid = businessid;
                dispatch({
                    type: ACTION_TYPES.PUSH_EMPLOYEE,
                    payload: {
                        data: data,
                        employeeno: parseInt(state.employeeno) + 1
                    }
                })
                dispatch({
                    type: ACTION_TYPES.ADD_EMPLOYEE,
                    payload: {
                        loading: false,
                        disabled: false,
                        message: dataObject.result,
                        error: ''
                    }
                })
            }
            else if (dataObject.result === 'Already Registered') {
                dispatch({
                    type: ACTION_TYPES.PUSH_CHECK_EMPLOYEE,
                    payload: {
                        data: dataObject.data,
                        employeeno: parseInt(state.employeeno) + 1
                    }
                })
                dispatch({
                    type: ACTION_TYPES.ADD_EMPLOYEE,
                    payload: {
                        loading: false,
                        disabled: false,
                        message: dataObject.result,
                        error: ''
                    }
                })
            }
            else {
                dispatch({
                    type: ACTION_TYPES.ADD_EMPLOYEE,
                    payload: {
                        loading: false,
                        disabled: false,
                        error: dataObject.result,
                        message: '',
                        state: state
                    }
                })
            }
        })
        .catch((err) => dispatch({
            type: ACTION_TYPES.ADD_EMPLOYEE,
            payload: {
                loading: false,
                disabled: false,
                error: err.message,
                message: '',
                state: state
            }
        }))
}

export const refreshEmployeeState = function (dispatch) {
    dispatch({
        type: ACTION_TYPES.REFRESH_EMPLOYEE,
        payload: {}
    })
}

export const getAttendanceList = function (fyid, date, dispatch) {
    dispatch({
        type: ACTION_TYPES.GET_ATTENDANCE_LOADING,
        payload: {
            loading: true,
            error: '',
            message: '',
            data: []
        }
    })
    return fetchRetry(baseUrl + 'updateattendance/' + fyid + "/" + date, {
        credentials: "include"
    })
        .then(response => response.json())
        .then(dataObject => {
            if (dataObject.result === 'success') {
                dispatch({
                    type: ACTION_TYPES.GET_ATTENDANCE,
                    payload: {
                        loading: false,
                        error: '',
                        message: dataObject.result,
                        data: dataObject.data
                    }
                })
            }
            else {
                dispatch({
                    type: ACTION_TYPES.GET_ATTENDANCE,
                    payload: {
                        loading: false,
                        error: dataObject.result,
                        message: '',
                        data: []
                    }
                })
            }
        })
        .catch(err => {
            dispatch({
                type: ACTION_TYPES.GET_ATTENDANCE,
                payload: {
                    loading: false,
                    error: err.message,
                    message: '',
                    data: []
                }
            })
        })
}

export const updateAttendance = function (fyid, date, state, dispatch) {
    dispatch({
        type: ACTION_TYPES.UPDATE_ATTENDANCE_LOADING,
        payload: {
            loading: true,
            error: '',
            message: ''
        }
    })
    let keys = Object.keys(state);
    let attendanceobject = {};
    for (let i = 0; i < keys.length; i++) {
        attendanceobject = {
            ...attendanceobject,
            [keys[i].split("present")[1]]: {
                present: state[keys[i]],
                ot: state[keys[i + 1]]
            }
        }
        i++;
    }
    return fetchRetry(baseUrl + 'updateattendance/' + fyid + "/" + date, {
        method: "PUT",
        body: JSON.stringify({ attendanceobject: JSON.stringify(attendanceobject) }),
        headers: {
            "Content-Type": "application/json"
        },
        credentials: "include"
    })
        .then(response => response.json())
        .then(dataObject => {
            if (dataObject.result === "success") {
                dispatch({
                    type: ACTION_TYPES.GET_ATTENDANCE,
                    payload: {
                        loading: false,
                        error: '',
                        message: '',
                        data: [{ attendanceobject: JSON.stringify(attendanceobject) }]
                    }
                })
                dispatch({
                    type: ACTION_TYPES.UPDATE_ATTENDANCE,
                    payload: {
                        loading: false,
                        error: '',
                        message: dataObject.result
                    }
                })
            }
            else {
                dispatch({
                    type: ACTION_TYPES.UPDATE_ATTENDANCE,
                    payload: {
                        loading: false,
                        error: dataObject.result,
                        message: '',
                        state: state
                    }
                })
            }
        })
        .catch(err => dispatch({
            type: ACTION_TYPES.UPDATE_ATTENDANCE,
            payload: {
                loading: false,
                error: err.message,
                message: '',
                state: state
            }
        }))
}

export const resetGetAttendance = function (dispatch) {
    dispatch({
        type: ACTION_TYPES.REFRESH_GET_ATTENDANCE,
        payload: {}
    })
}

export const resetUpdateAttendanceMessage = function (dispatch) {
    dispatch({
        type: ACTION_TYPES.REFRESH_ATTENDANCE_MESSAGE,
        payload: {}
    })
}

export const getMonthAttendanceList = function (fyid, month, year, dispatch) {
    dispatch({
        type: ACTION_TYPES.GET_MONTH_ATTENDANCE_LOADING,
        payload: {
            loading: true,
            error: '',
            message: '',
            data: []
        }
    })
    return fetchRetry(baseUrl + 'monthattendance/' + fyid + "/" + month + "/" + year, {
        credentials: "include"
    })
        .then(response => response.json())
        .then(dataObject => {
            if (dataObject.result === 'success') {
                dispatch({
                    type: ACTION_TYPES.GET_MONTH_ATTENDANCE,
                    payload: {
                        loading: false,
                        error: '',
                        message: dataObject.result,
                        data: dataObject.data
                    }
                })
            }
            else {
                dispatch({
                    type: ACTION_TYPES.GET_MONTH_ATTENDANCE,
                    payload: {
                        loading: false,
                        error: dataObject.result,
                        message: '',
                        data: []
                    }
                })
            }
        })
        .catch(err => {
            dispatch({
                type: ACTION_TYPES.GET_MONTH_ATTENDANCE,
                payload: {
                    loading: false,
                    error: err.message,
                    message: '',
                    data: []
                }
            })
        })
}

export const resetGetMonthAttendance = function (dispatch) {
    dispatch({
        type: ACTION_TYPES.REFRESH_GET_MONTH_ATTENDANCE,
        payload: {}
    })
}

export const updateSalary = function (fyid, month, state, dispatch) {
    dispatch({
        type: ACTION_TYPES.UPDATE_SALARY_LOADING,
        payload: {
            loading: true,
            error: '',
            message: '',
            data: []
        }
    })
    let nextmonth = undefined;
    let nextMonthState = {};
    switch (month) {
        case 'apr': nextmonth = 'may'; break;
        case 'may': nextmonth = 'jun'; break;
        case 'jun': nextmonth = 'jul'; break;
        case 'jul': nextmonth = 'aug'; break;
        case 'aug': nextmonth = 'sep'; break;
        case 'sep': nextmonth = 'oct'; break;
        case 'oct': nextmonth = 'nov'; break;
        case 'nov': nextmonth = 'dec'; break;
        case 'dec': nextmonth = 'jan'; break;
        case 'jan': nextmonth = 'feb'; break;
        case 'feb': nextmonth = 'mar'; break;
        case 'mar': nextmonth = 'none'; break;
    }
    if (month !== 'mar') {
        Object.keys(state).forEach(k => {
            nextMonthState = {
                ...nextMonthState,
                [k]: {
                    salary: 0,
                    paid: 0,
                    advancedue: state[k].advance,
                    advance: 0
                }
            }
        })
    }
    return fetchRetry(baseUrl + 'updatesalary/' + fyid + "/" + month, {
        method: "POST",
        body: JSON.stringify({ state: JSON.stringify(state), nextMonthState: JSON.stringify(nextMonthState), nextMonth: nextmonth }),
        headers: {
            "Content-Type": "application/json"
        },
        credentials: "include"
    })
        .then(response => response.json())
        .then(dataObject => {
            if (dataObject.result === "success") {
                dispatch({
                    type: ACTION_TYPES.GET_SALARY,
                    payload: {
                        loading: false,
                        error: '',
                        message: '',
                        data: [state]
                    }
                })
                dispatch({
                    type: ACTION_TYPES.UPDATE_SALARY,
                    payload: {
                        loading: false,
                        error: '',
                        message: dataObject.result
                    }
                })
            }
            else {
                dispatch({
                    type: ACTION_TYPES.UPDATE_SALARY,
                    payload: {
                        loading: false,
                        error: dataObject.result,
                        message: '',
                        state: state
                    }
                })
            }
        })
        .catch(err => dispatch({
            type: ACTION_TYPES.UPDATE_SALARY,
            payload: {
                loading: false,
                error: err.message,
                message: '',
                state: state
            }
        }))
}

export const getSalaryList = function (fyid, month, dispatch) {
    dispatch({
        type: ACTION_TYPES.GET_SALARY_LOADING,
        payload: {
            loading: true,
            error: '',
            message: '',
            data: []
        }
    })
    return fetchRetry(baseUrl + 'updatesalary/' + fyid + "/" + month, {
        credentials: "include"
    })
        .then(response => response.json())
        .then(dataObject => {
            if (dataObject.result === 'success') {
                dispatch({
                    type: ACTION_TYPES.GET_SALARY,
                    payload: {
                        loading: false,
                        error: '',
                        message: dataObject.result,
                        data: dataObject.data.length > 0 ? [JSON.parse(dataObject.data[0].salary)] : []
                    }
                })
            }
            else {
                dispatch({
                    type: ACTION_TYPES.GET_SALARY,
                    payload: {
                        loading: false,
                        error: dataObject.result,
                        message: '',
                        data: []
                    }
                })
            }
        })
        .catch(err => {
            dispatch({
                type: ACTION_TYPES.GET_SALARY,
                payload: {
                    loading: false,
                    error: err.message,
                    message: '',
                    data: []
                }
            })
        })
}

export const resetGetSalary = function (dispatch) {
    dispatch({
        type: ACTION_TYPES.REFRESH_GET_SALARY,
        payload: {}
    })
}

export const resetUpdateSalaryMessage = function (dispatch) {
    dispatch({
        type: ACTION_TYPES.REFRESH_SALARY_MESSAGE,
        payload: {}
    })
}

export const purchaseUpdate = function (businessid, fyid, state, dispatch) {
    dispatch({
        type: ACTION_TYPES.PURCHASE_UPDATE_LOADING,
        payload: {
            loading: true,
            message: '',
            error: ''
        }
    })
    const purchaseInformation = {
        vendorid: state.vendorid,
        invoicedate: state.invoicedate,
        invoicenumber: state.invoicenumber,
        freight: state.freight,
        itemno: state.itemno,
        totalbasic: state.totalamountbasic,
        totaligst: state.totaligst,
        totalcgst: state.totalcgst,
        totalsgst: state.totalsgst,
        grandtotal: state.grandtotal,
        invoicetype: state.invoicetype,
        purchaseid: state.purchaseid,
        operation: "updatepurchase",
        importduty: state.importduty,
        asset: state.asset
    }

    let item = {};
    for (let i = 0; i <= state.itemno; i++) {
        let itemobj = {
            ['name']: state['itemname' + i],
            ['qty']: state['qty' + i],
            ['price']: state['price' + i],
            ['basic']: state['basic' + i],
            ['igst']: state['igst' + i],
            ['cgst']: state['cgst' + i],
            ['sgst']: state['sgst' + i],
            ['vat']: state['vat' + i],
            ['excise']: state['excise' + i],
            ['total']: state['total' + i]
        }
        item[i] = itemobj;
    }
    purchaseInformation.itemobject = JSON.stringify(item);
    fetchRetry(baseUrl + 'raisepurchase/' + businessid + "/" + fyid, {
        method: "PUT",
        body: JSON.stringify(purchaseInformation),
        headers: {
            "Content-Type": "application/json"
        },
        credentials: "include"
    })
        .then(response => response.json())
        .then(dataObject => {
            if (dataObject.result === 'success') {
                purchaseInformation.businessid = businessid;
                purchaseInformation.financialyearid = fyid
                dispatch({
                    type: ACTION_TYPES.UPDATE_PURCHASE,
                    payload: {
                        purchaseid: state.purchaseid,
                        data: purchaseInformation
                    }
                })
                dispatch({
                    type: ACTION_TYPES.PURCHASE_UPDATE,
                    payload: {
                        loading: false,
                        message: dataObject.result,
                        error: '',
                        state: purchaseInformation
                    }
                })
            }
            else {
                dispatch({
                    type: ACTION_TYPES.PURCHASE_UPDATE,
                    payload: {
                        loading: false,
                        message: '',
                        error: dataObject.result,
                        state: purchaseInformation
                    }
                })
            }
        })
        .catch(err => {
            dispatch({
                type: ACTION_TYPES.INVOICE_UPDATE,
                payload: {
                    loading: false,
                    message: '',
                    error: err.message,
                    state: purchaseInformation
                }
            })
        })
}
export const resetPurchaseUpdateMessage = function (dispatch) {
    dispatch({
        type: ACTION_TYPES.RESET_PURCHASE_UPDATE_MESSAGE,
        payload: {}
    })
}
export const resetDeleteExpenseMessage = function (dispatch) {
    dispatch({
        type: ACTION_TYPES.RESET_DELETE_EXPENSE_MESSAGE,
        payload: {}
    })
}
export const cancelExpense = function (expenseid, businessid, fyid, dispatch) {
    dispatch({
        type: ACTION_TYPES.LOADING_DELETE_EXPENSE_MESSAGE,
        payload: {}
    })
    fetchRetry(baseUrl + 'raiseexpense/' + businessid + "/" + fyid, {
        method: "DELETE",
        body: JSON.stringify({ expenseid: expenseid }),
        headers: {
            "Content-Type": "application/json"
        },
        credentials: "include"
    })
        .then(response => response.json())
        .then(dataObject => {
            if (dataObject.result === 'success') {
                dispatch({
                    type: ACTION_TYPES.DELETE_EXPENSE,
                    payload: {
                        expenseid: expenseid
                    }
                })
                dispatch({
                    type: ACTION_TYPES.DELETE_EXPENSE_MESSAGE,
                    payload: {
                        message: dataObject.result,
                        error: ''
                    }
                })
            }
            else {
                dispatch({
                    type: ACTION_TYPES.DELETE_EXPENSE_MESSAGE,
                    payload: {
                        message: '',
                        error: dataObject.result
                    }
                })
            }
        })
        .catch(err => {
            dispatch({
                type: ACTION_TYPES.DELETE_EXPENSE_MESSAGE,
                payload: {
                    message: '',
                    error: err.message
                }
            })
        })
}

export const updateBankOpeningBalance = function (state, businessid, fyid, dispatch) {
    dispatch({
        type: ACTION_TYPES.UPDATE_BANK_OPENING_LOADING,
        payload: {
            loading: true,
            message: '',
            disabled: '',
            error: ''
        }
    })
    fetchRetry(baseUrl + 'bankOpeningBalance/' + businessid + "/" + fyid, {
        method: "POST",
        body: JSON.stringify(state),
        headers: {
            "Content-Type": "application/json"
        },
        credentials: "include"
    })
        .then(response => response.json())
        .then(dataObject => {
            if (dataObject.result === "success") {
                dispatch({
                    type: ACTION_TYPES.PUSH_BANK_OPENING_BALANCE,
                    payload: {
                        id: dataObject.id.id,
                        businessid: businessid,
                        fyid: fyid,
                        state: state
                    }
                })
                dispatch({
                    type: ACTION_TYPES.UPDATE_BANK_OPENING,
                    payload: {
                        loading: false,
                        message: dataObject.result,
                        error: '',
                        disabled: false,
                        state: state
                    }
                })
            }
            else {
                dispatch({
                    type: ACTION_TYPES.UPDATE_BANK_OPENING,
                    payload: {
                        loading: false,
                        error: dataObject.result,
                        message: '',
                        disabled: false,
                        state: state
                    }
                })
            }
        })
        .catch(err => dispatch({
            type: ACTION_TYPES.UPDATE_BANK_OPENING,
            payload: {
                loading: false,
                error: err.message,
                message: '',
                disabled: false,
                state: state
            }
        }))
}

export const refreshUpdateBankOpeningBalance = function (dispatch) {
    dispatch({
        type: ACTION_TYPES.UPDATE_BANK_OPENING,
        payload: {
            loading: false,
            message: "initial",
            disabled: false,
            error: '',
            state: {}
        }
    })
}

export const getBankOpeningBalanceList = function (businessid, fyid, dispatch) {
    dispatch({
        type: ACTION_TYPES.GET_BANK_OPENING_BALANCE_LOADING,
        payload: {
            loading: true,
            result: '',
            data: [],
            error: ''
        }
    });
    return fetchRetry(baseUrl + 'bankOpeningBalance/' + businessid + "/" + fyid, {
        credentials: "include"
    })
        .then((response) => response.json())
        .then((dataObject) => {
            if (dataObject.result === 'success') {
                dispatch({
                    type: ACTION_TYPES.GET_BANK_OPENING_BALANCE,
                    payload: {
                        message: dataObject.result,
                        error: '',
                        loading: false,
                        data: dataObject.data
                    }
                })
            }
            else {
                dispatch(
                    {
                        type: ACTION_TYPES.GET_BANK_OPENING_BALANCE,
                        payload: {
                            message: '',
                            error: dataObject.result,
                            loading: false,
                            data: []
                        }
                    }
                )
            }
        })
        .catch(err => {
            dispatch({
                type: ACTION_TYPES.GET_BANK_OPENING_BALANCE,
                payload: {
                    message: '',
                    error: err.message,
                    loading: false,

                }
            });
        })
}

export const resetBankOpeningBalance = function (dispatch) {
    dispatch({
        type: ACTION_TYPES.GET_BANK_OPENING_BALANCE,
        payload: {
            message: 'initial',
            error: '',
            loading: false,
            data: []
        }
    })
}

export const resetGSTOpeningBalance = function (dispatch) {
    dispatch({
        type: ACTION_TYPES.GET_GST_OPENING_BALANCE,
        payload: {
            message: 'initial',
            error: '',
            loading: false,
            igst: 0,
            cgst: 0,
            sgst: 0
        }
    })
}
export const resetGSTOpeningBalanceMessage = function (dispatch) {
    dispatch({
        type: ACTION_TYPES.UPDATE_GST_OPENING_BALANCE,
        payload: {
            loading: false,
            message: "initial",
            error: ''
        }
    })
}
export const getGSTOpeningBalanceDetail = function (businessid, fyid, dispatch) {
    dispatch({
        type: ACTION_TYPES.GET_GST_OPENING_BALANCE,
        payload: {
            loading: true,
            message: '',
            error: '',
            gst: 0,
            inputpayable: ''
        }
    });
    return fetchRetry(baseUrl + 'gstOpeningBalance/' + businessid + "/" + fyid, {
        credentials: "include"
    })
        .then((response) => response.json())
        .then((dataObject) => {
            if (dataObject.result === 'success') {
                dispatch({
                    type: ACTION_TYPES.GET_GST_OPENING_BALANCE,
                    payload: {
                        message: dataObject.result,
                        error: '',
                        loading: false,
                        gst: dataObject.data ? dataObject.data.gst : 0,
                        inputpayable: dataObject.data ? dataObject.data.inputpayable : ''
                    }
                })
            }
            else {
                dispatch(
                    {
                        type: ACTION_TYPES.GET_GST_OPENING_BALANCE,
                        payload: {
                            message: '',
                            error: dataObject.result,
                            loading: false,
                            gst: 0,
                            inputpayable: ''
                        }
                    }
                )
            }
        })
        .catch(err => {
            dispatch({
                type: ACTION_TYPES.GET_GST_OPENING_BALANCE,
                payload: {
                    message: '',
                    error: err.message,
                    loading: false,
                    gst: 0,
                    inputpayable: ''
                }
            });
        })
}
export const updateGSTOpeningBalance = function (businessid, fyid, state, dispatch) {
    dispatch({
        type: ACTION_TYPES.UPDATE_GST_OPENING_BALANCE_LOADING,
        payload: {
            loading: true,
            error: '',
            message: ''
        }
    })
    fetchRetry(baseUrl + 'gstOpeningBalance/' + businessid + "/" + fyid, {
        method: "POST",
        body: JSON.stringify(state),
        headers: {
            "Content-Type": "application/json"
        },
        credentials: "include"
    })
        .then(response => response.json())
        .then(dataObject => {
            if (dataObject.result === 'success') {
                dispatch({
                    type: ACTION_TYPES.GET_GST_OPENING_BALANCE,
                    payload: {
                        loading: false,
                        error: '',
                        message: '',
                        gst: state.gst,
                        inputpayable: state.inputpayable
                    }
                })
                dispatch({
                    type: ACTION_TYPES.UPDATE_GST_OPENING_BALANCE,
                    payload: {
                        loading: false,
                        error: '',
                        message: dataObject.result,
                    }
                })
            }
            else {
                dispatch({
                    type: ACTION_TYPES.UPDATE_GST_OPENING_BALANCE,
                    payload: {
                        loading: false,
                        message: '',
                        error: dataObject.result,
                        state: state
                    }
                })
            }
        })
        .catch(err => dispatch({
            type: ACTION_TYPES.UPDATE_GST_OPENING_BALANCE,
            payload: {
                loading: false,
                message: '',
                error: err.message,
                state: state
            }
        }))
}
export const refreshDebitCredit = function (dispatch) {
    dispatch({
        type: ACTION_TYPES.REFRESH_DEBIT_CREDIT,
        payload: {
            loading: false,
            message: 'initial',
            data: [],
            error: '',
            voucherno: ''
        }
    })
}
export const resetaddDebitCreditMessage = function (dispatch) {
    dispatch({
        type: ACTION_TYPES.RESET_ADD_DEBIT_CREDIT,
        payload: {}
    })
}
export const getDebitCreditList = function (businessid, fyid, dispatch) {
    dispatch({
        type: ACTION_TYPES.GET_DEBIT_CREDIT_LOADING,
        payload: {
            loading: true,
            message: '',
            data: [],
            error: '',
            voucherno: ''
        }
    });
    return fetchRetry(baseUrl + 'debitcredit/' + businessid + "/" + fyid, {
        credentials: "include"
    })
        .then(response => response.json())
        .then(dataObject => {
            if (dataObject.result === 'success') {
                let issuedvouchers = dataObject.data.filter(d => d.issuereceived === 'issue');
                issuedvouchers.sort(sortVoucherno);
                let voucherno = issuedvouchers[0] ? issuedvouchers[0].voucherno + 1 : 1
                dispatch({
                    type: ACTION_TYPES.GET_DEBIT_CREDIT,
                    payload: {
                        message: dataObject.result,
                        error: '',
                        loading: false,
                        data: dataObject.data,
                        voucherno: voucherno
                    }
                })
            }
            else {
                dispatch(
                    {
                        type: ACTION_TYPES.GET_DEBIT_CREDIT,
                        payload: {
                            message: '',
                            error: dataObject.result,
                            loading: false,
                            data: [],
                            voucherno: ''
                        }
                    }
                )
            }
        })
        .catch(err => {
            dispatch({
                type: ACTION_TYPES.GET_DEBIT_CREDIT,
                payload: {
                    message: '',
                    error: err.message,
                    loading: false,
                    data: [],
                    voucherno: ''
                }
            });
        })
}
export const raiseDebitCreditNote = function (businessid, fyid, state, dispatch) {
    dispatch({
        type: ACTION_TYPES.ADD_DEBIT_CREDIT_LOADING,
        payload: {
            loading: true,
            error: '',
            message: '',
            state: []
        }
    })
    fetchRetry(baseUrl + 'debitcredit/' + businessid + "/" + fyid, {
        method: "POST",
        body: JSON.stringify(state),
        headers: {
            "Content-Type": "application/json"
        },
        credentials: "include"
    })
        .then(response => response.json())
        .then(dataObject => {
            state = {
                ...state,
                businessid: businessid,
                fyid: fyid,
                type: "debitcredit"
            }
            if (dataObject.result === "success") {
                let data = {
                    ...state,
                    id: dataObject.id.id
                }
                dispatch({
                    type: ACTION_TYPES.PUSH_DEBIT_CREDIT,
                    payload: {
                        data: data,
                        voucherno: parseInt(state.voucherno) + 1
                    }
                })
                dispatch({
                    type: ACTION_TYPES.ADD_DEBIT_CREDIT,
                    payload: {
                        loading: false,
                        error: '',
                        message: dataObject.result,
                        state: []
                    }
                })
            }
            else if (dataObject.result === "Already Registered") {
                let vonum = parseInt(dataObject.data.voucherno);
                vonum = vonum + 1;
                dispatch({
                    type: ACTION_TYPES.PUSH_CHECK_DEBIT_CREDIT_DATA,
                    payload: {
                        data: state,
                        voucherno: vonum + 1
                    }
                })
                dispatch({
                    type: ACTION_TYPES.PUSH_CHECK_DEBIT_CREDIT_DATA,
                    payload: {
                        loading: false,
                        message: dataObject.result,
                        error: '',
                        state: []
                    }
                })
            }
            else {
                dispatch({
                    type: ACTION_TYPES.ADD_DEBIT_CREDIT,
                    payload: {
                        loading: false,
                        error: dataObject.result,
                        message: '',
                        state: state
                    }
                })
            }
        })
        .catch(err => {
            dispatch({
                type: ACTION_TYPES.ADD_DEBIT_CREDIT,
                payload: {
                    loading: false,
                    error: err.message,
                    message: '',
                    state: state
                }
            })
        })
}

export const deleteEntry = function (id, businessid, fyid, segment, dispatch) {
    dispatch({
        type: ACTION_TYPES.DELETE_ENTRY_LOADING,
        payload: {
            loading: true,
            error: '',
            message: ''
        }
    })
    if (segment !== "expensehead") {
        fetchRetry(baseUrl + 'deleteEntry/' + businessid + "/" + fyid, {
            method: "DELETE",
            body: JSON.stringify({ id: id, segment: segment }),
            headers: {
                "Content-Type": "application/json"
            },
            credentials: "include"
        })
            .then(response => response.json())
            .then(dataObject => {
                if (dataObject.result === 'success') {
                    dispatch({
                        type: ACTION_TYPES.DELETE_ENTRY,
                        payload: {
                            loading: false,
                            error: '',
                            message: dataObject.result,
                        }
                    })
                    if (segment === 'purchase') {
                        dispatch({
                            type: ACTION_TYPES.DELETE_PURCHASE,
                            payload: {
                                id: id
                            }
                        })
                    }
                    else if (segment === 'expense') {
                        dispatch({
                            type: ACTION_TYPES.DELETE_EXPENSE,
                            payload: {
                                id: id
                            }
                        })
                    }
                    else if (segment === "income") {
                        dispatch({
                            type: ACTION_TYPES.DELETE_INCOMEOS,
                            payload: {
                                id: id
                            }
                        })
                    }
                    else if (segment === "debitcredit") {
                        dispatch({
                            type: ACTION_TYPES.DELETE_DEBIT_CREDIT,
                            payload: {
                                id: id
                            }
                        })
                    }
                    else if (segment === "payment") {
                        dispatch({
                            type: ACTION_TYPES.DELETE_PAYMENT,
                            payload: {
                                id: id
                            }
                        })
                    }
                    else if (segment === "bankentry") {
                        dispatch({
                            type: ACTION_TYPES.DELETE_BANK_ENTRY,
                            payload: {
                                id: id
                            }
                        })
                    }
                    else if (segment === "loan") {
                        dispatch({
                            type: ACTION_TYPES.DELETE_LOAN,
                            payload: {
                                id: id
                            }
                        })
                    }
                    else if (segment === "investment") {
                        dispatch({
                            type: ACTION_TYPES.DELETE_INVESTMENT,
                            payload: {
                                id: id
                            }
                        })
                    }
                    else if (segment === "asset") {
                        dispatch({
                            type: ACTION_TYPES.DELETE_ASSET,
                            payload: {
                                dataObj: id
                            }
                        })
                    }
                    else if (segment === "depreciation") {
                        dispatch({
                            type: ACTION_TYPES.DELETE_DEPRECIATION,
                            payload: {
                                dataObj: id
                            }
                        })
                    }
                }
                else {
                    dispatch({
                        type: ACTION_TYPES.DELETE_ENTRY,
                        payload: {
                            loading: false,
                            message: '',
                            error: dataObject.result,
                        }
                    })
                }
            })
            .catch(err => dispatch({
                type: ACTION_TYPES.DELETE_ENTRY,
                payload: {
                    loading: false,
                    message: '',
                    error: err.message,
                }
            }))
    }
    else {
        fetchRetry(baseUrl + 'deleteEntry/' + businessid, {
            method: "DELETE",
            body: JSON.stringify({ id: id, segment: segment }),
            headers: {
                "Content-Type": "application/json"
            },
            credentials: "include"
        })
            .then(response => response.json())
            .then(dataObject => {
                if (dataObject.result === 'success') {
                    dispatch({
                        type: ACTION_TYPES.DELETE_ENTRY,
                        payload: {
                            loading: false,
                            error: '',
                            message: dataObject.result,
                        }
                    })
                    if (segment === "expensehead") {
                        dispatch({
                            type: ACTION_TYPES.DELETE_EXPENSE_HEAD,
                            payload: {
                                id: id
                            }
                        })
                    }
                    else if (segment === "incomehead") {
                        dispatch({
                            type: ACTION_TYPES.DELETE_INCOME_HEAD,
                            payload: {
                                id: id
                            }
                        })
                    }
                }
                else {
                    dispatch({
                        type: ACTION_TYPES.DELETE_ENTRY,
                        payload: {
                            loading: false,
                            message: '',
                            error: dataObject.result,
                        }
                    })
                }
            })
            .catch(err => dispatch({
                type: ACTION_TYPES.DELETE_ENTRY,
                payload: {
                    loading: false,
                    message: '',
                    error: err.message,
                }
            }))
    }
}

export const resetDeleteEntry = function (dispatch) {
    dispatch({
        type: ACTION_TYPES.DELETE_ENTRY,
        payload: {
            loading: false,
            error: '',
            message: 'initial'
        }
    })
}


export const getExpenseHeadList = function (businessid, dispatch) {
    dispatch({
        type: ACTION_TYPES.GET_EXPENSE_HEAD_LOADING,
        payload: {
            loading: true,
            message: '',
            data: [],
            error: ''
        }
    });
    return fetchRetry(baseUrl + 'expensehead/' + businessid, {
        credentials: "include"
    })
        .then(response => response.json())
        .then(dataObject => {
            if (dataObject.result === 'success') {
                dispatch({
                    type: ACTION_TYPES.GET_EXPENSE_HEAD,
                    payload: {
                        loading: false,
                        message: dataObject.result,
                        data: dataObject.data,
                        error: ''
                    }
                })
            }
            else {
                dispatch({
                    type: ACTION_TYPES.GET_EXPENSE_HEAD,
                    payload: {
                        loading: false,
                        message: '',
                        data: [],
                        error: "Error : " + dataObject.result
                    }
                })
            }
        })
        .catch(err => {
            dispatch({
                type: ACTION_TYPES.GET_EXPENSE_HEAD,
                payload: {
                    data: [],
                    error: "Error : " + err.message,
                    loading: false,
                    message: ''
                }
            });
        })
}

export const addExpenseHead = function (expenseheadname, businessid, dispatch) {
    dispatch({
        type: ACTION_TYPES.ADD_EXPENSE_HEAD_LOADING,
        payload: {
            loading: true,
            disabled: true,
            message: '',
            error: ''
        }
    });
    const data = {
        expenseheadname: expenseheadname
    }
    return fetchRetry(baseUrl + 'expensehead/' + businessid, {
        method: "POST",
        body: JSON.stringify(data),
        headers: {
            "Content-Type": "application/json"
        },
        credentials: "include"
    })
        .then(response => response.json())
        .then(dataObject => {
            if (dataObject.result === 'success') {
                let data = {
                    expenseheadname: expenseheadname,
                    expenseheadid: dataObject.id.expenseheadid,
                    businessid: businessid
                }
                dispatch({
                    type: ACTION_TYPES.PUSH_EXPENSE_HEAD,
                    payload: {
                        data: data
                    }
                })
                dispatch({
                    type: ACTION_TYPES.ADD_EXPENSE_HEAD,
                    payload: {
                        loading: false,
                        disabled: false,
                        message: dataObject.result,
                        error: ''
                    }
                })
            }
            else {
                dispatch({
                    type: ACTION_TYPES.ADD_EXPENSE_HEAD,
                    payload: {
                        loading: false,
                        disabled: false,
                        error: dataObject.result,
                        message: ''
                    }
                })
            }
        })
        .catch((err) => dispatch({
            type: ACTION_TYPES.ADD_EXPENSE_HEAD,
            payload: {
                loading: false,
                disabled: false,
                error: "Error : " + err.message,
                message: ''
            }
        }))
}

export const refreshExpenseHead = function (dispatch) {
    dispatch({
        type: ACTION_TYPES.REFRESH_EXPENSE_HEAD,
        payload: {
            message: 'initial',
            loading: false,
            disabled: false,
            error: ''
        }
    });
}

export const resetAddExpenseHeadMessageAndForm = function (dispatch) {
    dispatch({
        type: ACTION_TYPES.RESET_ADD_EXPENSE_HEAD_MESSAGE,
        payload: {}
    });
    dispatch(actions.reset('addExpenseHead'));
}
export const resetAddExpenseHeadMessage = function (dispatch) {
    dispatch({
        type: ACTION_TYPES.RESET_ADD_EXPENSE_HEAD_MESSAGE,
        payload: {}
    });
}

export const getIncomeOSDetail = function (businessid, fyid, dispatch) {
    dispatch({
        type: ACTION_TYPES.GET_INCOMEOS_LOADING,
        payload: {
            loading: true,
            message: '',
            data: [],
            error: ''
        }
    })
    fetchRetry(baseUrl + 'raiseincomeos/' + businessid + "/" + fyid, {
        credentials: "include"
    })
        .then(response => response.json())
        .then(dataObject => {
            if (dataObject.result === 'success') {
                let incomeosData = dataObject.data;
                let voucherno = 1;
                if (incomeosData.length > 0) {
                    incomeosData = incomeosData.sort(sortVoucherno);
                    voucherno = parseInt(incomeosData[0].voucherno) + 1
                }
                dispatch({
                    type: ACTION_TYPES.GET_INCOMEOS,
                    payload: {
                        message: dataObject.result,
                        error: '',
                        loading: false,
                        data: dataObject.data,
                        voucherno: voucherno
                    }
                })
            }
            else {
                dispatch(
                    {
                        type: ACTION_TYPES.GET_INCOMEOS,
                        payload: {
                            message: '',
                            error: dataObject.result,
                            loading: false,
                            data: []
                        }
                    }
                )
            }
        })
        .catch(err => dispatch(
            {
                type: ACTION_TYPES.GET_INCOMEOS,
                payload: {
                    message: '',
                    error: err.message,
                    loading: false,
                    data: []
                }
            }
        )

        )
}
export const raiseIncomeOS = function (businessid, fyid, state, dispatch) {
    dispatch({
        type: ACTION_TYPES.ADD_INCOMEOS_LOADING,
        payload: {
            loading: true,
            error: '',
            message: '',
            state: []
        }
    })
    let dataobj = {
        date: state.date,
        description: state.description,
        category: state.category,
        amount: state.amount,
        voucherno: state.voucherno,
        paymentmode: state.paymentmode,
        transactionno: state.transactionno,
        bankid: state.bankid,
        tds: state.tds,
        tdsamount: state.tdsamount,
        cash: state.cash,
        customerid: state.customerid.split("...")[0],
        customername: state.customerid.split("...")[1],
    };
    fetchRetry(baseUrl + 'raiseincomeos/' + businessid + "/" + fyid, {
        method: "POST",
        body: JSON.stringify(dataobj),
        headers: {
            "Content-Type": "application/json"
        },
        credentials: "include"
    })
        .then(response => response.json())
        .then(dataObject => {
            if (dataObject.result === "success") {
                let data = {
                    date: state.date,
                    description: state.description,
                    category: state.category,
                    amount: state.amount,
                    voucherno: state.voucherno,
                    paymentmode: state.paymentmode,
                    transactionno: state.transactionno,
                    bankdetail: state.bankid,
                    tds: state.tds === true ? "1" : '',
                    tdsamount: state.tdsamount,
                    incomeid: dataObject.id.incomeid,
                    businessid: businessid,
                    fyid: fyid,
                    type: "Income",
                    cash: state.cash === true ? "1" : '',
                    customerid: state.customerid.split("...")[0],
                    customername: state.customerid.split("...")[1]
                }
                dispatch({
                    type: ACTION_TYPES.PUSH_INCOMEOS,
                    payload: {
                        data: data,
                        voucherno: parseInt(state.voucherno) + 1
                    }
                })
                dispatch({
                    type: ACTION_TYPES.ADD_INCOMEOS,
                    payload: {
                        loading: false,
                        message: dataObject.result,
                        error: '',
                        state: []
                    }
                })
            }
            else if (dataObject.result === "Already Registered") {
                let vonum = parseInt(dataObject.data.voucherno) + 1;
                vonum = vonum + 1;
                dispatch({
                    type: ACTION_TYPES.PUSH_CHECK_INCOMEOS,
                    payload: {
                        data: dataObject.data,
                        voucherno: vonum
                    }
                })
                dispatch({
                    type: ACTION_TYPES.ADD_INCOMEOS,
                    payload: {
                        loading: false,
                        message: dataObject.result,
                        error: '',
                        state: []
                    }
                })
            }
            else {
                dispatch({
                    type: ACTION_TYPES.ADD_INCOMEOS,
                    payload: {
                        loading: false,
                        message: '',
                        error: dataObject.result,
                        state: state
                    }
                })
            }
        })
        .catch(err => {
            dispatch({
                type: ACTION_TYPES.ADD_INCOMEOS,
                payload: {
                    loading: false,
                    message: '',
                    error: err.message,
                    state: state
                }
            })
        })
}
export const resetaddIncomeOSMessage = function (dispatch) {
    dispatch({
        type: ACTION_TYPES.RESET_ADD_INCOMEOS,
        payload: {}
    })
}
export const resetIncomeOSDetail = function (dispatch) {
    dispatch({
        type: ACTION_TYPES.REFRESH_INCOMEOS,
        payload: {}
    })
}
export const reviseSalary = function (state, businessid, employeeid, getEmployee, dispatch) {
    dispatch({
        type: ACTION_TYPES.REVISE_SALARY_LOADING,
        payload: {
            loading: true,
            error: '',
            message: '',
            state: {}
        }
    })
    let employee = getEmployee.data.filter(e => e.employeeid === employeeid)[0];
    let salary = JSON.parse(employee.salary);
    let keys = Object.keys(salary);
    let i = keys.length;
    salary[i] = JSON.stringify({
        salary: state.salary,
        date: state.month + " " + state.year
    })
    fetchRetry(baseUrl + 'reviseSalary/' + businessid + "/" + employeeid, {
        method: "PUT",
        body: JSON.stringify({ salary: JSON.stringify(salary) }),
        headers: {
            "Content-Type": "application/json"
        },
        credentials: "include"
    })
        .then(response => response.json())
        .then(dataObject => {
            if (dataObject.result === "success") {
                dispatch({
                    type: ACTION_TYPES.DELETE_AND_PUSH_SALARY,
                    payload: {
                        employeeid: employeeid,
                        salary: JSON.stringify(salary)
                    }
                })
                dispatch({
                    type: ACTION_TYPES.REVISE_SALARY,
                    payload: {
                        loading: false,
                        message: dataObject.result,
                        error: '',
                        state: {}
                    }
                })
            }
            else {
                dispatch({
                    type: ACTION_TYPES.REVISE_SALARY,
                    payload: {
                        loading: false,
                        message: '',
                        error: dataObject.result,
                        state: state
                    }
                })
            }
        })
        .catch(err => {
            dispatch({
                type: ACTION_TYPES.REVISE_SALARY,
                payload: {
                    loading: false,
                    message: '',
                    error: err.message,
                    state: state
                }
            })
        })
}
export const resetReviseSalaryMessage = function (dispatch) {
    dispatch({
        type: ACTION_TYPES.REVISE_SALARY,
        payload: {
            loading: false,
            message: '',
            error: '',
            state: {}
        }
    })
}

export const terminateEmployee = function (businessid, employeeid, state, dispatch) {
    dispatch({
        type: ACTION_TYPES.TERMINATE_EMPLOYEE_LOADING,
        payload: {
            loading: true,
            error: '',
            message: '',
            state: {}
        }
    })
    fetchRetry(baseUrl + 'termninateEmployee/' + businessid + "/" + employeeid, {
        method: "PUT",
        body: JSON.stringify({ dot: state.dot }),
        headers: {
            "Content-Type": "application/json"
        },
        credentials: "include"
    })
        .then(response => response.json())
        .then(dataObject => {
            if (dataObject.result === "success") {
                dispatch({
                    type: ACTION_TYPES.UPDATE_EMPLOYEE_DOT,
                    payload: {
                        employeeid: employeeid,
                        dot: state.dot
                    }
                })
                dispatch({
                    type: ACTION_TYPES.TERMINATE_EMPLOYEE,
                    payload: {
                        loading: false,
                        message: dataObject.result,
                        error: '',
                        state: {}
                    }
                })
            }
            else {
                dispatch({
                    type: ACTION_TYPES.TERMINATE_EMPLOYEE,
                    payload: {
                        loading: false,
                        message: '',
                        error: dataObject.result,
                        state: state
                    }
                })
            }
        })
        .catch(err => {
            dispatch({
                type: ACTION_TYPES.TERMINATE_EMPLOYEE,
                payload: {
                    loading: false,
                    message: '',
                    error: err.message,
                    state: state
                }
            })
        })
}

export const resetTerminateEmployeeMessage = function (dispatch) {
    dispatch({
        type: ACTION_TYPES.TERMINATE_EMPLOYEE,
        payload: {
            loading: false,
            error: '',
            message: '',
            state: {}
        }
    })
}

export const changePassword = function (state, dispatch) {
    dispatch({
        type: ACTION_TYPES.CHANGE_PASSWORD_LOADING,
        payload: {
            loading: true,
            error: '',
            message: '',
            state: {}
        }
    })
    fetchRetry(baseUrl + 'changepassword', {
        method: "PUT",
        body: JSON.stringify({ oldpassword: state.oldpassword, newpassword: state.newpassword }),
        headers: {
            "Content-Type": "application/json"
        },
        credentials: "include"
    })
        .then(response => response.json())
        .then(dataObject => {
            if (dataObject.result === "success") {
                dispatch({
                    type: ACTION_TYPES.CHANGE_PASSWORD,
                    payload: {
                        loading: false,
                        message: dataObject.result,
                        error: '',
                        state: {}
                    }
                })
            }
            else {
                dispatch({
                    type: ACTION_TYPES.CHANGE_PASSWORD,
                    payload: {
                        loading: false,
                        message: '',
                        error: dataObject.result,
                        state: state
                    }
                })
            }
        })
        .catch(err => {
            dispatch({
                type: ACTION_TYPES.CHANGE_PASSWORD,
                payload: {
                    loading: false,
                    message: '',
                    error: err.message,
                    state: state
                }
            })
        })
}
export const resetChangePasswordMessage = function (dispatch) {
    dispatch({
        type: ACTION_TYPES.CHANGE_PASSWORD,
        payload: {
            loading: false,
            message: 'initial',
            error: '',
            state: {}
        }
    })
}

export const resetaddLoanMessage = function (dispatch) {
    dispatch({
        type: ACTION_TYPES.ADD_LOAN,
        payload: {
            loading: false,
            message: 'initial',
            error: '',
            state: {}
        }
    })
}

export const raiseLoan = function (businessid, fyid, state, fy, dispatch) {
    dispatch({
        type: ACTION_TYPES.ADD_LOAN_LOADING,
        payload: {
            loading: true,
            error: '',
            message: '',
            state: {}
        }
    })
    state = {
        ...state,
        fy: fy
    }
    fetchRetry(baseUrl + 'raiseloan/' + businessid + "/" + fyid, {
        method: "POST",
        body: JSON.stringify(state),
        headers: {
            "Content-Type": "application/json"
        },
        credentials: "include"
    })
        .then(response => response.json())
        .then(dataObject => {
            if (dataObject.result === "success") {
                let data = {
                    ...state,
                    id: dataObject.id.id,
                    businessid: businessid,
                    fyid: fyid,
                    type: "loan"
                }
                dispatch({
                    type: ACTION_TYPES.PUSH_LOAN,
                    payload: {
                        data: data,
                        voucherno: parseInt(state.voucherno) + 1
                    }
                })
                dispatch({
                    type: ACTION_TYPES.ADD_LOAN,
                    payload: {
                        loading: false,
                        message: dataObject.result,
                        error: '',
                        state: {}
                    }
                })
            }
            else if (dataObject.result === "Already Registered") {
                let vonum = parseInt(dataObject.data.voucherno);
                vonum = vonum + 1;
                dispatch({
                    type: ACTION_TYPES.PUSH_CHECK_LOAN,
                    payload: {
                        data: dataObject.data,
                        voucherno: vonum
                    }
                })
                dispatch({
                    type: ACTION_TYPES.ADD_LOAN,
                    payload: {
                        loading: false,
                        message: dataObject.result,
                        error: '',
                        state: {}
                    }
                })
            }
            else {
                dispatch({
                    type: ACTION_TYPES.ADD_LOAN,
                    payload: {
                        loading: false,
                        message: '',
                        error: dataObject.result,
                        state: state
                    }
                })
            }
        })
        .catch(err => {
            dispatch({
                type: ACTION_TYPES.ADD_LOAN,
                payload: {
                    loading: false,
                    message: '',
                    error: err.message,
                    state: state
                }
            })
        })
}

export const getLoanDetail = function (businessid, fyid, dispatch) {
    dispatch({
        type: ACTION_TYPES.GET_LOAN_LOADING,
        payload: {
            loading: true,
            message: '',
            data: [],
            error: ''
        }
    })
    fetchRetry(baseUrl + 'raiseloan/' + businessid + "/" + fyid, {
        credentials: "include"
    })
        .then(response => response.json())
        .then(dataObject => {
            if (dataObject.result === 'success') {
                let loanData = dataObject.data;
                let voucherno = 1;
                if (loanData.length > 0) {
                    loanData = loanData.sort(sortVoucherno);
                    voucherno = parseInt(loanData[0].voucherno) + 1;
                }
                dispatch({
                    type: ACTION_TYPES.GET_LOAN,
                    payload: {
                        message: dataObject.result,
                        error: '',
                        loading: false,
                        data: dataObject.data,
                        voucherno: voucherno
                    }
                })
            }
            else {
                dispatch(
                    {
                        type: ACTION_TYPES.GET_LOAN,
                        payload: {
                            message: '',
                            error: dataObject.result,
                            loading: false,
                            data: []
                        }
                    }
                )
            }
        })
        .catch(err => dispatch(
            {
                type: ACTION_TYPES.GET_LOAN,
                payload: {
                    message: '',
                    error: err.message,
                    loading: false,
                    data: []
                }
            }
        ))
}

export const refreshLoanState = function (dispatch) {
    dispatch({
        type: ACTION_TYPES.GET_LOAN,
        payload: {
            message: 'initial',
            error: '',
            loading: false,
            data: []
        }
    })
}

export const refreshLoanAccountState = function (dispatch) {
    dispatch({
        type: ACTION_TYPES.GET_LOAN_ACCOUNT,
        payload: {
            loading: false,
            message: 'initial',
            data: [],
            error: ''
        }
    })
}

export const getLoanAccountList = function (businessid, dispatch) {
    dispatch({
        type: ACTION_TYPES.GET_LOAN_ACCOUNT_LOADING,
        payload: {
            loading: true,
            message: '',
            data: [],
            error: ''
        }
    });
    return fetchRetry(baseUrl + 'loanaccount/' + businessid, {
        credentials: "include"
    })
        .then(response => response.json())
        .then(dataObject => {
            if (dataObject.result === 'success') {
                dispatch({
                    type: ACTION_TYPES.GET_LOAN_ACCOUNT,
                    payload: {
                        loading: false,
                        message: dataObject.result,
                        data: dataObject.data,
                        error: ''
                    }
                })
            }
            else {
                dispatch({
                    type: ACTION_TYPES.GET_LOAN_ACCOUNT,
                    payload: {
                        loading: false,
                        message: '',
                        data: [],
                        error: "Error : " + dataObject.result
                    }
                })
            }
        })
        .catch(err => {
            dispatch({
                type: ACTION_TYPES.GET_LOAN_ACCOUNT,
                payload: {
                    data: [],
                    error: "Error : " + err.message,
                    loading: false,
                    message: ''
                }
            });
        })
}
export const resetAddLoanAccountMessage = function (dispatch) {
    dispatch({
        type: ACTION_TYPES.ADD_LOAN_ACCOUNT,
        payload: {
            loading: false,
            message: '',
            error: '',
            state: {}
        }
    })
}
export const resetAddLoanAccountMessageAndForm = function (dispatch) {
    dispatch({
        type: ACTION_TYPES.ADD_LOAN_ACCOUNT,
        payload: {
            loading: false,
            message: '',
            error: '',
            state: {}
        }
    })
    dispatch(actions.reset('addLoanDetail'));
}

export const addLoanAccount = function (values, businessid, dispatch) {
    dispatch({
        type: ACTION_TYPES.ADD_LOAN_ACCOUNT_LOADING,
        payload: {
            loading: true,
            disabled: true,
            message: '',
            error: ''
        }
    });
    return fetchRetry(baseUrl + 'loanaccount/' + businessid, {
        method: "POST",
        body: JSON.stringify(values),
        headers: {
            "Content-Type": "application/json"
        },
        credentials: "include"
    })
        .then(response => response.json())
        .then(dataObject => {
            let bd = { ...values };
            bd.id = dataObject.id.id;
            bd.businessid = businessid;
            if (dataObject.result === 'success') {
                dispatch({
                    type: ACTION_TYPES.PUSH_NEW_LOAN_ACCOUNT,
                    payload: {
                        loanaccount: bd
                    }
                })
                dispatch({
                    type: ACTION_TYPES.ADD_LOAN_ACCOUNT,
                    payload: {
                        loading: false,
                        disabled: false,
                        message: dataObject.result,
                        error: ''
                    }
                })
            }
            else {
                dispatch({
                    type: ACTION_TYPES.ADD_LOAN_ACCOUNT,
                    payload: {
                        loading: false,
                        disabled: false,
                        error: dataObject.result,
                        message: ''
                    }
                })
            }
        })
        .catch((err) => dispatch({
            type: ACTION_TYPES.ADD_LOAN_ACCOUNT,
            payload: {
                loading: false,
                disabled: false,
                error: "Error : " + err.message,
                message: ''
            }
        }))
}

export const refreshInvestmentState = function (dispatch) {
    dispatch({
        type: ACTION_TYPES.GET_INVESTMENT,
        payload: {
            message: 'initial',
            error: '',
            loading: false,
            data: []
        }
    })
}

export const getInvestmentDetail = function (businessid, fyid, dispatch) {
    dispatch({
        type: ACTION_TYPES.GET_INVESTMENT_LOADING,
        payload: {
            loading: true,
            message: '',
            data: [],
            error: ''
        }
    })
    fetchRetry(baseUrl + 'raiseinvestment/' + businessid + "/" + fyid, {
        credentials: "include"
    })
        .then(response => response.json())
        .then(dataObject => {
            if (dataObject.result === 'success') {
                let investmentData = dataObject.data;
                let voucherno = 1;
                if (investmentData.length > 0) {
                    investmentData = investmentData.sort(sortVoucherno);
                    voucherno = parseInt(investmentData[0].voucherno) + 1;
                }
                dispatch({
                    type: ACTION_TYPES.GET_INVESTMENT,
                    payload: {
                        message: dataObject.result,
                        error: '',
                        loading: false,
                        data: dataObject.data,
                        voucherno: voucherno
                    }
                })
            }
            else {
                dispatch(
                    {
                        type: ACTION_TYPES.GET_INVESTMENT,
                        payload: {
                            message: '',
                            error: dataObject.result,
                            loading: false,
                            data: []
                        }
                    }
                )
            }
        })
        .catch(err => dispatch(
            {
                type: ACTION_TYPES.GET_INVESTMENT,
                payload: {
                    message: '',
                    error: err.message,
                    loading: false,
                    data: []
                }
            }
        ))
}

export const resetaddInvestmentMessage = function (dispatch) {
    dispatch({
        type: ACTION_TYPES.ADD_INVESTMENT,
        payload: {
            loading: false,
            message: 'initial',
            error: '',
            state: {}
        }
    })
}

export const raiseInvestment = function (businessid, fyid, state, fy, dispatch) {
    dispatch({
        type: ACTION_TYPES.ADD_INVESTMENT_LOADING,
        payload: {
            loading: true,
            error: '',
            message: '',
            state: {}
        }
    })
    state = {
        ...state,
        fy: fy
    }
    fetchRetry(baseUrl + 'raiseinvestment/' + businessid + "/" + fyid, {
        method: "POST",
        body: JSON.stringify(state),
        headers: {
            "Content-Type": "application/json"
        },
        credentials: "include"
    })
        .then(response => response.json())
        .then(dataObject => {
            if (dataObject.result === "success") {
                let data = {
                    ...state,
                    id: dataObject.id.id,
                    businessid: businessid,
                    fyid: fyid,
                    type: "investment",
                    sellinvestment: state.sellinvestment === true ? "1" : ''
                }
                dispatch({
                    type: ACTION_TYPES.PUSH_INVESTMENT,
                    payload: {
                        data: data,
                        voucherno: parseInt(state.voucherno) + 1
                    }
                })
                dispatch({
                    type: ACTION_TYPES.ADD_INVESTMENT,
                    payload: {
                        loading: false,
                        message: dataObject.result,
                        error: '',
                        state: {}
                    }
                })
            }
            else if (dataObject.result === "Already Registered") {
                let vonum = parseInt(dataObject.data.voucherno);
                vonum = vonum + 1;
                dispatch({
                    type: ACTION_TYPES.PUSH_CHECK_INVESTMENT,
                    payload: {
                        data: dataObject.data,
                        voucherno: vonum
                    }
                })
                dispatch({
                    type: ACTION_TYPES.ADD_INVESTMENT,
                    payload: {
                        loading: false,
                        message: dataObject.result,
                        error: '',
                        state: {}
                    }
                })
            }
            else {
                dispatch({
                    type: ACTION_TYPES.ADD_INVESTMENT,
                    payload: {
                        loading: false,
                        message: '',
                        error: dataObject.result,
                        state: state
                    }
                })
            }
        })
        .catch(err => {
            dispatch({
                type: ACTION_TYPES.ADD_INVESTMENT,
                payload: {
                    loading: false,
                    message: '',
                    error: err.message,
                    state: state
                }
            })
        })
}

export const getIncomeHeadList = function (businessid, dispatch) {
    dispatch({
        type: ACTION_TYPES.GET_INCOME_HEAD_LOADING,
        payload: {
            loading: true,
            message: '',
            data: [],
            error: ''
        }
    });
    return fetchRetry(baseUrl + 'incomehead/' + businessid, {
        credentials: "include"
    })
        .then(response => response.json())
        .then(dataObject => {
            if (dataObject.result === 'success') {
                dispatch({
                    type: ACTION_TYPES.GET_INCOME_HEAD,
                    payload: {
                        loading: false,
                        message: dataObject.result,
                        data: dataObject.data,
                        error: ''
                    }
                })
            }
            else {
                dispatch({
                    type: ACTION_TYPES.GET_INCOME_HEAD,
                    payload: {
                        loading: false,
                        message: '',
                        data: [],
                        error: "Error : " + dataObject.result
                    }
                })
            }
        })
        .catch(err => {
            dispatch({
                type: ACTION_TYPES.GET_INCOME_HEAD,
                payload: {
                    data: [],
                    error: "Error : " + err.message,
                    loading: false,
                    message: ''
                }
            });
        })
}
export const refreshIncomeHead = function (dispatch) {
    dispatch({
        type: ACTION_TYPES.REFRESH_INCOME_HEAD,
        payload: {
            message: 'initial',
            loading: false,
            disabled: false,
            error: ''
        }
    });
}

export const resetAddIncomeHeadMessageAndForm = function (dispatch) {
    dispatch({
        type: ACTION_TYPES.RESET_ADD_INCOME_HEAD_MESSAGE,
        payload: {}
    });
    dispatch(actions.reset('addIncomeHead'));
}
export const resetAddIncomeHeadMessage = function (dispatch) {
    dispatch({
        type: ACTION_TYPES.RESET_ADD_INCOME_HEAD_MESSAGE,
        payload: {}
    });
}
export const addIncomeHead = function (incomeheadname, businessid, dispatch) {
    dispatch({
        type: ACTION_TYPES.ADD_INCOME_HEAD_LOADING,
        payload: {
            loading: true,
            disabled: true,
            message: '',
            error: ''
        }
    });
    const data = {
        incomeheadname: incomeheadname
    }
    return fetchRetry(baseUrl + 'incomehead/' + businessid, {
        method: "POST",
        body: JSON.stringify(data),
        headers: {
            "Content-Type": "application/json"
        },
        credentials: "include"
    })
        .then(response => response.json())
        .then(dataObject => {
            if (dataObject.result === 'success') {
                let data = {
                    incomeheadname: incomeheadname,
                    expenseheadid: dataObject.id.expenseheadid,
                    businessid: businessid
                }
                dispatch({
                    type: ACTION_TYPES.PUSH_INCOME_HEAD,
                    payload: {
                        data: data
                    }
                })
                dispatch({
                    type: ACTION_TYPES.ADD_INCOME_HEAD,
                    payload: {
                        loading: false,
                        disabled: false,
                        message: dataObject.result,
                        error: ''
                    }
                })
            }
            else {
                dispatch({
                    type: ACTION_TYPES.ADD_INCOME_HEAD,
                    payload: {
                        loading: false,
                        disabled: false,
                        error: dataObject.result,
                        message: ''
                    }
                })
            }
        })
        .catch((err) => dispatch({
            type: ACTION_TYPES.ADD_INCOME_HEAD,
            payload: {
                loading: false,
                disabled: false,
                error: "Error : " + err.message,
                message: ''
            }
        }))
}

export const refreshAssetAccountState = function (dispatch) {
    dispatch({
        type: ACTION_TYPES.GET_ASSET_ACCOUNT,
        payload: {
            loading: false,
            message: 'initial',
            data: [],
            error: ''
        }
    })
}

export const getAssetAccountList = function (businessid, dispatch) {
    dispatch({
        type: ACTION_TYPES.GET_ASSET_ACCOUNT_LOADING,
        payload: {
            loading: true,
            message: '',
            data: [],
            error: ''
        }
    });
    return fetchRetry(baseUrl + 'assetaccount/' + businessid, {
        credentials: "include"
    })
        .then(response => response.json())
        .then(dataObject => {
            if (dataObject.result === 'success') {
                dispatch({
                    type: ACTION_TYPES.GET_ASSET_ACCOUNT,
                    payload: {
                        loading: false,
                        message: dataObject.result,
                        data: dataObject.data,
                        error: ''
                    }
                })
            }
            else {
                dispatch({
                    type: ACTION_TYPES.GET_ASSET_ACCOUNT,
                    payload: {
                        loading: false,
                        message: '',
                        data: [],
                        error: "Error : " + dataObject.result
                    }
                })
            }
        })
        .catch(err => {
            dispatch({
                type: ACTION_TYPES.GET_ASSET_ACCOUNT,
                payload: {
                    data: [],
                    error: "Error : " + err.message,
                    loading: false,
                    message: ''
                }
            });
        })
}
export const resetAddAssetAccountMessage = function (dispatch) {
    dispatch({
        type: ACTION_TYPES.ADD_ASSET_ACCOUNT,
        payload: {
            loading: false,
            message: '',
            error: '',
            state: {}
        }
    })
}
export const resetAddAssetAccountMessageAndForm = function (dispatch) {
    dispatch({
        type: ACTION_TYPES.ADD_ASSET_ACCOUNT,
        payload: {
            loading: false,
            message: '',
            error: '',
            state: {}
        }
    })
    dispatch(actions.reset('addAssetDetail'));
}

export const addAssetAccount = function (state, businessid, dispatch) {
    dispatch({
        type: ACTION_TYPES.ADD_ASSET_ACCOUNT_LOADING,
        payload: {
            loading: true,
            disabled: true,
            message: '',
            error: ''
        }
    });
    return fetchRetry(baseUrl + 'assetaccount/' + businessid, {
        method: "POST",
        body: JSON.stringify(state),
        headers: {
            "Content-Type": "application/json"
        },
        credentials: "include"
    })
        .then(response => response.json())
        .then(dataObject => {
            let bd = { ...state };
            bd.id = dataObject.id.id;
            bd.businessid = businessid;
            bd.depreciating = state.depreciating === true ? "1" : "0";
            bd.purchasedate = state.date;
            bd.purchasebasic = state.amount;
            if (dataObject.result === 'success') {
                dispatch({
                    type: ACTION_TYPES.PUSH_NEW_ASSET_ACCOUNT,
                    payload: {
                        assetaccount: bd
                    }
                })
                dispatch({
                    type: ACTION_TYPES.ADD_ASSET_ACCOUNT,
                    payload: {
                        loading: false,
                        disabled: false,
                        message: dataObject.result,
                        error: '',
                        state: state
                    }
                })
            }
            else {
                dispatch({
                    type: ACTION_TYPES.ADD_ASSET_ACCOUNT,
                    payload: {
                        loading: false,
                        disabled: false,
                        error: dataObject.result,
                        message: '',
                        state: state
                    }
                })
            }
        })
        .catch((err) => dispatch({
            type: ACTION_TYPES.ADD_ASSET_ACCOUNT,
            payload: {
                loading: false,
                disabled: false,
                error: "Error : " + err.message,
                message: '',
                state: state
            }
        }))
}

export const refreshAssetState = function (dispatch) {
    dispatch({
        type: ACTION_TYPES.GET_ASSET,
        payload: {
            message: 'initial',
            error: '',
            loading: false,
            data: []
        }
    })
}

export const raiseAsset = function (businessid, fyid, state, dispatch) {
    let assetInformation = {
        date: state.invoicedate,
        voucherno: state.invoicenumber,
        totalbasic: state.totalamountbasic,
        totaligst: state.totaligst,
        totalcgst: state.totalcgst,
        totalsgst: state.totalsgst,
        grandtotal: state.grandtotal,
        asset: state.asset,
        assetid: state.assetid,
        purchasefyid: fyid,
        vendorid: state.vendorid,
        desc: "assetPurchase",
        type: "asset"
    }
    dispatch({
        type: ACTION_TYPES.ADD_ASSET_LOADING,
        payload: {
            loading: true,
            message: '',
            error: ''
        }
    });
    fetchRetry(baseUrl + 'assetaccount/' + businessid, {
        method: "PUT",
        body: JSON.stringify(assetInformation),
        headers: {
            "Content-Type": "application/json"
        },
        credentials: "include"
    })
        .then(response => response.json())
        .then(dataObject => {
            if (dataObject.result === 'success') {
                assetInformation = {
                    ...assetInformation,
                    id: dataObject.id.id
                }
                dispatch({
                    type: ACTION_TYPES.UPDATE_ASSET_DATA_PURCHASE,
                    payload: {
                        assetInformation: assetInformation
                    }
                })
                dispatch({
                    type: ACTION_TYPES.ADD_ASSET,
                    payload: {
                        loading: false,
                        message: 'success',
                        error: ''
                    }
                })
            }
            else {
                dispatch({
                    type: ACTION_TYPES.ADD_ASSET,
                    payload: {
                        loading: false,
                        message: '',
                        error: "Error : " + dataObject.result,
                        state: state
                    }
                })
            }
        })
        .catch(err => dispatch({
            type: ACTION_TYPES.ADD_ASSET,
            payload: {
                loading: false,
                error: "Error " + err.message,
                message: '',
                state: state
            }
        }))
}
export const resetaddAssetMessage = function (dispatch) {
    dispatch({
        type: ACTION_TYPES.ADD_ASSET,
        payload: {
            loading: false,
            error: '',
            message: '',
        }
    })
}
export const sellAsset = function (businessid, fyid, state, dispatch) {
    const assetInformation = {
        date: state.invoicedate,
        voucherno: parseInt(state.invoicenumber),
        totalbasic: state.totalamountbasic,
        totaligst: state.totaligst,
        totalcgst: state.totalcgst,
        totalsgst: state.totalsgst,
        grandtotal: state.grandtotal,
        asset: state.asset,
        assetid: state.assetid,
        soldfyid: fyid,
        customerid: state.customername === "cash" ? state.customername : state.customerid + "..." + state.customername,
        desc: "assetSell",
        type: "asset"
    }
    dispatch({
        type: ACTION_TYPES.ADD_ASSET_LOADING,
        payload: {
            loading: true,
            message: '',
            error: ''
        }
    });
    fetchRetry(baseUrl + 'assetaccount/' + businessid, {
        method: "PUT",
        body: JSON.stringify(assetInformation),
        headers: {
            "Content-Type": "application/json"
        },
        credentials: "include"
    })
        .then(response => response.json())
        .then(dataObject => {
            if (dataObject.result === 'success') {
                dispatch({
                    type: ACTION_TYPES.UPDATE_ASSET_DATA_SELL,
                    payload: {
                        assetInformation: assetInformation
                    }
                })
                dispatch({
                    type: ACTION_TYPES.ADD_ASSET,
                    payload: {
                        loading: false,
                        message: 'success',
                        error: ''
                    }
                })
                dispatch({
                    type: ACTION_TYPES.ADJUST_ASSET_SALES_DATA,
                    payload: {
                        loading: false,
                        message: 'success',
                        error: '',
                        sales: {
                            businessid: businessid,
                            fyid: fyid,
                            invoicenumber: parseInt(state.invoicenumber),
                            type: "assetdummyentry"
                        },
                        invoicenumber: parseInt(state.invoicenumber) + 1,
                        invoicedate: state.invoicedate
                    }
                })
            }
            else {
                dispatch({
                    type: ACTION_TYPES.ADD_ASSET,
                    payload: {
                        loading: false,
                        message: '',
                        error: "Error : " + dataObject.result,
                        state: state
                    }
                })
            }
        })
        .catch(err => dispatch({
            type: ACTION_TYPES.ADD_ASSET,
            payload: {
                loading: false,
                error: "Error " + err.message,
                message: '',
                state: state
            }
        }))
}

export const resetAddDepreciationMessage = function (dispatch) {
    dispatch({
        type: ACTION_TYPES.ADD_DEPRECIATION,
        payload: {
            loading: false,
            error: '',
            message: 'initial',
            state: {}
        }
    })
}

export const raiseDepreciation = function (businessid, fyid, state, dispatch) {
    dispatch({
        type: ACTION_TYPES.ADD_DEPRECIATION_LOADING,
        payload: {
            loading: true,
            error: '',
            message: '',
            state: {}
        }
    })
    fetchRetry(baseUrl + 'raisedepreciation/' + businessid + "/" + fyid, {
        method: "POST",
        body: JSON.stringify(state),
        headers: {
            "Content-Type": "application/json"
        },
        credentials: "include"
    })
        .then(response => response.json())
        .then(dataObject => {
            if (dataObject.result === "success") {
                let data = {
                    ...state,
                    fyid: fyid,
                    type: "depreciation"
                }
                dispatch({
                    type: ACTION_TYPES.UPDATE_DEPRECIATION,
                    payload: {
                        data: data
                    }
                })
                dispatch({
                    type: ACTION_TYPES.ADD_DEPRECIATION,
                    payload: {
                        loading: false,
                        message: dataObject.result,
                        error: '',
                        state: state
                    }
                })
            }
            else {
                dispatch({
                    type: ACTION_TYPES.ADD_DEPRECIATION,
                    payload: {
                        loading: false,
                        message: '',
                        error: dataObject.result,
                        state: state
                    }
                })
            }
        })
        .catch(err => {
            dispatch({
                type: ACTION_TYPES.ADD_DEPRECIATION,
                payload: {
                    loading: false,
                    message: '',
                    error: err.message,
                    state: state
                }
            })
        })
}
export const getDepreciationList = function (businessid, fyid, dispatch) {
    dispatch({
        type: ACTION_TYPES.GET_DEPRECIATION_LOADING,
        payload: {
            loading: true,
            message: '',
            data: [],
            error: ''
        }
    })
    fetchRetry(baseUrl + 'raisedepreciation/' + businessid + "/" + fyid, {
        credentials: "include"
    })
        .then(response => response.json())
        .then(dataObject => {
            if (dataObject.result === 'success') {
                dispatch({
                    type: ACTION_TYPES.GET_DEPRECIATION,
                    payload: {
                        message: dataObject.result,
                        error: '',
                        loading: false,
                        data: dataObject.data
                    }
                })
            }
            else {
                dispatch(
                    {
                        type: ACTION_TYPES.GET_DEPRECIATION,
                        payload: {
                            message: '',
                            error: dataObject.result,
                            loading: false,
                            data: []
                        }
                    }
                )
            }
        })
        .catch(err => dispatch(
            {
                type: ACTION_TYPES.GET_DEPRECIATION,
                payload: {
                    message: '',
                    error: err.message,
                    loading: false,
                    data: []
                }
            }
        ))
}
export const refreshDepreciationState = function (dispatch) {
    dispatch({
        type: ACTION_TYPES.GET_DEPRECIATION,
        payload: {
            message: 'initial',
            error: '',
            loading: false,
            data: []
        }
    })
}
export const getLoanOpeningBalanceList = function (businessid, fyid, dispatch) {
    dispatch({
        type: ACTION_TYPES.GET_LOAN_OPENING_BALANCE_LOADING,
        payload: {
            loading: true,
            result: '',
            data: [],
            error: ''
        }
    });
    return fetchRetry(baseUrl + 'loanOpeningBalance/' + businessid + "/" + fyid, {
        credentials: "include"
    })
        .then((response) => response.json())
        .then((dataObject) => {
            if (dataObject.result === 'success') {
                dispatch({
                    type: ACTION_TYPES.GET_LOAN_OPENING_BALANCE,
                    payload: {
                        message: dataObject.result,
                        error: '',
                        loading: false,
                        data: dataObject.data
                    }
                })
            }
            else {
                dispatch(
                    {
                        type: ACTION_TYPES.GET_LOAN_OPENING_BALANCE,
                        payload: {
                            message: '',
                            error: dataObject.result,
                            loading: false,
                            data: []
                        }
                    }
                )
            }
        })
        .catch(err => {
            dispatch({
                type: ACTION_TYPES.GET_LOAN_OPENING_BALANCE,
                payload: {
                    message: '',
                    error: err.message,
                    loading: false,

                }
            });
        })
}
export const resetLoanOpeningBalance = function (dispatch) {
    dispatch({
        type: ACTION_TYPES.GET_LOAN_OPENING_BALANCE,
        payload: {
            message: 'initial',
            error: '',
            loading: false,
            data: []
        }
    })
}
export const updateLoanOpeningBalance = function (state, businessid, fyid, dispatch) {
    dispatch({
        type: ACTION_TYPES.UPDATE_LOAN_OPENING_LOADING,
        payload: {
            loading: true,
            message: '',
            disabled: '',
            error: ''
        }
    })
    fetchRetry(baseUrl + 'loanOpeningBalance/' + businessid + "/" + fyid, {
        method: "POST",
        body: JSON.stringify(state),
        headers: {
            "Content-Type": "application/json"
        },
        credentials: "include"
    })
        .then(response => response.json())
        .then(dataObject => {
            if (dataObject.result === "success") {
                dispatch({
                    type: ACTION_TYPES.PUSH_LOAN_OPENING_BALANCE,
                    payload: {
                        businessid: businessid,
                        fyid: fyid,
                        state: state
                    }
                })
                dispatch({
                    type: ACTION_TYPES.UPDATE_LOAN_OPENING,
                    payload: {
                        loading: false,
                        message: dataObject.result,
                        error: '',
                        disabled: false,
                        state: state
                    }
                })
            }
            else {
                dispatch({
                    type: ACTION_TYPES.UPDATE_LOAN_OPENING,
                    payload: {
                        loading: false,
                        error: dataObject.result,
                        message: '',
                        disabled: false,
                        state: state
                    }
                })
            }
        })
        .catch(err => dispatch({
            type: ACTION_TYPES.UPDATE_LOAN_OPENING,
            payload: {
                loading: false,
                error: err.message,
                message: '',
                disabled: false,
                state: state
            }
        }))
}
export const refreshUpdateLoanOpeningBalance = function (dispatch) {
    dispatch({
        type: ACTION_TYPES.UPDATE_LOAN_OPENING,
        payload: {
            loading: false,
            message: "initial",
            disabled: false,
            error: '',
            state: {}
        }
    })
}

export const refreshUpdateAssetOpeningBalance = function (dispatch) {
    dispatch({
        type: ACTION_TYPES.UPDATE_ASSET_OPENING,
        payload: {
            loading: false,
            message: "initial",
            disabled: false,
            error: '',
            state: {}
        }
    })
}
export const resetAssetOpeningBalance = function (dispatch) {
    dispatch({
        type: ACTION_TYPES.GET_ASSET_OPENING_BALANCE,
        payload: {
            message: 'initial',
            error: '',
            loading: false,
            data: []
        }
    })
}
export const getAssetOpeningBalanceList = function (businessid, fyid, dispatch) {
    dispatch({
        type: ACTION_TYPES.GET_ASSET_OPENING_BALANCE_LOADING,
        payload: {
            loading: true,
            result: '',
            data: [],
            error: ''
        }
    });
    return fetchRetry(baseUrl + 'assetOpeningBalance/' + businessid + "/" + fyid, {
        credentials: "include"
    })
        .then((response) => response.json())
        .then((dataObject) => {
            if (dataObject.result === 'success') {
                dispatch({
                    type: ACTION_TYPES.GET_ASSET_OPENING_BALANCE,
                    payload: {
                        message: dataObject.result,
                        error: '',
                        loading: false,
                        data: dataObject.data
                    }
                })
            }
            else {
                dispatch(
                    {
                        type: ACTION_TYPES.GET_ASSET_OPENING_BALANCE,
                        payload: {
                            message: '',
                            error: dataObject.result,
                            loading: false,
                            data: []
                        }
                    }
                )
            }
        })
        .catch(err => {
            dispatch({
                type: ACTION_TYPES.GET_ASSET_OPENING_BALANCE,
                payload: {
                    message: '',
                    error: err.message,
                    loading: false,

                }
            });
        })
}
export const updateAssetOpeningBalance = function (state, businessid, fyid, dispatch) {
    dispatch({
        type: ACTION_TYPES.UPDATE_ASSET_OPENING_LOADING,
        payload: {
            loading: true,
            message: '',
            disabled: '',
            error: ''
        }
    })
    fetchRetry(baseUrl + 'assetOpeningBalance/' + businessid + "/" + fyid, {
        method: "POST",
        body: JSON.stringify(state),
        headers: {
            "Content-Type": "application/json"
        },
        credentials: "include"
    })
        .then(response => response.json())
        .then(dataObject => {
            if (dataObject.result === "success") {
                dispatch({
                    type: ACTION_TYPES.PUSH_ASSET_OPENING_BALANCE,
                    payload: {
                        businessid: businessid,
                        fyid: fyid,
                        state: state
                    }
                })
                dispatch({
                    type: ACTION_TYPES.UPDATE_ASSET_OPENING,
                    payload: {
                        loading: false,
                        message: dataObject.result,
                        error: '',
                        disabled: false,
                        state: state
                    }
                })
            }
            else {
                dispatch({
                    type: ACTION_TYPES.UPDATE_ASSET_OPENING,
                    payload: {
                        loading: false,
                        error: dataObject.result,
                        message: '',
                        disabled: false,
                        state: state
                    }
                })
            }
        })
        .catch(err => dispatch({
            type: ACTION_TYPES.UPDATE_ASSET_OPENING,
            payload: {
                loading: false,
                error: err.message,
                message: '',
                disabled: false,
                state: state
            }
        }))
}
export const refreshInvestmentAccountState = function (dispatch) {
    dispatch({
        type: ACTION_TYPES.GET_INVESTMENT_ACCOUNT,
        payload: {
            loading: false,
            message: 'initial',
            data: [],
            error: ''
        }
    })
}
export const getInvestmentAccountList = function (businessid, dispatch) {
    dispatch({
        type: ACTION_TYPES.GET_INVESTMENT_ACCOUNT_LOADING,
        payload: {
            loading: true,
            message: '',
            data: [],
            error: ''
        }
    });
    return fetchRetry(baseUrl + 'investmentaccount/' + businessid, {
        credentials: "include"
    })
        .then(response => response.json())
        .then(dataObject => {
            if (dataObject.result === 'success') {
                dispatch({
                    type: ACTION_TYPES.GET_INVESTMENT_ACCOUNT,
                    payload: {
                        loading: false,
                        message: dataObject.result,
                        data: dataObject.data,
                        error: ''
                    }
                })
            }
            else {
                dispatch({
                    type: ACTION_TYPES.GET_INVESTMENT_ACCOUNT,
                    payload: {
                        loading: false,
                        message: '',
                        data: [],
                        error: "Error : " + dataObject.result
                    }
                })
            }
        })
        .catch(err => {
            dispatch({
                type: ACTION_TYPES.GET_INVESTMENT_ACCOUNT,
                payload: {
                    data: [],
                    error: "Error : " + err.message,
                    loading: false,
                    message: ''
                }
            });
        })
}
export const resetAddInvestmentAccountMessage = function (dispatch) {
    dispatch({
        type: ACTION_TYPES.ADD_INVESTMENT_ACCOUNT,
        payload: {
            loading: false,
            message: 'initial',
            error: '',
            state: {}
        }
    })
}
export const resetAddInvestmentAccountMessageAndForm = function (dispatch) {
    dispatch({
        type: ACTION_TYPES.ADD_INVESTMENT_ACCOUNT,
        payload: {
            loading: false,
            message: 'initial',
            error: '',
            state: {}
        }
    })
    dispatch(actions.reset('addInvestmentDetail'));
}
export const addInvestmentAccount = function (values, businessid, dispatch) {
    dispatch({
        type: ACTION_TYPES.ADD_INVESTMENT_ACCOUNT_LOADING,
        payload: {
            loading: true,
            disabled: true,
            message: '',
            error: ''
        }
    });
    return fetchRetry(baseUrl + 'investmentaccount/' + businessid, {
        method: "POST",
        body: JSON.stringify(values),
        headers: {
            "Content-Type": "application/json"
        },
        credentials: "include"
    })
        .then(response => response.json())
        .then(dataObject => {
            let bd = { ...values };
            bd.id = dataObject.id.id;
            bd.businessid = businessid;
            if (dataObject.result === 'success') {
                dispatch({
                    type: ACTION_TYPES.PUSH_INVESTMENT_ACCOUNT,
                    payload: {
                        investmentaccount: bd
                    }
                })
                dispatch({
                    type: ACTION_TYPES.ADD_INVESTMENT_ACCOUNT,
                    payload: {
                        loading: false,
                        disabled: false,
                        message: dataObject.result,
                        error: ''
                    }
                })
            }
            else {
                dispatch({
                    type: ACTION_TYPES.ADD_INVESTMENT_ACCOUNT,
                    payload: {
                        loading: false,
                        disabled: false,
                        error: dataObject.result,
                        message: ''
                    }
                })
            }
        })
        .catch((err) => dispatch({
            type: ACTION_TYPES.ADD_INVESTMENT_ACCOUNT,
            payload: {
                loading: false,
                disabled: false,
                error: "Error : " + err.message,
                message: ''
            }
        }))
}

export const updateInvestmentOpeningBalance = function (state, businessid, fyid, dispatch) {
    dispatch({
        type: ACTION_TYPES.UPDATE_INVESTMENT_OPENING_LOADING,
        payload: {
            loading: true,
            message: '',
            disabled: '',
            error: ''
        }
    })
    fetchRetry(baseUrl + 'investmentOpeningBalance/' + businessid + "/" + fyid, {
        method: "POST",
        body: JSON.stringify(state),
        headers: {
            "Content-Type": "application/json"
        },
        credentials: "include"
    })
        .then(response => response.json())
        .then(dataObject => {
            if (dataObject.result === "success") {
                dispatch({
                    type: ACTION_TYPES.PUSH_INVESTMENT_OPENING_BALANCE,
                    payload: {
                        businessid: businessid,
                        fyid: fyid,
                        state: state
                    }
                })
                dispatch({
                    type: ACTION_TYPES.UPDATE_INVESTMENT_OPENING,
                    payload: {
                        loading: false,
                        message: dataObject.result,
                        error: '',
                        disabled: false,
                        state: state
                    }
                })
            }
            else {
                dispatch({
                    type: ACTION_TYPES.UPDATE_INVESTMENT_OPENING,
                    payload: {
                        loading: false,
                        error: dataObject.result,
                        message: '',
                        disabled: false,
                        state: state
                    }
                })
            }
        })
        .catch(err => dispatch({
            type: ACTION_TYPES.UPDATE_INVESTMENT_OPENING,
            payload: {
                loading: false,
                error: err.message,
                message: '',
                disabled: false,
                state: state
            }
        }))
}

export const refreshUpdateInvestmentOpeningBalance = function (dispatch) {
    dispatch({
        type: ACTION_TYPES.UPDATE_INVESTMENT_OPENING,
        payload: {
            loading: false,
            message: "initial",
            disabled: false,
            error: '',
            state: {}
        }
    })
}
export const resetInvestmentOpeningBalance = function (dispatch) {
    dispatch({
        type: ACTION_TYPES.GET_INVESTMENT_OPENING_BALANCE,
        payload: {
            message: 'initial',
            error: '',
            loading: false,
            data: []
        }
    })
}
export const getInvestmentOpeningBalanceList = function (businessid, fyid, dispatch) {
    dispatch({
        type: ACTION_TYPES.GET_INVESTMENT_OPENING_BALANCE_LOADING,
        payload: {
            loading: true,
            result: '',
            data: [],
            error: ''
        }
    });
    return fetchRetry(baseUrl + 'investmentOpeningBalance/' + businessid + "/" + fyid, {
        credentials: "include"
    })
        .then((response) => response.json())
        .then((dataObject) => {
            if (dataObject.result === 'success') {
                dispatch({
                    type: ACTION_TYPES.GET_INVESTMENT_OPENING_BALANCE,
                    payload: {
                        message: dataObject.result,
                        error: '',
                        loading: false,
                        data: dataObject.data
                    }
                })
            }
            else {
                dispatch(
                    {
                        type: ACTION_TYPES.GET_INVESTMENT_OPENING_BALANCE,
                        payload: {
                            message: '',
                            error: dataObject.result,
                            loading: false,
                            data: []
                        }
                    }
                )
            }
        })
        .catch(err => {
            dispatch({
                type: ACTION_TYPES.GET_INVESTMENT_OPENING_BALANCE,
                payload: {
                    message: '',
                    error: err.message,
                    loading: false,

                }
            });
        })
}

export const updateTDSOpeningBalance = function (state, businessid, fyid, dispatch) {
    dispatch({
        type: ACTION_TYPES.UPDATE_TDS_OPENING_LOADING,
        payload: {
            loading: true,
            message: '',
            disabled: '',
            error: ''
        }
    })
    fetchRetry(baseUrl + 'tdsOpeningBalance/' + businessid + "/" + fyid, {
        method: "POST",
        body: JSON.stringify(state),
        headers: {
            "Content-Type": "application/json"
        },
        credentials: "include"
    })
        .then(response => response.json())
        .then(dataObject => {
            if (dataObject.result === "success") {
                dispatch({
                    type: ACTION_TYPES.PUSH_TDS_OPENING_BALANCE,
                    payload: {
                        businessid: businessid,
                        fyid: fyid,
                        state: state
                    }
                })
                dispatch({
                    type: ACTION_TYPES.UPDATE_TDS_OPENING,
                    payload: {
                        loading: false,
                        message: dataObject.result,
                        error: '',
                        disabled: false,
                        state: state
                    }
                })
            }
            else {
                dispatch({
                    type: ACTION_TYPES.UPDATE_TDS_OPENING,
                    payload: {
                        loading: false,
                        error: dataObject.result,
                        message: '',
                        disabled: false,
                        state: state
                    }
                })
            }
        })
        .catch(err => dispatch({
            type: ACTION_TYPES.UPDATE_TDS_OPENING,
            payload: {
                loading: false,
                error: err.message,
                message: '',
                disabled: false,
                state: state
            }
        }))
}

export const refreshUpdateTDSOpeningBalance = function (dispatch) {
    dispatch({
        type: ACTION_TYPES.UPDATE_TDS_OPENING,
        payload: {
            loading: false,
            message: "initial",
            disabled: false,
            error: '',
            state: {}
        }
    })
}
export const getTDSOpeningBalanceList = function (businessid, fyid, dispatch) {
    dispatch({
        type: ACTION_TYPES.GET_TDS_OPENING_BALANCE_LOADING,
        payload: {
            loading: true,
            result: '',
            data: [],
            error: ''
        }
    });
    return fetchRetry(baseUrl + 'tdsOpeningBalance/' + businessid + "/" + fyid, {
        credentials: "include"
    })
        .then((response) => response.json())
        .then((dataObject) => {
            if (dataObject.result === 'success') {
                dispatch({
                    type: ACTION_TYPES.GET_TDS_OPENING_BALANCE,
                    payload: {
                        message: dataObject.result,
                        error: '',
                        loading: false,
                        data: dataObject.data
                    }
                })
            }
            else {
                dispatch(
                    {
                        type: ACTION_TYPES.GET_TDS_OPENING_BALANCE,
                        payload: {
                            message: '',
                            error: dataObject.result,
                            loading: false,
                            data: []
                        }
                    }
                )
            }
        })
        .catch(err => {
            dispatch({
                type: ACTION_TYPES.GET_TDS_OPENING_BALANCE,
                payload: {
                    message: '',
                    error: err.message,
                    loading: false,
                    data: []
                }
            });
        })
}
export const resetTDSOpeningBalance = function (dispatch) {
    dispatch({
        type: ACTION_TYPES.GET_TDS_OPENING_BALANCE,
        payload: {
            message: 'initial',
            error: '',
            loading: false,
            data: []
        }
    })
}

export const updateRCOpeningBalance = function (state, businessid, fyid, dispatch) {
    dispatch({
        type: ACTION_TYPES.UPDATE_RC_OPENING_LOADING,
        payload: {
            loading: true,
            message: '',
            disabled: '',
            error: '',
            data: []
        }
    })
    fetchRetry(baseUrl + 'rcOpeningBalance/' + businessid + "/" + fyid, {
        method: "POST",
        body: JSON.stringify(state),
        headers: {
            "Content-Type": "application/json"
        },
        credentials: "include"
    })
        .then(response => response.json())
        .then(dataObject => {
            if (dataObject.result === "success") {
                dispatch({
                    type: ACTION_TYPES.PUSH_RC_OPENING_BALANCE,
                    payload: {
                        businessid: businessid,
                        fyid: fyid,
                        state: state
                    }
                })
                dispatch({
                    type: ACTION_TYPES.UPDATE_RC_OPENING,
                    payload: {
                        loading: false,
                        message: dataObject.result,
                        error: '',
                        disabled: false,
                        state: state
                    }
                })
            }
            else {
                dispatch({
                    type: ACTION_TYPES.UPDATE_RC_OPENING,
                    payload: {
                        loading: false,
                        error: dataObject.result,
                        message: '',
                        disabled: false,
                        state: state
                    }
                })
            }
        })
        .catch(err => dispatch({
            type: ACTION_TYPES.UPDATE_RC_OPENING,
            payload: {
                loading: false,
                error: err.message,
                message: '',
                disabled: false,
                state: state
            }
        }))
}

export const refreshUpdateRCOpeningBalance = function (dispatch) {
    dispatch({
        type: ACTION_TYPES.UPDATE_RC_OPENING,
        payload: {
            loading: false,
            message: "initial",
            disabled: false,
            error: '',
            state: {}
        }
    })
}
export const getRCOpeningBalanceList = function (businessid, fyid, dispatch) {
    dispatch({
        type: ACTION_TYPES.GET_RC_OPENING_BALANCE_LOADING,
        payload: {
            loading: true,
            result: '',
            data: '',
            error: ''
        }
    });
    return fetchRetry(baseUrl + 'rcOpeningBalance/' + businessid + "/" + fyid, {
        credentials: "include"
    })
        .then((response) => response.json())
        .then((dataObject) => {
            if (dataObject.result === 'success') {
                dispatch({
                    type: ACTION_TYPES.GET_RC_OPENING_BALANCE,
                    payload: {
                        message: dataObject.result,
                        error: '',
                        loading: false,
                        data: dataObject.data.length > 0 ? dataObject.data[0].amount : 0
                    }
                })
            }
            else {
                dispatch(
                    {
                        type: ACTION_TYPES.GET_RC_OPENING_BALANCE,
                        payload: {
                            message: '',
                            error: dataObject.result,
                            loading: false,
                            data: ''
                        }
                    }
                )
            }
        })
        .catch(err => {
            dispatch({
                type: ACTION_TYPES.GET_RC_OPENING_BALANCE,
                payload: {
                    message: '',
                    error: err.message,
                    loading: false,
                    data: ''

                }
            });
        })
}
export const resetRCOpeningBalance = function (dispatch) {
    dispatch({
        type: ACTION_TYPES.GET_RC_OPENING_BALANCE,
        payload: {
            message: 'initial',
            error: '',
            loading: false,
            data: ''
        }
    })
}


export const updateStockOpeningBalance = function (state, businessid, fyid, dispatch) {
    dispatch({
        type: ACTION_TYPES.UPDATE_STOCK_OPENING_LOADING,
        payload: {
            loading: true,
            message: '',
            disabled: '',
            error: ''
        }
    })
    fetchRetry(baseUrl + 'stockOpeningBalance/' + businessid + "/" + fyid, {
        method: "POST",
        body: JSON.stringify(state),
        headers: {
            "Content-Type": "application/json"
        },
        credentials: "include"
    })
        .then(response => response.json())
        .then(dataObject => {
            if (dataObject.result === "success") {
                dispatch({
                    type: ACTION_TYPES.PUSH_STOCK_OPENING_BALANCE,
                    payload: {
                        id: dataObject.id.id,
                        businessid: businessid,
                        fyid: fyid,
                        state: state
                    }
                })
                dispatch({
                    type: ACTION_TYPES.UPDATE_STOCK_OPENING,
                    payload: {
                        loading: false,
                        message: dataObject.result,
                        error: '',
                        disabled: false,
                        state: state
                    }
                })
            }
            else {
                dispatch({
                    type: ACTION_TYPES.UPDATE_STOCK_OPENING,
                    payload: {
                        loading: false,
                        error: dataObject.result,
                        message: '',
                        disabled: false,
                        state: state
                    }
                })
            }
        })
        .catch(err => dispatch({
            type: ACTION_TYPES.UPDATE_STOCK_OPENING,
            payload: {
                loading: false,
                error: err.message,
                message: '',
                disabled: false,
                state: state
            }
        }))
}

export const refreshUpdateStockOpeningBalance = function (dispatch) {
    dispatch({
        type: ACTION_TYPES.UPDATE_STOCK_OPENING,
        payload: {
            loading: false,
            message: "initial",
            disabled: false,
            error: '',
            state: {}
        }
    })
}

export const getStockOpeningBalanceList = function (businessid, fyid, dispatch) {
    dispatch({
        type: ACTION_TYPES.GET_STOCK_OPENING_BALANCE_LOADING,
        payload: {
            loading: true,
            result: '',
            data: [],
            error: ''
        }
    });
    return fetchRetry(baseUrl + 'stockOpeningBalance/' + businessid + "/" + fyid, {
        credentials: "include"
    })
        .then((response) => response.json())
        .then((dataObject) => {
            if (dataObject.result === 'success') {
                dispatch({
                    type: ACTION_TYPES.GET_STOCK_OPENING_BALANCE,
                    payload: {
                        message: dataObject.result,
                        error: '',
                        loading: false,
                        data: dataObject.data
                    }
                })
            }
            else {
                dispatch(
                    {
                        type: ACTION_TYPES.GET_STOCK_OPENING_BALANCE,
                        payload: {
                            message: '',
                            error: dataObject.result,
                            loading: false,
                            data: []
                        }
                    }
                )
            }
        })
        .catch(err => {
            dispatch({
                type: ACTION_TYPES.GET_STOCK_OPENING_BALANCE,
                payload: {
                    message: '',
                    error: err.message,
                    loading: false,
                    data: []
                }
            });
        })
}

export const resetStockOpeningBalance = function (dispatch) {
    dispatch({
        type: ACTION_TYPES.GET_STOCK_OPENING_BALANCE,
        payload: {
            message: 'initial',
            error: '',
            loading: false,
            data: []
        }
    })
}

export const updateIncomeTaxOpeningBalance = function (state, businessid, fyid, dispatch) {
    dispatch({
        type: ACTION_TYPES.UPDATE_INCOME_TAX_OPENING_LOADING,
        payload: {
            loading: true,
            message: '',
            disabled: '',
            error: ''
        }
    })
    fetchRetry(baseUrl + 'incometaxOpeningBalance/' + businessid + "/" + fyid, {
        method: "POST",
        body: JSON.stringify(state),
        headers: {
            "Content-Type": "application/json"
        },
        credentials: "include"
    })
        .then(response => response.json())
        .then(dataObject => {
            if (dataObject.result === "success") {
                dispatch({
                    type: ACTION_TYPES.PUSH_INCOME_TAX_OPENING_BALANCE,
                    payload: {
                        businessid: businessid,
                        fyid: fyid,
                        state: state
                    }
                })
                dispatch({
                    type: ACTION_TYPES.UPDATE_INCOME_TAX_OPENING,
                    payload: {
                        loading: false,
                        message: dataObject.result,
                        error: '',
                        disabled: false,
                        state: state
                    }
                })
            }
            else {
                dispatch({
                    type: ACTION_TYPES.UPDATE_INCOME_TAX_OPENING,
                    payload: {
                        loading: false,
                        error: dataObject.result,
                        message: '',
                        disabled: false,
                        state: state
                    }
                })
            }
        })
        .catch(err => dispatch({
            type: ACTION_TYPES.UPDATE_INCOME_TAX_OPENING,
            payload: {
                loading: false,
                error: err.message,
                message: '',
                disabled: false,
                state: state
            }
        }))
}
export const getIncomeTaxOpeningBalanceList = function (businessid, fyid, dispatch) {
    dispatch({
        type: ACTION_TYPES.GET_INCOME_TAX_OPENING_BALANCE_LOADING,
        payload: {
            loading: true,
            result: '',
            data: [],
            error: ''
        }
    });
    return fetchRetry(baseUrl + 'incometaxOpeningBalance/' + businessid + "/" + fyid, {
        credentials: "include"
    })
        .then((response) => response.json())
        .then((dataObject) => {
            if (dataObject.result === 'success') {
                dispatch({
                    type: ACTION_TYPES.GET_INCOME_TAX_OPENING_BALANCE,
                    payload: {
                        message: dataObject.result,
                        error: '',
                        loading: false,
                        data: dataObject.data
                    }
                })
            }
            else {
                dispatch(
                    {
                        type: ACTION_TYPES.GET_INCOME_TAX_OPENING_BALANCE,
                        payload: {
                            message: '',
                            error: dataObject.result,
                            loading: false,
                            data: []
                        }
                    }
                )
            }
        })
        .catch(err => {
            dispatch({
                type: ACTION_TYPES.GET_INCOME_TAX_OPENING_BALANCE,
                payload: {
                    message: '',
                    error: err.message,
                    loading: false,
                    data: []
                }
            });
        })
}
export const refreshUpdateIncomeTaxOpeningBalance = function (dispatch) {
    dispatch({
        type: ACTION_TYPES.UPDATE_INCOME_TAX_OPENING,
        payload: {
            loading: false,
            message: "initial",
            disabled: false,
            error: '',
            state: {}
        }
    })
}
export const resetIncomeTaxOpeningBalance = function (dispatch) {
    dispatch({
        type: ACTION_TYPES.GET_INCOME_TAX_OPENING_BALANCE,
        payload: {
            message: 'initial',
            error: '',
            loading: false,
            data: []
        }
    })
}

export const updateInvestmentAccount = function (values, businessid, id, dispatch) {
    dispatch({
        type: ACTION_TYPES.UPDATE_INVESTMENT_ACCOUNT_LOADING,
        payload: {
            loading: true,
            disabled: true,
            message: '',
            error: ''
        }
    });
    return fetchRetry(baseUrl + 'investmentaccount/' + businessid, {
        method: "PUT",
        body: JSON.stringify({ id: id, investmentaccount: values.investmentaccount, shareholding: values.shareholding }),
        headers: {
            "Content-Type": "application/json"
        },
        credentials: "include"
    })
        .then(response => response.json())
        .then(dataObject => {
            if (dataObject.result === 'success') {
                dispatch({
                    type: ACTION_TYPES.DELETE_AND_INSERT_INVESTMENT_ACCOUNT,
                    payload: {
                        investmentaccount: values,
                        id: id
                    }
                })
                dispatch({
                    type: ACTION_TYPES.UPDATE_INVESTMENT_ACCOUNT,
                    payload: {
                        loading: false,
                        disabled: false,
                        message: dataObject.result,
                        error: ''
                    }
                })
            }
            else {
                dispatch({
                    type: ACTION_TYPES.UPDATE_INVESTMENT_ACCOUNT,
                    payload: {
                        loading: false,
                        disabled: false,
                        error: dataObject.result,
                        message: '',
                        state: values
                    }
                })
            }
        })
        .catch((err) => dispatch({
            type: ACTION_TYPES.UPDATE_INVESTMENT_ACCOUNT,
            payload: {
                loading: false,
                disabled: false,
                error: "Error : " + err.message,
                message: '',
                state: values
            }
        }))
}
export const resetUpdateInvestmentAccountMessage = function (dispatch) {
    dispatch({
        type: ACTION_TYPES.UPDATE_INVESTMENT_ACCOUNT,
        payload: {
            loading: false,
            message: 'initial',
            error: '',
            state: {}
        }
    })
}
export const passStockJournal = function (businessid,fyid,state,dispatch) {
    dispatch({
        type: ACTION_TYPES.PASS_STOCK_JOURNAL_LOADING,
        payload: {
            loading: true,
            message: '',
            error: ''
        }
    })
    let data = {
        date : state.date,
        consumptiondetail : JSON.stringify(state.consumptiondetail),
        productiondetail : JSON.stringify(state.productiondetail)
    }
    fetchRetry(baseUrl + 'stockjournal/' + businessid + "/" + fyid, {
        method: "POST",
        body: JSON.stringify(data),
        headers: {
            "Content-Type": "application/json"
        },
        credentials: "include"
    })
        .then(response => response.json())
        .then(dataObject => {
            if (dataObject.result === "success") {
                dispatch({
                    type: ACTION_TYPES.PUSH_STOCK_JOURNAL_ENTRY,
                    payload: {
                        businessid: businessid,
                        fyid: fyid,
                        state: state,
                        error: '',
                        id: dataObject.id
                    }
                })
                dispatch({
                    type: ACTION_TYPES.PASS_STOCK_JOURNAL,
                    payload: {
                        loading: false,
                        message: dataObject.result,
                        error: '',
                        state: ''
                    }
                })
            }
            else {
                dispatch({
                    type: ACTION_TYPES.PASS_STOCK_JOURNAL,
                    payload: {
                        loading: false,
                        error: dataObject.result,
                        message: '',
                        state: state
                    }
                })
            }
        })
        .catch(err => dispatch({
            type: ACTION_TYPES.PASS_STOCK_JOURNAL,
            payload: {
                loading: false,
                error: err.message,
                message: '',
                state: state
            }
        }))    
}
export const refreshStockJournalState = function (dispatch) {
    dispatch({
        type: ACTION_TYPES.PASS_STOCK_JOURNAL,
        payload: {
            loading: false,
            error: '',
            message: '',
            state: ''
        }
    })
}
export const getStockJournalList = function (businessid, fyid, dispatch) {
    dispatch({
        type: ACTION_TYPES.GET_STOCK_JOURNAL_LOADING,
        payload: {
            loading: true,
            mesaage: '',
            error: '',
            data: []
        }
    })
    fetchRetry(baseUrl + 'stockjournal/' + businessid + "/" + fyid, {
        credentials: "include"
    })
        .then(response => response.json())
        .then(dataObject => {
            if (dataObject.result === 'success') {
                dispatch({
                    type: ACTION_TYPES.GET_STOCK_JOURNAL,
                    payload: {
                        message: dataObject.result,
                        data: dataObject.data,
                        error: '',
                        loading: false
                    }
                })
            }
            else {
                dispatch({
                    type: ACTION_TYPES.GET_STOCK_JOURNAL,
                    payload: {
                        message: '',
                        loading: false,
                        error: dataObject.result,
                        data: []
                    }
                })
            }
        })
        .catch(err => {
            dispatch({
                type: ACTION_TYPES.GET_STOCK_JOURNAL,
                payload: {
                    message: '',
                    loading: false,
                    error: err.message,
                    data: []
                }
            })
        })
}
export const refreshGetStockJournal = function (dispatch) {
    dispatch({
        type: ACTION_TYPES.GET_STOCK_JOURNAL,
        payload: {
            message: 'initial',
            loading: false,
            error: '',
            data: []
        }
    })
}

export const getEmployeeOpeningBalanceList = function (businessid, fyid, dispatch) {
    dispatch({
        type: ACTION_TYPES.GET_EMPLOYEE_OPENING_BALANCE_LOADING,
        payload: {
            loading: true,
            result: '',
            data: [],
            error: ''
        }
    });
    return fetchRetry(baseUrl + 'employeeOpeningBalance/' + businessid + "/" + fyid, {
        credentials: "include"
    })
        .then((response) => response.json())
        .then((dataObject) => {
            if (dataObject.result === 'success') {
                dispatch({
                    type: ACTION_TYPES.GET_EMPLOYEE_OPENING_BALANCE,
                    payload: {
                        message: dataObject.result,
                        error: '',
                        loading: false,
                        data: dataObject.data
                    }
                })
            }
            else {
                dispatch(
                    {
                        type: ACTION_TYPES.GET_EMPLOYEE_OPENING_BALANCE,
                        payload: {
                            message: '',
                            error: dataObject.result,
                            loading: false,
                            data: []
                        }
                    }
                )
            }
        })
        .catch(err => {
            dispatch({
                type: ACTION_TYPES.GET_EMPLOYEE_OPENING_BALANCE,
                payload: {
                    message: '',
                    error: err.message,
                    loading: false,
                }
            });
        })
}

export const refreshUpdateEmployeeOpeningBalance = function (dispatch) {
    dispatch({
        type: ACTION_TYPES.UPDATE_EMPLOYEE_OPENING,
        payload: {
            loading: false,
            message: "initial",
            disabled: false,
            error: '',
            state: {}
        }
    })
}

export const resetEmployeeOpeningBalance = function (dispatch) {
    dispatch({
        type: ACTION_TYPES.GET_EMPLOYEE_OPENING_BALANCE,
        payload: {
            message: 'initial',
            error: '',
            loading: false,
            data: [],
            state: {}
        }
    })
}

export const updateEmployeeOpeningBalance = function (state, businessid, fyid, dispatch) {
    dispatch({
        type: ACTION_TYPES.UPDATE_EMPLOYEE_OPENING_LOADING,
        payload: {
            loading: true,
            message: '',
            disabled: '',
            error: ''
        }
    })
    fetchRetry(baseUrl + 'employeeOpeningBalance/' + businessid + "/" + fyid, {
        method: "POST",
        body: JSON.stringify(state),
        headers: {
            "Content-Type": "application/json"
        },
        credentials: "include"
    })
        .then(response => response.json())
        .then(dataObject => {
            if (dataObject.result === "success") {
                dispatch({
                    type: ACTION_TYPES.PUSH_EMPLOYEE_OPENING_BALANCE,
                    payload: {
                        businessid: businessid,
                        fyid: fyid,
                        state: state
                    }
                })
                dispatch({
                    type: ACTION_TYPES.UPDATE_EMPLOYEE_OPENING,
                    payload: {
                        loading: false,
                        message: dataObject.result,
                        error: '',
                        disabled: false,
                        state: state
                    }
                })
            }
            else {
                dispatch({
                    type: ACTION_TYPES.UPDATE_EMPLOYEE_OPENING,
                    payload: {
                        loading: false,
                        error: dataObject.result,
                        message: '',
                        disabled: false,
                        state: state
                    }
                })
            }
        })
        .catch(err => dispatch({
            type: ACTION_TYPES.UPDATE_EMPLOYEE_OPENING,
            payload: {
                loading: false,
                error: err.message,
                message: '',
                disabled: false,
                state: state
            }
        }))
}
