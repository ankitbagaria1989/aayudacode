import * as ACTION_TYPES from './actionTypes';

const initialState = {
    loading : false,
    message : "initial",
    disabled : false,
    error : ''
}

export const updatePartyOpeningMessage = function (state = initialState, action) {
    switch(action.type) {
        case ACTION_TYPES.UPDATE_PARTY_OPENING_LOADING :
            return action.payload;
        case ACTION_TYPES.UPDATE_PARTY_OPENING :
            return action.payload;
        case ACTION_TYPES.UPDATE_PARTY_OPENING_ERROR:
            return action.payload;    
        case ACTION_TYPES.RESETTING_PARTY_OPENING_CHANGE:
            return action.payload;    
        default : return state;        
    }

}