import * as ACTION_TYPES from './actionTypes';

const initialState = {
    loading : false,
    error : '',
    message : 'initial',
    amount : 0
}

export const getCashInHand = function (state=initialState, action) {
    switch(action.type) {
        case ACTION_TYPES.GET_CASH_IN_HAND_LOADING:
            return action.payload
        case ACTION_TYPES.GET_CASH_IN_HAND:
            return action.payload
        case ACTION_TYPES.REFRESH_CASH_IN_HAND:
            return initialState;
        default: return state;            
    }
}