import * as ACTION_TYPES from './actionTypes';

const initialstate = {
    loading: false,
    message: 'initial',
    data: [],
    error: ''
};


export const getEmployee = function (state = initialstate, action) {
    switch (action.type) {
        case ACTION_TYPES.GET_EMPLOYEE_LOADING:
            return action.payload;
        case ACTION_TYPES.GET_EMPLOYEE:
            return action.payload;
        case ACTION_TYPES.REFRESH_EMPLOYEE:
            return ({
                loading: false,
                message: 'initial',
                data: [],
                error: ''
            })
        case ACTION_TYPES.PUSH_CHECK_EMPLOYEE:
            let data = action.payload.data;
            let existingdata = state.data;
            let present = false;
            existingdata.forEach(d => {
                if (d.employeeid === data.employeeid) present = true;
            })
            if (present === false) {
                existingdata.push(data);
                return ({
                    employeeno: action.payload.employeeno,
                    loading: false,
                    data: existingdata,
                    error: '',
                    message: ''
                })
            }
            else return state;

        case ACTION_TYPES.PUSH_EMPLOYEE:
            let da = action.payload.data;
            let existingda = state.data;
            existingda.push(da);
            return ({
                loading: false,
                data: existingda,
                error: '',
                message: '',
                employeeno: action.payload.employeeno
            })
        case ACTION_TYPES.DELETE_AND_PUSH_SALARY:
            let da1 = state.data;
            da1.map(e => {
                if (e.employeeid === action.payload.employeeid) {
                    e.salary = action.payload.salary
                }
            })
            return {
                ...state,
                data: da1
            }
        case ACTION_TYPES.UPDATE_EMPLOYEE_DOT:
            let da2 = state.data;
            da2.map(e => {
                if (e.employeeid === action.payload.employeeid) {
                    e.active = 0;
                    e.dot = action.payload.dot
                }
            })
            return {
                ...state,
                data: da2
            }
        default: return state;
    }
}