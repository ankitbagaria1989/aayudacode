import * as ACTION_TYPES from './actionTypes';

const inititalState = {
    loading : false,
    message : '',
    data : []
}

export const subUserList = function (state = inititalState, action) {
    switch (action.type) {
        case ACTION_TYPES.SUB_USER_LIST_LOADING :
            return ({
                loading : true,
                message : '',
                data : []
            });
        case ACTION_TYPES.SUB_USER_LIST :
            return (
                {
                    loading: false,
                    message : action.payload.result,
                    data : action.payload.data
                }
            );  
        default : return state;      
    }
}