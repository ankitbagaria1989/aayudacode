import * as ACTION_TYPES from './actionTypes';

const initialState = {
    loading: false,
    error: '',
    message: 'initial',
    data: []
}

export const getSalary = function (state = initialState, action) {
    switch (action.type) {
        case ACTION_TYPES.GET_SALARY_LOADING:
            return action.payload
        case ACTION_TYPES.GET_SALARY:
            return action.payload
        case ACTION_TYPES.REFRESH_GET_SALARY:
            return ({
                loading: false,
                error: '',
                message: 'initial',
                data: []
            })
        default: return state;
    }
}