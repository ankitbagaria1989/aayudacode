import * as ACTION_TYPES from './actionTypes';

const initialState = {
    loading : false,
    message : '',
    error : '',
    state : {}
}

export const addInvestmentAccountMessage = function (state = initialState, action) {
    switch (action.type) {
        case ACTION_TYPES.ADD_INVESTMENT_ACCOUNT_LOADING:
           return action.payload;
        case ACTION_TYPES.ADD_INVESTMENT_ACCOUNT:
            return action.payload;
        default: return state;
    }
}