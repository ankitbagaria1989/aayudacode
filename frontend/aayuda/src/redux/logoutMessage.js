import * as ACTION_TYPES from './actionTypes';

const initialState = {
    loading : false,
    message : 'initial',
    error : ''
}

export const logoutMessage = function (state=initialState, action) {
    switch (action.type) {
        case ACTION_TYPES.LOGOUTLOADING:
            return ({
                loading : true,
                message: '',
                error : ''
            })
        case ACTION_TYPES.LOGOUT:
            return action.payload;
        case ACTION_TYPES.REFRESH_LOGOUTMESSAGE:
            return ({
                loading:false,
                message:'initail',
                error : '' 
            })    
        default:
            return state;        
    }
}