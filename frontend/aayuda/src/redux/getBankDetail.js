import * as ACTION_TYPES from './actionTypes';

const initialstate = {
    loading: false,
    message: 'initial',
    data: [],
    error: ''
};


export const getBankDetail = function (state = initialstate, action) {
    switch (action.type) {
        case ACTION_TYPES.GET_BANK_DETAIL_LOADING:
            return action.payload;
        case ACTION_TYPES.GET_BANK_DETAIL:
            return action.payload;
        case ACTION_TYPES.GET_BANK_DETAIL_ERROR:
            return action.payload;
        case ACTION_TYPES.REFRESH_BANK:
            return ({
                loading: false,
                message: 'initial',
                data: [],
                error: ''
            })
        case ACTION_TYPES.REFRESH_BANK_DETAIL_SECOND:
            return ({
                loading: false,
                message: '',
                data: [],
                error: ''
            })
        case ACTION_TYPES.PUSH_NEW_BANK:
            let bank = state.data;
            let present = false;
            bank.forEach (b => {
                if (b.bankdetailid === action.payload.bankdetail.bankdetailid) present = true;
            })
            if (present === false) {
                bank.push(action.payload.bankdetail);
                return ({
                    message: '',
                    error: '',
                    loading: false,
                    data: bank
                })
            }
            else return state;
        case ACTION_TYPES.DELETE_AND_PUSH_BANK_DETAIL:
            let bankD = action.payload.bankDetail;
            let existingBank = state.data;
            existingBank = existingBank.filter(b => b.bankdetailid !== bankD.bankdetailid);
            existingBank.push(bankD);
            return ({
                loading: false,
                message: '',
                data : existingBank,
                error :''
            })    
        default: return state;
    }
}