import * as ACTION_TYPES from './actionTypes';
const initialState = {
    loading: false,
    error: '',
    message: 'initial'
}

export const updateAttendanceMessage = function (state = initialState, action) {
    switch (action.type) {
        case ACTION_TYPES.UPDATE_ATTENDANCE_LOADING:
            return action.payload;
        case ACTION_TYPES.UPDATE_ATTENDANCE:
            return action.payload;
        case ACTION_TYPES.REFRESH_ATTENDANCE_MESSAGE:
            return ({
                loading: false,
                error: '',
                message: 'initial'
            })
        default: return state;
    }
}