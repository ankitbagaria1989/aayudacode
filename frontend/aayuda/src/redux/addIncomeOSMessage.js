import * as ACTION_TYPES from './actionTypes';

const initialState = {
    loading : false,
    message : '',
    error : '',
    state : []
}

export const addIncomeOSMessage = function (state = initialState, action) {
    switch (action.type) {
        case ACTION_TYPES.ADD_INCOMEOS_LOADING:
           return action.payload;
        case ACTION_TYPES.ADD_INCOMEOS:
            return action.payload;
        case ACTION_TYPES.RESET_ADD_INCOMEOS:
            return ({
                loading : false,
                message : '',
                error : '',
                state : []
            })
        default: return state;
    }
}