import * as ACTION_TYPES from './actionTypes';

const initialstate = {
    loading: false,
    message: 'initial',
    data: [],
    error: ''
};

export const getInvestmentOpeningBalance = function (state = initialstate, action) {
    switch (action.type) {
        case ACTION_TYPES.GET_INVESTMENT_OPENING_BALANCE_LOADING:
            return action.payload;
        case ACTION_TYPES.GET_INVESTMENT_OPENING_BALANCE:
            return action.payload;
        case ACTION_TYPES.PUSH_INVESTMENT_OPENING_BALANCE:
            let amount = action.payload.state.amount;
            let investmentaccount = action.payload.state.Investmentaccount;
            let madereceived = action.payload.state.madereceived;
            let businessid = action.payload.businessid;
            let fyid = action.payload.fyid;
            let da = state.data;
            let existingdata = da.filter(d => d.investmentaccount === investmentaccount && d.madereceived === madereceived);
            if (existingdata.length === 0) {
                da.push({
                    investmentaccount: investmentaccount,
                    businessid: businessid,
                    fyid: fyid,
                    amount: amount,
                    madereceived: madereceived
                })
            }
            else da.map(d => d.investmentaccount === investmentaccount && d.madereceived === madereceived ? d.amount = amount : d.amount = d.amount);
            return ({
                loading: false,
                message: '',
                data: da,
                error: '',
                state: action.payload.state
            })
        default: return state;
    }
}