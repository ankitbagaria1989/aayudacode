import * as ACTION_TYPES from './actionTypes';

const initialState = {
    loading: false,
    disabled: false,
    message : 'initial',
    error : ''
}

export const addVendorMessage = function (state = initialState, action) {
    switch (action.type) {
        case ACTION_TYPES.ADD_VENDOR_LOADING:
            return action.payload;
        case ACTION_TYPES.ADD_VENDOR :
            return action.payload;
        case ACTION_TYPES.RESET_VENDOR_MESSAGE:
            return ({
                error : '',
                message: 'initial',
                loading : false,
                disabled : false
            })    
        default : return state;         
    }
}