import * as ACTION_TYPES from './actionTypes';

const initialState = {
    loading : false,
    message : '',
    error : '',
    state : {}
}

export const addInvestmentMessage = function (state = initialState, action) {
    switch (action.type) {
        case ACTION_TYPES.ADD_INVESTMENT_LOADING:
           return action.payload;
        case ACTION_TYPES.ADD_INVESTMENT:
            return action.payload;
        default: return state;
    }
}