import * as ACTION_TYPES from './actionTypes';

const initialstate = {
    loading: false,
    message: 'initial',
    data:[],
    error: ''
};

export const getIncomeTaxOpeningBalance = function (state = initialstate, action) {
    switch (action.type) {
        case ACTION_TYPES.GET_INCOME_TAX_OPENING_BALANCE_LOADING:
            return action.payload;
        case ACTION_TYPES.GET_INCOME_TAX_OPENING_BALANCE:
            return action.payload;
        case ACTION_TYPES.PUSH_INCOME_TAX_OPENING_BALANCE:
            let amount = action.payload.state.amount;
            let payreceive = action.payload.state.payreceive;
            let businessid = action.payload.businessid;
            let fyid = action.payload.fyid;
            let existingdata = state.data;
            existingdata = existingdata.filter(d => d.payreceive !== payreceive);
            existingdata.push ({
                payreceive: payreceive,
                businessid: businessid,
                fyid: fyid,
                amount: amount,
            });
            return ({
                loading: false,
                message: '',
                data: existingdata,
                error: '',
                state: action.payload.state
            })
        default: return state;
    }
}