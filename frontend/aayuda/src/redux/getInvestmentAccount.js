import * as ACTION_TYPES from './actionTypes';

const initialstate = {
    loading: false,
    message: 'initial',
    data: [],
    error: ''
};


export const getInvestmentAccount = function (state = initialstate, action) {
    switch (action.type) {
        case ACTION_TYPES.GET_INVESTMENT_ACCOUNT_LOADING:
            return action.payload;
        case ACTION_TYPES.GET_INVESTMENT_ACCOUNT:
            return action.payload;
        case ACTION_TYPES.PUSH_INVESTMENT_ACCOUNT:
            let existinginvestment = state.data;
            existinginvestment.push(action.payload.investmentaccount);
            return ({
                loading: false,
                message: '',
                data: existinginvestment,
                error: ''
            })
        case ACTION_TYPES.DELETE_INVESTMENT_ACCOUNT:
            let newData = state.data.filter(d => d.investmentid !== action.payload.id)
            return ({
                ...state,
                data: newData,
            })
        case ACTION_TYPES.DELETE_AND_INSERT_INVESTMENT_ACCOUNT:
            let id = action.payload.id;
            let existingData = state.data.filter(d => d.id !== id);
            existingData.push(action.payload.investmentaccount);
            return ({
                ...state,
                data: existingData
            })
        default: return state;
    }
}