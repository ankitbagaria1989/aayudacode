import * as ACTION_TYPES from './actionTypes';
import { sortBusiness } from './sortArrayofObjects';

const initialstate = {
    loading: false,
    message: 'initial',
    data: [],
    error: ''
};

export const getCustomer = function (state = initialstate, action) {
    switch (action.type) {
        case ACTION_TYPES.GET_CUSTOMER_LOADING:
            return action.payload;
        case ACTION_TYPES.GET_CUSTOMER:
            return action.payload;
        case ACTION_TYPES.GET_CUSTOMER_ERROR:
            return action.payload;
        case ACTION_TYPES.REFRESH_CUSTOMER:
            return ({
                message: 'initial',
                error: '',
                loading: false,
                data: []

            })
        case ACTION_TYPES.REFRESH_CUSTOMER_SECOND:
            return ({
                message: '',
                error: '',
                loading: false,
                data: []
            })
        case ACTION_TYPES.PUSH_NEW_CUSTOMER:
            let customers = state.data;
            customers.push(action.payload.customerdetail);
            return ({
                message: '',
                error: '',
                loading: false,
                data: customers
            })
        case ACTION_TYPES.PUSH_CHECK_CUSTOMER:
            let existingcust = state.data;
            let cust1 = action.payload.customerdetail;
            let present = false;
            existingcust.forEach(c => {
                if (c.customerid === cust1.customerid) present = true;
            })
            if (present === false) {
                existingcust.push(cust1);
                return ({
                    message: '',
                    error: '',
                    loading: false,
                    data: existingcust
                })
            }
            else return state;
        case ACTION_TYPES.DELETE_AND_PUSH_CUSTOMER:
            let cust = state.data;
            cust = cust.filter(c => c.customerid !== action.payload.customerdetail.customerid);
            cust.push(action.payload.customerdetail);
            cust.sort(sortBusiness);
            return ({
                message: '',
                error: '',
                loading: false,
                data: cust
            })
        default: return state;
    }
}