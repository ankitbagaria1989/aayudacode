import * as ACTION_TYPES from './actionTypes';

const initialState = {
    message: 'initial',
    disabled: false,
    loading: false,
    error: ""
};

export const registerMessage = function (state = initialState, action) {
    switch (action.type) {
        case ACTION_TYPES.REGISTER_MESSAGE:
            return action.payload;
        case ACTION_TYPES.REGISTER_LOADING:
            return action.payload;
        default:
            return state;
    }
}

export const loginMessage = function (state = initialState, action) {
    switch (action.type) {
        case ACTION_TYPES.LOGIN_MESSAGE:
            return action.payload;
        case ACTION_TYPES.LOGIN_LOADING:
            return action.payload;
        default:
            return state;
    }
}

export const addBusinessMessage = (state = initialState, action) => {
    switch (action.type) {
        case ACTION_TYPES.ADD_BUSINESS:
            return ({
                loading : false,
                disabled : false,
                message : action.payload.message,
                error: action.payload.error,
                state: action.payload.state
            })
        case ACTION_TYPES.ADD_BUSINESS_LOADING:
            return {
                error: '',
                disabled: action.payload.disabled,
                loading: action.payload.loading,
                message: ''
            }
        case ACTION_TYPES.ADD_BUSINESS_RESET:
            return ({
                loading: false,
                message: 'initial',
                disabled: false,
                error: '',
                state: {}
            })
        default:
            return state;
    }
}

export const addSubUserMessage = function (state = initialState, action) {
    switch (action.type) {
        case ACTION_TYPES.ADD_SUB_USER_MESSAGE:
            return action.payload;
        case ACTION_TYPES.ADD_SUB_USER_LOADING:
            return action.payload;
        default: return state;
    }
}

export const addFinancialYearMessage = function (state = initialState, action) {
    switch (action.type) {
        case ACTION_TYPES.ADD_FINANCIAL_YEAR_LOADING:
            return action.payload;
        case ACTION_TYPES.ADD_FINANCIAL_YEAR:
            return action.payload;
        case ACTION_TYPES.RESET_FINANCIAL_YEAR_MESSAGE:
            return ({
                error: '',
                message: 'initial',
                loading: false,
                disabled: false
            })
        default: return state;
    }
}