import * as ACTION_TYPES from './actionTypes';

const initialState = {
    loading: false,
    disabled: false,
    message: 'initial',
    error: ''
}

export const addEmployeeMessage = function (state = initialState, action) {
    switch (action.type) {
        case ACTION_TYPES.ADD_EMPLOYEE_LOADING:
            return action.payload;
        case ACTION_TYPES.ADD_EMPLOYEE:
            return action.payload;
        case ACTION_TYPES.RESET_EMPLOYEE_MESSAGE:
            return ({
                loading: false,
                disabled: false,
                message: 'initial',
                error: ''
            })
        default: return state;
    }
}