import * as ACTION_TYPES from './actionTypes';

const initialstate = {
    loading: false,
    message: 'initial',
    data: [],
    error: ''
};

export const getLoanOpeningBalance = function (state = initialstate, action) {
    switch (action.type) {
        case ACTION_TYPES.GET_LOAN_OPENING_BALANCE_LOADING:
            return action.payload;
        case ACTION_TYPES.GET_LOAN_OPENING_BALANCE:
            return action.payload;
        case ACTION_TYPES.PUSH_LOAN_OPENING_BALANCE:
            let amount = action.payload.state.amount;
            let loanaccount = action.payload.state.loanaccount;
            let payreceive = action.payload.state.payreceive;
            let businessid = action.payload.businessid;
            let fyid = action.payload.fyid;
            let da = state.data;
            let existingdata = da.filter(d => d.loanaccount === loanaccount);
            if (existingdata.length === 0) {
                da.push({
                    loanaccount: loanaccount,
                    businessid: businessid,
                    fyid: fyid,
                    amount: amount,
                    payreceive: payreceive
                })
            }
            else da.map(d => {
                if (d.loanaccount === loanaccount) {
                    d.amount = amount;
                    d.payreceive = payreceive
                }
                return d;
            })
            return ({
                loading: false,
                message: '',
                data: da,
                error: '',
                state: action.payload.state
            })
        default: return state;
    }
}