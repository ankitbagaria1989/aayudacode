export const style = {
    loginHeader:{
        backgroundColor:"rgb(86, 61, 124)"
    },
    errorWrapper: {
        lineHeight: "1.0",
        marginTop: "5px",
        marginBottom: "0px"
    },
    lineHeight1: {
        lineHeight: "1.3"
    },
    marginTopError: {
        marginTop: "-10px"
    },
    width: {
        height: "0%",
        display: "inline-block"
    },
    columnContainer: {
        display: "table",
        width: "100%"
    },
    column: {
        display: "table-cell"
    },
    columnConsignee: {
        display: "table-cell",
        height: "150px"
    },
    height: {
        height: "400px"
    },
    position: {
        position: "absolute",
        display: "contents",
        width: "100%"
    },
    columnContainerHeight: {
        width: "100%",
        height: "400px"
    },
    columnItem: {
        display: "table-cell",
        position: "inherit"
    },
    heightInvoice: {
        position: "absolute",
        width: "100%",
        paddingLeft: "14px",
        paddingRight: "17px"
    },
    itemRowAdjustement: {
        paddingLeft: "14px",
        paddingRight: "14px"
    },
    btnbackgroundblue: {
        backgroundColor: "cadetblue"
    },
    invoiceBody: {
        width: "600px"
    },
    invoiceHeader: {
        width:"600px",
        marginBottom:"20px",
        paddingLeft:"300px"
    },
    invoiceBusiness:{
        width:"100%",
        textAlign:"center"
    },
    logout: {
        cursor:"pointer"
    },
    height1: {
    },
    box: {
        overflowX: "scroll",
        overflowY: "hidden",
        whiteSpace: "nowrap"
    },
    card:{
        height : "100%",
        width:"100px"
    },
    cardMain:{
        display:"inline-block",
        width:"100px"
    },
    cardReport:{
        height : "100%",
        width:"100%"
    },
    cardForm:{
        height : "100%",
        width:"300px"
    },
    cardMainSalary:{
        display:"inline-block",
        width:"500px"
    },
    formControl:{
        color:"black"
    }
}
export const pdfstyles = {
    page: {
        display: 'flex',
        alignItems: 'center',
        fontSize: '11px',
        fontFamily: "Helvetica"
    },
    grey: {
        fontSize: "11px",
        fontFamily: "Helvetica"
    },
    grey1: {
        fontSize: "11px",
        fontFamily: "Helvetica"
    },
    taxInvoiceHeader: {
        width: '500px',
        marginTop: '20px',
        fontSize: "12px"
    },
    taxInvoiceSection: {
        position: "absolute",
        width: "100%",
        textAlign: 'center'
    },
    typeSection: {
        textAlign: "right",
        width: "100%",
        fontSize: '11px',
        paddingTop: '3px',
        fontFamily: "Helvetica-Oblique"
    },
    businessDetailSection: {
        marginTop: '10px',
        width: '500 px'
    },
    businessName: {
        fontWeight: '900',
        width: '100%',
        textAlign: 'center',
        fontSize: '14px'
    },
    businessAddress: {
        width: '100%',
        textAlign: 'center',
        fontSize: '11px',
        marginBottom: "2px"
    },
    businessFineDetail: {
        width: '100%',
        textAlign: 'center',
        fontSize: '11px'
    },
    billedToSection: {
        display: "flex",
        flexDirection: "row",
        marginTop: "5px"
    },
    customerSection: {
        width: "250px",
        height: "85px",
        borderLeft: "1px solid black",
        borderBottom: "1px solid black",
        borderRight: "1px solid black",
        borderTop: "1px solid black",
        padding: '2px'
    },
    customerSectionSecond: {
        width: "250px",
        height: "85px",
        borderBottom: "1px solid black",
        borderRight: "1px solid black",
        borderTop: "1px solid black",
        padding: '2px'
    },
    bold: {
        fontWeight: "900"
    },
    borderRight: {
        borderRight: "1px solid black"
    },
    borderBottom: {
        borderBottom: "1px solid black"
    },
    invoiceDetailSection: {
        display: "flex",
        flexDirection: "row",
        width: "500px",
        borderTop: "1px solid black",
        borderBottom: "1px solid black",
        borderRight: "1px solid black"
    },
    invoiceDetailSectionSecond: {
        display: "flex",
        flexDirection: "row",
        width: "500px",
        borderBottom: "1px solid black",
        borderRight: "1px solid black"
    },
    invoiceFineDetailWidth: {
        width: "125 px",
        padding: "2px",
        borderLeft: "1px solid black",
        fontSize: "11px",
        fontFamily: "Helvetica-Bold"
    },
    itemSection: {
        width: "500px",
        height: "270px",
        border: "1px solid black",
        marginTop: "5px",
        fontSize: "11px"
    },
    itemSectionLedger: {
        width: "575px",
        fontSize: "11px"
    },
    itemHeader: {
        display: "flex",
        flexDirection: "row",
    },
    itemDetail: {
        display: "flex",
        flexDirection: "row",
        marginTop: "5px"
    },
    itemDetailDescription: {
        display: "flex",
        flexDirection: "row"
    },
    itemDetailTax: {
        display: "flex",
        flexDirection: "row",
        fontFamily: "Helvetica-Oblique",
    }
}