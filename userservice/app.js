const createError = require('http-errors');
const express = require('express');
const path = require('path');
const morgan = require('morgan');
const fs = require('fs');
const cors = require('cors');

const indexRouter = require('./routes/index');
const usersRouter = require('./routes/users');
const loginRouter = require('./routes/login');
const registerUserRouter = require('./routes/register');
const businessRouter = require('./routes/business');
const fyRouter = require('./routes/fy');
const subUserRouter = require('./routes/subuser');
const customerRouter = require('./routes/customer');
const vendorRouter = require('./routes/vendor');
const bankDetailRouter = require('./routes/bankdetail');
const itemRouter = require('./routes/item');
const raiseInvoiceRouter = require('./routes/raiseinvoice');
const raisePurchaseRouter = require('./routes/raisePurchase');
const raiseExpenseRouter = require('./routes/raiseExpense');
const raisePaymentRouter = require('./routes/raisePayment');
const raiseBankEntryRouter = require('./routes/raiseBankEntry');
const raiseOpeningClosingRouter = require('./routes/raiseOpeningClosing');
const raisePartyRouter = require('./routes/partyOpening');
const logoutRouter = require('./routes/logout');
const cashinhandRouter = require('./routes/cashinhand');
const employeeRouter = require('./routes/employee');
const updateAttendanceRouter = require('./routes/updateattendance');
const monthAttendanceRouter = require('./routes/monthattendance');
const updateSalaryRouter = require('./routes/updatesalary');
const raiseBankRouter = require('./routes/bankopeningbalance');
const gstOpeningBalanceRouter = require('./routes/gstopeningbalance');
const debitCreditRouter = require('./routes/debitcredit');
const deleteEntryRouter = require('./routes/deleteEntry');
const expenseHeadRouter = require('./routes/expensehead');
const raiseIncomeOSRouter = require('./routes/raiseincomeos');
const reviseSalaryRouter = require('./routes/reviseSalary');
const terminateEmployeeRouter = require('./routes/terminateemployee');
const changePasswordRouter = require('./routes/changePassword');
const loanaccountRouter = require('./routes/loanaccount');
const raiseLoanRouter = require('./routes/raiseloan');
const raiseInvestmentRouter = require('./routes/raiseInvestment');
const incomeHeadRouter = require('./routes/incomehead');
const assetaccountRouter = require('./routes/assetaccount');
const raiseDepreciationRouter = require('./routes/raiseDepreciation');
const raiseLoanOpeningRouter = require('./routes/loanopeningbalance');
const raiseAssetOpeningRouter = require('./routes/assetopeningbalance');
const investmentaccountRouter = require('./routes/investmentaccount');
const raiseInvestmentOpeningRouter = require('./routes/investmentOpeningBalance');
const raiseTdsOpeningRouter = require('./routes/tdsopeningbalance');
const raiseRCOpeningRouter = require('./routes/rcopeningbalance');
const raiseStockRouter = require('./routes/stockopeningbalance');
const raiseIncomeTaxOpeningRouter = require('./routes/incometaxopeningbalance');
const stockJournalRouter = require('./routes/stockjournal');
const raiseEmployeeOpeningRouter = require('./routes/employeeopeningbalance');
const app = express();
require('dotenv').config();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

const corsOptions = {
  "origin": ['http://localhost:5000', 'http://localhost:3000', 'http://192.168.43.37:5000', process.env.IP],
  "methods": ['GET', 'PUT', 'POST', 'DELETE'],
  "optionsSuccessStatus": 200,
  "credentials": true
}
const accessLogStream = fs.createWriteStream(path.join(__dirname, 'access.log'), { flags: 'a' });
app.use(cors(corsOptions));
app.options('*', cors());
app.use(morgan('common', { stream: accessLogStream }));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'public')));
app.use('/api', indexRouter);
app.use('/api/users', usersRouter);
app.use('/api/login', loginRouter);
app.use('/api/register', registerUserRouter);
app.use('/api/business', businessRouter);
app.use('/api/fy', fyRouter);
app.use('/api/subuser', subUserRouter);
app.use('/api/customer', customerRouter);
app.use('/api/vendor', vendorRouter);
app.use('/api/bankdetail', bankDetailRouter);
app.use('/api/item', itemRouter);
app.use('/api/raiseinvoice', raiseInvoiceRouter);
app.use('/api/raisepurchase', raisePurchaseRouter);
app.use('/api/raiseexpense', raiseExpenseRouter);
app.use('/api/raisepayment', raisePaymentRouter);
app.use('/api/raisebankentry', raiseBankEntryRouter);
app.use('/api/raiseopeningclosing', raiseOpeningClosingRouter);
app.use('/api/partyOpening', raisePartyRouter);
app.use('/api/logout', logoutRouter);
app.use('/api/cashinhand', cashinhandRouter);
app.use('/api/employee', employeeRouter);
app.use('/api/updateattendance', updateAttendanceRouter);
app.use('/api/monthattendance', monthAttendanceRouter);
app.use('/api/updatesalary', updateSalaryRouter);
app.use('/api/bankOpeningBalance', raiseBankRouter);
app.use('/api/gstOpeningBalance', gstOpeningBalanceRouter);
app.use('/api/debitcredit', debitCreditRouter);
app.use('/api/deleteEntry', deleteEntryRouter);
app.use('/api/expensehead', expenseHeadRouter);
app.use('/api/raiseincomeos', raiseIncomeOSRouter);
app.use('/api/reviseSalary', reviseSalaryRouter);
app.use('/api/termninateEmployee', terminateEmployeeRouter);
app.use('/api/changepassword', changePasswordRouter);
app.use('/api/loanaccount', loanaccountRouter);
app.use('/api/raiseloan', raiseLoanRouter);
app.use('/api/raiseinvestment', raiseInvestmentRouter);
app.use('/api/incomehead', incomeHeadRouter);
app.use('/api/assetaccount', assetaccountRouter);
app.use('/api/raiseDepreciation', raiseDepreciationRouter);
app.use('/api/loanOpeningBalance', raiseLoanOpeningRouter);
app.use('/api/assetOpeningBalance', raiseAssetOpeningRouter);
app.use('/api/investmentaccount', investmentaccountRouter);
app.use('/api/investmentOpeningBalance', raiseInvestmentOpeningRouter);
app.use('/api/tdsOpeningbalance', raiseTdsOpeningRouter);
app.use('/api/rcOpeningBalance', raiseRCOpeningRouter);
app.use('/api/stockOpeningBalance', raiseStockRouter);
app.use('/api/incometaxOpeningbalance', raiseIncomeTaxOpeningRouter);
app.use('/api/stockjournal', stockJournalRouter);
app.use('/api/employeeOpeningBalance', raiseEmployeeOpeningRouter);
// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});
// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};
  console.log(err.message);
  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
