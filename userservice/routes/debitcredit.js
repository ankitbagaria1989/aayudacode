const express = require('express');
const bodyParser = require('body-parser');
const mysql = require('mysql2/promise');
const cookieParser = require('cookie-parser');
const pool = require('../reusableFunctionalities/dbconnection');
const getSessionId = require('../reusableFunctionalities/getSessionId');
const v4 = require('node-uuid');
const fs = require('fs');

const debitCreditRouter = express.Router();
debitCreditRouter.use(bodyParser.json());
debitCreditRouter.use(cookieParser());

let sessionId = undefined;
const writer = fs.createWriteStream('./access.log', { flags: 'a' });

debitCreditRouter.use((req, res, next) => {
    if (req.cookies['userservice'] == undefined) {
        res.statusCode = 401;
        res.setHeader("Content-Type", "application/json");
        res.json({ result: 'Please login in order to continue', data: [] });
    }
    else if (req.cookies['userservice']) {
        sessionId = getSessionId(req.cookies['userservice']);
        if (sessionId == false) {
            res.clearCookie('userservice');
            res.statusCode = 401;
            res.setHeader("Content-Type", "application/json");
            res.json({ result: 'Please Stop Tampering with Data', data: [] });
        }
        else {
            return next();
        }
    }
})

debitCreditRouter.route('/:businessid/:fyid')
    .get(async (req, res, next) => {
        try {
            let businessid = mysql.escape(req.params.businessid);
            let fyid = mysql.escape(req.params.fyid);
            let result = await pool.query("CALL getDebitCredit (" + mysql.escape(sessionId) + "," + businessid + "," + fyid + ")");
            if (result[0][1][0]) {
                if (result[0][1][0].result === 'success') {
                    res.statusCode = 200;
                    res.setHeader("Content-Type", "application/json");
                    res.json({ result: "success", data: result[0][0] });
                }
            }
            else if (result[0][0][0].result === 'Invalid Session Id') {
                res.clearCookie('userservice');
                res.statusCode = 401;
                res.setHeader("Content-Type", "application/json");
                res.json({ result: 'Invalid Login', data: [] });
            }
            else throw new Error("Database Error");
        }
        catch (err) {
            writer.write(err.message);
            res.statusCode = 401;
            res.setHeader("Content-Type", "application/json");
            res.json({ result: err.message, data: [] });
        }
    })

    .post(async (req, res, next) => {
        try {
            let businessid = mysql.escape(req.params.businessid);
            let fyid = mysql.escape(req.params.fyid);
            let debitcredit = mysql.escape(req.body.debitcredit);
            let issuereceived = mysql.escape(req.body.issuereceived);
            let vendorcustomer = mysql.escape(req.body.vendorcustomer);
            let partyid = mysql.escape(req.body.partyid.split("...")[0]);
            let businessname = mysql.escape(req.body.partyid.split("...")[1]);
            let basic = mysql.escape(req.body.basic);
            let igst = mysql.escape(req.body.igst);
            let cgst = mysql.escape(req.body.cgst);
            let sgst = mysql.escape(req.body.sgst);
            let total = mysql.escape(req.body.total);
            let date = mysql.escape(req.body.date);
            let voucherno = mysql.escape(req.body.voucherno);
            let remarks = mysql.escape(req.body.remarks);
            let id = v4();
            let idempotencykey = mysql.escape(req.body.partyid + req.params.fyid + req.body.voucherno);
            let result = await pool.query("CALL insertDebitCredit (" + mysql.escape(sessionId) + "," + businessid + "," + fyid + "," + debitcredit + "," + issuereceived + "," + vendorcustomer + "," + partyid + "," + businessname + "," + basic + "," + igst + "," + cgst + "," + sgst + "," + total + "," + date + "," + voucherno + "," + mysql.escape(id) + "," + idempotencykey + "," + remarks +")");
            if (result[0][0][0].result) {
                if (result[0][0][0].result === "success") {
                    res.statusCode = 200;
                    res.setHeader("Content-Type", "application/json");
                    res.json({ result: "success", id: { id: result[0][1][0].checkid } });
                }
                else if (result[0][0][0].result === 'Invalid Session Id') {
                    res.clearCookie('userservice');
                    res.statusCode = 401;
                    res.setHeader("Content-Type", "application/json");
                    res.json({ result: 'Invalid Login' });
                }
            }
            else if (result[0][1][0].result) {
                if (result[0][1][0].result === "Already Registered") {
                    res.statusCode = 200;
                    res.setHeader("Content-Type", "application/json");
                    res.json({
                        result: "Already Registered", data: result[0][0][0]
                    });
                }
            }
            else throw new error("Database Error");
        }
        catch (err) {
            writer.write(err.message);
            res.statusCode = 401;
            res.setHeader("Content-Type", "application/json");
            res.json({ result: err.message, data: [] });
        }
    })

module.exports = debitCreditRouter;