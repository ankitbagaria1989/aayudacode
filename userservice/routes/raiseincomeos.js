const express = require('express');
const bodyParser = require('body-parser');
const mysql = require('mysql2/promise');
const cookieParser = require('cookie-parser');
const pool = require('../reusableFunctionalities/dbconnection');
const getSessionId = require('../reusableFunctionalities/getSessionId');
const v4 = require('node-uuid');
const fs = require('fs');
const raiseIncomeOSRouter = express.Router();
raiseIncomeOSRouter.use(bodyParser.json());
raiseIncomeOSRouter.use(cookieParser());

let sessionId = undefined;
const writer = fs.createWriteStream('./access.log', { flags: 'a' });
raiseIncomeOSRouter.use((req, res, next) => {
    if (req.cookies['userservice'] == undefined) {
        res.statusCode = 401;
        res.setHeader("Content-Type", "application/json");
        res.json({ result: 'Please login in order to continue', data: [] });
    }
    else if (req.cookies['userservice']) {
        sessionId = getSessionId(req.cookies['userservice']);
        if (sessionId == false) {
            res.clearCookie('userservice');
            res.statusCode = 401;
            res.setHeader("Content-Type", "application/json");
            res.json({ result: 'Please Stop Tampering with Data', data: [] });
        }
        else {
            return next();
        }
    }
})

raiseIncomeOSRouter.route("/:businessid/:fyid")
    .post(async (req, res, next) => {
        try {
            let businessid = mysql.escape(req.params.businessid);
            let fyid = mysql.escape(req.params.fyid);
            let category = mysql.escape(req.body.category);
            let description = mysql.escape(req.body.description);
            let date = mysql.escape(req.body.date);
            let amount = mysql.escape(req.body.amount);
            let voucherno = mysql.escape(req.body.voucherno);
            let paymentmode = mysql.escape(req.body.paymentmode);
            let transactionno = mysql.escape(req.body.transactionno);
            let bankdetail = mysql.escape(req.body.bankid);
            let tds = mysql.escape(req.body.tds);
            let tdsamount = mysql.escape(req.body.tdsamount);
            let incomeid = v4();
            let cash = mysql.escape(req.body.cash);
            let customerid = mysql.escape(req.body.customerid);
            let customername = mysql.escape(req.body.customername);
            let idempotencykey = mysql.escape(req.params.fyid + req.body.voucherno);
            let result = await pool.query("CALL insertIncomeOS ( " + mysql.escape(sessionId) + "," + mysql.escape(incomeid) + "," + businessid + "," + fyid + "," + category + "," + description + "," + date + "," + amount + "," + voucherno + "," + idempotencykey + "," + paymentmode + "," + transactionno + "," + bankdetail + "," + tds + "," + tdsamount + "," + cash + "," + customerid + "," + customername + ")");
            if (result[0][0][0].result) {
                if (result[0][0][0].result === "success") {
                    res.statusCode = 200;
                    res.setHeader("Content-Type", "application/json");
                    res.json({ result: "success", id: { incomeid: result[0][1][0].checkid } });
                }
                else if (result[0][0][0].result === 'Invalid Session Id') {
                    res.clearCookie('userservice');
                    res.statusCode = 401;
                    res.setHeader("Content-Type", "application/json");
                    res.json({ result: 'Invalid Login' });
                }
            }
            else if (result[0][1][0].result) {
                if (result[0][1][0].result === "Already Registered") {
                    res.statusCode = 200;
                    res.setHeader("Content-Type", "application/json");
                    res.json({
                        result: "Already Registered", data: result[0][0][0]
                    });
                }
            }
            else throw new error(result[0][0][0].result);
        }
        catch (err) {
            writer.write(err.message);
            res.statusCode = 404;
            res.setHeader("Content-Type", "application/json");
            res.json({ "result": err.message });
        }
    })

    .get(async (req, res, next) => {
        try {
            let businessid = mysql.escape(req.params.businessid);
            let fyid = mysql.escape(req.params.fyid);
            const sql = "CALL getIncomeOS ( " + mysql.escape(sessionId) + "," + businessid + "," + fyid + ")";
            let result = await pool.query(sql);
            if (result[0][1][0]) {
                if (result[0][1][0].result === 'success') {
                    res.statusCode = 200;
                    res.setHeader("Content-Type", "application/json");
                    res.json({ result: "success", data: result[0][0] });
                }
            }
            else if (result[0][0][0].result === 'Invalid Session Id') {
                res.clearCookie('userservice');
                res.statusCode = 401;
                res.setHeader("Content-Type", "application/json");
                res.json({ result: 'Invalid Login', data: [] });
            }
            else throw new Error("Database Error");
        } catch (err) {
            writer.write(err.message);
            res.statusCode = 404;
            res.setHeader("Content-Type", "application/json");
            res.json({ "result": err.message, "data": [] });
        }
    })

    .delete(async (req, res, next) => {
        try {
            let expenseid = mysql.escape(req.body.expenseid);
            let result = await pool.query("CALL deleteExpense (" + mysql.escape(sessionId) + "," + expenseid + ")");
            if (result[0][0][0].result === "success") {
                res.statusCode = 200;
                res.setHeader("Content-Type", "application/json");
                res.json({ result: "success" });
            }
            else if (result[0][0][0] === "Invalid Session Id") {
                res.clearCookie('userservice');
                res.statusCode = 401;
                res.setHeader("Content-Type", "application/json");
                res.json({ result: 'Invalid Login', data: [] });
            }
            else throw new Error("Database Error");
        } catch (err) {
            writer.write(err.message);
            res.statusCode = 404;
            res.setHeader("Content-Type", "application/json");
            res.json({ "result": err.message });
        }
    })

module.exports = raiseIncomeOSRouter;