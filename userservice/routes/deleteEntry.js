const express = require('express');
const bodyParser = require('body-parser');
const mysql = require('mysql2/promise');
const cookieParser = require('cookie-parser');
const pool = require('../reusableFunctionalities/dbconnection');
const getSessionId = require('../reusableFunctionalities/getSessionId');
const fs = require('fs');

const deleteEntryRouter = express.Router();
deleteEntryRouter.use(bodyParser.json());
deleteEntryRouter.use(cookieParser());

let sessionId = undefined;
const writer = fs.createWriteStream('./access.log', { flags: 'a' });

deleteEntryRouter.use((req, res, next) => {
    if (req.cookies['userservice'] == undefined) {
        res.statusCode = 401;
        res.setHeader("Content-Type", "application/json");
        res.json({ result: 'Please login in order to continue', data: [] });
    }
    else if (req.cookies['userservice']) {
        sessionId = getSessionId(req.cookies['userservice']);
        if (sessionId == false) {
            res.clearCookie('userservice');
            res.statusCode = 401;
            res.setHeader("Content-Type", "application/json");
            res.json({ result: 'Please Stop Tampering with Data', data: [] });
        }
        else {
            return next();
        }
    }
})

deleteEntryRouter.route("/:businessid/:fyid")
    .delete(async (req, res, next) => {
        try {
            let businessid = mysql.escape(req.params.businessid);
            let fyid = mysql.escape(req.params.fyid);
            let id = req.body.id;
            let segment = req.body.segment;
            let result = undefined;
            if (segment === 'purchase') result = await pool.query("CALL deletePurchase ( " + mysql.escape(sessionId) + "," + businessid + "," + fyid + "," + mysql.escape(id) + ")");
            else if (segment === 'expense') result = await pool.query("CALL deleteExpense ( " + mysql.escape(sessionId) + "," + ysql.escape(id) + ")");
            else if (segment === 'debitcredit') result = await pool.query("CALL deleteDebitCredit ( " + mysql.escape(sessionId) + "," + businessid + "," + fyid + "," + mysql.escape(id) + ")");
            else if (segment === 'income') result = await pool.query("CALL deleteIncomeOS ( " + mysql.escape(sessionId) + "," + businessid + "," + fyid + "," + mysql.escape(id) + ")");
            else if (segment === 'expensehead') result = await pool.query("CALL deleteExpenseHead ( " + mysql.escape(sessionId) + "," + businessid + "," + fyid + "," + mysql.escape(id) + ")");
            else if (segment === 'payment') result = await pool.query("CALL deletePayment ( " + mysql.escape(sessionId) + "," + mysql.escape(id) + ")");
            else if (segment === 'bankentry') result = await pool.query("CALL deleteBankEntry ( " + mysql.escape(sessionId) + "," + ysql.escape(id) + ")");
            else if (segment === 'loan') result = await pool.query("CALL deleteLoan ( " + mysql.escape(sessionId) + "," + mysql.escape(id) + ")");
            else if (segment === 'investment') result = await pool.query("CALL deleteInvestment ( " + mysql.escape(sessionId) + "," + mysql.escape(id) + ")");
            else if (segment === 'asset') result = await pool.query("CALL deleteAsset ( " + mysql.escape(sessionId) + "," + mysql.escape(id.id) + "," + mysql.escape(id.type) + ")");
            else if (segment === 'depreciation') result = await pool.query("CALL deleteDepreciation ( " + mysql.escape(sessionId) + "," + mysql.escape(id.fyid) + "," + mysql.escape(id.asset) + ")");
            
            if (result[0][0][0].result === "success") {
                res.statusCode = 200;
                res.setHeader("Content-Type", "application/json");
                res.json({ result: "success" });

            }
            else if (result[0][0][0].result === 'Invalid Session Id') {
                res.statusCode = 200;
                res.setHeader("Content-Type", "application/json");
                res.json({
                    result: "invalid Session Id"
                });
            }
            else throw new error(result[0][0][0].result);
        }
        catch (err) {
            writer.write(err.message);
            res.statusCode = 404;
            res.setHeader("Content-Type", "application/json");
            res.json({ "result": err.message });
        }
    })

deleteEntryRouter.route("/:businessid")
.delete(async (req, res, next) => {
    try {
        let businessid = mysql.escape(req.params.businessid);
        let id = mysql.escape(req.body.id);
        let segment = req.body.segment;
        let result = undefined;
        if (segment === 'expensehead') result = await pool.query("CALL deleteExpenseHead ( " + mysql.escape(sessionId) + "," + businessid + "," + id + ")");
        if (result[0][0][0].result === "success") {
            res.statusCode = 200;
            res.setHeader("Content-Type", "application/json");
            res.json({ result: "success" });

        }
        else if (result[0][0][0].result === 'Invalid Session Id') {
            res.statusCode = 200;
            res.setHeader("Content-Type", "application/json");
            res.json({
                result: "invalid Session Id"
            });
        }
        else throw new error(result[0][0][0].result);
    }
    catch (err) {
        writer.write(err.message);
        res.statusCode = 404;
        res.setHeader("Content-Type", "application/json");
        res.json({ "result": err.message });
    }
})
module.exports = deleteEntryRouter;