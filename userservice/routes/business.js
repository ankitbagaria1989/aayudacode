const express = require('express');
const bodyParser = require('body-parser');
const mysql = require('mysql2/promise');
const cookieParser = require('cookie-parser');
const pool = require('../reusableFunctionalities/dbconnection');
const getSessionId = require('../reusableFunctionalities/getSessionId');
const v4 = require('node-uuid');
const fs = require('fs');

const businessRouter = express.Router();
businessRouter.use(bodyParser.json());
businessRouter.use(cookieParser());

let sessionId = undefined;
const writer = fs.createWriteStream('./access.log', { flags: 'a' });

businessRouter.use((req, res, next) => {
    if (req.cookies['userservice'] == undefined) {
        res.statusCode = 401;
        res.setHeader("Content-Type", "application/json");
        res.json({ result: 'Please login in order to continue', data: [] });
    }
    else if (req.cookies['userservice']) {
        sessionId = getSessionId(req.cookies['userservice']);
        if (sessionId == false) {
            res.clearCookie('userservice');
            res.statusCode = 401;
            res.setHeader("Content-Type", "application/json");
            res.json({ result: 'Please Stop Tampering with Data', data: [] });
        }
        else {
            return next();
        }
    }
});
//handling business
businessRouter.route('/')
    .get(async (req, res, next) => {
        if (sessionId) {
            try {
                const sql = "CALL getBusiness ( " + mysql.escape(sessionId) + ")";
                let result = await pool.query(sql);
                if (result[0][1][0]) {
                    if (result[0][1][0].result === 'success') {
                        res.statusCode = 200;
                        res.setHeader("Content-Type", "application/json");
                        res.json({ result: "success", data: result[0][0] });
                    }
                }
                else if (result[0][0][0].result === 'Invalid Session Id') {
                    res.clearCookie('userservice');
                    res.statusCode = 200;
                    res.setHeader("Content-Type", "application/json");
                    res.json({ result: 'Invalid Login', data: [] });
                }
                else throw new Error("DATABASE Failed. Please Try Again");
            } catch (err) {
                writer.write(err.message);
                res.statusCode = 401;
                res.setHeader("Content-Type", "application/json");
                res.json({ result: err.message, data: [] });
            }
        }
    })

    .put(async (req, res, next) => {
        try {
            let businessid = mysql.escape(req.body.businessid);
            let businessname = mysql.escape(req.body.businessname);
            let alias = mysql.escape(req.body.alias);
            let gstin = mysql.escape(req.body.gstin);
            let address = mysql.escape(req.body.address);
            let state = mysql.escape(req.body.state);
            let pincode = mysql.escape(req.body.pincode);
            let mobile = mysql.escape(req.body.mobile);
            let landline = mysql.escape(req.body.landline);
            let email = mysql.escape(req.body.email);
            let stateut = mysql.escape(req.body.stateut);
            let fax = mysql.escape(req.body.fax);
            let pan = mysql.escape(req.body.pan);
            let businesstype = mysql.escape(req.body.businesstype);
            let noofshares = mysql.escape(req.body.noofshares);
            let fv = mysql.escape(req.body.fv);
            let result = await pool.query("CALL updateBusiness (" + businessid + "," + businessname + "," + alias + "," + gstin + "," + address + "," + state + "," + landline + "," + mobile + "," + email + "," + fax + "," + pincode + "," + mysql.escape(sessionId) + "," + stateut + "," + pan + "," + businesstype + "," + noofshares + "," + fv + ")");
            if (result[0][0][0].result === 'success') {
                res.statusCode = 200;
                res.setHeader("Content-Type", "application/json");
                res.json({ result: "success" });
            }
            else if (result[0][0][0].result === 'Invalid Session Id') {
                res.clearCookie('userservice');
                res.statusCode = 401;
                res.setHeader("Content-Type", "application/json");
                res.json({ result: 'Invalid Login' });
            }
            else throw new Error("Database error");
        } catch (err) {
            writer.write(err.message);
            res.statusCode = 401;
            res.setHeader("Content-Type", "application/json");
            res.json({ result: err.message });
        }
    })

    .post(async (req, res, next) => {
        if (sessionId) {
            let businessname = mysql.escape(req.body.businessname);
            let alias = mysql.escape(req.body.alias);
            let gstin = mysql.escape(req.body.gstin);
            let address = mysql.escape(req.body.address);
            let state = mysql.escape(req.body.state);
            let landline = mysql.escape(req.body.landline);
            let mobile = mysql.escape(req.body.mobile);
            let email = mysql.escape(req.body.email);
            let pincode = mysql.escape(req.body.pincode);
            let fax = mysql.escape(req.body.fax);
            let stateut = mysql.escape(req.body.stateut);
            let pan = mysql.escape(req.body.pan);
            let businesstype = mysql.escape(req.body.businesstype);
            let noofshares = mysql.escape(req.body.noofshares);
            let fv = mysql.escape(req.body.fv);
            try {
                let key = v4();
                let sql = "CALL insertbusiness (" + mysql.escape(key) + "," + businessname + "," + alias + "," + gstin + "," + address + "," + state + "," + landline + "," + mobile + "," + email + "," + fax + "," + pincode + "," + mysql.escape(sessionId) + "," + stateut + "," + pan + "," + businesstype + "," + noofshares + "," + fv + ")";
                let result = await pool.query(sql);
                if (result[0][0][0].result) {
                    if (result[0][0][0].result === "success") {
                        res.statusCode = 200;
                        res.setHeader("Content-Type", "application/json");
                        res.json({ result: "success", id: { businessid: result[0][1][0].checkid } });
                    }
                }
                else if (result[0][1][0].result) {
                    if (result[0][1][0].result === "Already Registered") {
                        res.statusCode = 200;
                        res.setHeader("Content-Type", "application/json");
                        res.json({ result: "Already Registered", data: result[0][0][0] });
                    }
                }
                else if (result[0][0][0].result === 'Invalid SessionId') {
                    res.clearCookie('userservice');
                    res.statusCode = 401;
                    res.setHeader("Content-Type", "application/json");
                    res.json({ result: 'Invalid Login', data: [] });
                }
                else throw new Error("Problem Inserting Into Database.");
            }
            catch (err) {
                writer.write(err.message);
                res.statusCode = 401;
                res.setHeader("Content-Type", "application/json");
                res.json({ result: err.message });
            }
        }
    })

businessRouter.route('/:businessid')
    .get(async (req, res, next) => {
        try {
            if (sessionId) {
                let businessid = mysql.escape(req.params.businessid);
                const sql = "CALL getIndividualBusiness (" + mysql.escape(sessionId) + "," + businessid + ")";
                let result = await pool.query(sql);
                if (result[0][1][0]) {
                    if (result[0][1][0].result === 'success') {
                        res.statusCode = 200;
                        res.setHeader("Content-Type", "application/json");
                        res.json({ result: "success", data: result[0][0] });
                    }
                }
                else if (result[0][0][0].result === 'Invalid Session Id') {
                    res.clearCookie('userservice');
                    res.statusCode = 401;
                    res.setHeader("Content-Type", "application/json");
                    res.json({ result: 'Invalid Login', data: [] });
                }
                else throw new Error("DATABASE Failed. Please Try Again");
            }
        }
        catch (err) {
            writer.write(err.message);
            res.statusCode = 401;
            res.setHeader("Content-Type", "application/json");
            res.json({ result: "err.message" })
        }
    })
module.exports = businessRouter;
