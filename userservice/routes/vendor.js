const express = require('express');
const bodyParser = require('body-parser');
const mysql = require('mysql2/promise');
const cookieParser = require('cookie-parser');
const pool = require('../reusableFunctionalities/dbconnection');
const getSessionId = require('../reusableFunctionalities/getSessionId');
const v4 = require('node-uuid');
const fs = require('fs');
const vendorRouter = express.Router();
vendorRouter.use(bodyParser.json());
vendorRouter.use(cookieParser());

let sessionId = undefined;
const writer = fs.createWriteStream('./access.log', { flags: 'a' });
vendorRouter.use((req, res, next) => {
    if (req.cookies['userservice'] == undefined) {
        res.statusCode = 401;
        res.setHeader("Content-Type", "application/json");
        res.json({ result: 'Please login in order to continue', data: [] });
    }
    else if (req.cookies['userservice']) {
        sessionId = getSessionId(req.cookies['userservice']);
        if (sessionId == false) {
            res.clearCookie('userservice');
            res.statusCode = 401;
            res.setHeader("Content-Type", "application/json");
            res.json({ result: 'Please Stop Tampering with Data', data: [] });
        }
        else {
            return next();
        }
    }
})

vendorRouter.route('/:businessid')
    .get(async (req, res, next) => {
        try {
            let businessid = mysql.escape(req.params.businessid);
            const sql = "CALL getVendor ( " + mysql.escape(sessionId) + "," + businessid + ")";
            let result = await pool.query(sql);
            if (result[0][1][0]) {
                if (result[0][1][0].result === 'success') {
                    res.statusCode = 200;
                    res.setHeader("Content-Type", "application/json");
                    res.json({ result: "success", data: result[0][0] });
                }
            }
            else if (result[0][0][0].result === 'Invalid Session Id') {
                res.clearCookie('userservice');
                res.statusCode = 401;
                res.setHeader("Content-Type", "application/json");
                res.json({ result: 'Invalid Login', data: [] });
            }
            else throw new Error("Database Error");
        } catch (err) {
            writer.write(err.message);
            res.statusCode = 401;
            res.setHeader("Content-Type", "application/json");
            res.json({ result: err.message, data: [] });
        }

    })

    .post(async (req, res, next) => {
        try {
            let businessid = req.params.businessid;
            let businessname = mysql.escape(req.body.vendordetail.businessname);
            let address = mysql.escape(req.body.vendordetail.address);
            let gstin = mysql.escape(req.body.vendordetail.gstin);
            let state = mysql.escape(req.body.vendordetail.state);
            let mobile = mysql.escape(req.body.vendordetail.mobile);
            let email = mysql.escape(req.body.vendordetail.email);
            let landline = mysql.escape(req.body.vendordetail.landline);
            let pincode = mysql.escape(req.body.vendordetail.pincode);
            let alias = mysql.escape(req.body.vendordetail.alias);
            let vendorid = v4();
            let result = await pool.query("CALL insertVendor (" + mysql.escape(sessionId) + "," + mysql.escape(businessid) + "," + businessname + "," + gstin + "," + address + "," + state + "," + landline + "," + mobile + "," + email + "," + pincode + "," + mysql.escape(vendorid) + "," + alias + ")");
            if ((result[0][0][0].result === "success")) {
                res.statusCode = 200;
                res.setHeader("Content-Type", "application/json");
                res.json({ result: "success", id: { businessid: businessid, vendorid: result[0][1][0].checkid } });
            }
            else if (result[0][0][0].result === 'Invalid Session Id') {
                res.clearCookie('userservice');
                res.statusCode = 401;
                res.setHeader("Content-Type", "application/json");
                res.json({ result: 'Invalid Login' });
            }
            else throw new error("Database Error");
        } catch (err) {
            writer.write(err.message);
            res.statusCode = 401;
            res.setHeader("Content-Type", "application/json");
            res.json({ result: err.message });
        }
    })

    .put(async (req, res, next) => {
        try {
            let businessid = mysql.escape(req.params.businessid);
            let vendorid = mysql.escape(req.body.vendorid);
            let businessname = mysql.escape(req.body.vendordetail.businessname);
            let address = mysql.escape(req.body.vendordetail.address);
            let gstin = mysql.escape(req.body.vendordetail.gstin);
            let state = mysql.escape(req.body.vendordetail.state);
            let mobile = mysql.escape(req.body.vendordetail.mobile);
            let email = mysql.escape(req.body.vendordetail.email);
            let landline = mysql.escape(req.body.vendordetail.landline);
            let pincode = mysql.escape(req.body.vendordetail.pincode);
            let alias = mysql.escape(req.body.vendordetail.alias);
            let result = await pool.query("CALL updateVendor (" + mysql.escape(sessionId) + "," + businessid + "," + businessname + "," + gstin + "," + address + "," + state + "," + landline + "," + mobile + "," + email + "," + pincode + "," + vendorid + "," + alias + ")");
            if (result[0][0][0].result === "success") {
                res.statusCode = 200;
                res.setHeader("Content-Type", "application/json");
                res.json({ result: "success" });
            }
            else if (result[0][0][0].result === 'Invalid Session Id') {
                res.clearCookie('userservice');
                res.statusCode = 401;
                res.setHeader("Content-Type", "application/json");
                res.json({ result: 'Invalid Login' });
            }
            else throw new error("Database Error");
        } catch (err) {
            writer.write(err.message);
            res.statusCode = 401;
            res.setHeader("Content-Type", "application/json");
            res.json({ result: err.message });
        }
    })

module.exports = vendorRouter;