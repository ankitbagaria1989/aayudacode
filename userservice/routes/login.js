const express = require('express');
const bodyParser = require('body-parser');
const mysql = require('mysql2/promise');
const pool = require('../reusableFunctionalities/dbconnection');
const session = require('express-session');
const cookieParser = require('cookie-parser');
const getSessionId = require('../reusableFunctionalities/getSessionId');
const bcrypt = require('bcryptjs');
const v4 = require('node-uuid');
const fs = require('fs');
const loginRouter = express.Router();
loginRouter.use(bodyParser.json());
const writer = fs.createWriteStream('./access.log', { flags: 'a' });
var sessionConfig = session({
    genid: function (req) {
        return v4();
    },
    name: 'userservice',
    resave: false,
    saveUninitialized: false,
    secret: 'dravid@987&!!--true,,.//eijfvliajh894798173!!',
    cookie: {
        maxAge: 43200000000,
        httpOnly: false,
        sameSite: true,
        secure: false
    }
});

loginRouter.use((req, res, next) => {
    if (req.cookies) {
        if (req.cookies.userservice) {
            var sessionId = getSessionId(req.cookies['userservice']);
            if (sessionId == false) {
                res.clearCookie('userservice');
                res.statusCode = 401;
                res.setHeader("Content-Type", "application/json");
                res.json({ result: "Stop Tampering with data !!" });
            }
            else {
                res.statusCode = 401;
                res.setHeader("Content-Type", "application/json");
                res.json({ result: "Already LoggedIn !!" });
            }
        }
    }
    else return sessionConfig(req, res, next);
    
});

loginRouter.route('/')
    .post(async (req, res, next) => {
        let email = mysql.escape(req.body.email);
        let password = mysql.escape(req.body.password);
        let sessionid = mysql.escape(req.session.id);
        console.log(JSON.stringify(req.session));
        console.log((req.session.id));
        try {
            let result = await pool.query('SELECT hashpassword AS password,userid from users WHERE useremail = ' + email);
            if (result[0][0].password && result[0][0].userid) {
                // release connection handle and redirect to business
                let bcryptResult = await bcrypt.compare(password, result[0][0].password);
                if (bcryptResult == true) {
                    let sessionResult = await pool.query("INSERT INTO tokendetail (userid,tokenid) VALUES (" + mysql.escape(result[0][0].userid) + "," + sessionid + ")");
                    if (sessionResult[0].affectedRows > 0) {
                        req.session.user = sessionid;
                        res.statusCode = 200;
                        res.setHeader('Content-Type', 'application/json');
                        res.json({
                            result: "success",
                        });
                    }
                    else {
                        throw new Error("Database Error. Please Try Again.");
                    }
                }
                else {
                    //tempConn.release();
                    throw new Error("Invalid Username or Password");
                }
            }
            else {
                //tempConn.release();
                throw new Error("Invalid Username or Password");
            }
        } catch (err) {
            if (err.message.match("Cannot read property 'password' of undefined")) err.message = "Invalid Username or Password";
            writer.write(err.message);
            res.statusCode = 404;
            res.setHeader("Content-Type", "application/json");
            res.json({ result: err.message });
        }
    })

module.exports = loginRouter;

