const express = require('express');
const bodyParser = require('body-parser');
const mysql = require('mysql2/promise');
const cookieParser = require('cookie-parser');
const pool = require('../reusableFunctionalities/dbconnection');
const getSessionId = require('../reusableFunctionalities/getSessionId');
const v4 = require('node-uuid');
const fs = require('fs');
const raiseInvestmentRouter = express.Router();
raiseInvestmentRouter.use(bodyParser.json());
raiseInvestmentRouter.use(cookieParser());

let sessionId = undefined;
const writer = fs.createWriteStream('./access.log', { flags: 'a' });
raiseInvestmentRouter.use((req, res, next) => {
    if (req.cookies['userservice'] == undefined) {
        res.statusCode = 401;
        res.setHeader("Content-Type", "application/json");
        res.json({ result: 'Please login in order to continue', data: [] });
    }
    else if (req.cookies['userservice']) {
        sessionId = getSessionId(req.cookies['userservice']);
        if (sessionId == false) {
            res.clearCookie('userservice');
            res.statusCode = 401;
            res.setHeader("Content-Type", "application/json");
            res.json({ result: 'Please Stop Tampering with Data', data: [] });
        }
        else {
            return next();
        }
    }
})

raiseInvestmentRouter.route("/:businessid/:fyid")
    .post(async (req, res, next) => {
        try {
            let businessid = mysql.escape(req.params.businessid);
            let fyid = mysql.escape(req.params.fyid);
            let fy = mysql.escape(req.body.fy);
            let activity = mysql.escape(req.body.activity);
            let madereceived = mysql.escape(req.body.madereceived);
            let date = mysql.escape(req.body.date);
            let paymentmode = mysql.escape(req.body.paymentmode);
            let remark = mysql.escape(req.body.remark);
            let voucherno = mysql.escape(req.body.voucherno);
            let bankdetail = mysql.escape(req.body.bankdetail);
            let amount = mysql.escape(req.body.amount);
            let extraamount = mysql.escape(req.body.extraamount);
            let investmentaccount = mysql.escape(req.body.investmentaccount);
            let drawingtype = mysql.escape(req.body.drawingtype);
            let idempotencykey = mysql.escape(req.params.businessid + req.body.voucherno);
            let id = v4();
            let result = await pool.query("CALL insertInvestment ( " + mysql.escape(sessionId) + "," + mysql.escape(id) + "," + businessid + "," + fyid + "," + fy + "," + madereceived + "," + activity + "," + date + "," + paymentmode + "," + remark + "," + voucherno + "," + bankdetail + "," + amount + "," + idempotencykey + "," + investmentaccount + "," + extraamount + "," + drawingtype + ")");
            if (result[0][0][0].result) {
                if (result[0][0][0].result === "success") {
                    res.statusCode = 200;
                    res.setHeader("Content-Type", "application/json");
                    res.json({ result: "success", id: { id: result[0][1][0].checkid } });
                }
                else if (result[0][0][0].result === 'Invalid Session Id') {
                    res.clearCookie('userservice');
                    res.statusCode = 401;
                    res.setHeader("Content-Type", "application/json");
                    res.json({ result: 'Invalid Login' });
                }
            }
            else if (result[0][1][0].result) {
                if (result[0][1][0].result === "Already Registered") {
                    res.statusCode = 200;
                    res.setHeader("Content-Type", "application/json");
                    res.json({
                        result: "Already Registered", data: result[0][0][0]
                    });
                }
            }
            else throw new error(result[0][0][0].result);
        }
        catch (err) {
            writer.write(err.message);
            res.statusCode = 404;
            res.setHeader("Content-Type", "application/json");
            res.json({ "result": err.message });
        }
    })

    .get(async (req, res, next) => {
        try {
            let businessid = mysql.escape(req.params.businessid);
            let fyid = mysql.escape(req.params.fyid);
            const sql = "CALL getInvestment ( " + mysql.escape(sessionId) + "," + businessid + "," + fyid + ")";
            let result = await pool.query(sql);
            if (result[0][1][0]) {
                if (result[0][1][0].result === 'success') {
                    res.statusCode = 200;
                    res.setHeader("Content-Type", "application/json");
                    res.json({ result: "success", data: result[0][0] });
                }
            }
            else if (result[0][0][0].result === 'Invalid Session Id') {
                res.clearCookie('userservice');
                res.statusCode = 401;
                res.setHeader("Content-Type", "application/json");
                res.json({ result: 'Invalid Login', data: [] });
            }
            else throw new Error("Database Error");
        } catch (err) {
            writer.write(err.message);
            res.statusCode = 404;
            res.setHeader("Content-Type", "application/json");
            res.json({ "result": err.message, "data": [] });
        }
    })

module.exports = raiseInvestmentRouter;