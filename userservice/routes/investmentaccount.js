const express = require('express');
const bodyParser = require('body-parser');
const mysql = require('mysql2/promise');
const cookieParser = require('cookie-parser');
const pool = require('../reusableFunctionalities/dbconnection');
const getSessionId = require('../reusableFunctionalities/getSessionId');
const v4 = require('node-uuid');
const fs = require('fs');
const investmentaccountRouter = express.Router();
investmentaccountRouter.use(bodyParser.json());
investmentaccountRouter.use(cookieParser());

let sessionId = undefined;
const writer = fs.createWriteStream('./access.log', { flags: 'a' });

investmentaccountRouter.use((req, res, next) => {
    if (req.cookies['userservice'] == undefined) {
        res.statusCode = 401;
        res.setHeader("Content-Type", "application/json");
        res.json({ result: 'Please login in order to continue', data: [] });
    }
    else if (req.cookies['userservice']) {
        sessionId = getSessionId(req.cookies['userservice']);
        if (sessionId == false) {
            res.clearCookie('userservice');
            res.statusCode = 401;
            res.setHeader("Content-Type", "application/json");
            res.json({ result: 'Please Stop Tampering with Data', data: [] });
        }
        else {
            return next();
        }
    }
})

investmentaccountRouter.route('/:businessid')
    .get(async (req, res, next) => {
        try {
            let businessid = mysql.escape(req.params.businessid);
            const sql = "CALL getInvestmentAccount ( " + mysql.escape(sessionId) + "," + businessid + ")";
            let result = await pool.query(sql);
            if (result[0][1][0]) {
                if (result[0][1][0].result === 'success') {
                    res.statusCode = 200;
                    res.setHeader("Content-Type", "application/json");
                    res.json({ result: "success", data: result[0][0] });
                }
            }
            else if (result[0][0][0].result === 'Invalid Session Id') {
                res.clearCookie('userservice');
                res.statusCode = 401;
                res.setHeader("Content-Type", "application/json");
                res.json({ result: 'Invalid Login', data: [] });
            }
            else throw new Error("Database Error");
        } catch (err) {
            writer.write(err.message);
            res.statusCode = 401;
            res.setHeader("Content-Type", "application/json");
            res.json({ result: err.message, data: [] });
        }
    })

    .post(async (req, res, next) => {
        try {
            let businessid = mysql.escape(req.params.businessid);
            let investmentaccount = mysql.escape(req.body.investmentaccount);
            let shareholding = mysql.escape(req.body.shareholding);
            let id = v4();
            let result = await pool.query("CALL insertInvestmentAccount (" + mysql.escape(sessionId) + "," + businessid + "," + investmentaccount + "," + mysql.escape(id) + "," + shareholding + ")");
            if (result[0][0][0].result === "success") {
                res.statusCode = 200;
                res.setHeader("Content-Type", "application/json");
                res.json({ result: "success", id: { id: result[0][1][0].checkid } });
            }
            else if (result[0][0][0].result === 'Invalid Session Id') {
                res.clearCookie('userservice');
                res.statusCode = 401;
                res.setHeader("Content-Type", "application/json");
                res.json({ result: 'Invalid Login' });
            }
            else throw new error(result[0][0][0].result);
        } catch (err) {
            writer.write(err.message);
            res.statusCode = 401;
            res.setHeader("Content-Type", "application/json");
            res.json({ result: err.message });
        }
    })

    .put(async (req, res, next) => {
        try {
            let businessid = mysql.escape(req.params.businessid);
            let investmentaccount = mysql.escape(req.body.investmentaccount);
            let shareholding = mysql.escape(req.body.shareholding);
            let id = mysql.escape(req.body.id);
            let result = await pool.query("CALL updateInvestmentAccount (" + mysql.escape(sessionId) + "," + businessid + "," + investmentaccount + "," + id + "," + shareholding + ")");
            if (result[0][0][0].result === "success") {
                res.statusCode = 200;
                res.setHeader("Content-Type", "application/json");
                res.json({ result: "success" });
            }
            else if (result[0][0][0].result === 'Invalid Session Id') {
                res.clearCookie('userservice');
                res.statusCode = 401;
                res.setHeader("Content-Type", "application/json");
                res.json({ result: 'Invalid Login' });
            }
            else throw new error("Database Error");
        } catch (err) {
            writer.write(err.message);
            res.statusCode = 401;
            res.setHeader("Content-Type", "application/json");
            res.json({ result: err.message });
        }
    })
module.exports = investmentaccountRouter;