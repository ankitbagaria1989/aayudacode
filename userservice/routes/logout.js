const express = require ('express');
const bodyParser = require ('body-parser');
const cookieParser = require('cookie-parser');
const mysql = require('mysql2/promise');
const pool = require ('../reusableFunctionalities/dbconnection');
const getSessionId = require('../reusableFunctionalities/getSessionId');
const fs = require('fs');
const logoutRouter = express.Router();
logoutRouter.use(bodyParser.json());
logoutRouter.use(cookieParser());

const itemRouter = express.Router();
itemRouter.use(bodyParser.json());
itemRouter.use(cookieParser());

let sessionId = undefined;
const writer = fs.createWriteStream('./access.log', { flags: 'a' });
logoutRouter.use((req, res, next) => {
    if (req.cookies['userservice'] == undefined) {
        res.statusCode = 401;
        res.setHeader("Content-Type", "application/json");
        res.json({ result: 'Please login in order to continue', data: [] });
    }
    else if (req.cookies['userservice']) {
        sessionId = getSessionId(req.cookies['userservice']);
        if (sessionId == false) {
            res.clearCookie('userservice');
            res.statusCode = 401;
            res.setHeader("Content-Type", "application/json");
            res.json({ result: 'Please Stop Tampering with Data', data: [] });
        }
        else {
            return next();
        }
    }
})

logoutRouter.route('/')
    .get(async (req,res,next) => {
        try {
            let result = await pool.query("DELETE from tokendetail WHERE tokenid = " + mysql.escape(sessionId));
            if (result[0].affectedRows === 1) {
                res.clearCookie('userservice');
                res.statusCode = 200;
                res.setHeader("Content-Type", "application/json");
                res.json({ result: 'success', data: [] });
            }
            else throw new Error('Error Logging Out. Please Try Again');
        } catch (err) {
            writer.write(err.message);
            res.statusCode = 401;
            res.setHeader("Content-Type", "application/json");
            res.json({ result: err.message });
        }   
    })

module.exports = logoutRouter;