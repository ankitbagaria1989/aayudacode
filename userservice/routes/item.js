const express = require('express');
const bodyParser = require('body-parser');
const mysql = require('mysql2/promise');
const cookieParser = require('cookie-parser');
const pool = require('../reusableFunctionalities/dbconnection');
const getSessionId = require('../reusableFunctionalities/getSessionId');
const v4 = require('node-uuid');
const fs = require('fs');
const itemRouter = express.Router();
itemRouter.use(bodyParser.json());
itemRouter.use(cookieParser());

let sessionId = undefined;
const writer = fs.createWriteStream('./access.log', { flags: 'a' });
itemRouter.use((req, res, next) => {
    if (req.cookies['userservice'] == undefined) {
        res.statusCode = 401;
        res.setHeader("Content-Type", "application/json");
        res.json({ result: 'Please login in order to continue', data: [] });
    }
    else if (req.cookies['userservice']) {
        sessionId = getSessionId(req.cookies['userservice']);
        if (sessionId == false) {
            res.clearCookie('userservice');
            res.statusCode = 401;
            res.setHeader("Content-Type", "application/json");
            res.json({ result: 'Please Stop Tampering with Data', data: [] });
        }
        else {
            return next();
        }
    }
})

itemRouter.route('/:businessid')
    .get(async (req, res, next) => {
        try {
            let businessid = mysql.escape(req.params.businessid);
            const sql = "CALL getItem ( " + mysql.escape(sessionId) + "," + businessid + ")";
            let result = await pool.query(sql);
            if (result[0][1][0]) {
                if (result[0][1][0].result === 'success') {
                    res.statusCode = 200;
                    res.setHeader("Content-Type", "application/json");
                    res.json({ result: "success", data: result[0][0] });
                }
            }
            else if (result[0][0][0].result === 'Invalid Session Id') {
                res.clearCookie('userservice');
                res.statusCode = 401;
                res.setHeader("Content-Type", "application/json");
                res.json({ result: 'Invalid Login', data: [] });
            }
            else throw new Error("Database Error");

        } catch (err) {
            writer.write(err.message);
            res.statusCode = 401;
            res.setHeader("Content-Type", "application/json");
            res.json({ result: err.message, data: [] });
        }

    })

    .post(async (req, res, next) => {
        try {
            let businessid = mysql.escape(req.params.businessid);
            let alias = mysql.escape(req.body.itemdetail.alias);
            let itemname = mysql.escape(req.body.itemdetail.itemname);
            let hsncode = mysql.escape(req.body.itemdetail.hsncode);
            let taxtype = mysql.escape(req.body.itemdetail.taxtype);
            let gstrate = mysql.escape(req.body.itemdetail.gstrate);
            let billingunit = mysql.escape(req.body.itemdetail.billingunit);
            let itemid = v4();
            let result = await pool.query("CALL insertItem (" + mysql.escape(sessionId) + "," + businessid + "," + itemname + "," + alias + "," + hsncode + "," + gstrate + "," + billingunit + "," + mysql.escape(itemid) + "," + taxtype + ")");
            if (result[0][0][0].result === "success") {
                res.statusCode = 200;
                res.setHeader("Content-Type", "application/json");
                res.json({ result: "success", id: { itemid: result[0][1][0].checkid } });
            }
            else if (result[0][0][0].result === 'Invalid Session Id') {
                res.clearCookie('userservice');
                res.statusCode = 401;
                res.setHeader("Content-Type", "application/json");
                res.json({ result: 'Invalid Login' });
            }
            else throw new error(result[0][0][0].result);

        } catch (err) {
            writer.write(err.message);
            res.statusCode = 401;
            res.setHeader("Content-Type", "application/json");
            res.json({ result: err.message });
        }
    })

    .put(async (req, res, next) => {
        try {
            let businessid = mysql.escape(req.params.businessid);
            let itemid = mysql.escape(req.body.itemid);
            let itemname = mysql.escape(req.body.itemdetail.itemname);
            let alias = mysql.escape(req.body.itemdetail.alias);
            let hsncode = mysql.escape(req.body.itemdetail.hsncode);
            let taxtype = mysql.escape(req.body.itemdetail.taxtype);
            let exciserate = mysql.escape(req.body.itemdetail.exciserate);
            let billingunit = mysql.escape(req.body.itemdetail.billingunit);
            let gstrate = mysql.escape(req.body.itemdetail.gstrate);
            let result = await pool.query("CALL updateItem (" + mysql.escape(sessionId) + "," + businessid + "," + itemname + "," + alias + "," + hsncode + "," + gstrate + "," + billingunit + "," + itemid + "," + taxtype + ")");
            if (result[0][0][0].result === "success") {
                res.statusCode = 200;
                res.setHeader("Content-Type", "application/json");
                res.json({ result: "success" });
            }
            else if (result[0][0][0].result === 'Invalid Session Id') {
                res.clearCookie('userservice');
                res.statusCode = 401;
                res.setHeader("Content-Type", "application/json");
                res.json({ result: 'Invalid Login' });
            }
            else throw new error("Database Error");
        } catch (err) {
            writer.write(err.message);
            res.statusCode = 401;
            res.setHeader("Content-Type", "application/json");
            res.json({ result: err.message });
        }
    })

module.exports = itemRouter;