const express = require('express');
const bodyParser = require('body-parser');
const mysql = require('mysql2/promise');
const cookieParser = require('cookie-parser');
const pool = require('../reusableFunctionalities/dbconnection');
const getSessionId = require('../reusableFunctionalities/getSessionId');
const v4 = require('node-uuid');
const fs = require('fs');
const updateSalaryRouter = express.Router();
updateSalaryRouter.use(bodyParser.json());
updateSalaryRouter.use(cookieParser());

let sessionId = undefined;
const writer = fs.createWriteStream('./access.log', { flags: 'a' });
updateSalaryRouter.use((req, res, next) => {
    if (req.cookies['userservice'] == undefined) {
        res.statusCode = 401;
        res.setHeader("Content-Type", "application/json");
        res.json({ result: 'Please login in order to continue', data: [] });
    }
    else if (req.cookies['userservice']) {
        sessionId = getSessionId(req.cookies['userservice']);
        if (sessionId == false) {
            res.clearCookie('userservice');
            res.statusCode = 401;
            res.setHeader("Content-Type", "application/json");
            res.json({ result: 'Please Stop Tampering with Data', data: [] });
        }
        else {
            return next();
        }
    }
});

updateSalaryRouter.route("/:fyid/:month")
    .post(async (req, res, next) => {
        try {
            let fyid = mysql.escape(req.params.fyid);
            let month = mysql.escape(req.params.month);
            let salary = mysql.escape(req.body.state);
            let nextMonthState = mysql.escape(req.body.nextMonthState)
            let nextMonth = mysql.escape(req.body.nextMonth);
            let nextMonthNumber = 0;
            nextMonth === 'none' ? nextMonthNumber = 12 : '';
            let idempotencykey1 = mysql.escape(req.params.fyid+req.params.month);
            let idempotencykey2 = mysql.escape(req.params.fyid+req.params.nextMonth);
            let result = await pool.query("CALL updateSalary (" + mysql.escape(sessionId) + "," + fyid + "," + month + "," + salary + "," + nextMonthState + "," + nextMonth + "," + nextMonthNumber + "," + idempotencykey1 + "," + idempotencykey2 + ")");    
            if (result[0][0][0].result === "success") {
                res.statusCode = 200;
                res.setHeader("Content-Type", "application/json");
                res.json({ result: "success" });
            }
            else if (result[0][0][0].result === 'Invalid Session Id') {
                res.clearCookie('userservice');
                res.statusCode = 401;
                res.setHeader("Content-Type", "application/json");
                res.json({ result: 'Invalid Login' });
            }
            else throw new error("Database Error");
        }
        catch (err) {
            writer.write(err.message);
            res.statusCode = 401;
            res.setHeader("Content-Type", "application/json");
            res.json({ result: err.message });
        }
    })

    .get(async (req, res, next) => {
        try {
            let fyid = mysql.escape(req.params.fyid);
            let month = mysql.escape(req.params.month)
            const sql = "CALL getSalary ( " + mysql.escape(sessionId) + "," + fyid + "," + month + ")";
            let result = await pool.query(sql);
            if (result[0][1][0]) {
                if (result[0][1][0].result === 'success') {
                    res.statusCode = 200;
                    res.setHeader("Content-Type", "application/json");
                    res.json({ result: "success", data: result[0][0] });
                }
            }
            else if (result[0][0][0].result === 'Invalid Session Id') {
                res.clearCookie('userservice');
                res.statusCode = 401;
                res.setHeader("Content-Type", "application/json");
                res.json({ result: 'Invalid Login', data: [] });
            }
            else throw new Error("Database Error");
        } catch (err) {
            writer.write(err.message);
            res.statusCode = 404;
            res.setHeader("Content-Type", "application/json");
            res.json({ "result": err.message, "data": [] });
        }
    })

module.exports = updateSalaryRouter;