const express = require('express');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const mysql = require('mysql2/promise');
const pool = require('../reusableFunctionalities/dbconnection');
const getSessionId = require ('../reusableFunctionalities/getSessionId');
const fs = require('fs');
const subUserRouter = express.Router();
subUserRouter.use(bodyParser.json());
subUserRouter.use(cookieParser());

let sessionid = null;
const writer = fs.createWriteStream('./access.log', { flags: 'a' });
subUserRouter.use((req,res,next) => {
    if (req.cookies['userservice'] === undefined) {
        res.statusCode = 401;
        res.setHeader ("Content-Type","application/json");
        res.json({result : 'Please login in order to continue',data:[]});
    }
    else {
        sessionid = getSessionId(req.cookies['userservice']);
        if (sessionid === false) {
            res.statusCode = 401;
            res.setHeader ("Content-Type","application/json");
            res.json({result : 'Please login in order to continue',data:[]});
        }
        else next();
    }
});

subUserRouter.route('/')
.get (async (req,res,next) => {
    try { 
        let result = await pool.query("SELECT username, useremail FROM users WHERE userid IN (SELECT userid FROM businessusers WHERE usertype = 's' AND businessid = " + businessid + ")");
        if (result[0]) {
            res.statusCode = 200;
            res.setHeader("Content-Type","application/json");
            res.json({result:"success",data:result[0]});
        }
        else throw new Error ("database Error");
    } catch (err) {
        writer.write(err.message);
        res.statusCode = 401;
        res.contentType("Content-Type","application/json");
        res.json({result:err.message, data:[]});
    }
})
.post (async (req,res,next) => {
    let subUserEmail = mysql.escape(req.body.email);
    try {
        let result  = await pool.query("CALL insertSubUser ( " + subUserEmail + "," + mysql.escape(sessionid) + ")");
        if(result[0][0][0].result === "success") {
            res.statusCode = 200;
            res.setHeader("Content-Type","application/json");
            res.json({result : "Success"});
        }
        else throw new Error(result[0][0][0].result);
    }
    catch(err) {
        writer.write(err.message);
        res.statusCode = 401;
        res.setHeader("Content-Type","application/json");
        res.json({result : err.message});
    }
})
.delete (async (req,res,next) => {
    try {
        let email = mysql.escape(req.body.email);
        let sql = "DELETE FROM businessusers WHERE usertype = 's' AND userid IN (SELECT userid FROM users WHERE useremail = " + email + ") AND businessid IN (SELECT bussid  FROM (SELECT b.businessid AS bussid FROM businessusers b  INNER JOIN tokendetail t WHERE b.userid = t.userid AND t.tokenid = " + mysql.escape(sessionid) +") AS buss)";        
        let result = await pool.query(sql);
        if (result[0].affectedRows === 0) {
            res,statusCode = 200;
            res.setHeader ("Content-Type","application/json");
            res.json({result : "Invalid Sub User Email Address"});
        }
        else {
            res.statusCode = 200;
            res.setHeader("Content-Type","application/json");
            res.json({result:"success"});
        }
    } catch (err) {
        writer.write(err.message);
        res.statusCode = 401;
        res.setHeader("Content-Type","application/json");
        res.json({result:err.message});
    }
})

module.exports = subUserRouter;