const express = require('express');
const bodyParser = require('body-parser');
const mysql = require('mysql2/promise');
const pool = require('../reusableFunctionalities/dbconnection');
const cookieParser = require('cookie-parser');
const getSessionId = require('../reusableFunctionalities/getSessionId');
const bcrypt = require('bcryptjs');
const fs = require('fs');

const changePasswordRouter = express.Router();
changePasswordRouter.use(bodyParser.json());
changePasswordRouter.use(cookieParser());

let sessionId = undefined;
const writer = fs.createWriteStream('./access.log', { flags: 'a' });

changePasswordRouter.use((req, res, next) => {
    if (req.cookies['userservice'] == undefined) {
        res.statusCode = 401;
        res.setHeader("Content-Type", "application/json");
        res.json({ result: 'Please login in order to continue', data: [] });
    }
    else if (req.cookies['userservice']) {
        sessionId = getSessionId(req.cookies['userservice']);
        if (sessionId == false) {
            res.clearCookie('userservice');
            res.statusCode = 401;
            res.setHeader("Content-Type", "application/json");
            res.json({ result: 'Please Stop Tampering with Data', data: [] });
        }
        else {
            return next();
        }
    }
});

changePasswordRouter.route('/')
    .put(async (req, res, next) => {
        let oldpassword = mysql.escape(req.body.oldpassword);
        let newpassword = mysql.escape(req.body.newpassword);
        let rounds = 2;
        try {
            let result = await pool.query('SELECT u.hashpassword AS password,u.userid AS userid from users u LEFT JOIN tokendetail t ON t.userid = u.userid WHERE t.tokenid = ' + mysql.escape(sessionId));
            if (result[0][0].password && result[0][0].userid) {
                // release connection handle and redirect to business
                let bcryptResult = await bcrypt.compare(oldpassword, result[0][0].password);
                if (bcryptResult == true) {
                    let salt = await bcrypt.genSalt(rounds);
                    let hash = await bcrypt.hash(newpassword, salt);
                    result = await pool.query('CALL changePassword (' + mysql.escape(sessionId) + "," + mysql.escape(hash) + "," + mysql.escape(result[0][0].userid) + ')');
                    if (result[0][0][0].result === 'success') {
                        res.clearCookie('userservice');
                        res.statusCode = 200;
                        res.setHeader("Content-Type", "application/json");
                        res.json({ result: 'success' });
                    }
                    else if (result[0][0][0].result === 'Invalid Session Id') {
                        res.clearCookie('userservice');
                        res.statusCode = 401;
                        res.setHeader("Content-Type", "application/json");
                        res.json({ result: 'Invalid Login Credentials' });
                    }
                    else throw new Error("Database error");
                }
                else {
                    throw new Error("Invalid Existing Password");
                }
            }
            else {
                throw new Error("Invalid Existing Password");
            }
        } catch (err) {
            let errorPattern = /^Duplicate Entry/i;
            let result = err.message.match(errorPattern);
            if (result != null) err.message = "Email Address Already Registered";
            writer.write(err.message);
            res.statusCode = 401;
            res.setHeader("Content-Type", "application/json");
            res.json({ result: err.message });
        }
    })

module.exports = changePasswordRouter;