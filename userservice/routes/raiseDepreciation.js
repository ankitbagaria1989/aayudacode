const express = require('express');
const bodyParser = require('body-parser');
const mysql = require('mysql2/promise');
const cookieParser = require('cookie-parser');
const pool = require('../reusableFunctionalities/dbconnection');
const getSessionId = require('../reusableFunctionalities/getSessionId');
const fs = require('fs');
const raiseDepreciationRouter = express.Router();
raiseDepreciationRouter.use(bodyParser.json());
raiseDepreciationRouter.use(cookieParser());

let sessionId = undefined;
const writer = fs.createWriteStream('./access.log', { flags: 'a' });
raiseDepreciationRouter.use((req, res, next) => {
    if (req.cookies['userservice'] == undefined) {
        res.statusCode = 401;
        res.setHeader("Content-Type", "application/json");
        res.json({ result: 'Please login in order to continue', data: [] });
    }
    else if (req.cookies['userservice']) {
        sessionId = getSessionId(req.cookies['userservice']);
        if (sessionId == false) {
            res.clearCookie('userservice');
            res.statusCode = 401;
            res.setHeader("Content-Type", "application/json");
            res.json({ result: 'Please Stop Tampering with Data', data: [] });
        }
        else {
            return next();
        }
    }
})

raiseDepreciationRouter.route("/:businessid/:fyid")
    .post(async (req, res, next) => {
        try {
            let fyid = mysql.escape(req.params.fyid);
            let asset = mysql.escape(req.body.asset);
            let amount = mysql.escape(req.body.amount);
            let remark = mysql.escape(req.body.remark);
            let depreciateappreciate = mysql.escape(req.body.depreciateappreciate);
            let result = await pool.query("CALL updateDepreciation ( " + mysql.escape(sessionId) + "," + fyid + "," + asset + "," + remark + "," + amount + "," + depreciateappreciate + ")");
            if (result[0][0][0].result === "success") {
                res.statusCode = 200;
                res.setHeader("Content-Type", "application/json");
                res.json({ result: "success" });
            }
            else if (result[0][0][0].result === 'Invalid Session Id') {
                res.clearCookie('userservice');
                res.statusCode = 401;
                res.setHeader("Content-Type", "application/json");
                res.json({ result: 'Invalid Login' });
            }
            else throw new error(result[0][0][0].result);
        }
        catch (err) {
            writer.write(err.message);
            res.statusCode = 404;
            res.setHeader("Content-Type", "application/json");
            res.json({ "result": err.message });
        }
    })

    .get(async (req, res, next) => {
        try {
            let businessid = mysql.escape(req.params.businessid);
            let fyid = mysql.escape(req.params.fyid);
            const sql = "CALL getDepreciation ( " + mysql.escape(sessionId) + "," + fyid + ")";
            let result = await pool.query(sql);
            if (result[0][1][0]) {
                if (result[0][1][0].result === 'success') {
                    res.statusCode = 200;
                    res.setHeader("Content-Type", "application/json");
                    res.json({ result: "success", data: result[0][0] });
                }
            }
            else if (result[0][0][0].result === 'Invalid Session Id') {
                res.clearCookie('userservice');
                res.statusCode = 401;
                res.setHeader("Content-Type", "application/json");
                res.json({ result: 'Invalid Login', data: [] });
            }
            else throw new Error("Database Error");
        } catch (err) {
            writer.write(err.message);
            res.statusCode = 404;
            res.setHeader("Content-Type", "application/json");
            res.json({ "result": err.message, "data": [] });
        }
    })

module.exports = raiseDepreciationRouter;