const express = require('express');
const bodyParser = require('body-parser');
const mysql = require('mysql2/promise');
const cookieParser = require('cookie-parser');
const pool = require('../reusableFunctionalities/dbconnection');
const getSessionId = require('../reusableFunctionalities/getSessionId');
const fs = require('fs');
const v4 = require('node-uuid');

const assetaccountRouter = express.Router();
assetaccountRouter.use(bodyParser.json());
assetaccountRouter.use(cookieParser());

let sessionId = undefined;
const writer = fs.createWriteStream('./access.log', { flags: 'a' });

assetaccountRouter.use((req, res, next) => {
    if (req.cookies['userservice'] == undefined) {
        res.statusCode = 401;
        res.setHeader("Content-Type", "application/json");
        res.json({ result: 'Please login in order to continue', data: [] });
    }
    else if (req.cookies['userservice']) {
        sessionId = getSessionId(req.cookies['userservice']);
        if (sessionId == false) {
            res.clearCookie('userservice');
            res.statusCode = 401;
            res.setHeader("Content-Type", "application/json");
            res.json({ result: 'Please Stop Tampering with Data', data: [] });
        }
        else {
            return next();
        }
    }
})

assetaccountRouter.route('/:businessid')
    .get(async (req, res, next) => {
        try {
            let businessid = mysql.escape(req.params.businessid);
            const sql = "CALL getAssetAccount ( " + mysql.escape(sessionId) + "," + businessid + ")";
            let result = await pool.query(sql);
            if (result[0][1][0]) {
                if (result[0][1][0].result === 'success') {
                    res.statusCode = 200;
                    res.setHeader("Content-Type", "application/json");
                    res.json({ result: "success", data: result[0][0] });
                }
            }
            else if (result[0][0][0].result === 'Invalid Session Id') {
                res.clearCookie('userservice');
                res.statusCode = 401;
                res.setHeader("Content-Type", "application/json");
                res.json({ result: 'Invalid Login', data: [] });
            }
            else throw new Error("Database Error");
        } catch (err) {
            writer.write(err.message);
            res.statusCode = 401;
            res.setHeader("Content-Type", "application/json");
            res.json({ result: err.message, data: [] });
        }
    })

    .post(async (req, res, next) => {
        try {
            let businessid = mysql.escape(req.params.businessid);
            let assetaccount = mysql.escape(req.body.assetaccount);
            let depreciating = mysql.escape(req.body.depreciating);
            let purchasedbefore = mysql.escape(req.body.purchasedbefore);
            let date = mysql.escape(req.body.date);
            let amount = mysql.escape(req.body.amount);
            let id = v4();
            let result = await pool.query("CALL insertAssetAccount (" + mysql.escape(sessionId) + "," + businessid + "," + assetaccount + "," + mysql.escape(id) + "," + depreciating + "," + purchasedbefore + "," + date + "," + amount + ")");
            if (result[0][0][0].result === "success") {
                res.statusCode = 200;
                res.setHeader("Content-Type", "application/json");
                res.json({ result: "success", id: { id: result[0][1][0].checkid } });
            }
            else if (result[0][0][0].result === 'Invalid Session Id') {
                res.clearCookie('userservice');
                res.statusCode = 401;
                res.setHeader("Content-Type", "application/json");
                res.json({ result: 'Invalid Login' });
            }
            else throw new error(result[0][0][0].result);
        } catch (err) {
            writer.write(err.message);
            res.statusCode = 401;
            res.setHeader("Content-Type", "application/json");
            res.json({ result: err.message });
        }
    })

    .put(async (req, res, next) => {
        if (req.body.desc === 'assetPurchase') {
            try {
                let businessid = mysql.escape(req.params.businessid);
                let date = mysql.escape(req.body.date);
                let voucherno = mysql.escape(req.body.voucherno);
                let totalbasic = mysql.escape(req.body.totalbasic);
                let totaligst = mysql.escape(req.body.totaligst);
                let totalcgst = mysql.escape(req.body.totalcgst);
                let totalsgst = mysql.escape(req.body.totalsgst);
                let grandtotal = mysql.escape(req.body.grandtotal);
                let assetid = mysql.escape(req.body.assetid);
                let purchasefyid = mysql.escape(req.body.purchasefyid);
                let vendorid = mysql.escape(req.body.vendorid);
                let result = await pool.query("CALL updateAssetAccountPurchase (" + mysql.escape(sessionId) + "," + businessid + "," + date + "," + voucherno + "," + totalbasic + "," + totaligst + "," + totalcgst + "," + totalsgst + "," + grandtotal + "," + assetid + "," + purchasefyid + "," + vendorid + ")");
                if (result[0][0][0].result === "success") {
                    res.statusCode = 200;
                    res.setHeader("Content-Type", "application/json");
                    res.json({ result: "success", id: { id: result[0][1][0].checkid } });
                }
                else if (result[0][0][0].result === 'Invalid Session Id') {
                    res.clearCookie('userservice');
                    res.statusCode = 401; 
                    res.setHeader("Content-Type", "application/json");
                    res.json({ result: 'Invalid Login' });
                }
                else throw new error(result[0][0][0].result);
            } catch (err) {
                writer.write(err.message);
                res.statusCode = 401;
                res.setHeader("Content-Type", "application/json");
                res.json({ result: err.message });
            }
        }
        else if (req.body.desc === 'assetSell') {
            try {
                let businessid = mysql.escape(req.params.businessid);
                let date = mysql.escape(req.body.date);
                let voucherno = mysql.escape(req.body.voucherno);
                let totalbasic = mysql.escape(req.body.totalbasic);
                let totaligst = mysql.escape(req.body.totaligst);
                let totalcgst = mysql.escape(req.body.totalcgst);
                let totalsgst = mysql.escape(req.body.totalsgst);
                let grandtotal = mysql.escape(req.body.grandtotal);
                let assetid = mysql.escape(req.body.assetid);
                let soldfyid = mysql.escape(req.body.soldfyid);
                let customerid = mysql.escape(req.body.customerid);
                let id = v4();
                let idempotencykey = mysql.escape (req.body.soldfyid+req.body.voucherno);
                let result = await pool.query("CALL updateAssetAccountSell (" + mysql.escape(sessionId) + "," + businessid + "," + date + "," + voucherno + "," + totalbasic + "," + totaligst + "," + totalcgst + "," + totalsgst + "," + grandtotal + "," + assetid + "," + soldfyid + "," + customerid + "," + mysql.escape(id) + "," + idempotencykey + ")");
                if (result[0][0][0].result === "success") {
                    res.statusCode = 200;
                    res.setHeader("Content-Type", "application/json");
                    res.json({ result: "success", id: { id: result[0][1][0].checkid } });
                }
                else if (result[0][0][0].result === 'Invalid Session Id') {
                    res.clearCookie('userservice');
                    res.statusCode = 401; 
                    res.setHeader("Content-Type", "application/json");
                    res.json({ result: 'Invalid Login' });
                }
                else throw new error(result[0][0][0].result);
            } catch (err) {
                writer.write(err.message);
                res.statusCode = 401;
                res.setHeader("Content-Type", "application/json");
                res.json({ result: err.message });
            }
        }
    })

module.exports = assetaccountRouter;