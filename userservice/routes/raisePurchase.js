const express = require('express');
const bodyParser = require('body-parser');
const mysql = require('mysql2/promise');
const cookieParser = require('cookie-parser');
const pool = require('../reusableFunctionalities/dbconnection');
const getSessionId = require('../reusableFunctionalities/getSessionId');
const v4 = require('node-uuid');
const fs = require('fs');
const raisePurchaseRouter = express.Router();
raisePurchaseRouter.use(bodyParser.json());
raisePurchaseRouter.use(cookieParser());

let sessionId = undefined;
const writer = fs.createWriteStream('./access.log', { flags: 'a' });
raisePurchaseRouter.use((req, res, next) => {
    if (req.cookies['userservice'] == undefined) {
        res.statusCode = 401;
        res.setHeader("Content-Type", "application/json");
        res.json({ result: 'Please login in order to continue', data: [] });
    }
    else if (req.cookies['userservice']) {
        sessionId = getSessionId(req.cookies['userservice']);
        if (sessionId == false) {
            res.clearCookie('userservice');
            res.statusCode = 401;
            res.setHeader("Content-Type", "application/json");
            res.json({ result: 'Please Stop Tampering with Data', data: [] });
        }
        else {
            return next();
        }
    }
})

raisePurchaseRouter.route("/:businessid/:fyid")
    .post(async (req, res, next) => {
        try {
            let businessid = mysql.escape(req.params.businessid);
            let fyid = mysql.escape(req.params.fyid);
            let vendorid = mysql.escape(req.body.vendorid);
            let invoicedate = mysql.escape(req.body.invoicedate);
            let invoicenumber = mysql.escape(req.body.invoicenumber);
            let freightcharges = mysql.escape(req.body.freight);
            let item = mysql.escape(req.body.itemobject);
            let itemno = mysql.escape(req.body.itemno);
            let totalbasic = mysql.escape(req.body.totalbasic);
            let totaligst = mysql.escape(req.body.totaligst);
            let totalcgst = mysql.escape(req.body.totalcgst);
            let totalsgst = mysql.escape(req.body.totalsgst);
            let importduty = mysql.escape(req.body.importduty);
            let grandtotal = mysql.escape(req.body.grandtotal);
            let invoicetype = mysql.escape(req.body.invoicetype);
            let asset = mysql.escape(req.body.asset);
            let purchaseid = v4();
            let idempotencykey = mysql.escape(req.params.fyid + req.body.vendorid + req.body.invoicenumber);
            let result = await pool.query("CALL insertPurchase ( " + mysql.escape(sessionId) + "," + mysql.escape(purchaseid) + "," + businessid + "," + vendorid + "," + invoicenumber + "," + invoicedate + "," + freightcharges + "," + fyid + "," + itemno + "," + item + "," + totalbasic + "," + totaligst + "," + totalcgst + "," + totalsgst + "," + importduty + "," + grandtotal + "," + invoicetype + "," + idempotencykey + "," + asset + ")");
            if (result[0][0][0].result) {
                if (result[0][0][0].result === "success") {
                    res.statusCode = 200;
                    res.setHeader("Content-Type", "application/json");
                    res.json({ result: "success", id: { purchaseid: result[0][1][0].checkid } });
                }
                else if (result[0][0][0].result === 'Invalid Session Id') {
                    res.clearCookie('userservice');
                    res.statusCode = 401;
                    res.setHeader("Content-Type", "application/json");
                    res.json({ result: 'Invalid Login' });
                }
            }
            else if (result[0][1][0].result) {
                if (result[0][1][0].result === "Already Registered") {
                    res.statusCode = 200;
                    res.setHeader("Content-Type", "application/json");
                    res.json({
                        result: "Already Registered", data: result[0][0][0]
                    });
                }
            }
            else throw new error(result[0][0][0].result);
        }
        catch (err) {
            writer.write(err.message);
            res.statusCode = 404;
            res.setHeader("Content-Type", "application/json");
            res.json({ "result": err.message });
        }
    })

    .get(async (req, res, next) => {
        try {
            let businessid = mysql.escape(req.params.businessid);
            let fyid = mysql.escape(req.params.fyid);
            const sql = "CALL getPurchase ( " + mysql.escape(sessionId) + "," + businessid + "," + fyid + ")";
            let result = await pool.query(sql);
            if (result[0][1][0]) {
                if (result[0][1][0].result === 'success') {
                    res.statusCode = 200;
                    res.setHeader("Content-Type", "application/json");
                    res.json({ result: "success", data: result[0][0] });
                }
            }
            else if (result[0][0][0].result === 'Invalid Session Id') {
                res.clearCookie('userservice');
                res.statusCode = 401;
                res.setHeader("Content-Type", "application/json");
                res.json({ result: 'Invalid Login', data: [] });
            }
            else throw new Error("Database Error");
        } catch (err) {
            writer.write(err.message);
            res.statusCode = 404;
            res.setHeader("Content-Type", "application/json");
            res.json({ "result": err.message, "data": [] });
        }
    })

    .put(async (req, res, next) => {
        try {
            let businessid = mysql.escape(req.params.businessid);
            let purchaseid = mysql.escape(req.body.purchaseid);
            let fyid = mysql.escape(req.params.fyid);
            let vendorid = mysql.escape(req.body.vendorid);
            let invoicedate = mysql.escape(req.body.invoicedate);
            let invoicenumber = mysql.escape(req.body.invoicenumber);
            let freightcharges = mysql.escape(req.body.freight);
            let item = mysql.escape(req.body.itemobject);
            let itemno = mysql.escape(req.body.itemno);
            let totalbasic = mysql.escape(req.body.totalbasic);
            let totaligst = mysql.escape(req.body.totaligst);
            let totalcgst = mysql.escape(req.body.totalcgst);
            let totalsgst = mysql.escape(req.body.totalsgst);
            let grandtotal = mysql.escape(req.body.grandtotal);
            let invoicetype = mysql.escape(req.body.invoicetype);
            let importduty = mysql.escape(req.body.importduty);
            let asset = mysql.escape(req.body.asset);
            let result = await pool.query("CALL updatePurchase ( " + mysql.escape(sessionId) + "," + purchaseid + "," + businessid + "," + vendorid + "," + invoicenumber + "," + invoicedate + "," + freightcharges + "," + fyid + "," + itemno + "," + item + "," + totalbasic + "," + totaligst + "," + totalcgst + "," + totalsgst + "," + importduty + "," + grandtotal + "," + invoicetype + "," + asset + ")");
            if (result[0][0][0].result === "success") {
                res.statusCode = 200;
                res.setHeader("Content-Type", "application/json");
                res.json({ result: "success" });
            }
            else if (result[0][0][0].result === 'Invalid Session Id') {
                res.clearCookie('userservice');
                res.statusCode = 401;
                res.setHeader("Content-Type", "application/json");
                res.json({ result: 'Invalid Login' });
            }
            else throw new error("Database Error");
        } catch (err) {
            writer.write(err.message);
            res.statusCode = 401;
            res.setHeader("Content-Type", "application/json");
            res.json({ result: err.message });
        }
    })

module.exports = raisePurchaseRouter;