const express = require('express');
const bodyParser = require('body-parser');
const mysql = require('mysql2/promise');
const cookieParser = require('cookie-parser');
const pool = require('../reusableFunctionalities/dbconnection');
const getSessionId = require('../reusableFunctionalities/getSessionId');
const v4 = require('node-uuid');
const fs = require('fs');
const updateAttendanceRouter = express.Router();
updateAttendanceRouter.use(bodyParser.json());
updateAttendanceRouter.use(cookieParser());

let sessionId = undefined;
const writer = fs.createWriteStream('./access.log', { flags: 'a' });
updateAttendanceRouter.use((req, res, next) => {
    if (req.cookies['userservice'] == undefined) {
        res.statusCode = 401;
        res.setHeader("Content-Type", "application/json");
        res.json({ result: 'Please login in order to continue', data: [] });
    }
    else if (req.cookies['userservice']) {
        sessionId = getSessionId(req.cookies['userservice']);
        if (sessionId == false) {
            res.clearCookie('userservice');
            res.statusCode = 401;
            res.setHeader("Content-Type", "application/json");
            res.json({ result: 'Please Stop Tampering with Data', data: [] });
        }
        else {
            return next();
        }
    }
});

updateAttendanceRouter.route("/:fyid/:date")
    .put(async (req, res, next) => {
        try {
            let fyid = mysql.escape(req.params.fyid);
            let date = mysql.escape(req.params.date);
            let attendanceobject = mysql.escape(req.body.attendanceobject);
            let idempotencykey = mysql.escape(req.params.fyid+req.params.date);
            let monthyear = mysql.escape(req.params.date.split(" ")[0] + " " + req.params.date.split(" ")[2]);
            let result = await pool.query("CALL updateAttendance (" + mysql.escape(sessionId) + "," + fyid + "," + date + "," + monthyear + "," + attendanceobject + "," + idempotencykey + ")");    
            if (result[0][0][0].result === "success") {
                res.statusCode = 200;
                res.setHeader("Content-Type", "application/json");
                res.json({ result: "success" });
            }
            else if (result[0][0][0].result === 'Invalid Session Id') {
                res.clearCookie('userservice');
                res.statusCode = 401;
                res.setHeader("Content-Type", "application/json");
                res.json({ result: 'Invalid Login' });
            }
            else throw new error("Database Error");
        }
        catch (err) {
            writer.write(err.message);
            res.statusCode = 401;
            res.setHeader("Content-Type", "application/json");
            res.json({ result: err.message });
        }
    })

    .get(async (req, res, next) => {
        try {
            let idempotencykey = mysql.escape(req.params.fyid+req.params.date);
            const sql = "CALL getAttendance ( " + mysql.escape(sessionId) + "," + idempotencykey + ")";
            let result = await pool.query(sql);
            if (result[0][1][0]) {
                if (result[0][1][0].result === 'success') {
                    res.statusCode = 200;
                    res.setHeader("Content-Type", "application/json");
                    res.json({ result: "success", data: result[0][0] });
                }
            }
            else if (result[0][0][0].result === 'Invalid Session Id') {
                res.clearCookie('userservice');
                res.statusCode = 401;
                res.setHeader("Content-Type", "application/json");
                res.json({ result: 'Invalid Login', data: [] });
            }
            else throw new Error("Database Error");
        } catch (err) {
            writer.write(err.message);
            res.statusCode = 404;
            res.setHeader("Content-Type", "application/json");
            res.json({ "result": err.message, "data": [] });
        }
    })

module.exports = updateAttendanceRouter;