const express = require('express');
const bodyParser = require('body-parser');
const mysql = require('mysql2/promise');
const cookieParser = require('cookie-parser');
const pool = require('../reusableFunctionalities/dbconnection');
const getSessionId = require('../reusableFunctionalities/getSessionId');
const v4 = require('node-uuid');
const fs = require('fs');
const raiseLoanRouter = express.Router();
raiseLoanRouter.use(bodyParser.json());
raiseLoanRouter.use(cookieParser());

let sessionId = undefined;
const writer = fs.createWriteStream('./access.log', { flags: 'a' });
raiseLoanRouter.use((req, res, next) => {
    if (req.cookies['userservice'] == undefined) {
        res.statusCode = 401;
        res.setHeader("Content-Type", "application/json");
        res.json({ result: 'Please login in order to continue', data: [] });
    }
    else if (req.cookies['userservice']) {
        sessionId = getSessionId(req.cookies['userservice']);
        if (sessionId == false) {
            res.clearCookie('userservice');
            res.statusCode = 401;
            res.setHeader("Content-Type", "application/json");
            res.json({ result: 'Please Stop Tampering with Data', data: [] });
        }
        else {
            return next();
        }
    }
})

raiseLoanRouter.route("/:businessid/:fyid")
    .post(async (req, res, next) => {
        try {
            let businessid = mysql.escape(req.params.businessid);
            let fyid = mysql.escape(req.params.fyid);
            let fy = mysql.escape(req.body.fy);
            let loanaccount = mysql.escape(req.body.loanaccount);
            let loanrepayment = mysql.escape(req.body.loanrepayment);
            let loantype = mysql.escape(req.body.loantype);
            let date = mysql.escape(req.body.date);
            let paymentmode = mysql.escape(req.body.paymentmode);
            let remark = mysql.escape(req.body.remark);
            let voucherno = mysql.escape(req.body.voucherno);
            let bankdetail = mysql.escape(req.body.bankdetail);
            let amount = mysql.escape(req.body.amount);
            let idempotencykey = mysql.escape(req.params.businessid + req.body.voucherno);
            let id = v4();
            let result = await pool.query("CALL insertLoan ( " + mysql.escape(sessionId) + "," + mysql.escape(id) + "," + businessid + "," + fyid + "," + fy + "," + loanaccount + "," + loanrepayment + "," + loantype + "," + date + "," + paymentmode + "," + remark + "," + voucherno + "," + bankdetail + "," + amount + "," + idempotencykey + ")");
            if (result[0][0][0].result) {
                if (result[0][0][0].result === "success") {
                    res.statusCode = 200;
                    res.setHeader("Content-Type", "application/json");
                    res.json({ result: "success", id: { id: result[0][1][0].checkid } });
                }
                else if (result[0][0][0].result === 'Invalid Session Id') {
                    res.clearCookie('userservice');
                    res.statusCode = 401;
                    res.setHeader("Content-Type", "application/json");
                    res.json({ result: 'Invalid Login' });
                }
            }
            else if (result[0][1][0].result) {
                if (result[0][1][0].result === "Already Registered") {
                    res.statusCode = 200;
                    res.setHeader("Content-Type", "application/json");
                    res.json({
                        result: "Already Registered", data: result[0][0][0]
                    });
                }
            }
            else throw new error(result[0][0][0].result);
        }
        catch (err) {
            writer.write(err.message);
            res.statusCode = 404;
            res.setHeader("Content-Type", "application/json");
            res.json({ "result": err.message });
        }
    })

    .get(async (req, res, next) => {
        try {
            let businessid = mysql.escape(req.params.businessid);
            let fyid = mysql.escape(req.params.fyid);
            const sql = "CALL getLoan ( " + mysql.escape(sessionId) + "," + businessid + "," + fyid + ")";
            let result = await pool.query(sql);
            if (result[0][1][0]) {
                if (result[0][1][0].result === 'success') {
                    res.statusCode = 200;
                    res.setHeader("Content-Type", "application/json");
                    res.json({ result: "success", data: result[0][0] });
                }
            }
            else if (result[0][0][0].result === 'Invalid Session Id') {
                res.clearCookie('userservice');
                res.statusCode = 401;
                res.setHeader("Content-Type", "application/json");
                res.json({ result: 'Invalid Login', data: [] });
            }
            else throw new Error("Database Error");
        } catch (err) {
            writer.write(err.message);
            res.statusCode = 404;
            res.setHeader("Content-Type", "application/json");
            res.json({ "result": err.message, "data": [] });
        }
    })

module.exports = raiseLoanRouter;