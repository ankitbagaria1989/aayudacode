const express = require('express');
const bodyParser = require('body-parser');
const mysql = require('mysql2/promise');
const cookieParser = require('cookie-parser');
const pool = require('../reusableFunctionalities/dbconnection');
const getSessionId = require('../reusableFunctionalities/getSessionId');
const v4 = require('node-uuid');
const fs = require('fs');
const raisePaymentRouter = express.Router();
raisePaymentRouter.use(bodyParser.json());
raisePaymentRouter.use(cookieParser());

let sessionId = undefined;
const writer = fs.createWriteStream('./access.log', { flags: 'a' });
raisePaymentRouter.use((req, res, next) => {
    if (req.cookies['userservice'] == undefined) {
        res.statusCode = 401;
        res.setHeader("Content-Type", "application/json");
        res.json({ result: 'Please login in order to continue', data: [] });
    }
    else if (req.cookies['userservice']) {
        sessionId = getSessionId(req.cookies['userservice']);
        if (sessionId == false) {
            res.clearCookie('userservice');
            res.statusCode = 401;
            res.setHeader("Content-Type", "application/json");
            res.json({ result: 'Please Stop Tampering with Data', data: [] });
        }
        else {
            return next();
        }
    }
})

raisePaymentRouter.route("/:businessid/:fyid")
    .post(async (req, res, next) => {
        try {
            let businessid = mysql.escape(req.params.businessid);
            let fyid = mysql.escape(req.params.fyid);
            let paymenttype = mysql.escape(req.body.paymenttype);
            let vendorid = mysql.escape(req.body.vendorid ? req.body.vendorid.split("...")[0] : '');
            let customerid = mysql.escape(req.body.customerid ? req.body.customerid.split("...")[0] : '');
            let date = mysql.escape(req.body.date);
            let amount = mysql.escape(req.body.amount);
            let remark = mysql.escape(req.body.remark);
            let expenseorpurchase = mysql.escape(req.body.expenseorpurchase);
            let paymentmode = mysql.escape(req.body.paymentmode);
            let voucherno = mysql.escape(req.body.voucherno);
            let idempotencykey = mysql.escape(req.params.fyid + req.body.voucherno);
            let businessname = mysql.escape(req.body.vendorid ? req.body.vendorid.split("...")[1] : req.body.customerid.split("...")[1])
            let transactionno = mysql.escape(req.body.transactionno);
            let bankdetail = mysql.escape(req.body.bankdetail);
            let paymentid = v4();
            let expensepurchase = mysql.escape(req.body.expensepurchase);
            let expenseheadid = mysql.escape(req.body.expenseheadid ? req.body.expenseheadid.split("...")[0] : '');
            let expenseheadname = mysql.escape(req.body.expenseheadid ? req.body.expenseheadid.split("...")[1] : '');
            let salesincomeos = mysql.escape(req.body.salesincomeos);
            let specialpayment = mysql.escape(req.body.specialpayment);
            let previouscurrent = mysql.escape(req.body.previouscurrent);
            if (!businessname) vendorid ? businessname = vendorid : businessname = customerid;
            let result = await pool.query("CALL insertPayment ( " + mysql.escape(sessionId) + "," + mysql.escape(paymentid) + "," + businessid + "," + fyid + "," + date + "," + paymenttype + "," + vendorid + "," + customerid + "," + expenseorpurchase + "," + voucherno + "," + amount + "," + paymentmode + "," + remark + "," + idempotencykey + "," + businessname + "," + transactionno + "," + bankdetail + "," + expensepurchase + "," + expenseheadid + "," + expenseheadname + "," + salesincomeos + "," + specialpayment + "," + previouscurrent + ")");
            if (result[0][0][0].result) {
                if (result[0][0][0].result === "success") {
                    res.statusCode = 200;
                    res.setHeader("Content-Type", "application/json");
                    res.json({ result: "success", id: { purchaseid: result[0][1][0].checkid } });
                }
                else if (result[0][0][0].result === 'Invalid Session Id') {
                    res.clearCookie('userservice');
                    res.statusCode = 401;
                    res.setHeader("Content-Type", "application/json");
                    res.json({ result: 'Invalid Login' });
                }
            }
            else if (result[0][1][0].result) {
                if (result[0][1][0].result === "Already Registered") {
                    res.statusCode = 200;
                    res.setHeader("Content-Type", "application/json");
                    res.json({
                        result: "Already Registered", data: result[0][0][0]
                    });
                }
            }
            else throw new error(result[0][0][0].result);
        }
        catch (err) {
            writer.write(err.message);
            res.statusCode = 404;
            res.setHeader("Content-Type", "application/json");
            res.json({ "result": err.message });
        }
    })

    .get(async (req, res, next) => {
        try {
            let businessid = mysql.escape(req.params.businessid);
            let fyid = mysql.escape(req.params.fyid);
            const sql = "CALL getPayment ( " + mysql.escape(sessionId) + "," + businessid + "," + fyid + ")";
            let result = await pool.query(sql);
            if (result[0][1][0]) {
                if (result[0][1][0].result === 'success') {
                    res.statusCode = 200;
                    res.setHeader("Content-Type", "application/json");
                    res.json({ result: "success", data: result[0][0] });
                }
            }
            else if (result[0][0][0].result === 'Invalid Session Id') {
                res.clearCookie('userservice');
                res.statusCode = 401;
                res.setHeader("Content-Type", "application/json");
                res.json({ result: 'Invalid Login', data: [] });
            }
            else throw new Error("Database Error");
        } catch (err) {
            writer.write(err.message);
            res.statusCode = 404;
            res.setHeader("Content-Type", "application/json");
            res.json({ "result": err.message, "data": [] });
        }
    })

module.exports = raisePaymentRouter;