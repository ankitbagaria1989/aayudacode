const express = require('express');
const bodyParser = require('body-parser');
const mysql = require('mysql2/promise');
const cookieParser = require('cookie-parser');
const pool = require('../reusableFunctionalities/dbconnection');
const getSessionId = require('../reusableFunctionalities/getSessionId');
const v4 = require('node-uuid');
const fs = require('fs');

const fyRouter = express.Router();
fyRouter.use(bodyParser.json());
fyRouter.use(cookieParser());

let sessionId = undefined;
const writer = fs.createWriteStream('./access.log', { flags: 'a' });
fyRouter.use((req, res, next) => {
    if (req.cookies['userservice'] === undefined) {
        res.send('Please login in order to continue');
    }
    else if (req.cookies['userservice']) {
        sessionId = getSessionId(req.cookies['userservice']);
        if (sessionId === false) {
            req.session = null;
            res.send("Please stop manupalating data");
        }
        else return next();
    }
});

fyRouter.route('/')
    .get(async (req, res, next) => {
        try {
            let sql = "SELECT fyid, financialyear FROM financialyear WHERE businessid IN (SELECT businessid FROM businessusers WHERE userid IN (SELECT userid FROM tokendetail WHERE tokenid = " + mysql.escape(sessionId) + "))";
            let result = await pool.query(sql);
            if (result[0]) {
                res.statusCode = 200;
                res.setHeader("Content-type", "application/json");
                res.json({ result: "success", data: result[0] });
            }
        } catch (err) {
            writer.write(err.message);
            res.statusCode = 401;
            res.setHeader("Content-type", "application/json");
            res.json({ result: err.message, data: [] });
        }
    })

    .post(async (req, res, next) => {
        try {
            let businessid = mysql.escape(req.body.businessid);
            let financialyear = mysql.escape(req.body.financialyear);
            let obj = mysql.escape(req.body.obj)
            let key = v4();
            let idempotencykey1 = mysql.escape(businessid + financialyear);
            let sql = "CALL insertfinancialyear ( " + businessid + "," + financialyear + "," + mysql.escape(sessionId) + "," + mysql.escape(key) + "," + obj + "," + idempotencykey1 + ")";
            let result = await pool.query(sql);
            if (result[0][0][0].result) {
                if (result[0][0][0].result === "success") {
                    res.statusCode = 200;
                    res.setHeader("Content-Type", "application/json");
                    res.json({ result: "success", id: { fyid: result[0][1][0].checkid } });
                }
                else if (result[0][0][0].result === 'Invalid Session Id') {
                    res.clearCookie('userservice');
                    res.statusCode = 401;
                    res.setHeader("Content-Type", "application/json");
                    res.json({ result: 'Invalid Login' });
                }
            }
            else if (result[0][1][0].result) {
                if (result[0][1][0].result === "Already Registered") {
                    res.statusCode = 200;
                    res.setHeader("Content-Type", "application/json");
                    res.json({ result: "Already Registered", data: result[0][0][0] });
                }
            }
            else throw new ERROR("dataBase Error");
        } catch (err) {
            writer.write(err.message);
            res.statusCode = 401;
            res.setHeader("Content-Type", "application/json");
            res.json({ result: err.message });
        }
    })

module.exports = fyRouter;