const express = require ('express');
const bodyParser = require ('body-parser');
const mysql = require('mysql2/promise');
const pool = require ('../reusableFunctionalities/dbconnection');
const cookieParser = require('cookie-parser');
const bcrypt = require('bcryptjs');
const v4 = require('node-uuid');

const registerUserRouter = express.Router();
registerUserRouter.use(bodyParser.json());
registerUserRouter.use(cookieParser());

registerUserRouter.use((req,res,next) => {
    if(req.cookies['userservice'] == undefined) {
        return next();
    }
    else if(req.cookies['userservice']) {
        res.statusCode = 401;
        res.setHeader('Content-Type','application/json');
        res.json({result : "Already LoggedIn !!"});
    }
});

registerUserRouter.route('/')
.post(async (req,res,next) => {
    const username = mysql.escape(req.body.username);
    const email = mysql.escape(req.body.email);
    const password = mysql.escape(req.body.password);
    const rounds = 2;
    const key = mysql.escape(v4());
    try {
        console.log (email + " " + password);
        let salt = await bcrypt.genSalt(rounds);
        let hash = await bcrypt.hash(password,salt);
        let sql = "INSERT INTO users (userid,username,useremail,hashpassword) VALUES (" + key + "," + username + "," + email + "," + mysql.escape(hash) + ")";
        let result = await pool.query(sql);  
        console.log(result[0]);
        if(result[0].affectedRows === 1) {
            res.statusCode = 200;
            res.setHeader("Content-Type","application/json");
            res.json({result : "success"});
        }
        else throw Error("Database Error Occured. Please Try Again");
    } catch (err) {
        let errorPattern = /^Duplicate Entry/i;
        let result = err.message.match(errorPattern);
        if (result != null) err.message = "Email Address Already Registered";
        res.statusCode = 401;
        res.setHeader("Content-Type","application/json");
        res.json({result : err.message});
    }
})

module.exports = registerUserRouter;