const express = require('express');
const bodyParser = require('body-parser');
const mysql = require('mysql2/promise');
const cookieParser = require('cookie-parser');
const pool = require('../reusableFunctionalities/dbconnection');
const getSessionId = require('../reusableFunctionalities/getSessionId');
const v4 = require('node-uuid');
const fs = require('fs');
const raiseInvoiceRouter = express.Router();
raiseInvoiceRouter.use(bodyParser.json());
raiseInvoiceRouter.use(cookieParser());

let sessionId = undefined;
const writer = fs.createWriteStream('./access.log', { flags: 'a' });
raiseInvoiceRouter.use((req, res, next) => {
    if (req.cookies['userservice'] == undefined) {
        res.statusCode = 401;
        res.setHeader("Content-Type", "application/json");
        res.json({ result: 'Please login in order to continue', data: [] });
    }
    else if (req.cookies['userservice']) {
        sessionId = getSessionId(req.cookies['userservice']);
        if (sessionId == false) {
            res.clearCookie('userservice');
            res.statusCode = 401;
            res.setHeader("Content-Type", "application/json");
            res.json({ result: 'Please Stop Tampering with Data', data: [] });
        }
        else {
            return next();
        }
    }
})

raiseInvoiceRouter.route("/:businessid/:fyid")
    .post(async (req, res, next) => {
        try {
            let businessid = mysql.escape(req.params.businessid);
            let fyid = mysql.escape(req.params.fyid);
            let customerid = mysql.escape(req.body.customerid);
            let customername = mysql.escape(req.body.customername);
            let shippedtoname = mysql.escape(req.body.shippedtoname);
            let shippedtoid = mysql.escape(req.body.shippedtoid);
            let invoicedate = mysql.escape(req.body.invoicedate);
            let invoicenumber = mysql.escape(req.body.invoicenumber);
            let ponumber = mysql.escape(req.body.ponumber);
            let podate = mysql.escape(req.body.podate);
            let transportername = mysql.escape(req.body.transporter);
            let vehiclenumber = mysql.escape(req.body.vehiclenumber);
            let grno = mysql.escape(req.body.grno);
            let destination = mysql.escape(req.body.destination);
            let ewaybillnumber = mysql.escape(req.body.ewaybillnumber);
            let freightcharges = mysql.escape(req.body.freight);
            let terms = mysql.escape(req.body.terms);
            let item = mysql.escape(req.body.itemobject);
            let itemno = mysql.escape(req.body.itemno);
            let totalbasic = mysql.escape(req.body.totalbasic);
            let totaligst = mysql.escape(req.body.totaligst);
            let totalcgst = mysql.escape(req.body.totalcgst);
            let totalsgst = mysql.escape(req.body.totalsgst);
            let grandtotal = mysql.escape(req.body.grandtotal);
            let invoicetype = mysql.escape(req.body.invoicetype);
            let active = mysql.escape(req.body.active);
            let invoiceid = v4();
            let cash = mysql.escape(req.body.cash);
            let blamount = mysql.escape(req.body.blamount);
            let idempotencykey = mysql.escape(req.params.fyid + req.body.invoicenumber);
            let result = await pool.query("CALL insertInvoice ( " + mysql.escape(sessionId) + "," + mysql.escape(invoiceid) + "," + businessid + "," + customerid + "," + shippedtoid + "," + invoicenumber + "," + invoicedate + "," + ponumber + "," + podate + "," + transportername + "," + vehiclenumber + "," + grno + "," + destination + "," + freightcharges + "," + terms + "," + fyid + "," + ewaybillnumber + "," + itemno + "," + item + "," + totalbasic + "," + totaligst + "," + totalcgst + "," + totalsgst + "," + grandtotal + "," + invoicetype + "," + active + "," + idempotencykey + "," + customername + "," + shippedtoname + "," + cash + "," + blamount + ")");
            if (result[0][0][0].result) {
                if (result[0][0][0].result === "success") {
                    res.statusCode = 200;
                    res.setHeader("Content-Type", "application/json");
                    res.json({ result: "success", id: { invoiceid: result[0][1][0].checkid } });
                }
                else if (result[0][0][0].result === 'Invalid Session Id') {
                    res.clearCookie('userservice');
                    res.statusCode = 401;
                    res.setHeader("Content-Type", "application/json");
                    res.json({ result: 'Invalid Login' });
                }
            }
            else if (result[0][1][0].result) {
                if (result[0][1][0].result === "Already Registered") {
                    res.statusCode = 200;
                    res.setHeader("Content-Type", "application/json");
                    res.json({
                        result: "Already Registered", data: result[0][0][0]
                    });
                }
            }
            else throw new error(result[0][0][0].result);
        }
        catch (err) {
            writer.write(err.message);
            res.statusCode = 404;
            res.setHeader("Content-Type", "application/json");
            res.json({ "result": err.message });
        }
    })

    .get(async (req, res, next) => {
        try {
            let businessid = mysql.escape(req.params.businessid);
            let fyid = mysql.escape(req.params.fyid);
            const sql = "CALL getInvoice ( " + mysql.escape(sessionId) + "," + businessid + "," + fyid + ")";
            let result = await pool.query(sql);
            if (result[0][1][0]) {
                if (result[0][1][0].result === 'success') {
                    res.statusCode = 200;
                    res.setHeader("Content-Type", "application/json");
                    res.json({ result: "success", data: result[0][0] });
                }
            }
            else if (result[0][0][0].result === 'Invalid Session Id') {
                res.clearCookie('userservice');
                res.statusCode = 401;
                res.setHeader("Content-Type", "application/json");
                res.json({ result: 'Invalid Login', data: [] });
            }
            else throw new Error("Database Error");
        } catch (err) {
            writer.write(err.message);
            res.statusCode = 404;
            res.setHeader("Content-Type", "application/json");
            res.json({ "result": err.message, "data": [] });
        }
    })

    .put(async (req, res, next) => {
        try {
            let businessid = mysql.escape(req.params.businessid);
            let invoiceid = mysql.escape(req.body.invoiceid);
            let operation = req.body.operation;
            let active = mysql.escape(req.body.active);
            let result;
            if (operation === "updateinvoice") {
                let fyid = mysql.escape(req.params.fyid);
                let customerid = mysql.escape(req.body.customerid);
                let shippedtoid = mysql.escape(req.body.shippedtoid);
                let invoicedate = mysql.escape(req.body.invoicedate);
                let invoicenumber = mysql.escape(req.body.invoicenumber);
                let ponumber = mysql.escape(req.body.ponumber);
                let podate = mysql.escape(req.body.podate);
                let transportername = mysql.escape(req.body.transporter);
                let vehiclenumber = mysql.escape(req.body.vehiclenumber);
                let grno = mysql.escape(req.body.grno);
                let destination = mysql.escape(req.body.destination);
                let ewaybillnumber = mysql.escape(req.body.ewaybillnumber);
                let freightcharges = mysql.escape(req.body.freight);
                let terms = mysql.escape(req.body.terms);
                let item = mysql.escape(req.body.itemobject);
                let itemno = mysql.escape(req.body.itemno);
                let totalbasic = mysql.escape(req.body.totalbasic);
                let totaligst = mysql.escape(req.body.totaligst);
                let totalcgst = mysql.escape(req.body.totalcgst);
                let totalsgst = mysql.escape(req.body.totalsgst);
                let grandtotal = mysql.escape(req.body.grandtotal);
                let invoicetype = mysql.escape(req.body.invoicetype);
                let active = mysql.escape(req.body.active);
                let cash = mysql.escape(req.body.cash);
                let customername = mysql.escape(req.body.customername);
                let shippedtoname = mysql.escape(req.body.shippedtoname);
                let blamount = mysql.escape(req.body.blamount);
                let idempotencykey = mysql.escape(req.body.invoiceid + req.body.invoicenumber);
                result = await pool.query("CALL updateInvoice ( " + mysql.escape(sessionId) + "," + invoiceid + "," + businessid + "," + customerid + "," + shippedtoid + "," + invoicenumber + "," + invoicedate + "," + ponumber + "," + podate + "," + transportername + "," + vehiclenumber + "," + grno + "," + destination + "," + freightcharges + "," + terms + "," + fyid + "," + ewaybillnumber + "," + itemno + "," + item + "," + totalbasic + "," + totaligst + "," + totalcgst + "," + totalsgst + "," + grandtotal + "," + invoicetype + "," + active + "," + idempotencykey + "," + cash + "," + customername + "," + shippedtoname + "," + blamount + ")");
            }
            else if (operation === 'cancelinvoice') {
                let active = 1;
                result = await pool.query("CALL cancelInvoice ( " + mysql.escape(sessionId) + "," + invoiceid + "," + active + ")");
            }
            else if (operation === 'restoreinvoice') {
                let active = 0;
                result = await pool.query("CALL cancelInvoice ( " + mysql.escape(sessionId) + "," + invoiceid + "," + active + ")");
            }
            if (result[0][0][0].result === "success") {
                res.statusCode = 200;
                res.setHeader("Content-Type", "application/json");
                res.json({ result: "success" });
            }
            else if (result[0][0][0].result === 'Invalid Session Id') {
                res.clearCookie('userservice');
                res.statusCode = 401;
                res.setHeader("Content-Type", "application/json");
                res.json({ result: 'Invalid Login' });
            }
            else throw new error("Database Error");
        } catch (err) {
            writer.write(err.message);
            res.statusCode = 401;
            res.setHeader("Content-Type", "application/json");
            res.json({ result: err.message });
        }
    })

module.exports = raiseInvoiceRouter;