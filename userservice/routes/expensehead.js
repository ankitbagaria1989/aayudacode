const express = require('express');
const bodyParser = require('body-parser');
const mysql = require('mysql2/promise');
const cookieParser = require('cookie-parser');
const pool = require('../reusableFunctionalities/dbconnection');
const getSessionId = require('../reusableFunctionalities/getSessionId');
const v4 = require('node-uuid');
const fs = require('fs');

const expenseHeadRouter = express.Router();
expenseHeadRouter.use(bodyParser.json());
expenseHeadRouter.use(cookieParser());

let sessionId = undefined;
const writer = fs.createWriteStream('./access.log', { flags: 'a' });
expenseHeadRouter.use((req, res, next) => {
    if (req.cookies['userservice'] == undefined) {
        res.statusCode = 401;
        res.setHeader("Content-Type", "application/json");
        res.json({ result: 'Please login in order to continue', data: [] });
    }
    else if (req.cookies['userservice']) {
        sessionId = getSessionId(req.cookies['userservice']);
        if (sessionId == false) {
            res.clearCookie('userservice');
            res.statusCode = 401;
            res.setHeader("Content-Type", "application/json");
            res.json({ result: 'Please Stop Tampering with Data', data: [] });
        }
        else {
            return next();
        }
    }
})

expenseHeadRouter.route('/:businessid')
    .get(async (req, res, next) => {
        try {
            let businessid = mysql.escape(req.params.businessid);
            //let result = await pool.query("SELECT * FROM bankdetails WHERE businessid = " + businessid);
            const sql = "CALL getExpenseHead ( " + mysql.escape(sessionId) + "," + businessid + ")";
            let result = await pool.query(sql);
            if (result[0][1][0]) {
                if (result[0][1][0].result === 'success') {
                    res.statusCode = 200;
                    res.setHeader("Content-Type", "application/json");
                    res.json({ result: "success", data: result[0][0] });
                }
            }
            else if (result[0][0][0].result === 'Invalid Session Id') {
                res.clearCookie('userservice');
                res.statusCode = 401;
                res.setHeader("Content-Type", "application/json");
                res.json({ result: 'Invalid Login', data: [] });
            }
            else throw new Error("Database Error");

        } catch (err) {
            writer.write(err.message);
            res.statusCode = 401;
            res.setHeader("Content-Type", "application/json");
            res.json({ result: err.message, data: [] });
        }

    })

    .post(async (req, res, next) => {
        try {
            let businessid = mysql.escape(req.params.businessid);
            let expenseheadname = mysql.escape(req.body.expenseheadname);
            let expenseheadid = v4();
            let result = await pool.query("CALL insertExpenseHead (" + mysql.escape(sessionId) + "," + businessid + "," + expenseheadname + "," + mysql.escape(expenseheadid) + ")");
            if (result[0][0][0].result === "success") {
                res.statusCode = 200;
                res.setHeader("Content-Type", "application/json");
                res.json({ result: "success", id: { expenseheadid: result[0][1][0].checkid } });
            }
            else if (result[0][0][0].result === 'Invalid Session Id') {
                res.clearCookie('userservice');
                res.statusCode = 401;
                res.setHeader("Content-Type", "application/json");
                res.json({ result: 'Invalid Login' });
            }
            else throw new error(result[0][0][0].result);
        } catch (err) {
            writer.write(err.message);
            res.statusCode = 401;
            res.setHeader("Content-Type", "application/json");
            res.json({ result: err.message });
        }
    })
module.exports = expenseHeadRouter;