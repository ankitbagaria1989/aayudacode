const express = require('express');
const bodyParser = require('body-parser');
const mysql = require('mysql2/promise');
const cookieParser = require('cookie-parser');
const pool = require('../reusableFunctionalities/dbconnection');
const getSessionId = require('../reusableFunctionalities/getSessionId');
const v4 = require('node-uuid');
const fs = require('fs');

const raiseAssetOpeningRouter = express.Router();
raiseAssetOpeningRouter.use(bodyParser.json());
raiseAssetOpeningRouter.use(cookieParser());

let sessionId = undefined;
const writer = fs.createWriteStream('./access.log', { flags: 'a' });

raiseAssetOpeningRouter.use((req, res, next) => {
    if (req.cookies['userservice'] == undefined) {
        res.statusCode = 401;
        res.setHeader("Content-Type", "application/json");
        res.json({ result: 'Please login in order to continue', data: [] });
    }
    else if (req.cookies['userservice']) {
        sessionId = getSessionId(req.cookies['userservice']);
        if (sessionId == false) {
            res.clearCookie('userservice');
            res.statusCode = 401;
            res.setHeader("Content-Type", "application/json");
            res.json({ result: 'Please Stop Tampering with Data', data: [] });
        }
        else {
            return next();
        }
    }
})

raiseAssetOpeningRouter.route("/:businessid/:fyid")
    .post(async (req, res, next) => {
        try {
            let businessid = mysql.escape(req.params.businessid);
            let fyid = mysql.escape(req.params.fyid);
            let assetaccount = mysql.escape(req.body.assetaccount);
            let amount = mysql.escape(req.body.amount);
            let id = v4();
            let result = await pool.query("CALL updateassetopeningbalance ( " + mysql.escape(id) + "," + businessid + "," + fyid + "," + assetaccount + "," + amount + "," + mysql.escape(sessionId) + ")");
            if (result[0][0][0].result === "success") {
                res.statusCode = 200;
                res.setHeader("Content-Type", "application/json");
                res.json({ result: "success" });
            }
            else if (result[0][0][0].result === 'Invalid Session Id') {
                res.clearCookie('userservice');
                res.statusCode = 401;
                res.setHeader("Content-Type", "application/json");
                res.json({ result: 'Invalid Login' });
            }
            else throw new error("Database Error");
        }
        catch (err) {
            writer.write(err.message);
            res.statusCode = 404;
            res.setHeader("Content-Type", "application/json");
            res.json({ "result": err.message });
        }
    })

    .get(async (req, res, next) => {
        try {
            let fyid = mysql.escape(req.params.fyid);
            let result = await pool.query("SELECT * FROM assetopeningbalance WHERE fyid = " + fyid);
            if (result) {
                res.statusCode = 200;
                res.setHeader("Content-Type", "application/json");
                res.json({ "result": "success", "data": result[0] });
            }
            else throw new Error("Database Error");
        } catch (err) {
            writer.write(err.message);
            res.statusCode = 404;
            res.setHeader("Content-Type", "application/json");
            res.json({ "result": err.message, "data": [] });
        }
    })

module.exports = raiseAssetOpeningRouter;