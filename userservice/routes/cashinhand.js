const express = require('express');
const bodyParser = require('body-parser');
const mysql = require('mysql2/promise');
const cookieParser = require('cookie-parser');
const pool = require('../reusableFunctionalities/dbconnection');
const getSessionId = require('../reusableFunctionalities/getSessionId');
const v4 = require('node-uuid');
const fs = require('fs');

const cashinhandRouter = express.Router();
cashinhandRouter.use(bodyParser.json());
cashinhandRouter.use(cookieParser());

let sessionId = undefined;
const writer = fs.createWriteStream('./access.log', { flags: 'a' });

cashinhandRouter.use((req, res, next) => {
    if (req.cookies['userservice'] == undefined) {
        res.statusCode = 401;
        res.setHeader("Content-Type", "application/json");
        res.json({ result: 'Please login in order to continue', data: [] });
    }
    else if (req.cookies['userservice']) {
        sessionId = getSessionId(req.cookies['userservice']);
        if (sessionId == false) {
            res.clearCookie('userservice');
            res.statusCode = 401;
            res.setHeader("Content-Type", "application/json");
            res.json({ result: 'Please Stop Tampering with Data', data: [] });
        }
        else {
            return next();
        }
    }
})


cashinhandRouter.route('/:businessid/:fyid')
    .get(async (req, res, next) => {
        try {
            let businessid = mysql.escape(req.params.businessid);
            let fyid = mysql.escape(req.params.fyid);
            let result = await pool.query("CALL getCashInHand (" + mysql.escape(sessionId) + "," + businessid + "," + fyid + ")");
            if (result[0][1][0]) {
                if (result[0][1][0].result === 'success') {
                    res.statusCode = 200;
                    res.setHeader("Content-Type", "application/json");
                    res.json({ result: "success", amount: result[0][0][0] });
                }
            }
            else if (result[0][0][0].result === 'Invalid Session Id') {
                res.clearCookie('userservice');
                res.statusCode = 401;
                res.setHeader("Content-Type", "application/json");
                res.json({ result: 'Invalid Login', data: [] });
            }
            else throw new Error("Database Error");
        }
        catch (err) {
            writer.write(err.message);
            res.statusCode = 401;
            res.setHeader("Content-Type", "application/json");
            res.json({ result: err.message, data: [] });
        }
    })

    .put(async (req, res, next) => {
        try {
            let businessid = mysql.escape(req.params.businessid);
            let fyid = mysql.escape(req.params.fyid);
            let amount = mysql.escape(req.body.amount);
            let id = v4();
            let result = await pool.query("CALL updateCashInHand (" + mysql.escape(sessionId) + "," + businessid + "," + fyid + "," + amount + "," + mysql.escape(id) + ")");
            if (result[0][0][0].result === "success") {
                res.statusCode = 200;
                res.setHeader("Content-Type", "application/json");
                res.json({ result: "success", id: { cashinhandid: result[0][1][0].checkid } });
            }
            else if (result[0][0][0].result === 'Invalid Session Id') {
                res.clearCookie('userservice');
                res.statusCode = 401;
                res.setHeader("Content-Type", "application/json");
                res.json({ result: 'Invalid Login' });
            }
            else throw new error("Database Error");
        }
        catch (err) {
            writer.write(err.message);
            res.statusCode = 401;
            res.setHeader("Content-Type", "application/json");
            res.json({ result: err.message, data: [] });
        }
    })

module.exports = cashinhandRouter;