const cookieParser = require('cookie-parser');

function getSessionId (cookie) {
    const sessionId = cookieParser.signedCookie(cookie, 'dravid@987&!!--true,,.//eijfvliajh894798173!!');
    return sessionId;
}

/* function auth (req,res,next) {
    if (req.cookies['userservice'] == undefined) return false;
    cookie = req.cookies['userservice'];
    const sessioId = cookieParser.signedCookie(cookie, 'dravid@987&!!--true,,.//eijfvliajh894798173!!');
    return sessionId;
} */

module.exports = getSessionId;
//module.exports.auth = auth;