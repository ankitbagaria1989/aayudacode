#!/usr/bin/env node

/*const express = require('express');
const morgan = require('morgan');
const cors = require('cors');
const bodyParser = require('body-parser');
require('dotenv').config();
const cors = require('cors');

//import routes
const indexRouter = require('./routes/index');
const usersRouter = require('./routes/users');
const loginRouter = require('./routes/login');
const registerUserRouter = require('./routes/register');
const businessRouter = require('./routes/business');
const fyRouter = require('./routes/fy');
const subUserRouter = require('./routes/subuser');
const customerRouter = require('./routes/customer');
const vendorRouter = require('./routes/vendor');
const bankDetailRouter = require('./routes/bankdetail');
const itemRouter = require('./routes/item');
const raiseInvoiceRouter = require('./routes/raiseinvoice');
const raisePurchaseRouter = require('./routes/raisePurchase');
const raiseExpenseRouter = require('./routes/raiseExpense');
const raisePaymentRouter = require('./routes/raisePayment');
const raiseBankEntryRouter = require('./routes/raiseBankEntry');
const raiseOpeningClosingRouter = require('./routes/raiseOpeningClosing');
const raisePartyRouter = require('./routes/partyOpening');
const logoutRouter = require('./routes/logout');
const cashinhandRouter = require('./routes/cashinhand');
const employeeRouter = require('./routes/employee');
const updateAttendanceRouter = require('./routes/updateattendance');
const monthAttendanceRouter = require('./routes/monthattendance');
const updateSalaryRouter = require('./routes/updatesalary');
const raiseBankRouter = require('./routes/bankopeningbalance');
const gstOpeningBalanceRouter = require('./routes/gstopeningbalance');
const debitCreditRouter = require('./routes/debitcredit');
const deleteEntryRouter = require('./routes/deleteEntry');
const expenseHeadRouter = require('./routes/expensehead');
const raiseIncomeOSRouter = require('./routes/raiseincomeos');
const reviseSalaryRouter = require('./routes/reviseSalary');
const terminateEmployeeRouter = require('./routes/terminateemployee');
const changePasswordRouter = require('./routes/changePassword');
const loanaccountRouter = require('./routes/loanaccount');
const raiseLoanRouter = require('./routes/raiseloan');
const raiseInvestmentRouter = require('./routes/raiseInvestment');
const incomeHeadRouter = require('./routes/incomehead');
const assetaccountRouter = require('./routes/assetaccount');
const raiseDepreciationRouter = require('./routes/raiseDepreciation');
const raiseLoanOpeningRouter = require('./routes/loanopeningbalance');
const raiseAssetOpeningRouter = require('./routes/assetopeningbalance');
const investmentaccountRouter = require('./routes/investmentaccount');
const raiseInvestmentOpeningRouter = require('./routes/investmentOpeningBalance');
const raiseTdsOpeningRouter = require('./routes/tdsopeningbalance');
const raiseRCOpeningRouter = require('./routes/rcopeningbalance');
const raiseStockRouter = require('./routes/stockopeningbalance');
const raiseIncomeTaxOpeningRouter = require('./routes/incometaxopeningbalance');
const app = express();

// connect to db

//cors option
const corsOptions = {
    "origin": ['http://localhost:5000', 'http://localhost:3000', 'http://192.168.43.37:5000'],
    "methods": ['GET', 'PUT', 'POST', 'DELETE'],
    "optionsSuccessStatus": 200,
    "credentials": true
  }

//middleware

const accessLogStream = fs.createWriteStream(path.join(__dirname, 'access.log'), { flags: 'a' });
app.use(cors(corsOptions));
app.options('*', cors());
app.use(morgan('common', { stream: accessLogStream }));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'public')));
app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/login', loginRouter);
app.use('/register', registerUserRouter);
app.use('/business', businessRouter);
app.use('/fy', fyRouter);
app.use('/subuser', subUserRouter);
app.use('/customer', customerRouter);
app.use('/vendor', vendorRouter);
app.use('/bankdetail', bankDetailRouter);
app.use('/item', itemRouter);
app.use('/raiseinvoice', raiseInvoiceRouter);
app.use('/raisepurchase', raisePurchaseRouter);
app.use('/raiseexpense', raiseExpenseRouter);
app.use('/raisepayment', raisePaymentRouter);
app.use('/raisebankentry', raiseBankEntryRouter);
app.use('/raiseopeningclosing', raiseOpeningClosingRouter);
app.use('/partyOpening', raisePartyRouter);
app.use('/logout', logoutRouter);
app.use('/cashinhand', cashinhandRouter);
app.use('/employee', employeeRouter);
app.use('/updateattendance', updateAttendanceRouter);
app.use('/monthattendance', monthAttendanceRouter);
app.use('/updatesalary', updateSalaryRouter);
app.use('/bankOpeningBalance', raiseBankRouter);
app.use('/gstOpeningBalance', gstOpeningBalanceRouter);
app.use('/debitcredit', debitCreditRouter);
app.use('/deleteEntry', deleteEntryRouter);
app.use('/expensehead', expenseHeadRouter);
app.use('/raiseincomeos', raiseIncomeOSRouter);
app.use('/reviseSalary', reviseSalaryRouter);
app.use('/termninateEmployee', terminateEmployeeRouter);
app.use('/changepassword', changePasswordRouter);
app.use('/loanaccount', loanaccountRouter);
app.use('/raiseloan', raiseLoanRouter);
app.use('/raiseinvestment', raiseInvestmentRouter);
app.use('/incomehead', incomeHeadRouter);
app.use('/assetaccount', assetaccountRouter);
app.use('/raiseDepreciation', raiseDepreciationRouter);
app.use('/loanOpeningBalance', raiseLoanOpeningRouter);
app.use('/assetOpeningBalance', raiseAssetOpeningRouter);
app.use('/investmentaccount', investmentaccountRouter);
app.use('/investmentOpeningBalance', raiseInvestmentOpeningRouter);
app.use('/tdsOpeningbalance', raiseTdsOpeningRouter);
app.use('/rcOpeningBalance', raiseRCOpeningRouter);
app.use('/stockOpeningBalance', raiseStockRouter);
app.use('/incometaxOpeningbalance', raiseIncomeTaxOpeningRouter);
// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});
// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = process.env.NODE_ENV === 'development' ? err : {};
  console.log(err.message);
  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

const port = process.env.PORT || 8000;
app.listen(port, () => {
    console.log(`UserService API is running on port ${port}`);
});
*/

/**
 * Module dependencies.
 */

var app = require('./app');
var debug = require('debug')('userservice:server');
var http = require('http');
require('dotenv').config();
/**
 * Get port from environment and store in Express.
 */

//var port = normalizePort(process.env.PORT || '3000');
var port = process.env.PORT;
app.set('port', port);

/**
 * Create HTTP server.
 */

var server = http.createServer(app);

/**
 * Listen on provided port, on all network interfaces.
 */

server.listen(port);
server.on('error', onError);
server.on('listening', onListening);

/**
 * Normalize a port into a number, string, or false.
 */

function normalizePort(val) {
  var port = parseInt(val, 10);

  if (isNaN(port)) {
    // named pipe
    return val;
  }

  if (port >= 0) {
    // port number
    return port;
  }

  return false;
}

/**
 * Event listener for HTTP server "error" event.
 */

function onError(error) {
  if (error.syscall !== 'listen') {
    throw error;
  }

  var bind = typeof port === 'string'
    ? 'Pipe ' + port
    : 'Port ' + port;

  // handle specific listen errors with friendly messages
  switch (error.code) {
    case 'EACCES':
      console.error(bind + ' requires elevated privileges');
      process.exit(1);
      break;
    case 'EADDRINUSE':
      console.error(bind + ' is already in use');
      process.exit(1);
      break;
    default:
      throw error;
  }
}

/**
 * Event listener for HTTP server "listening" event.
 */

function onListening() {
  var addr = server.address();
  var bind = typeof addr === 'string'
    ? 'pipe ' + addr
    : 'port ' + addr.port;
  debug('Listening on ' + bind);
}


